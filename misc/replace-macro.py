
import re, sys

# s = "			this->result_of_integr [ psi_Q ] [ psi_Q ] [ int_psi_x_psi_x ] -= tmp + tmp;"

pat = re .compile ( "^(?P<before>.*)this->result_of_integr \[ (?P<psi_1>\w+) \] \[ (?P<psi_2>\w+) \] \[ (?P<int_psi>\w+) \] =(?P<after>.*)$" )
# result = pat .match (s)
# print ( result )
# sys .exit(0)
    
input_file  = open ( "finite-elem.cpp", 'r' )
output_file = open ( "finite-elem-modif.cpp", 'w' )
for l in input_file .readlines() :
    result = pat .match (l)
    if result :
        # print ( result .group ("before" ) + "this->result_of_integr [ " + result .group ("psi_1") + " ] [ " + result .group ("psi_2") + " ] [ " + result .group ("int_psi") + " ] = " + result .group ("after") )
        output_file .write ( result .group ("before") + "RESULT ( " + result .group ("psi_1") + ", " + result .group ("psi_2") + ", " + result .group ("int_psi") + " ) =" + result .group ("after") + '\n' )
    else :
        output_file .write (l)
