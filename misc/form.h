
   element = FiniteElement("Lagrange", tetrahedron, 1)
   v = TestFunction(element)
   u = TrialFunction(element)

   #  tetrahedron = PQRS
   #  P ( coord[0], coord[1],  coord[2]  )
   #  Q ( coord[3], coord[4],  coord[5]  )
   #  R ( coord[6], coord[7],  coord[8]  )
   #  S ( coord[9], coord[10], coord[11] )

   # a = ( 77*v.dx(0) * u.dx(0) + 100*v.dx(0) * u.dx(1) + 13*v.dx(0) * u.dx(2) \
   #       + 17*v.dx(1) * u.dx(0) + 23*v.dx(1) * u.dx(1) + 37*v.dx(1) * u.dx(2) \
   #       + 41*v.dx(2) * u.dx(0) + 47*v.dx(2) * u.dx(1) + 51*v.dx(2) * u.dx(2) ) * dx
   a = u * v * dx
   form = compile_form ( a )

	  
    PQ_x = J_c0 = coord[0] * FE8_C0_D001_Q1[0][0][0] + coord[3] * FE8_C0_D001_Q1[0][0][1];
    PQ_y = J_c3 = coord[1] * FE8_C0_D001_Q1[0][0][0] + coord[4] * FE8_C0_D001_Q1[0][0][1];
    PQ_z = J_c6 = coord[2] * FE8_C0_D001_Q1[0][0][0] + coord[5] * FE8_C0_D001_Q1[0][0][1];
    PR_x = J_c1 = coord[0] * FE8_C0_D001_Q1[0][0][0] + coord[6] * FE8_C0_D001_Q1[0][0][1];
    PR_y = J_c4 = coord[1] * FE8_C0_D001_Q1[0][0][0] + coord[7] * FE8_C0_D001_Q1[0][0][1];
    PR_z = J_c7 = coord[2] * FE8_C0_D001_Q1[0][0][0] + coord[8] * FE8_C0_D001_Q1[0][0][1];
    PS_x = J_c2 = coord[0] * FE8_C0_D001_Q1[0][0][0] + coord[9] * FE8_C0_D001_Q1[0][0][1];
    PS_y = J_c5 = coord[1] * FE8_C0_D001_Q1[0][0][0] + coord[10] * FE8_C0_D001_Q1[0][0][1];
    PS_z = J_c8 = coord[2] * FE8_C0_D001_Q1[0][0][0] + coord[11] * FE8_C0_D001_Q1[0][0][1];
    
    sp[0] = PR_y * PS_z
    sp[1] = PS_y * PR_z
    PRSyz = sp[2] = sp[0] - sp[1] = PR_y * PS_z - PS_y * PR_z
    sp[3] = PQ_x * PRSyz
    sp[4] = PS_y * PQ_z
    sp[5] = PQ_y * PS_z
    det_PQSyz = sp[6] = sp[4] - sp[5] = PS_y * PQ_z - PQ_y * PS_z
    sp[7] = PR_x * det_PQSyz
    sp[8] = sp[3] + sp[7] = PQ_x * PRSyz + PR_x * det_PQSyz
    sp[9] = PQ_y * PR_z
    sp[10] = PR_y * PQ_z
    det_PQRyz = sp[11] = sp[9] - sp[10] = PQ_y * PR_z - PR_y * PQ_z
    sp[12] = PS_x * det_PQRyz
    det = sp[13] = sp[8] + sp[12] = PQ_x * PRSyz + PR_x * det_PQSyz + PS_x * det_PQRyz
	 assert ( det > 0. );
    sp[14] = std::abs(sp[13]);
    A[0] = 0.01666666666666656 * sp[14];
    A[1] = 0.008333333333333293 * sp[14];
    A[2] = 0.008333333333333299 * sp[14];
    A[3] = 0.008333333333333297 * sp[14];
    A[4] = 0.008333333333333293 * sp[14];
    A[5] = 0.0166666666666667 * sp[14];
    A[6] = 0.008333333333333373 * sp[14];
    A[7] = 0.008333333333333371 * sp[14];
    A[8] = 0.008333333333333299 * sp[14];
    A[9] = 0.008333333333333373 * sp[14];
    A[10] = 0.01666666666666671 * sp[14];
    A[11] = 0.008333333333333373 * sp[14];
    A[12] = 0.008333333333333297 * sp[14];
    A[13] = 0.008333333333333371 * sp[14];
    A[14] = 0.008333333333333373 * sp[14];
    A[15] = 0.01666666666666671 * sp[14];
