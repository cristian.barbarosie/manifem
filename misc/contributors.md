[ManiFEM](https://manifem.rd.ciencias.ulisboa.pt) is being developed by
[Cristian Barbarosie](mailto:cristian.barbarosie@gmail.com)
and [Anca-Maria Toader](mailto:anca.maria.toader@gmail.com).

[Sérgio Lopes](mailto:slopes@adm.isel.pt) contributed to the initial stage of the code.

[Baptiste Claustre](mailto:baptiste.claustre@ens-lyon.fr)
and [Léo Moreau](mailto:leo.moreau@bordeaux-inp.fr)
helped by testing and reporting bugs.

See also the "Aknowledgements" paragraph in the
[manual](https://maniFEM.rd.ciencias.ulisboa.pt/manual-maniFEM.pdf).
