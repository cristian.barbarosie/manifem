
# create random geometries and test frontal mesh generation with thetrahedra
# to stop : "touch stop-new" or "touch stop-all"

import subprocess as sp
import threading as thr
import random, time, os, re, readline

#---------------------------------------------------------------------------------------------------------

class myObject ( object ) :  pass

#---------------------------------------------------------------------------------------------------------

class ResultsOnePass ( object ) :  pass

#---------------------------------------------------------------------------------------------------------

class Request ( object ) :
   def __init__ ( self, perce ) :
      self .percentage = perce
      self .condition = thr .Condition()

#---------------------------------------------------------------------------------------------------------
      
class GeomCase ( object ) :
   def __init__ ( self, geom_nb, nx, ny, nl ) :
      self .geom_nb = geom_nb
      self .nx = nx
      self .ny = ny
      self .nl = nl
      self .runs = []
      self .status = "running OK"

#---------------------------------------------------------------------------------------------------------
      
class Run ( object ) :
   def __init__ ( self, steps ) :
      self .nb_steps = steps

#---------------------------------------------------------------------------------------------------------
   
def get_dirname_from_case ( geom_case ) :
   assert  geom_case >= 0
   dirname = str ( geom_case )
   missing_zeroes = 5 - len ( dirname )
   assert  missing_zeroes >= 0
   dirname = '0' * missing_zeroes + dirname
   # while  len ( dirname ) < 5 :  dirname = '0' + dirname
   return  "geom-" + dirname

#---------------------------------------------------------------------------------------------------------

pattern_P = re .compile ("frontal-3d\.cpp line \d*, P \[ (.*) \]")

def performance_one_pass ( geom_case, steps_3d ) :
   # geom_case.nx and geom_case.ny define the aspect of the spheroid
   # geom_case.nl defines the variation of the desired segment length
   # all must be between -30 and 30

   global pattern_P

   results = ResultsOnePass ()

   dirname = get_dirname_from_case ( geom_case .geom_nb )
   input_file = open ( os .path .join ( dirname, "input.txt" ), 'w' )
   input_file .write ( str ( geom_case .nx ) + ' ' + str ( geom_case .ny ) +    \
                       ' ' + str ( geom_case .nl ) + '\n' )
   steps_2d = 9876
   input_file .write ( str ( steps_2d ) + '\n' )     # max steps
   input_file .write ( str ( steps_3d ) + '\n' )     # max steps
   # del input_file
   input_file = open ( os .path .join ( dirname, "input.txt" ), 'r' )

   # out = open ( "output.txt", mode='wt' )
   # inp = open ( "input.txt",  mode='rt' )
   # manifem_exe = os .path .expanduser ( "~/src/manifem/manifem-exe-test-3d" )
   manifem_exe = os .path .join ( os .getcwd(), "manifem-exe-test-3d" )
   with  global_vars .interact_lock :
      geom_case .active = Run ( steps_3d )
      geom_case .active .start_time = time .time()
   manifem_proc = sp .Popen ( manifem_exe, cwd = dirname, stdin = input_file,                   \
                              stdout = sp .PIPE, stderr = sp .STDOUT, text=True )

   tail_proc = sp .Popen ( ["/usr/bin/tail","-500"], stdin = manifem_proc .stdout,              \
                           stdout = sp .PIPE, stderr = sp .STDOUT, text = True    )

   for l in tail_proc .stdout .readlines() :
      ll = l .split()
      if l [:18] == "inner prod between" :
         assert  len ( ll ) == 6
         assert  ll [4] == "and"
         results .vol_min = float ( ll [3] )
         results .vol_max = float ( ll [5] )
         continue
      if l [:7] == "counter" :
         assert  len ( ll ) == 2
         results .counter_from_output = int ( ll [1] )
         continue
      if ( len (ll) > 3 ) and ( ll [3] == "interface" ) :
         assert  len ( ll ) == 7
         results .nb_faces = int ( ll [5] )
         continue
      if  ( len (ll) > 7 ) and ( ll [7] == "interface" ) :
         assert  len ( ll ) == 11
         assert  ll [8] == "has"
         assert  ll [10] == "faces"
         results .nb_faces = int ( ll [9] )
         continue
      m = pattern_P .search (l)
      if m :  results .P = '[ ' + m .group(1) + ' ]'

   manifem_proc .wait()
   tail_proc .wait()

   results .poll = manifem_proc .poll()
   if  not hasattr ( results, "counter_from_output" ) :
      results .counter_from_output = "unknown number of"
   if  not hasattr ( results, "vol_min" ) :  results .vol_min = "unknown"
   if  not hasattr ( results, "P" ) :  results .P = "unknown"

   with  global_vars .interact_lock :
      geom_case .active .results = results
      geom_case .active .end_time = time .time()
      duration = geom_case .active .end_time - geom_case .active .start_time
      geom_case .runs .append ( geom_case .active )
      del  geom_case .active
      if  results .poll != 0 :  return results
      assert  len ( global_vars .duration_formula .previous_cases ) <= 2
      if  len ( global_vars .duration_formula .previous_cases ) < 2 :
         global_vars .duration_formula .previous_cases [ steps_3d ] = duration
         if  len ( global_vars .duration_formula .previous_cases ) == 2 :
            s = []
            d = []
            for i in global_vars .duration_formula .previous_cases :
               s .append ( i )
               d .append ( global_vars .duration_formula .previous_cases [i] )
            assert  len(s) == 2
            assert  len(d) == 2
            global_vars .duration_formula .beta  = (d[1]-d[0]) / (s[1]-s[0])
            global_vars .duration_formula .alpha = d[1] - s[1] * global_vars .duration_formula .beta
      if  len ( global_vars .duration_formula .previous_cases ) == 2 :
         k = ( duration - global_vars .duration_formula .alpha                    \
               - global_vars .duration_formula .beta * steps_3d ) /               \
             ( 1 + steps_3d * steps_3d )
         global_vars .duration_formula .alpha += k
         global_vars .duration_formula .beta  += k * steps_3d
   
   return  results

#---------------------------------------------------------------------------------------------------------

def performance_after_error ( geom_case, steps, increment_steps ) :
   # an error has occcured (either poll != 0 or vol_min < 0.1)
   # somewhere between 'steps' and 'steps + increment_steps'
   # at 'steps' things were OK

   req = Request ( 45. )
   with  global_vars .cpu_load_lock :
      global_vars .request_queue .append ( req )
      global_vars .change_in_request_queue = True
   with  req .condition :  req .condition .wait()
   with  global_vars .interact_lock :
      if  geom_case .status == "please stop" :
         geom_case .status = "stopped by user, errors"
         return
   
   increment_steps = int ( increment_steps / 2 )
   results = performance_one_pass ( geom_case, steps + increment_steps )
   if  ( results .poll == 0 ) and ( results .vol_min > 0.1 ) :
      steps += increment_steps

   with  global_vars .interact_lock :
      if  increment_steps <= 1 :
         geom_case .status = "finished, errors found"
         return
      if  geom_case .status == "please stop" :
         geom_case .status = "stopped by user, errors"
         return
   
   performance_after_error ( geom_case, steps, increment_steps )
   
#---------------------------------------------------------------------------------------------------------
   
def performance ( geom_case ) :

   dirname = get_dirname_from_case ( geom_case_nb )
   os .mkdir ( dirname )
   
   steps = 0
   increment_steps = 8192

   while True :

      with  global_vars .interact_lock :
         if  geom_case .status == "please stop" :
            geom_case .status = "stopped by user"
            return
      req = Request ( 45. )
      with  global_vars .cpu_load_lock :
         global_vars .request_queue .append ( req )
         global_vars .change_in_request_queue = True
      with  req .condition :  req .condition .wait()
      with  global_vars .interact_lock :
         if  geom_case .status == "please stop" :
            geom_case .status = "stopped by user"
            return
   
      results = performance_one_pass ( geom_case, steps + increment_steps )
      if  ( results .poll == 0 ) and ( results .vol_min > 0.1 ) and ( results .nb_faces == 0 ) :
         geom_case .status = "finished OK"
         return

      if  ( results .poll != 0 ) or ( results .vol_min < 0.1 ) :
         with  global_vars .interact_lock :
            geom_case .status = "running, errors found"
         performance_after_error ( geom_case, steps, increment_steps )
         return
      
      steps += increment_steps

#---------------------------------------------------------------------------------------------------------
      
def get_cpu_percentage ( ) :

   # call 'top' with -n2, read only the second result
   read_top = sp .Popen ( ["/usr/bin/top",'-b','-n','2'], \
                          stdout = sp .PIPE, stderr = sp .STDOUT, text = True )
   first_passage = True
   percentage = -1.
   for l in read_top .stdout .readlines() :
      # %Cpu(s): 59.1 us,  0.0 sy,  0.0 ni, 40.9 id,  0.0 wa,  0.0 hi,  0.0 si,  0.0 st
      if  l [:8] == '%Cpu(s):' :
         # print ( l, end = '' )
         if  first_passage :  first_passage = False
         else :  # second passage
            ll = l .split()
            assert  len (ll) >= 16
            ll [1] = ll [1] .replace ( ',', '.' )
            percentage = float ( ll [1] )
   assert  percentage > -0.5

   read_top .wait()
   assert  read_top .poll() == 0

   return  percentage

#---------------------------------------------------------------------------------------------------------
      
def should_we_stop ( ) :
   with  global_vars .interact_lock :
      if  not  global_vars .stop_new :  return False
      for geom in global_vars .geometries .values() :
         if  geom .status [:7] == "running" :
            return False
   # notify calling threads, cleanup 'request_queue'
   # calling threads will know they must stop
   with global_vars .cpu_load_lock :
      for req in global_vars .request_queue :
         with req .condition :  req .condition .notify()
      del global_vars .request_queue [:]
   return True

#---------------------------------------------------------------------------------------------------------

def monitor_cpu_load ( ) :
   time_step = 1.5
   while True :
      while True :  # wait for requests from other threads
         if  should_we_stop() :  return
         with global_vars .cpu_load_lock :
            if  len ( global_vars .request_queue ) > 0 :  break
         time .sleep ( time_step )
      sleep_time = 2.9
      while True :
         slept = 0.
         while True :
            if  should_we_stop() :  return
            with global_vars .cpu_load_lock :
               if  global_vars .change_in_request_queue :  break
            if  slept > sleep_time :  break
            time .sleep ( time_step )
            slept += time_step
         if  sleep_time < 8. :  sleep_time *= 1.1
         with global_vars .cpu_load_lock :
            if global_vars .change_in_request_queue :
               global_vars .change_in_request_queue = False
               # which is the highest CPU percentage requested ?
               max_perce = 0.
               index = -1
               for i in range ( len ( global_vars .request_queue ) ) :
                  perce = global_vars .request_queue [i] .percentage
                  if  perce > max_perce :
                     max_perce = perce
                     index = i
         assert  index >= 0
         if  get_cpu_percentage() > max_perce :  continue
         with global_vars .cpu_load_lock :
            cond = global_vars .request_queue [index] .condition
            del global_vars .request_queue [index]
            del index
            with cond :  cond .notify()
            if  len ( global_vars .request_queue ) == 0 :  break
            global_vars .change_in_request_queue = True  # force computing again 'index' and 'max_perce'
         sleep_time = 2.9

#---------------------------------------------------------------------------------------------------------
         
def print_status_line (  geom  ) :
   print ( geom .status, end = '' )
   if  hasattr ( geom, "active" ) :
      active = geom .active
      print ( ", towards", active .nb_steps, end = " steps" )
      df = global_vars .duration_formula
      if  len ( df .previous_cases ) == 2 :
         eta = active .start_time + df .alpha + df .beta * active .nb_steps
         gmt = time .gmtime ( eta )
         hour = str ( gmt .tm_hour )
         minute = str ( gmt .tm_min )
         while  len ( minute ) < 2 :  minute = '0' + minute
         print ( ", ETA", hour + ':' + minute, end = '' )
         eta -= time .time()
         hour = max ( int ( eta / 3600 ), 0 )
         minute = max ( int ( eta / 60 - hour * 60 ), 0 )
         minute = str ( minute )
         while  len ( minute ) < 2 :  minute = '0' + minute
         print ( " (now+" + str ( hour ) + ':' +  minute, end = ")" )
   print ()

#---------------------------------------------------------------------------------------------------------
            
def show_status_all ( n ) :
   "one line per geometry case, only show geom_nb > n"
   with  global_vars .interact_lock :
      for geom_nb in global_vars .geometries :  # run over integer keys
         geom = global_vars .geometries [ geom_nb ]
         assert  geom .geom_nb == geom_nb
         if  geom_nb <= n :  continue
         print ( "geom", geom_nb, end = '' )
         if  len ( geom .runs ) > 0 :
            latest_run = geom .runs [-1]
            print ( ", performed", latest_run .results .counter_from_output, end = " steps" )
            print ( ", vol min", latest_run .results .vol_min, end = '' )
         print ( end = ", " )
         print_status_line ( geom )

#---------------------------------------------------------------------------------------------------------
            
def show_status (  geom_nb  ) :
   "history of geometry case geom_nb"
   with  global_vars .interact_lock :
      try :  geom = global_vars .geometries [ geom_nb ]
      except KeyError :  return
      assert  geom .geom_nb == geom_nb
      print ( "geom ", geom_nb, ", nx ", geom .nx, ", ny ", geom .ny, ", nl ", geom .nl, sep = '' )
      for  run  in  geom .runs :
         print ( "after", run .results .counter_from_output, "steps", end ='' )
         print ( ", vol min", run .results .vol_min, end = '' )
         print ( ", P", run .results .P )
      print_status_line ( geom )

#---------------------------------------------------------------------------------------------------------

def usage_interaction ( ) :
   print ( "usage:" )
   print ( "   show all" )
   print ( "   show > 15" )
   print ( "   show 34" )
   print ( "   stop new" )
   print ( "   stop all" )
   print ( "   stop 34" )
   print ( "   restart" )

#---------------------------------------------------------------------------------------------------------

def stop_all ( ) :
   global_vars .stop_new = True
   for geom in global_vars .geometries .values() :
      if  geom .status [:7] == "running" :
         geom .status_previous_to_stop = geom .status
         geom .status = "please stop"

#---------------------------------------------------------------------------------------------------------

def interaction ( ) :
   "wait for user commands"
   while True :
      try :  command = input ( "insert command " )
      except EOFError :
         print ()
         with  global_vars .interact_lock :  stop_all ()
         return
      comms = command .split()
      if  len ( comms ) == 0 :
         usage_interaction()
         continue
      match comms[0] :
         case "show" :
            if  len ( comms ) == 3 :
               assert comms[1] == '>'
               show_status_all ( int ( comms[2] ) )
            else :
               assert  len ( comms ) == 2
               if  comms[1] == "all" :  show_status_all (-1)
               else :  show_status ( int ( comms[1] ) )
         case "stop" :
            assert ( len ( comms ) == 2 )
            with  global_vars .interact_lock :
               match comms[1] :
                  case 'new' :  global_vars .stop_new = True
                  case "all" :  stop_all()
                  case geom_case_nb :
                     geom = global_vars .geometries [ int ( geom_case_nb ) ]
                     assert  geom .geom_nb == int ( geom_case_nb )
                     if  geom .status [:7] == "running" :
                        geom .status_previous_to_stop = geom .status
                        geom .status = "please stop"
         case "restart" :
            with  global_vars .interact_lock :
               global_vars .stop_new = False
               for geom in global_vars .geometries .values() :
                  if  geom .status == "please stop" :
                     geom .status = geom .status_previous_to_stop
            if  not  global_vars .monitor_cpu_thread .is_alive() :
               global_vars .monitor_cpu_thread = thr .Thread ( target = monitor_cpu_load )
               global_vars .monitor_cpu_thread .start()
         case "exit" :
            with  global_vars .interact_lock :  stop_all ()
            return
         case _ :  usage_interaction()

#---------------------------------------------------------------------------------------------------------

geom_case_nb = 1  # starts at

random .seed()
global_vars = myObject()

global_vars .cpu_load_lock = thr .Lock()
global_vars .request_queue = []
global_vars .change_in_request_queue = False

global_vars .interact_lock = thr .Lock()
global_vars .stop_new = False
global_vars .geometries = dict()
global_vars .duration_formula = myObject()
# duration of a process = alpha + beta * nb_steps
global_vars .duration_formula .previous_cases = dict()

global_vars .monitor_cpu_thread = thr .Thread ( target = monitor_cpu_load )
global_vars .monitor_cpu_thread .start()
global_vars .interact_thread = thr .Thread ( target = interaction )
global_vars .interact_thread .start()
         
while True :
   with  global_vars .interact_lock :
      if  global_vars .stop_new :  break
   req = Request ( 40.)
   with  global_vars .cpu_load_lock :
      global_vars .request_queue .append ( req )
      global_vars .change_in_request_queue = True
   with  req .condition :  req .condition .wait()
   with  global_vars .interact_lock :
      if  global_vars .stop_new :  break
   nx = random .randint ( -30, 30 )
   ny = random .randint ( -30, 30 )
   nl = random .randint ( -30, 30 )
   # nx and ny define the aspect of the spheroid
   # nl defines the variation of the desired segment length
   with  global_vars .interact_lock :
      geom_case = GeomCase ( geom_case_nb, nx, ny, nl )
      global_vars .geometries [ geom_case_nb ] = geom_case
   thr .Thread ( target = performance, args = [ geom_case ] ) .start()
   geom_case_nb += 1
