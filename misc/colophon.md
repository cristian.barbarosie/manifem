

# [colophon](https://www.google.com/search?q=define+colophon)

In the development of [maniFEM](https://manifem.rd.ciencias.ulisboa.pt) (and of its manual)
the following tools have been precious.

The [GNU/linux](https://www.gnu.org/gnu/linux-and-gnu.en.html) operating system, of course.

[g++](https://gcc.gnu.org/projects/cxx-status.html), [make](https://www.gnu.org/software/make/),
[git](https://git-scm.com/)

The [emacs](https://www.gnu.org/software/emacs/) text editor.
Nice features : one can open the same buffer in different frames with `C-x 5 b`,
this is useful for moving text around within one large file.
Search and replace limited to a region.
One can launch emacs in daemon mode then invoke emacs-client which opens very quickly.
Highly customizable : you can add your own shortcuts, useful especially when using awkward physical
keyboards.

[Cygwin](https://cygwin.com/) for turning a windows-running computer into a decent one.

[UserLAnd](https://userland.tech/) and [termius](https://termius.com/)
for working on an android-based tablet.
Alternatives : [termux](https://termux.dev/), [andronix](https://andronix.app/).

`screen` is invaluable, either for multiplexing when I work in a terminal with no X windows
or for launching remotely jobs which take a long time.

[spacedesk](https://www.spacedesk.net/) is a nice way to gain more display area in windows.

LaTeX with [scrreprt](https://ctan.org/pkg/scrreprt) (Koma-Script report class) and
[fancyvrb](https://ctan.org/pkg/fancyvrb) (sophisticated verbatim text).

[This script](https://codeberg.org/cristian.barbarosie/manifem-manual/src/branch/master/manip-number.py)
is useful for keeping track of numbering (of sections, paragraphs, figures) in the source of the manual.
The script was crucial when the manual was written in plain TeX;
now that I switched to LaTeX it is no longer essential but is still useful
(it allows the use of labels like "sect 6.fig 2" directly in the LaTeX source file).

[evince](https://wiki.gnome.org/Apps/Evince) (on windows) for viewing PostScript files,
[Foxit PDF Reader](https://www.foxit.com/pdf-reader/).

[gmsh](https://gmsh.info/), mainly for visualising meshes.

[Eigen](https://eigen.tuxfamily.org/),
a library for solving linear algebra problems.

Fast (hand-coded) integrators are inspired in the syntax of
[UFL](https://fenics.readthedocs.io/projects/ufl/en/latest/) and the output of
[FFC](https://fenics.readthedocs.io/projects/ffc/en/latest/).

