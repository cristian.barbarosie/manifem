
# use of FFC + UFL

from ufl import FiniteElement, triangle, tetrahedron, TestFunction, TrialFunction, dx, Index
from ffc import compile_form

# i = Index()
# j = Index()

# a = u.dx(1) * dx
# a1 = v * u * dx
# a1 = ( 77*v.dx(0) * u.dx(0) + 100*v.dx(0) * u.dx(1) + 13*v.dx(1) * u.dx(1) ) * dx
# a2 = ( v.dx(0) * u.dx(0) + v.dx(1) * u.dx(1) ) * dx

   
if False :

   element = FiniteElement("Lagrange", triangle, 1)
   v = TestFunction(element)
   u = TrialFunction(element)

   #  triangle = ABC
   #  A ( coordinate_dofs[0], coordinate_dofs[1] )
   #  B ( coordinate_dofs[2], coordinate_dofs[3] )
   #  C ( coordinate_dofs[4], coordinate_dofs[5] )

   a = v * u * dx
   form = compile_form ( a )


if True :

   element = FiniteElement("Lagrange", tetrahedron, 1)
   v = TestFunction(element)
   u = TrialFunction(element)

   #  tetrahedron = PQRS
   #  P ( coordinate_dofs[0], coordinate_dofs[1],  coordinate_dofs[2]  )
   #  Q ( coordinate_dofs[3], coordinate_dofs[4],  coordinate_dofs[5]  )
   #  R ( coordinate_dofs[6], coordinate_dofs[7],  coordinate_dofs[8]  )
   #  S ( coordinate_dofs[9], coordinate_dofs[10], coordinate_dofs[11] )

   # a = ( 77*v.dx(0) * u.dx(0) + 100*v.dx(0) * u.dx(1) + 13*v.dx(0) * u.dx(2) \
   #       + 17*v.dx(1) * u.dx(0) + 23*v.dx(1) * u.dx(1) + 37*v.dx(1) * u.dx(2) \
   #       + 41*v.dx(2) * u.dx(0) + 47*v.dx(2) * u.dx(1) + 51*v.dx(2) * u.dx(2) ) * dx
   a = u * v * dx
   form = compile_form ( a )

f = open ("form.h",'w')
for l in form : f.write(l)
# see 'tabulate_tensor'
