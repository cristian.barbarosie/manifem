
# test frontal mesh generation with thetrahedra by repeating prescribed steps
# to stop : "touch stop"

def usage () :
   print ( "usage : python repeat-steps.py {from,include} 3220 {to,exclude} 3240 [step 100] [{serial,parallel}]" )
   sys .exit (1)


import subprocess as sp
import threading as thr
import os, sys, time, shutil


def pad_with_zeroes (n) :
   dirname = str (n)
   missing_zeroes = 5 - len ( dirname )
   assert  missing_zeroes >= 0
   dirname = '0' * missing_zeroes + dirname
   return  dirname


def perform_step ( common_input, n ) :
   "perform n steps of frontal mesh generation with tetrahedra"

   dirname = pad_with_zeroes (n)
   assert  not os .path .exists ( dirname )

   print ( "starting process with", n, "steps" )
   os .mkdir ( dirname )
   manifem_exe = os .path .join ( os .getcwd(), "manifem-exe-test-3d" )
   manifem_proc = sp .Popen ( manifem_exe, cwd = dirname, stdin = sp .PIPE,                     \
                              stdout = sp .PIPE, stderr = sp .STDOUT, text=True )

   for l in common_input :
      manifem_proc .stdin .write (l)
   manifem_proc .stdin .write ( str (n) + '\n' )
   manifem_proc .stdin .flush()

   output_file = open ( "out-" + dirname + ".txt", 'w' )
   tail_proc = sp .Popen ( ["/usr/bin/tail","-500"], stdin = manifem_proc .stdout,               \
                           stdout = output_file, text = True                      )

   manifem_proc .wait()
   tail_proc .wait()

   if  os .path .exists ( os .path .join ( dirname, "interf.msh" ) ) :
      os .rename ( os .path .join ( dirname, "interf.msh" ), "interf-" + dirname + ".msh" )
   ll = os .listdir ( dirname )
   assert  len ( ll ) == 0
   os .rmdir ( dirname )


def get_cpu_percentage ( ) :

   # call 'top' with -n2, read only the second result
   read_top = sp .Popen ( ["/usr/bin/top",'-b','-n','2'], \
                          stdout = sp .PIPE, stderr = sp .STDOUT, text = True )
   first_passage = True
   percentage = -1.
   for l in read_top .stdout .readlines() :
      # %Cpu(s): 59.1 us,  0.0 sy,  0.0 ni, 40.9 id,  0.0 wa,  0.0 hi,  0.0 si,  0.0 st
      if  l [:8] == "%Cpu(s):" :
         # print ( l, end = '' )
         if  first_passage :  first_passage = False
         else :
            ll = l .split()
            assert  len (ll) >= 16
            ll [1] = ll [1] .replace ( ',', '.' )
            percentage = float ( ll [1] )
   assert  percentage > -0.5

   read_top .wait()
   assert  read_top .poll() == 0

   return  percentage


def remove_repeated ( first_n, after_last_n ) :
   "compare output files, remove repeated ones"
   for n in range ( first_n, after_last_n ) :
      out_current = "out-" + pad_with_zeroes (n) + ".txt"
      assert  os .path .exists ( out_current )
      interf_current = "interf-" + pad_with_zeroes (n) + ".msh"
      assert  os .path .exists ( interf_current )
      out_next = "out-" + pad_with_zeroes ( n+1 ) + ".txt"
      if  not os .path .exists ( out_next ) :
         assert  n+1 == after_last_n
         break
      interf_next = "interf-" + pad_with_zeroes ( n+1 ) + ".msh"
      assert  os .path .exists ( interf_next )
      diff_proc = sp .Popen ( [ "/usr/bin/diff", out_current, out_next ], stdin = sp .DEVNULL,       \
                              stdout = sp .PIPE, stderr = sp .STDOUT, text = True             )
      wc_proc   = sp .Popen ( "/usr/bin/wc", stdin = diff_proc .stdout, stdout = sp .PIPE,           \
                              stderr = sp .STDOUT, text = True                            )
      diff_proc .wait()
      wc_proc .wait()
      la = wc_proc .stdout .readlines()
      assert  len (la) == 1
      lb = la [0]
      lc = lb .split()
      assert  len (lc) == 3
      if  int ( lc [0] ) >= 5 :  continue
      diff_proc = sp .Popen ( [ "/usr/bin/diff", interf_current, interf_next ],                      \
                              stdin = sp .DEVNULL, stdout = sp .PIPE, stderr = sp .STDOUT,           \
                              text = True                                                 )
      wc_proc   = sp .Popen ( "/usr/bin/wc", stdin = diff_proc .stdout, stdout = sp .PIPE,           \
                              stderr = sp .STDOUT, text = True                            )
      diff_proc .wait()
      wc_proc .wait()
      la = wc_proc .stdout .readlines()
      assert  len (la) == 1
      lb = la [0]
      lc = lb .split()
      assert  len (lc) == 3
      assert  lc [0] == '0'
      # if  lc [0] != '0' :  continue
      os .remove ( out_current )
      os .remove ( interf_current )

      
args = sys .argv
if  len ( args ) not in { 5, 6, 7, 8 } :  usage()
if  ( len ( args ) == 6 ) and args [5] not in { "serial", "parallel" } :  usage()
if  ( len ( args ) == 8 ) and args [7] not in { "serial", "parallel" } :  usage()
if  args [1] not in { "include", "from" } :  usage()
if  args [3] not in { "exclude", "to"   } :  usage()
if  ( len ( args ) >= 7 ) and args [5] != "step" :  usage()
first_n = int ( args [2] )
after_last_n = int ( args [4] )
step = 1
if  len ( args ) >= 7 :  step = int ( args [6] )
serial = False   # defaults to parallel
if  len ( args ) == 6 :  serial = args [5] == "serial"
if  len ( args ) == 8 :  serial = args [7] == "serial"

input_file = open ( "input.txt", 'r' )
# first line of "input.txt" contains nx, ny, nl
# the first two describe the geometry of the spheroid
# the third describes the variarion on the seg length
# second line of "input.txt" is unimportant (number of steps for mesh generation with triangles)
common_input = []
for l in input_file .readlines() :
   if len ( l .split() ) > 0 :
      common_input .append (l)
assert  len ( common_input ) == 3
del common_input [-1]
assert  len ( common_input ) == 2
if  os .path .exists ( "stop" ) :  os .remove ( "stop" )

# launch processes
if  serial :
   for n in range ( first_n, after_last_n, step ) :
      if  os .path .exists ( "stop" ) :  break
      perform_step ( common_input, n )
else :  # parallel
   jobs = []
   for n in range ( first_n, after_last_n, step ) :
      if  os .path .exists ( "stop" ) :  break
      sleep_interval = 2.
      while  get_cpu_percentage() > 45. :
         time .sleep ( sleep_interval )
         if  sleep_interval < 8. :  sleep_interval *= 1.1
      t = thr .Thread ( target = perform_step, args = [ common_input, n ] )
      jobs .append (t)
      t .start()
   for t in jobs :  t .join()  # wait for all processes to finish

if  step == 1 :  remove_repeated ( first_n, after_last_n )



