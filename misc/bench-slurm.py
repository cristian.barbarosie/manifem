
# create random geometries and test frontal mesh generation with thetrahedra
# to stop : "touch stop-new" or "touch stop-all"

import subprocess as sp
import threading as thr
import random, time, os, re


class ResultsOnePass ( object ) :  pass


class Request ( object ) :
   def __init__ ( self, perce ) :
      self .percentage = perce
      self .condition = thr .Condition()

      
def get_dirname_from_case ( geom_case ) :
   assert  geom_case >= 0
   dirname = str ( geom_case )
   missing_zeroes = 5 - len ( dirname )
   assert  missing_zeroes >= 0
   dirname = '0' * missing_zeroes + dirname
   # while  len ( dirname ) < 5 :  dirname = '0' + dirname
   return  "geom-" + dirname


pattern_P = re .compile ("frontal-3d\.cpp line \d*, P \[ (.*) \]")

def performance_one_pass ( geom_case, nx, ny, nl, steps_3d, machine ) :

   # nx and ny define the aspect of the spheroid
   # nl defines the variation of the desired segment length
   # all must be between -30 and 30

   # print ( 'trying with ', steps_3d, ' steps', sep = '' )
   results = ResultsOnePass ()

   dirname = get_dirname_from_case ( geom_case )
   input_file = open ( os .path .join ( dirname, "input.txt" ), 'w' )
   input_file .write ( str (nx) + ' ' + str (ny) + ' ' + str (nl) + '\n' )
   steps_2d = 9876
   input_file .write ( str ( steps_2d ) + '\n' )     # max steps
   input_file .write ( str ( steps_3d ) + '\n' )     # max steps
   # del input_file
   input_file = open ( os .path .join ( dirname, "input.txt" ), 'r' )

   # out = open ('output.txt',mode='wt')
   # inp = open('input.txt',mode='rt')
   # manifem_exe = os .path .expanduser ( '~/src/manifem/manifem-exe-test-3d' )
   manifem_exe = os .path .join ( os .getcwd(), "manifem-exe-test-3d" )
   manifem_proc = sp .Popen ( ["srun","-p",machine,manifem_exe], cwd = dirname,                     \
                              stdin = input_file, stdout = sp .PIPE, stderr = sp .STDOUT, text=True )

   # 'tail' is executed on the local machine of course
   tail_proc = sp .Popen ( ["/usr/bin/tail","-500"], stdin = manifem_proc .stdout,              \
                           stdout = sp .PIPE, stderr = sp .STDOUT, text = True    )

   for l in tail_proc .stdout .readlines() :
      # print ( l, end = '' )
      ll = l .split()
      if l [:18] == "inner prod between" :
         assert  len ( ll ) == 6
         assert  ll [4] == "and"
         results .vol_min = float ( ll [3] )
         results .vol_max = float ( ll [5] )
         # print ( 'python: vol_min ', results .vol_min, ', vol max ', results .vol_max )
         continue
      if l [:7] == "counter" :
         assert  len ( ll ) == 2
         results .counter_from_output = int ( ll [1] )
         # print ( "python: counter", results .counter_from_output )
         continue
      if ( len (ll) > 3 ) and ( ll [3] == "interface" ) :
         assert  len ( ll ) == 7
         results .nb_faces = int ( ll [5] )
         # print ( "python: nb faces", results .nb_faces )
         continue
      if  ( len (ll) > 7 ) and ( ll [7] == "interface" ) :
         assert  len ( ll ) == 11
         assert  ll [8] == "has"
         assert  ll [10] == "faces"
         results .nb_faces = int ( ll [9] )
         # print ( 'python: nb faces ', results .nb_faces )
         continue
      m = pattern_P .search (l)
      if m :  results .P = '[ ' + m .group(1) + ' ]'

   manifem_proc .wait()
   tail_proc .wait()
   
   results .poll = manifem_proc .poll()
   # print ( "python: poll", results .poll )
   if  not hasattr ( results, "counter_from_output" ) :
      results .counter_from_output = "unknown number of"
   if  not hasattr ( results, "vol_min" ) :  results .vol_min = "unknown"
   if  not hasattr ( results, "P" ) :  results .P = "unknown"
   print ( "geom ", geom_case, ", nx ", nx, ", ny ", ny, ", nl ", nl, ", ", machine,         \
           ", after ", results .counter_from_output,                                         \
           " steps, vol min ", results .vol_min, ", P ", results .P, sep = ''       )
   print ( "geom ", geom_case, ", nx ", nx, ", ny ", ny, ", nl ", nl, ", ", machine,         \
           ", after ", results .counter_from_output, ' steps, vol min ',                     \
           results .vol_min, ", P ", results .P, sep = '', file = output_file       )
   output_file .flush()

   return  results


def performance_after_error ( geom_case, nx, ny, nl, steps, increment_steps ) :
   # an error has occcured (either poll != 0 or vol_min < 0.1)
   # somewhere between 'steps' and 'steps + increment_steps'
   # at 'steps' things were OK

   global request_queue, change_in_request_queue, cpu_load_lock, machines
   if  os .path .exists ( "stop-all" ) :  return
   req = Request ( 45. )
   with  cpu_load_lock :
      request_queue .append ( req )
      for machine in machines :  change_in_request_queue [ machine ] = True
   with  req .condition :  req .condition .wait()
   if  os .path .exists ( "stop-all" ) :  return
   
   increment_steps = int ( increment_steps / 2 )
   assert  hasattr ( req, "machine" )  # not used below, a new request will be made in 'performance_one_pass'
   results = performance_one_pass ( geom_case, nx, ny, nl, steps + increment_steps, req .machine )
   if  ( results .poll == 0 ) and ( results .vol_min > 0.1 ) :
      steps += increment_steps
   if  increment_steps <= 1 :
      print ( "geom ", geom_case, ", nx ", nx, ", ny ", ny, ", nl ", nl,                        \
              ", error after ", steps, " steps", sep = ''               )
      print ( "geom ", geom_case, ", nx ", nx, ", ny ", ny, ", nl ", nl,                        \
              ", error after ", steps, " steps", sep = '', file = output_file )
      output_file .flush()
      return
   performance_after_error ( geom_case, nx, ny, nl, steps, increment_steps )
   
   
def performance ( geom_case, nx, ny, nl ) :

   # nx and ny define the aspect of the spheroid
   # nl defines the variation of the desired segment length
   # all three must be between -30 and 30

   global request_queue, change_in_request_queue, cpu_load_lock, machines
   dirname = get_dirname_from_case ( geom_case )
   os .mkdir ( dirname )
   
   steps = 0
   increment_steps = 8192

   while True :
      if  os .path .exists ( "stop-all" ) :  return
      req = Request ( 45. )
      with  cpu_load_lock :
         request_queue .append ( req )
         for machine in machines :  change_in_request_queue [ machine ] = True
      with  req .condition :  req .condition .wait()
      if  os .path .exists ( "stop-all" ) :  return
   
      assert  hasattr ( req, "machine" )
      results = performance_one_pass ( geom_case, nx, ny, nl, steps + increment_steps, req .machine )
      if  ( results .poll == 0 ) and ( results .vol_min > 0.1 ) and ( results .nb_faces == 0 ) :
         print ( "geom ", geom_case, ", success after ", results .counter_from_output,                \
                 " steps, vol min ", results .vol_min, sep = ''                       )
         print ( "geom ", geom_case, ", success after ", results .counter_from_output,                \
                 " steps, vol min ", results .vol_min, sep = '', file = output_file   )
         output_file .flush()
         return
      if  ( results .poll != 0 ) or ( results .vol_min < 0.1 ) :
         performance_after_error ( geom_case, nx, ny, nl, steps, increment_steps )
         return
      steps += increment_steps

      
def get_cpu_percentage ( machine ) :

   # call 'top' with -n2, read only the second result
   read_top = sp .Popen ( ["srun","-p",machine,"/usr/bin/top",'-b','-n','2'], \
                          stdout = sp .PIPE, stderr = sp .STDOUT, text = True )
   first_passage = True
   percentage = -1.
   for l in read_top .stdout .readlines() :
      # %Cpu(s): 59.1 us,  0.0 sy,  0.0 ni, 40.9 id,  0.0 wa,  0.0 hi,  0.0 si,  0.0 st
      if  l [:8] == '%Cpu(s):' :
         # print ( l, end = '' )
         if  first_passage :  first_passage = False
         else :  # second passage
            ll = l .split()
            assert  len (ll) >= 16
            ll [1] = ll [1] .replace ( ',', '.' )
            percentage = float ( ll [1] )
   assert  percentage > -0.5

   read_top .wait()
   assert  read_top .poll() == 0

   return  percentage

      
def should_we_stop ( ) :
   keep_running = not os .path .exists ( "stop-all" )
   if  keep_running and os .path .exists ( "stop-new" ) :
      nb_thr = thr .active_count()
      assert  nb_thr >= 2
      keep_running = nb_thr > 2
   if  keep_running :  return False
   # notify calling threads, cleanup 'request_queue'
   # calling threads will know they must stop because they see files "stop-{new,all}"
   with cpu_load_lock :
      for req in request_queue :
         with req .condition :  req .condition .notify()
      del request_queue [:]
   return True


def monitor_cpu_load ( machine ) :
   global request_queue, change_in_request_queue, cpu_load_lock, machines
   time_step = 1.5
   while True :
      while True :  # wait for requests from other threads
         if  should_we_stop() :  return
         with cpu_load_lock :
            if  len ( request_queue ) > 0 :  break
         time .sleep ( time_step )
      sleep_time = 2.9
      while True :
         slept = 0.
         while True :
            if  should_we_stop() :  return
            with cpu_load_lock :
               if  change_in_request_queue [ machine ] :  break
            if  slept > sleep_time :  break
            time .sleep ( time_step )
            slept += time_step
         if  sleep_time < 8. :  sleep_time *= 1.1
         with cpu_load_lock :
            if  not change_in_request_queue [ machine ] :  continue
            change_in_request_queue [ machine ] = False
            # which is the highest CPU percentage requested ?
            max_perce = 0.
            index = -1
            for i in range ( len ( request_queue ) ) :
               perce = request_queue [i] .percentage
               if  perce > max_perce :
                  max_perce = perce
                  index = i
            if  index < 0 :  continue
            if  get_cpu_percentage ( machine ) > max_perce :  continue
            cond = request_queue [index] .condition
            request_queue [index] .machine = machine
            del request_queue [index]
            del index
            with cond :  cond .notify()
            if  len ( request_queue ) == 0 :  break
            change_in_request_queue [ machine ] = True  # force computing again 'index' and 'max_perce'
         sleep_time = 2.9

      
machines = [ 'salvia', 'paeonia', 'rosmarinus']
geom_case = 1  # starts at

random .seed()
if  os .path .exists ( "stop-all" ) :  os .remove ( "stop-all" )
if  os .path .exists ( "stop-new" ) :  os .remove ( "stop-new" )
output_file = open ( "bench.out.txt", 'a' )
cpu_load_lock = thr .Lock()
request_queue = []
change_in_request_queue = {}
for machine in machines :  change_in_request_queue [ machine ] = False

for machine in machines :
   thr .Thread ( target = monitor_cpu_load, args = [ machine ] ) .start()
         
while True :
   if  os .path .exists ( "stop-all" )  or  os .path .exists ( "stop-new" ) :  break
   req = Request ( 40.)
   with  cpu_load_lock :
      request_queue .append ( req )
      for machine in machines :  change_in_request_queue [ machine ] = True
   with  req .condition :  req .condition .wait()
   if  os .path .exists ( "stop-all" )  or  os .path .exists ( "stop-new" ) :  break
   nx = random .randint ( -30, 30 )
   ny = random .randint ( -30, 30 )
   nl = random .randint ( -30, 30 )
   # nx and ny define the aspect of the spheroid
   # nl defines the variation of the desired segment length
   assert  hasattr ( req, "machine" )  # not used below, a new request will be made in 'performance'
   thr .Thread ( target = performance, args = [ geom_case, nx, ny, nl ] ) .start()
   geom_case += 1
