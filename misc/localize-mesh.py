
# takes all files named 'interf-*.msh' in the current directory
# produces localized mesh files 'interf-*-loc.msh'

def usage () :
   print ( "usage : python localize_mesh.py around 0.1 0.12 -1.2 radius 2.5" )
   sys .exit (1)


import subprocess as sp
import threading as thr
import os, sys, time, shutil, glob, re

args = sys .argv
if  len ( args ) != 7 :  usage()
if  args [1] != "around" :  usage()
if  args [5] != "radius" :  usage()
# do not convert arguments to numeric types, use strings
Px = args [2]
Py = args [3]
Pz = args [4]
radius = args [6]

rex = re .compile ( r"(interf\-)(\d+)(\.msh)\Z" )
manifem_exe = os .path .join ( os .getcwd(), "manifem-exe-test-locali" )
for mesh_file in glob .iglob ( "interf-*.msh" ) :
   ma = rex .match ( mesh_file )
   if  ma == None :  continue
   assert  ma .group(0) == mesh_file
   # print ( "localizing", mesh_file )
   loc_mesh_file = ma .group(1) + ma .group(2) + "-loc" + ma .group(3)
   os .replace ( mesh_file, "interf.msh" )
   manifem_proc = sp .Popen ( manifem_exe, stdin = sp .PIPE, text = True )
   manifem_proc .stdin .write ( Px + ' ' + Py + ' ' + Pz + '\n' )
   manifem_proc .stdin .write ( radius + '\n' )
   manifem_proc .stdin .flush()
   manifem_proc .wait()
   os .replace ( "interf.msh", mesh_file )
   os .replace ( "interf-loc.msh", loc_mesh_file )



#  prefix = vere .begin + vere .string ( "interf-" )
#  index = vere .digit .repeat_at_least_once    # r"\d+"
#  sufix = vere .string ( ".msh" ) + vere .end
#  pattern = prefix + index + sufix
#  reali = pattern .match ( "interf-2341.ms" )
#  reali & prefix == "interf-"
#  reali & index == "2341"
#  reali & sufix == ".msh"


