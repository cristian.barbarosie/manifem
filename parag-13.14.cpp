
// example presented in paragraph 13.14 of the manual
// https://maniFEM.rd.ciencias.ulisboa.pt/manual-manifem.pdf
// build a MetricTree over a cloud of points in RR2


#include <fstream>
#include <random>
std::ofstream file_ps ("metric-tree.eps");

#include "metric-tree-verbose.h"
// metric-tree-verbose.h  draws a PS file
// for normal use, include 
// https://codeberg.org/cristian.barbarosie/MetricTree/src/branch/master/metric-tree.h


class SqDistanceOnRn

{	public:
	inline double operator() ( const std::vector<double> &, const std::vector<double> & );
};
	
//-----------------------------------------------------------------------------------------------//

inline double SqDistanceOnRn::operator()
( const std::vector < double > & u, const std::vector < double > & v )

{	double res = 0.;
	size_t n = u .size();
	assert ( n == v .size() );
	for ( size_t i = 0; i < n; i++ )
	{	double tmp = u[i] - v[i];
		res += tmp*tmp;            }
	return res;                       }

//-----------------------------------------------------------------------------------------------//


int main ()

{	SqDistanceOnRn sq_dist_Rn;
	MetricTree < std::vector < double >, SqDistanceOnRn > cloud ( sq_dist_Rn, 1., 6. );
	// second argument (1.) is the rank-zero distance
	// can be thought of as the average distance between points in the cloud
	// third argument (6.) is the ration between successive distances

	double scale_factor = 10.;
	double border = 0.5;
	double xmin = 0., xmax = 60., ymin = 0., ymax = 30.;
	double translation_x = -scale_factor*xmin;
	double translation_y = -scale_factor*ymin;
	file_ps << "%!PS-Adobe-3.0 EPSF-3.0" << std::endl;
	file_ps << "%%Title:                     MetricTree" << std::endl;
	file_ps << "%%BoundingBox:  0 0 " << " " << scale_factor*(xmax-xmin+2*border)
	        << "   " << scale_factor*(ymax-ymin+2*border) << std::endl;
	file_ps << "%%EndComments" << std::endl;
	file_ps << "%%BeginSetup" << std::endl;
	file_ps << "<< /PageSize [" << scale_factor*(xmax-xmin+2*border) << " "
	        << scale_factor*(ymax-ymin+2*border) << "] >> setpagedevice" << std::endl;
	file_ps << "%%EndSetup" << std::endl << std::endl;
	file_ps << "gsave" << std::endl;
	file_ps << translation_x + scale_factor*border << " "
	        << translation_y + scale_factor*border << " translate" << std::endl;
	file_ps << scale_factor << " dup scale" << std::endl << std::endl;
	file_ps << "gsave " << 0.9 / scale_factor << " setlinewidth" << std::endl;
	file_ps << "0.5 setgray" << std::endl;
		
	std::default_random_engine random_generator;
	random_generator .seed ( 4 );
	std::uniform_real_distribution < double > distr ( 0., 30. );
	for ( int i = 0; i < 300; i++ )
	{	double x = 2. * distr ( random_generator ), y = distr ( random_generator );
		cloud .add ( { x, y } );                                                     }
	cloud .draw_ps ( file_ps );
	
	std::vector < double > P { 25, 5 };
	cloud .find_close_neighbours_of ( P, 5. );
	file_ps << "1 0 0 setrgbcolor ";
	file_ps << P [0] << " " << P [1] << " moveto ";
	file_ps << P [0] << " " << P [1] << " 0.3 0 360 arc fill" << std::endl;

	file_ps << "grestore" << std::endl;
	file_ps << "grestore" << std::endl << std::endl;
	file_ps << "showpage" << std::endl;
	file_ps << "%%Trailer" << std::endl;
	file_ps << "%EOF" << std::endl;

	cloud .remove ( cloud .root );

	std::cout << "produced file metric-tree.eps" << std::endl;
	std::cout << "arrows not drawn, see comments in parag-13.14.cpp" << std::endl;
	// in order to get arrows, go to 'draw_arrows' in metric-tree-verbose.h and
	// replace 'lineto' by 'Lineto^', then 'make run-parag-13.14',
	// then copy the definitions of 'ArrowHead' and 'Lineto^' from arrows.ps into metric-tree.eps

}  // end of main
