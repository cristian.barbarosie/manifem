
//   this file is part of maniFEM, a C++ library for meshes and finite elements on manifolds
//   development version

//   Copyright  2019 - 2025  Cristian Barbarosie  cristian.barbarosie@gmail.com

//   https://maniFEM.rd.ciencias.ulisboa.pt/
//   https://codeberg.org/cristian.barbarosie/maniFEM

//   ManiFEM is free software: you can redistribute it and/or modify it
//   under the terms of the GNU Lesser General Public License as published
//   by the Free Software Foundation, either version 3 of the License
//   or (at your option) any later version.

//   ManiFEM is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//   See the GNU Lesser General Public License for more details.

//   You should have received a copy of the GNU Lesser General Public License
//   along with maniFEM.  If not, see <https://www.gnu.org/licenses/>.


#include "src/mesh.h"
#include "src/iterator.h"
#include "src/field.h"
#include "src/function.h"
#include "src/manifold.h"

#ifndef MANIFEM_NO_FEM
#include "src/finite-elem.h"
#include "src/var-form.h"
#endif  // ifndef MANIFEM_NO_FEM


//------------------------------------------------------------------------------------------------------//
