
//   finite-elem-xx.h  2024.08.31

//   This file is part of maniFEM, a C++ library for meshes and finite elements on manifolds.

//   Copyright  2019 - 2024  Cristian Barbarosie  cristian.barbarosie@gmail.com

//   https://maniFEM.rd.ciencias.ulisboa.pt/
//   https://codeberg.org/cristian.barbarosie/maniFEM

//   ManiFEM is free software: you can redistribute it and/or modify it
//   under the terms of the GNU Lesser General Public License as published
//   by the Free Software Foundation, either version 3 of the License
//   or (at your option) any later version.

//   ManiFEM is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//   See the GNU Lesser General Public License for more details.

//   You should have received a copy of the GNU Lesser General Public License
//   along with maniFEM.  If not, see <https://www.gnu.org/licenses/>.


// included only from  finite-elem.cpp


#ifndef MANIFEM_FINITE_ELEM_XX_H
#define MANIFEM_FINITE_ELEM_XX_H

#include <set>
#include <array>


namespace maniFEM {


class Integrator::Gauss : public Integrator::Core

{	public :

	FiniteElement finite_element;
	
	std::vector < Cell > points;
	std::vector < double > weights;
	
	// constructor

	inline Gauss ( const tag::gauss_quadrature & q )
	: Integrator::Core (), finite_element ( tag::non_existent )
	{	switch ( q )
		{	case tag::tri_3 : std::cout << "tri 3" << std::endl;  break;
			case tag::tri_4 : std::cout << "tri 4" << std::endl;  break;
			case tag::tri_6 : std::cout << "tri 6" << std::endl;  break;
			default: std::cout << "default" << std::endl;                 }  }

	Gauss ( const tag::gauss_quadrature & q,
	        const tag::FromFiniteElementWithMaster &, FiniteElement & fe );

	double action ( const FiniteElement & fe );
	double action ( const double f, const FiniteElement & fe );
	double action ( Function f, const FiniteElement & fe );
	double action ( Function f );
	double action ( const Function & f, const tag::On &, const Cell & cll );
	double action ( const Function & f, const tag::On &, const Mesh & msh );
  // virtual from Integrator::Core
	
	//  pre_compute  and  retrieve_precomputed  are virtual from Integrator::Core,
	// here execution forbidden
	void pre_compute ( const std::vector < Function > & bf,
	                   const std::vector < Function > & res );
	std::vector < double > retrieve_precomputed ( const Function & bf, const Function & psi );
	std::vector < double > retrieve_precomputed
	( const Function & bf1, const Function & psi1,
	  const Function & bf2, const Function & psi2 );

	void dock_on ( const Cell & cll );  // virtual from Integrator::Core

};  // end of  class Integrator::Gauss

//------------------------------------------------------------------------------------------------------//

	
class Integrator::HandCoded : public Integrator::Core

// arithmetic expressions computed by hand -- long and tedious computations

// some of these computations are inspired in UFL and FFC
// https://fenics.readthedocs.io/projects/ufl/en/latest/
// https://fenics.readthedocs.io/projects/ffc/en/latest/

// in other cases, computations were done bluntly by hand, from scratch

{	public :

	// this type of integrator is tightly linked to a finite element,
	// so we include it as an attribute
	FiniteElement finite_element;

	//constructor
	
	inline HandCoded ( const tag::FromFiniteElement &, FiniteElement & f )
	:	finite_element { f }
	{	}

	double action ( const FiniteElement & fe );
	double action ( const double f, const FiniteElement & fe );
	double action ( Function f, const FiniteElement & fe );
	double action ( Function f );
	double action ( const Function & f, const tag::On &, const Cell & cll );
	double action ( const Function & f, const tag::On &, const Mesh & msh );
	// virtual from Integrator::Core
	
	// this type of integrator benefits from an early declaration of
	// the integrals we intend to compute later (after docking on a cell)
	//  pre_compute  is virtual from Integrator::Core
	void pre_compute ( const std::vector < Function > & bf,
	                   const std::vector < Function > & res );

	void dock_on ( const Cell & cll );  // virtual from Integrator::Core
	// here execution forbidden, may change in the future

};  // end of  class Integrator::HandCoded

//------------------------------------------------------------------------------------------------------//


class FiniteElement::WithMaster : public FiniteElement::Core

// finite elements which use a master element
// abstract class, instantiated in FiniteElement::WithMaster::***

{	public :

	// attributes inherited from FiniteElement::Core :
	// Integrator integr
	// std::map < Cell::Core *, Function > base_fun_1
	// std::map < Cell::Core *, std::map < Cell::Core *, Function > > base_fun_2
	// Cell docked_on
	// std::string info_string (ifndef NDEBUG)

	Manifold master_manif;
	Function transf;

	// constructor

	inline WithMaster ( Manifold m )
	: FiniteElement::Core (), master_manif (m), transf ( 0. )
	{ }

	// two methods  dock_on  stay pure virtual from FiniteElement::Core
	// void dock_on ( const Cell & cll )
	// void dock_on ( const Cell & cll, const tag::Winding & )

	// two methods  dock_on  only significant for
	// FiniteElement::StandAlone::TypeOne::{Rectangle,Square}
	// virtual from FiniteElement::Core, execution forbidden
	// void dock_on ( const Cell & cll, const tag::FirstVertex &, const Cell & )
	// void dock_on ( const Cell & cll, const tag::FirstVertex &, const Cell &,
	//                const tag::Winding &                                     )

	//  pre_compute  virtual from FiniteElement::Core, here execution forbidden
	void pre_compute ( const std::vector < Function > & bf,
	                   const std::vector < Function > & result );
	std::vector < double > retrieve_precomputed ( const Function & bf, const Function & psi );
	std::vector < double > retrieve_precomputed
	( const Function & bf1, const Function & psi1, const Function & bf2, const Function & psi2 );

	// Cell::Numbering & build_global_numbering ( )  stays pure virtual from FiniteElement::Core
	
	#ifndef NDEBUG  // DEBUG mode
	std::string info ( );  // virtual from FiniteElement::Core
	#endif

	class Segment;  class Triangle;  class Quadrangle;  class Tetrahedron;  class Hexahedron;
	
};  // end of  class FiniteElement::WithMaster

//------------------------------------------------------------------------------------------------------//


class FiniteElement::WithMaster::Segment : public FiniteElement::WithMaster

// linear finite elements which use a master element

{	public :

	// attributes inherited from FiniteElement::Core :
	// Integrator integr
	// std::map < Cell::Core *, Function > base_fun_1
	// std::map < Cell::Core *, std::map < Cell::Core *, Function > > base_fun_2
	// Cell docked_on
	// std::string info_string (ifndef NDEBUG)

	// attributes inherited from FiniteElement::WithMaster :
	// Manifold master_manif
	// Function transf

	// constructor

	inline Segment ( Manifold m ) : FiniteElement::WithMaster ( m )
	#ifndef NDEBUG   // DEBUG mode
	{	this->info_string = "FiniteElement::WithMaster::Segment\n";
		this->info_string += "(slow) symbolic computations coupled with Gauss quadrature\n";  }
	#else  // NDEBUG
	{	}
	#endif
	
	// two methods  dock_on  virtual from FiniteElement::Core
	void dock_on ( const Cell & cll );
	void dock_on ( const Cell & cll, const tag::Winding & );

	// two methods  dock_on  only significant for
	// FiniteElement::StandAlone::TypeOne::{Rectangle,Square}
	// virtual from FiniteElement::Core, execution forbidden
	// void dock_on ( const Cell & cll, const tag::FirstVertex &, const Cell & )
	// void dock_on ( const Cell & cll, const tag::FirstVertex &, const Cell &,
	//                const tag::Winding &                                     )

	// 'pre_compute' and 'retrieve_precomputed' virtual from FiniteElement::Core,
	// defined by FiniteElement::WithMaster, execution forbidden

	Cell::Numbering & build_global_numbering ( );  // virtual from FiniteElement::Core
	
	// std::string info()  virtual from FiniteElement::Core
	// defined by FiniteElement::WithMaster (ifndef NDEBUG)

};  // end of  class FiniteElement::WithMaster::Segment

//------------------------------------------------------------------------------------------------------//


class FiniteElement::WithMaster::Triangle : public FiniteElement::WithMaster

// triangular finite elements which use a triangular master element

// abstract class, instantiated in FiniteElement::WithMaster::Triangle::P1
//       in FiniteElement::WithMaster::Triangle::P2::Straight
//       in FiniteElement::WithMaster::Triangle::P2::Straight::Incremental
//   and in FiniteElement::WithMaster::Triangle::P2::Curved

{	public :

	// attributes inherited from FiniteElement::Core :
	// Integrator integr
	// std::map < Cell::Core *, Function > base_fun_1
	// std::map < Cell::Core *, std::map < Cell::Core *, Function > > base_fun_2
	// Cell docked_on
	// std::string info_string (ifndef NDEBUG)

	// attributes inherited from FiniteElement::WithMaster :
	// Manifold master_manif
	// Function transf

	// constructor

	inline Triangle ( Manifold m ) : FiniteElement::WithMaster ( m )
	{	}
	
	// two methods  dock_on  stay pure virtual from FiniteElement::Core
	// void dock_on ( const Cell & cll )
	// void dock_on ( const Cell & cll, const tag::Winding & )

	// two methods  dock_on  only significant for
	// FiniteElement::StandAlone::TypeOne::{Rectangle,Square}
	// virtual from FiniteElement::Core, execution forbidden
	// void dock_on ( const Cell & cll, const tag::FirstVertex &, const Cell & )
	// void dock_on ( const Cell & cll, const tag::FirstVertex &, const Cell &,
	//                const tag::Winding &                                     )

	// 'pre_compute' and 'retrieve_precomputed' virtual from FiniteElement::Core,
	// defined by FiniteElement::WithMaster, execution forbidden

	// Cell::Numbering & build_global_numbering ( )  stays pure virtual from FiniteElement::Core
	
	// std::string info()  virtual from FiniteElement::Core
	// defined by FiniteElement::WithMaster (ifndef NDEBUG)

	class P1;  struct P2 { class Straight; class Curved; };

};  // end of  class FiniteElement::WithMaster::Triangle

//------------------------------------------------------------------------------------------------------//


class FiniteElement::WithMaster::Triangle::P1 : public FiniteElement::WithMaster::Triangle

// triangular finite elements which use a triangular master element, Lagrange P1
// three affine basis functions, associated to each of the three vertices

{	public :

	// attributes inherited from FiniteElement::Core :
	// Integrator integr
	// std::map < Cell::Core *, Function > base_fun_1
	// std::map < Cell::Core *, std::map < Cell::Core *, Function > > base_fun_2
	// Cell docked_on
	// std::string info_string (ifndef NDEBUG)

	// attributes inherited from FiniteElement::WithMaster :
	// Manifold master_manif
	// Function transf

	// constructor

	inline P1 ( Manifold m ) : FiniteElement::WithMaster::Triangle ( m )
	#ifndef NDEBUG  // DEBUG mode
	{	this->info_string = "FiniteElement::WithMaster::Triangle::P1\n";
		this->info_string += "(slow) symbolic computations coupled with Gauss quadrature\n";  }
	#else  // NDEBUG
	{	}
	#endif
	
	// two methods  dock_on  virtual from FiniteElement::Core
	void dock_on ( const Cell & cll );
	void dock_on ( const Cell & cll, const tag::Winding & );

	// two methods  dock_on  only significant for
	// FiniteElement::StandAlone::TypeOne::{Rectangle,Square}
	// virtual from FiniteElement::Core, execution forbidden
	// void dock_on ( const Cell & cll, const tag::FirstVertex &, const Cell & )
	// void dock_on ( const Cell & cll, const tag::FirstVertex &, const Cell &,
	//                const tag::Winding &                                     )

	// 'pre_compute' and 'retrieve_precomputed' virtual from FiniteElement::Core,
	// defined by FiniteElement::WithMaster, execution forbidden

	Cell::Numbering & build_global_numbering ( );  // virtual from FiniteElement::Core
	
	// std::string info()  virtual from FiniteElement::Core
	// defined by FiniteElement::WithMaster (ifndef NDEBUG)

};  // end of  class FiniteElement::WithMaster::Triangle::P1

//------------------------------------------------------------------------------------------------------//


class FiniteElement::WithMaster::Triangle::P2::Straight : public FiniteElement::WithMaster::Triangle

// triangular finite elements which use a triangular master element, Lagrange P2
// six quadratic basis functions, associated to each vertex and to each side

{	public :

	// attributes inherited from FiniteElement::Core :
	// Integrator integr
	// std::map < Cell::Core *, Function > base_fun_1
	// std::map < Cell::Core *, std::map < Cell::Core *, Function > > base_fun_2
	// Cell docked_on
	// std::string info_string (ifndef NDEBUG)

	// attributes inherited from FiniteElement::WithMaster :
	// Manifold master_manif
	// Function transf

	// constructor

	inline Straight ( Manifold m ) : FiniteElement::WithMaster::Triangle ( m )
	#ifndef NDEBUG  // DEBUG mode
	{	this->info_string = "FiniteElement::WithMaster::Triangle::P2::Straight\n";
		this->info_string += "(slow) symbolic computations coupled with Gauss quadrature\n";  }
	#else  // NDEBUG
	{	}
	#endif
	
	// two methods  dock_on  virtual from FiniteElement::Core
	void dock_on ( const Cell & cll );
	void dock_on ( const Cell & cll, const tag::Winding & );

	// two methods  dock_on  only significant for
	// FiniteElement::StandAlone::TypeOne::{Rectangle,Square}
	// virtual from FiniteElement::Core, execution forbidden
	// void dock_on ( const Cell & cll, const tag::FirstVertex &, const Cell & )
	// void dock_on ( const Cell & cll, const tag::FirstVertex &, const Cell &,
	//                const tag::Winding &                                     )

	// 'pre_compute' and 'retrieve_precomputed' virtual from FiniteElement::Core,
	// defined by FiniteElement::WithMaster, execution forbidden

	Cell::Numbering & build_global_numbering ( );  // virtual from FiniteElement::Core
	
	// std::string info()  virtual from FiniteElement::Core
	// defined by FiniteElement::WithMaster (ifndef NDEBUG)

	class Incremental;
	
};  // end of  class FiniteElement::WithMaster::Triangle::P2::Straight

//------------------------------------------------------------------------------------------------------//


class FiniteElement::WithMaster::Triangle::P2::Straight::Incremental
: public FiniteElement::WithMaster::Triangle

// triangular finite elements which use a triangular master element, Lagrange P2
// three affine basis functions, associated to each vertex (the same as for P1)
// plus three quadratic basis functions, associated to each side

{	public :

	// attributes inherited from FiniteElement::Core :
	// Integrator integr
	// std::map < Cell::Core *, Function > base_fun_1
	// std::map < Cell::Core *, std::map < Cell::Core *, Function > > base_fun_2
	// Cell docked_on
	// std::string info_string (ifndef NDEBUG)

	// attributes inherited from FiniteElement::WithMaster :
	// Manifold master_manif
	// Function transf

	// constructor

	inline Incremental ( Manifold m ) : FiniteElement::WithMaster::Triangle ( m )
	#ifndef NDEBUG  // DEBUG mode
	{	this->info_string = "FiniteElement::WithMaster::Triangle::P2::Straight::Incremental\n";
		this->info_string += "(slow) symbolic computations coupled with Gauss quadrature\n";  }
	#else  // NDEBUG
	{	}
	#endif
	
	// two methods  dock_on  virtual from FiniteElement::Core
	void dock_on ( const Cell & cll );
	void dock_on ( const Cell & cll, const tag::Winding & );

	// two methods  dock_on  only significant for
	// FiniteElement::StandAlone::TypeOne::{Rectangle,Square}
	// virtual from FiniteElement::Core, execution forbidden
	// void dock_on ( const Cell & cll, const tag::FirstVertex &, const Cell & )
	// void dock_on ( const Cell & cll, const tag::FirstVertex &, const Cell &,
	//                const tag::Winding &                                     )

	// 'pre_compute' and 'retrieve_precomputed' virtual from FiniteElement::Core,
	// defined by FiniteElement::WithMaster, execution forbidden

	Cell::Numbering & build_global_numbering ( );  // virtual from FiniteElement::Core
	
	// std::string info()  virtual from FiniteElement::Core
	// defined by FiniteElement::WithMaster (ifndef NDEBUG)

};  // end of  class FiniteElement::WithMaster::Triangle::P2::Straight::Incremental

//------------------------------------------------------------------------------------------------------//


class FiniteElement::WithMaster::Triangle::P2::Curved : public FiniteElement::WithMaster::Triangle

// triangular finite elements which use a triangular master element, curved sides

{	public :

	// attributes inherited from FiniteElement::Core :
	// Integrator integr
	// std::map < Cell::Core *, Function > base_fun_1
	// std::map < Cell::Core *, std::map < Cell::Core *, Function > > base_fun_2
	// Cell docked_on
	// std::string info_string (ifndef NDEBUG)

	// attributes inherited from FiniteElement::WithMaster :
	// Manifold master_manif
	// Function transf

	// constructor

	inline Curved ( Manifold m ) : FiniteElement::WithMaster::Triangle ( m )
	#ifndef NDEBUG  // DEBUG mode
	{	this->info_string = "FiniteElement::WithMaster::Triangle::P2::Curved\n";
		this->info_string += "(slow) symbolic computations coupled with Gauss quadrature\n";  }
	#else  // NDEBUG
	{	}
	#endif
	
	// two methods  dock_on  virtual from FiniteElement::Core
	void dock_on ( const Cell & cll );
	void dock_on ( const Cell & cll, const tag::Winding & );

	// two methods  dock_on  only significant for
	// FiniteElement::StandAlone::TypeOne::{Rectangle,Square}
	// virtual from FiniteElement::Core, execution forbidden
	// void dock_on ( const Cell & cll, const tag::FirstVertex &, const Cell & )
	// void dock_on ( const Cell & cll, const tag::FirstVertex &, const Cell &,
	//                const tag::Winding &                                     )

	// 'pre_compute' and 'retrieve_precomputed' virtual from FiniteElement::Core,
	// defined by FiniteElement::WithMaster, execution forbidden

	Cell::Numbering & build_global_numbering ( );  // virtual from FiniteElement::Core
	
	// std::string info()  virtual from FiniteElement::Core
	// defined by FiniteElement::WithMaster (ifndef NDEBUG)

};  // end of  class FiniteElement::WithMaster::Triangle::P2::Curved

//------------------------------------------------------------------------------------------------------//


class FiniteElement::WithMaster::Quadrangle : public FiniteElement::WithMaster

// quadrangular finite elements which use a square master element

// abstract class, instantiated in FiniteElement::WithMaster::Quadrangle::P1
//       in FiniteElement::WithMaster::Quadrangle::P2::Straight
//       in FiniteElement::WithMaster::Quadrangle::P2::Straight::Incremental
//   and in FiniteElement::WithMaster::Quadrangle::P2::Curved

{	public :

	// attributes inherited from FiniteElement::Core :
	// Integrator integr
	// std::map < Cell::Core *, Function > base_fun_1
	// std::map < Cell::Core *, std::map < Cell::Core *, Function > > base_fun_2
	// Cell docked_on
	// std::string info_string (ifndef NDEBUG)

	// attributes inherited from FiniteElement::WithMaster :
	// Manifold master_manif
	// Function transf

	// constructor

	inline Quadrangle ( Manifold m ) : FiniteElement::WithMaster ( m )
	{	}
	
	// two methods  dock_on  stay pure virtual from FiniteElement::Core
	// void dock_on ( const Cell & cll )
	// void dock_on ( const Cell & cll, const tag::Winding & )

	// two methods  dock_on  only significant for
	// FiniteElement::StandAlone::TypeOne::{Rectangle,Square}
	// virtual from FiniteElement::Core, execution forbidden
	// void dock_on ( const Cell & cll, const tag::FirstVertex &, const Cell & )
	// void dock_on ( const Cell & cll, const tag::FirstVertex &, const Cell &,
	//                const tag::Winding &                                     )

	// 'pre_compute' and 'retrieve_precomputed' virtual from FiniteElement::Core,
	// defined by FiniteElement::WithMaster, execution forbidden

	// Cell::Numbering & build_global_numbering ( )  stays pure virtual from FiniteElement::Core
	
	// std::string info()  virtual from FiniteElement::Core
	// defined by FiniteElement::WithMaster (ifndef NDEBUG)

	class Q1;  struct Q2 { class Straight; class Curved; };

};  // end of  class FiniteElement::WithMaster::Quadrangle

//------------------------------------------------------------------------------------------------------//


class FiniteElement::WithMaster::Quadrangle::Q1 : public FiniteElement::WithMaster::Quadrangle

// quadrangular finite elements which use a square master element
// four bi-linear (bi-affine) basis functions, associated to vertices

{	public :

	// attributes inherited from FiniteElement::Core :
	// Integrator integr
	// std::map < Cell::Core *, Function > base_fun_1
	// std::map < Cell::Core *, std::map < Cell::Core *, Function > > base_fun_2
	// Cell docked_on
	// std::string info_string (ifndef NDEBUG)

	// attributes inherited from FiniteElement::WithMaster :
	// Manifold master_manif
	// Function transf

	// constructor

	inline Q1 ( Manifold m ) : FiniteElement::WithMaster::Quadrangle ( m )
	#ifndef NDEBUG  // DEBUG mode
	{	this->info_string = "FiniteElement::WithMaster::Quadrangle::Q1\n";
		this->info_string += "(slow) symbolic computations coupled with Gauss quadrature\n";  }
	#else  // NDEBUG
	{	}
	#endif
	
	// two methods  dock_on  virtual from FiniteElement::Core
	void dock_on ( const Cell & cll );
	void dock_on ( const Cell & cll, const tag::Winding & );

	// two methods  dock_on  only significant for
	// FiniteElement::StandAlone::TypeOne::{Rectangle,Square}
	// virtual from FiniteElement::Core, execution forbidden
	// void dock_on ( const Cell & cll, const tag::FirstVertex &, const Cell & )
	// void dock_on ( const Cell & cll, const tag::FirstVertex &, const Cell &,
	//                const tag::Winding &                                     )

	// 'pre_compute' and 'retrieve_precomputed' virtual from FiniteElement::Core,
	// defined by FiniteElement::WithMaster, execution forbidden

	Cell::Numbering & build_global_numbering ( );  // virtual from FiniteElement::Core
	
	// std::string info()  virtual from FiniteElement::Core
	// defined by FiniteElement::WithMaster (ifndef NDEBUG)

};  // end of  class FiniteElement::WithMaster::Quadrangle::Q1

//------------------------------------------------------------------------------------------------------//


class FiniteElement::WithMaster::Quadrangle::Q2::Straight
: public FiniteElement::WithMaster::Quadrangle

// quadrangular finite elements which use a square master element, Lagrange Q2
// nine bi-quadratic basis functions, associated to each vertex,
// to the center of each side and to the baricenter of the square

{	public :

	// attributes inherited from FiniteElement::Core :
	// Integrator integr
	// std::map < Cell::Core *, Function > base_fun_1
	// std::map < Cell::Core *, std::map < Cell::Core *, Function > > base_fun_2
	// Cell docked_on
	// std::string info_string (ifndef NDEBUG)

	// attributes inherited from FiniteElement::WithMaster :
	// Manifold master_manif
	// Function transf

	// constructor

	inline Straight ( Manifold m ) : FiniteElement::WithMaster::Quadrangle ( m )
	#ifndef NDEBUG  // DEBUG mode
	{	this->info_string = "FiniteElement::WithMaster::Quadrangle::Q2::Straight\n";
		this->info_string += "(slow) symbolic computations coupled with Gauss quadrature\n";  }
	#else  // NDEBUG
	{	}
	#endif
	
	// two methods  dock_on  virtual from FiniteElement::Core
	void dock_on ( const Cell & cll );
	void dock_on ( const Cell & cll, const tag::Winding & );

	// two methods  dock_on  only significant for
	// FiniteElement::StandAlone::TypeOne::{Rectangle,Square}
	// virtual from FiniteElement::Core, execution forbidden
	// void dock_on ( const Cell & cll, const tag::FirstVertex &, const Cell & )
	// void dock_on ( const Cell & cll, const tag::FirstVertex &, const Cell &,
	//                const tag::Winding &                                     )

	// 'pre_compute' and 'retrieve_precomputed' virtual from FiniteElement::Core,
	// defined by FiniteElement::WithMaster, execution forbidden

	Cell::Numbering & build_global_numbering ( );  // virtual from FiniteElement::Core
	
	// std::string info()  virtual from FiniteElement::Core
	// defined by FiniteElement::WithMaster (ifndef NDEBUG)

	class Incremental;
	
};  // end of  class FiniteElement::WithMaster::Quadrangle::Q2::Straight

//------------------------------------------------------------------------------------------------------//


class FiniteElement::WithMaster::Quadrangle::Q2::Straight::Incremental
: public FiniteElement::WithMaster::Quadrangle

// quadrangular finite elements which use a square master element, Lagrange Q2
// four bi-linear basis functions, associated to each vertex (the same as for Q1)
// plus four bi-quadratic basis functions, associated to each side
// plus a bi-quadratic function associated to the baricenter of the square

{	public :

	// attributes inherited from FiniteElement::Core :
	// Integrator integr
	// std::map < Cell::Core *, Function > base_fun_1
	// std::map < Cell::Core *, std::map < Cell::Core *, Function > > base_fun_2
	// Cell docked_on
	// std::string info_string (ifndef NDEBUG)

	// attributes inherited from FiniteElement::WithMaster :
	// Manifold master_manif
	// Function transf

	// constructor

	inline Incremental ( Manifold m ) : FiniteElement::WithMaster::Quadrangle ( m )
	#ifndef NDEBUG  // DEBUG mode
	{	this->info_string = "FiniteElement::WithMaster::Quadrangle::Q2::Straight::Incremental\n";
		this->info_string += "(slow) symbolic computations coupled with Gauss quadrature\n";      }
	#else  // NDEBUG
	{	}
	#endif
	
	// two methods  dock_on  virtual from FiniteElement::Core
	void dock_on ( const Cell & cll );
	void dock_on ( const Cell & cll, const tag::Winding & );

	// two methods  dock_on  only significant for
	// FiniteElement::StandAlone::TypeOne::{Rectangle,Square}
	// virtual from FiniteElement::Core, execution forbidden
	// void dock_on ( const Cell & cll, const tag::FirstVertex &, const Cell & )
	// void dock_on ( const Cell & cll, const tag::FirstVertex &, const Cell &,
	//                const tag::Winding &                                     )

	// 'pre_compute' and 'retrieve_precomputed' virtual from FiniteElement::Core,
	// defined by FiniteElement::WithMaster, execution forbidden

	Cell::Numbering & build_global_numbering ( );  // virtual from FiniteElement::Core
	
	// std::string info()  virtual from FiniteElement::Core
	// defined by FiniteElement::WithMaster (ifndef NDEBUG)

};  // end of  class FiniteElement::WithMaster::Quadrangle::Q2::Straight::Incremental

//------------------------------------------------------------------------------------------------------//


class FiniteElement::WithMaster::Quadrangle::Q2::Curved
: public FiniteElement::WithMaster::Quadrangle

// quadrangular finite elements which use a square master element, curved sides

{	public :

	// attributes inherited from FiniteElement::Core :
	// Integrator integr
	// std::map < Cell::Core *, Function > base_fun_1
	// std::map < Cell::Core *, std::map < Cell::Core *, Function > > base_fun_2
	// Cell docked_on
	// std::string info_string (ifndef NDEBUG)

	// attributes inherited from FiniteElement::WithMaster :
	// Manifold master_manif
	// Function transf

	// constructor

	inline Curved ( Manifold m ) : FiniteElement::WithMaster::Quadrangle ( m )
	#ifndef NDEBUG  // DEBUG mode
	{	this->info_string = "FiniteElement::WithMaster::Quadrangle::Q2::Curved\n";
		this->info_string += "(slow) symbolic computations coupled with Gauss quadrature\n";  }
	#else  // NDEBUG
	{	}
	#endif
	
	// two methods  dock_on  virtual from FiniteElement::Core
	void dock_on ( const Cell & cll );
	void dock_on ( const Cell & cll, const tag::Winding & );

	// two methods  dock_on  only significant for
	// FiniteElement::StandAlone::TypeOne::{Rectangle,Square}
	// virtual from FiniteElement::Core, execution forbidden
	// void dock_on ( const Cell & cll, const tag::FirstVertex &, const Cell & )
	// void dock_on ( const Cell & cll, const tag::FirstVertex &, const Cell &,
	//                const tag::Winding &                                     )

	// 'pre_compute' and 'retrieve_precomputed' virtual from FiniteElement::Core,
	// defined by FiniteElement::WithMaster, execution forbidden

	Cell::Numbering & build_global_numbering ( );  // virtual from FiniteElement::Core
	
	// std::string info()  virtual from FiniteElement::Core
	// defined by FiniteElement::WithMaster (ifndef NDEBUG)

};  // end of  class FiniteElement::WithMaster::Quadrangle::Q2::Curved

//------------------------------------------------------------------------------------------------------//


class FiniteElement::WithMaster::Tetrahedron : public FiniteElement::WithMaster

// tetrahedron finite elements which use a tetrahedron master element

// abstract class, instantiated in FiniteElement::WithMaster::Tetrahedron::P1

{	public :

	// attributes inherited from FiniteElement::Core :
	// Integrator integr
	// std::map < Cell::Core *, Function > base_fun_1
	// std::map < Cell::Core *, std::map < Cell::Core *, Function > > base_fun_2
	// Cell docked_on
	// std::string info_string (ifndef NDEBUG)

	// attributes inherited from FiniteElement::WithMaster :
	// Manifold master_manif
	// Function transf

	// constructor

	inline Tetrahedron ( Manifold m ) : FiniteElement::WithMaster ( m )
	{	}
	
	// two methods  dock_on  stay pure virtual from FiniteElement::Core
	// void dock_on ( const Cell & cll )
	// void dock_on ( const Cell & cll, const tag::Winding & )

	// two methods  dock_on  only significant for
	// FiniteElement::StandAlone::TypeOne::{Rectangle,Square}
	// virtual from FiniteElement::Core, execution forbidden
	// void dock_on ( const Cell & cll, const tag::FirstVertex &, const Cell & )
	// void dock_on ( const Cell & cll, const tag::FirstVertex &, const Cell &,
	//                const tag::Winding &                                     )

	// 'pre_compute' and 'retrieve_precomputed' virtual from FiniteElement::Core,
	// defined by FiniteElement::WithMaster, execution forbidden

	// Cell::Numbering & build_global_numbering ( )  stays pure virtual from FiniteElement::Core
	
	// std::string info()  virtual from FiniteElement::Core
	// defined by FiniteElement::WithMaster (ifndef NDEBUG)

	class P1;  struct P2 { class Straight; class Curved; };

};  // end of  class FiniteElement::WithMaster::Tetrahedron

//------------------------------------------------------------------------------------------------------//

  
class FiniteElement::WithMaster::Tetrahedron::P1 : public FiniteElement::WithMaster::Tetrahedron

// tetrahedron finite elements which use a tetrahedron master element, Lagrange P1
// four affine basis functions, associated to each of the four vertices

{	public :

	// attributes inherited from FiniteElement::Core :
	// Integrator integr
	// std::map < Cell::Core *, Function > base_fun_1
	// std::map < Cell::Core *, std::map < Cell::Core *, Function > > base_fun_2
	// Cell docked_on
	// std::string info_string (ifndef NDEBUG)

	// attributes inherited from FiniteElement::WithMaster :
	// Manifold master_manif
	// Function transf

	// constructor

	inline P1 ( Manifold m ) : FiniteElement::WithMaster::Tetrahedron ( m )
	#ifndef NDEBUG  // DEBUG mode
	{	this->info_string = "FiniteElement::WithMaster::Tetrahedron::P1\n";
		this->info_string += "(slow) symbolic computations coupled with Gauss quadrature\n";  }
	#else  // NDEBUG
	{	}
	#endif
	
	// two methods  dock_on  virtual from FiniteElement::Core
	void dock_on ( const Cell & cll );
	void dock_on ( const Cell & cll, const tag::Winding & );

	// two methods  dock_on  only significant for
	// FiniteElement::StandAlone::TypeOne::{Rectangle,Square}
	// virtual from FiniteElement::Core, execution forbidden
	// void dock_on ( const Cell & cll, const tag::FirstVertex &, const Cell & )
	// void dock_on ( const Cell & cll, const tag::FirstVertex &, const Cell &,
	//                const tag::Winding &                                     )

	// 'pre_compute' and 'retrieve_precomputed' virtual from FiniteElement::Core,
	// defined by FiniteElement::WithMaster, execution forbidden

	Cell::Numbering & build_global_numbering ( );  // virtual from FiniteElement::Core
	
	// std::string info()  virtual from FiniteElement::Core
	// defined by FiniteElement::WithMaster (ifndef NDEBUG)

};  // end of  class FiniteElement::WithMaster::Tetrahedron::P1

//------------------------------------------------------------------------------------------------------//


class FiniteElement::WithMaster::Hexahedron : public FiniteElement::WithMaster

// hexahedron finite elements which use a hexahedron master element

// abstract class, instantiated in FiniteElement::WithMaster::Hexahedron::P1

{	public :

	// attributes inherited from FiniteElement::Core :
	// Integrator integr
	// std::map < Cell::Core *, Function > base_fun_1
	// std::map < Cell::Core *, std::map < Cell::Core *, Function > > base_fun_2
	// Cell docked_on
	// std::string info_string (ifndef NDEBUG)

	// attributes inherited from FiniteElement::WithMaster :
	// Manifold master_manif
	// Function transf

	// constructor

	inline Hexahedron ( Manifold m ) : FiniteElement::WithMaster ( m )
	{	}
	
	// two methods  dock_on  stay pure virtual from FiniteElement::Core
	// void dock_on ( const Cell & cll )
	// void dock_on ( const Cell & cll, const tag::Winding & )

	// two methods  dock_on  only significant for
	// FiniteElement::StandAlone::TypeOne::{Rectangle,Square}
	// virtual from FiniteElement::Core, execution forbidden
	// void dock_on ( const Cell & cll, const tag::FirstVertex &, const Cell & )
	// void dock_on ( const Cell & cll, const tag::FirstVertex &, const Cell &,
	//                const tag::Winding &                                     )

	// 'pre_compute' and 'retrieve_precomputed' virtual from FiniteElement::Core,
	// defined by FiniteElement::WithMaster, execution forbidden

	// Cell::Numbering & build_global_numbering ( )  stays pure virtual from FiniteElement::Core
	
	// std::string info()  virtual from FiniteElement::Core
	// defined by FiniteElement::WithMaster (ifndef NDEBUG)

	class Q1;  struct Q2 { class Straight; class Curved; };

};  // end of  class FiniteElement::WithMaster::Hexahedron

//------------------------------------------------------------------------------------------------------//

  
class FiniteElement::WithMaster::Hexahedron::Q1 : public FiniteElement::WithMaster::Hexahedron

// hexahedron finite elements which use a hexahedron master element, Lagrange Q1
// eight affine basis functions, associated to each of the eight vertices

{	public :

	// attributes inherited from FiniteElement::Core :
	// Integrator integr
	// std::map < Cell::Core *, Function > base_fun_1
	// std::map < Cell::Core *, std::map < Cell::Core *, Function > > base_fun_2
	// Cell docked_on
	// std::string info_string (ifndef NDEBUG)

	// attributes inherited from FiniteElement::WithMaster :
	// Manifold master_manif
	// Function transf

	// constructor

	inline Q1 ( Manifold m ) : FiniteElement::WithMaster::Hexahedron ( m )
	#ifndef NDEBUG  // DEBUG mode
	{	this->info_string = "FiniteElement::WithMaster::Hexahedron::Q1\n";
		this->info_string += "(slow) symbolic computations coupled with Gauss quadrature\n";  }
	#else  // NDEBUG
	{	}
	#endif
	
	// two methods  dock_on  virtual from FiniteElement::Core
	void dock_on ( const Cell & cll );
	void dock_on ( const Cell & cll, const tag::Winding & );

	// two methods  dock_on  only significant for
	// FiniteElement::StandAlone::TypeOne::{Rectangle,Square}
	// virtual from FiniteElement::Core, execution forbidden
	// void dock_on ( const Cell & cll, const tag::FirstVertex &, const Cell & )
	// void dock_on ( const Cell & cll, const tag::FirstVertex &, const Cell &,
	//                const tag::Winding &                                     )

	// 'pre_compute' and 'retrieve_precomputed' virtual from FiniteElement::Core,
	// defined by FiniteElement::WithMaster, execution forbidden

	Cell::Numbering & build_global_numbering ( );  // virtual from FiniteElement::Core
	
	// std::string info()  virtual from FiniteElement::Core
	// defined by FiniteElement::WithMaster (ifndef NDEBUG)

};  // end of  class FiniteElement::WithMaster::Hexahedron::Q1

//------------------------------------------------------------------------------------------------------//


Integrator::Integrator ( const tag::gauss &, const tag::gauss_quadrature & q )
	
:	Integrator ( tag::non_existent )

{	FiniteElement fe ( tag::non_existent );

	// we guess from 'q' the type of finite element

	switch ( q )
	{	case tag::seg_2 :
		case tag::seg_3 :
		case tag::seg_4 :
		case tag::seg_5 :
		case tag::seg_6 :
			fe = FiniteElement ( tag::with_master, tag::segment, tag::Lagrange, tag::of_degree, 1 );
			break;
		case tag::tri_3 :
		case tag::tri_3_Oden :
		case tag::tri_4 :
		case tag::tri_4_Oden :
		case tag::tri_6 :
			fe = FiniteElement ( tag::with_master, tag::triangle, tag::Lagrange, tag::of_degree, 1 );
			break;
		case tag::quad_4 :
		case tag::quad_9 :
			fe = FiniteElement ( tag::with_master, tag::quadrangle, tag::Lagrange, tag::of_degree, 1 );
			break;
		default : assert ( false );                                                                    }

	this->core = new Integrator::Gauss ( q, tag::from_finite_element_with_master, fe );
	assert ( this->core->counter == 0 );
	
	fe .core->integr = *this;
	assert ( this->core->counter == 1 );
	fe .core->integr .weak = true;                                                                       }

// if 'fe .core->integr' weren't weak, 'this->core->counter' should be 2


Integrator::Integrator ( const tag::gauss &, const tag::gauss_quadrature & q,
                         const tag::FromFiniteElementWithMaster &, FiniteElement & fe )

// this constructor is called from a FiniteElement, through method set_integrator	
	
:	core { new Integrator::Gauss ( q, tag::from_finite_element_with_master, fe ) }, weak { false }

{	assert ( this->core->counter == 0 );
	this->core->counter = 1;             }

//------------------------------------------------------------------------------------------------------//


FiniteElement::FiniteElement ( const tag::WithMaster &, const tag::Segment &,
                               const tag::lagrange &, const tag::OfDegree &, size_t deg )
	
:	core { nullptr }, weak { false }

{	assert ( deg == 1 );

	// we keep the working manifold and restore it at the end
	Manifold work_manif = Manifold::working;
	
	Manifold RR_master ( tag::Euclid, tag::of_dim, 1 );
	Function t = RR_master.build_coordinate_system ( tag::Lagrange, tag::of_degree, 1 );
	// we should take advantage of the memory space already reserved for x and y

	this->core = new FiniteElement::WithMaster::Segment ( RR_master );
	assert ( this->core->counter == 0 );
	this->core->counter = 1;
	
	work_manif.set_as_working_manifold();                                                 }


FiniteElement::FiniteElement ( const tag::WithMaster &, const tag::Triangle &,
                               const tag::lagrange &, const tag::OfDegree &, size_t deg )
	
:	core { nullptr }, weak { false }

{	assert ( deg == 1 );

	// we keep the working manifold and restore it at the end
	Manifold work_manif = Manifold::working;
	
	Manifold RR2_master ( tag::Euclid, tag::of_dim, 2 );
	Function xi_eta = RR2_master .build_coordinate_system ( tag::Lagrange, tag::of_degree, 1 );
	// we should take advantage of the memory space already reserved for x and y
	// Function xi = xi_eta [0], eta = xi_eta [1];

	this->core = new FiniteElement::WithMaster::Triangle::P1 ( RR2_master );
	assert ( this->core->counter == 0 );
	this->core->counter = 1;
	
	work_manif .set_as_working_manifold();                                                       }


FiniteElement::FiniteElement ( const tag::WithMaster &, const tag::Triangle &,
  const tag::lagrange &, const tag::OfDegree &, size_t deg, const tag::Straight & )
	
:	core { nullptr }, weak { false }

{	assert ( deg == 2 );

	// we keep the working manifold and restore it at the end
	Manifold work_manif = Manifold::working;
	
	Manifold RR2_master ( tag::Euclid, tag::of_dim, 2 );
	Function xi_eta = RR2_master .build_coordinate_system ( tag::Lagrange, tag::of_degree, 1 );
	// we should take advantage of the memory space already reserved for x and y
	// Function xi = xi_eta [0], eta = xi_eta [1];

	this->core = new FiniteElement::WithMaster::Triangle::P2::Straight ( RR2_master );
	assert ( this->core->counter == 0 );
	this->core->counter = 1;
	
	work_manif .set_as_working_manifold();                                                       }


FiniteElement::FiniteElement ( const tag::WithMaster &, const tag::Triangle &,
                               const tag::lagrange &, const tag::OfDegree &, size_t deg,
                               const tag::Straight &, const tag::IncrementalBasis & )
	
:	core { nullptr }, weak { false }

{	assert ( deg == 2 );

	// we keep the working manifold and restore it at the end
	Manifold work_manif = Manifold::working;
	
	Manifold RR2_master ( tag::Euclid, tag::of_dim, 2 );
	Function xi_eta = RR2_master .build_coordinate_system ( tag::Lagrange, tag::of_degree, 1 );
	// we should take advantage of the memory space already reserved for x and y
	// Function xi = xi_eta [0], eta = xi_eta [1];

	this->core = new FiniteElement::WithMaster::Triangle::P2::Straight::Incremental ( RR2_master );
	assert ( this->core->counter == 0 );
	this->core->counter = 1;
	
	work_manif .set_as_working_manifold();                                                           }


FiniteElement::FiniteElement ( const tag::WithMaster &, const tag::Triangle &,
  const tag::lagrange &, const tag::OfDegree &, size_t deg, const tag::Curved & )
	
:	core { nullptr }, weak { false }

{	assert ( deg == 2 );
	assert ( false );

	// we keep the working manifold and restore it at the end
	Manifold work_manif = Manifold::working;
	
	Manifold RR2_master ( tag::Euclid, tag::of_dim, 2 );
	Function xi_eta = RR2_master .build_coordinate_system ( tag::Lagrange, tag::of_degree, 1 );
	// we should take advantage of the memory space already reserved for x and y
	// Function xi = xi_eta [0], eta = xi_eta [1];

	this->core = new FiniteElement::WithMaster::Triangle::P2::Curved ( RR2_master );
	assert ( this->core->counter == 0 );
	this->core->counter = 1;
	
	work_manif .set_as_working_manifold();                                                       }


FiniteElement::FiniteElement ( const tag::WithMaster &, const tag::Quadrangle &,
                               const tag::lagrange &, const tag::OfDegree &, size_t deg )
	
:	core { nullptr }, weak { false }

{	assert ( deg == 1 );

	// we keep the working manifold and restore it at the end
	Manifold work_manif = Manifold::working;
	
	Manifold RR2_master ( tag::Euclid, tag::of_dim, 2 );
	Function xi_eta = RR2_master .build_coordinate_system ( tag::Lagrange, tag::of_degree, 1 );
	// we should take advantage of the memory space already reserved for x and y
	// Function xi = xi_eta [0], eta = xi_eta [1];

	this->core = new FiniteElement::WithMaster::Quadrangle::Q1 ( RR2_master );
	assert ( this->core->counter == 0 );
	this->core->counter = 1;
	
	work_manif .set_as_working_manifold();                                                       }


FiniteElement::FiniteElement ( const tag::WithMaster &, const tag::Quadrangle &,
	const tag::lagrange &, const tag::OfDegree &, size_t deg, const tag::Straight & )
	
:	core { nullptr }, weak { false }

{	assert ( deg == 2 );

	// we keep the working manifold and restore it at the end
	Manifold work_manif = Manifold::working;
	
	Manifold RR2_master ( tag::Euclid, tag::of_dim, 2 );
	Function xi_eta = RR2_master .build_coordinate_system ( tag::Lagrange, tag::of_degree, 1 );
	// we should take advantage of the memory space already reserved for x and y
	// Function xi = xi_eta [0], eta = xi_eta [1];

	this->core = new FiniteElement::WithMaster::Quadrangle::Q2::Straight ( RR2_master );
	assert ( this->core->counter == 0 );
	this->core->counter = 1;
	
	work_manif .set_as_working_manifold();                                                       }


FiniteElement::FiniteElement ( const tag::WithMaster &, const tag::Quadrangle &,
                               const tag::lagrange &, const tag::OfDegree &, size_t deg,
                               const tag::Straight &, const tag::IncrementalBasis &     )
	
:	core { nullptr }, weak { false }

{	assert ( deg == 2 );

	// we keep the working manifold and restore it at the end
	Manifold work_manif = Manifold::working;
	
	Manifold RR2_master ( tag::Euclid, tag::of_dim, 2 );
	Function xi_eta = RR2_master .build_coordinate_system ( tag::Lagrange, tag::of_degree, 1 );
	// we should take advantage of the memory space already reserved for x and y
	// Function xi = xi_eta [0], eta = xi_eta [1];

	this->core = new FiniteElement::WithMaster::Quadrangle::Q2::Straight::Incremental ( RR2_master );
	assert ( this->core->counter == 0 );
	this->core->counter = 1;
	
	work_manif .set_as_working_manifold();                                                             }


FiniteElement::FiniteElement ( const tag::WithMaster &, const tag::Quadrangle &,
	const tag::lagrange &, const tag::OfDegree &, size_t deg, const tag::Curved & )
	
:	core { nullptr }, weak { false }

{	assert ( deg == 2 );
	assert ( false );

	// we keep the working manifold and restore it at the end
	Manifold work_manif = Manifold::working;
	
	Manifold RR2_master ( tag::Euclid, tag::of_dim, 2 );
	Function xi_eta = RR2_master .build_coordinate_system ( tag::Lagrange, tag::of_degree, 1 );
	// we should take advantage of the memory space already reserved for x and y
	// Function xi = xi_eta [0], eta = xi_eta [1];

	this->core = new FiniteElement::WithMaster::Quadrangle::Q2::Curved ( RR2_master );
	assert ( this->core->counter == 0 );
	this->core->counter = 1;
	
	work_manif .set_as_working_manifold();                                                       }
	

FiniteElement::FiniteElement ( const tag::WithMaster &, const tag::Tetrahedron &,
                               const tag::lagrange &, const tag::OfDegree &, size_t deg )
	
:	core { nullptr }, weak { false }

{	assert ( deg == 1 );

	// we keep the working manifold and restore it at the end
	Manifold work_manif = Manifold::working;
	
	Manifold RR3_master ( tag::Euclid, tag::of_dim, 3 );
	Function xi_eta_zeta = RR3_master .build_coordinate_system ( tag::Lagrange, tag::of_degree, 1 );
	// we should take advantage of the memory space already reserved for x, y and z
	// Function xi = xi_eta_zeta [0], eta = xi_eta_zeta [1], zeta = xi_eta_zeta [2];

	this->core = new FiniteElement::WithMaster::Tetrahedron::P1 ( RR3_master );
	assert ( this->core->counter == 0 );
	this->core->counter = 1;
	
	work_manif .set_as_working_manifold();                                                            }


FiniteElement::FiniteElement ( const tag::WithMaster &, const tag::Hexahedron &,
                               const tag::lagrange &, const tag::OfDegree &, size_t deg )
	
:	core { nullptr }, weak { false }

{	assert ( deg == 1 );

	// we keep the working manifold and restore it at the end
	Manifold work_manif = Manifold::working;
	
	Manifold RR3_master ( tag::Euclid, tag::of_dim, 3 );
	Function xi_eta_zeta = RR3_master .build_coordinate_system ( tag::Lagrange, tag::of_degree, 1 );
	// we should take advantage of the memory space already reserved for x, y and z
	// Function xi = xi_eta_zeta [0], eta = xi_eta_zeta [1], zeta = xi_eta_zeta [2];

	this->core = new FiniteElement::WithMaster::Hexahedron::Q1 ( RR3_master );
	assert ( this->core->counter == 0 );
	this->core->counter = 1;
	
	work_manif .set_as_working_manifold();                                                            }


Integrator FiniteElement::set_integrator
( const tag::gauss &, const tag::gauss_quadrature & q )
	
{ FiniteElement::WithMaster * this_core = tag::Util::assert_cast
		< FiniteElement::Core *, FiniteElement::WithMaster * > ( this->core );
	Integrator::Gauss * integ =
		new Integrator::Gauss ( q, tag::from_finite_element_with_master, *this );
	this_core->integr .core = integ;
	assert ( integ->counter == 0 );
	integ->counter = 1;

	assert ( this_core->counter > 1 );
	this_core->counter --;
	integ->finite_element .weak = true;
	
	return this_core->integr;                                                     }

//------------------------------------------------------------------------------------------------------//


class FiniteElement::StandAlone : public FiniteElement::Core

// finite elements with no master
// abstract class, instantiated in FiniteElement::StandAlone::Type***::***

{	public :

	// attributes inherited from FiniteElement::Core :
	// Integrator integr
	// std::map < Cell::Core *, Function > base_fun_1
	// std::map < Cell::Core *, std::map < Cell::Core *, Function > > base_fun_2
	// Cell docked_on
	// std::string info_string (ifndef NDEBUG)

	// for HandCoded integrators, we study previously several cases
	// and hard-code the necessary expressions (hand-computed)
	size_t cas { 0 };

	// for HandCoded integrators, we re-arrange the pre-computed results
	// according to the specification of the user, we use a 'selector'
	std::vector < size_t > selector;
	
	std::map < Cell::Core *, size_t > local_numbering_1;
	std::map < Function::Core *, size_t > basis_numbering;

	// at doking, this finite element will perform all computations
	// (previously declared in 'pre_compute') and store the results in 'result_of_integr'
	// conceptually, what we want is
	// std::vector < std::vector < std::vector < double > > > result_of_integr
	// the first and second indices identify one or two basis functions
	// (with the aid of 'basis_numbering')
	// the third index simply identifies the required computation
	// however, for efficiency reasons, we prefer an array declared in each subclass

	const std::set < size_t > implemented_cases;

	// a multi-line string describing cases not implemented, skipped by 'pre_compute'
	#ifndef NDEBUG  // DEBUG mode
	std::string pre_compute_tries;
	#endif

	// constructor
	inline StandAlone ( const std::set < size_t > & ic )
	:	FiniteElement::Core (), implemented_cases { ic }
	{	}

	// two methods  dock_on  stay pure virtual from FiniteElement::Core
	// void dock_on ( const Cell & cll )
	// void dock_on ( const Cell & cll, const tag::Winding & )

	// two methods  dock_on  only significant for
	// FiniteElement::StandAlone::TypeOne::{Rectangle,Square}
	// virtual from FiniteElement::Core, execution forbidden, later overridden
	// void dock_on ( const Cell & cll, const tag::FirstVertex &, const Cell & )
	// void dock_on ( const Cell & cll, const tag::FirstVertex &, const Cell &,
	//                const tag::Winding &                                     )

	// 'pre_compute' and 'retrieve_precomputed' stay pure virtual from FiniteElement::Core

	// Cell::Numbering & build_global_numbering ( )  stays pure virtual from FiniteElement::Core
	
	// std::string info ( )  stays pure virtual from FiniteElement::Core (ifndef NDEBUG)

	static constexpr size_t index
	( const size_t psi_P, const size_t psi_Q, const size_t int_psi_psi,
	  const size_t base_d, const size_t c6                             );
	
	static size_t assert_index
	( const size_t psi_P, const size_t psi_Q, const size_t int_psi_psi,
	  const size_t base_d, const size_t c16, const size_t buffer_size  );
	
	class TypeOne;
	
};  // end of  class FiniteElement::StandAlone

//------------------------------------------------------------------------------------------------------//


class FiniteElement::StandAlone::TypeOne : public FiniteElement::StandAlone

// this class will probably be eliminated

// finite elements with no master
// searches for the basis function provided by 'integrate'
// uses  std::map < Function::Core *, size_t > basis_numbering

// abstract class, instantiated in FiniteElement::StandAlone::TypeOne::***

{	public :

	// attributes inherited from FiniteElement::Core :
	// Integrator integr
	// std::map < Cell::Core *, Function > base_fun_1
	// std::map < Cell::Core *, std::map < Cell::Core *, Function > > base_fun_2
	// Cell docked_on
	// std::string info_string (ifndef NDEBUG)

	// attributes inherited from FiniteElement::StandAlone :
	// size_t cas { 0 }
	// std::vector < size_t > selector
	// std::map < Cell::Core *, size_t > local_numbering_1
	// std::map < Function::Core *, size_t > basis_numbering
	// const std::set < size_t > implemented_cases
	// std::string pre_compute_tries  (ifndef NDEBUG)

	// dummy base functions, arguments of 'pre_compute' :
	std::vector < Function > dummy_bf;

	// constructor
	inline TypeOne ( const std::set < size_t > & ic )
	:	FiniteElement::StandAlone ( ic )
	{	}

	// two methods  dock_on  stay pure virtual from FiniteElement::Core
	// void dock_on ( const Cell & cll )
	// void dock_on ( const Cell & cll, const tag::Winding & )

	// two methods  dock_on  only significant for
	// FiniteElement::StandAlone::TypeOne::{Rectangle,Square}
	// virtual from FiniteElement::Core, execution forbidden, later overridden
	// void dock_on ( const Cell & cll, const tag::FirstVertex &, const Cell & )
	// void dock_on ( const Cell & cll, const tag::FirstVertex &, const Cell &,
	//                const tag::Winding &                                     )

	//  pre_compute  virtual from FiniteElement::Core
	void pre_compute ( const std::vector < Function > & bf,
                     const std::vector < Function > & result );

	// 'retrieve_precomputed' stays pure virtual from FiniteElement::Core

	// Cell::Numbering & build_global_numbering ( )  stays pure virtual from FiniteElement::Core
	
	// std::string info ( )  stays pure virtual from FiniteElement::Core (ifndef NDEBUG)

	class Segment;  class Triangle;  class Quadrangle;
	class Parallelogram;  class Rectangle;  class Square;
	class Tetrahedron;  
	
};  // end of  class FiniteElement::StandAlone::TypeOne

//------------------------------------------------------------------------------------------------------//


constexpr size_t FiniteElement::StandAlone::index  // static
( const size_t psi_P, const size_t psi_Q, const size_t int_psi_psi,
  const size_t base_d, const size_t c16                            )

{	return ( psi_P * base_d + psi_Q ) * c16 + int_psi_psi ;  }


size_t FiniteElement::StandAlone::assert_index  // static
( const size_t psi_P, const size_t psi_Q, const size_t int_psi_psi,
  const size_t base_d, const size_t c16, const size_t buffer_size  )

{	size_t i = FiniteElement::StandAlone::index ( psi_P, psi_Q, int_psi_psi, base_d, c16 );
	// std::cout << "line 1313, " << i << " " << buffer_size << std::endl;
	assert ( i < buffer_size );
	return i;                                                                                }

//------------------------------------------------------------------------------------------------------//


class FiniteElement::StandAlone::TypeOne::Triangle : public FiniteElement::StandAlone::TypeOne

// triangular finite elements, no master element

{	public :

	// attributes inherited from FiniteElement::Core :
	// Integrator integr
	// std::map < Cell::Core *, Function > base_fun_1
	// std::map < Cell::Core *, std::map < Cell::Core *, Function > > base_fun_2
	// Cell docked_on
	// std::string info_string (ifndef NDEBUG)

	// attributes inherited from FiniteElement::StandAlone :
	// size_t cas { 0 }
	// std::vector < size_t > selector
	// std::map < Cell::Core *, size_t > local_numbering_1
	// std::map < Function::Core *, size_t > basis_numbering
	// const std::set < size_t > implemented_cases
	// std::string pre_compute_tries  (ifndef NDEBUG)

	// attributes inherited from FiniteElement::StandAlone::TypeOne :
	// std::vector < Function > dummy_bf  dummy base functions, arguments of 'pre_compute'

	static constexpr size_t base_dim = 3;
	static constexpr size_t space_dim = 2;
	static constexpr size_t
		case_1 = 1,  // integral of psi
		case_2 = 1,  // integral of psi1 * psi2
		case_3 = space_dim,  // integral of d_psi_dx
		case_4 = space_dim * space_dim,  // integral of d_psi1_dx * d_psi2_dy
		case_5 = 1,  // integral of grad psi1 * grad psi2
		case_6 = space_dim,  // integral of psi1 * d_psi2_dx
		case_7 = case_1 + case_2, case_8 = case_1 + case_3,
		case_9 = case_1 + case_4, case_10 = case_1 + case_5,
		case_11 = case_3 + case_4, case_12 = case_2 + case_4,
		case_13 = case_2 + case_5, case_14 = case_7 + case_11,
		case_15 = case_14 + case_5, case_16 = case_15 + case_6;  // case_16 = 11

	// conceptually, 'result_of_integr' belongs to 'FiniteElement::StandAlone'
	// and should have been declared there
	// but arrays have fixed size while 'result_of_integr's size depends on base_dim and space_dim
	// so we declare it here in subclasses
	// the only disadvantage is that we must duplicate some code, e.g. 'retrieve_precomputed'
	std::array < double, base_dim * base_dim * case_16 > result_of_integr;  // size 99
	// ideally, 'result_of_integr' should be stored in the CPU cache, for quick access
	// however, I doubt this will happen since 'result_of_integr' is an attribute
	// of a dynamically allocated 'FiniteElement::StandAlone::TypeOne::Triangle' object

	Function bf1, bf2, bf3;

	// constructor

	inline Triangle ( )                    // implemented cases
	:	FiniteElement::StandAlone::TypeOne ( { 1, 2, 3, 4, 5, 6, 7, 8, 11, 12, 13, 14, 15, 16 } ),
		bf1 ( tag::mere_symbol ), bf2 ( tag::mere_symbol ), bf3 ( tag::mere_symbol )
	{	this->basis_numbering [ bf1.core ] = 0;
		this->basis_numbering [ bf2.core ] = 1;
		this->basis_numbering [ bf3.core ] = 2;
		#ifndef NDEBUG  // DEBUG mode
		this->info_string = "FiniteElement::StandAlone::TypeOne::Triangle\n";
		#endif
		                                                                       }
	// two methods  dock_on  virtual from FiniteElement::Core
	void dock_on ( const Cell & cll );
	void dock_on ( const Cell & cll, const tag::Winding & );

	// two methods  dock_on  only significant for
	// FiniteElement::StandAlone::TypeOne::{Rectangle,Square}
	// virtual from FiniteElement::Core, execution forbidden
	// void dock_on ( const Cell & cll, const tag::FirstVertex &, const Cell & )
	// void dock_on ( const Cell & cll, const tag::FirstVertex &, const Cell &,
	//                const tag::Winding &                                     )

	inline void dock_on_comput ( const double & xP, const double & yP,
	                             const double & xQ, const double & yQ,
	                             const double & xR, const double & yR );
	
	// pre_compute  virtual from FiniteElement::Core
	// defined by FiniteElement::StandAlone::TypeOne

	// retrieve_precomputed  virtual from FiniteElement::Core
	std::vector < double > retrieve_precomputed ( const Function & bf, const Function & psi );
	std::vector < double > retrieve_precomputed
	( const Function & bf1, const Function & psi1, const Function & bf2, const Function & psi2 );

	Cell::Numbering & build_global_numbering ( );  // virtual from FiniteElement::Core

	#ifndef NDEBUG  // DEBUG mode
	size_t assert_index
	( const size_t psi_P, const size_t psi_Q, const size_t int_psi_psi, const size_t base_dim );
	#endif
	
	#ifndef NDEBUG  // DEBUG mode
	std::string info ( );  // virtual from FiniteElement::Core
	#endif

};  // end of  class FiniteElement::StandAlone::TypeOne::Triangle

//------------------------------------------------------------------------------------------------------//


class FiniteElement::StandAlone::TypeOne::Quadrangle : public FiniteElement::StandAlone::TypeOne

// quadrangular finite element, no master element

{	public :

	// attributes inherited from FiniteElement::Core :
	// Integrator integr
	// std::map < Cell::Core *, Function > base_fun_1
	// std::map < Cell::Core *, std::map < Cell::Core *, Function > > base_fun_2
	// Cell docked_on
	// std::string info_string (ifndef NDEBUG)

	// attributes inherited from FiniteElement::StandAlone :
	// size_t cas { 0 }
	// std::vector < size_t > selector
	// std::map < Cell::Core *, size_t > local_numbering_1
	// std::map < Function::Core *, size_t > basis_numbering
	// const std::set < size_t > implemented_cases
	// std::string pre_compute_tries  (ifndef NDEBUG)

	// attributes inherited from FiniteElement::StandAlone::TypeOne :
	// std::vector < Function > dummy_bf  dummy base functions, arguments of 'pre_compute'

	static constexpr size_t base_dim = 4;
	static constexpr size_t space_dim = 2;
	static constexpr size_t
		case_1 = 1,  // integral of psi
		case_2 = 1,  // integral of psi1 * psi2
		case_3 = space_dim,  // integral of d_psi_dx
		case_4 = space_dim * space_dim,  // integral of d_psi1_dx * d_psi2_dy
		case_5 = 1,  // integral of grad psi1 * grad psi2
		case_6 = space_dim,  // integral of psi1 * d_psi2_dx
		case_7 = case_1 + case_2, case_8 = case_1 + case_3,
		case_9 = case_1 + case_4, case_10 = case_1 + case_5,
		case_11 = case_3 + case_4, case_12 = case_2 + case_4,
		case_13 = case_2 + case_5, case_14 = case_7 + case_11,
		case_15 = case_14 + case_5, case_16 = case_15 + case_6;  // case_16 = 11
			
	// conceptually, 'result_of_integr' belongs to 'FiniteElement::StandAlone'
	// and should have been declared there
	// but arrays have fixed size while 'result_of_integr's size depends on base_dim and space_dim
	// so we declare it here in subclasses
	// the only disadvantage is that we must duplicate some code, e.g. 'retrieve_precomputed'
	std::array < double, base_dim * base_dim * case_16 > result_of_integr;  // size 176
	// ideally, 'result_of_integr' should be stored in the CPU cache, for quick access
	// however, I doubt this will happen since 'result_of_integr' is an attribute
	// of a dynamically allocated 'FiniteElement::StandAlone::TypeOne::Quadrangle' object

	Function bf1, bf2, bf3, bf4;

	// constructor by itself :
	inline Quadrangle ( )                 // implemented cases
	:	FiniteElement::StandAlone::TypeOne ( { 3, 4 } ), bf1 ( tag::mere_symbol ),
		bf2 ( tag::mere_symbol ), bf3 ( tag::mere_symbol ), bf4 ( tag::mere_symbol )
	{	this->basis_numbering [ bf1.core ] = 0;
		this->basis_numbering [ bf2.core ] = 1;
		this->basis_numbering [ bf3.core ] = 2;
		this->basis_numbering [ bf4.core ] = 3;
		#ifndef NDEBUG  // DEBUG mode
		this->info_string = "FiniteElement::StandAlone::TypeOne::Quadrangle\n";
		#endif
		                                                                         }
	// constructor called from a subclass :
	inline Quadrangle ( const std::set < size_t > & ic )    // implemented cases
	:	FiniteElement::StandAlone::TypeOne ( ic ), bf1 ( tag::mere_symbol ),
		bf2 ( tag::mere_symbol ), bf3 ( tag::mere_symbol ), bf4 ( tag::mere_symbol )
	{	this->basis_numbering [ bf1.core ] = 0;
		this->basis_numbering [ bf2.core ] = 1;
		this->basis_numbering [ bf3.core ] = 2;
		this->basis_numbering [ bf4.core ] = 3;  }
	
	// two methods  dock_on  virtual from FiniteElement::Core
	void dock_on ( const Cell & cll );
	void dock_on ( const Cell & cll, const tag::Winding & );

	// two methods  dock_on  only significant for
	// FiniteElement::StandAlone::TypeOne::{Rectangle,Square}
	// virtual from FiniteElement::Core, execution forbidden, later overridden
	// void dock_on ( const Cell & cll, const tag::FirstVertex &, const Cell & )
	// void dock_on ( const Cell & cll, const tag::FirstVertex &, const Cell &,
	//                const tag::Winding &                                     )

	inline void dock_on_comput 
	( const double & xP, const double & yP, const double & xQ, const double & yQ,
	  const double & xR, const double & yR, const double & xS, const double & yS );
	
	// pre_compute  virtual from FiniteElement::Core
	// defined by FiniteElement::StandAlone::TypeOne

	// retrieve_precomputed  virtual from FiniteElement::Core
	std::vector < double > retrieve_precomputed ( const Function & bf, const Function & psi );
	std::vector < double > retrieve_precomputed
	( const Function & bf1, const Function & psi1, const Function & bf2, const Function & psi2 );

	Cell::Numbering & build_global_numbering ( );  // virtual from FiniteElement::Core

	#ifndef NDEBUG  // DEBUG mode
	size_t assert_index
	( const size_t psi_P, const size_t psi_Q, const size_t int_psi_psi, const size_t base_dim );
	#endif
	
	#ifndef NDEBUG  // DEBUG mode
	std::string info ( );  // virtual from FiniteElement::Core
	#endif

};  // end of  class FiniteElement::StandAlone::TypeOne::Quadrangle

//------------------------------------------------------------------------------------------------------//


class FiniteElement::StandAlone::TypeOne::Parallelogram
: public FiniteElement::StandAlone::TypeOne::Quadrangle

// parallelogram finite element (a particular case of quadrangle), no master element

{	public :

	// attributes inherited from FiniteElement::Core :
	// Integrator integr
	// std::map < Cell::Core *, Function > base_fun_1
	// std::map < Cell::Core *, std::map < Cell::Core *, Function > > base_fun_2
	// Cell docked_on
	// std::string info_string (ifndef NDEBUG)

	// attributes inherited from FiniteElement::StandAlone :
	// size_t cas { 0 }
	// std::vector < size_t > selector
	// std::map < Cell::Core *, size_t > local_numbering_1
	// std::map < Function::Core *, size_t > basis_numbering
	// const std::set < size_t > implemented_cases
	// std::string pre_compute_tries  (ifndef NDEBUG)

	// attributes inherited from FiniteElement::StandAlone::TypeOne :
	// std::vector < Function > dummy_bf  dummy base functions, arguments of 'pre_compute'

	// attributes inherited from FiniteElement::StandAlone::TypeOne::Quadrangle :
	// static constexpr base_dim = 4, space_dim = 2, case_16 = 11
	// std::array < double, base_dim * base_dim * case_16 > result_of_integr  // size 176
	// Function bf1, bf2, bf3, bf4

	// constructor by itself :
	inline Parallelogram ( )                          // no cases implemented yet
	:	FiniteElement::StandAlone::TypeOne::Quadrangle ( std::set < size_t > { } )
	#ifndef NDEBUG  // DEBUG mode
	{	this->info_string = "FiniteElement::StandAlone::TypeOne::Parallelogram\n";  }
	#else  // NDEBUG
	{	}
	#endif
	
	// constructor called from a subclass :
	inline Parallelogram ( const std::set < size_t > & ic )  // implemented cases
	:	FiniteElement::StandAlone::TypeOne::Quadrangle ( ic )
	{	}
	
	// two methods  dock_on  virtual from FiniteElement::Core, here overridden
	void dock_on ( const Cell & cll ) override;
	#ifndef MANIFEM_NO_QUOTIENT
	void dock_on ( const Cell & cll, const tag::Winding & ) override;
	#endif  // ifndef MANIFEM_NO_QUOTIENT

	// two methods  dock_on  only significant for
	// FiniteElement::StandAlone::TypeOne::{Rectangle,Square}
	// virtual from FiniteElement::Core, execution forbidden
	// void dock_on ( const Cell & cll, const tag::FirstVertex &, const Cell & )
	// void dock_on ( const Cell & cll, const tag::FirstVertex &, const Cell &,
	//                const tag::Winding &                                     )

	inline void dock_on_comput 
	( const double & xP, const double & yP, const double & xQ, const double & yQ,
	  const double & xR, const double & yR, const double & xS, const double & yS );
	
	// pre_compute  virtual from FiniteElement::Core
	// defined by FiniteElement::StandAlone::TypeOne

	// retrieve_precomputed  virtual from FiniteElement::Core
	// defined by FiniteElement::StandAlone::TypeOne::Quadrangle

	Cell::Numbering & build_global_numbering ( );  // virtual from FiniteElement::Core

	#ifndef NDEBUG  // DEBUG mode
	std::string info ( );  // virtual from FiniteElement::Core
	#endif

};  // end of  class FiniteElement::StandAlone::TypeOne::Parallelogram

//------------------------------------------------------------------------------------------------------//


class FiniteElement::StandAlone::TypeOne::Rectangle
: public FiniteElement::StandAlone::TypeOne::Parallelogram

// rectangular finite element (a particular case of parallelogram), no master element
// sides parallel to axes

{	public :

	// attributes inherited from FiniteElement::Core :
	// Integrator integr
	// std::map < Cell::Core *, Function > base_fun_1
	// std::map < Cell::Core *, std::map < Cell::Core *, Function > > base_fun_2
	// Cell docked_on
	// std::string info_string (ifndef NDEBUG)

	// attributes inherited from FiniteElement::StandAlone :
	// size_t cas { 0 }
	// std::vector < size_t > selector
	// std::map < Cell::Core *, size_t > local_numbering_1
	// std::map < Function::Core *, size_t > basis_numbering
	// const std::set < size_t > implemented_cases
	// std::string pre_compute_tries  (ifndef NDEBUG)

	// attributes inherited from FiniteElement::StandAlone::TypeOne :
	// std::vector < Function > dummy_bf  dummy base functions, arguments of 'pre_compute'

	// attributes inherited from FiniteElement::StandAlone::TypeOne::Quadrangle :
	// static constexpr base_dim = 4, space_dim = 2, case_16 = 11
	// std::array < double, base_dim * base_dim * case_16 > result_of_integr  // size 176
	// Function bf1, bf2, bf3, bf4

	// constructor by itself :
	inline Rectangle ( )                          
	:	FiniteElement::StandAlone::TypeOne::Parallelogram
		( { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14 } )    // implemented cases
	#ifndef NDEBUG  // DEBUG mode
	{	this->info_string = "FiniteElement::StandAlone::TypeOne::Rectangle\n";  }
	#else  // NDEBUG
	{	}
	#endif
	
	// constructor called from a subclass :
	inline Rectangle ( const std::set < size_t > & ic )    // implemented cases
	:	FiniteElement::StandAlone::TypeOne::Parallelogram ( ic )
	{	}
	
	// dock_on  virtual from FiniteElement::Core, here overridden
	void dock_on ( const Cell & cll ) override;
	#ifndef MANIFEM_NO_QUOTIENT
	void dock_on ( const Cell & cll, const tag::Winding & ) override;
	#endif  // ifndef MANIFEM_NO_QUOTIENT

	// two methods  dock_on  virtual from FiniteElement::Core, here overridden
	void dock_on ( const Cell & cll, const tag::FirstVertex &, const Cell & ) override;
	#ifndef MANIFEM_NO_QUOTIENT
	void dock_on ( const Cell & cll, const tag::FirstVertex &, const Cell &,
	               const tag::Winding &                                     ) override;
	#endif  // ifndef MANIFEM_NO_QUOTIENT

	inline void dock_on_comput 
	( const double & xP, const double & yP, const double & xQ, const double & yQ,
	  const double & xR, const double & yR, const double & xS, const double & yS );
	
	// pre_compute  virtual from FiniteElement::Core
	// defined by FiniteElement::StandAlone::TypeOne

	// retrieve_precomputed  virtual from FiniteElement::Core
	// defined by FiniteElement::StandAlone::TypeOne::Quadrangle

	Cell::Numbering & build_global_numbering ( );  // virtual from FiniteElement::Core

	#ifndef NDEBUG  // DEBUG mode
	std::string info ( );  // virtual from FiniteElement::Core
	#endif

};  // end of  class FiniteElement::StandAlone::TypeOne::Rectangle

//------------------------------------------------------------------------------------------------------//


class FiniteElement::StandAlone::TypeOne::Square
: public FiniteElement::StandAlone::TypeOne::Rectangle

// square finite element (a particular case of rectangle), no master element
// sides parallel to axes

{	public :

	// attributes inherited from FiniteElement::Core :
	// Integrator integr
	// std::map < Cell::Core *, Function > base_fun_1
	// std::map < Cell::Core *, std::map < Cell::Core *, Function > > base_fun_2
	// Cell docked_on
	// std::string info_string (ifndef NDEBUG)

	// attributes inherited from FiniteElement::StandAlone :
	// size_t cas { 0 }
	// std::vector < size_t > selector
	// std::map < Cell::Core *, size_t > local_numbering_1
	// std::map < Function::Core *, size_t > basis_numbering
	// const std::set < size_t > implemented_cases
	// std::string pre_compute_tries  (ifndef NDEBUG)

	// attributes inherited from FiniteElement::StandAlone::TypeOne :
	// std::vector < Function > dummy_bf  dummy base functions, arguments of 'pre_compute'

	// attributes inherited from FiniteElement::StandAlone::TypeOne::Quadrangle :
	// static constexpr base_dim = 4, space_dim = 2, case_16 = 11
	// std::array < double, base_dim * base_dim * case_16 > result_of_integr  // size 176
	// Function bf1, bf2, bf3, bf4

	// constructor
	inline Square ( )                              // implemented cases
	  :	FiniteElement::StandAlone::TypeOne::Rectangle ( { 1, 3, 4, 5, 6 } )
	#ifndef NDEBUG  // DEBUG mode
	{	this->info_string = "FiniteElement::StandAlone::TypeOne::Square\n";  }
	#else  // NDEBUG
	{	}
	#endif
	
	// dock_on  virtual from FiniteElement::Core, here overridden
	void dock_on ( const Cell & cll ) override;
	#ifndef MANIFEM_NO_QUOTIENT
	void dock_on ( const Cell & cll, const tag::Winding & ) override;
	#endif  // ifndef MANIFEM_NO_QUOTIENT

	// two methods  dock_on  virtual from FiniteElement::Core, here overridden
	void dock_on ( const Cell & cll, const tag::FirstVertex &, const Cell & ) override;
	#ifndef MANIFEM_NO_QUOTIENT
	void dock_on ( const Cell & cll, const tag::FirstVertex &, const Cell &,
	               const tag::Winding &                                     ) override;
	#endif  // ifndef MANIFEM_NO_QUOTIENT

	inline void dock_on_comput 
	( const double & xP, const double & yP, const double & xQ, const double & yQ,
	  const double & xR, const double & yR, const double & xS, const double & yS );
	
	// pre_compute  virtual from FiniteElement::Core
	// defined by FiniteElement::StandAlone::TypeOne

	// retrieve_precomputed  virtual from FiniteElement::Core
	// defined by FiniteElement::StandAlone::TypeOne::Quadrangle

	Cell::Numbering & build_global_numbering ( );  // virtual from FiniteElement::Core

	#ifndef NDEBUG  // DEBUG mode
	std::string info ( );  // virtual from FiniteElement::Core
	#endif

};  // end of  class FiniteElement::StandAlone::TypeOne::Square

//------------------------------------------------------------------------------------------------------//


class FiniteElement::StandAlone::TypeOne::Tetrahedron : public FiniteElement::StandAlone::TypeOne

// tetrahedral finite element, no master element

{	public :

	// attributes inherited from FiniteElement::Core :
	// Integrator integr
	// std::map < Cell::Core *, Function > base_fun_1
	// std::map < Cell::Core *, std::map < Cell::Core *, Function > > base_fun_2
	// Cell docked_on
	// std::string info_string (ifndef NDEBUG)

	// attributes inherited from FiniteElement::StandAlone :
	// size_t cas { 0 }
	// std::vector < size_t > selector
	// std::map < Cell::Core *, size_t > local_numbering_1
	// std::map < Function::Core *, size_t > basis_numbering
	// const std::set < size_t > implemented_cases
	// std::string pre_compute_tries  (ifndef NDEBUG)

	// attributes inherited from FiniteElement::StandAlone::TypeOne :
	// std::vector < Function > dummy_bf  dummy base functions, arguments of 'pre_compute'

	static constexpr size_t base_dim = 4;
	static constexpr size_t space_dim = 3;
	static constexpr size_t
		case_1 = 1,  // integral of psi
		case_2 = 1,  // integral of psi1 * psi2
		case_3 = space_dim,  // integral of d_psi_dx
		case_4 = space_dim * space_dim,  // integral of d_psi1_dx * d_psi2_dy
		case_5 = 1,  // integral of grad psi1 * grad psi2
		case_6 = space_dim,  // integral of psi1 * d_psi2_dx
		case_7 = case_1 + case_2, case_8 = case_1 + case_3,
		case_9 = case_1 + case_4, case_10 = case_1 + case_5,
		case_11 = case_3 + case_4, case_12 = case_2 + case_4,
		case_13 = case_2 + case_5, case_14 = case_7 + case_11,
		case_15 = case_14 + case_5, case_16 = case_15 + case_6;  // case_16 = 18
			
	// conceptually, 'result_of_integr' belongs to 'FiniteElement::StandAlone'
	// and should have been declared there
	// but arrays have fixed size while 'result_of_integr's size depends on base_dim and space_dim
	// so we declare it here in subclasses
	// the only disadvantage is that we must duplicate some code, e.g. 'retrieve_precomputed'
	std::array < double, base_dim * base_dim * case_16 > result_of_integr;  // size 288
	// ideally, 'result_of_integr' should be stored in the CPU cache, for quick access
	// however, I doubt this will happen since 'result_of_integr' is an attribute
	// of a dynamically allocated 'FiniteElement::StandAlone::TypeOne::Tetrahedron' object

	Function bf1, bf2, bf3, bf4;

	// constructor
	inline Tetrahedron ( )               // implemented cases
	:	FiniteElement::StandAlone::TypeOne ( { 1, 2, 3, 4, 5, 7, 8, 9, 10, 11, 12, 13, 14 } ),
		bf1 ( tag::mere_symbol ), bf2 ( tag::mere_symbol ),
		bf3 ( tag::mere_symbol ), bf4 ( tag::mere_symbol )
	{	this->basis_numbering [ bf1.core ] = 0;
		this->basis_numbering [ bf2.core ] = 1;
		this->basis_numbering [ bf3.core ] = 2;
		this->basis_numbering [ bf4.core ] = 3;
		#ifndef NDEBUG  // DEBUG mode
		this->info_string = "FiniteElement::StandAlone::TypeOne::Tetrahedron\n";
		#endif
		                                                                          }
	// two methods  dock_on  virtual from FiniteElement::Core
	void dock_on ( const Cell & cll );
	#ifndef MANIFEM_NO_QUOTIENT
	void dock_on ( const Cell & cll, const tag::Winding & );
	#endif  // ifndef MANIFEM_NO_QUOTIENT

	// virtual from FiniteElement::Core, execution forbidden
	// void dock_on ( const Cell & cll, const tag::FirstVertex &, const Cell & )
	// void dock_on ( const Cell & cll, const tag::FirstVertex &, const Cell &,
	//                const tag::Winding &                                     )
	
	inline void dock_on_comput ( const double & xP, const double & yP, const double & zP,
	                             const double & xQ, const double & yQ, const double & zQ,
	                             const double & xR, const double & yR, const double & zR,
	                             const double & xS, const double & yS, const double & zS );
	
	// pre_compute  virtual from FiniteElement::Core
	// defined by FiniteElement::StandAlone::TypeOne

	// retrieve_precomputed  virtual from FiniteElement::Core
	std::vector < double > retrieve_precomputed ( const Function & bf, const Function & psi );
	std::vector < double > retrieve_precomputed
	( const Function & bf1, const Function & psi1, const Function & bf2, const Function & psi2 );

	Cell::Numbering & build_global_numbering ( );  // virtual from FiniteElement::Core

	#ifndef NDEBUG  // DEBUG mode
	size_t assert_index
	( const size_t psi_P, const size_t psi_Q, const size_t int_psi_psi, const size_t base_dim );
	#endif
	
	#ifndef NDEBUG  // DEBUG mode
	std::string info ( );  // virtual from FiniteElement::Core
	#endif

};  // end of  class FiniteElement::StandAlone::TypeOne::Tetrahedron

//------------------------------------------------------------------------------------------------------//


FiniteElement::FiniteElement  // no master, stand-alone
( const tag::Triangle &, const tag::lagrange &, const tag::OfDegree &, size_t deg )
	
:	core { new FiniteElement::StandAlone::TypeOne::Triangle() }, weak { false }

{	this->core->counter = 1;
	assert ( deg == 1 );      }


FiniteElement::FiniteElement  // no master, stand-alone
( const tag::Quadrangle &, const tag::lagrange &, const tag::OfDegree &, size_t deg )
	
:	core { new FiniteElement::StandAlone::TypeOne::Quadrangle() }, weak { false }

{	this->core->counter = 1;
	assert ( deg == 1 );      }


FiniteElement::FiniteElement  // no master, stand-alone
( const tag::Rectangle &, const tag::lagrange &, const tag::OfDegree &, size_t deg )
	
:	core { new FiniteElement::StandAlone::TypeOne::Rectangle() }, weak { false }

{	this->core->counter = 1;
	assert ( deg == 1 );      }


FiniteElement::FiniteElement  // no master, stand-alone
( const tag::Square &, const tag::lagrange &, const tag::OfDegree &, size_t deg )
	
:	core { new FiniteElement::StandAlone::TypeOne::Square() }, weak { false }

{	this->core->counter = 1;
	assert ( deg == 1 );      }


FiniteElement::FiniteElement  // no master, stand-alone
( const tag::Tetrahedron &, const tag::lagrange &, const tag::OfDegree &, size_t deg )
	
:	core { new FiniteElement::StandAlone::TypeOne::Tetrahedron() }, weak { false }

{	this->core->counter = 1;
	assert ( deg == 1 );      }


Integrator FiniteElement::set_integrator ( const tag::HandCoded & )
	
{ FiniteElement::StandAlone::TypeOne * this_core = tag::Util::assert_cast
		< FiniteElement::Core *, FiniteElement::StandAlone::TypeOne * > ( this->core );
	Integrator::HandCoded * integ = new Integrator::HandCoded ( tag::from_finite_element, *this );
	this_core->integr .core = integ;
	assert ( integ->counter == 0 );
   integ->counter = 1;

	assert ( this_core->counter > 1 );
	this_core->counter --;
	integ->finite_element .weak = true;
	
	return this_core->integr;                                                                       }

//------------------------------------------------------------------------------------------------------//

}  // end of  namespace maniFEM

#endif  // ifndef MANIFEM_FINITE_ELEM_XX_H
