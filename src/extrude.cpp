
//   extrude.cpp  2025.02.24

//   This file is part of maniFEM, a C++ library for meshes and finite elements on manifolds.

//   Copyright  2019 - 2025  Cristian Barbarosie  cristian.barbarosie@gmail.com

//   https://maniFEM.rd.ciencias.ulisboa.pt/
//   https://codeberg.org/cristian.barbarosie/maniFEM

//   ManiFEM is free software: you can redistribute it and/or modify it
//   under the terms of the GNU Lesser General Public License as published
//   by the Free Software Foundation, either version 3 of the License
//   or (at your option) any later version.

//   ManiFEM is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//   See the GNU Lesser General Public License for more details.

//   You should have received a copy of the GNU Lesser General Public License
//   along with maniFEM.  If not, see <https://www.gnu.org/licenses/>.


#include "maniFEM.h"


namespace maniFEM {

namespace tag

{	struct VertexIsAtEnd { };  static const VertexIsAtEnd vertex_is_at_end;
	struct VertexIsAtBegin { };  static const VertexIsAtBegin vertex_is_at_begin;  }

}  // end of  namespace maniFEM


using namespace maniFEM;

namespace {  // anonymous namespace, mimics static linkage


class Translation2D  // hidden in anonymous namespace

{	public :

	static constexpr size_t geom_dim = 2;
	static const std::vector < double > identity;
	// see comments below about 'deform'
	
	typedef std::vector < double > deform_type;

	static void place ( const Cell & V, const Cell & common_vertex,
	                    const Cell & S, const std::vector < double > & deform );
	// 'deform' is a vector of two doubles (dx,dy)

	// the two classes below produce the same results, we use 'Direct'
	class Direct;  class Cumulative;

};  // end of  class Translation2D


class Translation2D::Direct : public Translation2D

{	public :

	// inherited from Translation2D :
	// static constexpr size_t geom_dim = 2
	// static const std::vector < double > identity
	// static void place ( ... )

	static void describe_geometry
	( const Mesh & track, const Cell & common_vertex,
	  std::map < Cell, std::vector < double > > & deform, size_t max_counter );
	// 'deform' is a map which, for each vertex, keeps two doubles (dx,dy)

};  // end of  class Translation2D::Direct


class RotoTranslation2D  // hidden in anonymous namespace

{	public :

	static constexpr size_t geom_dim = 2;
	static constexpr size_t rot_matrix_dim = 2;
	static const std::pair < std::vector < double >, std::vector < double > > identity;
	// see comments below about 'deform'

	typedef std::pair < std::vector < double >, std::vector < double > > deform_type;
	
	static void place ( const Cell & V, const Cell & common_vertex, const Cell & S,
	                    const deform_type & deform                                 );
	// 'deform' is a pair vectors { dx, dy } { cos theta, sin theta }

	static inline void apply_rotation_matrix_to_vector
	( std::vector < double > & res,
	  const std::vector < double > & rot, const std::vector < double > & tau  );
	static inline void apply_rotation_matrix_to_matrix
	( std::vector < double > & res,
	  const std::vector < double > & rot, const std::vector < double > & matr );
	static inline std::vector < double > compute_rotation_matrix
	( const std::vector < double > & u, const std::vector < double > & v, double & c );
	static inline void normalize_rotation_matrix ( std::vector < double > & rot );

	class Direct;  class Cumulative;

};  // end of  class RotoTranslation2D


class RotoTranslation2D::Direct : public RotoTranslation2D 

// compute the angle of rotation comparing directions of segments in the 'track'
// far from each other
// with one exception: when the angle of rotation becomes too wide, we re-center

// translations are computed directly from the common vertex

{	public :

	// inherited from RotoTranslation2D :
	// static constexpr size_t geom_dim = 2
	// static const std::pair < std::vector < double >, std::vector < double > > identity
	// static void place ( ... )
	// typedef std::pair < std::vector < double >, std::vector < double > > deform_type

	static void describe_geometry
	( const Mesh & track, const Cell & common_vertex,
	  std::map < Cell, deform_type > & deform, size_t max_counter );
	// 'deform' is a map which, for each vertex, keeps four doubles { dx, dy } { cos theta, sin theta }

};  // end of  class RotoTranslation2D::Direct


class Translation3D    // hidden in anonymous namespace

{	public :

	static constexpr size_t geom_dim = 3;
	static const std::vector < double > identity;
	// see comments below about 'deform'
	
	typedef std::vector < double > deform_type;

	static void place ( const Cell & V, const Cell & common_vertex,
	                    const Cell & S, const std::vector < double > & deform );
	// 'deform' is a vector of three doubles (dx,dy,dz)

	// the two classes below produce the same results, we use 'Direct'
	struct Direct      {  class Track1D;  class Track2D;  };
	struct Cumulative  {  class Track1D;  class Track2D;  };

};  // end of  class Translation3D


class Translation3D::Direct::Track1D : public Translation3D

{	public :

	// inherited from Translation3D :
	// static constexpr size_t geom_dim = 3
	// static const std::vector < double > identity
	// static void place ( ... )

	static void describe_geometry
	( const Mesh & track, const Cell & common_vertex,
	  std::map < Cell, std::vector < double > > & deform, size_t max_counter );
	// 'deform' is a map which, for each vertex, keeps three doubles (dx,dy,dz)

};  // end of  class Translation3D::Direct::Track1D


class Translation3D::Direct::Track2D : public Translation3D

{	public :

	// inherited from Translation3D :
	// static constexpr size_t geom_dim = 3
	// static const std::vector < double > identity
	// static void place ( ... )

	static void describe_geometry
	( const Mesh & track, const Cell & common_vertex,
	  std::map < Cell, std::vector < double > > & deform, size_t max_counter );
	// 'deform' is a map which, for each vertex, keeps three doubles (dx,dy,dz)

};  // end of  class Translation3D::Direct::Track2D


class RotoTranslation3D  // hidden in anonymous namespace

{	public :

	static constexpr size_t geom_dim = 3;
	static constexpr size_t rot_matrix_dim = 9;
	static const std::pair < std::vector < double >, std::vector < double > > identity;
	// see comments below about 'deform'
	
	typedef std::pair < std::vector < double >, std::vector < double > > deform_type;
	
	static void place ( const Cell & V, const Cell & common_vertex, const Cell & S,
	                    const deform_type & deform                                 );
	// 'deform' is a pair vectors { dx, dy, dz } { 3x3 orthogonal matrix m_ij }
	
	static inline void apply_rotation_matrix_to_vector
	( std::vector < double > & res,
	  const std::vector < double > & rot, const std::vector < double > & tau  );
	static inline void apply_rotation_matrix_to_matrix
	( std::vector < double > & res,
	  const std::vector < double > & rot, const std::vector < double > & matr );
	static inline std::vector < double > compute_rotation_matrix
	( const std::vector < double > & u, const std::vector < double > & v, double & c );
	static inline void normalize_rotation_matrix ( std::vector < double > & rot );

	struct Direct      {  class Track1D;  class Track2D;  };
	struct Cumulative  {  class Track1D;  class Track2D;  };

};  // end of  class RotoTranslation3D

  
class RotoTranslation3D::Direct::Track1D : public RotoTranslation3D

// compute the angle of rotation comparing directions of segments in the 'track'
// far from each other
// with one exception: when the angle of rotation becomes too wide, we re-center

// translations are computed directly from the common vertex

{	public :

	// inherited from RotoTranslation3D :
	// static constexpr size_t geom_dim = 3
	// static const std::pair < std::vector < double >, std::vector < double > > identity
	// static void place ( ... )
	// typedef std::pair < std::vector < double >, std::vector < double > > deform_type
	
	static void describe_geometry
	( const Mesh & track, const Cell & common_vertex,
	  std::map < Cell, deform_type > & deform, size_t max_counter );
	// 'deform' is a map which, for each vertex, keeps two vectors
	// (dx,dy,dz) and a 3x3 orthogonal matrix m_ij

};  // end of  class RotoTranslation3D::Direct::Track1D


class RotoTranslation3D::Cumulative::Track1D : public RotoTranslation3D 

// compute small angles of rotation between adjacent segments in the 'track'
// then accumulate the effects of these rotations by successive matrix multiplication

// translations are computed directly from the common vertex

{	public :

	// inherited from RotoTranslation3D :
	// static constexpr size_t geom_dim = 3
	// static const std::pair < std::vector < double >, std::vector < double > > identity
	// static void place ( ... )
	// typedef std::pair < std::vector < double >, std::vector < double > > deform_type
	
	static void describe_geometry
	( const Mesh & track, const Cell & common_vertex,
	  std::map < Cell, deform_type > & deform, size_t max_counter );
	// 'deform' is a map which, for each vertex, keeps twelve doubles
	// (dx,dy,dz) and a 3x3 orthogonal matrix m_ij

};  // end of  class RotoTranslation3D::Cumulative::Track1D

//------------------------------------------------------------------------------------------------------//


const std::vector < double >             Translation2D::identity { 0., 0. };                 // static
const RotoTranslation2D::deform_type RotoTranslation2D::identity { { 0., 0. }, { 1., 0. } }; // static
const std::vector < double >             Translation3D::identity { 0., 0., 0. };             // static
const RotoTranslation3D::deform_type RotoTranslation3D::identity                             // static
	{ { 0., 0., 0. }, { 1., 0., 0., 0., 1., 0., 0., 0., 1. } };
// see comments about 'deform' in the corresponding class

//------------------------------------------------------------------------------------------------------//
//------------------------------------------------------------------------------------------------------//


inline void RotoTranslation2D::apply_rotation_matrix_to_vector  // static
( std::vector < double > & res,
  const std::vector < double > & rot, const std::vector < double > & tau )

// res = rot * tau

// 'res' may be the same as 'tau'

{	assert ( rot .size() == 2 );  // cos theta, sin theta
	assert ( tau .size() == 2 );  // x y
	assert ( res .size() == 2 );  // x y

	const double tmp = rot [0] * tau [0] - rot [1] * tau [1];    // cos theta * x - sin theta * y
	res [1] =          rot [1] * tau [0] + rot [0] * tau [1];    // sin theta * x + cos theta * y
	res [0] = tmp;                                             }


inline void RotoTranslation2D::apply_rotation_matrix_to_matrix  // static
( std::vector < double > & res,
  const std::vector < double > & rot, const std::vector < double > & matr )

// res = rot * matr

// 'res' may be the same as 'matr'

{	assert ( rot  .size() == 2 );  // cos a, sin a
	assert ( matr .size() == 2 );  // cos b, sin b
	assert ( res  .size() == 2 );  // cos theta, sin theta

	const double tmp = rot [0] * matr [0] - rot [1] * matr [1];    // cos a * cos b - sin a * sin b
	res [1] =          rot [1] * matr [0] + rot [0] * matr [1];    // sin a * cos b + cos a * sin b
	res [0] = tmp;                                               }


inline std::vector < double > RotoTranslation2D::compute_rotation_matrix  // static
( const std::vector < double > & u, const std::vector < double > & v, double & c )

// res * u = v
// both 'u' and 'v' have norm 1.
// return also separately c = cos theta, used to decide whether the angle is wide or not

{	assert ( u .size() == 2 );  // x y
	assert ( v .size() == 2 );  // x y
	std::vector < double > res (2);  // cos theta, sin theta

	res [0] = c = u[0] * v[0] + u[1] * v[1];   // cos theta = u.v
	res [1] =     u[0] * v[1] - u[1] * v[0];   // sin theta = uxv

	return res;                               }


inline void RotoTranslation3D::apply_rotation_matrix_to_vector  // static
( std::vector < double > & res,
  const std::vector < double > & rot, const std::vector < double > & tau )

// res = rot * tau

// 'res' may be the same as 'tau'

{	assert ( rot .size() == 9 );  // m00, m01, m02, m10, m11, m12, m20, m21, m22
	assert ( tau .size() == 3 );  // x y z
	assert ( res .size() == 3 );  // x y z

	const double tmp_0 = rot [0] * tau [0] + rot [1] * tau [1] + rot [2] * tau [2];
	const double tmp_1 = rot [3] * tau [0] + rot [4] * tau [1] + rot [5] * tau [2];
	res [2] =            rot [6] * tau [0] + rot [7] * tau [1] + rot [8] * tau [2];
	res [0] = tmp_0;
	res [1] = tmp_1;
}


inline void RotoTranslation3D::apply_rotation_matrix_to_matrix  // static
( std::vector < double > & res,
  const std::vector < double > & rot, const std::vector < double > & matr )

// res = rot * matr

// 'res' may be the same as 'matr'

{	assert ( rot  .size() == 9 );  // m00, m01, m02, m10, m11, m12, m20, m21, m22
	assert ( matr .size() == 9 );  // m00, m01, m02, m10, m11, m12, m20, m21, m22
	assert ( res  .size() == 9 );  // m00, m01, m02, m10, m11, m12, m20, m21, m22

	std::vector < double > resres (9);
	
	resres [0] = rot [0] * matr [0] + rot [1] * matr [3] + rot [2] * matr [6];
	resres [1] = rot [0] * matr [1] + rot [1] * matr [4] + rot [2] * matr [7];
	resres [2] = rot [0] * matr [2] + rot [1] * matr [5] + rot [2] * matr [8];

	resres [3] = rot [3] * matr [0] + rot [4] * matr [3] + rot [5] * matr [6];
	resres [4] = rot [3] * matr [1] + rot [4] * matr [4] + rot [5] * matr [7];
	resres [5] = rot [3] * matr [2] + rot [4] * matr [5] + rot [5] * matr [8];

	resres [6] = rot [6] * matr [0] + rot [7] * matr [3] + rot [8] * matr [6];
   resres [7] = rot [6] * matr [1] + rot [7] * matr [4] + rot [8] * matr [7];
	resres [8] = rot [6] * matr [2] + rot [7] * matr [5] + rot [8] * matr [8];

	res .swap ( resres );                                                       }


inline std::vector < double > RotoTranslation3D::compute_rotation_matrix  // static
( const std::vector < double > & u, const std::vector < double > & v, double & c )

// res * u = v
// both 'u' and 'v' have norm 1.
// return also separately c = cos theta, used to decide whether the angle is wide or not

// see paragraphs 8.3 and 8.4 in the manual

{	assert ( u .size() == 3 );  // x y z
	assert ( v .size() == 3 );  // x y z
	std::vector < double > res (9);  // m00, m01, m02, m10, m11, m12, m20, m21, m22

	std::vector < double > u_plus_v (3);
	for ( size_t i = 0; i < 3; i++ )
		u_plus_v [i] = u[i] + v[i];

	c = u[0]*v[0] + u[1]*v[1] + u[2]*v[2];
	const double one_plus_c = 1. + c;

	res [0] = 1. + 2. * u[0] * v[0] - u_plus_v [0] * u_plus_v [0] / one_plus_c;     // res00
	res [1] =      2. * u[1] * v[0] - u_plus_v [0] * u_plus_v [1] / one_plus_c;     // res01
	res [2] =      2. * u[2] * v[0] - u_plus_v [0] * u_plus_v [2] / one_plus_c;     // res02
	res [3] =      2. * u[0] * v[1] - u_plus_v [1] * u_plus_v [0] / one_plus_c;     // res10
	res [4] = 1. + 2. * u[1] * v[1] - u_plus_v [1] * u_plus_v [1] / one_plus_c;     // res11
	res [5] =      2. * u[2] * v[1] - u_plus_v [1] * u_plus_v [2] / one_plus_c;     // res12
	res [6] =      2. * u[0] * v[2] - u_plus_v [2] * u_plus_v [0] / one_plus_c;     // res20
	res [7] =      2. * u[1] * v[2] - u_plus_v [2] * u_plus_v [1] / one_plus_c;     // res21
	res [8] = 1. + 2. * u[2] * v[2] - u_plus_v [2] * u_plus_v [2] / one_plus_c;     // res22

	return res;                                                                  }

//------------------------------------------------------------------------------------------------------//


template < class Position >
inline void place_transl      // hidden in anonymous namespace
( const Cell & V, const Cell & S, const std::vector < double > & deform )

// class Position  could be 'Translation2D' or 'Translation3D'

{	Manifold RRd = Manifold::working;
	constexpr size_t geom_dim = Position::geom_dim;
	assert ( RRd .topological_dim() == geom_dim );
	Function coords = RRd .coordinates();
	assert ( coords .number_of ( tag::components ) == geom_dim );
	assert ( deform .size() == geom_dim );

	for ( size_t i = 0; i < geom_dim; i++ )
	{	const Function & x = coords [i];
		x(V) = x(S) + deform [i];         }                         }


void Translation2D::place  // static
( const Cell & V, const Cell & common_vertex, const Cell & S, const std::vector < double > & deform )
{	place_transl < Translation2D > ( V, S, deform );  }


void Translation3D::place  // static
( const Cell & V, const Cell & common_vertex, const Cell & S, const std::vector < double > & deform )
{	place_transl < Translation3D > ( V, S, deform );  }


template < class Position >
inline void place_roto_transl  // static
( const Cell & V, const Cell & common_vertex, const Cell & S,
  const std::pair < std::vector < double >, std::vector < double > > & deform )

// class Position  could be 'RotoTranslation2D' or 'RotoTranslation3D'

{	Manifold RRd = Manifold::working;
	constexpr size_t geom_dim = Position::geom_dim;
	assert ( RRd .topological_dim() == geom_dim );
	Function coords = RRd .coordinates();
	assert ( coords .number_of ( tag::components ) == geom_dim );

	const std::vector < double > & transl = deform .first;
	const std::vector < double > & rot    = deform .second;
	assert ( transl .size() == geom_dim );

	std::vector < double > position ( geom_dim );
	// set 'common_vertex' as origin
	for ( size_t i = 0; i < geom_dim; i++ )
	{	const Function & x = coords [i];
		position [i] = x(S) - x ( common_vertex );   }
	// rotate :  position = rot * position
   Position::apply_rotation_matrix_to_vector ( position, rot, position );
	// translate back
	for ( size_t i = 0; i < geom_dim; i++ )
	{	const Function & x = coords [i];
		x(V) = x ( common_vertex ) + position [i] + transl [i];  }

}  // end of  place_roto_transl


void RotoTranslation2D::place  // static
( const Cell & V, const Cell & common_vertex, const Cell & S,
  const std::pair < std::vector < double >, std::vector < double > > & deform )

// deform : { dx, dy }, { cos theta, sin theta }

{	place_roto_transl < RotoTranslation2D > ( V, common_vertex, S, deform );  }


void RotoTranslation3D::place  // static
( const Cell & V, const Cell & common_vertex, const Cell & S,
  const std::pair < std::vector < double >, std::vector < double > > & deform )

// deform : { dx, dy, dz }, { m00, m01, m02, m10, m11, m12, m20, m21, m22 }

{	place_roto_transl < RotoTranslation3D > ( V, common_vertex, S, deform );  }

//------------------------------------------------------------------------------------------------------//


template < class Position >
void compute_tangent_direction  // hidden in anonymous namespace  // defined below
( const Mesh & track, const Cell & B, std::vector < double > & tau );


template < class Position >
inline void compute_tangent_direction  // hidden in anonymous namespace
( const Mesh & track, const Cell & A, std::vector < double > & tau, const tag::VertexIsAtBegin & )

// class Position  could be 'RotoTranslation2D' 'RotoTranslation3D'

// 'track' is a 1D mesh, 'A' is its first vertex

// upon return, 'tau' will be a vector of norm 1.

{	Manifold RRd = Manifold::working;
	const size_t geom_dim = RRd .topological_dim();
	Function coords = RRd .coordinates();
	assert ( coords .number_of ( tag::components ) == geom_dim );
	assert ( tau .size() == geom_dim );

	Cell AB = track .cell_in_front_of ( A, tag::surely_exists );
	Cell B = AB .tip();
	Cell BC = track .cell_in_front_of ( B, tag::surely_exists );
	Cell C = BC .tip();

	compute_tangent_direction < Position > ( track, B, tau );

	// rotate a little more
	std::vector < double > vec_AB ( geom_dim );
	for ( size_t i = 0; i < geom_dim; i++ )
	{	const Function & x = coords [i];
   	vec_AB [i] = x(B) - x(A);         }
	double nor = 0.;
	for ( size_t i = 0; i < geom_dim; i++ )
	{	const double d = vec_AB [i];
		nor += d*d;                   }
	nor = std::sqrt ( nor );
	for ( size_t i = 0; i < geom_dim; i++ )
   	vec_AB [i] /= nor;

	double c;   // not used here
	std::vector < double > rot = Position::compute_rotation_matrix ( tau, vec_AB, c );
	// rot * tau = vec_AB
	Position::apply_rotation_matrix_to_vector ( tau, rot, vec_AB );  //  tau = rot * vec_AB
	
}  // end of  compute_tangent_direction with tag::vertex_is_at_begin
	
	
template < class Position >
inline void compute_tangent_direction  // hidden in anonymous namespace
( const Mesh & track, const Cell & C, std::vector < double > & tau, const tag::VertexIsAtEnd & )

// class Position  could be 'RotoTranslation2D' 'RotoTranslation3D'

// 'track' is a 1D mesh, 'C' is its last vertex

// upon return, 'tau' will be a vector of norm 1.

{	Manifold RRd = Manifold::working;
	const size_t geom_dim = RRd .topological_dim();
	Function coords = RRd .coordinates();
	assert ( coords .number_of ( tag::components ) == geom_dim );
	assert ( tau .size() == geom_dim );

	Cell BC = track .cell_behind ( C, tag::surely_exists );
	Cell B = BC .base() .reverse ( tag::surely_exists );
	Cell AB = track .cell_behind ( B, tag::surely_exists );
	Cell A = AB .base() .reverse ( tag::surely_exists );

	compute_tangent_direction < Position > ( track, B, tau );

	// rotate a little more
	std::vector < double > vec_BC ( geom_dim );
	for ( size_t i = 0; i < geom_dim; i++ )
	{	const Function & x = coords [i];
   	vec_BC [i] = x(C) - x(B);         }
	double nor = 0.;
	for ( size_t i = 0; i < geom_dim; i++ )
	{	const double d = vec_BC [i];
		nor += d*d;                   }
	nor = std::sqrt ( nor );
	for ( size_t i = 0; i < geom_dim; i++ )
   	vec_BC [i] /= nor;

	double c;   // not used here
	std::vector < double > rot = Position::compute_rotation_matrix ( tau, vec_BC, c );
	// rot * tau = vec_BC
	Position::apply_rotation_matrix_to_vector ( tau, rot, vec_BC );  //  tau = rot * vec_BC
	
}  // end of  compute_tangent_direction with tag::vertex_is_at_end
	
	
template < class Position >
void compute_tangent_direction  // hidden in anonymous namespace
( const Mesh & track, const Cell & B, std::vector < double > & tau )

// class Position  could be 'RotoTranslation2D' 'RotoTranslation3D'

// 'track' is a 1D mesh, 'B' is a vertex
// the tangent direction is computed as the direction of AC if 'B' is inner to 'track'
// if 'B' is an extremity, special care must be taken

// upon return, 'tau' will be a vector of norm 1.

{	Manifold RRd = Manifold::working;
	const size_t geom_dim = RRd .topological_dim();
	Function coords = RRd .coordinates();
	assert ( coords .number_of ( tag::components ) == geom_dim );
	assert ( tau .size() == geom_dim );

	assert ( track .dim() == 1 );
	assert ( B .dim() == 0 );
	assert ( B .is_positive() );
	
	Cell AB = track .cell_behind      ( B, tag::may_not_exist );
	Cell BC = track .cell_in_front_of ( B, tag::may_not_exist );

	if ( AB .exists() and BC .exists() )  // 'B' is inner to 'track'
	{	Cell A = AB .base() .reverse ( tag::surely_exists );
		Cell C = BC .tip();
		for ( size_t i = 0; i < geom_dim; i++ )
		{	const Function & x = coords [i];
	   	tau [i] = x(C) - x(A);            }
		double nor = 0.;
		for ( size_t i = 0; i < geom_dim; i++ )
		{	const double d = tau [i];
			nor += d*d;                }
		nor = std::sqrt ( nor );
		for ( size_t i = 0; i < geom_dim; i++ )
	   	tau [i] /= nor;
		return;                                               }

	if ( AB .exists() )  // 'B' is at end of 'track'
	{	compute_tangent_direction < Position > ( track, B, tau, tag::vertex_is_at_end );
		return;                                                                           }

	assert ( BC .exists() );  // 'B' is at beginning of 'track'
	compute_tangent_direction < Position > ( track, B, tau, tag::vertex_is_at_begin );
	
}  // end of  compute_tangent_direction

//------------------------------------------------------------------------------------------------------//


template < class Position >
inline void describe_geometry_transl_direct_1Dtrack      // hidden in anonymous namespace
( const Mesh & track, const Cell & common_vertex,
  std::map < Cell, std::vector < double > > & deform, size_t max_counter )

// class Position  could be 'Translation2D' 'Translation3D'

// 'track' is a 1D mesh

// compute in 'deform' a map of translations
// 'deform' must contain already : common_vertex -> Position::identity

{	Manifold RRd = Manifold::working;
	constexpr size_t geom_dim = Position::geom_dim;
	assert ( RRd .topological_dim() == geom_dim );
	Function coords = RRd .coordinates();
	assert ( coords .number_of ( tag::components ) == geom_dim );

	// the calling code has already done this :
	// deform .insert ( { common_vertex, Position::identity } )

	Cell Q = common_vertex;
	size_t counter = 0;
	while ( true )
	{	if ( counter == max_counter )  break;
		counter ++;
		Cell PQ = track .cell_behind ( Q, tag::may_not_exist );
		if ( not PQ .exists() )  break;  // Q is first vertex in 'track'
		Cell P = PQ .base() .reverse();
		std::vector < double > transl ( geom_dim );
		for ( size_t i = 0; i < geom_dim; i++ )
		{	const Function & x = coords [i];
	   	transl  [i] = x(P) - x ( common_vertex );  }
		assert ( transl .size() == geom_dim );
		tag::Util::insert_new_elem_in_map ( deform, { P, transl } );
		Q = P;
	}  // end of  while true

}  // end of  describe_geometry_transl_direct_1Dtrack


void Translation2D::Direct::describe_geometry
( const Mesh & track, const Cell & common_vertex,
  std::map < Cell, std::vector < double > > & deform, size_t max_counter )

{	describe_geometry_transl_direct_1Dtrack < Translation2D >
		( track, common_vertex, deform, max_counter );          }


void Translation3D::Direct::Track1D::describe_geometry
( const Mesh & track, const Cell & common_vertex,
  std::map < Cell, std::vector < double > > & deform, size_t max_counter )

{	describe_geometry_transl_direct_1Dtrack < Translation3D >
		( track, common_vertex, deform, max_counter );          }


template < class Position >
inline void describe_geometry_direct_1Dtrack      // hidden in anonymous namespace
( const Mesh & track, const Cell & common_vertex,
  std::map < Cell, typename Position::deform_type > & deform, size_t max_counter )

// class Position  could be 'RotoTranslation2D' or 'RotoTranslation3D'

// 'track' is a 1D mesh

// compute in 'deform' (an approximation of) the curvature of this polygonal line
// more precisely, compute a rotation (in the form cos theta, sin theta) for each vertex

// we start from the common vertex and go backwards, for no special reason
// we compute large rotations relatively to the 'base'
// with one exception: when the angle of rotation becomes too wide, we re-center

// translations are computed directly from the common vertex

// 'deform' must contain already : common_vertex -> Position::identity

{	Manifold RRd = Manifold::working;
	constexpr size_t geom_dim = Position::geom_dim;
	assert ( RRd .topological_dim() == geom_dim );
	Function coords = RRd .coordinates();
	assert ( coords .number_of ( tag::components ) == geom_dim );

	Cell Q = common_vertex;
	std::vector < double > vec_common_vertex ( geom_dim );
	for ( size_t i = 0; i < geom_dim; i++ )
	{	const Function & x = coords [i];
		vec_common_vertex [i] = x ( common_vertex );  }
	std::vector < double > rot_base = Position::identity .second;
	std::vector < double > tau_base ( geom_dim );  //  tangent direction at 'base'
	compute_tangent_direction < Position > ( track, common_vertex, tau_base );

	// the calling code has already done this :
	// deform .insert ( { common_vertex, Position::identity } )
	
	size_t counter = 0;
	while ( true )
	{	if ( counter == max_counter )  break;
		counter ++;
		Cell PQ = track .cell_behind ( Q, tag::may_not_exist );
		if ( not PQ .exists() )  break;
		Cell P = PQ .base() .reverse();
		std::vector < double > transl ( geom_dim );
		for ( size_t i = 0; i < geom_dim; i++ )
		{	const Function & x = coords [i];
			transl [i] = x (P) - x ( common_vertex );  }
		std::vector < double > tau_current ( geom_dim );  //  tangent direction at 'P'
		compute_tangent_direction < Position > ( track, P, tau_current );
		double c;  // cosine of the angle of rotation, computed below
		std::vector < double > rot =
				Position::compute_rotation_matrix ( tau_base, tau_current, c );
		// rot * tau_base = tau_current
		std::vector < double > current_rot ( Position::rot_matrix_dim );
		Position::apply_rotation_matrix_to_matrix ( current_rot, rot, rot_base );
		// current_rot = rot * rot_base
		if ( c < -0.5 )  //  cos theta < -0.5  //  wide angle of rotation
		{	rot_base = current_rot;
			tau_base = tau_current;  }
		tag::Util::insert_new_elem_in_map
			( deform, { P, { transl, current_rot } } );
		Q = P;
	}  // end of  while true

}  // end of  describe_geometry_direct_1Dtrack


void RotoTranslation2D::Direct::describe_geometry  // static
( const Mesh & track, const Cell & common_vertex,
  std::map < Cell, RotoTranslation2D::deform_type > & deform, size_t max_counter )

{	describe_geometry_direct_1Dtrack < RotoTranslation2D >
		( track, common_vertex, deform, max_counter );       }


void RotoTranslation3D::Direct::Track1D::describe_geometry  // static
( const Mesh & track, const Cell & common_vertex,
  std::map < Cell, RotoTranslation3D::deform_type > & deform, size_t max_counter )

{	describe_geometry_direct_1Dtrack < RotoTranslation3D >
		( track, common_vertex, deform, max_counter );       }
																				
//------------------------------------------------------------------------------------------------------//
//------------------------------------------------------------------------------------------------------//


template < class Position >
void extrude_backwards_1Dtrack_1Dswatch       // hidden in anonymous namespace
( Mesh & result, const Mesh & swatch, const Mesh & track,
  const std::map < Cell, typename Position::deform_type > & deform, const Cell & common_vertex,
  std::map < Cell, Cell > & previous_ver_to_ver, std::map < Cell, Cell > & previous_seg_to_seg,
  size_t max_counter                                                                           )

// build the extruded mesh, starting at the common vertex and advancing in one direction
// (backwards along the track)

// class Position  could be 'Translation2D::***'          'RotoTranslation2D::***'
//                          'Translation3D::***::Track1D' 'RotoTranslation3D::***::Track1D'
	
// 'swatch' may be open chain or closed loop
// it may even be disconnected
// that's why we avoid using 'first_vertex' and 'last_vertex'
// we also avoid ordered iterators

// 'previous_ver_to_ver' 'previous_seg_to_seg' useful after return
// (only if 'track' is closed) for closing the extruded mesh
// if 'track' is open, they are discarded

// a more complex version of this function can be found below, intended at extruding composite meshes

{	assert ( common_vertex .is_positive() );
	assert ( common_vertex .belongs_to ( track ) );
	assert ( common_vertex .belongs_to ( swatch ) );

	Cell Q = common_vertex;

	{  // just a block of code for hiding 'it_ver_swatch' 'it_seg_swatch'
	Mesh::Iterator it_ver_swatch = swatch .iterator ( tag::over_vertices );
	for ( it_ver_swatch .reset(); it_ver_swatch .in_range(); it_ver_swatch ++ )
	{	Cell S = * it_ver_swatch;
		tag::Util::insert_new_elem_in_map ( previous_ver_to_ver, { S, S } );  }
	Mesh::Iterator it_seg_swatch = swatch .iterator
		( tag::over_segments, tag::orientation_compatible_with_mesh );
	for ( it_seg_swatch .reset(); it_seg_swatch .in_range(); it_seg_swatch ++ )
	{	Cell ST = * it_seg_swatch;
		tag::Util::insert_new_elem_in_map ( previous_seg_to_seg, { ST, ST } );  }
	}  // just a block of code

	size_t counter = 0;
	while ( counter < max_counter )

	{	Cell PQ = track .cell_behind ( Q, tag::may_not_exist );
		if ( not PQ .exists() )  break;
		counter ++;
		Cell P = PQ .base() .reverse ( tag::surely_exists );
		std::map < Cell, Cell > next_ver_to_ver, next_seg_to_seg;

		// build one segment for each vertex of 'swatch'
		// note : one of them will be PQ
		std::map < Cell, Cell > segments_by_ver;
		Mesh::Iterator it_ver_swatch = swatch .iterator ( tag::over_vertices );
		for ( it_ver_swatch .reset(); it_ver_swatch .in_range(); it_ver_swatch ++ )
		{	Cell S = * it_ver_swatch;
			Cell A = tag::Util::get_element_of_map ( previous_ver_to_ver, S, tag::surely_found );
			if ( S == common_vertex )
			{	assert ( A == Q );
				tag::Util::insert_new_elem_in_map ( segments_by_ver, { Q, PQ } );
				tag::Util::insert_new_elem_in_map ( next_ver_to_ver, { S, P } );   }
			else
			{	Cell V ( tag::vertex );
				const typename Position::deform_type & def =
					tag::Util::get_element_of_const_map ( deform, P, tag::surely_found );
				Position::place ( V, common_vertex, S, def );
				Cell VA ( tag::segment, V .reverse(), A );
				tag::Util::insert_new_elem_in_map ( segments_by_ver, { A, VA } );
				tag::Util::insert_new_elem_in_map ( next_ver_to_ver, { S, V } );                 }  }
		
		// for each segment of 'swatch', build one parallel segment and the corresponding rectangle
		Mesh::Iterator it_seg_swatch = swatch .iterator
			( tag::over_segments, tag::orientation_compatible_with_mesh );
		for ( it_seg_swatch .reset(); it_seg_swatch .in_range(); it_seg_swatch ++ )
		{	Cell ST = * it_seg_swatch;
			Cell S = ST .base() .reverse();
			Cell T = ST .tip();
			Cell A = tag::Util::get_element_of_map ( previous_ver_to_ver, S, tag::surely_found );
			Cell B = tag::Util::get_element_of_map ( previous_ver_to_ver, T, tag::surely_found );
			Cell VA = tag::Util::get_element_of_map ( segments_by_ver, A, tag::surely_found );
			Cell V = VA .base() .reverse ( tag::surely_exists );
			Cell WB = tag::Util::get_element_of_map ( segments_by_ver, B, tag::surely_found );
			Cell W = WB .base() .reverse ( tag::surely_exists );
			Cell AB = tag::Util::get_element_of_map ( previous_seg_to_seg, ST, tag::surely_found );
			assert ( AB .base() .reverse ( tag::surely_exists ) == A );
			assert ( AB .tip() == B );
			Cell VW ( tag::segment, V .reverse(), W );
			tag::Util::insert_new_elem_in_map ( next_seg_to_seg, { ST, VW } );
			Cell ABWV ( tag::quadrangle, AB, WB .reverse(),
			            VW .reverse ( tag::does_not_exist, tag::build_now ), VA );
			ABWV .add_to ( result );                                                                    }

		Q = P;
		previous_ver_to_ver .swap ( next_ver_to_ver );
		previous_seg_to_seg .swap ( next_seg_to_seg );

	}  // end of  while loop

	assert ( counter == max_counter );
	
}  // end of  extrude_backwards_1Dtrack_1Dswatch


template < class Position >
void extrude_forward_1Dtrack_1Dswatch       // hidden in anonymous namespace
( Mesh & result, const Mesh & swatch, const Mesh & track,
  const std::map < Cell, typename Position::deform_type > & deform, const Cell & common_vertex,
  std::map < Cell, Cell > & previous_ver_to_ver, std::map < Cell, Cell > & previous_seg_to_seg,
  size_t max_counter                                                                           )

// build the extruded mesh, starting at the common vertex and advancing in one direction
// (forward along the track)

// class Position  could be 'Translation2D::***'          'RotoTranslation2D::***'
//                          'Translation3D::***::Track1D' 'RotoTranslation3D::***::Track1D'
	
// 'swatch' may be open chain or closed loop
// it may even be disconnected
// that's why we avoid using 'first_vertex' and 'last_vertex'
// we also avoid ordered iterators

// 'previous_ver_to_ver' 'previous_seg_to_seg' useful after return
// (only if 'track' is closed) for closing the extruded mesh
// if 'track' is open, they are discarded

// a more complex version of this function can be found below, intended at extruding composite meshes

{	assert ( common_vertex .is_positive() );
	assert ( common_vertex .belongs_to ( track ) );
	assert ( common_vertex .belongs_to ( swatch ) );

	Cell P = common_vertex;

	{  // just a block of code for hiding 'it_ver_swatch' 'it_seg_swatch'
	Mesh::Iterator it_ver_swatch = swatch .iterator ( tag::over_vertices );
	for ( it_ver_swatch .reset(); it_ver_swatch .in_range(); it_ver_swatch ++ )
	{	Cell S = * it_ver_swatch;
		tag::Util::insert_new_elem_in_map ( previous_ver_to_ver, { S, S } );  }
	Mesh::Iterator it_seg_swatch = swatch .iterator
		( tag::over_segments, tag::orientation_compatible_with_mesh );
	for ( it_seg_swatch .reset(); it_seg_swatch .in_range(); it_seg_swatch ++ )
	{	Cell ST = * it_seg_swatch;
		tag::Util::insert_new_elem_in_map ( previous_seg_to_seg, { ST, ST } );  }
	}  // just a block of code

	size_t counter = 0;
	while ( counter < max_counter )

	{	Cell PQ = track .cell_in_front_of ( P, tag::may_not_exist );
		if ( not PQ .exists() )  break;
		counter ++;
		Cell Q = PQ .tip();
		std::map < Cell, Cell > next_ver_to_ver, next_seg_to_seg;

		// build one segment for each vertex of 'swatch'
		// note : one of them will be PQ
		std::map < Cell, Cell > segments_by_ver;
		Mesh::Iterator it_ver_swatch = swatch .iterator ( tag::over_vertices );
		for ( it_ver_swatch .reset(); it_ver_swatch .in_range(); it_ver_swatch ++ )
		{	Cell S = * it_ver_swatch;
			Cell A = tag::Util::get_element_of_map ( previous_ver_to_ver, S, tag::surely_found );
			if ( S == common_vertex )
			{	assert ( A == P );
				tag::Util::insert_new_elem_in_map ( segments_by_ver, { P, PQ } );
				tag::Util::insert_new_elem_in_map ( next_ver_to_ver, { S, Q } );   }
			else
			{	Cell V ( tag::vertex );
				const typename Position::deform_type & def =
					tag::Util::get_element_of_const_map ( deform, Q, tag::surely_found );
				Position::place ( V, common_vertex, S, def );
				Cell AV ( tag::segment, A .reverse(), V );
				tag::Util::insert_new_elem_in_map ( segments_by_ver, { A, AV } );
				tag::Util::insert_new_elem_in_map ( next_ver_to_ver, { S, V } );                 }  }
		
		// for each segment of 'swatch', build one parallel segment and the corresponding rectangle
		Mesh::Iterator it_seg_swatch = swatch .iterator
			( tag::over_segments, tag::orientation_compatible_with_mesh );
		for ( it_seg_swatch .reset(); it_seg_swatch .in_range(); it_seg_swatch ++ )
		{	Cell ST = * it_seg_swatch;
			Cell S = ST .base() .reverse();
			Cell T = ST .tip();
			Cell A = tag::Util::get_element_of_map ( previous_ver_to_ver, S, tag::surely_found );
			Cell B = tag::Util::get_element_of_map ( previous_ver_to_ver, T, tag::surely_found );
			Cell AV = tag::Util::get_element_of_map ( segments_by_ver, A, tag::surely_found );
			Cell V = AV .tip();
			Cell BW = tag::Util::get_element_of_map ( segments_by_ver, B, tag::surely_found );
			Cell W = BW .tip();
			Cell AB = tag::Util::get_element_of_map ( previous_seg_to_seg, ST, tag::surely_found );
			assert ( AB .base() .reverse ( tag::surely_exists ) == A );
			assert ( AB .tip() == B );
			Cell VW ( tag::segment, V .reverse(), W );
			tag::Util::insert_new_elem_in_map ( next_seg_to_seg, { ST, VW } );
			Cell AVWB ( tag::quadrangle, AV, VW, BW .reverse(), AB .reverse() );
			// 'AB' may already have a reverse, in one case :
			// if it belongs to 'swatch' and, incidentally, has a reverse
			AVWB .add_to ( result );                                                                 }

		P = Q;
		previous_ver_to_ver .swap ( next_ver_to_ver );
		previous_seg_to_seg .swap ( next_seg_to_seg );

	}  // end of  while loop

	assert ( counter == max_counter );
	
}  // end of  extrude_forward_1Dtrack_1Dswatch


inline void add_to_map       // hidden in anonymous namespace
( const Cell & V, const Cell & S,
  const std::map < Cell, std::map < Cell, Cell > > ::iterator it_find_P_cll,
  const std::map < Cell, std::map < Cell, Cell > > & cell_in_track_cell_in_swatch )
// funny thing, last argument is modified indirectly, through 'it_find_P_cll'

{	if ( it_find_P_cll == cell_in_track_cell_in_swatch .end() )  return;
	std::map < Cell, Cell > & map_P = it_find_P_cll->second;
	std::map < Cell, Cell > ::iterator it_find_P_S = map_P .find ( S );
	if ( it_find_P_S != map_P .end() )
	{	Cell & vertex = it_find_P_S->second;
		assert ( not vertex .exists() );
		vertex = V;                           }                            }


inline void add_to_map       // hidden in anonymous namespace
( Cell & VA, const Cell & PQ, const Cell & S,
  const std::vector < std::map < Mesh, std::map < Cell, Mesh > > ::iterator > vec_it_find_PQ_cll,
  const std::map < Mesh, std::map < Cell, Mesh > > & mesh_in_track_cell_in_swatch                )
// funny thing, last argument is modified indirectly, through 'it_find_PQ_cll'

{	std::vector < std::map < Mesh, std::map < Cell, Mesh > > ::iterator > ::const_iterator it_vec;
	for ( it_vec = vec_it_find_PQ_cll .begin(); it_vec != vec_it_find_PQ_cll .end(); it_vec ++ )
	{	std::map < Mesh, std::map < Cell, Mesh > > ::iterator it_find_PQ_cll = * it_vec;
		assert ( it_find_PQ_cll != mesh_in_track_cell_in_swatch .end() );
		Mesh above_PQ = it_find_PQ_cll->first;  // submesh of 'track'
		assert ( PQ .belongs_to ( above_PQ, tag::orientation_compatible_with_mesh ) );
		std::map < Cell, Mesh > & map_above_PQ = it_find_PQ_cll->second;
		for ( std::map < Cell, Mesh > ::iterator it_map_above_PQ = map_above_PQ .begin();
		      it_map_above_PQ != map_above_PQ .end(); it_map_above_PQ ++                  )
		{	Cell SS = it_map_above_PQ->first;
			if ( S != SS )  continue;
			Mesh & piece_of_track_times_S = it_map_above_PQ->second;
			VA .add_to ( piece_of_track_times_S, tag::do_not_bother, tag::keep_as_first_ver );  }  }  }
			// if 'piece_of_track_times_S' is Mesh::Fuzzy,
			// the above is equivalent to  VA .add_to ( piece_of_track_times_S )


inline void add_to_map       // hidden in anonymous namespace
( Cell & VW, const Cell & ST,
  const std::map < Cell, std::map < Mesh, Mesh > > ::iterator it_find_P_msh,
  const std::map < Cell, std::map < Mesh, Mesh > > & cell_in_track_mesh_in_swatch )
// funny thing, last argument is modified indirectly, through 'it_find_P_msh'

{	if ( it_find_P_msh == cell_in_track_mesh_in_swatch .end() )  return;
	std::map < Mesh, Mesh > & map_P = it_find_P_msh->second;
	Cell::Iterator it_above_ST = ST .iterator
		( tag::over_meshes_above, tag::of_same_dim,
		  tag::orientation_compatible_with_cell, tag::do_not_build_cells );
	// may return negative meshes, does not build reverse cells
	for ( it_above_ST .reset(); it_above_ST .in_range(); it_above_ST ++ )
	{	Mesh msh = * it_above_ST;  // submesh of 'swatch'
		std::map < Mesh, Mesh > ::iterator it_find_P_ST = map_P .find ( msh );
		if ( it_find_P_ST != map_P .end() )
		{	Mesh & msh_extr = it_find_P_ST->second;
			VW .add_to ( msh_extr, tag::do_not_bother, tag::keep_as_first_ver );  }  }  }
			// if 'msh_extr' is Mesh::Fuzzy,
			// the above is equivalent to  VW .add_to ( msh_extr )


inline void add_to_map       // hidden in anonymous namespace
( Cell & prod_cll, const Cell & PQ, const Cell & ST,
  const std::vector < std::map < Mesh, std::map < Mesh, Mesh > > ::iterator > vec_it_find_PQ_msh,
  const std::map < Mesh, std::map < Mesh, Mesh > > & mesh_in_track_mesh_in_swatch                )
// funny thing, last argument is modified indirectly, through 'it_find_PQ_msh'

{	std::vector < std::map < Mesh, std::map < Mesh, Mesh > > ::iterator > ::const_iterator it_vec;
	for ( it_vec = vec_it_find_PQ_msh .begin(); it_vec != vec_it_find_PQ_msh .end(); it_vec ++ )
	{	std::map < Mesh, std::map < Mesh, Mesh > > ::iterator it_find_PQ_msh = * it_vec;
		assert ( it_find_PQ_msh != mesh_in_track_mesh_in_swatch .end() );
		Mesh above_PQ = it_find_PQ_msh->first;  // submesh of 'track'
		assert ( PQ .belongs_to ( above_PQ, tag::orientation_compatible_with_mesh ) );
		std::map < Mesh, Mesh > & map_above_PQ = it_find_PQ_msh->second;
		Cell::Iterator it_above_ST = ST .iterator
				( tag::over_meshes_above, tag::of_same_dim,
				  tag::orientation_compatible_with_cell, tag::do_not_build_cells );
		// may return negative meshes, does not build reverse cells
		for ( it_above_ST .reset(); it_above_ST .in_range(); it_above_ST ++ )
		{	const Mesh & msh = * it_above_ST;  // submesh of 'swatch'
			std::map < Mesh, Mesh > ::iterator it_find_PQ_ST = map_above_PQ .find ( msh );
			if ( it_find_PQ_ST != map_above_PQ .end() )
			{	Mesh & piece_of_track_times_piece_of_swatch = it_find_PQ_ST->second;
				prod_cll .add_to ( piece_of_track_times_piece_of_swatch );            }      }  }      }


template < class Position >
void extrude_backwards_1Dtrack_1Dswatch       // hidden in anonymous namespace
( const Mesh & swatch, const Mesh & track,
  const Mesh::Composite & comp_swatch, const Mesh::Composite & comp_track,
  const std::map < Cell, typename Position::deform_type > & deform, const Cell & common_vertex,
  Cell & missing_seg,
  std::map < Cell, Cell > & previous_ver_to_ver, std::map < Cell, Cell > & previous_seg_to_seg,
  size_t max_counter,
  std::map < Cell, std::map < Cell, Cell > > & cell_in_track_cell_in_swatch,
  std::map < Cell, std::map < Mesh, Mesh > > & cell_in_track_mesh_in_swatch,
  std::map < Mesh, std::map < Cell, Mesh > > & mesh_in_track_cell_in_swatch,
  std::map < Mesh, std::map < Mesh, Mesh > > & mesh_in_track_mesh_in_swatch                    )
  
// build the extruded mesh, starting at the common vertex and advancing in one direction
// (backwards along the track)

// class Position  could be 'Translation2D::***'          'RotoTranslation2D::***'
//                          'Translation3D::***::Track1D' 'RotoTranslation3D::***::Track1D'
	
// 'swatch' may be open chain or closed loop
// it may even be disconnected
// that's why we avoid using 'first_vertex' and 'last_vertex'
// we also avoid ordered iterators

// returns also 'missing_seg', the segment after the process has stopped
// only relevant if 'track' is closed loop

// 'previous_ver_to_ver' 'previous_seg_to_seg' useful after return
// (only if 'track' is closed) for closing the extruded mesh
// if 'track' is open, they are discarded

// a simpler version of this function can be found above, indended at extruding meshes (not composite)

{	assert ( common_vertex .is_positive() );
	assert ( common_vertex .belongs_to ( track ) );
	assert ( common_vertex .belongs_to ( swatch ) );

	Cell Q = common_vertex;

	{  // just a block of code for hiding 'it_ver_swatch' 'it_seg_swatch'
	Mesh::Iterator it_ver_swatch = swatch .iterator ( tag::over_vertices );
	for ( it_ver_swatch .reset(); it_ver_swatch .in_range(); it_ver_swatch ++ )
	{	Cell S = * it_ver_swatch;
		tag::Util::insert_new_elem_in_map ( previous_ver_to_ver, { S, S } );  }
	Mesh::Iterator it_seg_swatch = swatch .iterator
		( tag::over_segments, tag::orientation_compatible_with_mesh );
	for ( it_seg_swatch .reset(); it_seg_swatch .in_range(); it_seg_swatch ++ )
	{	Cell ST = * it_seg_swatch;
		tag::Util::insert_new_elem_in_map ( previous_seg_to_seg, { ST, ST } );  }
	}  // just a block of code

	size_t counter = 0;
	while ( counter < max_counter )

	{	Cell PQ = track .cell_behind ( Q, tag::may_not_exist );
		if ( not PQ .exists() )  break;
		
		counter ++;
		Cell P = PQ .base() .reverse ( tag::surely_exists );
		std::map < Cell, std::map < Cell, Cell > > ::iterator it_find_P_cll =
			cell_in_track_cell_in_swatch .find ( P );
		std::map < Cell, std::map < Mesh, Mesh > > ::iterator it_find_P_msh =
			cell_in_track_mesh_in_swatch .find ( P );
		std::vector < std::map < Mesh, std::map < Cell, Mesh > > ::iterator > vec_it_find_PQ_cll;
		std::vector < std::map < Mesh, std::map < Mesh, Mesh > > ::iterator > vec_it_find_PQ_msh;
		Cell::Iterator it_above_PQ = PQ .iterator
			( tag::over_meshes_above, tag::of_same_dim,
			  tag::orientation_compatible_with_cell, tag::do_not_build_cells );
		// may return negative meshes, does not build reverse cells
		for ( it_above_PQ .reset(); it_above_PQ .in_range(); it_above_PQ ++ )
		{	Mesh msh = * it_above_PQ;
			std::map < Mesh, std::map < Cell, Mesh > > ::iterator
				it_find_PQ_cll = mesh_in_track_cell_in_swatch .find ( msh );
			if ( it_find_PQ_cll != mesh_in_track_cell_in_swatch .end() )
				vec_it_find_PQ_cll .push_back ( it_find_PQ_cll );
			std::map < Mesh, std::map < Mesh, Mesh > > ::iterator
				it_find_PQ_msh = mesh_in_track_mesh_in_swatch .find ( msh );
			if ( it_find_PQ_msh != mesh_in_track_mesh_in_swatch .end() )
				vec_it_find_PQ_msh .push_back ( it_find_PQ_msh );             }

		std::map < Cell, Cell > next_ver_to_ver, next_seg_to_seg;

		// build one segment for each vertex of 'swatch'
		// note : one of them will be PQ
		std::map < Cell, Cell > segments_by_ver;
		Mesh::Iterator it_ver_swatch = swatch .iterator ( tag::over_vertices );
		for ( it_ver_swatch .reset(); it_ver_swatch .in_range(); it_ver_swatch ++ )
		{	Cell S = * it_ver_swatch;
			Cell A = tag::Util::get_element_of_map ( previous_ver_to_ver, S, tag::surely_found );
			Cell V ( tag::non_existent ), VA ( tag::non_existent );
			if ( S == common_vertex )
			{	assert ( A == Q );
				V = P;  VA = PQ;    }
			else
			{	V = Cell ( tag::vertex );
				const typename Position::deform_type & def =
					tag::Util::get_element_of_const_map ( deform, P, tag::surely_found );
				Position::place ( V, common_vertex, S, def );  // V is sort of (P,S)
				VA = Cell ( tag::segment, V .reverse(), A );                              }
			assert ( V .exists() );
			assert ( VA .exists() );
			tag::Util::insert_new_elem_in_map ( segments_by_ver, { A, VA } );
			tag::Util::insert_new_elem_in_map ( next_ver_to_ver, { S, V } );
			add_to_map ( V, S, it_find_P_cll, cell_in_track_cell_in_swatch );
			add_to_map ( VA, PQ, S, vec_it_find_PQ_cll, mesh_in_track_cell_in_swatch );            }
		 
		// for each segment of 'swatch', build one parallel segment and the corresponding rectangle
		Mesh::Iterator it_seg_swatch = swatch .iterator
			( tag::over_segments, tag::orientation_compatible_with_mesh );
		for ( it_seg_swatch .reset(); it_seg_swatch .in_range(); it_seg_swatch ++ )
		{	Cell ST = * it_seg_swatch;
			Cell S = ST .base() .reverse();
			Cell T = ST .tip();
			Cell A = tag::Util::get_element_of_map ( previous_ver_to_ver, S, tag::surely_found );
			Cell B = tag::Util::get_element_of_map ( previous_ver_to_ver, T, tag::surely_found );
			Cell VA = tag::Util::get_element_of_map ( segments_by_ver, A, tag::surely_found );
			Cell V = VA .base() .reverse ( tag::surely_exists );
			Cell WB = tag::Util::get_element_of_map ( segments_by_ver, B, tag::surely_found );
			Cell W = WB .base() .reverse ( tag::surely_exists );
			Cell AB = tag::Util::get_element_of_map ( previous_seg_to_seg, ST, tag::surely_found );
			assert ( AB .base() .reverse ( tag::surely_exists ) == A );
			assert ( AB .tip() == B );
			Cell VW ( tag::segment, V .reverse(), W );
			tag::Util::insert_new_elem_in_map ( next_seg_to_seg, { ST, VW } );
			add_to_map ( VW, ST, it_find_P_msh, cell_in_track_mesh_in_swatch );
			Cell ABWV ( tag::quadrangle, AB, WB .reverse(),
			            VW .reverse ( tag::does_not_exist, tag::build_now ), VA );
			add_to_map ( ABWV, PQ, ST, vec_it_find_PQ_msh, mesh_in_track_mesh_in_swatch );           }

		Q = P;
		previous_ver_to_ver .swap ( next_ver_to_ver );
		previous_seg_to_seg .swap ( next_seg_to_seg );

	}  // end of  while loop

	assert ( counter == max_counter );
	missing_seg = track .cell_behind ( Q, tag::may_not_exist );
	
}  // end of  extrude_backwards_1Dtrack_1Dswatch

//------------------------------------------------------------------------------------------------------//


template < class Position >
void extrude_forward_1Dtrack_1Dswatch       // hidden in anonymous namespace
( const Mesh & swatch, const Mesh & track,
  const Mesh::Composite & comp_swatch, const Mesh::Composite & comp_track,
  const std::map < Cell, typename Position::deform_type > & deform, const Cell & common_vertex,
  std::map < Cell, Cell > & previous_ver_to_ver, std::map < Cell, Cell > & previous_seg_to_seg,
  size_t max_counter,
  std::map < Cell, std::map < Cell, Cell > > & cell_in_track_cell_in_swatch,
  std::map < Cell, std::map < Mesh, Mesh > > & cell_in_track_mesh_in_swatch,
  std::map < Mesh, std::map < Cell, Mesh > > & mesh_in_track_cell_in_swatch,
  std::map < Mesh, std::map < Mesh, Mesh > > & mesh_in_track_mesh_in_swatch                    )
  
// build the extruded mesh, starting at the common vertex and advancing in one direction
// (forward along the track)

// class Position  could be 'Translation2D::***'          'RotoTranslation2D::***'
//                          'Translation3D::***::Track1D' 'RotoTranslation3D::***::Track1D'
	
// 'swatch' may be open chain or closed loop
// it may even be disconnected
// that's why we avoid using 'first_vertex' and 'last_vertex'
// we also avoid ordered iterators

// 'previous_ver_to_ver' 'previous_seg_to_seg' useful after return
// (only if 'track' is closed) for closing the extruded mesh
// if 'track' is open, they are discarded

// a simpler version of this function can be found above, indended at extruding meshes (not composite)

{	assert ( common_vertex .is_positive() );
	assert ( common_vertex .belongs_to ( track ) );
	assert ( common_vertex .belongs_to ( swatch ) );

	Cell P = common_vertex;

	{  // just a block of code for hiding 'it_ver_swatch' 'it_seg_swatch'
	Mesh::Iterator it_ver_swatch = swatch .iterator ( tag::over_vertices );
	for ( it_ver_swatch .reset(); it_ver_swatch .in_range(); it_ver_swatch ++ )
	{	Cell S = * it_ver_swatch;
		tag::Util::insert_new_elem_in_map ( previous_ver_to_ver, { S, S } );  }
	Mesh::Iterator it_seg_swatch = swatch .iterator
		( tag::over_segments, tag::orientation_compatible_with_mesh );
	for ( it_seg_swatch .reset(); it_seg_swatch .in_range(); it_seg_swatch ++ )
	{	Cell ST = * it_seg_swatch;
		tag::Util::insert_new_elem_in_map ( previous_seg_to_seg, { ST, ST } );  }
	}  // just a block of code

	size_t counter = 0;
	while ( counter < max_counter )

	{	Cell PQ = track .cell_in_front_of ( P, tag::may_not_exist );
		if ( not PQ .exists() )  break;
		counter ++;
		Cell Q = PQ .tip();
		std::map < Cell, std::map < Cell, Cell > > ::iterator it_find_Q_cll =
			cell_in_track_cell_in_swatch .find (Q);
		std::map < Cell, std::map < Mesh, Mesh > > ::iterator it_find_Q_msh =
			cell_in_track_mesh_in_swatch .find (Q);
		std::vector < std::map < Mesh, std::map < Cell, Mesh > > ::iterator > vec_it_find_PQ_cll;
		std::vector < std::map < Mesh, std::map < Mesh, Mesh > > ::iterator > vec_it_find_PQ_msh;
		Cell::Iterator it_above_PQ = PQ .iterator
			( tag::over_meshes_above, tag::of_same_dim,
			  tag::orientation_compatible_with_cell, tag::do_not_build_cells );
		// may return negative meshes, does not build reverse cells
		for ( it_above_PQ .reset(); it_above_PQ .in_range(); it_above_PQ ++ )
		{	Mesh msh = * it_above_PQ;
			std::map < Mesh, std::map < Cell, Mesh > > ::iterator
				it_find_PQ_cll = mesh_in_track_cell_in_swatch .find ( msh );
			if ( it_find_PQ_cll != mesh_in_track_cell_in_swatch .end() )
				vec_it_find_PQ_cll .push_back ( it_find_PQ_cll );
			std::map < Mesh, std::map < Mesh, Mesh > > ::iterator
				it_find_PQ_msh = mesh_in_track_mesh_in_swatch .find ( msh );
			if ( it_find_PQ_msh != mesh_in_track_mesh_in_swatch .end() )
				vec_it_find_PQ_msh .push_back ( it_find_PQ_msh );             }
		std::map < Cell, Cell > next_ver_to_ver, next_seg_to_seg;

		// build one segment for each vertex of 'swatch'
		// note : one of them will be PQ
		std::map < Cell, Cell > segments_by_ver;
		Mesh::Iterator it_ver_swatch = swatch .iterator ( tag::over_vertices );
		for ( it_ver_swatch .reset(); it_ver_swatch .in_range(); it_ver_swatch ++ )
		{	Cell S = * it_ver_swatch;
			Cell A = tag::Util::get_element_of_map ( previous_ver_to_ver, S, tag::surely_found );
			Cell V ( tag::non_existent ), AV ( tag::non_existent );
			if ( S == common_vertex )
			{	assert ( A == P );
				V = Q;  AV = PQ;    }
			else
			{	V = Cell ( tag::vertex );
				const typename Position::deform_type & def =
					tag::Util::get_element_of_const_map ( deform, Q, tag::surely_found );
				Position::place ( V, common_vertex, S, def );  // V is sort of (P,S)
				AV = Cell ( tag::segment, A .reverse(), V );                              }
			assert ( V .exists() );
			assert ( AV .exists() );
			tag::Util::insert_new_elem_in_map ( segments_by_ver, { A, AV } );
			tag::Util::insert_new_elem_in_map ( next_ver_to_ver, { S, V } );
			add_to_map ( V, S, it_find_Q_cll, cell_in_track_cell_in_swatch );
			add_to_map ( AV, PQ, S, vec_it_find_PQ_cll, mesh_in_track_cell_in_swatch );            }
		 
		// for each segment of 'swatch', build one parallel segment and the corresponding rectangle
		Mesh::Iterator it_seg_swatch = swatch .iterator
			( tag::over_segments, tag::orientation_compatible_with_mesh );
		for ( it_seg_swatch .reset(); it_seg_swatch .in_range(); it_seg_swatch ++ )
		{	Cell ST = * it_seg_swatch;
			Cell S = ST .base() .reverse();
			Cell T = ST .tip();
			Cell A = tag::Util::get_element_of_map ( previous_ver_to_ver, S, tag::surely_found );
			Cell B = tag::Util::get_element_of_map ( previous_ver_to_ver, T, tag::surely_found );
			Cell AV = tag::Util::get_element_of_map ( segments_by_ver, A, tag::surely_found );
			Cell V = AV .tip();
			Cell BW = tag::Util::get_element_of_map ( segments_by_ver, B, tag::surely_found );
			Cell W = BW .tip();
			Cell AB = tag::Util::get_element_of_map ( previous_seg_to_seg, ST, tag::surely_found );
			assert ( AB .base() .reverse ( tag::surely_exists ) == A );
			assert ( AB .tip() == B );
			Cell VW ( tag::segment, V .reverse(), W );
			tag::Util::insert_new_elem_in_map ( next_seg_to_seg, { ST, VW } );
			add_to_map ( VW, ST, it_find_Q_msh, cell_in_track_mesh_in_swatch );
			Cell AVWB ( tag::quadrangle, AV, VW, BW .reverse(), AB .reverse() );
			// 'AB' may already have a reverse, in one case :
			// if it belongs to 'swatch' and, incidentally, has a reverse
			add_to_map ( AVWB, PQ, ST, vec_it_find_PQ_msh, mesh_in_track_mesh_in_swatch );           }

		P = Q;
		previous_ver_to_ver .swap ( next_ver_to_ver );
		previous_seg_to_seg .swap ( next_seg_to_seg );

	}  // end of  while loop

	assert ( counter == max_counter );
	
}  // end of  extrude_forward_1Dtrack_1Dswatch

//------------------------------------------------------------------------------------------------------//


template < class Position >
void extrude_backwards_1Dtrack_2Dswatch       // hidden in anonymous namespace
( Mesh & result, const Mesh & swatch, const Mesh & track,
  const std::map < Cell, typename Position::deform_type > & deform, const Cell & common_vertex,
  std::map < Cell, Cell > & previous_ver_to_ver, std::map < Cell, Cell > & previous_seg_to_seg,
  std::map < Cell, Cell > & previous_tri_to_tri, size_t max_counter                            )

// what we call 'tri' could be a square or some other polygon

// build the extruded mesh, starting at the common vertex and advancing in one direction
// (backwards along the track)

// class Position  could be 'Translation3D::***::Track1D' 'RotoTranslation3D::***::Track1D'
	
// 'previous_ver_to_ver' 'previous_seg_to_seg' 'previous_tri_to_tri' useful after return
// (only if 'track' is closed) for closing the extruded mesh
// if 'track' is open, they are discarded
 
{	assert ( common_vertex .is_positive() );
	assert ( common_vertex .belongs_to ( track ) );
	assert ( common_vertex .belongs_to ( swatch ) );

	Cell Q = common_vertex;

	{  // just a block of code for hiding 'it_ver_swatch' 'it_seg_swatch' 'it_tri_swatch'
	Mesh::Iterator it_ver_swatch = swatch .iterator ( tag::over_vertices );
	for ( it_ver_swatch .reset(); it_ver_swatch .in_range(); it_ver_swatch ++ )
	{	Cell S = * it_ver_swatch;
		tag::Util::insert_new_elem_in_map ( previous_ver_to_ver, { S, S } );  }
	Mesh::Iterator it_seg_swatch = swatch .iterator ( tag::over_segments );
	for ( it_seg_swatch .reset(); it_seg_swatch .in_range(); it_seg_swatch ++ )
	{	Cell ST = * it_seg_swatch;
		tag::Util::insert_new_elem_in_map ( previous_seg_to_seg, { ST, ST } );
		tag::Util::insert_new_elem_in_map ( previous_seg_to_seg, { ST .reverse(), ST .reverse() } );  }
	Mesh::Iterator it_tri_swatch = swatch .iterator
		( tag::over_cells_of_max_dim, tag::orientation_compatible_with_mesh );
	// what we call 'tri' could be a square or some other polygon
	for ( it_tri_swatch .reset(); it_tri_swatch .in_range(); it_tri_swatch ++ )
	{	Cell polygon = * it_tri_swatch;
		tag::Util::insert_new_elem_in_map ( previous_tri_to_tri, { polygon, polygon } );  }
	}  // just a block of code

	size_t counter = 0;
	while ( counter < max_counter )

	{	Cell PQ = track .cell_behind ( Q, tag::may_not_exist );
		if ( not PQ .exists() )  break;
		counter ++;
		Cell P = PQ .base() .reverse ( tag::surely_exists );
		std::map < Cell, Cell > next_ver_to_ver, next_seg_to_seg, next_tri_to_tri;

		// build one segment for each vertex of 'swatch'
		// note : one of them will be PQ
		std::map < Cell, Cell > segments_by_ver;
		Mesh::Iterator it_ver_swatch = swatch .iterator ( tag::over_vertices );
		for ( it_ver_swatch .reset(); it_ver_swatch .in_range(); it_ver_swatch ++ )
		{	Cell S = * it_ver_swatch;
			Cell A = tag::Util::get_element_of_map ( previous_ver_to_ver, S, tag::surely_found );
			if ( S == common_vertex )
			{	assert ( A == Q );
				tag::Util::insert_new_elem_in_map ( segments_by_ver, { Q, PQ } );
				tag::Util::insert_new_elem_in_map ( next_ver_to_ver, { S, P } );   }
			else
			{	Cell V ( tag::vertex );
				const typename Position::deform_type & def =
					tag::Util::get_element_of_const_map ( deform, P, tag::surely_found );
				Position::place ( V, common_vertex, S, def );
				Cell VA ( tag::segment, V .reverse(), A );
				tag::Util::insert_new_elem_in_map ( segments_by_ver, { A, VA } );
				tag::Util::insert_new_elem_in_map ( next_ver_to_ver, { S, V } );                 }         }

		// build one rectangle for each segment of 'swatch'
		std::map < Cell, Cell > rectangles_by_seg;
		Mesh::Iterator it_seg_swatch = swatch .iterator ( tag::over_segments );
		for ( it_seg_swatch .reset(); it_seg_swatch .in_range(); it_seg_swatch ++ )
		{	Cell ST = * it_seg_swatch;
			Cell S = ST .base() .reverse();
			Cell T = ST .tip();
			Cell AB = tag::Util::get_element_of_map ( previous_seg_to_seg, ST, tag::surely_found );
			Cell A = AB .base() .reverse();
			Cell B = AB .tip();
			assert ( tag::Util::get_element_of_map
			         ( previous_ver_to_ver, S, tag::surely_found ) == A );
			assert ( tag::Util::get_element_of_map
			         ( previous_ver_to_ver, T, tag::surely_found ) == B );
			Cell VA = tag::Util::get_element_of_map ( segments_by_ver, A, tag::surely_found );
			Cell V = VA .base() .reverse ( tag::surely_exists );
			Cell WB = tag::Util::get_element_of_map ( segments_by_ver, B, tag::surely_found );
			Cell W = WB .base() .reverse ( tag::surely_exists );
			assert ( tag::Util::get_element_of_map ( next_ver_to_ver, S, tag::surely_found ) == V );
			assert ( tag::Util::get_element_of_map ( next_ver_to_ver, T, tag::surely_found ) == W );
			Cell VW ( tag::segment, V .reverse(), W );
			Cell ABWV ( tag::rectangle, AB, WB .reverse(),
			            VW .reverse ( tag::does_not_exist, tag::build_now ), VA );
			tag::Util::insert_new_elem_in_map ( next_seg_to_seg, { ST, VW } );
			tag::Util::insert_new_elem_in_map ( next_seg_to_seg, { ST .reverse(), VW .reverse() } );
			tag::Util::insert_new_elem_in_map ( rectangles_by_seg, { AB, ABWV } );
			tag::Util::insert_new_elem_in_map ( rectangles_by_seg,
				{ AB .reverse(), ABWV .reverse ( tag::does_not_exist, tag::build_now ) } );             }

		// for each square of 'swatch', build one parallel square and the corresponding cube
		// for each triangle of 'swatch', build one parallel triangle and the corresponding prism
		Mesh::Iterator it_tri_swatch = swatch .iterator
			( tag::over_cells_of_max_dim, tag::orientation_compatible_with_mesh );
		// what we call 'tri' could be a square or some other polygon
		for ( it_tri_swatch .reset(); it_tri_swatch .in_range(); it_tri_swatch ++ )
		{	Cell polygon_ST = * it_tri_swatch;
			Cell polygon_AB = tag::Util::get_element_of_map
				( previous_tri_to_tri, polygon_ST, tag::surely_found );
			Mesh bdry_ST = polygon_ST .boundary();
			const size_t n = bdry_ST .number_of ( tag::cells_of_max_dim );
			assert ( n == polygon_AB .boundary() .number_of ( tag::cells_of_max_dim ) );
			Mesh::Connected::OneDim * bdry_VW_ptr =   // boudary of the future polygon_VW
				new Mesh::Connected::OneDim ( tag::with, n, tag::segments,
				                              tag::one_dummy_wrapper      );
			Mesh bdry_VW ( tag::whose_core_is, bdry_VW_ptr, tag::move, tag::is_positive );
			Mesh bdry_prism ( tag::whose_core_is,  // boudary of the future prism, or cube
			                  new Mesh::Fuzzy ( tag::of_dimension, 3, tag::minus_one,
			                                    tag::one_dummy_wrapper               ),
			                  tag::move, tag::is_positive                              );
			polygon_AB .core->add_to_mesh ( bdry_prism .core );
			Cell U ( tag::non_existent );
			// we need a vertex U, any one, in the boundary of the future polygon_VW
			Mesh::Iterator it_segs_bdry = bdry_ST .iterator
				( tag::over_cells_of_max_dim, tag::orientation_compatible_with_mesh,
				  tag::require_order                                                );
			for ( it_segs_bdry .reset(); it_segs_bdry .in_range(); it_segs_bdry ++ )
			{	Cell ST = * it_segs_bdry;
				Cell AB = tag::Util::get_element_of_map
					( previous_seg_to_seg, ST, tag::surely_found );
				Cell VW = tag::Util::get_element_of_map
					( next_seg_to_seg, ST, tag::surely_found );
				U = VW .tip();
				VW .core->add_to_mesh ( bdry_VW .core, tag::do_not_bother );
				Cell ABWV = tag::Util::get_element_of_map
					( rectangles_by_seg, AB, tag::surely_found );
				ABWV .reverse ( tag::surely_exists ) .core->add_to_mesh ( bdry_prism .core );  }

			bdry_VW_ptr->first_ver = U;
			bdry_VW_ptr->last_ver  = U;
			Cell polygon_VW ( tag::whose_core_is,
				new Cell::Positive::HighDim
					( tag::whose_boundary_is, bdry_VW, tag::one_dummy_wrapper ),
				tag::move                                                      );
			tag::Util::insert_new_elem_in_map ( next_tri_to_tri, { polygon_ST, polygon_VW } );

			polygon_VW .reverse ( tag::does_not_exist, tag::build_now ) .core
			  ->add_to_mesh ( bdry_prism .core );
			Cell prism ( tag::whose_core_is,
				new Cell::Positive::HighDim
					( tag::whose_boundary_is, bdry_prism, tag::one_dummy_wrapper ),
				tag::move                                                         );
			prism .add_to ( result );                                                            }

		Q = P;
		previous_ver_to_ver .swap ( next_ver_to_ver );
		previous_seg_to_seg .swap ( next_seg_to_seg );
		previous_tri_to_tri .swap ( next_tri_to_tri );

	}  // end of  while loop

	assert ( counter == max_counter );
	
}  // end of  extrude_backwards_1Dtrack_2Dswatch

//------------------------------------------------------------------------------------------------------//


template < class Position >
void extrude_forward_1Dtrack_2Dswatch       // hidden in anonymous namespace
( Mesh & result, const Mesh & swatch, const Mesh & track,
  const std::map < Cell, typename Position::deform_type > & deform, const Cell & common_vertex,
  std::map < Cell, Cell > & previous_ver_to_ver, std::map < Cell, Cell > & previous_seg_to_seg,
  std::map < Cell, Cell > & previous_tri_to_tri, size_t max_counter                            )

// what we call 'tri' could be a square or some other polygon

// build the extruded mesh, starting at the common vertex and advancing in one direction
// (forward along the track)

// class Position  could be 'Translation3D::***::Track1D' 'RotoTranslation3D::***::Track1D'
	
// 'previous_ver_to_ver' 'previous_seg_to_seg' 'previous_tri_to_tri' useful after return
// (only if 'track' is closed) for closing the extruded mesh
// if 'track' is open, they are discarded
 
{	assert ( common_vertex .is_positive() );
	assert ( common_vertex .belongs_to ( track ) );
	assert ( common_vertex .belongs_to ( swatch ) );

	Cell P = common_vertex;

	{  // just a block of code for hiding 'it_ver_swatch' 'it_seg_swatch' 'it_tri_swatch'
	Mesh::Iterator it_ver_swatch = swatch .iterator ( tag::over_vertices );
	for ( it_ver_swatch .reset(); it_ver_swatch .in_range(); it_ver_swatch ++ )
	{	Cell S = * it_ver_swatch;
		tag::Util::insert_new_elem_in_map ( previous_ver_to_ver, { S, S } );  }
	Mesh::Iterator it_seg_swatch = swatch .iterator ( tag::over_segments );
	for ( it_seg_swatch .reset(); it_seg_swatch .in_range(); it_seg_swatch ++ )
	{	Cell ST = * it_seg_swatch;
		tag::Util::insert_new_elem_in_map ( previous_seg_to_seg, { ST, ST } );
		tag::Util::insert_new_elem_in_map ( previous_seg_to_seg, { ST .reverse(), ST .reverse() } );  }
	Mesh::Iterator it_tri_swatch = swatch .iterator
		( tag::over_cells_of_max_dim, tag::orientation_compatible_with_mesh );
	// what we call 'tri' could be a square or some other polygon
	for ( it_tri_swatch .reset(); it_tri_swatch .in_range(); it_tri_swatch ++ )
	{	Cell polygon = * it_tri_swatch;
		tag::Util::insert_new_elem_in_map ( previous_tri_to_tri, { polygon, polygon } );  }
	}  // just a block of code

	size_t counter = 0;
	while ( counter < max_counter )

	{	Cell PQ = track .cell_in_front_of ( P, tag::may_not_exist );
		if ( not PQ .exists() )  break;
		counter ++;
		Cell Q = PQ .tip();
		std::map < Cell, Cell > next_ver_to_ver, next_seg_to_seg, next_tri_to_tri;

		// build one segment for each vertex of 'swatch'
		// note : one of them will be PQ
		std::map < Cell, Cell > segments_by_ver;
		Mesh::Iterator it_ver_swatch = swatch .iterator ( tag::over_vertices );
		for ( it_ver_swatch .reset(); it_ver_swatch .in_range(); it_ver_swatch ++ )
		{	Cell S = * it_ver_swatch;
			Cell A = tag::Util::get_element_of_map ( previous_ver_to_ver, S, tag::surely_found );
			if ( S == common_vertex )
			{	assert ( A == P );
				tag::Util::insert_new_elem_in_map ( segments_by_ver, { P, PQ } );
				tag::Util::insert_new_elem_in_map ( next_ver_to_ver, { S, Q } );   }
			else
			{	Cell V ( tag::vertex );
				const typename Position::deform_type & def =
					tag::Util::get_element_of_const_map ( deform, Q, tag::surely_found );
				Position::place ( V, common_vertex, S, def );
				Cell AV ( tag::segment, A .reverse(), V );
				tag::Util::insert_new_elem_in_map ( segments_by_ver, { A, AV } );
				tag::Util::insert_new_elem_in_map ( next_ver_to_ver, { S, V } );                 }         }

		// build one rectangle for each segment of 'swatch'
		std::map < Cell, Cell > rectangles_by_seg;
		Mesh::Iterator it_seg_swatch = swatch .iterator ( tag::over_segments );
		for ( it_seg_swatch .reset(); it_seg_swatch .in_range(); it_seg_swatch ++ )
		{	Cell ST = * it_seg_swatch;
			Cell S = ST .base() .reverse();
			Cell T = ST .tip();
			Cell AB = tag::Util::get_element_of_map ( previous_seg_to_seg, ST, tag::surely_found );
			Cell A = AB .base() .reverse();
			Cell B = AB .tip();
			assert ( tag::Util::get_element_of_map
			         ( previous_ver_to_ver, S, tag::surely_found ) == A );
			assert ( tag::Util::get_element_of_map
			         ( previous_ver_to_ver, T, tag::surely_found ) == B );
			Cell AV = tag::Util::get_element_of_map ( segments_by_ver, A, tag::surely_found );
			Cell V = AV .tip();
			Cell BW = tag::Util::get_element_of_map ( segments_by_ver, B, tag::surely_found );
			Cell W = BW .tip();
			assert ( tag::Util::get_element_of_map ( next_ver_to_ver, S, tag::surely_found ) == V );
			assert ( tag::Util::get_element_of_map ( next_ver_to_ver, T, tag::surely_found ) == W );
			Cell VW ( tag::segment, V .reverse(), W );
			Cell AVWB ( tag::quadrangle, AV, VW, BW .reverse(), AB .reverse() );
			tag::Util::insert_new_elem_in_map ( next_seg_to_seg, { ST, VW } );
			tag::Util::insert_new_elem_in_map ( next_seg_to_seg, { ST .reverse(), VW .reverse() } );
			tag::Util::insert_new_elem_in_map ( rectangles_by_seg, { AB, AVWB } );
			tag::Util::insert_new_elem_in_map ( rectangles_by_seg,
				{ AB .reverse(), AVWB .reverse ( tag::does_not_exist, tag::build_now ) } );             }

		// for each square of 'swatch', build one parallel square and the corresponding cube
		// for each triangle of 'swatch', build one parallel triangle and the corresponding prism
		Mesh::Iterator it_tri_swatch = swatch .iterator
			( tag::over_cells_of_max_dim, tag::orientation_compatible_with_mesh );
		// what we call 'tri' could be a square or some other polygon
		for ( it_tri_swatch .reset(); it_tri_swatch .in_range(); it_tri_swatch ++ )
		{	Cell polygon_ST = * it_tri_swatch;
			Cell polygon_AB = tag::Util::get_element_of_map
				( previous_tri_to_tri, polygon_ST, tag::surely_found );
			Mesh bdry_ST = polygon_ST .boundary();
			const size_t n = bdry_ST .number_of ( tag::cells_of_max_dim );
			assert ( n == polygon_AB .boundary() .number_of ( tag::cells_of_max_dim ) );
			Mesh::Connected::OneDim * bdry_VW_ptr =   // boudary of the future polygon_VW
				new Mesh::Connected::OneDim ( tag::with, n, tag::segments,
				                              tag::one_dummy_wrapper      );
			Mesh bdry_VW ( tag::whose_core_is, bdry_VW_ptr, tag::move, tag::is_positive );
			Mesh bdry_prism ( tag::whose_core_is,  // boudary of the future prism, or cube
			                  new Mesh::Fuzzy ( tag::of_dimension, 3, tag::minus_one,
			                                    tag::one_dummy_wrapper               ),
			                  tag::move, tag::is_positive                              );
			polygon_AB .reverse() .core->add_to_mesh ( bdry_prism .core );
			// 'polygon_AB' may already have a reverse, in one case :
			// if it belongs to 'swatch' and, incidentally, has a reverse
			Cell U ( tag::non_existent );
			// we need a vertex U, any one, in the boundary of the future polygon_VW
			Mesh::Iterator it_segs_bdry = bdry_ST .iterator
				( tag::over_cells_of_max_dim, tag::orientation_compatible_with_mesh,
				  tag::require_order                                                );
			for ( it_segs_bdry .reset(); it_segs_bdry .in_range(); it_segs_bdry ++ )
			{	Cell ST = * it_segs_bdry;
				Cell AB = tag::Util::get_element_of_map
					( previous_seg_to_seg, ST, tag::surely_found );
				Cell VW = tag::Util::get_element_of_map
					( next_seg_to_seg, ST, tag::surely_found );
				U = VW .tip();
				VW .core->add_to_mesh ( bdry_VW .core, tag::do_not_bother );
				Cell AVWB = tag::Util::get_element_of_map
					( rectangles_by_seg, AB, tag::surely_found );
				AVWB .reverse ( tag::surely_exists ) .core->add_to_mesh ( bdry_prism .core );  }

			bdry_VW_ptr->first_ver = U;
			bdry_VW_ptr->last_ver  = U;
			Cell polygon_VW ( tag::whose_core_is,
				new Cell::Positive::HighDim
					( tag::whose_boundary_is, bdry_VW, tag::one_dummy_wrapper ),
				tag::move                                                      );
			tag::Util::insert_new_elem_in_map ( next_tri_to_tri, { polygon_ST, polygon_VW } );

			polygon_VW .core->add_to_mesh ( bdry_prism .core );
			Cell prism ( tag::whose_core_is,
				new Cell::Positive::HighDim
					( tag::whose_boundary_is, bdry_prism, tag::one_dummy_wrapper ),
				tag::move                                                         );
			prism .add_to ( result );                                                            }

		P = Q;
		previous_ver_to_ver .swap ( next_ver_to_ver );
		previous_seg_to_seg .swap ( next_seg_to_seg );
		previous_tri_to_tri .swap ( next_tri_to_tri );

	}  // end of  while loop

	assert ( counter == max_counter );
	
}  // end of  extrude_forward_1Dtrack_2Dswatch

//------------------------------------------------------------------------------------------------------//


inline void close_extruded_1Dswatch   // hidden in anonymous namespace
( Mesh & result, const Mesh & swatch, const Mesh & track, const Cell & common_vertex,
  const std::map < Cell, Cell > & ver_to_ver, const std::map < Cell, Cell > & seg_to_seg )

// 'swatch' is either an open chain of segments or a closed loop
// it may even be disconnected
// that's why we avoid using 'first_vertex' and 'last_vertex'
// we also avoid ordered iterators

{	Cell P = common_vertex;
	Cell PQ = track .cell_in_front_of ( P, tag::surely_exists );
	
	// build one segment for each vertex of 'swatch'
	// note : one of them will be PQ
	std::map < Cell, Cell > segments_by_ver;
	Mesh::Iterator it_ver_swatch = swatch .iterator ( tag::over_vertices );
	for ( it_ver_swatch .reset(); it_ver_swatch .in_range(); it_ver_swatch ++ )
	{	Cell S = * it_ver_swatch;
		Cell V = tag::Util::get_element_of_const_map ( ver_to_ver, S, tag::surely_found );
		if ( S == common_vertex )
		{	assert ( V == PQ .tip() );
			tag::Util::insert_new_elem_in_map ( segments_by_ver, { P, PQ } );  }
		else
		{	Cell SV ( tag::segment, S .reverse(), V );
			tag::Util::insert_new_elem_in_map ( segments_by_ver, { S, SV } );  }                    }

	// build one rectangle for each segment of 'swatch'
	Mesh::Iterator it_seg_swatch = swatch .iterator ( tag::over_segments );
	for ( it_seg_swatch .reset(); it_seg_swatch .in_range(); it_seg_swatch ++ )
	{	Cell ST = * it_seg_swatch;
		Cell S = ST .base() .reverse();
		Cell T = ST .tip();
		Cell VW = tag::Util::get_element_of_const_map ( seg_to_seg, ST, tag::surely_found );
		Cell V = VW .base() .reverse();
		Cell W = VW .tip();
		assert ( tag::Util::get_element_of_const_map ( ver_to_ver, S, tag::surely_found ) == V );
		assert ( tag::Util::get_element_of_const_map ( ver_to_ver, T, tag::surely_found ) == W );
		Cell SV = tag::Util::get_element_of_const_map ( segments_by_ver, S, tag::surely_found );
		assert ( SV .base() .reverse ( tag::surely_exists ) == S );
		assert ( SV .tip() == V );
		Cell TW = tag::Util::get_element_of_const_map ( segments_by_ver, T, tag::surely_found );
		assert ( TW .base() .reverse ( tag::surely_exists ) == T );
		assert ( TW .tip() == W );
		assert ( tag::Util::get_element_of_const_map ( ver_to_ver, S, tag::surely_found ) == V );
		assert ( tag::Util::get_element_of_const_map ( ver_to_ver, T, tag::surely_found ) == W );
		Cell TSVW ( tag::rectangle, ST .reverse(), SV, VW, TW .reverse() );
		TSVW .add_to ( result );                                                                   }

}  // end of  close_extruded_1Dswatch

//------------------------------------------------------------------------------------------------------//


inline void close_extruded_1Dswatch   // hidden in anonymous namespace
( const Mesh & swatch, const Mesh & track, const Cell & common_vertex, const Cell & missing_seg,
  const std::map < Cell, Cell > & ver_to_ver, const std::map < Cell, Cell > & seg_to_seg,
  std::map < Cell, std::map < Cell, Cell > > & cell_in_track_cell_in_swatch,
  std::map < Cell, std::map < Mesh, Mesh > > & cell_in_track_mesh_in_swatch,
  std::map < Mesh, std::map < Cell, Mesh > > & mesh_in_track_cell_in_swatch,
  std::map < Mesh, std::map < Mesh, Mesh > > & mesh_in_track_mesh_in_swatch                       )

// 'swatch' is either an open chain of segments or a closed loop
// it may even be disconnected
// that's why we avoid using 'first_vertex' and 'last_vertex'
// we also avoid ordered iterators

{	Cell PQ = missing_seg;
	assert ( PQ .belongs_to ( track, tag::orientation_compatible_with_mesh ) );
	Cell P = PQ .base() .reverse ( tag::surely_exists );
	assert ( P == common_vertex );
	
	std::vector < std::map < Mesh, std::map < Cell, Mesh > > ::iterator > vec_it_find_PQ_cll;
	std::vector < std::map < Mesh, std::map < Mesh, Mesh > > ::iterator > vec_it_find_PQ_msh;
	Cell::Iterator it_above_PQ = PQ .iterator
		( tag::over_meshes_above, tag::of_same_dim,
		  tag::orientation_compatible_with_cell, tag::do_not_build_cells );
	// may return negative meshes, does not build reverse cells
	for ( it_above_PQ .reset(); it_above_PQ .in_range(); it_above_PQ ++ )
	{	Mesh msh = * it_above_PQ;
		std::map < Mesh, std::map < Cell, Mesh > > ::iterator
			it_find_PQ_cll = mesh_in_track_cell_in_swatch .find ( msh );
		if ( it_find_PQ_cll != mesh_in_track_cell_in_swatch .end() )
			vec_it_find_PQ_cll .push_back ( it_find_PQ_cll );
		std::map < Mesh, std::map < Mesh, Mesh > > ::iterator
			it_find_PQ_msh = mesh_in_track_mesh_in_swatch .find ( msh );
		if ( it_find_PQ_msh != mesh_in_track_mesh_in_swatch .end() )
			vec_it_find_PQ_msh .push_back ( it_find_PQ_msh );             }

	// build one segment for each vertex of 'swatch'
	// note : one of them will be PQ
	std::map < Cell, Cell > segments_by_ver;
	Mesh::Iterator it_ver_swatch = swatch .iterator ( tag::over_vertices );
	for ( it_ver_swatch .reset(); it_ver_swatch .in_range(); it_ver_swatch ++ )
	{	Cell S = * it_ver_swatch;
		Cell V = tag::Util::get_element_of_const_map ( ver_to_ver, S, tag::surely_found );
		Cell SV ( tag::non_existent );
		if ( S == common_vertex )
		{	assert ( V == PQ .tip() );
			SV = PQ;                    }
		else  SV = Cell ( tag::segment, S .reverse(), V );
		assert ( SV .exists() );
		tag::Util::insert_new_elem_in_map ( segments_by_ver, { S, SV } );
		add_to_map ( SV, PQ, S, vec_it_find_PQ_cll, mesh_in_track_cell_in_swatch );         }

	// build one rectangle for each segment of 'swatch'
	Mesh::Iterator it_seg_swatch = swatch .iterator ( tag::over_segments );
	for ( it_seg_swatch .reset(); it_seg_swatch .in_range(); it_seg_swatch ++ )
	{	Cell ST = * it_seg_swatch;
		Cell S = ST .base() .reverse();
		Cell T = ST .tip();
		Cell VW = tag::Util::get_element_of_const_map ( seg_to_seg, ST, tag::surely_found );
		Cell V = VW .base() .reverse();
		Cell W = VW .tip();
		assert ( tag::Util::get_element_of_const_map ( ver_to_ver, S, tag::surely_found ) == V );
		assert ( tag::Util::get_element_of_const_map ( ver_to_ver, T, tag::surely_found ) == W );
		Cell SV = tag::Util::get_element_of_const_map ( segments_by_ver, S, tag::surely_found );
		assert ( SV .base() .reverse ( tag::surely_exists ) == S );
		assert ( SV .tip() == V );
		Cell TW = tag::Util::get_element_of_const_map ( segments_by_ver, T, tag::surely_found );
		assert ( TW .base() .reverse ( tag::surely_exists ) == T );
		assert ( TW .tip() == W );
		assert ( tag::Util::get_element_of_const_map ( ver_to_ver, S, tag::surely_found ) == V );
		assert ( tag::Util::get_element_of_const_map ( ver_to_ver, T, tag::surely_found ) == W );
		Cell TSVW ( tag::rectangle, ST .reverse(), SV, VW, TW .reverse() );
		add_to_map ( TSVW, PQ, ST, vec_it_find_PQ_msh, mesh_in_track_mesh_in_swatch );             }

}  // end of  close_extruded_1Dswatch

//------------------------------------------------------------------------------------------------------//


inline void close_extruded_2Dswatch   // hidden in anonymous namespace
( Mesh & result, const Mesh & swatch, const Mesh & track, const Cell & common_vertex,
  const std::map < Cell, Cell > & ver_to_ver,
  const std::map < Cell, Cell > & seg_to_seg, const std::map < Cell, Cell > & tri_to_tri )

// what we call 'tri' could be a square or some other polygon

{	Cell P = common_vertex;
	Cell PQ = track .cell_in_front_of ( P, tag::surely_exists );
	
	// build one segment for each vertex of 'swatch'
	// note : one of them will be PQ
	std::map < Cell, Cell > segments_by_ver;
	Mesh::Iterator it_ver_swatch = swatch .iterator ( tag::over_vertices );
	for ( it_ver_swatch .reset(); it_ver_swatch .in_range(); it_ver_swatch ++ )
	{	Cell S = * it_ver_swatch;
		Cell V = tag::Util::get_element_of_const_map ( ver_to_ver, S, tag::surely_found );
		if ( S == common_vertex )
		{	assert ( V == PQ .tip() );
			tag::Util::insert_new_elem_in_map ( segments_by_ver, { P, PQ } );  }
		else
		{	Cell SV ( tag::segment, S .reverse(), V );
			tag::Util::insert_new_elem_in_map ( segments_by_ver, { S, SV } );  }                    }

	// build one rectangle for each segment of 'swatch'
	std::map < Cell, Cell > rectangles_by_seg;
	Mesh::Iterator it_seg_swatch = swatch .iterator ( tag::over_segments );
	for ( it_seg_swatch .reset(); it_seg_swatch .in_range(); it_seg_swatch ++ )
	{	Cell ST = * it_seg_swatch;
		Cell S = ST .base() .reverse();
		Cell T = ST .tip();
		Cell VW = tag::Util::get_element_of_const_map ( seg_to_seg, ST, tag::surely_found );
		Cell V = VW .base() .reverse();
		Cell W = VW .tip();
		assert ( tag::Util::get_element_of_const_map ( ver_to_ver, S, tag::surely_found ) == V );
		assert ( tag::Util::get_element_of_const_map ( ver_to_ver, T, tag::surely_found ) == W );
		Cell SV = tag::Util::get_element_of_const_map ( segments_by_ver, S, tag::surely_found );
		assert ( SV .base() .reverse ( tag::surely_exists ) == S );
		assert ( SV .tip() == V );
		Cell TW = tag::Util::get_element_of_const_map ( segments_by_ver, T, tag::surely_found );
		assert ( TW .base() .reverse ( tag::surely_exists ) == T );
		assert ( TW .tip() == W );
		assert ( tag::Util::get_element_of_const_map ( ver_to_ver, S, tag::surely_found ) == V );
		assert ( tag::Util::get_element_of_const_map ( ver_to_ver, T, tag::surely_found ) == W );
		Cell TSVW ( tag::rectangle, ST .reverse(), SV, VW, TW .reverse() );
		tag::Util::insert_new_elem_in_map ( rectangles_by_seg, { ST, TSVW } );
		tag::Util::insert_new_elem_in_map ( rectangles_by_seg,
			{ ST .reverse(), TSVW .reverse ( tag::does_not_exist, tag::build_now ) } );             }

	// for each square of 'swatch', find a parallel square and build the corresponding cube
	// for each triangle of 'swatch', find a parallel triangle and build the corresponding prism
	Mesh::Iterator it_tri_swatch = swatch .iterator
		( tag::over_cells_of_max_dim, tag::orientation_compatible_with_mesh );
	// what we call 'tri' could be a square or some other polygon
	for ( it_tri_swatch .reset(); it_tri_swatch .in_range(); it_tri_swatch ++ )
	{	Cell polygon_ST = * it_tri_swatch;
		Cell polygon_VW = tag::Util::get_element_of_const_map
			( tri_to_tri, polygon_ST, tag::surely_found );
		Mesh bdry_ST = polygon_ST .boundary();
		const size_t n = bdry_ST .number_of ( tag::cells_of_max_dim );
		assert ( n == polygon_VW .boundary() .number_of ( tag::cells_of_max_dim ) );
		Mesh bdry_prism ( tag::whose_core_is,  // boudary of the future prism, or cube
		                  new Mesh::Fuzzy ( tag::of_dimension, 3, tag::minus_one,
		                                    tag::one_dummy_wrapper               ),
		                  tag::move, tag::is_positive                              );
		polygon_ST .reverse() .core->add_to_mesh ( bdry_prism .core );
		Mesh::Iterator it_segs_bdry = bdry_ST .iterator
			( tag::over_cells_of_max_dim, tag::orientation_compatible_with_mesh,
			  tag::require_order                                                );
		for ( it_segs_bdry .reset(); it_segs_bdry .in_range(); it_segs_bdry ++ )
		{	Cell ST = * it_segs_bdry;
			Cell VW = tag::Util::get_element_of_const_map ( seg_to_seg, ST, tag::surely_found );
			Cell TSVW = tag::Util::get_element_of_const_map
				( rectangles_by_seg, ST, tag::surely_found );
		   TSVW .reverse ( tag::surely_exists ) .core->add_to_mesh ( bdry_prism .core );         }
		polygon_VW .core->add_to_mesh ( bdry_prism .core );
		Cell prism ( tag::whose_core_is,
			new Cell::Positive::HighDim
				( tag::whose_boundary_is, bdry_prism, tag::one_dummy_wrapper ),
			tag::move                                                         );
		prism .add_to ( result );                                                                   }
	
}  // end of  close_extruded_2Dswatch

//------------------------------------------------------------------------------------------------------//

							  
inline void count_vertices_forward  // hidden in anonymous namespace
( const Mesh & track, const Cell & initial_vertex, size_t & n_forward )

// 'track' is a 1D mesh (open)

// starting from 'initial_vertex', count vertices going forward along 'track'
// 'n_forward' has already a value, we just increment it

{	Cell Q = initial_vertex;

	while ( true )
	{	Cell after_Q = track .cell_in_front_of ( Q, tag::may_not_exist );
		if ( not after_Q .exists() )  return;
		Q = after_Q .tip();
		n_forward ++;                                                       }  }


inline void count_vertices_backwards  // hidden in anonymous namespace
( const Mesh & track, const Cell & initial_vertex, size_t & n_backwards )

// 'track' is a 1D mesh (open)

// starting from 'initial_vertex', count vertices going backwards along 'track'
// 'n_backwards' has already a value, we just increment it

{	Cell P = initial_vertex;

	while ( true )
	{	Cell before_P = track .cell_behind ( P, tag::may_not_exist );
		if ( not before_P .exists() )  return;
		P = before_P .base() .reverse();
		n_backwards ++;                                                 }  }


inline Cell count_vertices  // hidden in anonymous namespace
( const Mesh & track, const Cell & initial_vertex, size_t & n_backwards, size_t & n_forward )

// 'track' is a 1D mesh

// starting from 'initial_vertex', count vertices in both directions of 'track'
// if 'track' is open, return the number of vertices to each extremity
// if 'track' is a closed loop, return the number of vertices to the opposite vertex

// return missing segment if 'track' is a closed loop, non-existent cell if 'track' is open
  
// 'n_backwards' and 'n_forward' are already initialized as zero

{	Cell P = initial_vertex, Q = initial_vertex;
	// n_backwards = 0, n_forward = 0;  // calling code does this

	while ( true )
	{	Cell before_P = track .cell_behind ( P, tag::may_not_exist );
		if ( not before_P .exists() )
		{	count_vertices_forward ( track, Q, n_forward );
			return  Cell ( tag::non_existent );              }
		Cell after_Q = track .cell_in_front_of ( Q, tag::may_not_exist );
		if ( not after_Q .exists() )
		{	count_vertices_backwards ( track, P, n_backwards );
			return  Cell ( tag::non_existent );                  }
		P = before_P .base() .reverse();
		if ( P == Q )  return before_P;
		n_backwards ++;
		Q = after_Q .tip();
		if ( P == Q )  return after_Q;
		n_forward ++;                                                      }  }

//------------------------------------------------------------------------------------------------------//


template < class Position >  // hidden in anonymous namespace
Mesh extrude_1Dtrack ( const Mesh & swatch, const Mesh & track, const Cell & common_vertex )

// class Position  could be 'Translation2D::***'          'RotoTranslation2D::***'
//                          'Translation3D::***::Track1D' 'RotoTranslation3D::***::Track1D'
	
// topological dimension : 'swatch' 1D, 'track' 1D, 'result' 2D
//                    or : 'swatch' 2D, 'track' 1D, 'result' 3D
// geometric dimension could be 2 or 3, depending on class Position

{	assert ( common_vertex .is_positive() );
	assert ( common_vertex .belongs_to ( track ) );
	assert ( common_vertex .belongs_to ( swatch ) );

	std::map < Cell, typename Position::deform_type > deform;
	tag::Util::insert_new_elem_in_map ( deform, { common_vertex, Position::identity } );

	size_t counter_backwards = 0, counter_forward = 0;
	Cell missing_link = count_vertices ( track, common_vertex, counter_backwards, counter_forward );
	// if track is open, missing_link does not exist

	// 'describe_geometry' does not depend on whether 'track' is closed or open
	// if 'track' is closed, we treat it as being made of two (open) halves
	Position::describe_geometry ( track, common_vertex, deform, counter_backwards );
	Position::describe_geometry ( track .reverse(), common_vertex, deform, counter_forward );
	// 'describe_geometry' goes backwards, for no special reason

	// if 'track' is closed, we could regularize 'deform' a little bit !

	std::map < Cell, Cell > ver_to_ver, seg_to_seg;

	if ( swatch .dim() == 1 )

	{	Mesh result ( tag::fuzzy, tag::of_dimension, 2 );

		if ( missing_link .exists() )  // 'track' is closed
		// there will be 'counter_forward + counter_backwards + 1' layers
		{	extrude_backwards_1Dtrack_1Dswatch < Position >  // we go backwards, for no special reason
			( result, swatch, track, deform, common_vertex,
			  ver_to_ver, seg_to_seg, counter_forward + counter_backwards );
			// class Position is irrelevant below, vertices have already been placed
			close_extruded_1Dswatch ( result, swatch, track, common_vertex, ver_to_ver, seg_to_seg );  }
		
		else  // 'track' is open
		// there will be 'counter_forward + counter_backwards' layers
		{	extrude_backwards_1Dtrack_1Dswatch < Position >  // go backwards along the 'track'
			( result, swatch, track, deform, common_vertex,
			  ver_to_ver, seg_to_seg, counter_backwards    );
			ver_to_ver .clear();  seg_to_seg .clear();
			extrude_forward_1Dtrack_1Dswatch < Position >  // now go forward along the 'track'
			( result, swatch, track, deform, common_vertex,
			  ver_to_ver, seg_to_seg, counter_forward      );  }

		return result;                                                                            }

	// else
	assert ( swatch .dim() == 2 );
	Mesh result ( tag::fuzzy, tag::of_dimension, 3 );

	std::map < Cell, Cell > tri_to_tri;  // what we call 'tri' could be a square or some other polygon
	if ( missing_link .exists() )  // 'track' is closed
	{	extrude_backwards_1Dtrack_2Dswatch < Position >  // we go backwards, for no special reason
		( result, swatch, track, deform, common_vertex,
		  ver_to_ver, seg_to_seg, tri_to_tri, counter_forward + counter_backwards );
		// class Position is irrelevant below, vertices have already been placed
		close_extruded_2Dswatch ( result, swatch, track, common_vertex,
		                          ver_to_ver, seg_to_seg, tri_to_tri   );             }
	  
	else  // 'track' is open
	{	extrude_backwards_1Dtrack_2Dswatch < Position >  // go backwards along the 'track'
		( result, swatch, track, deform, common_vertex,
		  ver_to_ver, seg_to_seg, tri_to_tri, counter_backwards );
		ver_to_ver .clear();  seg_to_seg .clear();  tri_to_tri .clear();
		extrude_forward_1Dtrack_2Dswatch < Position >  // now go forward along the 'track'
		( result, swatch, track, deform, common_vertex,
		  ver_to_ver, seg_to_seg, tri_to_tri, counter_forward );          }

	return result;
	
}  // end of  extrude_1Dtrack


void find_extremities_of ( Mesh & one_dim_mesh )  // hidden in anonymous namespace
// 'one_dim_mesh' is of type Mesh::Connected::OneDim and has one segment kept under 'first_ver'
// although this is of course not true
// it is just useful for starting a search for first and last vertices
// however, 'one_dim_mesh' has the right number of segments

{	Mesh::Connected::OneDim * msh_c =
		tag::Util::assert_cast < Mesh::Core *, Mesh::Connected::OneDim * > ( one_dim_mesh .core );
	// one segment has been kept under the (absurd) name 'msh_c->first_ver'
	Cell ini_seg = msh_c->first_ver;
	assert ( ini_seg .belongs_to ( one_dim_mesh, tag::orientation_compatible_with_mesh ) );
	#ifndef NDEBUG  // DEBUG mode
	size_t counter = 0;
	#endif  // DEBUG
	Cell seg = ini_seg;
	Cell P ( tag::non_existent );;
	while ( seg .exists() )
	{	P = seg .base() .reverse ( tag::surely_exists );
		if ( P == ini_seg .tip() )  break;
		#ifndef NDEBUG  // DEBUG mode
		counter ++;
		#endif  // DEBUG
		seg = one_dim_mesh .cell_behind ( P, tag::may_not_exist );  }
	msh_c->first_ver = P;
	if ( P == ini_seg .tip() )  // closed loop
	{	msh_c->last_ver = P;
		assert ( counter + 1 == one_dim_mesh .number_of ( tag::cells_of_max_dim ) );
		return;                                                                       }
	seg = ini_seg;
	while ( seg .exists() )
	{	P = seg .tip();
		#ifndef NDEBUG  // DEBUG mode
		counter ++;
		#endif  // DEBUG
		seg = one_dim_mesh .cell_in_front_of ( P, tag::may_not_exist );
		assert ( P != ini_seg .base() .reverse ( tag::surely_exists ) );  }
	msh_c->last_ver = P;
	assert ( counter == one_dim_mesh .number_of ( tag::cells_of_max_dim ) + 1 );

}  // end of  find_extremities_of


template < class Position >  // hidden in anonymous namespace
Mesh::Composite extrude_1Dtrack
( const Mesh & swatch, const Mesh & track, const Cell & common_vertex,
  const Mesh::Composite & comp_swatch, const Mesh::Composite & comp_track,
  std::map < Cell, std::map < Cell, Cell > > & cell_in_track_cell_in_swatch,
  std::map < Cell, std::map < Mesh, Mesh > > & cell_in_track_mesh_in_swatch,
  std::map < Mesh, std::map < Cell, Mesh > > & mesh_in_track_cell_in_swatch,
  std::map < Mesh, std::map < Mesh, Mesh > > & mesh_in_track_mesh_in_swatch )

// class Position  could be 'Translation2D::***'          'RotoTranslation2D::***'
//                          'Translation3D::***::Track1D' 'RotoTranslation3D::***::Track1D'
	
// topological dimension : 'swatch' 1D, 'track' 1D, 'result' 2D
//                    or : 'swatch' 2D, 'track' 1D, 'result' 3D
// geometric dimension could be 2 or 3, depending on class Position

{	assert ( common_vertex .is_positive() );
	assert ( common_vertex .belongs_to ( track ) );
	assert ( common_vertex .belongs_to ( swatch ) );

	std::map < Cell, typename Position::deform_type > deform;
	tag::Util::insert_new_elem_in_map ( deform, { common_vertex, Position::identity } );

	size_t counter_backwards = 0, counter_forward = 0;
	Cell missing_link = count_vertices ( track, common_vertex, counter_backwards, counter_forward );
	// if track is open, missing_link does not exist

	// 'describe_geometry' does not depend on whether 'track' is closed or open
	// if 'track' is closed, we treat it as being made of two (open) halves
	Position::describe_geometry ( track, common_vertex, deform, counter_backwards );
	Position::describe_geometry ( track .reverse(), common_vertex, deform, counter_forward );
	// 'describe_geometry' goes backwards, for no special reason

	// if 'track' is closed, we could regularize 'deform' a little bit !

	std::map < Cell, Cell > ver_to_ver, seg_to_seg;

	if ( swatch .dim() == 1 )

	{	if ( missing_link .exists() )  // 'track' is closed
		// there will be 'counter_forward + counter_backwards + 1' layers
		{	Cell missing_seg ( tag::non_existent );
			extrude_backwards_1Dtrack_1Dswatch < Position >  // we go backwards, for no special reason
			( swatch, track, comp_swatch, comp_track, deform, common_vertex,
			  missing_seg,
			  ver_to_ver, seg_to_seg, counter_forward + counter_backwards,
			  cell_in_track_cell_in_swatch, cell_in_track_mesh_in_swatch,
			  mesh_in_track_cell_in_swatch, mesh_in_track_mesh_in_swatch    );
			assert ( missing_seg .exists() );
			// 'missing_seg' is not the same as 'missing_link'
			// because we end up in a different position in 'track'
			assert ( common_vertex == missing_seg .base() .reverse() );
			// class Position is irrelevant below, vertices have already been placed
			close_extruded_1Dswatch
			( swatch, track, common_vertex, missing_seg, ver_to_ver, seg_to_seg,
			  cell_in_track_cell_in_swatch, cell_in_track_mesh_in_swatch,
			  mesh_in_track_cell_in_swatch, mesh_in_track_mesh_in_swatch        );  }
		else  // 'track' is open
		// there will be 'counter_forward + counter_backwards' layers
		{	extrude_backwards_1Dtrack_1Dswatch < Position >  // go backwards along the 'track'
			( swatch, track, comp_swatch, comp_track, deform, common_vertex,
			  missing_link, ver_to_ver, seg_to_seg, counter_backwards,
			  cell_in_track_cell_in_swatch, cell_in_track_mesh_in_swatch,
			  mesh_in_track_cell_in_swatch, mesh_in_track_mesh_in_swatch    );
			// in the above, we change 'missing_link' but it's not relevant, it won't be used
			ver_to_ver .clear();  seg_to_seg .clear();
			extrude_forward_1Dtrack_1Dswatch < Position >  // go forward along the 'track'
			( swatch, track, comp_swatch, comp_track, deform, common_vertex,
			  ver_to_ver, seg_to_seg, counter_forward,
			  cell_in_track_cell_in_swatch, cell_in_track_mesh_in_swatch,
			  mesh_in_track_cell_in_swatch, mesh_in_track_mesh_in_swatch    );  }      }
	assert ( swatch .dim() == 1 );

	// some (sub)meshes are Mesh::Fuzzy, others are Mesh::Connected::1D
	// for the latter case, we need to find first and last segments

	for ( std::map < Mesh, std::map < Cell, Mesh > > ::iterator
	        it_1  = mesh_in_track_cell_in_swatch .begin();
	        it_1 != mesh_in_track_cell_in_swatch .end(); it_1 ++  )
	{	const Mesh & piece_of_track = it_1->first;
		Mesh::Connected::OneDim * msh_c =
			dynamic_cast < Mesh::Connected::OneDim * > ( piece_of_track .core );
		if ( msh_c == nullptr )  continue;  // mesh is probably Fuzzy
		std::map < Cell, Mesh > & m = it_1->second;
		for ( std::map < Cell, Mesh > ::iterator it_2 = m .begin(); it_2 != m .end(); it_2 ++ )
		{	Mesh & submesh_of_extruded = it_2->second;
			find_extremities_of ( submesh_of_extruded );                                          }  }
	
	for ( std::map < Cell, std::map < Mesh, Mesh > > ::iterator
	        it_1  = cell_in_track_mesh_in_swatch .begin();
	        it_1 != cell_in_track_mesh_in_swatch .end(); it_1 ++  )
	{	std::map < Mesh, Mesh > & m = it_1->second;
		for ( std::map < Mesh, Mesh > ::iterator it_2 = m .begin(); it_2 != m .end(); it_2 ++ )
		{	Mesh piece_of_swatch = it_2->first;
			Mesh::Connected::OneDim * msh_c =
				dynamic_cast < Mesh::Connected::OneDim * > ( piece_of_swatch .core );
			if ( msh_c == nullptr )  continue;  // mesh is probably Fuzzy
			Mesh & submesh_of_extruded = it_2->second;
			find_extremities_of ( submesh_of_extruded );                              }           }
		
	// transform maps '****_in_track_****_in_swatch' into a Mesh::Composite
	Mesh::Composite result_comp;

	std::map < std::string, std::vector < Cell > > ::const_iterator
		it_points_swatch, it_points_track;
	std::map < std::string, std::vector < Mesh > > ::const_iterator
		it_meshes_swatch, it_meshes_track;
	for ( it_points_swatch  = comp_swatch .points .begin();
	      it_points_swatch != comp_swatch .points .end(); it_points_swatch ++ )
	{	assert ( it_points_swatch->second .size() == 1 );
		Cell S = it_points_swatch->second [0];
		for ( it_points_track  = comp_track .points .begin();
		      it_points_track != comp_track .points .end(); it_points_track ++ )
		{	assert ( it_points_track->second .size() == 1 );
			Cell P = it_points_track->second [0];
			std::string name = it_points_swatch->first + " times " + it_points_track->first;
			std::map < Cell, Cell > & m =
				tag::Util::get_element_of_map
					( cell_in_track_cell_in_swatch, P, tag::surely_found );
			Cell PS = tag::Util::get_element_of_map ( m, S, tag::surely_found );
			assert ( PS .exists() );
			tag::Util::insert_new_elem_in_map ( result_comp .points, { name, { PS } } );             }
		for ( it_meshes_track  = comp_track .meshes .begin();
		      it_meshes_track != comp_track .meshes .end(); it_meshes_track ++ )
		{	assert ( it_meshes_track->second .size() == 1 );
			Mesh piece_of_track = it_meshes_track->second [0];
			std::string name = it_points_swatch->first + " times " + it_meshes_track->first;
			std::map < Cell, Mesh > & m =
				tag::Util::get_element_of_map
					( mesh_in_track_cell_in_swatch, piece_of_track, tag::surely_found );
			Mesh msh = tag::Util::get_element_of_map ( m, S, tag::surely_found );
			tag::Util::insert_new_elem_in_map ( result_comp .meshes, { name, { msh } } );            }  }
	for ( it_meshes_swatch  = comp_swatch .meshes .begin();
	      it_meshes_swatch != comp_swatch .meshes .end(); it_meshes_swatch ++ )
	{	assert ( it_meshes_swatch->second .size() == 1 );
		Mesh piece_of_swatch = it_meshes_swatch->second [0];
		for ( it_points_track  = comp_track .points .begin();
		      it_points_track != comp_track .points .end(); it_points_track ++ )
		{	assert ( it_points_track->second .size() == 1 );
			Cell P = it_points_track->second [0];
			std::string name = it_meshes_swatch->first + " times " + it_points_track->first;
			std::map < Mesh, Mesh > & m =
				tag::Util::get_element_of_map
					( cell_in_track_mesh_in_swatch, P, tag::surely_found );
			Mesh msh = tag::Util::get_element_of_map ( m, piece_of_swatch, tag::surely_found );
			tag::Util::insert_new_elem_in_map ( result_comp .meshes, { name, { msh } } );               }
		for ( it_meshes_track  = comp_track .meshes .begin();
		      it_meshes_track != comp_track .meshes .end(); it_meshes_track ++ )
		{	assert ( it_meshes_track->second .size() == 1 );
			Mesh piece_of_track = it_meshes_track->second [0];
			std::string name = it_meshes_swatch->first + " times " + it_meshes_track->first;
			std::map < Mesh, Mesh > & m =
				tag::Util::get_element_of_map
					( mesh_in_track_mesh_in_swatch, piece_of_track, tag::surely_found );
			Mesh msh = tag::Util::get_element_of_map ( m, piece_of_swatch, tag::surely_found );
			tag::Util::insert_new_elem_in_map ( result_comp .meshes, { name, { msh } } );               }  }

	return result_comp;
	
}  // end of  extrude_1Dtrack
	
}  // end of anonymous namespace


Mesh Mesh::Build::Extrude::build_mesh ( )
// virtual from Mesh::Build::Core, here overridden

{	Manifold RRn = Manifold::working;

	assert ( this->track .dim() == 1 );  // in the future, a 2D 'track' may be allowed

	assert ( not this->common_vertex .exists() );
	size_t nb_of_found_vertices = 0;
	Mesh::Iterator it_track = this->track .iterator ( tag::over_vertices );
	for ( it_track .reset(); it_track .in_range(); it_track ++ )
	{	Cell V = * it_track;
		Mesh::Iterator it_swatch = this->swatch .iterator ( tag::over_vertices );
		for ( it_swatch .reset(); it_swatch .in_range(); it_swatch ++ )
			if ( V == * it_swatch )
			{	this->common_vertex = V;    // we break the inner loop, over vertices of 'swatch'
				nb_of_found_vertices ++;    // the loop over vertices of 'track' continues
				break;                    }                                          }

	if ( nb_of_found_vertices == 0 )
	{	std::cout << "no common vertices found between swatch and track" << std::endl;
		exit (1);                                                                       }
	assert ( this->common_vertex .exists() );

	if ( nb_of_found_vertices > 1 )
	{	std::cout << "swatch and track have more than one vertex in common" << std::endl;
		exit (1);                                                                          }

	if ( RRn .topological_dim() == 2 )
	{	assert ( this->swatch .dim() == 1 );
		if ( this->follow_curvature )
			return  extrude_1Dtrack < RotoTranslation2D::Direct >
				( this->swatch, this->track, this->common_vertex );
		// else
		return  extrude_1Dtrack < Translation2D::Direct >
			( this->swatch, this->track, this->common_vertex );      }
	// else
	assert ( RRn .topological_dim() == 3 );
	if ( this->follow_curvature )
		return  extrude_1Dtrack < RotoTranslation3D::Direct::Track1D >
			( this->swatch, this->track, this->common_vertex );
	// else
	return  extrude_1Dtrack < Translation3D::Direct::Track1D >
		( this->swatch, this->track, this->common_vertex );
	
}  // end of  Mesh::Build::Extrude::build_mesh


Mesh Mesh::Build::Extrude::Composite::build_mesh ( )
// virtual from Mesh::Build::Core, here overridden

{	Mesh local_swatch = this->swatch .get_global_mesh();
	// may return a newly build mesh (from joining components) or a previously existing one
	Mesh local_track = this->track .get_global_mesh();
	// may return a newly build mesh (from joining components) or a previously existing one
	
	std::shared_ptr < Mesh::Build::Extrude > builder_core =
		std::make_shared < Mesh::Build::Extrude > ();
	builder_core->swatch = local_swatch;
	builder_core->track = local_track;
	builder_core->follow_curvature = this->follow_curvature;
	builder_core->follow_torsion = this->follow_torsion;

	return  Mesh::Build ( tag::whose_core_is, builder_core );  // will be converted to 'Mesh'

}  // end of  Mesh::Build::Extrude::Composite::build_mesh

  
Mesh::Composite Mesh::Build::Extrude::build_composite_mesh ( )
// virtual from Mesh::Build::Core, here overridden

{	Mesh res = this->build_mesh();
	assert ( this->common_vertex .exists() );
	return  Mesh::Build ( tag::gather ) .mesh ( "extruded", res )
	                                    .mesh ( "swatch", this->swatch )
	                                    .mesh ( "track", this->track )
	                                    .cell ( "common vertex", this->common_vertex );  }


Mesh::Composite Mesh::Build::Extrude::Composite::build_composite_mesh ( )
// virtual from Mesh::Build::Core, here overridden

{	Manifold RRn = Manifold::working;

	Mesh local_swatch = this->swatch .get_global_mesh();
	// may return a newly build mesh (from joining components) or a previously existing one
	Mesh local_track = this->track .get_global_mesh();
	// may return a newly build mesh (from joining components) or a previously existing one
	
	assert ( local_track .dim() == 1 );  // in the future, a 2D 'track' may be allowed

	Cell common_vertex { tag::non_existent };
	size_t nb_of_found_vertices = 0;
	Mesh::Iterator it_track = local_track .iterator ( tag::over_vertices );
	for ( it_track .reset(); it_track .in_range(); it_track ++ )
	{	Cell V = * it_track;
		Mesh::Iterator it_swatch = local_swatch .iterator ( tag::over_vertices );
		for ( it_swatch .reset(); it_swatch .in_range(); it_swatch ++ )
			if ( V == * it_swatch )
			{	common_vertex = V;          // we break the inner loop, over vertices of 'swatch'
				nb_of_found_vertices ++;    // the loop over vertices of 'track' continues
				break;                    }                                    }

	if ( nb_of_found_vertices == 0 )
	{	std::cout << "no common vertices found between swatch and track" << std::endl;
		exit (1);                                                                       }

	if ( nb_of_found_vertices > 1 )
	{	std::cout << "swatch and track have more than one vertex in common" << std::endl;
		exit (1);                                                                          }

	std::map < Cell, std::map < Cell, Cell > > cell_in_track_cell_in_swatch;
	std::map < Cell, std::map < Mesh, Mesh > > cell_in_track_mesh_in_swatch;
	std::map < Mesh, std::map < Cell, Mesh > > mesh_in_track_cell_in_swatch;
	std::map < Mesh, std::map < Mesh, Mesh > > mesh_in_track_mesh_in_swatch;

	std::map < std::string, std::vector < Cell > > ::const_iterator
		it_points_swatch, it_points_track;
	std::map < std::string, std::vector < Mesh > > ::const_iterator
		it_meshes_swatch, it_meshes_track;
	for ( it_points_swatch  = this->swatch .points .begin();
	      it_points_swatch != this->swatch .points .end(); it_points_swatch ++ )
	{	assert ( it_points_swatch->second .size() == 1 );
		Cell S = it_points_swatch->second [0];
		for ( it_points_track  = this->track .points .begin();
		      it_points_track != this->track .points .end(); it_points_track ++ )
		{	assert ( it_points_track->second .size() == 1 );
			Cell P = it_points_track->second [0];
			// below, a reference would be more elegant, but the 'if' blocks do not allow it
			std::map < Cell, Cell > * m = nullptr;
			// improve efficiency of code below, book of meyers, emplace_hint
			std::map < Cell, std::map < Cell, Cell > > ::iterator it
				= cell_in_track_cell_in_swatch .find (P);
			if ( it == cell_in_track_cell_in_swatch .end() )
				m = & cell_in_track_cell_in_swatch .emplace
					( std::piecewise_construct, std::forward_as_tuple (P),
					  std::forward_as_tuple ( std::map < Cell, Cell > { } ) ) .first->second;
			else
				m = & it->second;
			assert ( m );
			tag::Util::insert_new_elem_in_map ( *m, { S, Cell ( tag::non_existent ) } );            }
		for ( it_meshes_track  = this->track .meshes .begin();
		      it_meshes_track != this->track .meshes .end(); it_meshes_track ++ )
		{	assert ( it_meshes_track->second .size() == 1 );
			Mesh piece_of_track = it_meshes_track->second [0];
			// below, a reference would be more elegant, but the 'if' blocks do not allow it
			std::map < Cell, Mesh > * m = nullptr;
			// improve efficiency of code below, book of meyers, emplace_hint
			std::map < Mesh, std::map < Cell, Mesh > > ::iterator it
				= mesh_in_track_cell_in_swatch .find ( piece_of_track );
			if ( it == mesh_in_track_cell_in_swatch .end() )
				m = & mesh_in_track_cell_in_swatch .emplace
					( std::piecewise_construct, std::forward_as_tuple ( piece_of_track ),
					  std::forward_as_tuple ( std::map < Cell, Mesh > { } ) ) .first->second;
			else
				m = & it->second;
			assert ( m );
			Mesh::Connected::OneDim * msh_c =
				dynamic_cast < Mesh::Connected::OneDim * > ( piece_of_track .core );
			if ( msh_c )
				// each time a cell is added to such a Mesh::Connected::OneDim,
				// it will be kept as 'first_ver' (not true, of course)
				// in the end, we search for the true first and last vertices
				// cells will be added with tag::do_not_bother and tag::keep_as_first_ver
				// which has no effect for Mesh::Fuzzy
			{	Mesh::Connected::OneDim * c1d_msh_c =
					new Mesh::Connected::OneDim ( tag::with,
						piece_of_track .number_of ( tag::cells_of_max_dim ),
						tag::segments, tag::one_dummy_wrapper               );
				tag::Util::insert_new_elem_in_map ( *m, { S,
					Mesh ( tag::whose_core_is, c1d_msh_c, tag::move, tag::is_positive ) } );  }
			else  // piece_of_track is probably Fuzzy
				tag::Util::insert_new_elem_in_map ( *m, { S,
					Mesh ( tag::fuzzy, tag::of_dimension, piece_of_track .dim() ) } );           }  }
	for ( it_meshes_swatch  = this->swatch .meshes .begin();
	      it_meshes_swatch != this->swatch .meshes .end(); it_meshes_swatch ++ )
	{	assert ( it_meshes_swatch->second .size() == 1 );
		Mesh piece_of_swatch = it_meshes_swatch->second [0];
		for ( it_points_track  = this->track .points .begin();
		      it_points_track != this->track .points .end(); it_points_track ++ )
		{	assert ( it_points_track->second .size() == 1 );
			Cell P = it_points_track->second [0];
			// below, a reference would be more elegant, but the 'if' blocks do not allow it
			std::map < Mesh, Mesh > * m = nullptr;
			// improve efficiency of code below, book of meyers, emplace_hint
			std::map < Cell, std::map < Mesh, Mesh > > ::iterator it
				= cell_in_track_mesh_in_swatch .find (P);
			if ( it == cell_in_track_mesh_in_swatch .end() )
				m = & cell_in_track_mesh_in_swatch .emplace
					( std::piecewise_construct, std::forward_as_tuple (P),
					  std::forward_as_tuple ( std::map < Mesh, Mesh > { } ) ) .first->second;
			else
				m = & it->second;
			assert ( m );
			Mesh::Connected::OneDim * msh_c =
				dynamic_cast < Mesh::Connected::OneDim * > ( piece_of_swatch .core );
			if ( msh_c )
				// each time a cell is added to such a Mesh::Connected::OneDim,
				// it will be kept as 'first_ver' (not true, of course)
				// in the end, we search for the true first and last vertices
				// cells will be added with tag::do_not_bother and tag::keep_as_first_ver
				// which has no effect for Mesh::Fuzzy
			{	Mesh::Connected::OneDim * c1d_msh_c =
					new Mesh::Connected::OneDim ( tag::with,
						piece_of_swatch .number_of ( tag::cells_of_max_dim ),
						tag::segments, tag::one_dummy_wrapper                );
				tag::Util::insert_new_elem_in_map ( *m, { piece_of_swatch,
					Mesh ( tag::whose_core_is, c1d_msh_c, tag::move, tag::is_positive ) } );  }
			else  // piece_of_swatch is probably Fuzzy
				tag::Util::insert_new_elem_in_map ( *m, { piece_of_swatch,
					Mesh ( tag::fuzzy, tag::of_dimension, piece_of_swatch .dim() ) } );           }
		for ( it_meshes_track  = this->track .meshes .begin();
		      it_meshes_track != this->track .meshes .end(); it_meshes_track ++ )
		{	assert ( it_meshes_track->second .size() == 1 );
			Mesh piece_of_track = it_meshes_track->second [0];
			const size_t d = piece_of_swatch .dim() + piece_of_track .dim();
			// below, a reference would be more elegant, but the 'if' blocks do not allow it
			std::map < Mesh, Mesh > * m = nullptr;
			// improve efficiency of code below, book of meyers, emplace_hint
			std::map < Mesh, std::map < Mesh, Mesh > > ::iterator it
				= mesh_in_track_mesh_in_swatch .find ( piece_of_track );
			if ( it == mesh_in_track_mesh_in_swatch .end() )
				m = & mesh_in_track_mesh_in_swatch .emplace
					( std::piecewise_construct, std::forward_as_tuple ( piece_of_track ),
					  std::forward_as_tuple ( std::map < Mesh, Mesh > { } ) ) .first->second;
			else
				m = & it->second;
			assert ( m );
			tag::Util::insert_new_elem_in_map ( *m,
				{ piece_of_swatch, Mesh ( tag::fuzzy, tag::of_dimension, d ) } );          }  }

	// if 'common_vertex' is marked as 'point' in 'track',
	// account for cells and meshes in 'swatch', aligned with 'common_vertex'

	std::map < std::string, std::vector < Cell > > ::const_iterator
			found_point_track = this->track .points .end();
	for ( it_points_track  = this->track .points .begin();
         it_points_track != this->track .points .end(); it_points_track ++ )
	{	assert ( it_points_track->second .size() == 1 );
		if ( it_points_track->second [0] == common_vertex )
		{	assert ( found_point_track == this->track .points .end() );
			found_point_track = it_points_track;                         }  }
	if ( found_point_track  != this->track .points .end() )
	{	std::map < Cell, std::map < Cell, Cell > > ::iterator it_cll
			= cell_in_track_cell_in_swatch .find ( common_vertex );
		if ( it_cll != cell_in_track_cell_in_swatch .end() )
		// 'it_cll' may be equal to end if there are no cells marked in 'swatch'
		{	std::map < Cell, Cell > & m = it_cll->second;
			std::map < Cell, Cell > ::iterator it;
			for ( it = m .begin(); it != m .end(); it ++ )
			{	assert ( not it->second .exists() );
				it->second = it->first;              }        }
		std::map < Cell, std::map < Mesh, Mesh > > ::iterator it_msh
			= cell_in_track_mesh_in_swatch .find ( common_vertex );
		assert ( it_msh != cell_in_track_mesh_in_swatch .end() );
		// 'it_msh' is only equal to end if there are no meshes marked in 'swatch'
		// but this would make no sense
		std::map < Mesh, Mesh > & m = it_msh->second;
		std::map < Mesh, Mesh > ::iterator it;
		for ( it = m .begin(); it != m .end(); it ++ )
		{	assert ( it->first .exists() );      // mesh in swatch
			assert ( it->second .exists() );     // mesh
			assert ( it->second .number_of ( tag::cells_of_max_dim ) ==  // fake, will be true at the end
			         it->first .number_of ( tag::cells_of_max_dim )     );
			Mesh::Iterator itt = it->first .iterator
				( tag::over_cells_of_max_dim, tag::orientation_compatible_with_mesh );
			for ( itt .reset(); itt .in_range(); itt ++ )
				(*itt) .add_to ( it->second, tag::do_not_bother, tag::keep_as_first_ver );  }  }
				// if 'it->second' is Mesh::Fuzzy,
				// the above is equivalent to  (*itt) .add_to ( it->second )
	
	// if 'common_vertex' is marked as 'point' in 'swatch', no need to do anything here,
	// 'extrude_forward_xDtrack_xDswatch' does the job
			
	if ( RRn .topological_dim() == 2 )
	{	assert ( local_swatch .dim() == 1 );
		if ( this->follow_curvature )
			return  extrude_1Dtrack < RotoTranslation2D::Direct >
				( local_swatch, local_track, common_vertex, this->swatch, this->track,
				  cell_in_track_cell_in_swatch, cell_in_track_mesh_in_swatch,
				  mesh_in_track_cell_in_swatch, mesh_in_track_mesh_in_swatch          );
		// else
		return  extrude_1Dtrack < Translation2D::Direct >
			( local_swatch, local_track, common_vertex, this->swatch, this->track,
			  cell_in_track_cell_in_swatch, cell_in_track_mesh_in_swatch,
			  mesh_in_track_cell_in_swatch, mesh_in_track_mesh_in_swatch          );  }
	// else
	assert ( RRn .topological_dim() == 3 );
	if ( this->follow_curvature )
		return  extrude_1Dtrack < RotoTranslation3D::Direct::Track1D >
			( local_swatch, local_track, common_vertex, this->swatch, this->track,
			  cell_in_track_cell_in_swatch, cell_in_track_mesh_in_swatch,
			  mesh_in_track_cell_in_swatch, mesh_in_track_mesh_in_swatch          );
	// else
	return  extrude_1Dtrack < Translation3D::Direct::Track1D >
		( local_swatch, local_track, common_vertex, this->swatch, this->track,
		  cell_in_track_cell_in_swatch, cell_in_track_mesh_in_swatch,
		  mesh_in_track_cell_in_swatch, mesh_in_track_mesh_in_swatch          );
	
}  // end of  Mesh::Build::Extrude::Composite::build_composite_mesh

//------------------------------------------------------------------------------------------------------//
//------------------------------------------------------------------------------------------------------//


#ifndef OMIT_OBSOLETE_CODE

class Translation2D::Cumulative : public Translation2D
// not used

{	public :

	// inherited from Translation2D :
	// static constexpr size_t geom_dim = 2
	// static const std::vector < double > identity
	// static void place ( ... )

	static void describe_geometry
	( const Mesh & track, const Cell & common_vertex,
	  std::map < Cell, std::vector < double > > & deform, size_t max_counter );
	// 'deform' is a map which, for each vertex, keeps two doubles (dx,dy)

};  // end of  class Translation2D::Cumulative


class RotoTranslation2D::Cumulative : public RotoTranslation2D 
// not used

// compute small angles of rotation between adjacent segments in the 'track'
// then accumulate the effects of these rotations by successive matrix multiplication

// translations are computed directly from the common vertex

{	public :

	// inherited from RotoTranslation2D :
	// static constexpr size_t geom_dim = 2
	// static const std::pair < std::vector < double >, std::vector < double > > identity
	// static void place ( ... )
	// typedef std::pair < std::vector < double >, std::vector < double > > deform_type

	static void describe_geometry
	( const Mesh & track, const Cell & common_vertex,
	  std::map < Cell, deform_type > & deform, size_t max_counter );
	// 'deform' is a map which, for each vertex, keeps four doubles { dx, dy } { cos theta, sin theta }

};  // end of  class RotoTranslation2D::Cumulative


class Translation3D::Cumulative::Track1D : public Translation3D
// not used

{	public :

	// inherited from Translation3D :
	// static constexpr size_t geom_dim = 3
	// static const std::vector < double > identity
	// static void place ( ... )

	static void describe_geometry
	( const Mesh & track, const Cell & common_vertex,
	  std::map < Cell, std::vector < double > > & deform, size_t max_counter );
	// 'deform' is a map which, for each vertex, keeps three doubles (dx,dy,dz)

};  // end of  class Translation3D::Cumulative::Track1D


class Translation3D::Cumulative::Track2D : public Translation3D
// not used; does not even make much sense: cumulative on a 2D track ?

{	public :

	// inherited from Translation3D :
	// static constexpr size_t geom_dim = 3
	// static const std::vector < double > identity
	// static void place ( ... )

	static void describe_geometry
	( const Mesh & track, const Cell & common_vertex,
	  std::map < Cell, std::vector < double > > & deform, size_t max_counter );
	// 'deform' is a map which, for each vertex, keeps three doubles (dx,dy,dz)

};  // end of  class Translation3D::Cumulative::Track2D


class RotoTranslation3D::Cumulative::Track2D : public Translation3D
// not used; does not even make much sense: cumulative on a 2D track ?

{	public :

	// inherited from Translation3D :
	// static constexpr size_t geom_dim = 3
	// static const std::vector < double > identity
	// static void place ( ... )

	static void describe_geometry
	( const Mesh & track, const Cell & common_vertex,
	  std::map < Cell, std::vector < double > > & deform, size_t max_counter );
	// 'deform' is a map which, for each vertex, keeps three doubles (dx,dy,dz)

};  // end of  class RotoTranslation3D::Cumulative::Track2D


template < class Position >
inline void describe_geometry_transl_cumul_1Dtrack   // hidden in anonymous namespace  // not used
( const Mesh & track, const Cell & common_vertex,
  std::map < Cell, std::vector < double > > & deform, size_t max_counter )

// class Position  could be 'Translation2D' or 'Translation3D'

// 'track' is a 1D mesh

// compute in 'deform' a map of translations
// 'deform' must contain already : common_vertex -> Position::identity

{	Manifold RRd = Manifold::working;
	constexpr size_t geom_dim = Position::geom_dim;
	assert ( RRd .topological_dim() == geom_dim );
	Function coords = RRd .coordinates();
	assert ( coords .number_of ( tag::components ) == geom_dim );

	std::vector < double > transl = Position::identity;
	// the calling code has already done this :
	// deform .insert ( { common_vertex, Position::identity } );
	  
	Cell Q = common_vertex;
	size_t counter = 0;
	while ( true )
	{	if ( counter == max_counter )  break;
		counter ++;
		Cell PQ = track .cell_behind ( Q, tag::may_not_exist );
		if ( not PQ .exists() )  break;  // Q is first vertex in 'track'
		Cell P = PQ .base() .reverse();
		for ( size_t i = 0; i < geom_dim; i++ )
		{	const Function & x = coords [i];
	   	transl [i] -= x(Q) - x(P);        }
		tag::Util::insert_new_elem_in_map ( deform, { P, transl } );
		Q = P;
	}  // end of  while true

}  // end of  describe_geometry_transl_cumul_1Dtrack
																				

void Translation2D::Cumulative::describe_geometry  // not used
( const Mesh & track, const Cell & common_vertex,
  std::map < Cell, std::vector < double > > & deform, size_t max_counter )

{	describe_geometry_transl_cumul_1Dtrack < Translation2D >
		( track, common_vertex, deform, max_counter );          }


void Translation3D::Cumulative::Track1D::describe_geometry  // not used
( const Mesh & track, const Cell & common_vertex,
  std::map < Cell, std::vector < double > > & deform, size_t max_counter )

{	describe_geometry_transl_cumul_1Dtrack < Translation3D >
		( track, common_vertex, deform, max_counter );          }


inline void RotoTranslation2D::normalize_rotation_matrix  // static  // not used
( std::vector < double > & rot )

{	double nor = std::sqrt
		( rot [0] * rot [0] + rot [1] * rot [1] );
	rot [0] /= nor;  rot [1] /= nor;               }


inline void RotoTranslation3D::normalize_rotation_matrix  // static  // not used
( std::vector < double > & rot )

{	assert ( false );  // to implement
	double nor = std::sqrt
		( rot [0] * rot [0] + rot [1] * rot [1] );
	rot [0] /= nor;  rot [1] /= nor;               }


template < class Position >
inline void describe_geometry_roto_transl_cumul_1Dtrack   // hidden in anonymous namespace  // not used
( const Mesh & track, const Cell & common_vertex,
  std::map < Cell, RotoTranslation2D::deform_type > & deform, size_t max_counter )

// class Position  could be 'RotoTranslation2D' or 'RotoTranslation3D'

// 'track' is a 1D mesh

// compute in 'deform' (an approximation of) the curvature of this polygonal line
// more precisely, compute a rotation (in the form cos theta, sin theta) for each vertex

// we start from the common vertex and go backwards, for no special reason
// we compute small angles of rotation between adjacent segments in the 'track'
// then accumulate the effects of these rotations by successive matrix multiplication

// translations are computed directly from the common vertex

// 'deform' must contain already : common_vertex -> Position::identity

{	Manifold RRd = Manifold::working;
	constexpr size_t geom_dim = Position::geom_dim;
	assert ( RRd .topological_dim() == geom_dim );
	Function coords = RRd .coordinates();
	assert ( coords .number_of ( tag::components ) == geom_dim );

	Cell Q = common_vertex;
	std::vector < double > current_rot = Position::identity .second;
	// the calling code has already done this :
	// deform .insert ( { common_vertex, Position::identity } )
	std::vector < double > tau_base ( geom_dim );  //  tangent direction at 'base'
	compute_tangent_direction < Position > ( track, common_vertex, tau_base );
	
	size_t counter = 0;
	while ( true )
	{	if ( counter == max_counter )  break;
		counter ++;
		Cell PQ = track .cell_behind ( Q, tag::may_not_exist );
		if ( not PQ .exists() )  break;
		Cell P = PQ .base() .reverse();
		std::vector < double > transl ( geom_dim );
		for ( size_t i = 0; i < geom_dim; i++ )
		{	const Function & x = coords [i];
			transl [i] = x(P) - x ( common_vertex );  }
		std::vector < double > tau_current ( geom_dim );  //  tangent direction at 'P'
		compute_tangent_direction < Position > ( track, P, tau_current );
		double c;  // not used here
		std::vector < double > delta_rot =
			   Position::compute_rotation_matrix ( tau_base, tau_current, c );
		// delta_rot * tau_base = tau_current
	   Position::apply_rotation_matrix_to_matrix ( current_rot, delta_rot, current_rot );
		// current_rot = delta_rot * current_rot
		if ( counter % 50 == 0 )
			Position::normalize_rotation_matrix ( current_rot );
		tag::Util::insert_new_elem_in_map
			( deform, { P, { transl, current_rot } } );
		tau_base = tau_current;
		Q = P;
	}  // end of  while true

}  // end of  describe_geometry_roto_transl_cumul_1Dtrack


void RotoTranslation2D::Cumulative::describe_geometry   // not used
( const Mesh & track, const Cell & common_vertex,
  std::map < Cell, RotoTranslation2D::deform_type > & deform, size_t max_counter )

{	describe_geometry_roto_transl_cumul_1Dtrack < RotoTranslation2D >
		( track, common_vertex, deform, max_counter );                  }


void RotoTranslation3D::Cumulative::Track1D::describe_geometry   // not used
( const Mesh & track, const Cell & common_vertex,
  std::map < Cell, RotoTranslation3D::deform_type > & deform, size_t max_counter )

{	describe_geometry_roto_transl_cumul_1Dtrack < RotoTranslation3D >
		( track, common_vertex, deform, max_counter );                  }

#endif  // ifndef OMIT_OBSOLETE_CODE

