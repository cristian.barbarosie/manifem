
//   var-form.h  2024.12.03

//   This file is part of maniFEM, a C++ library for meshes and finite elements on manifolds.

//   Copyright  2019 - 2024  Cristian Barbarosie  cristian.barbarosie@gmail.com

//   https://maniFEM.rd.ciencias.ulisboa.pt/
//   https://codeberg.org/cristian.barbarosie/maniFEM

//   ManiFEM is free software: you can redistribute it and/or modify it
//   under the terms of the GNU Lesser General Public License as published
//   by the Free Software Foundation, either version 3 of the License
//   or (at your option) any later version.

//   ManiFEM is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//   See the GNU Lesser General Public License for more details.

//   You should have received a copy of the GNU Lesser General Public License
//   along with maniFEM.  If not, see <https://www.gnu.org/licenses/>.

// only included ifndef MANIFEM_NO_FEM


#ifndef MANIFEM_VAR_FORM_H
#define MANIFEM_VAR_FORM_H

#include <string>
#include <iostream>
#include <vector>
#include <map>
#include <forward_list>
#include "assert.h"

#include "mesh.h"
#include "field.h"
#include "function.h"
#include "manifold.h"

#include <Eigen/SparseCore>
#include <Eigen/IterativeLinearSolvers>


namespace maniFEM {


class Function::Form

// integral of an 'integrand' on a 'domain'
// or the value of a test function at a cell
// or a linear combination of such integrals or values

{	public :

	class Core;

	std::shared_ptr < Function::Form::Core > core;

	inline Form ( ) { assert ( false );  }

	inline Form ( const tag::WhoseCoreIs &, std::shared_ptr < Function::Form::Core > c )
	:	core { c }
	{	}

	inline Form ( const Function::Form & f );

	inline Function::Form & operator= ( const Function::Form & );

	inline Function::Form replace ( const Function & x, const Function & y ) const;
	
	inline void inspect_cells_of_domain_of_integration
	( std::map < std::pair < size_t, size_t >, FiniteElement > & fe ) const;
	// look at cells of domain of integration and builds appropriate finite elements
	
	inline void analyse_form ( VariationalFormulation & vf ) const;
	// analyse a form and detect Function::Test and Function::Unknown or derivatives thereof
	// copy them as 'vf.unknown' and 'vf.test'
	// also, copy 'this->domain' as 'vf.domain' if the latter does not exist yet
	// call 'analyse_integrand' on the integrand

	inline void assemble_matrix ( VariationalFormulation & vf,
	                 std::map < std::pair < size_t, size_t >, FiniteElement > & ) const;
	inline void assemble_vector ( VariationalFormulation & vf,
	                 std::map < std::pair < size_t, size_t >, FiniteElement > & ) const;
	// invoke integrator of appropriate finite element on linear and bilinear forms
	
	class Sum;  class ValueOfTest;
	typedef tag::Util::DelayedIntegral DelayedIntegral;
	class Equality;
	struct BoundaryCondition  {  class Dirichlet;  };
		
};  // end of  class Function::Form

//------------------------------------------------------------------------------------------------------//


class Function::Form::Core

// abstract class, specialized in
// Function::Form::DelayedIntegral, Function::Form::ValueOfTest, Function::Form::Sum

{	public :
		
	inline Core ( )
	{	}

	virtual ~Core ( );
	
	virtual Function::Form replace ( const Function & x, const Function & y ) const = 0;
	
	virtual void inspect_cells_of_domain_of_integration
	( std::map < std::pair < size_t, size_t >, FiniteElement > & fe ) const = 0;

	virtual void analyse_form ( VariationalFormulation & vf ) const = 0;
	
	virtual void assemble_matrix ( VariationalFormulation & vf,
	                 std::map < std::pair < size_t, size_t >, FiniteElement > & ) const = 0;
	virtual void assemble_vector ( VariationalFormulation & vf,
	                 std::map < std::pair < size_t, size_t >, FiniteElement > & ) const = 0;

	virtual Function::Form product ( double c ) = 0;
	
};  // end of  class Function::Form::Core

//------------------------------------------------------------------------------------------------------//


inline Function::Form::Form ( const Function::Form & f )
:	core { f.core }
{	}
	

inline Function::Form & Function::Form::operator= ( const Function::Form & f )
{	this->core = f .core;
	return  * this;        }
	

inline Function::Form Function::Form::replace ( const Function & x, const Function & y ) const

{	assert ( this->core );
	return  this->core->replace ( x, y );  }
	

inline void Function::Form::inspect_cells_of_domain_of_integration
( std::map < std::pair < size_t, size_t >, FiniteElement > & fe ) const

{	assert ( this->core );
	this->core->inspect_cells_of_domain_of_integration ( fe );  }

	
inline void Function::Form::analyse_form ( VariationalFormulation & vf ) const

{	assert ( this->core );
	this->core->analyse_form ( vf );  }

	
inline void Function::Form::assemble_matrix ( VariationalFormulation & vf,
                 std::map < std::pair < size_t, size_t >, FiniteElement > & fe ) const
	
{	assert ( this->core );
	this->core->assemble_matrix ( vf, fe );  }

	
inline void Function::Form::assemble_vector ( VariationalFormulation & vf,
                 std::map < std::pair < size_t, size_t >, FiniteElement > & fe ) const

{	assert ( this->core );
	this->core->assemble_vector ( vf, fe );  }


inline Function::Form operator* ( double c, const Function::Form & f )
	
{	return  f .core->product (c);  }
	

Function::Form operator+ ( const Function::Form & f, const Function::Form & g );


inline Function::Form operator- ( const Function::Form & f, const Function::Form & g )
  
{	return  f + (-1.) * g;  }

//------------------------------------------------------------------------------------------------------//


class tag::Util::DelayedIntegral : public Function::Form::Core
// aka  class Function::Form::DelayedIntegral

// integral of an 'integrand' on a 'domain'

{	public :

	Function integrand;  // we use wrapper as a pointer
	Mesh domain;

	inline DelayedIntegral ( const Function & b, Mesh msh )
	:	Function::Form::Core (), integrand { b }, domain { msh }
	{	}

	Function::Form replace ( const Function & x, const Function & y ) const;
	//  virtual from Function::Form::Core
	
	void inspect_cells_of_domain_of_integration  // virtual from Function::Form::Core
	( std::map < std::pair < size_t, size_t >, FiniteElement > & fe ) const;

	void analyse_form ( VariationalFormulation & vf ) const;  // virtual from Function::Form::Core
		
	void assemble_matrix ( VariationalFormulation & vf,    // virtual from Function::Form::Core
	                       std::map < std::pair < size_t, size_t >, FiniteElement > & ) const;

	void assemble_vector ( VariationalFormulation & vf,    // virtual from Function::Form::Core
	                       std::map < std::pair < size_t, size_t >, FiniteElement > & ) const;
		
	Function::Form product ( double c );  // virtual from Function::Form::Core
	
};  // end of  class Function::Form::DelayedIntegral

//------------------------------------------------------------------------------------------------------//


class Function::Form::ValueOfTest : public Function::Form::Core

// (a coefficient times) the value of a test function at a cell

{	public :

	double coeff;
	Function test;  // we use wrapper as a pointer
	Cell cll;

	inline ValueOfTest ( double co, const Function & t, const Cell & c )
	:	Function::Form::Core (), coeff { co }, test { t }, cll { c }
	{	}

	inline ValueOfTest ( const Function & t, const Cell & c )
	:	Function::Form::ValueOfTest ( 1., t ,c )
	{	}

	Function::Form replace ( const Function & x, const Function & y ) const;
	//  virtual from Function::Form::Core
	
	void inspect_cells_of_domain_of_integration  // virtual from Function::Form::Core
	( std::map < std::pair < size_t, size_t >, FiniteElement > & fe ) const;

	void analyse_form ( VariationalFormulation & vf ) const;  // virtual from Function::Form::Core
	
	void assemble_matrix ( VariationalFormulation & vf,    // virtual from Function::Form::Core
	                       std::map < std::pair < size_t, size_t >, FiniteElement > & ) const;

	void assemble_vector ( VariationalFormulation & vf,    // virtual from Function::Form::Core
	                       std::map < std::pair < size_t, size_t >, FiniteElement > & ) const;
	
	Function::Form product ( double c );  // virtual from Function::Form::Core
	
};  // end of  class Function::Form::ValueOfTest

//------------------------------------------------------------------------------------------------------//


class Function::Form::Sum : public Function::Form::Core

{	public :

	std::forward_list < Function::Form > terms;

	inline Sum ( )
	:	Function::Form::Core ()
	{	}

	Function::Form replace ( const Function & x, const Function & y ) const;
	//  virtual from Function::Form::Core
	
	void inspect_cells_of_domain_of_integration  // virtual from Function::Form::Core
	( std::map < std::pair < size_t, size_t >, FiniteElement > & fe ) const;

	void analyse_form ( VariationalFormulation & vf ) const;  // virtual from Function::Form::Core
	
	void assemble_matrix ( VariationalFormulation & vf,    // virtual from Function::Form::Core
	                       std::map < std::pair < size_t, size_t >, FiniteElement > & ) const;
	void assemble_vector ( VariationalFormulation & vf,    // virtual from Function::Form::Core
	                       std::map < std::pair < size_t, size_t >, FiniteElement > & ) const;
	
	Function::Form product ( double c );  // virtual from Function::Form::Core
	
};  // end of  class Function::Form::Sum

//------------------------------------------------------------------------------------------------------//


inline Function::Form Function::integral_on ( const Mesh & msh )

{  return  Function::Form ( tag::whose_core_is,
	                         std::make_shared < Function::Form::DelayedIntegral > ( *this, msh ) );  }


inline Function::Form Function::value_at ( const Cell & cll )

{  return  Function::Form ( tag::whose_core_is,
	                         std::make_shared < Function::Form::ValueOfTest > ( *this, cll ) );  }


inline void Function::analyse_integrand ( VariationalFormulation & vf ) const
{	assert ( this->exists() );  // assert ( this->core )
	this->core->analyse_integrand ( vf );  }

//------------------------------------------------------------------------------------------------------//


class Function::Form::Equality

// the result of an equality comparison between Functions

{	public :

	Function::Form lhs, rhs;

	inline Equality ( const Function::Form & l, const Function::Form & r )
	:	lhs { l }, rhs { r }
	{	}
	
};

//------------------------------------------------------------------------------------------------------//


inline Function::Form::Equality operator== ( const Function::Form & f, const Function::Form & g )
{	return Function::Form::Equality { f, g };  }

//------------------------------------------------------------------------------------------------------//


class Function::Form::BoundaryCondition::Dirichlet
// aka VariationalFormulation::BoundaryCondition::Dirichlet

// this is not a Function::Form, we just use that namespace for lack of a better place

{	public :

	Mesh domain;  // usually a piece of a boundary
	Function::Equality equality;

	inline Dirichlet ( const Function::Equality & eq, Mesh msh )
	:	domain { msh }, equality { eq }
	{	}

	inline Dirichlet ( const tag::NonExistent & )
	:	domain ( tag::non_existent )
	{	}

};  // end of  class Function::Form::BoundaryCondition::Dirichlet

//------------------------------------------------------------------------------------------------------//


class VariationalFormulation

// see paragraph 13.17 in the manual

{	public :

	typedef Function::Form::BoundaryCondition BoundaryCondition;

	// we use wrappers as pointers
	// Mesh domain { tag::non_existent };
	// we don't need a domain, each integral has its own domain of integration
	// 'numbering' will contain a list of relevant vertices
	
	std::set < Mesh > domains;
	// temporarily store domains of all integrals

	Function unknown { tag::non_existent };
	Function test { tag::non_existent };
  
	Function::Form bilinear_form;
	Function::Form linear_form;
	std::vector < VariationalFormulation::BoundaryCondition::Dirichlet > bdry_cond;

	Cell::Numbering numbering { tag::non_existent };
	
	std::shared_ptr < Eigen::VectorXd > vector_lin { std::make_shared < Eigen::VectorXd > () };
	// shared with the solution Function
	
	Eigen::SparseMatrix < double, Eigen::RowMajor > matrix_bilin;
	Eigen::ConjugateGradient < Eigen::SparseMatrix < double, Eigen::RowMajor >,
	                           Eigen::Lower | Eigen::Upper                     > solver;

	inline VariationalFormulation ( const Function::Form::Equality & eq )
	:	bilinear_form { eq .lhs }, linear_form { eq .rhs }
	{	}

	inline void add_boundary_condition
		( const tag::dirichlet &, const Function::Equality & eq, const tag::On &, Mesh msh );

	inline Function solve ( );

	void analyse_equation ( );  // also build 'numbering'

	void assemble_matrix ( );
	// implement equilibrium equation, loads, Neumann boundary conditions
	// also apply Dirichlet boundary conditions (they modify the matrix)
	// also prepare Eigen solver

	void solve_linear_system ( Function sol );  // invoke Eigen solver
	
};  // end of  class VariationalFormulation

//------------------------------------------------------------------------------------------------------//


inline void VariationalFormulation::add_boundary_condition
( const tag::dirichlet &, const Function::Equality & eq, const tag::On &, Mesh msh )

{	this->unknown = Function ( tag::whose_core_is,  // calls assert on core
		dynamic_cast < Function::Unknown * > ( eq .lhs .core ) );
	this->bdry_cond .push_back
			( VariationalFormulation::BoundaryCondition::Dirichlet ( eq, msh ) );  }

//------------------------------------------------------------------------------------------------------//


inline Function VariationalFormulation::solve ( )

// return the solution as a 'Function::CoupledWithField' either '::Scalar' or '::Vector'

{	std::shared_ptr < Eigen::VectorXd > vector_sol = std::make_shared < Eigen::VectorXd > ();
	
	this->analyse_equation();  // also build 'this->numbering'
	assert ( this->numbering .exists() );

	Function solution ( tag::coupled_with_array,
	                    tag::use_numbering, this->numbering,
	                    tag::use_vector, vector_sol         );
	this->assemble_matrix();
	// implement equilibrium equation, loads, Neumann boundary conditions
	// also apply Dirichlet boundary conditions (they modify the matrix)
	// also prepare Eigen solver

	this->solve_linear_system ( solution );  // invoke Eigen solver
	
	return  solution;                                                                          }

//------------------------------------------------------------------------------------------------------//


}  // end of  namespace maniFEM

#endif  // ifndef MANIFEM_VAR_FORM_H
