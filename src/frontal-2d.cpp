
//   frontal-2d.cpp  2024.12.25

//   This file is part of maniFEM, a C++ library for meshes and finite elements on manifolds.

//   Copyright  2019 - 2024  Cristian Barbarosie  cristian.barbarosie@gmail.com

//   https://maniFEM.rd.ciencias.ulisboa.pt/
//   https://codeberg.org/cristian.barbarosie/maniFEM

//   ManiFEM is free software: you can redistribute it and/or modify it
//   under the terms of the GNU Lesser General Public License as published
//   by the Free Software Foundation, either version 3 of the License
//   or (at your option) any later version.

//   ManiFEM is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty
//   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//   See the GNU Lesser General Public License for more details.

//   You should have received a copy of the GNU Lesser General Public License
//   along with maniFEM.  If not, see <https://www.gnu.org/licenses/>.


// included by "frontal.cpp"


template < class environ >    // hidden in anonymous namespace
void build_bridge ( Cell & P, std::set < Cell > & s, const Mesh & interf_orig, Mesh & interf_copy,
                    std::vector < typename environ::manif_type::winding_cell > & l                )
// perhaps inline to increase speed

// 'environ' describes the type of manifold we are working in
// and how we treat information about metric and exterior product
// environ::manif_type could be ManifoldNoWinding or ManifoldQuotient (defined in frontal.cpp)
// environ::metric_type may be one of ISRContainer::{Inactive,Active} (defined in frontal.cpp)
// environ::ext_prod_type may be ExtProd2d, 2dRev, Codim1 (defined in frontal.cpp)
	
// P is a recently created vertex, its close neighbours are listed in 'l'
// we want to build a segment joining P to some other node
// since this is done after fill_isolated_triangle and fill_one_triangle,
// there should be no new triangle, just a bridge

// 's' is a set of vertices which we want to avoid
	
// this function is called from fill_two_triangles and tri_out_of_the_blue

// a bridge is just a pair of faces, like a zero-width cell
// ideally, these two faces should be each other's reverse
// however, even STSI meshes are unable to handle such a configuration
// this why we create two independent faces and keep the pair in 'dummy_faces'
// later, the first one will be replaced by the second one's reverse
// this is the job of 'remove_face'

// interf_orig is the boundary provided by the user
// interf_copy is the working copy which contains new segments and vertices
// but may contain not all connected components of interf_orig
	
{	Cell seg_behind_P = interf_copy .cell_behind ( P, tag::surely_exists );
	Cell A = seg_behind_P .base() .reverse ( tag::surely_exists );
	s .insert (A);
	Cell other_seg = interf_copy .cell_behind ( A, tag::seen_from, seg_behind_P, tag::surely_exists );
	Cell B = other_seg .base() .reverse ( tag::surely_exists );
	s .insert (B);
	other_seg = interf_copy .cell_behind ( B, tag::seen_from, other_seg, tag::surely_exists );
	Cell C = other_seg .base() .reverse ( tag::surely_exists );
	s .insert (C);
	Cell seg_in_front_of_P = interf_copy .cell_in_front_of ( P, tag::surely_exists );
	Cell D = seg_in_front_of_P .tip();
	s .insert (D);
	other_seg = interf_copy .cell_in_front_of ( D, tag::seen_from, seg_in_front_of_P, tag::surely_exists );
	Cell E = other_seg .tip();
	s .insert (E);
	other_seg = interf_copy .cell_in_front_of ( E, tag::seen_from, other_seg, tag::surely_exists );
	Cell F = other_seg .tip();
	s .insert (F);

	typename std::vector < typename environ::manif_type::winding_cell > ::iterator it_l = l .begin();
	while ( ( it_l != l .end() ) and ( not s .empty() ) )
	{	Cell S = environ::manif_type::remove_winding ( *it_l );
		assert ( S != P );
		std::set < Cell >  ::iterator it_s = s .find (S);
		if ( it_s != s .end() )
		{	it_l = l .erase ( it_l );  s .erase ( it_s );  }
		else  it_l ++;
		if ( l .empty() ) return;                                }

	#if FRONTAL_VERBOSITY > 1
	std::cout << "frontal-2d.cpp line 2418, build bridge, "
	          << l .size() << " candidates" << std::endl << std::flush;
	#endif

	// there are vertices left in 'l', we want to build a bridge
	// the bridge should be in the forward direction
	// more : if there are several candidates, we choose the most "forward"

	std::vector < double > e = environ::manif_type::get_vector ( seg_behind_P );
	typename environ::ext_prod_type::pointer_vector n  =
		environ::ext_prod_type::get_normal_vector ( tag::domain_2d, tag::at_seg, seg_behind_P );
	typename environ::ext_prod_type::pointer_vector nf =
		environ::ext_prod_type::get_normal_vector ( tag::domain_2d, tag::at_seg, seg_in_front_of_P );
	// although the interface is "looking outwards",
	// towards the zone where we do not intend to build triangles,
	// 'get_normal_vector' returns a normal vector pointing "inwards",
	// that is, towards the zone we intend to mesh
	std::vector < double > forward ( frontal_nb_of_coords );
	for ( size_t i = 0; i < frontal_nb_of_coords; i++ )  forward [i] = n [i] + nf [i];

	// in the above, for the case of co-dim one (surface in RR3),
	// we use a pointer to a vector, wrapped into a PointerVector
	// thus avoiding a copy operation
	// for other cases, we use a vector
	
	#ifndef NDEBUG  // DEBUG mode
	// eliminate nodes backwards
	for ( it_l = l .begin(); it_l != l .end(); )
	{	std::vector < double > direc = environ::manif_type::get_vector ( P, * it_l );
		const double prod = Manifold::working .inner_prod ( P, direc, forward );
		if ( prod < 0. )
		{	assert ( false );  // this never happened until now, so ...
			#if FRONTAL_VERBOSITY > 1
			std::cout << "backwards ";
			for ( size_t i = 0; i < frontal_nb_of_coords; i++ )
			{	const Function & x = Manifold::working .coordinates() [i];
				std::cout << x(P) << " ";                                  }
			for ( size_t i = 0; i < frontal_nb_of_coords; i++ )
			{	const Function & x = Manifold::working .coordinates() [i];
				std::cout << x ( environ::manif_type::remove_winding(*it_l) ) << " ";  }
			std::cout << std::endl;
			#endif  // FRONTAL_VERBOSITY > 1
			it_l = l .erase ( it_l );                                                  }
		else  it_l ++;                                                                  }
	#endif  // DEBUG

	if ( l .empty() ) return;

	// among the remaining nodes, choose the closest, this should be the "most forward"

	it_l = l .begin();
	if ( l .size() > 1 )
	{	double min_dist2 = 100.;
		for ( typename std::vector < typename environ::manif_type::winding_cell > ::iterator
		        itt_l = l .begin(); itt_l != l .end(); itt_l++                             )
		{	std::vector < double > direc = environ::manif_type::get_vector ( P, * itt_l );
			double d2 = Manifold::working .inner_prod ( P, direc, direc );
			if ( d2 < min_dist2 )  {  min_dist2 = d2;  it_l = itt_l;  }                    }  }

	assert ( it_l != l .end() );
	typename environ::manif_type::winding_cell & QQ = *it_l;
	Cell Q = environ::manif_type::remove_winding ( QQ );

	#if FRONTAL_VERBOSITY > 1
	std::cout << "chosen bridge : ";
	for ( size_t i = 0; i < frontal_nb_of_coords; i++ )
	{	const Function & x = Manifold::working .coordinates() [i]; 
		std::cout << x(P) << " ";                                  }
	for ( size_t i = 0; i < frontal_nb_of_coords; i++ )
	{	const Function & x = Manifold::working .coordinates() [i]; 
		std::cout << x(Q) << " ";                                  }
	std::cout << std::endl;
	#endif  // FRONTAL_VERBOSITY > 1

	Cell PQ = environ::manif_type::build_seg ( P .reverse(), QQ );
	// QQ contains information about winding
	Cell QP ( tag::segment, Q .reverse(), P );
	environ::manif_type::set_winding_neg_1 ( QP, PQ );

	tag::Util::insert_new_elem_in_map ( dummy_faces, { QP, PQ } );
	tag::Util::insert_new_elem_in_map ( dummy_faces, { PQ, QP } );
	// dummy_faces [ QP ] = PQ; dummy_faces [ PQ ] = QP;
	// 'QP' will be later replaced by  PQ .reverse(), or the other way around

	typename environ::ext_prod_type::vector_or_void normal_QP =
		environ::ext_prod_type::build_double_normal
		( tag::at_seg, QP, PQ, tag::set_correct_size, tag::use_information_from, P, e, n );
	// return normal at QP

	// in the above, for the case of co-dim one (surface in RR3),
	// we use a pointer to a vector, wrapped into a PointerVector
	// for other cases, we use a  void *
	
	if ( not Q .belongs_to ( interf_copy ) )  // we have to build normals and eigendirs
		
	{	assert ( Q .belongs_to ( interf_orig ) );
		Cell start = interf_orig .cell_behind ( Q, tag::surely_exists );
		assert ( not start .belongs_to ( interf_copy, tag::not_oriented ) );

		build_component ( tag::domain_2d, interf_orig, interf_copy, tag::start_at, start );
		// add faces to interf_copy, seg_fill_isolated, seg_fill_one_tri, seg_fill_two_tri
		// the new part does not touch the old part yet (interface is disconneted)

		assert ( not normal_container .has_vector ( start ) );
		// for cases other than co-dim one, normal_container will be empty anyway

		// if the metric is anisotropic,
		// 'compute_data' uses information from P to compute inner spectral radius at Q
		environ::metric_type::compute_data ( tag::at_point, Q, tag::use_information_from, P );
		// interf_copy has already been enlarged (by build_component)
		// so we may provide either interf_orig or interf_copy to 'compute_data', does not matter
		environ::metric_type::compute_data ( tag::along, interf_copy, tag::start_at, Q );

		environ::ext_prod_type::compute_data
			( tag::domain_2d, tag::at_seg, start, tag::use_information_from, Q, e, normal_QP );
		// build_one_normal < manif_type > ( Q, e, * normal_QP, start, *normal_start );
		environ::ext_prod_type::compute_data
			( tag::domain_2d, tag::along, interf_copy, tag::start_at, start );                   }
		// propagate normal vector
		// build_normals < manif_type, metric_type > ( tag::along, interf_copy, tag::start_at, start );
		// interf_copy has already been enlarged (by build_component)
		// so we may provide either interf_orig or interf_copy to 'compute_data', does not matter
		
	// 'interf_copy' is a Mesh::STSI, we are going to create two singularities at P and at Q
	// method 'add_to' with tag::force breaks existing neighbourhood relations
	Cell seg_in_front_of_Q = interf_copy .cell_in_front_of ( Q, tag::surely_exists );
	Cell seg_behind_Q = interf_copy .cell_behind ( Q, tag::surely_exists );
	QP .add_to ( interf_copy, tag::force );
	PQ .add_to ( interf_copy );
	assert ( interf_copy .cell_in_front_of ( P, tag::seen_from, seg_behind_P, tag::surely_exists )
	         == PQ                                                                                 );
	assert ( interf_copy .cell_in_front_of ( P, tag::seen_from, QP, tag::surely_exists )
	         == seg_in_front_of_P                                                        );
	assert ( interf_copy .cell_in_front_of ( Q, tag::seen_from, PQ, tag::surely_exists )
	         == seg_in_front_of_Q                                                        );
	assert ( interf_copy .cell_in_front_of ( Q, tag::seen_from, seg_behind_Q, tag::surely_exists )
	         == QP                                                                                 );
	assert ( interf_copy .cell_behind ( P, tag::seen_from, PQ, tag::surely_exists )
	         == seg_behind_P                                                        );
	assert ( interf_copy .cell_behind ( P, tag::seen_from, seg_in_front_of_P, tag::surely_exists )
	         == QP                                                                                 );
	assert ( interf_copy .cell_behind ( Q, tag::seen_from, QP, tag::surely_exists )
	         == seg_behind_Q                                                        );
	assert ( interf_copy .cell_behind ( Q, tag::seen_from, seg_in_front_of_Q, tag::surely_exists )
	         == PQ                                                                                 );

	// 'seg_behind_Q' may have already been inserted by 'build_component'
	// ... or not ... since 'build_component' is invoked only when we touch virgin parts of the interface
	seg_fill_one_tri .insert ( seg_behind_P );
	seg_fill_one_tri .insert ( seg_behind_Q );
	seg_fill_one_tri .insert ( PQ );
	seg_fill_one_tri .insert ( QP );
	insert_or_replace ( seg_fill_two_tri, seg_behind_P, 21.);
	insert_or_replace ( seg_fill_two_tri, seg_behind_Q, 21.);
	seg_fill_two_tri .insert ( { PQ, 21.} );
	seg_fill_two_tri .insert ( { QP, 21.} );

	#if FRONTAL_VERBOSITY > 1	
	frontal_counter ++;
	#endif
	
}  // end of  build_bridge
	
//------------------------------------------------------------------------------------------------------//


inline Cell remove_face ( Cell & face, Mesh & interf )    // hidden in anonymous namespace

// remove a face from interf and return it
// special case : when the face belongs to the list of dummy faces

// the returned cell is used for creating a new triangle / tetrahedron
	
// used in 'fill_isolated_triangle', 'fill_two_triangles', 'fill_one_triangle', 'tri_out_of_the_blue'
	
{	face .remove_from ( interf );

	std::map < Cell, Cell > ::iterator it = dummy_faces .find ( face );
	if ( it == dummy_faces .end() )  return face;

	// else  //  it != dummy_faces .end()
	Cell other_face = it->second;
	dummy_faces .erase ( it );
	it = dummy_faces .find ( other_face );
	assert ( it != dummy_faces .end() );
	dummy_faces .erase ( it );
	
	return  other_face .reverse();                                         }

//------------------------------------------------------------------------------------------------------//


template < class manif_type >    // hidden in anonymous namespace
void relocate_core_2d
( const Cell & P, const std::vector < typename manif_type::winding_cell > & neighb, const double tol_2 )

// try to locate P at distance approx 1. to all given neighbours
// we perform a sort of barycenter operation with weights  dist2 - 1.
// thus, vertices which are far pull P, vertices which are close push P

// this is a minimization algorithm for  Phi(P) = sum_A | dist(P,A)^2 - 1 |^3
// see paragraph 13.12 in the manual
	
// tol_2 is the square of the tolerance, i.e. stopping criterion is  ||step|| < tol = sqrt ( tol_2 )
	
// used in 'fill_two_triangles' 'tri_out_of_the_blue',
	
{	
	#if FRONTAL_VERBOSITY > 1
	std::cout << std::endl << "relocate_core_2d " << neighb .size() << ", ";
	#endif
	for ( size_t iter = 0; iter < 100; iter++ )
	{	std::vector < double > delta ( frontal_nb_of_coords, 0. );
		double Phi = 0.;
		for ( typename std::vector < typename manif_type::winding_cell > ::const_iterator
		      it = neighb .begin(); it != neighb .end(); it++                             )
		{	const typename manif_type::winding_cell & AA = *it;
			const std::vector < double > e = manif_type::get_vector ( P, AA );
			// note that e[i] is not P_i - A_i but A_i - P_i
			Cell A = manif_type::remove_winding ( AA );
			const double dist2 = Manifold::working .sq_dist ( P, A, e );
	#if FRONTAL_VERBOSITY > 1
			std::cout << std::sqrt ( dist2 ) << " ";
	#endif
			const double coef = dist2 - 1.;
			const double acoef = std::abs ( coef );
			Phi += coef * coef * acoef;
			for ( size_t i = 0; i < frontal_nb_of_coords; i++ )
				delta [i] += coef * acoef * e[i];                                 }
			// if the metric is isotropic, delta is minus the gradient of Phi
			// if the metric is anisotropic, delta is not minus the gradient of Phi
			// however, it is a reasonable descent direction
	#if FRONTAL_VERBOSITY > 1
		std::cout << std::endl;
	#endif
		// now we choose the length of the step using a one-dimensional Newton method
		double D_Phi_delta = 0., D2_Phi_delta_delta_part_1 = 0., D2_Phi_delta_delta_part_2 = 0.;
		for ( typename std::vector < typename manif_type::winding_cell > ::const_iterator
		      it = neighb .begin(); it != neighb .end(); it++                           )
		{	const typename manif_type::winding_cell & AA = *it;
			const std::vector < double > e = manif_type::get_vector ( P, AA );
			// note that e[i] is not P_i - A_i but A_i - P_i
			Cell A = manif_type::remove_winding ( AA );
			const double dist2 = Manifold::working .sq_dist ( P, A, e );
			const double coef = dist2 - 1.;
			const double acoef = std::abs ( coef );
			double prod_e_delta = Manifold::working .inner_prod ( P, A, e, delta );
			D_Phi_delta += coef * acoef * prod_e_delta;
			// below we add 0.3 to 'acoef' to stabilize the optimization process
			D2_Phi_delta_delta_part_1 += ( acoef + 0.3 ) * prod_e_delta * prod_e_delta;
			D2_Phi_delta_delta_part_2 += acoef * acoef * Manifold::working .sq_dist ( P, A, delta );  }
		// below there is no minus sign because e[i] is not P_i - A_i but A_i - P_i
		double eta = D_Phi_delta / ( 4 * D2_Phi_delta_delta_part_1 + D2_Phi_delta_delta_part_2 );
		eta *= 0.7;
	#if FRONTAL_VERBOSITY > 1
		std::cout << "0 " << D_Phi_delta << ", 1 " << D2_Phi_delta_delta_part_1 << ", 2 "
							<< D2_Phi_delta_delta_part_2;
		std::cout << ", eta " << eta << ", Phi " << Phi;
	#endif
		for ( size_t i = 0; i < frontal_nb_of_coords; i++ )  delta [i] *= eta;
		double norm_2 = Manifold::working .inner_prod ( P, delta, delta );
		// std::cout << std::sqrt ( Manifold::working .inner_prod ( P, delta, delta ) ) << std::endl;
		Manifold::working .core->move_vertex ( P, delta, norm_2 );
		// move P, project it if necessary, may change norm_2
	#if FRONTAL_VERBOSITY > 1
		std::cout << ", norm " << std::sqrt ( norm_2 ) << std::endl << std::flush;
	#endif
		if ( norm_2 < tol_2 )  return;  //  norm of delta < tol
	}  // end of  for iter

	std::cout << std::endl << "frontal mesh generation process is unstable" << std::endl;
	std::cout << "perhaps the curvature of your manifold is too high ?" << std::endl;
	std::cout << "one possible solution is to decrease the segment size" << std::endl;
	exit (1);

}  // end of  relocate_core_2d

//------------------------------------------------------------------------------------------------------//


template < class manif_type >            // hidden in anonymous namespace
bool compare_two_lists ( const Cell & P,
                         std::vector < typename manif_type::winding_cell > & already,
                         std::list < typename manif_type::winding_cell > & candidates,
                         const std::set < Cell > & inadmissible_ver, const double dist_sq )

// used in 'fill_two_triangles' 'tri_out_of_the_blue',

// well, the two lists are actually a vector and a list
// and may become soon two vectors
	
// return true if 'candidates' included in 'already'

// 'already' contains vertices already taken into account when locating a new vertex
// 'candidates' contains vertices close enough (sqrt of two) to the newly created vertex P
// 'candidates' is the result of a call to 'cloud .find_close_neighbours_of (P)'
// one of the candidates not already in 'already' (the closest to P)
// will be added to 'already', then return false

// 'inadmissible_ver' is a set of vertices "behind the front line", not to be taken into account
	
// we do not assume that 'already' is included in 'candidates'
// because 'candidates' may have shrunk along the relocation process

// perhaps transform 'already' in a set of Cells, just like 'inadmissible_ver'

// why the fourth argument 'dist_sq' ?
// if the metric is anisotropic, the list 'candidates', produced by 'find_close_neighbours_of',
// may contain vertices which are too far from P
// so we filter it with 'dist_sq'

// even for an isotropic, non-uniform, metric, some vertices may be too far from P
// because here we use the distance averaged at P and V
// while in 'find_close_neighbours_of' we use inner_spectral_radius at P only
	
{	// first, we erase from 'candidates' all cells belonging to 'already'
	for ( typename std::vector < typename manif_type::winding_cell > ::iterator
	      it_al = already .begin(); it_al != already .end(); it_al ++        )
	{	for ( typename std::list < typename manif_type::winding_cell > ::iterator
		      it_ca = candidates .begin(); it_ca != candidates .end();           )
			// if ( *it_al == *it_ca )   should work but does not ...
			// if ( it_al->first == it_ca->first )  does not work if manif_type is no-winding
			if ( manif_type::same_cell ( *it_al, *it_ca ) )  it_ca = candidates .erase ( it_ca );
			else  it_ca++;
		// assert ( found )  // unfortunately we cannot assume that 'already' is included in 'candidates'
		if ( candidates .empty() ) return true;  // 'candidates' included in 'already'
	} // end of for 'already'

	// in rare configurations, the algorithm senses vertices behind a thin strip (of width approx 1.)
	// 'inadmissible_ver' prevents that
	// exception : if the distance is small, keep that vertex
	for ( typename std::list < typename manif_type::winding_cell > ::iterator
	      it_ca = candidates .begin(); it_ca != candidates .end();           )
	{	typename manif_type::winding_cell V = * it_ca;
		std::set < Cell > ::iterator it_in =
			inadmissible_ver .find ( manif_type::remove_winding (V) );
  	if ( ( it_in != inadmissible_ver .end() )
		     and ( manif_type::sq_dist::working_manif ( P, V ) > 0.8 ) )
			it_ca = candidates .erase ( it_ca );
		else  it_ca++;                                                    }
	if ( candidates .empty() ) return true;
	
	double d2min = 1000.;
	typename std::list < typename manif_type::winding_cell > ::iterator kept;
	for ( typename std::list < typename manif_type::winding_cell > ::iterator
	      it_ca = candidates .begin(); it_ca != candidates .end(); it_ca ++  )
	{	typename manif_type::winding_cell V = * it_ca;
		double d2 = manif_type::sq_dist::working_manif ( P, V );  // rescaled metric
		if ( d2 < d2min )
		{	d2min = d2;  kept = it_ca;  }                          }
	if ( d2min > dist_sq )
	{	// list 'candidates' contains vertices too far from P, due to the anisotropy
		#if FRONTAL_VERBOSITY > 1
		std::cout << "(" << std::sqrt(d2min) << ", " << candidates .size() << " vertices too far) ";
		#endif
		return true;                                                                                  }

	#if FRONTAL_VERBOSITY > 1
	std::cout << " [ ";
	for ( size_t i = 0; i < frontal_nb_of_coords; i++ )
	{	const Function & x = Manifold::working .coordinates() [i];
		std::cout << x (manif_type::remove_winding(*kept)) << " ";  }
	std::cout << "] ";
	#endif  // FRONTAL_VERBOSITY > 1
		
	already .push_back ( *kept );
	return false;
		
}  // end of  compare_two_lists


//------------------------------------------------------------------------------------------------------//
//------------------------------------------------------------------------------------------------------//


template < class environ >
inline bool fill_isolated_triangle ( Mesh & msh, Mesh & interf_copy )   // hidden in anonymous namespace
	
// try to fill an isolated triangle
// return false if did nothing

// 'msh' is a Mesh::Fuzzy made of triangles (being built now)
// 'interf_copy' is a Mesh::STSI made of segments
	
// 'environ' describes the type of manifold we are working in
// and how we treat information about metric and exterior product
// environ::manif_type could be ManifoldNoWinding or ManifoldQuotient (defined in frontal.cpp)
// environ::metric_type may be one of ISRContainer::{Inactive,Active} (defined in frontal.cpp)
// environ::ext_prod_type may be ExtProd2d, 2dRev, Codim1 (defined in frontal.cpp)
	
{	for ( std::set < Cell > ::iterator it = seg_fill_isolated .begin();
	      it != seg_fill_isolated .end();                              )
	{	Cell AB = *it;
		it = seg_fill_isolated .erase ( it );
 		assert ( AB .belongs_to ( interf_copy, tag::same_dim, tag::orientation_compatible_with_mesh ) );
		Cell B = AB .tip();
		Cell A = AB .base() .reverse ( tag::surely_exists );
		Cell BC = interf_copy .cell_in_front_of ( B, tag::seen_from, AB, tag::surely_exists );
		Cell C = BC .tip();
		Cell before_A = interf_copy .cell_behind ( A, tag::seen_from, AB, tag::surely_exists );
		Cell after_C = interf_copy .cell_in_front_of ( C, tag::seen_from, BC, tag::surely_exists );
		if ( before_A != after_C )  continue;

		std::vector < double > e = environ::manif_type::get_vector ( BC );
		typename environ::ext_prod_type::pointer_vector n =
			environ::ext_prod_type::get_normal_vector ( tag::domain_2d, tag::at_seg, AB );
		// although the interface is "looking outwards",
		// towards the zone where we do not intend to build triangles,
		// 'get_normal_vector' returns a normal vector pointing "inwards",
		// that is, towards the zone we intend to mesh
		const double prod = environ::ext_prod_type::inner_prod ( B, e, n );
		// we wrap 'Manifold::working .inner_prod' into 'environ::ext_prod_type::inner_prod'
		// because sometimes n is a PointerVector
		if ( prod < 0. )  continue;

		Cell AB_alt = remove_face ( AB,      interf_copy );
		Cell BC_alt = remove_face ( BC,      interf_copy );
		Cell CA_alt = remove_face ( after_C, interf_copy );
		// AB      .remove_from ( interf_copy );
		// BC      .remove_from ( interf_copy );
		// after_C .remove_from ( interf_copy );
		Cell tri ( tag::triangle, AB_alt, BC_alt, CA_alt );
		tri .add_to ( msh );
	  environ::ext_prod_type::erase_normal_vector ( tag::at_seg, AB );
		environ::ext_prod_type::erase_normal_vector ( tag::at_seg, BC );
		environ::ext_prod_type::erase_normal_vector ( tag::at_seg, after_C );
		environ::manif_type::node_container .cond_remove ( A, interf_copy );
		environ::manif_type::node_container .cond_remove ( B, interf_copy );
		environ::manif_type::node_container .cond_remove ( C, interf_copy );
		environ::metric_type::cond_remove ( A, interf_copy );
		environ::metric_type::cond_remove ( B, interf_copy );
		environ::metric_type::cond_remove ( C, interf_copy );
		// AB has already been eliminated from seg_fill_isolated, at the beginning of the loop
		seg_fill_isolated .erase ( BC );
		seg_fill_isolated .erase ( after_C );
		seg_fill_one_tri  .erase ( AB );
		seg_fill_one_tri  .erase ( BC );
		seg_fill_one_tri  .erase ( after_C );
		#if FRONTAL_VERBOSITY > 1
		frontal_counter ++;
		std::cout << "frontal-2d.cpp line 2679, fill isolated triangle" << std::endl << std::flush;
		#endif
		return true;                                                                                   }

	return false;

}  // end of fill_isolated_triangle	
	
//------------------------------------------------------------------------------------------------------//


template < class environ >
inline bool fill_one_triangle    // hidden in anonymous namespace
( Mesh & msh, Mesh & interf )
	
// try to fill a new triangle just by joining
// the two neighbour vertices by a new segment (60 degree-angle, sort of)
// return false if did nothing

// 'msh' is a Mesh::Fuzzy made of triangles (being built now)
// 'interf' is a Mesh::STSI made of segments
	
// 'environ' describes the type of manifold we are working in
// and how we treat information about metric and exterior product
// environ::manif_type could be ManifoldNoWinding or ManifoldQuotient (defined in frontal.cpp)
// environ::metric_type may be one of ISRContainer::{Inactive,Active} (defined in frontal.cpp)
// environ::ext_prod_type may be ExtProd2d, 2dRev, Codim1 (defined in frontal.cpp)
	
{	assert ( seg_fill_isolated .empty() );

	for ( std::set < Cell > ::iterator it  = seg_fill_one_tri .begin();
	                                   it != seg_fill_one_tri .end();  )
	{	Cell AB = *it;
		it = seg_fill_one_tri .erase ( it );
 		assert ( AB .belongs_to ( interf, tag::same_dim, tag::orientation_compatible_with_mesh ) );
		Cell B = AB .tip();
		Cell A = AB .base() .reverse ( tag::surely_exists );
		Cell BC = interf .cell_in_front_of ( B, tag::seen_from, AB, tag::surely_exists );
		Cell C = BC .tip();
		const std::vector < double > e_AC =
			environ::manif_type::get_vector ( A, environ::manif_type::add_winding_2 ( C, AB, BC ) );
		const double AC2 = Manifold::working .sq_dist ( A, C, e_AC );
		if ( AC2 > 2.) continue;  // frontal_seg_size 2D
		// AC < sqrt 2, we may want to fill the triangle ABC
		// check curvature :
		const std::vector < double > e_BC = environ::manif_type::get_vector ( BC );
		typename environ::ext_prod_type::pointer_vector n_AB =
			environ::ext_prod_type::get_normal_vector ( tag::domain_2d, tag::at_seg, AB );
		// although the interface is "looking outwards",
		// towards the zone where we do not intend to build triangles,
		// 'get_normal_vector' returns a normal vector pointing "inwards",
		// that is, towards the zone we intend to mesh
		if ( environ::ext_prod_type::inner_prod ( B, e_BC, n_AB ) < 0. )  continue;
		// we wrap 'Manifold::working .inner_prod' into 'environ::ext_prod_type::inner_prod'
		// because sometimes n_AB is a PointerVector
		// do not create triangle if angle is wide :
		const std::vector < double > e_AB = environ::manif_type::get_vector ( AB );
		if ( Manifold::working .inner_prod ( B, e_AB, e_BC ) > 0. )  continue;

		#if FRONTAL_VERBOSITY > 1
		std::cout << "frontal-2d.cpp line 2769, insert one tri";
		std::cout << " " << std::sqrt ( Manifold::working .sq_dist ( A, C, e_AC ) )
		          << std::endl << std::flush;
		#endif
		Cell AB_alt = remove_face ( AB, interf );
		Cell BC_alt = remove_face ( BC, interf );
		// AB .remove_from ( interf );
		// BC .remove_from ( interf );
		Cell AC ( tag::segment, A .reverse(), C );
		environ::manif_type::set_winding_2 ( AC, AB, BC );
		AC .add_to ( interf );
		// build_one_normal < environ::manif_type > ( A, e_AB, * n_AB, AC, * normal_AC );
		environ::ext_prod_type::compute_data
			( tag::domain_2d, tag::at_seg, AC, tag::use_information_from, A, e_AB, n_AB );
	  environ::ext_prod_type::erase_normal_vector ( tag::at_seg, AB );
		environ::ext_prod_type::erase_normal_vector ( tag::at_seg, BC );
		Cell tri ( tag::triangle, AB_alt, BC_alt, AC .reverse() );
		tri .add_to ( msh );
		environ::manif_type::node_container .cond_remove ( B, interf );
		environ::metric_type::cond_remove ( B, interf );
		seg_fill_one_tri .erase ( BC );
		Cell before_A = interf .cell_behind ( A, tag::seen_from, AC, tag::surely_exists );
		seg_fill_isolated .insert ( AC );
		seg_fill_one_tri  .insert ( before_A );
		seg_fill_one_tri  .insert ( AC );
		insert_or_replace ( seg_fill_two_tri, before_A, 21.);
		seg_fill_two_tri  .insert ( { AC, 21.} );
		#if FRONTAL_VERBOSITY > 1	
		frontal_counter ++;
		#endif
		return true;                                                                              }

	return false;

}  // end of  fill_one_triangle	
	
//------------------------------------------------------------------------------------------------------//


template < class environ >
inline void fill_isolated_quadrangle    // hidden in anonymous namespace
( Mesh & msh, const Mesh & interf_orig, Mesh & interf_copy,
  Cell A, Cell B, Cell C, Cell D, Cell AB, Cell BC, Cell CD, Cell DA )

{	if ( environ::manif_type::sq_dist::working_manif
	     ( A, environ::manif_type::add_winding_2 ( C, AB, BC ) )
	     > environ::manif_type::sq_dist::working_manif
	       ( B, environ::manif_type::add_winding_2 ( D, BC, CD ) ) )
	{	A = B;  C = D;
		Cell tmp = AB;
		AB = BC;  BC = CD;  CD = DA;  DA = tmp;  }
	
	#if FRONTAL_VERBOSITY > 1
	std::cout << "frontal-2d.cpp line 3546, fill quadrangle" << std::endl << std::flush;
	#endif
	
	Cell AC ( tag::segment, A .reverse(), C );
	environ::manif_type::set_winding_2 ( AC, AB, BC );
	AB = remove_face ( AB, interf_copy );
	BC = remove_face ( BC, interf_copy );
	// AB .remove_from ( interf_copy );
	// BC .remove_from ( interf_copy );

	// CD and DA may not belong to interf_copy, we have to ask
	if ( CD .belongs_to ( interf_copy, tag::same_dim, tag::orientation_compatible_with_mesh ) )
		CD = remove_face ( CD, interf_copy );
		// CD .remove_from ( interf_copy );
	if ( DA .belongs_to ( interf_copy, tag::same_dim, tag::orientation_compatible_with_mesh ) )
		DA = remove_face ( DA, interf_copy );
		// DA .remove_from ( interf_copy );
	Cell ABC ( tag::triangle, AB, BC, AC .reverse() );
	Cell CDA ( tag::triangle, CD, DA, AC );
	ABC .add_to ( msh );
	CDA .add_to ( msh );

}  // end of  fill_isolated_quadrangle

//------------------------------------------------------------------------------------------------------//


template < class environ >
inline bool fill_two_triangles    // hidden in anonymous namespace
( Mesh & msh, const Mesh & interf_orig, Mesh & interf_copy )
	
// try to fill two new triangles (120 degree-angle, sort of)
// we search for the best place to do this
// by choosing the largest value of prod (line 2072)
// return false if did nothing

// 'msh' is a Mesh::Fuzzy made of triangles (being built now)
// 'interf_orig' is a mesh made of segments
// 'interf_copy' is a Mesh::STSI made of segments
	
// 'environ' describes the type of manifold we are working in
// and how we treat information about metric and exterior product
// environ::manif_type could be ManifoldNoWinding or ManifoldQuotient (defined in frontal.cpp)
// environ::metric_type may be one of ISRContainer::{Inactive,Active} (defined in frontal.cpp)
	
{	assert ( seg_fill_isolated .empty() );
	assert ( seg_fill_one_tri  .empty() );
	// twice the height of an equilateral triangle : sqrt 3

	// first, eliminate wide angles
	for ( std::map < Cell, double > ::iterator it  = seg_fill_two_tri .begin();
	                                           it != seg_fill_two_tri .end();  )
	{	Cell AB = it->first;
		if ( not AB .belongs_to ( interf_copy, tag::same_dim, tag::orientation_compatible_with_mesh ) )
		{	it = seg_fill_two_tri .erase ( it );  continue;  }  // to next segment in seg_fill_two_tri
		if ( it->second < 20. )
		{	it ++;  continue;  }  // to next segment in seg_fill_two_tri
		Cell B = AB .tip();
		Cell A = AB .base() .reverse ( tag::surely_exists );
		Cell BC = interf_copy .cell_in_front_of ( B, tag::seen_from, AB, tag::surely_exists );
		Cell C = BC .tip();
		const std::vector < double > e_AC = environ::manif_type::get_vector
			( A, environ::manif_type::add_winding_2 ( C, AB, BC ) );
		const double AC2 = Manifold::working .sq_dist ( A, C, e_AC );
		// if AC is too long, we reject the case
		if ( AC2 > 3.7 ) 
		{	it = seg_fill_two_tri .erase ( it );  continue;  }  // to next segment in seg_fill_two_tri
		const std::vector < double > e_BC = environ::manif_type::get_vector ( BC );
		typename environ::ext_prod_type::pointer_vector n_AB =
			environ::ext_prod_type::get_normal_vector ( tag::domain_2d, tag::at_seg, AB );
		// although the interface is "looking outwards",
		// towards the zone where we do not intend to build triangles,
		// 'get_normal_vector' returns a normal vector pointing "inwards",
		// that is, towards the zone we intend to mesh
		const double prod = environ::ext_prod_type::inner_prod ( B, e_BC, n_AB );
		// we wrap 'Manifold::working .inner_prod' into 'environ::ext_prod_type::inner_prod'
		// because sometimes n_AB is a PointerVector
		// if the angle is too wide, we reject the case
		if ( prod < 0.2 )
		{	it = seg_fill_two_tri .erase ( it );  continue;  }  // to next segment in seg_fill_two_tri
		it++;                                                                                   }

	if ( seg_fill_two_tri .empty() )  return false;
	#if FRONTAL_VERBOSITY > 1
	std::cout << "frontal-2d.cpp line 3627, seg_fill_two_tri size "
	          << seg_fill_two_tri .size() << std::endl << std::flush;
	#endif
	
	Cell kept_AB ( tag::non_existent );
	double prod_min = 20.;
	for ( std::map < Cell, double > ::iterator it  = seg_fill_two_tri .begin();
	                                           it != seg_fill_two_tri .end(); it++ )
	{	Cell AB = it->first;
		assert ( AB .belongs_to ( interf_copy, tag::same_dim, tag::orientation_compatible_with_mesh ) );
		if ( it->second > 20. )
		{	Cell B = AB .tip();
			Cell BC = interf_copy .cell_in_front_of ( B, tag::seen_from, AB, tag::surely_exists );
			const std::vector < double > e_AB = environ::manif_type::get_vector ( AB );
			const std::vector < double > e_BC = environ::manif_type::get_vector ( BC );
			it->second = Manifold::working .inner_prod ( B, e_AB, e_BC );                           }
		const double prod = it->second;
		if ( prod < prod_min )
		{	prod_min = prod;
			kept_AB = AB;      }                                                                      }

  // fill two triangles around 'kept_B' (or a quadrangle)
	assert ( kept_AB .exists() );
	Cell B = kept_AB .tip();
	Cell BC = interf_copy .cell_in_front_of ( B, tag::seen_from, kept_AB, tag::surely_exists );
	Cell A = kept_AB .base() .reverse ( tag::surely_exists );
	Cell C = BC .tip();
	std::vector < double > e_AB = environ::manif_type::get_vector ( kept_AB );
	typename environ::ext_prod_type::pointer_vector n_AB =
		environ::ext_prod_type::get_normal_vector ( tag::domain_2d, tag::at_seg, kept_AB );
	// although the interface is "looking outwards",
	// towards the zone where we do not intend to build triangles,
	// 'get_normal_vector' returns a normal vector pointing "inwards",
	// that is, towards the zone we intend to mesh
	// 'n_AB' is only used (three times) in compute_data, we should eliminate it !

	// before building a new vertex, check for an isolated quadrangle
	Cell after_C  = interf_copy .cell_in_front_of ( C, tag::seen_from, BC, tag::surely_exists );
	Cell before_A = interf_copy .cell_behind      ( A, tag::seen_from, kept_AB, tag::surely_exists );
	Cell D = after_C .tip();
	if ( D == before_A .base() .reverse ( tag::surely_exists ) )  // yes, fill quadrangle
	{	fill_isolated_quadrangle < environ >
			( msh, interf_orig, interf_copy, A, B, C, D, kept_AB, BC, after_C, before_A );
		#if FRONTAL_VERBOSITY > 1	
		frontal_counter ++;
		#endif
		environ::manif_type::node_container .cond_remove ( A, interf_copy );
		environ::manif_type::node_container .cond_remove ( B, interf_copy );
		environ::manif_type::node_container .cond_remove ( C, interf_copy );
		environ::manif_type::node_container .cond_remove ( D, interf_copy );
		environ::metric_type::cond_remove ( A, interf_copy );
		environ::metric_type::cond_remove ( B, interf_copy );
		environ::metric_type::cond_remove ( C, interf_copy );
		environ::metric_type::cond_remove ( D, interf_copy );
		environ::ext_prod_type::erase_normal_vector ( tag::at_seg, kept_AB );
		environ::ext_prod_type::erase_normal_vector ( tag::at_seg, BC );
		environ::ext_prod_type::erase_normal_vector ( tag::at_seg, after_C );
		environ::ext_prod_type::erase_normal_vector ( tag::at_seg, before_A );
		return true;                                                                      }
		
	// search for an almost isolated quadrangle (only one side missing)
	Cell Z = before_A .base() .reverse ( tag::surely_exists );
	const double dist_AD = environ::manif_type::sq_dist::working_manif
		( A, environ::manif_type::add_winding_3 ( D, kept_AB, BC, after_C ) );
	const double dist_CZ = environ::manif_type::sq_dist::working_manif
		( Z, environ::manif_type::add_winding_3 ( C, before_A, kept_AB, BC ) );
	// assert ( ( not A_close_to_D ) or ( not C_close_to_Z ) );
	if ( ( dist_AD < 1.5376 ) or ( dist_CZ < 1.5376 ) )
  // 1.5376 = 1.24 ^2 = ( frontal_seg_size + epsilon ) ^2
	{	if ( dist_CZ < dist_AD )  // switch cells
		{	D = C;  C = B;  B = A;  A = Z;
			after_C = BC;  BC = kept_AB;  kept_AB = before_A;  }
		before_A = interf_copy .cell_behind ( A, tag::seen_from, kept_AB, tag::surely_exists );
		Cell AD ( tag::segment, A .reverse ( tag::surely_exists ), D );
		environ::manif_type::set_winding_3 ( AD, kept_AB, BC, after_C );
		Cell DA = AD .reverse ( tag::does_not_exist, tag::build_now );
		fill_isolated_quadrangle < environ >
			( msh, interf_orig, interf_copy, A, B, C, D, kept_AB, BC, after_C, DA );
		AD .add_to ( interf_copy );
		// build normal to AD
		// build_one_normal < manif_type > ( A, e_AB, * n_AB, AD, * normal_AD );
		environ::ext_prod_type::compute_data
			( tag::domain_2d, tag::at_seg, AD, tag::use_information_from, A, e_AB, n_AB );
		seg_fill_isolated .insert ( AD );
		seg_fill_one_tri .insert ( before_A );
		seg_fill_one_tri .insert ( AD );
		insert_or_replace ( seg_fill_two_tri, before_A, 21.);
		seg_fill_two_tri .insert ( { AD, 21.} );
		#if FRONTAL_VERBOSITY > 1	
		frontal_counter ++;
		#endif
		environ::manif_type::node_container .cond_remove ( B, interf_copy );
		environ::manif_type::node_container .cond_remove ( C, interf_copy );
		environ::metric_type::cond_remove ( B, interf_copy );
		environ::metric_type::cond_remove ( C, interf_copy );
		environ::ext_prod_type::erase_normal_vector ( tag::at_seg, kept_AB );
		environ::ext_prod_type::erase_normal_vector ( tag::at_seg, BC );
		environ::ext_prod_type::erase_normal_vector ( tag::at_seg, after_C );
		return true;                                                                            }
	
	// else  // no quadrangle, create new vertex and fill two triangles

	// change code below, mimic what is done for tetrahedra in 'relocate_3d'
	
	// in most cases this is a complete waste of time, but in rare configurations
	// the algorithm senses vertices behind a thin strip (of width approx 1.)
	// we must prevent that
	std::set < Cell > inadmissible_ver;
	{ // just a block of code for hiding admissible_ver, Q, R, QR
	std::set < Cell > admissible_ver;
	Cell R = B;
	admissible_ver .insert ( R );
	Cell QR = kept_AB;
	for ( size_t i = 0; i < 4; i++ )
	{	QR = interf_copy .cell_in_front_of ( R, tag::seen_from, QR, tag::surely_exists );
		R = QR .tip();
		admissible_ver .insert ( R );                                                    }
	Cell Q = B;
	QR = BC;
	for ( size_t i = 0; i < 4; i++ )
	{	QR = interf_copy .cell_behind ( Q, tag::seen_from, QR, tag::surely_exists );
		Q = QR .base() .reverse ( tag::surely_exists );
		admissible_ver .insert ( Q );                                                }
	for ( std::set < Cell > ::iterator it  = admissible_ver .begin();
	                                   it != admissible_ver .end();   it++ )
	{	Cell V = *it;
		if ( not V .belongs_to ( msh ) )  continue;
		Mesh::Iterator itt = msh .iterator ( tag::over_vertices, tag::connected_to, V );
		for ( itt .reset(); itt .in_range(); itt++ )
		{	Cell W = *itt;
			std::set < Cell > ::iterator found = admissible_ver .find (W);
			if ( found == admissible_ver .end() )
				inadmissible_ver .insert (W);                                 }         }
	} // just a block of code
	
	std::vector < double > e_BC = environ::manif_type::get_vector ( BC );
	Cell P ( tag::vertex );
	for ( size_t i = 0; i < frontal_nb_of_coords; i++ )
	{	const Function & x = Manifold::working .coordinates() [i]; 
		x(P) = x ( B ) - e_AB [i] + e_BC [i];                      }
	// we consider both  B  and  P  have zero winding
	Manifold::working .project (P);
	// 'compute_data' uses information from B to compute eigendirection and isr at P
	environ::metric_type::compute_data ( tag::at_point, P, tag::use_information_from, B );

	// l1 contains vertices already known to be close to P
	// let l1 be a set instead ?
	std::vector < typename environ::manif_type::winding_cell > l1;  // { A, B, C }
	l1 .reserve (8);
	l1 .push_back ( environ::manif_type::add_winding_neg_1 ( A, kept_AB ) );
	l1 .push_back ( environ::manif_type::add_winding   ( B, 0 ) );
	l1 .push_back ( environ::manif_type::add_winding_1 ( C, BC ) );
	#if FRONTAL_VERBOSITY > 1
	std::cout << "frontal-2d.cpp line 3132, insert two tri ";
	#endif
	
	while ( true )
	{	relocate_core_2d < typename environ::manif_type > ( P, l1, 0.0004 );
		std::list < typename environ::manif_type::winding_cell > l2 =   // was tag::Util::sqrt_2
			environ::manif_type::node_container .find_close_neighbours_of ( P, frontal_seg_size );
		#if FRONTAL_VERBOSITY > 1
		std::cout << l1 .size() << l2 .size() << " " << std::flush;
		#endif
		if ( compare_two_lists < typename environ::manif_type >
				 ( P, l1, l2, inadmissible_ver, frontal_seg_size_2 ) )  break;                        }
	relocate_core_2d < typename environ::manif_type > ( P, l1, 0.0001 );
	#if FRONTAL_VERBOSITY > 1
	std::cout << ": " << std::sqrt ( environ::manif_type::sq_dist::working_manif  // rescaled metric
	                     ( A, environ::manif_type::add_winding ( P, 0 ) ) );
	std::cout << " " << std::sqrt ( environ::manif_type::sq_dist::working_manif  // rescaled metric
	                     ( B, environ::manif_type::add_winding ( P, 0 ) ) );
	std::cout << " " << std::sqrt ( environ::manif_type::sq_dist::working_manif  // rescaled metric
	                     ( C, environ::manif_type::add_winding ( P, 0 ) ) );
	std::cout << std::endl << std::flush;
	#endif  // FRONTAL_VERBOSITY > 1

	Cell AB_alt = remove_face ( kept_AB, interf_copy );
	Cell BC_alt = remove_face ( BC, interf_copy );
	// kept_AB .remove_from ( interf_copy );
	// BC .remove_from ( interf_copy );
	Cell AP ( tag::segment, A .reverse(), P );
	environ::manif_type::set_winding_1 ( AP, kept_AB );
	Cell BP ( tag::segment, B .reverse(), P );   // zero winding
	Cell PC ( tag::segment, P .reverse(), C );
	environ::manif_type::set_winding_1 ( PC, BC );
	// build normals to AP and PC
	// build_one_normal < manif_type > ( P, e_AB, * n_AB, AP, * normal_AP );
	environ::ext_prod_type::compute_data
		( tag::domain_2d, tag::at_seg, AP, tag::use_information_from, P, e_AB, n_AB );
	// build_one_normal < manif_type > ( P, e_AB, * n_AB, PC, * normal_PC );
	environ::ext_prod_type::compute_data
		( tag::domain_2d, tag::at_seg, PC, tag::use_information_from, P, e_AB, n_AB );
  environ::ext_prod_type::erase_normal_vector ( tag::at_seg, kept_AB );
  environ::ext_prod_type::erase_normal_vector ( tag::at_seg, BC );
	AP .add_to ( interf_copy );
	PC .add_to ( interf_copy );
	Cell ABP ( tag::triangle, AB_alt, BP, AP .reverse() );
	Cell BCP ( tag::triangle, BC_alt, PC .reverse(), BP .reverse() );
	ABP .add_to ( msh );
	BCP .add_to ( msh );
	environ::manif_type::node_container .cond_remove ( B, interf_copy );
	environ::metric_type::cond_remove ( B, interf_copy  );
	environ::manif_type::node_container .insert (P);
	seg_fill_one_tri .insert ( before_A );
	seg_fill_one_tri .insert ( PC );
	insert_or_replace ( seg_fill_two_tri, before_A, 21.);
	seg_fill_two_tri .insert ( { PC, 21.} );
	//  seg_fill_two_tri [ PC ] == 21.

	#if FRONTAL_VERBOSITY > 1
	frontal_counter ++;
	if ( frontal_counter >= frontal_counter_max )  return true;
	#endif
	if ( l1 .size() > 3 )
	{	std::set < Cell > s { B };
		build_bridge < environ > ( P, s, interf_orig, interf_copy, l1 );  }
		// empties l1

	return true;

}  // end of fill_two_triangles
	
//------------------------------------------------------------------------------------------------------//


template < class environ >
inline void tri_out_of_the_blue      // hidden in anonymous namespace
( Mesh & msh, const Mesh & interf_orig, Mesh & interf_copy )

// build a brand new triangle
	
// 'msh' is a Mesh::Fuzzy made of triangles (being built now)
// 'interf_orig' is a mesh made of segments
// 'interf_copy' is a Mesh::STSI made of segments
	
// 'environ' describes the type of manifold we are working in
// and how we treat information about metric and exterior product
// environ::manif_type could be ManifoldNoWinding or ManifoldQuotient (defined in frontal.cpp)
// environ::metric_type may be one of ISRContainer::{Inactive,Active} (defined in frontal.cpp)
// environ::ext_prod_type may be ExtProd2d, 2dRev, Codim1 (defined in frontal.cpp)
	
{	assert ( seg_fill_isolated .empty() );
	assert ( seg_fill_one_tri  .empty() );
	assert ( seg_fill_two_tri  .empty() );

	Mesh::Iterator it = interf_copy .iterator
		( tag::over_segments, tag::orientation_compatible_with_mesh );
	it .reset();  assert ( it .in_range() );
	Cell AB = *it;
	const Cell & A = AB .base() .reverse ( tag::surely_exists );
	const Cell & B = AB .tip();
	std::vector < double > e_AB = environ::manif_type::get_vector ( AB );
	typename environ::ext_prod_type::pointer_vector n_AB =
		environ::ext_prod_type::get_normal_vector ( tag::domain_2d, tag::at_seg, AB );
	// although the interface is looking "outwards",
	// towards the zone where we do not intend to build triangles,
	// 'get_normal_vector' returns a normal vector pointing "inwards",
	// that is, towards the zone we intend to mesh

	// change code below, mimic what is done for tetrahedra in 'relocate_3d'

	std::set < Cell > inadmissible_ver;
	// if something goes wrong, fill in 'inadmissible_ver' just like in 'fill_two_triangles',
	// perhaps with  i < 5  instead of  i < 4

	Cell P ( tag::vertex );
	for ( size_t i = 0; i < frontal_nb_of_coords; i++ )
	{	const Function & x = Manifold::working .coordinates() [i];
		x(P) = x(A) + e_AB [i] / 2. + n_AB [i] * tag::Util::sqrt_three_quarters;  }
	// we consider both  A  and  P  have zero winding
	Manifold::working .project (P);
	// 'compute_data' uses information from A to compute eigendirection and isr at P
	environ::metric_type::compute_data ( tag::at_point, P, tag::use_information_from, A );

	#if FRONTAL_VERBOSITY > 1
	std::cout << "frontal-2d.cpp line 3250, create brand new triangle, [";
	for ( size_t i = 0; i < frontal_nb_of_coords; i++ )
	{	const Function & x = Manifold::working .coordinates() [i];
		std::cout << x(A) << ",";                                   }
	std::cout << "], ";
	for ( size_t i = 0; i < frontal_nb_of_coords; i++ )
	{	const Function & x = Manifold::working .coordinates() [i];
		std::cout << x(B) << ",";                                   }
	std::cout << "], ";
	for ( size_t i = 0; i < frontal_nb_of_coords; i++ )
	{	const Function & x = Manifold::working .coordinates() [i];
		std::cout << x(P) << ",";                                   }
	std::cout << "], ";
	#endif  // FRONTAL_VERBOSITY > 1

	// l1 contains vertices already known to be close to P
	// let l1 be a set instead ?
	std::vector < typename environ::manif_type::winding_cell > l1;  // { A, B }
	l1 .reserve (7);
	l1 .push_back ( environ::manif_type::add_winding   ( A, 0  ) );
	l1 .push_back ( environ::manif_type::add_winding_1 ( B, AB ) );
	while ( true )
	{	relocate_core_2d < typename environ::manif_type > ( P, l1, 0.0004 );
		std::list < typename environ::manif_type::winding_cell > l2 =   // was tag::Util::sqrt_2
			environ::manif_type::node_container .find_close_neighbours_of ( P, frontal_seg_size );
		#if FRONTAL_VERBOSITY > 1
		std::cout << l1 .size() << l2 .size() << " ";
		#endif
		if ( compare_two_lists < typename environ::manif_type >
		     ( P, l1, l2, inadmissible_ver, frontal_seg_size_2 ) )  break;                         }
	relocate_core_2d < typename environ::manif_type > ( P, l1, 0.0001 );
	#if FRONTAL_VERBOSITY > 1
	std::cout << ": " << std::sqrt ( environ::manif_type::sq_dist::working_manif  // rescaled metric
	                      ( A, environ::manif_type::add_winding ( P, 0 ) ) );
	std::cout << " " << std::sqrt ( environ::manif_type::sq_dist::working_manif  // rescaled metric
	                     ( B, environ::manif_type::add_winding ( P, 0 ) ) );
	std::cout << std::endl << std::flush;
	#endif  // FRONTAL_VERBOSITY > 1

	Cell AB_alt = remove_face ( AB, interf_copy );
	// AB .remove_from ( interf_copy );
	Cell AP ( tag::segment, A .reverse(), P );  // zero winding
	Cell PB ( tag::segment, P .reverse(), B );
	environ::manif_type::set_winding_1 ( PB, AB );
	AP .add_to ( interf_copy );
	PB .add_to ( interf_copy );
	// build_one_normal < manif_type > ( A, e_AB, * n_AB, AP, * normal_AP );
	environ::ext_prod_type::compute_data
		( tag::domain_2d, tag::at_seg, AP, tag::use_information_from, A, e_AB, n_AB );
	// build_one_normal < manif_type > ( B, e_AB, * n_AB, PB, * normal_PB );
	environ::ext_prod_type::compute_data
		( tag::domain_2d, tag::at_seg, PB, tag::use_information_from, B, e_AB, n_AB );
  environ::ext_prod_type::erase_normal_vector ( tag::at_seg, AB );
	Cell tri ( tag::triangle, AB_alt, PB .reverse(), AP .reverse() );
	tri .add_to ( msh );
	environ::manif_type::node_container .insert (P);
	Cell before_A = interf_copy .cell_behind ( A, tag::seen_from, AP, tag::surely_exists );
	seg_fill_one_tri .insert ( before_A );
	seg_fill_one_tri .insert ( PB );
	insert_or_replace ( seg_fill_two_tri, before_A, 21.);
	seg_fill_two_tri .insert ( { PB, 21.} );
	#if FRONTAL_VERBOSITY > 1
  frontal_counter ++;
	if ( frontal_counter >= frontal_counter_max ) return;
	#endif
	if ( l1 .size() > 2 )
	{	std::set < Cell > s;  // empty set
		build_bridge < environ > ( P, s, interf_orig, interf_copy, l1 );  }
		// empties l1 

	return;

}  // end of tri_out_of_the_blue

//------------------------------------------------------------------------------------------------------//


size_t count_neighb ( const Cell & ver, const Mesh & msh )     // hidden in anonymous namespace

// counts neighbours of a given vertex in a 2D mesh
// returns 0 if vertex is not inner (is on the boundary of msh)
	
{	assert ( ver .dim() == 0 );
	assert ( ver .is_positive() );
	assert ( msh .dim() == 2 );

	// we use an iterator just for starting the process
	Mesh::Iterator it = msh .iterator ( tag::over_cells, tag::of_max_dim,
	                                    tag::orientation_compatible_with_mesh, tag::around, ver );
	it .reset();  assert ( it .in_range() );
	Cell first_tri = *it;
	Cell tri = first_tri;

	size_t counter = 0;
	while ( true )
	{	Cell seg = tri .boundary() .cell_in_front_of ( ver, tag::surely_exists );
		assert ( seg .base() == ver .reverse() );
		tri = msh .cell_in_front_of ( seg, tag::may_not_exist );
		if ( not tri .exists() )  return 0;
		counter ++;
		if ( tri == first_tri )  return counter;                                                      }

}  // end of  count_neighb


inline void conditional_insert          // hidden in anonymous namespace
( const Cell & seg, const Mesh & msh,  std::map < Cell, int > & dif )

// used in 'flip' and 'perform_flips'
	
{	Cell tri_1 = msh .cell_in_front_of ( seg, tag::may_not_exist );
	if ( not tri_1 .exists() )  return;  // seg is on the boundary of msh
	assert ( not seg .belongs_to
		( tri_1 .boundary(), tag::same_dim, tag::orientation_compatible_with_mesh ) );
	assert ( seg .reverse() .belongs_to
		( tri_1 .boundary(), tag::same_dim, tag::orientation_compatible_with_mesh ) );
	Cell tri_2 = msh .cell_behind ( seg, tag::may_not_exist );
	if ( not tri_2 .exists() )  return;  // seg is on the boundary of msh
	assert ( seg .belongs_to
		( tri_2 .boundary(), tag::same_dim, tag::orientation_compatible_with_mesh ) );
	assert ( not seg .reverse() .belongs_to
		( tri_2 .boundary(), tag::same_dim, tag::orientation_compatible_with_mesh ) );
	Cell A = seg .base() .reverse ( tag::surely_exists );
	int d_A = count_neighb ( A, msh );
	if ( d_A == 0 )  return;   // A is on the boundary of msh
	if ( d_A < 6 )   return;
	Cell B = seg .tip();
	int d_B = count_neighb ( B, msh );
	if ( d_B == 0 )  return;   // B is on the boundary of msh
	if ( d_B < 6 )   return;
	Cell s = tri_1 .boundary() .cell_in_front_of ( A, tag::surely_exists );
	Cell P = s .tip();
	int d_P = count_neighb ( P, msh );
	if ( d_P == 0 )  return;   // P is on the boundary of msh
	if ( d_P > 6 )   return;
	s = tri_2 .boundary() .cell_in_front_of ( B, tag::surely_exists );
	Cell Q = s .tip();
	int d_Q = count_neighb ( Q, msh );
	if ( d_Q == 0 )  return;   // Q is on the boundary of msh
	if ( d_Q > 6 )   return;
	int d = d_A + d_B - d_P - d_Q;
	if ( d >= 3 )  dif [ seg ] = d;                                                 }
	
	
inline void flip ( Cell & seg, Mesh & msh, std::map < Cell, int > & dif ) // hidden in anonymous namespace

// assumes none of seg's extremities are on the boundary of msh
// manipulates dif
	
{	Cell tri2 = msh .cell_in_front_of ( seg, tag::surely_exists );
	Cell tri1 = msh .cell_behind ( seg, tag::surely_exists );

	Cell A = seg .base() .reverse ( tag::surely_exists );
	Cell B = seg .tip();

	Cell BC = tri1 .boundary() .cell_in_front_of ( B, tag::surely_exists );
	Cell CA = tri1 .boundary() .cell_behind ( A, tag::surely_exists );
	Cell AD = tri2 .boundary() .cell_in_front_of ( A, tag::surely_exists );
	Cell DB = tri2 .boundary() .cell_behind ( B, tag::surely_exists );
	Cell C = BC .tip();
	assert ( CA .base() .reverse ( tag::surely_exists ) == C );
	Cell D = AD .tip();
	assert ( DB .base() .reverse ( tag::surely_exists ) == D );
	
	B .cut_from_bdry_of ( seg, tag::do_not_bother );
	A .reverse() .cut_from_bdry_of ( seg, tag::do_not_bother );
	CA .cut_from_bdry_of ( tri1, tag::do_not_bother );
	DB .cut_from_bdry_of ( tri2, tag::do_not_bother );
	C .reverse() .glue_on_bdry_of ( seg, tag::do_not_bother );
	D .glue_on_bdry_of ( seg, tag::do_not_bother );
	DB .glue_on_bdry_of ( tri1, tag::do_not_bother );
	CA .glue_on_bdry_of ( tri2, tag::do_not_bother );

	tri1 .boundary() .closed_loop ( B );
	tri2 .boundary() .closed_loop ( A );

	assert ( A .is_inner_to ( msh ) );  msh .barycenter ( A );
	assert ( B .is_inner_to ( msh ) );  msh .barycenter ( B );
	assert ( C .is_inner_to ( msh ) );  msh .barycenter ( C );
	assert ( D .is_inner_to ( msh ) );  msh .barycenter ( D );

	// analyse again neighbour segments

	std::set < Cell > set_of_segs;
	Mesh::Iterator it = msh .iterator ( tag::over_cells, tag::of_max_dim,
	                                    tag::orientation_compatible_with_mesh, tag::around, A );
	for ( it .reset(); it .in_range() ; it++ )
	{	Cell tri = *it;
		assert ( tri .dim() == 2 );
		Mesh::Iterator itt = tri .boundary() .iterator
			( tag::over, tag::cells_of_max_dim, tag::force_positive );
		for ( itt .reset(); itt .in_range() ; itt++ )
		{	Cell s = *itt;
			assert ( s .dim() == 1 );
			set_of_segs .insert (s);   }                                }

	for ( std::set < Cell > ::iterator itt = set_of_segs .begin(); itt != set_of_segs .end(); itt++ )
	{	Cell s = *itt;
		dif .erase ( s );
		conditional_insert ( s, msh, dif );   }
	
}  // end of  flip


inline void perform_flips ( Mesh & msh )      // hidden in anonymous namespace
	
{	std::map < Cell, int > dif;

	{ // just a block of code for hiding 'it'
	Mesh::Iterator it = msh .iterator ( tag::over_segments, tag::force_positive );
	for ( it .reset(); it .in_range(); it++ )  conditional_insert ( *it, msh, dif );
	} // just a block of code

	#if FRONTAL_VERBOSITY > 1
	std::cout << "frontal-2d.cpp line 3449, " << dif .size() << " candidates for flip"
	          << std::endl << std::flush;
	size_t counter = 0;
	#endif
	
	while ( not dif .empty() )
	{	int max_dif = 0;
		Cell kept_seg ( tag::non_existent );
		for ( std::map < Cell, int > ::iterator it = dif .begin(); it != dif .end(); it++ )
		{	int d = it->second;
			if ( d > max_dif )  {  max_dif = d;  kept_seg = it->first;  }  }
		assert ( max_dif >= 3 );
		assert ( kept_seg .exists() );
		flip ( kept_seg, msh, dif );   //  updates dif
		#if FRONTAL_VERBOSITY > 1
		counter ++;
		#endif
	}  // end of while

	#if FRONTAL_VERBOSITY > 1
	std::cout << "frontal-2d.cpp line 3468, " << counter << " flips performed" << std::endl << std::flush;
	#endif

}  // end of  perform_flips

//------------------------------------------------------------------------------------------------------//


template < class environ >     // line 1255
void frontal_construct_2d            // hidden in anonymous namespace
( Mesh & msh, const tag::Boundary &, const Mesh & bdry,
  const tag::StartAt &, Cell start                     )

// for two-dimensional meshes (arbitrary geometric dimension)
	
// 'start' is a segment belonging to 'bdry'

// 'environ' describes the type of manifold we are working in
// and how we treat information about metric and exterior product
// environ::manif_type could be ManifoldNoWinding or ManifoldQuotient (defined in frontal.cpp)
// environ::metric_type may be one of ISRContainer::{Inactive,Active} (defined in frontal.cpp)
// environ::ext_prod_type may be ExtProd2d, 2dRev, Codim1 (defined in frontal.cpp)
	
{	assert ( bdry .has_empty_boundary() );
	
	#if FRONTAL_VERBOSITY > 1
	std::cout << "frontal-2d.cpp line 3492, manif_type "
	          << environ::manif_type::info() << std::endl << std::flush;
	#endif
	// if ( start .dim() != 1 )
	// {	assert ( start .dim() == 0 );
	// 	start = bdry .cell_in_front_of ( start );  }
	assert ( start .dim() == 1 );
	assert ( bdry  .dim() == 1 );
	assert ( msh   .dim() == 2 );
	assert ( start .belongs_to ( bdry, tag::same_dim, tag::orientation_compatible_with_mesh ) );

	#if FRONTAL_VERBOSITY <= 1
	assert ( seg_fill_isolated .empty() );
	assert ( seg_fill_one_tri  .empty() );
	assert ( seg_fill_two_tri  .empty() );
	assert ( environ::manif_type::node_container .empty() );
	assert ( environ::metric_type::empty() );
	#endif

	{ // just a block of code for hiding 'it'
	Mesh::Iterator it = bdry .iterator ( tag::over_vertices );
	for ( it .reset(); it .in_range(); it++ )
		environ::manif_type::node_container .insert ( *it );
	} // just a block of code

	// we don't want to change 'bdry' so we make a copy of it
	// one more reason : bdry may be Mesh::Connected::OneDim or Mesh::Fuzzy,
	// we want a Mesh::STSI interface to play with

	Mesh interface ( tag::STSI, tag::of_dim, 1 );  // initially empty

	build_component ( tag::domain_2d, bdry, interface, tag::start_at, start );
	// add faces to interface, seg_fill_isolated, seg_fill_one_tri, seg_fill_two_tri

	// if the metric is anisotropic, compute_data computes
	// the inner spectral radius for all vertices along the interface
	// if the metric is isotropic, does nothing at all
	environ::metric_type::compute_data ( tag::at_point, start .tip() );
	environ::metric_type::compute_data ( tag::along, interface, tag::start_at, start .tip() );

	// environ::compute_data ( tag::domain_2d, tag::first_time, tag::at_seg, start )
	// must have already been invoked by calling code
	environ::ext_prod_type::compute_data
		( tag::domain_2d, tag::along, interface, tag::start_at, start );
	// propagate normal vector

	#if FRONTAL_VERBOSITY > 1
	std::cout << "counter max = ";  std::cin >> frontal_counter_max;
	frontal_counter = 0;
	#endif
		
	while ( true )

	{	if ( interface .number_of ( tag::segments ) == 0 )
		{	assert ( seg_fill_isolated .empty() );
			assert ( seg_fill_one_tri  .empty() );
			seg_fill_two_tri .clear();
			assert ( environ::manif_type::node_container .empty() );
			assert ( environ::metric_type::empty() );
			break;                                                   }

		#if FRONTAL_VERBOSITY > 1
		std::cout << "frontal-2d.cpp line 3553, seg_fill_isolated size " << seg_fill_isolated .size()
		          << ", counter " << frontal_counter + 1 << std::endl << std::flush;
		#endif

		if ( fill_isolated_triangle < environ > ( msh, interface ) )
		#if FRONTAL_VERBOSITY > 1
		{	if ( frontal_counter >= frontal_counter_max ) break;
			continue;                                            }  // resume process
		#else
			continue;  // resume process
		#endif

		#if FRONTAL_VERBOSITY > 1
		std::cout << "frontal-2d.cpp line 3566, seg_fill_one_tri  size "
		          << seg_fill_one_tri .size() << std::endl << std::flush;
		#endif

		if ( fill_one_triangle < environ > ( msh, interface ) )
		#if FRONTAL_VERBOSITY > 1
		{	if ( frontal_counter >= frontal_counter_max ) break;
			continue;                                            }  // resume process
		#else
			continue;  // resume process
		#endif

		#if FRONTAL_VERBOSITY > 1
		std::cout << "frontal-2d.cpp line 3579, seg_fill_two_tri  size "
		          << seg_fill_two_tri .size() << std::endl << std::flush;
		#endif
	
		if ( fill_two_triangles < environ > ( msh, bdry, interface ) )
		#if FRONTAL_VERBOSITY > 1
		{	if ( frontal_counter >= frontal_counter_max ) break;
			continue;                                            }  // resume process
		#else
			continue;  // resume process
		#endif

		// if an isolated quadrangle is filled, 'fill_two_triangles' returns false
		// thus, we must ckeck if mesh is finished
		if ( interface .number_of ( tag::segments ) == 0 )
		{	assert ( seg_fill_isolated .empty() );
			assert ( seg_fill_one_tri  .empty() );
			seg_fill_two_tri .clear();
			assert ( environ::manif_type::node_container .empty() );
			assert ( environ::metric_type::empty() );
			break;                                                    }
	
		tri_out_of_the_blue < environ > ( msh, bdry, interface );
		#if FRONTAL_VERBOSITY > 1
		if ( frontal_counter >= frontal_counter_max ) break;
		#endif
		continue;  // resume process

	}  // end of  while  loop

	#if FRONTAL_VERBOSITY > 1
	if ( frontal_counter < frontal_counter_max )
	{
	#endif

	assert ( seg_fill_isolated .empty() );
	assert ( seg_fill_one_tri  .empty() );
	assert ( seg_fill_two_tri  .empty() );
	assert ( environ::manif_type::node_container .empty() );
	assert ( environ::metric_type::empty() );

	#ifndef NDEBUG  // DEBUG mode
	{ // just a block of code for hiding 'it'
	Mesh::Iterator it =  msh .iterator ( tag::over_vertices );
	for ( it .reset(); it .in_range(); it++ )
	{	Cell ver = *it;
		std::map < tag::KeyForHook, void * > ::iterator it_h =
			ver .core->hook .find ( tag::node_in_cloud );
		assert ( it_h == ver .core->hook .end() );
		it_h = ver .core->hook .find ( tag::isr_inv_matrix );
		assert ( it_h == ver .core->hook .end() );
		it_h = ver .core->hook .find ( tag::isr_eigen_dir );
		assert ( it_h == ver .core->hook .end() );
		it_h = ver .core->hook .find ( tag::sqrt_det );
		assert ( it_h == ver .core->hook .end() );
		it_h = ver .core->hook .find ( tag::isr_radius );
		assert ( it_h == ver .core->hook .end() );              }
	} { // just a block of code for hiding 'it'
	Mesh::Iterator it =  msh .iterator ( tag::over_segments );
	for ( it .reset(); it .in_range(); it++ )
	{	Cell seg = *it;
		std::map < tag::KeyForHook, void * > ::iterator it_h =
			seg .core->hook .find ( tag::normal_vector );
		assert ( it_h == seg .core->hook .end() );              }
	} // just a block of code
	#endif  // DEBUG
		
	#if FRONTAL_VERBOSITY > 1
	}
	std::cout << "counter " << frontal_counter << std::endl << std::flush;
	#endif
	
} // end of  frontal_construct_2d

//------------------------------------------------------------------------------------------------------//


template < class environ >     // line 1437
void frontal_construct_2d            // hidden in anonymous namespace
( Mesh & msh, const tag::Boundary &, const Mesh & bdry,
  const tag::StartAt &, Cell start,
  const tag::Towards &, std::vector<double> normal     )

// 'environ' describes the type of manifold we are working in
// and how we treat information about metric and exterior product
// environ::manif_type could be ManifoldNoWinding or ManifoldQuotient (defined in frontal.cpp)
// environ::metric_type may be one of ISRContainer::{Inactive,Active} (defined in frontal.cpp)
// environ::ext_prod_type may be ExtProd2d, 2dRev, Codim1 (defined in frontal.cpp)
	
{	assert ( bdry .has_empty_boundary() );

	if ( start .dim() != 1 )
	{	assert ( start .dim() == 0 );
		start = bdry .cell_in_front_of ( start );  }
	assert ( start .dim() == 1 );
	#if FRONTAL_VERBOSITY > 1
	std::cout << "frontal-2d.cpp line 3665, manif_type "
	          << environ::manif_type::info() << std::endl << std::flush;
	#endif
	environ::ext_prod_type::compute_data   
		( tag::domain_2d, tag::first_time, tag::at_seg, start, tag::normal_vect, normal );
	frontal_construct_2d < environ > ( msh, tag::boundary, bdry, tag::start_at, start );    // line 1255

} // end of  frontal_construct_2d

