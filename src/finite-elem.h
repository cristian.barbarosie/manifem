
//   finite-elem.h  2024.10.04

//   This file is part of maniFEM, a C++ library for meshes and finite elements on manifolds.

//   Copyright  2019 - 2024  Cristian Barbarosie  cristian.barbarosie@gmail.com

//   https://maniFEM.rd.ciencias.ulisboa.pt/
//   https://codeberg.org/cristian.barbarosie/maniFEM

//   ManiFEM is free software: you can redistribute it and/or modify it
//   under the terms of the GNU Lesser General Public License as published
//   by the Free Software Foundation, either version 3 of the License
//   or (at your option) any later version.

//   ManiFEM is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//   See the GNU Lesser General Public License for more details.

//   You should have received a copy of the GNU Lesser General Public License
//   along with maniFEM.  If not, see <https://www.gnu.org/licenses/>.

// only included ifndef MANIFEM_NO_FEM


#ifndef MANIFEM_FINITE_ELEM_H
#define MANIFEM_FINITE_ELEM_H

#include <string>
#include <iostream>
#include <vector>
#include <map>
#include <forward_list>
#include "assert.h"

#include "mesh.h"
#include "field.h"
#include "function.h"
#include "manifold.h"

namespace maniFEM {

namespace tag {
	struct gauss { };  static const gauss Gauss;
	enum gauss_quadrature { seg_1, seg_2, seg_3, seg_4, seg_5, seg_6,
	                        tri_1, tri_3, tri_3_Oden, tri_4, tri_4_Oden,
	                        tri_6, tri_7, tri_12, tri_13,
	                        quad_1, quad_4, quad_9,
	                        tetra_1, tetra_4, tetra_5, tetra_11, tetra_15,
	                        cube_1, cube_8, cube_27, cube_64              };
	struct FromFiniteElementWithMaster { };
	static const FromFiniteElementWithMaster from_finite_element_with_master;
	struct FromFiniteElement { };  static const FromFiniteElement from_finite_element;
	struct ThroughDockedFiniteElement { };
	static const ThroughDockedFiniteElement through_docked_finite_element;
	struct EnumerateCells { };  static const EnumerateCells enumerate_cells;
	struct PreComputed { };  static const PreComputed pre_computed;
	struct ForGiven { };  static const ForGiven for_given;
	struct ForAGiven { };  static const ForAGiven for_a_given;
	struct BasisFunctions { };  static const BasisFunctions basis_functions;
	struct IntegralOf { };  static const IntegralOf integral_of;
	struct HandCoded { };  static const HandCoded hand_coded;
	struct FirstVertex { };  static const FirstVertex first_vertex;
	struct Straight { };  static const Straight straight;
	struct Curved { };  static const Curved curved;
	struct IncrementalBasis { };  static const IncrementalBasis incremental_basis;
}

class FiniteElement;

//------------------------------------------------------------------------------------------------------//

	
class Integrator

// a wrapper for Gauss quadrature
// and perhaps other integration methods (e.g. symbolic integration)

// if 'weak', its constructor does not increase the counter of the respective Integrator::Core,
// and its destructor does not try to destroy the core (more or less like a weak pointer)

{	public :

	class Core;

	Integrator::Core * core;
	bool weak;

	inline Integrator ( const tag::NonExistent & ) : core { nullptr }, weak { false } { }
	// constructors below defined in 'finite-elem-xx.h'
	Integrator ( const tag::gauss &, const tag::gauss_quadrature & q );
	Integrator ( const tag::gauss &, const tag::gauss_quadrature & q,
	             const tag::FromFiniteElementWithMaster &, FiniteElement & fe );

	~Integrator ( );

	// copy operations transform a weak Integrator into a strong one
	// a weak Integrator cannot be moved
	Integrator ( const Integrator & );
	Integrator ( const Integrator && );
	Integrator & operator= ( const Integrator & );
	Integrator & operator= ( const Integrator && );

	void conditionally_dispose_core ( );

	// operator() integrates a given function on a given mesh or cell
	
	inline double operator() ( const Function & f, const tag::On &, const Cell & cll ) const;
	inline double operator() ( const Function & f, const tag::On &, const Mesh & msh ) const;

	// there is also an operator() without a domain,
	// for integrators attached to a finite element,
	// in the event that the finite element is already docked on a cell
	inline double operator() ( const Function & f ) const;

	// we assume the function f is already expressed in terms of master coordinates
	// (it is composed with fe.core->transf)
	// (this is necessary for Gauss quadrature, not for symbolic integration)
	inline double operator()
	( const Function & f, const tag::ThroughDockedFiniteElement &, const FiniteElement & fe );

	inline void dock_on ( const Cell & cll );

	class Gauss;  class HandCoded;

	#ifndef NDEBUG  // DEBUG mode
	inline std::string info ( );  // return a multi-line documentation string
	#endif

};  // end of  class Integrator

//------------------------------------------------------------------------------------------------------//


class FiniteElement

// wrapper for different types of finite elements	
	
// if 'weak', its constructor does not increase the counter of the respective FiniteElement::Core,
// and its destructor does not try to destroy the core (more or less like a weak pointer)

{	public :

	class Core;  class WithMaster;  class StandAlone;
	
	FiniteElement::Core * core;
	bool weak;

	inline FiniteElement ( const tag::NonExistent & )	: core { nullptr }, weak { false }  { }
	// constructors below defined in 'finite-elem-xx.h'
	FiniteElement ( const tag::WithMaster &, const tag::Segment &,
	                const tag::lagrange &, const tag::OfDegree &, size_t deg );
	FiniteElement ( const tag::WithMaster &, const tag::Triangle &,
	                const tag::lagrange &, const tag::OfDegree &, size_t deg );
	FiniteElement ( const tag::WithMaster &, const tag::Triangle &,
	          const tag::lagrange &, const tag::OfDegree &, size_t deg, const tag::Straight & );
	FiniteElement ( const tag::WithMaster &, const tag::Triangle &,
	                const tag::lagrange &, const tag::OfDegree &, size_t deg,
	                const tag::Straight &, const tag::IncrementalBasis & );
	FiniteElement ( const tag::WithMaster &, const tag::Triangle &,
	          const tag::lagrange &, const tag::OfDegree &, size_t deg, const tag::Curved & );
	FiniteElement ( const tag::WithMaster &, const tag::Quadrangle &,
	                const tag::lagrange &, const tag::OfDegree &, size_t deg );
	inline FiniteElement ( const tag::WithMaster &, const tag::Parallelogram &,
	                       const tag::lagrange &, const tag::OfDegree &, size_t deg )
	:	FiniteElement ( tag::with_master, tag::quadrangle, tag::Lagrange, tag::of_degree, deg )
	{	}
	inline FiniteElement ( const tag::WithMaster &, const tag::Square &,
	                       const tag::lagrange &, const tag::OfDegree &, size_t deg )
	:	FiniteElement ( tag::with_master, tag::quadrangle, tag::Lagrange, tag::of_degree, deg )
	{	}
	FiniteElement ( const tag::WithMaster &, const tag::Quadrangle &,
	          const tag::lagrange &, const tag::OfDegree &, size_t deg, const tag::Straight & );
	inline FiniteElement ( const tag::WithMaster &, const tag::Parallelogram &,
	                       const tag::lagrange &, const tag::OfDegree &, size_t deg,
	                       const tag::Straight &                                    )
	:	FiniteElement ( tag::with_master, tag::quadrangle,
                    tag::Lagrange, tag::of_degree, deg, tag::straight )
	{	}
	inline FiniteElement ( const tag::WithMaster &, const tag::Square &,
	                       const tag::lagrange &, const tag::OfDegree &, size_t deg,
	                       const tag::Straight &                                    )
	:	FiniteElement ( tag::with_master, tag::quadrangle,
                    tag::Lagrange, tag::of_degree, deg, tag::straight )
	{	}
	FiniteElement ( const tag::WithMaster &, const tag::Quadrangle &,
	                const tag::lagrange &, const tag::OfDegree &, size_t deg,
	                const tag::Straight &, const tag::IncrementalBasis & );
	FiniteElement ( const tag::WithMaster &, const tag::Quadrangle &,
	         const tag::lagrange &, const tag::OfDegree &, size_t deg, const tag::Curved & );
	FiniteElement ( const tag::WithMaster &, const tag::Tetrahedron &,
	                const tag::lagrange &, const tag::OfDegree &, size_t deg );
	FiniteElement ( const tag::WithMaster &, const tag::Hexahedron &,
	                const tag::lagrange &, const tag::OfDegree &, size_t deg );
	FiniteElement ( const tag::Triangle &,  // no master, stand-alone
	                const tag::lagrange &, const tag::OfDegree &, size_t deg );
	FiniteElement ( const tag::Quadrangle &,  // no master, stand-alone
	                const tag::lagrange &, const tag::OfDegree &, size_t deg );
	FiniteElement ( const tag::Rectangle &,  // no master, stand-alone
	                const tag::lagrange &, const tag::OfDegree &, size_t deg );
	FiniteElement ( const tag::Square &,  // no master, stand-alone
	                const tag::lagrange &, const tag::OfDegree &, size_t deg );
	FiniteElement ( const tag::Tetrahedron &,  // no master, stand-alone
	                const tag::lagrange &, const tag::OfDegree &, size_t deg );
	FiniteElement ( const tag::Hexahedron &,  // no master, stand-alone
	                const tag::lagrange &, const tag::OfDegree &, size_t deg );
	

	inline ~FiniteElement ( );
	
	// copy operations transform a weak FiniteElement into a strong one
	// a weak FiniteElement cannot be moved
	inline FiniteElement ( const FiniteElement & );
	inline FiniteElement ( const FiniteElement && );
	inline FiniteElement & operator= ( const FiniteElement & );
	inline FiniteElement & operator= ( const FiniteElement && );

	inline void conditionally_dispose_core ( );

	// get basis functions associated to vertices, to segments, etc
	inline Function basis_function ( const Cell cll );
	inline Function basis_function ( const Cell c1, const Cell c2 );

	Integrator set_integrator ( const tag::gauss &, const tag::gauss_quadrature & );
	Integrator set_integrator ( const tag::HandCoded & );

	inline void dock_on ( const Cell & cll );
	#ifndef MANIFEM_NO_QUOTIENT
	inline void dock_on ( const Cell & cll, const tag::Winding & );
	#endif  // ifndef MANIFEM_NO_QUOTIENT
	
	// two methods below only significant for
	// FiniteElement::StandAlone::TypeOne::{Rectangle,Square}
	inline void dock_on ( const Cell & cll, const tag::FirstVertex &, const Cell & );
	#ifndef MANIFEM_NO_QUOTIENT
	inline void dock_on ( const Cell & cll, const tag::FirstVertex &, const Cell &,
	                      const tag::Winding &                                     );
	#endif  // ifndef MANIFEM_NO_QUOTIENT

	inline double integrate ( const Function & );
	inline std::vector < double > integrate
	( const tag::PreComputed &, const tag::Replace &, const Function &,
	                            const tag::By &,      const Function & );
	inline std::vector < double > integrate
	( const tag::PreComputed &, const tag::Replace &, const Function &,
	                            const tag::By &,      const Function &,
	                            const tag::Replace &, const Function &,
	                            const tag::By &,      const Function & );

	inline void pre_compute
	( const tag::ForAGiven &, const tag::BasisFunction &, Function bf, 
	  const tag::IntegralOf &, const std::vector < Function > & res   );
	inline void pre_compute
	( const tag::ForGiven &, const tag::BasisFunctions &, Function bf1, Function bf2, 
	  const tag::IntegralOf &, const std::vector < Function > & res                  );

	// the first version of build_global_numbering (without arguments) delegates the job
	// to the core which must guess the type of numbering
	inline Cell::Numbering & build_global_numbering ( );
	inline Cell::Numbering & build_global_numbering ( const tag::Vertices & );
	inline Cell::Numbering & build_global_numbering ( const tag::Segments & );
	inline Cell::Numbering & build_global_numbering ( const tag::CellsOfDim &, const size_t d );
	
	inline Cell::Numbering & numbering ( const tag::Vertices & );
	inline Cell::Numbering & numbering ( const tag::Segments & );
	inline Cell::Numbering & numbering ( const tag::CellsOfDim &, const size_t d );

	#ifndef NDEBUG  // DEBUG mode
	inline std::string info ( );  // return a multi-line documentation string
	#endif

};  // end of  class FiniteElement
	
//------------------------------------------------------------------------------------------------------//


class Integrator::Core

// a base class for Gauss quadrature
// and perhaps other integration methods (e.g. symbolic integration)

{	public :

	short unsigned int counter { 0 };

	#ifndef NDEBUG  // DEBUG mode
	std::string info_string;  // multi-line documentation string
	#endif

	// constructor

	inline Core ()  { };

	// destructor

	virtual ~Core()  { };
	
	virtual double action ( Function f, const FiniteElement & fe ) = 0;
	virtual double action ( Function f ) = 0;
	virtual double action ( const Function & f, const tag::On &, const Cell & cll ) = 0;
	virtual double action ( const Function & f, const tag::On &, const Mesh & msh ) = 0;

	virtual void dock_on ( const Cell & cll ) = 0;

};  // end of  class Integrator::Core

//------------------------------------------------------------------------------------------------------//


inline double Integrator::operator()
( const Function & f, const tag::ThroughDockedFiniteElement &, const FiniteElement & fe )
{	return this->core->action ( f, fe );  }

inline double Integrator::operator() ( const Function & f ) const
{	return this->core->action ( f );  }

inline double Integrator::operator() ( const Function & f, const tag::On &, const Cell & cll ) const
{	return this->core->action ( f, tag::on, cll );  }

inline double Integrator::operator() ( const Function & f, const tag::On &, const Mesh & msh ) const
{	return this->core->action ( f, tag::on, msh );  }

//------------------------------------------------------------------------------------------------------//

	
class FiniteElement::Core

// a base class for several types of FiniteElements
// abstract class, instantiated in FiniteElement::WithMaster::***
//                             and FiniteElement::StandAlone::Type***::***

{	public :

	short unsigned int counter { 0 };

	Integrator integr;

	std::map < Cell::Core *, Function > base_fun_1;
	std::map < Cell::Core *, std::map < Cell::Core *, Function > > base_fun_2;

	// at the beginning, 'docked_on' is a non-existent cell
	// later, we shall dock the finite element on a physical cell
	Cell docked_on;

	// a numbering system for each dimension of cells
	// we could think of a more flexible approach, for instance
	// the user may want to number vertices and segments mixed in the same map
	std::vector < Cell::Numbering > numbers;
	
	#ifndef NDEBUG  // DEBUG mode
	std::string info_string;  // multi-line documentation string
	#endif
	
	// constructor

	inline Core ( ) : integr ( tag::non_existent ), docked_on ( tag::non_existent )  { };

	// destructor

	virtual ~Core()  { };
	
	virtual void dock_on ( const Cell & cll ) = 0;
	#ifndef MANIFEM_NO_QUOTIENT
	virtual void dock_on ( const Cell & cll, const tag::Winding & ) = 0;
	#endif  // ifndef MANIFEM_NO_QUOTIENT

	// two methods below only significant for
	// FiniteElement::StandAlone::TypeOne::{Rectangle,Square}
	virtual void dock_on ( const Cell & cll, const tag::FirstVertex &, const Cell & );
	#ifndef MANIFEM_NO_QUOTIENT
	virtual void dock_on ( const Cell & cll, const tag::FirstVertex &, const Cell &,
	                       const tag::Winding &                                     );
	#endif  // ifndef MANIFEM_NO_QUOTIENT
	
	// 'pre_compute' and 'retrieve_precomputed' are only meaningful for
	// FiniteElement::StandAlone::TypeOne::***
	// we forbid execution here and later override
	virtual void pre_compute ( const std::vector < Function > & bf,
	                           const std::vector < Function > & res ) = 0;
	virtual std::vector < double > retrieve_precomputed ( const Function & bf, const Function & psi ) = 0;
	virtual std::vector < double > retrieve_precomputed
	( const Function & bf1, const Function & psi1, const Function & bf2, const Function & psi2 ) = 0;
	
	virtual Cell::Numbering & build_global_numbering ( ) = 0;
	
	#ifndef NDEBUG  // DEBUG mode
	virtual std::string info ( ) = 0;  // return a multi-line documentation string
	// defined by subclasses FiniteElement::WithMaster and FiniteElement::StandAlone
	#endif

};  // end of  class FiniteElement::Core

//------------------------------------------------------------------------------------------------------//


inline Function FiniteElement::basis_function ( const Cell cll )
{	std::map < Cell::Core *, Function > :: iterator it =
		this->core->base_fun_1 .find ( cll .core );
	assert ( it != this->core->base_fun_1 .end() );
	return it->second;                                   }
	
inline Function FiniteElement::basis_function ( const Cell c1, const Cell c2 )
{	std::map < Cell::Core *, std::map < Cell::Core *, Function > >
		:: iterator it = this->core->base_fun_2 .find ( c1 .core );
	assert ( it != this->core->base_fun_2 .end() );
	std::map < Cell::Core *, Function > :: iterator itt =
		it->second .find ( c2 .core );
	assert ( itt != it->second .end() );
	return itt->second;                                               }


inline void FiniteElement::dock_on ( const Cell & cll )
{	this->core->dock_on ( cll );  }

#ifndef MANIFEM_NO_QUOTIENT
inline void FiniteElement::dock_on ( const Cell & cll, const tag::Winding & )
{	this->core->dock_on ( cll, tag::winding );  }
#endif  // ifndef MANIFEM_NO_QUOTIENT

inline void FiniteElement::dock_on ( const Cell & cll, const tag::FirstVertex &, const Cell & v )
{	this->core->dock_on ( cll, tag::first_vertex, v );  }

#ifndef MANIFEM_NO_QUOTIENT
inline void FiniteElement::dock_on
( const Cell & cll, const tag::FirstVertex &, const Cell & v, const tag::Winding & )
{	this->core->dock_on ( cll, tag::first_vertex, v, tag::winding );  }
#endif  // ifndef MANIFEM_NO_QUOTIENT


inline void Integrator::dock_on ( const Cell & cll )
{	this->core->dock_on ( cll );  }


inline Cell::Numbering & FiniteElement::build_global_numbering ( )
{	return this->core->build_global_numbering();  }

inline Cell::Numbering & FiniteElement::build_global_numbering ( const tag::Vertices & )
{	assert ( this->core->numbers .size() == 0 );
	this->core->numbers .push_back
		( Cell::Numbering ( tag::whose_core_is,
		                    std::make_shared < Cell::Numbering::Field > ( tag::vertices ) ) );
	return  this->core->numbers [0];                                                           }


inline Cell::Numbering & FiniteElement::build_global_numbering ( const tag::Segments & )
{	assert ( this->core->numbers .size() <= 1 );
	assert ( false );
	return  this->core->numbers [1];             }

inline Cell::Numbering & FiniteElement::build_global_numbering
( const tag::CellsOfDim &, const size_t d )
{	assert ( this->core->numbers .size() <= d );
	assert ( false );
	return  this->core->numbers [d];             }


inline Cell::Numbering & FiniteElement::numbering ( const tag::Vertices & )
{	assert ( this->core->numbers .size() > 0 );
	return  this->core->numbers [0];             }

inline Cell::Numbering & FiniteElement::numbering ( const tag::Segments & )
{	assert ( this->core->numbers .size() > 1 );
	return  this->core->numbers [1];             }

inline Cell::Numbering & FiniteElement::numbering ( const tag::CellsOfDim &, const size_t d )
{	assert ( this->core->numbers .size() > d );
	return  this->core->numbers [d];             }


#ifndef NDEBUG  // DEBUG mode

inline std::string FiniteElement::info ( )
{	return this->core->info();  }  // multi-line documentation string

inline std::string Integrator::info ( )
{	return this->core->info_string;  }  // multi-line documentation string

#endif

//------------------------------------------------------------------------------------------------------//


inline double FiniteElement::integrate ( const Function & f )
{	// assert that 'this' is already docked :
	assert ( this->core->docked_on .exists() );
	return this->core->integr ( f, tag::through_docked_finite_element, *this );  }

inline std::vector < double > FiniteElement::integrate
( const tag::PreComputed &, const tag::Replace &, const Function & bf,
                            const tag::By &,      const Function & psi)
{	// assert that 'this' is already docked :
	assert ( this->core->docked_on .exists() );
	return this->core->retrieve_precomputed ( bf, psi );  }

inline std::vector < double > FiniteElement::integrate
( const tag::PreComputed &, const tag::Replace &, const Function & bf1,
                            const tag::By &,      const Function & psi1,
                            const tag::Replace &, const Function & bf2,
                            const tag::By &,      const Function & psi2 )
{	return this->core->retrieve_precomputed ( bf1, bf2, psi1, psi2 );  }


inline void FiniteElement::pre_compute
( const tag::ForAGiven &, const tag::BasisFunction &, Function bf,
  const tag::IntegralOf &, const std::vector < Function > & v     )
{	this->core->pre_compute ( { bf }, v );  }

inline void FiniteElement::pre_compute
( const tag::ForGiven &, const tag::BasisFunctions &, Function bf1, Function bf2,
  const tag::IntegralOf &, const std::vector < Function > & v                    )
{	this->core->pre_compute ( { bf1, bf2 }, v );  }

//------------------------------------------------------------------------------------------------------//


inline void FiniteElement::conditionally_dispose_core ( )

{	if ( this->core )
	if ( not this->weak )
	{	assert ( this->core->counter > 0 );
		this->core->counter --;
		if ( this->core->counter == 0 ) delete this->core;  }  }

inline FiniteElement::~FiniteElement ( )

{	this->conditionally_dispose_core();  }


inline FiniteElement::FiniteElement ( const FiniteElement & arg )
// copying a FiniteElement makes the copy strong

:	core { arg .core }, weak { false }

{ this->core->counter ++;  }

	
inline FiniteElement::FiniteElement ( const FiniteElement && arg )
// cannot move a weak FiniteElement
	
:	core { arg .core }, weak { false }

{ assert ( not arg .weak );
	this->core->counter ++;   }


inline FiniteElement & FiniteElement::operator= ( const FiniteElement & arg )
// copying a FiniteElement makes the copy strong

{	this->conditionally_dispose_core();
	this->core = arg .core;
	this->core->counter ++;
	this->weak = false;
	return * this;                       }


inline FiniteElement & FiniteElement::operator= ( const FiniteElement && arg )
// cannot move a weak FiniteElement

{	this->conditionally_dispose_core();
	assert ( not arg .weak );
	this->core = arg .core;
	this->weak = false;
	this->core->counter ++;   
	return * this;                       }

//------------------------------------------------------------------------------------------------------//


}  // end of  namespace maniFEM

#endif  // ifndef MANIFEM_FINITE_ELEM_H
