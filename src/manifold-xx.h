
//   manifold-xx.h  2025.01.25

//   This file is part of maniFEM, a C++ library for meshes and finite elements on manifolds.

//   Copyright  2019 -- 2025  Cristian Barbarosie  cristian.barbarosie@gmail.com

//   https://maniFEM.rd.ciencias.ulisboa.pt/
//   https://codeberg.org/cristian.barbarosie/maniFEM

//   ManiFEM is free software: you can redistribute it and/or modify it
//   under the terms of the GNU Lesser General Public License as published
//   by the Free Software Foundation, either version 3 of the License
//   or (at your option) any later version.

//   ManiFEM is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//   See the GNU Lesser General Public License for more details.

//   You should have received a copy of the GNU Lesser General Public License
//   along with maniFEM.  If not, see <https://www.gnu.org/licenses/>.


#ifndef MANIFEM_MANIFOLD_XX_H
#define MANIFEM_MANIFOLD_XX_H


namespace maniFEM {


#ifndef MANIFEM_NO_FRONTAL

class tag::Util::Metric::Constant : public tag::Util::Metric::Core

// abstract class, specialized in tag::Util::Metric::Constant::Isotropic, Matrix

{	public :

	// seven pointers to functions, more precisely, pointers to static methods
	// inherited from tag::Util::Metric::Core :
	// frontal_method_nw_***, frontal_method_q_***, remove_node_isr_p

	inline   Constant ( ) : tag::Util::Metric::Core()  { }
	virtual ~Constant ( );

	using tag::Util::Metric::Core::operator=;  // operator=, copy and move, deleted

	// tag::Util::MetricCore * clone ( )  stays pure virtual from tag::Util::Metric::Core

	// tag::Util::MetricCore * regist ( )   virtual from tag::Util::Metric::Core
	// defined there, execution forbidden
	
	// bool isotropic   stays pure virtual from tag::Util::Metric::Core
	
	// double inner_prod ( v, w )  stays pure virtual from tag::Util::Metric::Core

	double inner_prod ( const std::vector < double > & v, const std::vector < double > & w ) = 0;

	double inner_prod  // virtual from tag::Util::Metric::Core
	( const Cell & P, const std::vector < double > & v, const std::vector < double > & w );	

	double inner_prod ( const Cell & P, const Cell & Q, 
	  const std::vector < double > & v, const std::vector < double > & w ) override;
	// virtual from tag::Util::Metric::Core, here return inner_prod ( v, w )

	// double inner_spectral_radius ( const Cell & A )  stays pure virtual from tag::Util::Metric::Core

	// double avrg_spectral_radius ( const Cell & A )
	// virtual from tag::Util::Metric::Core, defined there, return inner_spectral_radius
	
	// two 'scale_sq' and two 'scale_sq_inv' stay pure virtual from tag::Util::Metric::Core

	class Isotropic;  class Matrix;  class Rayleigh;
	
	// inlined 'sq_dist' defined by tag::Util::Metric::Core

	// two versions of  compute_data
	// virtual from tag::Util::Metric::Trivial, defined there, execution forbidden

	// methods codim_0, codim_1, de_activate virtual from tag::Util::MetricCore,
	// defined there, execution forbidden

	// correct_ext_prod_{2,3}d  stay pure virtual from tag::Util::MetricCore
	
};  // end of  class tag::Util::Metric::Constant

//------------------------------------------------------------------------------------------------------//

	
class tag::Util::Metric::Constant::Isotropic : public tag::Util::Metric::Constant

{	public :

	// seven pointers to functions, more precisely, pointers to static methods
	// inherited from tag::Util::Metric::Core :
	// frontal_method_nw_***, frontal_method_q_***, remove_node_isr_p

	double zoom;  // the norm gets zoomed by 'zoom'
	// the inner product gets zoomed by square of 'zoom'
	
	inline   Isotropic ( const double z ) : tag::Util::Metric::Constant(), zoom {z} { }
	virtual ~Isotropic ( );

	using tag::Util::Metric::Core::operator=;  // operator=, copy and move, deleted

	tag::Util::MetricCore * clone ( );  // virtual from tag::Util::Metric::Core

	// tag::Util::MetricCore * regist ( )   virtual from tag::Util::Metric::Core
	// defined there, execution forbidden
	
	bool isotropic ( );  // virtual from tag::Util::Metric::Core
	
	// inner product, virtual from tag::Util::Metric::Core
	double inner_prod ( const std::vector < double > & v, const std::vector < double > & w );	

	double inner_prod  // virtual from tag::Util::Metric::Core
	( const Cell & P, const std::vector < double > & v, const std::vector < double > & w ) override;	

	double inner_prod ( const Cell & P, const Cell & Q,  // virtual from tag::Util::Metric::Core
	  const std::vector < double > & v, const std::vector < double > & w ) override;	

	double inner_spectral_radius ( const Cell & A );  // here, could be called length_of_unit
	// virtual from tag::Util::Metric::Core, here return this->zoom

	// double avrg_spectral_radius ( const Cell & A )
	// virtual from tag::Util::Metric::Core, defined there, return inner_spectral_radius
	
	// two 'scale_sq' and two 'scale_sq_inv' methods are virtual from tag::Util::Metric::Core
	tag::Util::MetricCore * scale_sq ( const Function & f );
	tag::Util::MetricCore * scale_sq ( const double f );
	tag::Util::MetricCore * scale_sq_inv ( const Function & f );
	tag::Util::MetricCore * scale_sq_inv ( const double f );

	// inlined 'sq_dist' defined by tag::Util::Metric::Core

	// two versions of  compute_data
	// virtual from tag::Util::Metric::Trivial, defined there, execution forbidden

	// methods codim_0, codim_1, de_activate virtual from tag::Util::MetricCore,
	// defined there, execution forbidden

	void correct_ext_prod_2d ( const Cell & P, std::vector < double > & n );
	void correct_ext_prod_3d ( const Cell & P, std::vector < double > & n );
	// virtual from tag::Util::MetricCore, defined in frontal.cpp, here does nothing
	
};  // end of  class tag::Util::Metric::Constant::Isotropic

//------------------------------------------------------------------------------------------------------//

	
inline double tag::Util::norm_infty ( const std::vector < double > & w )
{	double res = 0.;
	for ( size_t i = 0; i < w .size(); i++ )
	{	const double tmp = std::abs ( w[i] );
		if ( tmp > res )  res = tmp;    }
	return res;                               }


inline void tag::Util::improve_inverse_matrix
( const Tensor < double > & matrix, Tensor < double > & inv_matr )
// improve approximation of inverse matrix  

// apply Gauss-Seidel SOR to compute the inverse of 'matrix'
// using 'inv_matr' as initial guess (and updating it)
	
{	assert ( matrix .dimensions .size() == 2 );
	size_t n = matrix .dimensions [0];
	assert ( n == matrix .dimensions [1] );
	assert ( inv_matr .dimensions .size() == 2 );
	assert ( inv_matr .dimensions [0] == n );
	assert ( inv_matr .dimensions [1] == n );

	static const double omega = 1.1;  // SOR
	static const double tol = 1.e-2;
	// the tolerance is relatively large
	// we do not need high precision in frontal mesh generation

	size_t iter;
	for ( iter = 0; iter < 10000; iter++ )
	{ // we solve the system M W = I :  m_ij w_jk = delta_ik
		bool good = true;
		for ( size_t i = 0; i < n; i++ )
		for ( size_t k = 0; k < n; k++ )
		{	double delta = 0.;
			if ( i == k )  delta = 1.;
			for ( size_t j = 0; j < n; j++ )
				delta -= matrix (i,j) * inv_matr (j,k);
			if ( std::abs ( delta ) > tol )  good = false;
			inv_matr (i,k) += omega * delta / matrix (i,i);  }
		if ( good )  break;                                   }
	assert ( iter < 10000 );                                   }


inline size_t tag::Util::power_method
( const Tensor < double > & matrix,
  std::vector < std::vector < double > > & starting_vectors,
  std::vector < double > & eigenval                         )

// power method, starting simultaneously from several different vectors

// return the index of the winner (first one to stabilize)

{	assert ( matrix .dimensions .size() == 2 );
	size_t n = matrix .dimensions [0];
	assert ( n == matrix .dimensions [1] );
	assert ( starting_vectors .size() == eigenval .size() );

	static const double tol = 1.e-2;
	// the tolerance is relatively large
	// we do not need high precision in frontal mesh generation

	size_t d_good, iter;
	for ( iter = 0; iter < 10000; iter++ )
	{	double max_norm = 0.;
		std::vector < bool > good ( starting_vectors .size(), true );
		for ( size_t d = 0; d < starting_vectors .size(); d++ )
		{	std::vector < double > & v = starting_vectors [d];
			assert ( v .size() == n );
			std::vector < double > Xv ( n, 0. );
			for ( size_t i = 0; i < n; i++ )
			for ( size_t j = 0; j < n; j++ )
				Xv[i] += matrix (i,j) * v[j];
			double norm_loc = tag::Util::norm_infty ( Xv );
			if ( norm_loc > max_norm )  max_norm = norm_loc;
			eigenval [d] = norm_loc;
			for ( size_t i = 0; i < n; i++ )
			{	double tmp = Xv[i] / norm_loc;
				if ( std::abs ( tmp - v[i] ) > tol )  good [d] = false;
				v[i] = tmp;                                             }   }
		
		d_good = starting_vectors .size();
		for ( size_t d = 0; d < starting_vectors .size(); d++ )
			if ( good [d] and ( eigenval [d] > 0.99 * max_norm ) )  d_good = d;
		if ( d_good < starting_vectors .size() )  break;                      }
	assert ( iter < 10000 );

	return d_good;                                                             }


inline double tag::Util::compute_isr ( const Tensor < double > & matrix )

// apply Gauss-Seidel SOR to compute the inverse of 'matrix'

// then apply the power method to 'inv_matr'
// starting simultaneously from several different vectors
// the first one to stabilize will be our eigenvector

{	assert ( matrix .dimensions .size() == 2 );
	size_t n = matrix .dimensions [0];
	assert ( n == matrix .dimensions [1] );

	Tensor < double > inv_matr (n,n);
  // candidate for inverse matrix
	double trace = 0.;
	for ( size_t i = 0; i < n; i++ )  trace += matrix (i,i);
	trace = n / trace;
	for ( size_t i = 0; i < n; i++ )  inv_matr (i,i) = trace;

	tag::Util::improve_inverse_matrix ( matrix, inv_matr );

	// we take the canonical basis in RRn and add the eigen_candidate
	std::vector < std::vector < double > > starting_vectors =
		tag::Util::ortho_basis_double [n];
	assert ( starting_vectors .size() == n );
	// all these vectors have norm infty equal to 1.
	
	// apply power method to inv_matr
	std::vector < double > eigenval ( starting_vectors .size() );
	size_t d_good = tag::Util::power_method ( inv_matr, starting_vectors, eigenval );
	assert ( d_good < starting_vectors .size() );
				
	return  1. / std::sqrt ( eigenval [ d_good ] );

}  // end of  tag::Util::compute_isr

//------------------------------------------------------------------------------------------------------//


class tag::Util::Metric::Constant::Matrix : public tag::Util::Metric::Constant

// abstract class, specialized in tag::Util::Metric::Constant::Matrix::Simple, SquareRoot

{	public :

	// seven pointers to functions, more precisely, pointers to static methods
	// inherited from tag::Util::Metric::Core :
	// frontal_method_nw_***, frontal_method_q_***, remove_node_isr_p

	Tensor < double > matrix;  // nxn, symmetric positive definite
	double isr;  // inner spectral radius
	// the interpretation of 'matrix' and 'isr' is different in each specialization
	
	inline Matrix ( const Tensor < double > & m )
	:	tag::Util::Metric::Constant(), matrix { m }, isr { tag::Util::compute_isr (m) }
	{	assert ( matrix .dimensions .size() == 2 );
		assert ( matrix .dimensions [0] == matrix .dimensions [1] );  }
	
	inline Matrix ( const Tensor < double > & m, const double r )
	:	tag::Util::Metric::Constant(), matrix { m }, isr { r }
	{	assert ( matrix .dimensions .size() == 2 );
		assert ( matrix .dimensions [0] == matrix .dimensions [1] );  }
	
	inline Matrix ( const double a11, const double a12, const double a22 )
	:	tag::Util::Metric::Constant(), matrix ( 2, 2 )
	{ matrix (0,0) = a11;
		matrix (1,0) = matrix (0,1) = a12;
		matrix (1,1) = a22;
		isr = tag::Util::compute_isr ( matrix );  }
	
	inline Matrix ( const double a11, const double a12, const double a22, const double r )
	:	tag::Util::Metric::Constant(), matrix ( 2, 2 ), isr { r }
	{ matrix (0,0) = a11;
		matrix (1,0) = matrix (0,1) = a12;
		matrix (1,1) = a22;                 }
	
	inline Matrix ( const double a11, const double a12, const double a13,
	                const double a22, const double a23, const double a33 )
	:	tag::Util::Metric::Constant(), matrix ( 3, 3 )
	{	matrix (0,0) = a11;
		matrix (0,1) = matrix (1,0) = a12;
		matrix (0,2) = matrix (2,0) = a13;
		matrix (1,1) = a22;
		matrix (1,2) = matrix (2,1) = a23;
		matrix (2,2) = a33;
		isr = tag::Util::compute_isr ( matrix );  }
	
	inline Matrix ( const double a11, const double a12, const double a13,
	                const double a22, const double a23, const double a33, const double r )
	:	tag::Util::Metric::Constant(), matrix ( 3, 3 ), isr { r }
	{	matrix (0,0) = a11;
		matrix (0,1) = matrix (1,0) = a12;
		matrix (0,2) = matrix (2,0) = a13;
		matrix (1,1) = a22;
		matrix (1,2) = matrix (2,1) = a23;
		matrix (2,2) = a33;                 }
	
	virtual ~Matrix ( );

	using tag::Util::Metric::Core::operator=;  // operator=, copy and move, deleted

	// tag::Util::MetricCore * clone ( )  stays pure virtual from tag::Util::Metric::Core

	// tag::Util::MetricCore * regist ( )   virtual from tag::Util::Metric::Core
	// defined there, execution forbidden
	
	bool isotropic ( );  // virtual from tag::Util::Metric::Core
	
	// double inner_prod ( v, w )  virtual from tag::Util::Metric::Core,
	// defined by tag::Util::Metric::Variable, execution forbidden
	
	// double inner_prod ( P, v, w )  stays pure virtual from tag::Util::Metric::Core

	// double inner_prod ( P, Q, v, w )  virtual from tag::Util::Metric::Core, defined there
	// will be overridden for anisotropic metrics

	// double inner_spectral_radius ( const Cell & A )  stays pure virtual from tag::Util::Metric::Core
	
	// double avrg_spectral_radius ( const Cell & A )
	// virtual from tag::Util::Metric::Core, defined there, will be overridden
	
	// two 'scale_sq' and two 'scale_sq_inv' stay pure virtual from tag::Util::Metric::Core

	// inlined 'sq_dist' defined by tag::Util::Metric::Core

	// two versions of  compute_data
	// virtual from tag::Util::Metric::Trivial, defined there, execution forbidden

	// methods codim_0, codim_1, de_activate virtual from tag::Util::MetricCore,
	// defined there, execution forbidden

	// correct_ext_prod_{2,3}d  stay pure virtual from tag::Util::MetricCore
	
	class Simple;  class SquareRoot;
	
};  // end of  class tag::Util::Metric::Constant::Matrix

//------------------------------------------------------------------------------------------------------//

	
class tag::Util::Metric::Constant::Matrix::Simple : public tag::Util::Metric::Constant::Matrix

{	public :

	// seven pointers to functions, more precisely, pointers to static methods
	// inherited from tag::Util::Metric::Core :
	// frontal_method_nw_***, frontal_method_q_***, remove_node_isr_p

	// Tensor < double > matrix  // nxn, symmetric positive definite
	// double isr  // inner spectral radius
	// inherited from tag::Util::Metric::Variable::Matrix

	// here  <u,v> = matrix_ij u_j v_i
	//       'isr' is the square root of the inner spectral radius of 'matrix'
	//       method  inner_spectral_radius  simply returns 'isr'
	
	inline Simple ( const Tensor < double > & m )
	: tag::Util::Metric::Constant::Matrix (m)
	{	}
	
	inline Simple ( const Tensor < double > & m, const double r )
	: tag::Util::Metric::Constant::Matrix ( m, r )
	{	}
	
	inline Simple ( const double a11, const double a12, const double a22 )
	:	tag::Util::Metric::Constant::Matrix ( a11, a12, a22 )
	{ }
	
	inline Simple ( const double a11, const double a12, const double a22, const double r )
	:	tag::Util::Metric::Constant::Matrix ( a11, a12, a22, r )
	{ }
	
	inline Simple ( const double a11, const double a12, const double a13,
	                const double a22, const double a23, const double a33 )
	:	tag::Util::Metric::Constant::Matrix ( a11, a12, a13, a22, a23, a33 )
	{ }
	
	inline Simple ( const double a11, const double a12, const double a13,
	                const double a22, const double a23, const double a33, const double r )
	:	tag::Util::Metric::Constant::Matrix ( a11, a12, a13, a22, a23, a33, r )
	{ }
	
	virtual ~Simple ( );

	using tag::Util::Metric::Core::operator=;  // operator=, copy and move, deleted

	tag::Util::MetricCore * clone ( );  // virtual from tag::Util::Metric::Core

	// tag::Util::MetricCore * regist ( )   virtual from tag::Util::Metric::Core
	// defined there, execution forbidden
	
	// bool isotropic ( )  virtual from tag::Util::Metric::Core
	// defined by tag::Util::Metric::Constant::Matrix
	
	// inner product, virtual from tag::Util::Metric::Core
	double inner_prod ( const std::vector < double > & v, const std::vector < double > & w );	

	// double inner_prod ( P, v, w )  virtual from tag::Util::Metric::Core,
	// defined by tag::Util::Metric::Constant

	// double inner_prod ( P, Q, v, w )  virtual from tag::Util::Metric::Core,
	// defined by tag::Util::Metric::Constant

	double inner_spectral_radius ( const Cell & A );  // virtual from tag::Util::Metric::Core
	// return 'isr'

	double avrg_spectral_radius ( const Cell & A ) override;  // virtual from tag::Util::Metric::Core
	
	// two 'scale_sq' and two 'scale_sq_inv' methods are virtual from tag::Util::Metric::Core
	tag::Util::MetricCore * scale_sq ( const Function & f );
	tag::Util::MetricCore * scale_sq ( const double f );
	tag::Util::MetricCore * scale_sq_inv ( const Function & f );
	tag::Util::MetricCore * scale_sq_inv ( const double f );

	// inlined 'sq_dist' defined by tag::Util::Metric::Core

	// two versions of  compute_data
	// virtual from tag::Util::Metric::Trivial, defined there, execution forbidden

	// methods codim_0, codim_1, de_activate virtual from tag::Util::MetricCore,
	// defined there, execution forbidden

	void correct_ext_prod_2d ( const Cell & P, std::vector < double > & n );
	void correct_ext_prod_3d ( const Cell & P, std::vector < double > & n );
	// virtual from tag::Util::MetricCore, defined in frontal.cpp
	
};  // end of  class tag::Util::Metric::Constant::Matrix::Simple

//------------------------------------------------------------------------------------------------------//

	
class tag::Util::Metric::Constant::Matrix::SquareRoot : public tag::Util::Metric::Constant::Matrix

{	public :

	// seven pointers to functions, more precisely, pointers to static methods
	// inherited from tag::Util::Metric::Core :
	// frontal_method_nw_***, frontal_method_q_***, remove_node_isr_p

	// Tensor < double > matrix  // nxn, symmetric positive definite
	// double isr  // inner spectral radius
	// inherited from tag::Util::Metric::Variable::Matrix

	// here  <u,v> = matrix_jk u_j matrix_ik v_i
	//       'isr' is the inner spectral radius of 'matrix'
	//       method  inner_spectral_radius  simply returns 'isr'
	
	inline SquareRoot ( const Tensor < double > & m )
	: tag::Util::Metric::Constant::Matrix (m)
	{	}
	
	inline SquareRoot ( const Tensor < double > & m, const double r )
	: tag::Util::Metric::Constant::Matrix ( m, r )
	{	}
	
	inline SquareRoot ( const double a11, const double a12, const double a22 )
	:	tag::Util::Metric::Constant::Matrix ( a11, a12, a22 )
	{ }
	
	inline SquareRoot ( const double a11, const double a12, const double a22, const double r )
	:	tag::Util::Metric::Constant::Matrix ( a11, a12, a22, r )
	{ }
	
	inline SquareRoot ( const double a11, const double a12, const double a13,
	                const double a22, const double a23, const double a33 )
	:	tag::Util::Metric::Constant::Matrix ( a11, a12, a13, a22, a23, a33 )
	{ }
	
	inline SquareRoot ( const double a11, const double a12, const double a13,
	                const double a22, const double a23, const double a33, const double r )
	:	tag::Util::Metric::Constant::Matrix ( a11, a12, a13, a22, a23, a33, r )
	{ }
	
	virtual ~SquareRoot ( );

	using tag::Util::Metric::Core::operator=;  // operator=, copy and move, deleted

	tag::Util::MetricCore * clone ( );  // virtual from tag::Util::Metric::Core

	// tag::Util::MetricCore * regist ( )   virtual from tag::Util::Metric::Core
	// defined there, execution forbidden
	
	// bool isotropic ( )  virtual from tag::Util::Metric::Core
	// defined by tag::Util::Metric::Constant::Matrix
	
	// inner product, virtual from tag::Util::Metric::Core
	double inner_prod ( const std::vector < double > & v, const std::vector < double > & w );	

	// double inner_prod ( P, v, w )  virtual from tag::Util::Metric::Core,
	// defined by tag::Util::Metric::Constant

	// double inner_prod ( P, Q, v, w )  virtual from tag::Util::Metric::Core,
	// defined by tag::Util::Metric::Constant

	double inner_spectral_radius ( const Cell & A );  // virtual from tag::Util::Metric::Core
	// return 'isr'

	double avrg_spectral_radius ( const Cell & A ) override;  // virtual from tag::Util::Metric::Core
	
	// two 'scale_sq' and two 'scale_sq_inv' methods are virtual from tag::Util::Metric::Core
	tag::Util::MetricCore * scale_sq ( const Function & f );
	tag::Util::MetricCore * scale_sq ( const double f );
	tag::Util::MetricCore * scale_sq_inv ( const Function & f );
	tag::Util::MetricCore * scale_sq_inv ( const double f );

	// inlined 'sq_dist' defined by tag::Util::Metric::Core

	// two versions of  compute_data
	// virtual from tag::Util::Metric::Trivial, defined there, execution forbidden

	// methods codim_0, codim_1, de_activate virtual from tag::Util::MetricCore,
	// defined there, execution forbidden

	void correct_ext_prod_2d ( const Cell & P, std::vector < double > & n );
	void correct_ext_prod_3d ( const Cell & P, std::vector < double > & n );
	// virtual from tag::Util::MetricCore, defined in frontal.cpp
	
};  // end of  class tag::Util::Metric::Constant::Matrix::SquareRoot

//------------------------------------------------------------------------------------------------------//

	
class tag::Util::Metric::Variable : public tag::Util::Metric::Core

// abstract class, specialized in tag::Util::Metric::Variable::Isotropic, Matrix::***

{	public :

	// seven pointers to functions, more precisely, pointers to static methods
	// inherited from tag::Util::Metric::Core :
	// frontal_method_nw_***, frontal_method_q_***, remove_node_isr_p

	inline   Variable ( ) : tag::Util::Metric::Core()  { }

	virtual ~Variable ( );

	using tag::Util::Metric::Core::operator=;  // operator=, copy and move, deleted

	// tag::Util::MetricCore * clone ( )  stays pure virtual from tag::Util::Metric::Core

	// tag::Util::MetricCore * regist ( )   virtual from tag::Util::Metric::Core
	// defined there, execution forbidden
	
	// bool isotropic   stays pure virtual from tag::Util::Metric::Core
	
	double inner_prod ( const std::vector < double > & v, const std::vector < double > & w );
	// virtual from tag::Util::Metric::Core, here execution forbidden
	
	// double inner_prod ( P, v, w )  stays pure virtual from tag::Util::Metric::Core

	// double inner_prod ( P, Q, v, w )  virtual from tag::Util::Metric::Core, defined there
	// will be overridden for anisotropic metrics

	// double inner_spectral_radius ( const Cell & A )  stays pure virtual from tag::Util::Metric::Core

	// double avrg_spectral_radius ( const Cell & A )
	// virtual from tag::Util::Metric::Core, defined there, return inner_spectral_radius
	
	// two 'scale_sq' and two 'scale_sq_inv' stay pure virtual from tag::Util::Metric::Core

	// inlined 'sq_dist' defined by tag::Util::Metric::Core

	// two versions of  compute_data
	// virtual from tag::Util::Metric::Trivial, defined there, execution forbidden

	// methods codim_0, codim_1, de_activate virtual from tag::Util::MetricCore,
	// defined there, execution forbidden

	// correct_ext_prod_{2,3}d  stay pure virtual from tag::Util::MetricCore
	
	class Isotropic;  class Matrix;
	
};  // end of  class tag::Util::Metric::Variable

//------------------------------------------------------------------------------------------------------//

	
class tag::Util::Metric::Variable::Isotropic : public tag::Util::Metric::Variable

{	public :

	// seven pointers to functions, more precisely, pointers to static methods
	// inherited from tag::Util::Metric::Core :
	// frontal_method_nw_***, frontal_method_q_***, remove_node_isr_p

	Function zoom;  // the norm gets zoomed by zoom'
	// the inner product gets zoomed by square of 'zoom'
	
	inline   Isotropic ( const Function & z ) : tag::Util::Metric::Variable(), zoom {z} { }

	virtual ~Isotropic ( );

	using tag::Util::Metric::Core::operator=;  // operator=, copy and move, deleted

	tag::Util::MetricCore * clone ( );  // virtual from tag::Util::Metric::Core

	// tag::Util::MetricCore * regist ( )   virtual from tag::Util::Metric::Core
	// defined there, execution forbidden
	
	bool isotropic ( );  // virtual from tag::Util::Metric::Core
	
	// double inner_prod ( v, w )  virtual from tag::Util::Metric::Core,
	// defined by tag::Util::Metric::Variable, execution forbidden

	double inner_prod  // virtual fromm tag::Util::Metric
	( const Cell & P, const std::vector < double > & v, const std::vector < double > & w );	

	double inner_prod ( const Cell & P, const Cell & Q,
	  const std::vector < double > & v, const std::vector < double > & w ) override;	
	// virtual from tag::Util::Metric::Core, here overridden just for a slight speedup

	double inner_spectral_radius ( const Cell & A );  // virtual from tag::Util::Metric::Core
	// return 'zoom' at point; here, could be called length_of_unit
	
	// double avrg_spectral_radius ( const Cell & A )
	// virtual from tag::Util::Metric::Core, defined there, return inner_spectral_radius
	
	tag::Util::MetricCore * scale_sq ( const Function & f );
	tag::Util::MetricCore * scale_sq ( const double f );
	tag::Util::MetricCore * scale_sq_inv ( const Function & f );
	tag::Util::MetricCore * scale_sq_inv ( const double f );

	// inlined 'sq_dist' defined by tag::Util::Metric::Core

	// two versions of  compute_data
	// virtual from tag::Util::Metric::Trivial, defined there, execution forbidden

	// methods codim_0, codim_1, de_activate virtual from tag::Util::MetricCore,
	// defined there, execution forbidden

	void correct_ext_prod_2d ( const Cell & P, std::vector < double > & n );
	void correct_ext_prod_3d ( const Cell & P, std::vector < double > & n );
	// virtual from tag::Util::MetricCore, defined in frontal.cpp, here does nothing
	
};  // end of  class tag::Util::Metric::Variable::Isotropic

//------------------------------------------------------------------------------------------------------//

	
class tag::Util::Metric::Variable::Matrix : public tag::Util::Metric::Variable

// abstract class, specialized in tag::Util::Metric::Variable::Matrix::Simple,
// SquareRoot, ISR, SquareRoot::ISR

{	public :

	// seven pointers to functions, more precisely, pointers to static methods
	// inherited from tag::Util::Metric::Core :
	// frontal_method_nw_***, frontal_method_q_***, remove_node_isr_p

	Tensor < Function > matrix;  // nxn, symmetric positive definite
	// the interpretation of this matrix is different in each specialization
	
	inline Matrix ( const Tensor < Function > & m )
	: tag::Util::Metric::Variable(), matrix {m}
	{ assert ( matrix .dimensions .size() == 2 );
		assert ( matrix .dimensions [0] == matrix .dimensions [1] );  }
	
	inline Matrix ( const Function & a11, const Function & a12, const Function & a22 )
	:	tag::Util::Metric::Variable(), matrix ( 2, 2 )
	{ matrix (0,0) = a11;
		matrix (1,0) = matrix (0,1) = a12;
		matrix (1,1) = a22;                }
	
	inline Matrix ( const Function & a11, const Function & a12, const Function & a13,
	                const Function & a22, const Function & a23, const Function & a33 )
	:	tag::Util::Metric::Variable(), matrix ( 3, 3 )
	{	matrix (0,0) = a11;
		matrix (0,1) = matrix (1,0) = a12;
		matrix (0,2) = matrix (2,0) = a13;
		matrix (1,1) = a22;
		matrix (1,2) = matrix (2,1) = a23;
		matrix (2,2) = a33;                }
	
	virtual ~Matrix ( );

	using tag::Util::Metric::Core::operator=;  // operator=, copy and move, deleted

	// tag::Util::MetricCore * clone ( )  stays pure virtual from tag::Util::Metric::Core

	tag::Util::MetricCore * regist ( ) override;  //  virtual from tag::Util::Metric::Core
	// here returns clone, later overridden again for SquareRoot::ISR
	
	bool isotropic ( );  // virtual from tag::Util::Metric::Core
	
	// double inner_prod ( v, w )  virtual from tag::Util::Metric::Core,
	// defined by tag::Util::Metric::Variable, execution forbidden
	
	// double inner_prod ( P, v, w )  stays pure virtual from tag::Util::Metric::Core

	// double inner_prod ( P, Q, v, w )  virtual from tag::Util::Metric::Core, defined there
	// will be overridden for anisotropic metrics

	// double inner_spectral_radius ( const Cell & A )  stays pure virtual from tag::Util::Metric::Core
	
	// double avrg_spectral_radius ( const Cell & A )
	// virtual from tag::Util::Metric::Core, defined there, will be overridden
	
	// two 'scale_sq' and two 'scale_sq_inv' methods are virtual from tag::Util::Metric::Core
	tag::Util::MetricCore * scale_sq ( const Function & f );
	tag::Util::MetricCore * scale_sq ( const double f );
	tag::Util::MetricCore * scale_sq_inv ( const Function & f );
	tag::Util::MetricCore * scale_sq_inv ( const double f );

	// inlined 'sq_dist' defined by tag::Util::Metric::Core

	// two versions of  compute_data
	// virtual from tag::Util::Metric::Trivial, defined there, execution forbidden

	// methods codim_0, codim_1, de_activate virtual from tag::Util::MetricCore,
	// defined there, execution forbidden

	// correct_ext_prod_{2,3}d  stay pure virtual from tag::Util::MetricCore
	
	class Simple;  class SquareRoot;  class ISR;  // inner spectral radius
	
};  // end of  class tag::Util::Metric::Variable::Matrix

//------------------------------------------------------------------------------------------------------//

	
class tag::Util::Metric::Variable::Matrix::Simple : public tag::Util::Metric::Variable::Matrix

{	public :

	// seven pointers to functions, more precisely, pointers to static methods
	// inherited from tag::Util::Metric::Core :
	// frontal_method_nw_***, frontal_method_q_***, remove_node_isr_p

	// Tensor < Function > matrix;  // nxn, symmetric positive definite
	// inherited from tag::Util::Metric::Variable::Matrix

	// here  <u,v> = matrix_ij u_j v_i
	//       method inner_spectral_radius  gives the square root of the inner spectral radius of 'matrix'

	void ( * compute_inner_data_3 )  // pointer to function, more precisely, pointer to static method
	( const tag::Util::Metric::Variable::Matrix::Simple * that, const tag::AtPoint &, const Cell & V );

	void ( * compute_inner_data_5 )  // pointer to function, more precisely, pointer to static method
	( const tag::Util::Metric::Variable::Matrix::Simple * that,
	  const tag::AtPoint &, const Cell & W, const tag::UseInformationFrom &, const Cell & V );
	
	static void compute_data_3_forbid  // defined in frontal.cpp, execution forbidden
	// called through pointer compute_inner_data_3
	( const tag::Util::Metric::Variable::Matrix::Simple * that, const tag::AtPoint &, const Cell & V );

	static void compute_data_5_forbid  // defined in frontal.cpp, execution forbidden
	// called through pointer compute_inner_data_5
	( const tag::Util::Metric::Variable::Matrix::Simple * that,
	  const tag::AtPoint &, const Cell & W, const tag::UseInformationFrom &, const Cell & V );

	inline void make_active ( )
	{	frontal_method_nw_oc  = & tag::Util::Metric::frontal_method_nw_active_oc;
		frontal_method_nw_tow = & tag::Util::Metric::frontal_method_nw_active_tow;
		frontal_method_nw_nem = & tag::Util::Metric::frontal_method_nw_active_nem;
		#ifndef MANIFEM_NO_QUOTIENT
		frontal_method_q_oc   = & tag::Util::Metric::frontal_method_q_active_oc;
		frontal_method_q_tow  = & tag::Util::Metric::frontal_method_q_active_tow;
		frontal_method_q_nem  = & tag::Util::Metric::frontal_method_q_active_nem;
		#endif  // ifndef MANIFEM_NO_QUOTIENT
		compute_inner_data_3  = & tag::Util::Metric::Variable::Matrix::Simple::compute_data_3_forbid;
		compute_inner_data_5  = & tag::Util::Metric::Variable::Matrix::Simple::compute_data_5_forbid;  }
  
	inline Simple ( const Tensor < Function > & m )
	: tag::Util::Metric::Variable::Matrix (m)
	{	make_active();  }
	
	inline Simple ( const Function & a11, const Function & a12, const Function & a22 )
	:	tag::Util::Metric::Variable::Matrix ( a11, a12, a22 )
	{ make_active();  }
	
	inline Simple ( const Function & a11, const Function & a12, const Function & a13,
	                const Function & a22, const Function & a23, const Function & a33 )
	:	tag::Util::Metric::Variable::Matrix ( a11, a12, a13, a22, a23, a33 )
	{	make_active();  }
	
	virtual ~Simple ( );

	using tag::Util::Metric::Core::operator=;  // operator=, copy and move, deleted

	tag::Util::MetricCore * clone ( );  // virtual from tag::Util::Metric::Core

	// tag::Util::MetricCore * regist ( )   virtual from tag::Util::Metric::Core
	// defined by tag::Util::Metric::Variable::Matrix, returns clone
	
	// bool isotropic ( )  virtual from tag::Util::Metric::Core
	// defined by tag::Util::Metric::Variable::Matrix
	
	// double inner_prod ( v, w )  virtual from tag::Util::Metric::Core,
	// defined by tag::Util::Metric::Variable, execution forbidden

	virtual double inner_prod  // virtual from tag::Util::Metric::Core
	( const Cell & P, const std::vector < double > & v, const std::vector < double > & w );
	// later overridden by SquareRoot, ISR, SquarerRoot::ISR

	virtual double inner_prod ( const Cell & P, const Cell & Q,  // virtual from tag::Util::Metric::Core
	  const std::vector < double > & v, const std::vector < double > & w ) override;	
	// later overridden by SquareRoot, ISR, SquarerRoot::ISR

	double inner_spectral_radius ( const Cell & A );  // virtual from tag::Util::Metric::Core
	// defined in frontal.cpp, return square root of the inner spectral radius of 'matrix'

	double avrg_spectral_radius ( const Cell & A ) override;  // virtual from tag::Util::Metric::Core
	
	// two 'scale_sq' and two 'scale_sq_inv' methods are virtual from tag::Util::Metric::Core
	// defined by tag::Util::Metric::Variable::Matrix

	// inlined 'sq_dist' defined by tag::Util::Metric::Core

	void compute_data ( const tag::AtPoint &, const Cell & V ) override;
	// virtual from tag::Util::Metric::Trivial, interacts with the container, defined in frontal.cpp

	void compute_data
	( const tag::AtPoint &, const Cell & W, const tag::UseInformationFrom &, const Cell & V ) override;
	// virtual from tag::Util::Metric::Trivial, interacts with the container, defined in frontal.cpp

	void codim_0 ( ) override;   // virtual from tag::Util::MetricCore, defined in frontal.cpp
	void codim_1 ( ) override;   // virtual from tag::Util::MetricCore, defined in frontal.cpp

	// void de_activate ( )  virtual from tag::Util::MetricCore, defined there, execution forbidden
	
	static void compute_data_3_codim_0  // defined in frontal.cpp
	// called through pointer compute_inner_data_3
	( const tag::Util::Metric::Variable::Matrix::Simple * that, const tag::AtPoint &, const Cell & V );

	static void compute_data_5_codim_0  // defined in frontal.cpp
	// called through pointer compute_inner_data_5
	( const tag::Util::Metric::Variable::Matrix::Simple * that,
	  const tag::AtPoint &, const Cell & W, const tag::UseInformationFrom &, const Cell & V );

	static void compute_data_3_codim_1  // defined in frontal.cpp
	// called through pointer compute_inner_data_3
	( const tag::Util::Metric::Variable::Matrix::Simple * that, const tag::AtPoint &, const Cell & V );

	static void compute_data_5_codim_1  // defined in frontal.cpp
	// called through pointer compute_inner_data_5
	( const tag::Util::Metric::Variable::Matrix::Simple * that,
	  const tag::AtPoint &, const Cell & W, const tag::UseInformationFrom &, const Cell & V );

	static void remove_node_isr_codim_0 ( const Cell & V );  // called through pointer remove_node_isr_p
	static void remove_node_isr_codim_1 ( const Cell & V );  // called through pointer remove_node_isr_p
	
	void correct_ext_prod_2d ( const Cell & P, std::vector < double > & n );
	void correct_ext_prod_3d ( const Cell & P, std::vector < double > & n );
	// virtual from tag::Util::MetricCore, defined in frontal.cpp
	
};  // end of  class tag::Util::Metric::Variable::Matrix::Simple

//------------------------------------------------------------------------------------------------------//


class tag::Util::Metric::Variable::Matrix::SquareRoot : public tag::Util::Metric::Variable::Matrix

{	public :

	// seven pointers to functions, more precisely, pointers to static methods
	// inherited from tag::Util::Metric::Core :
	// frontal_method_nw_***, frontal_method_q_***, remove_node_isr_p

	// Tensor < Function > matrix;  // nxn, symmetric positive definite
	// inherited from tag::Util::Metric::Variable::Matrix

	// here  <u,v> = matrix_jk u_j matrix_ik v_i
	//       method  inner_spectral_radius  gives the inner spectral radius of 'matrix'
	
	void ( * compute_inner_data_3 )  // pointer to function, more precisely, pointer to static method
	( const tag::Util::Metric::Variable::Matrix::SquareRoot * that, const tag::AtPoint &, const Cell & V );

	void ( * compute_inner_data_5 )  // pointer to function, more precisely, pointer to static method
	( const tag::Util::Metric::Variable::Matrix::SquareRoot * that,
	  const tag::AtPoint &, const Cell & W, const tag::UseInformationFrom &, const Cell & V );
	
	static void compute_data_3_forbid  // defined in frontal.cpp, execution forbidden
	// called through pointer compute_inner_data_3
	( const tag::Util::Metric::Variable::Matrix::SquareRoot * that, const tag::AtPoint &, const Cell & V );

	static void compute_data_5_forbid  // defined in frontal.cpp, execution forbidden
	// called through pointer compute_inner_data_5
	( const tag::Util::Metric::Variable::Matrix::SquareRoot * that,
	  const tag::AtPoint &, const Cell & W, const tag::UseInformationFrom &, const Cell & V );

	inline void make_active ( )
	{	frontal_method_nw_oc  = & tag::Util::Metric::frontal_method_nw_active_oc;
		frontal_method_nw_tow = & tag::Util::Metric::frontal_method_nw_active_tow;
		frontal_method_nw_nem = & tag::Util::Metric::frontal_method_nw_active_nem;
		#ifndef MANIFEM_NO_QUOTIENT
		frontal_method_q_oc   = & tag::Util::Metric::frontal_method_q_active_oc;
		frontal_method_q_tow  = & tag::Util::Metric::frontal_method_q_active_tow;
		frontal_method_q_nem  = & tag::Util::Metric::frontal_method_q_active_nem;
		#endif  // ifndef MANIFEM_NO_QUOTIENT
		compute_inner_data_3  = & tag::Util::Metric::Variable::Matrix::SquareRoot::compute_data_3_forbid;
		compute_inner_data_5  = & tag::Util::Metric::Variable::Matrix::SquareRoot::compute_data_5_forbid;   }
  
	inline SquareRoot ( const Tensor < Function > & m )
	: tag::Util::Metric::Variable::Matrix (m)
	{	make_active();  }
	
	inline SquareRoot ( const Function & a11, const Function & a12, const Function & a22 )
	:	tag::Util::Metric::Variable::Matrix ( a11, a12, a22 )
	{	make_active();  }
	
	inline SquareRoot ( const Function & a11, const Function & a12, const Function & a13,
	                    const Function & a22, const Function & a23, const Function & a33 )
	:	tag::Util::Metric::Variable::Matrix ( a11, a12, a13, a22, a23, a33 )
	{	make_active();  }
	
	virtual ~SquareRoot ( );

	using tag::Util::Metric::Core::operator=;  // operator=, copy and move, deleted

	tag::Util::MetricCore * clone ( ) override;  // virtual from tag::Util::Metric::Core

	// tag::Util::MetricCore * regist ( )   virtual from tag::Util::Metric::Core
	// defined by tag::Util::Metric::Variable::Matrix, returns clone
	
	// bool isotropic ( )  virtual from tag::Util::Metric::Core
	// defined by tag::Util::Metric::Variable::Matrix
	
	// double inner_prod ( v, w )  virtual from tag::Util::Metric::Core,
	// defined by tag::Util::Metric::Variable, execution forbidden

	virtual double inner_prod  // virtual from tag::Util::Metric::Core
	( const Cell & P, const std::vector < double > & v, const std::vector < double > & w ) override;

	virtual double inner_prod ( const Cell & P, const Cell & Q,  // virtual from tag::Util::Metric::Core
	  const std::vector < double > & v, const std::vector < double > & w ) override;	

	double inner_spectral_radius ( const Cell & A );  // virtual from tag::Util::Metric::Core
	// defined in frontal.cpp, return the inner spectral radius of 'matrix'

	double avrg_spectral_radius ( const Cell & A ) override;  // virtual from tag::Util::Metric::Core
	
	// two 'scale_sq' and two 'scale_sq_inv' methods are virtual from tag::Util::Metric::Core
	// defined by tag::Util::Metric::Variable::Matrix

	// inlined 'sq_dist' defined by tag::Util::Metric::Core

	void compute_data ( const tag::AtPoint &, const Cell & V ) override;
	// virtual from tag::Util::Metric::Trivial, interacts with the container, defined in frontal.cpp

	void compute_data
	( const tag::AtPoint &, const Cell & W, const tag::UseInformationFrom &, const Cell & V ) override;
	// virtual from tag::Util::Metric::Trivial, interacts with the container, defined in frontal.cpp

	void codim_0 ( ) override;   // virtual from tag::Util::MetricCore, defined in frontal.cpp
	void codim_1 ( ) override;   // virtual from tag::Util::MetricCore, defined in frontal.cpp

	// void de_activate ( )  virtual from tag::Util::MetricCore, defined there, execution forbidden

	static void compute_data_3_codim_0  // defined in frontal.cpp
	// called through pointer compute_inner_data_3
	( const tag::Util::Metric::Variable::Matrix::SquareRoot * that, const tag::AtPoint &, const Cell & V );

	static void compute_data_5_codim_0  // defined in frontal.cpp
	// called through pointer compute_inner_data_5
	( const tag::Util::Metric::Variable::Matrix::SquareRoot * that,
	  const tag::AtPoint &, const Cell & W, const tag::UseInformationFrom &, const Cell & V );

	static void compute_data_3_codim_1  // defined in frontal.cpp
	// called through pointer compute_inner_data_3
	( const tag::Util::Metric::Variable::Matrix::SquareRoot * that, const tag::AtPoint &, const Cell & V );

	static void compute_data_5_codim_1  // defined in frontal.cpp
	// called through pointer compute_inner_data_5
	( const tag::Util::Metric::Variable::Matrix::SquareRoot * that,
	  const tag::AtPoint &, const Cell & W, const tag::UseInformationFrom &, const Cell & V );

	static void remove_node_isr_codim_0 ( const Cell & V );  // called through pointer remove_node_isr_p
	static void remove_node_isr_codim_1 ( const Cell & V );  // called through pointer remove_node_isr_p
	
	void correct_ext_prod_2d ( const Cell & P, std::vector < double > & n );
	void correct_ext_prod_3d ( const Cell & P, std::vector < double > & n );
	// virtual from tag::Util::MetricCore, defined in frontal.cpp
	
	class ISR;  // inner spectral radius

};  // end of  class tag::Util::Metric::Variable::Matrix::SquareRoot

//------------------------------------------------------------------------------------------------------//


class tag::Util::Metric::Variable::Matrix::ISR : public tag::Util::Metric::Variable::Matrix

{	public :

	// seven pointers to functions, more precisely, pointers to static methods
	// inherited from tag::Util::Metric::Core :
	// frontal_method_nw_***, frontal_method_q_***, remove_node_isr_p

	// Tensor < Function > matrix;  // nxn, symmetric positive definite
	// inherited from tag::Util::Metric::Variable::Matrix

	// here  <u,v> = isr u_i v_i + matrix_ij u_j v_i
	//       thus 'matrix' can be seen as the deviatoric part of  isr I + matrix
	//       'isr' is (a lower bound of) the inner spectral radius of  isr I + matrix
	//       method  inner_spectral_radius  gives the square root of 'isr' at point
	
	Function isr;  // inner spectral radius of 'matrix'

	void ( * compute_inner_data_3 )  // pointer to function, more precisely, pointer to static method
	( const tag::Util::Metric::Variable::Matrix::ISR * that, const tag::AtPoint &, const Cell & V );

	void ( * compute_inner_data_5 )  // pointer to function, more precisely, pointer to static method
	( const tag::Util::Metric::Variable::Matrix::ISR * that,
	  const tag::AtPoint &, const Cell & W, const tag::UseInformationFrom &, const Cell & V );
	
	static void compute_data_3_forbid  // defined in frontal.cpp, execution forbidden
	// called through pointer compute_inner_data_3
	( const tag::Util::Metric::Variable::Matrix::ISR * that, const tag::AtPoint &, const Cell & V );

	static void compute_data_5_forbid  // defined in frontal.cpp, execution forbidden
	// called through pointer compute_inner_data_5
	( const tag::Util::Metric::Variable::Matrix::ISR * that,
	  const tag::AtPoint &, const Cell & W, const tag::UseInformationFrom &, const Cell & V );
  
	inline void make_active ( )
	{	frontal_method_nw_oc  = & tag::Util::Metric::frontal_method_nw_active_oc;
		frontal_method_nw_tow = & tag::Util::Metric::frontal_method_nw_active_tow;
		frontal_method_nw_nem = & tag::Util::Metric::frontal_method_nw_active_nem;
		#ifndef MANIFEM_NO_QUOTIENT
		frontal_method_q_oc   = & tag::Util::Metric::frontal_method_q_active_oc;
		frontal_method_q_tow  = & tag::Util::Metric::frontal_method_q_active_tow;
		frontal_method_q_nem  = & tag::Util::Metric::frontal_method_q_active_nem;
		compute_inner_data_3  = & tag::Util::Metric::Variable::Matrix::ISR::compute_data_3_forbid;
		#endif  // ifndef MANIFEM_NO_QUOTIENT
		compute_inner_data_5  = & tag::Util::Metric::Variable::Matrix::ISR::compute_data_5_forbid;   }
	  
	inline ISR ( const Function & sr, const Tensor < Function > & m )
	: tag::Util::Metric::Variable::Matrix (m), isr { sr }
	{	make_active();  }
	
	inline ISR ( const Function & sr, const Function & a11, const Function & a12, const Function & a22 )
	: tag::Util::Metric::Variable::Matrix ( a11, a12, a22 ), isr { sr }
	{	make_active();  }
	
	inline ISR ( const Function & sr,
	             const Function & a11, const Function & a12, const Function & a13,
	             const Function & a22, const Function & a23, const Function & a33 )
	: tag::Util::Metric::Variable::Matrix ( a11, a12, a13, a22, a23, a33 ), isr { sr }
	{	make_active();  }
	
	virtual ~ISR ( );

	using tag::Util::Metric::Core::operator=;  // operator=, copy and move, deleted

	tag::Util::MetricCore * clone ( ) override;  // virtual from tag::Util::Metric::Core

	// tag::Util::MetricCore * regist ( )   virtual from tag::Util::Metric::Core
	// defined by tag::Util::Metric::Variable::Matrix, returns clone
	
	// bool isotropic ( )  virtual from tag::Util::Metric::Core
	// defined by tag::Util::Metric::Variable::Matrix
	
	// double inner_prod ( v, w )  virtual from tag::Util::Metric::Core,
	// defined by tag::Util::Metric::Variable, execution forbidden

	virtual double inner_prod  // virtual from tag::Util::Metric::Core
	( const Cell & P, const std::vector < double > & v, const std::vector < double > & w ) override;

	virtual double inner_prod ( const Cell & P, const Cell & Q,  // virtual from tag::Util::Metric::Core
	  const std::vector < double > & v, const std::vector < double > & w ) override;	

	double inner_spectral_radius ( const Cell & A );  // virtual from tag::Util::Metric::Core
	// defined in frontal.cpp, return square root of 'isr' at point

	double avrg_spectral_radius ( const Cell & A ) override;  // virtual from tag::Util::Metric::Core
	
	// two 'scale_sq' and two 'scale_sq_inv' methods are virtual from tag::Util::Metric::Core
	// defined by tag::Util::Metric::Variable::Matrix

	// inlined 'sq_dist' defined by tag::Util::Metric::Core

	void compute_data ( const tag::AtPoint &, const Cell & V ) override;
	// virtual from tag::Util::Metric::Trivial, interacts with the container, defined in frontal.cpp

	void compute_data
	( const tag::AtPoint &, const Cell & W, const tag::UseInformationFrom &, const Cell & V ) override;
	// virtual from tag::Util::Metric::Trivial, interacts with the container, defined in frontal.cpp

	void codim_0 ( ) override;   // virtual from tag::Util::MetricCore, defined in frontal.cpp
	void codim_1 ( ) override;   // virtual from tag::Util::MetricCore, defined in frontal.cpp

	// void de_activate ( )  virtual from tag::Util::MetricCore, defined there, execution forbidden

	static void compute_data_3_codim_0
	// called through pointer compute_inner_data_3
	( const tag::Util::Metric::Variable::Matrix::ISR * that, const tag::AtPoint &, const Cell & V );

	static void compute_data_5_codim_0
	// called through pointer compute_inner_data_5
	( const tag::Util::Metric::Variable::Matrix::ISR * that,
	  const tag::AtPoint &, const Cell & W, const tag::UseInformationFrom &, const Cell & V );

	static void compute_data_3_codim_1
	// called through pointer compute_inner_data_3
	( const tag::Util::Metric::Variable::Matrix::ISR * that, const tag::AtPoint &, const Cell & V );

	static void compute_data_5_codim_1
	// called through pointer compute_inner_data_5
	( const tag::Util::Metric::Variable::Matrix::ISR * that,
	  const tag::AtPoint &, const Cell & W, const tag::UseInformationFrom &, const Cell & V );

	static void remove_node_isr_codim_0 ( const Cell & V );  // called through pointer remove_node_isr_p
	static void remove_node_isr_codim_1 ( const Cell & V );  // called through pointer remove_node_isr_p
	
	void correct_ext_prod_2d ( const Cell & P, std::vector < double > & n );
	void correct_ext_prod_3d ( const Cell & P, std::vector < double > & n );
	// virtual from tag::Util::MetricCore, defined in frontal.cpp
	
};  // end of  class tag::Util::Metric::Variable::Matrix::ISR

//------------------------------------------------------------------------------------------------------//


class tag::Util::Metric::Variable::Matrix::SquareRoot::ISR : public tag::Util::Metric::Variable::Matrix

{	public :

	// seven pointers to functions, more precisely, pointers to static methods
	// inherited from tag::Util::Metric::Core :
	// frontal_method_nw_***, frontal_method_q_***, remove_node_isr_p

	// Tensor < Function > matrix;  // nxn, symmetric positive definite
	// inherited from tag::Util::Metric::Variable::Matrix

	// here  <u,v> = ( isr delta_jk + matrix_jk ) u_j ( isr delta_ik + matrix_ik ) v_i
	//       thus 'matrix' can be seen as the deviatoric part of  isr I + matrix
	//       'isr' is (a lower bound of) the inner spectral radius of  isr I + matrix
	//       method  inner_spectral_radius  returns isr at point
	
	Function isr;  // inner spectral radius of 'matrix'
	
	void ( * compute_inner_data_3 )  // pointer to function, more precisely, pointer to static method
	( const tag::Util::Metric::Variable::Matrix::SquareRoot::ISR * that,
	  const tag::AtPoint &, const Cell & V                              );

	void ( * compute_inner_data_5 )  // pointer to function, more precisely, pointer to static method
	( const tag::Util::Metric::Variable::Matrix::SquareRoot::ISR * that,
	  const tag::AtPoint &, const Cell & W, const tag::UseInformationFrom &, const Cell & V );
	
	static void compute_data_3_forbid  // defined in frontal.cpp, execution forbidden
	// called through pointer compute_inner_data_3
	( const tag::Util::Metric::Variable::Matrix::SquareRoot::ISR * that,
	  const tag::AtPoint &, const Cell & V                              );

	static void compute_data_5_forbid  // defined in frontal.cpp, execution forbidden
	// called through pointer compute_inner_data_5
	( const tag::Util::Metric::Variable::Matrix::SquareRoot::ISR * that,
	  const tag::AtPoint &, const Cell & W, const tag::UseInformationFrom &, const Cell & V );

	inline void make_active ( )
	{	frontal_method_nw_oc  = & tag::Util::Metric::frontal_method_nw_active_oc;
		frontal_method_nw_tow = & tag::Util::Metric::frontal_method_nw_active_tow;
		frontal_method_nw_nem = & tag::Util::Metric::frontal_method_nw_active_nem;
		#ifndef MANIFEM_NO_QUOTIENT
		frontal_method_q_oc   = & tag::Util::Metric::frontal_method_q_active_oc;
		frontal_method_q_tow  = & tag::Util::Metric::frontal_method_q_active_tow;
		frontal_method_q_nem  = & tag::Util::Metric::frontal_method_q_active_nem;
		#endif  // ifndef MANIFEM_NO_QUOTIENT
		compute_inner_data_3  =
			& tag::Util::Metric::Variable::Matrix::SquareRoot::ISR::compute_data_3_forbid;
		compute_inner_data_5  =
			& tag::Util::Metric::Variable::Matrix::SquareRoot::ISR::compute_data_5_forbid;   }
	
	inline ISR ( const Function & sr, const Tensor < Function > & m )
	: tag::Util::Metric::Variable::Matrix (m), isr { sr }
	{	make_active();  }
	
	inline ISR ( const Function & sr, const Function & a11, const Function & a12, const Function & a22 )
	: tag::Util::Metric::Variable::Matrix ( a11, a12, a22 ), isr { sr }
	{	make_active();  }
	
	inline ISR ( const Function & sr,
	             const Function & a11, const Function & a12, const Function & a13,
	             const Function & a22, const Function & a23, const Function & a33 )
	: tag::Util::Metric::Variable::Matrix ( a11, a12, a13, a22, a23, a33 ), isr { sr }
	{	make_active();  }
	
	virtual ~ISR ( );

	using tag::Util::Metric::Core::operator=;  // operator=, copy and move, deleted

	tag::Util::MetricCore * clone ( ) override;  // virtual from tag::Util::Metric::Core

	tag::Util::MetricCore * regist ( ) override;  //  virtual from tag::Util::Metric::Core
	// here overridden a second time, execution forbidden
	
	// bool isotropic ( )  virtual from tag::Util::Metric::Core
	// defined by tag::Util::Metric::Variable::Matrix
	
	// double inner_prod ( v, w )  virtual from tag::Util::Metric::Core,
	// defined by tag::Util::Metric::Variable, execution forbidden

	virtual double inner_prod  // virtual from tag::Util::Metric::Core
	( const Cell & P, const std::vector < double > & v, const std::vector < double > & w ) override;

	virtual double inner_prod ( const Cell & P, const Cell & Q,  // virtual from tag::Util::Metric::Core
	  const std::vector < double > & v, const std::vector < double > & w ) override;	

	double inner_spectral_radius ( const Cell & A );  // virtual from tag::Util::Metric::Core
	// defined in frontal.cpp, return 'isr' at point

	double avrg_spectral_radius ( const Cell & A ) override;  // virtual from tag::Util::Metric::Core
	
	// two 'scale_sq' and two 'scale_sq_inv' methods are virtual from tag::Util::Metric::Core
	// defined by tag::Util::Metric::Variable::Matrix

	// inlined 'sq_dist' defined by tag::Util::Metric::Core

	void compute_data ( const tag::AtPoint &, const Cell & V ) override;
	// virtual from tag::Util::Metric::Trivial, interacts with the container, defined in frontal.cpp

	void compute_data
	( const tag::AtPoint &, const Cell & W, const tag::UseInformationFrom &, const Cell & V ) override;
	// virtual from tag::Util::Metric::Trivial, interacts with the container, defined in frontal.cpp

	void codim_0 ( ) override;       // virtual from tag::Util::MetricCore, defined in frontal.cpp
	void codim_1 ( ) override;       // virtual from tag::Util::MetricCore, defined in frontal.cpp
	void de_activate ( ) override;   // virtual from tag::Util::MetricCore, defined in frontal.cpp

	static void compute_data_3_codim_0
	// called through pointer compute_inner_data_3
	( const tag::Util::Metric::Variable::Matrix::SquareRoot::ISR * that,
	  const tag::AtPoint &, const Cell & V                              );

	static void compute_data_5_codim_0
	// called through pointer compute_inner_data_5
	( const tag::Util::Metric::Variable::Matrix::SquareRoot::ISR * that,
	  const tag::AtPoint &, const Cell & W, const tag::UseInformationFrom &, const Cell & V );

	static void compute_data_3_codim_1
	// called through pointer compute_inner_data_3
	( const tag::Util::Metric::Variable::Matrix::SquareRoot::ISR * that,
	  const tag::AtPoint &, const Cell & V                              );

	static void compute_data_5_codim_1
	// called through pointer compute_inner_data_5
	( const tag::Util::Metric::Variable::Matrix::SquareRoot::ISR * that,
	  const tag::AtPoint &, const Cell & W, const tag::UseInformationFrom &, const Cell & V );

	static void remove_node_isr_codim_0 ( const Cell & V );  // called through pointer remove_node_isr_p
	static void remove_node_isr_codim_1 ( const Cell & V );  // called through pointer remove_node_isr_p
	
	void correct_ext_prod_2d ( const Cell & P, std::vector < double > & n );
	void correct_ext_prod_3d ( const Cell & P, std::vector < double > & n );
	// virtual from tag::Util::MetricCore, defined in frontal.cpp
	
};  // end of  class tag::Util::Metric::Variable::Matrix::SquareRoot::ISR

//------------------------------------------------------------------------------------------------------//


class tag::Util::Metric::ScaledSq : public tag::Util::Metric::Variable

// this class in useful for scaling an anisotropic metric, kept in 'base'
// scaling an isotropic metric is done directly, returns a tag::Util::Metric::***::Isotropic

{	public :

	// seven pointers to functions, more precisely, pointers to static methods
	// inherited from tag::Util::Metric::Core :
	// frontal_method_nw_***, frontal_method_q_***, remove_node_isr_p

	tag::Util::Metric base;  // anisotropic metric
	Function zoom;

	inline ScaledSq ( const tag::Util::Metric & b, const Function & z )
	: tag::Util::Metric::Variable(), base {b}, zoom {z}
		// we must copy frontal methods from the base to 'this'
	#ifndef MANIFEM_NO_QUOTIENT
	{	assert ( base .core );
		frontal_method_nw_oc  = base .core->frontal_method_nw_oc;
		frontal_method_nw_tow = base .core->frontal_method_nw_tow;
		frontal_method_nw_nem = base .core->frontal_method_nw_nem;
		frontal_method_q_oc   = base .core->frontal_method_q_oc;
		frontal_method_q_tow  = base .core->frontal_method_q_tow;
		frontal_method_q_nem  = base .core->frontal_method_q_nem;   }
	#else // MANIFEM_NO_QUOTIENT
	{	assert ( base .core );
		frontal_method_nw_oc  = base .core->frontal_method_nw_oc;
		frontal_method_nw_tow = base .core->frontal_method_nw_tow;
		frontal_method_nw_nem = base .core->frontal_method_nw_nem;  }
	#endif  // ifndef MANIFEM_NO_QUOTIENT

	virtual ~ScaledSq ( );

	using tag::Util::Metric::Core::operator=;  // operator=, copy and move, deleted

	tag::Util::MetricCore * clone ( );  // virtual from tag::Util::Metric::Core

	tag::Util::MetricCore * regist ( ) override;  //  virtual from tag::Util::Metric::Core
	// here returns base .core->regist(), often equivalent to base .core->clone()
	
	bool isotropic ( );  // virtual from tag::Util::Metric::Core
	
	// double inner_prod ( v, w )  virtual from tag::Util::Metric::Core,
	// defined by tag::Util::Metric::Variable, execution forbidden

	double inner_prod  // virtual from tag::Util::Metric::Core, here delegate to base then scale
	( const Cell & P, const std::vector < double > & v, const std::vector < double > & w );	

	double inner_prod ( const Cell & P, const Cell & Q,  // virtual from tag::Util::Metric
	  const std::vector < double > & v, const std::vector < double > & w ) override;
	// return  ( this->inner_prod ( P, v, w ) + this->inner_prod ( Q, v, w ) ) / 2.

	double inner_spectral_radius ( const Cell & A );  // virtual from tag::Util::Metric::Core

	double avrg_spectral_radius ( const Cell & A ) override;  // virtual from tag::Util::Metric::Core
	
	// two 'scale_sq' and two 'scale_sq_inv' methods are virtual from tag::Util::Metric
	tag::Util::MetricCore * scale_sq ( const Function & f );
	tag::Util::MetricCore * scale_sq ( const double f );
	tag::Util::MetricCore * scale_sq_inv ( const Function & f );
	tag::Util::MetricCore * scale_sq_inv ( const double f );

	// inlined 'sq_dist' defined by tag::Util::Metric::Core

	// two versions of  compute_data
	// virtual from tag::Util::Metric::Trivial, defined there, execution forbidden

	// methods codim_0, codim_1, de_activate virtual from tag::Util::MetricCore,
	// defined there, execution forbidden

	void correct_ext_prod_2d ( const Cell & P, std::vector < double > & n );
	void correct_ext_prod_3d ( const Cell & P, std::vector < double > & n );
	// virtual from tag::Util::MetricCore, defined in frontal.cpp, here execution forbidden
	
};  // end of  class tag::Util::Metric::ScaledSq


//------------------------------------------------------------------------------------------------------//


class tag::Util::Metric::ScaledSqInv : public tag::Util::Metric::Variable

// this class in useful for scaling an anisotropic metric, kept in 'base'
// scaling an isotropic metric is done directly, returns a tag::Util::Metric::***::Isotropic

{	public :

	// seven pointers to functions, more precisely, pointers to static methods
	// inherited from tag::Util::Metric::Core :
	// frontal_method_nw_***, frontal_method_q_***, remove_node_isr_p

	tag::Util::Metric base;  // anisotropic metric
	Function zoom;

	inline ScaledSqInv ( const tag::Util::Metric & b, const Function & z )
	: tag::Util::Metric::Variable(), base {b}, zoom {z}
		// we must copy frontal methods from the base to 'this'
	#ifndef MANIFEM_NO_QUOTIENT
	{ assert ( base .core );
		frontal_method_nw_oc  = base .core->frontal_method_nw_oc;
		frontal_method_nw_tow = base .core->frontal_method_nw_tow;
		frontal_method_nw_nem = base .core->frontal_method_nw_nem;
		frontal_method_q_oc   = base .core->frontal_method_q_oc;
		frontal_method_q_tow  = base .core->frontal_method_q_tow;
		frontal_method_q_nem  = base .core->frontal_method_q_nem;   }
	#else // MANIFEM_NO_QUOTIENT
	{ assert ( base .core );
		frontal_method_nw_oc  = base .core->frontal_method_nw_oc;
		frontal_method_nw_tow = base .core->frontal_method_nw_tow;
		frontal_method_nw_nem = base .core->frontal_method_nw_nem;  }
	#endif  // ifndef MANIFEM_NO_QUOTIENT

	virtual ~ScaledSqInv ( );

	using tag::Util::Metric::Core::operator=;  // operator=, copy and move, deleted

	tag::Util::MetricCore * clone ( );  // virtual from tag::Util::Metric::Core

	tag::Util::MetricCore * regist ( ) override;  //  virtual from tag::Util::Metric::Core
	// here returns base .core->regist(), often equivalent to base .core->clone()
	
	bool isotropic ( );  // virtual from tag::Util::Metric::Core
	
	// double inner_prod ( v, w )  virtual from tag::Util::Metric::Core,
	// defined by tag::Util::Metric::Variable, execution forbidden

	double inner_prod  // virtual from tag::Util::Metric::Core, here delegate to base then scale
	( const Cell & P, const std::vector < double > & v, const std::vector < double > & w );	

	double inner_prod ( const Cell & P, const Cell & Q,  // virtual from tag::Util::Metric
	  const std::vector < double > & v, const std::vector < double > & w ) override;
	// return  ( this->inner_prod ( P, v, w ) + this->inner_prod ( Q, v, w ) ) / 2.

	double inner_spectral_radius ( const Cell & A );  // virtual from tag::Util::Metric::Core

	double avrg_spectral_radius ( const Cell & A ) override;  // virtual from tag::Util::Metric::Core
	
	// two 'scale_sq' and two 'scale_sq_inv' methods are virtual from tag::Util::Metric
	tag::Util::MetricCore * scale_sq ( const Function & f );
	tag::Util::MetricCore * scale_sq ( const double f );
	tag::Util::MetricCore * scale_sq_inv ( const Function & f );
	tag::Util::MetricCore * scale_sq_inv ( const double f );

	// inlined 'sq_dist' defined by tag::Util::Metric::Core

	// two versions of  compute_data
	// virtual from tag::Util::Metric::Trivial, defined there, execution forbidden

	// methods codim_0, codim_1, de_activate virtual from tag::Util::MetricCore,
	// defined there, execution forbidden

	void correct_ext_prod_2d ( const Cell & P, std::vector < double > & n );
	void correct_ext_prod_3d ( const Cell & P, std::vector < double > & n );
	// virtual from tag::Util::MetricCore, defined in frontal.cpp, here execution forbidden
	
};  // end of  class tag::Util::Metric::ScaledSqInv

#endif  // ifndef MANIFEM_NO_FRONTAL

//------------------------------------------------------------------------------------------------------//
//------------------------------------------------------------------------------------------------------//

	
class Manifold::Euclid : public Manifold::Core

{	public :

	// tag::Util::Metric * metric  inherited from Manifold::Core

	size_t dim;
	
	tag::Util::Field::Core * coord_field { nullptr };
	Function coord_func { tag::non_existent };

	inline Euclid ( size_t d )
	:	Manifold::Core(), dim { d }
	{	assert ( d > 0 );  }

  Function build_coord_func ( const tag::lagrange &, const tag::OfDegree &, size_t d ) ;
  // virtual from Manifold::Core
	
  Function get_coord_func ( ) const;  // virtual from Manifold::Core
	
	void set_coords ( const Function co );  // virtual from Manifold::Core

	size_t topological_dim () const;  // virtual from Manifold::Core
	
	// measure of the entire manifold, here produces run-time error
	// (a Euclidian maifold has infinite measure)
	double measure () const;  // virtual from Manifold::Core

	// double sq_dist ( const Cell & A, const Cell & B )
	// virtual from Manifold::Core, defined there

	void project ( Cell::Positive::Vertex * ) const;
	// virtual from Manifold::Core, here does nothing

	// P = sA + sB,  s + t == 1     virtual from Manifold::Core
	void interpolate ( Cell::Positive::Vertex * P,
	  double s, Cell::Positive::Vertex * A, double t, Cell::Positive::Vertex * B ) const;
	void interpolate ( Cell::Positive::Vertex * P,
	  double s, Cell::Positive::Vertex * A, double t, Cell::Positive::Vertex * B,
	  const tag::Winding &, const Manifold::Action & exp                         ) const ;
	void pretty_interpolate
		( const Cell & P, double s, const Cell & A, double t, const Cell & B ) const;

	// P = sA + sB + uC,  s+t+u == 1
	void interpolate ( Cell::Positive::Vertex * P,
	  double s, Cell::Positive::Vertex * A, double t, Cell::Positive::Vertex * B,
	  double u, Cell::Positive::Vertex * C                                       ) const;

	// P = sA + sB + uC,  s+t+u == 1
	void interpolate ( Cell::Positive::Vertex * P,
	  double s, Cell::Positive::Vertex * A,
	  double t, Cell::Positive::Vertex * B,
	  const tag::Winding &, const Manifold::Action &,
	  double u, Cell::Positive::Vertex * C,
	  const tag::Winding &, const Manifold::Action & ) const;

	// P = sA + sB + uC + vD,  s + t + u + v == 1     virtual from Manifold::Core
	void interpolate ( Cell::Positive::Vertex * P,
		double s, Cell::Positive::Vertex * A, double t, Cell::Positive::Vertex * B,
		double u, Cell::Positive::Vertex * C, double v, Cell::Positive::Vertex * D  ) const;

	void interpolate ( Cell::Positive::Vertex * P,
	  double s, Cell::Positive::Vertex * A,
	  double t, Cell::Positive::Vertex * B,
	  const tag::Winding &, const Manifold::Action &,
	  double u, Cell::Positive::Vertex * C,
	  const tag::Winding &, const Manifold::Action &,
	  double v, Cell::Positive::Vertex * D,
	  const tag::Winding &, const Manifold::Action & ) const;

	void pretty_interpolate
	( const Cell & P, double s, const Cell & A, double t, const Cell & B,
	                  double u, const Cell & C, double v, const Cell & D  ) const;

	// P = sA + sB + uC + vD + wE + zF,  s + t + u + v + w + z == 1    virtual from Manifold::Core
	void interpolate ( Cell::Positive::Vertex * P,
	  double s, Cell::Positive::Vertex * A, double t, Cell::Positive::Vertex * B,
	  double u, Cell::Positive::Vertex * C, double v, Cell::Positive::Vertex * D,
	  double w, Cell::Positive::Vertex * E, double z, Cell::Positive::Vertex * F ) const;
	void interpolate ( Cell::Positive::Vertex * P,
	  double s, Cell::Positive::Vertex * A,
	  double t, Cell::Positive::Vertex * B,
	  const tag::Winding &, const Manifold::Action &,
	  double u, Cell::Positive::Vertex * C,
	  const tag::Winding &, const Manifold::Action &,
	  double v, Cell::Positive::Vertex * D,
	  const tag::Winding &, const Manifold::Action &,
	  double w, Cell::Positive::Vertex * E,
	  const tag::Winding &, const Manifold::Action &,
	  double z, Cell::Positive::Vertex * F,
	  const tag::Winding &, const Manifold::Action & ) const;
	void pretty_interpolate ( const Cell & P, double s, const Cell & A,
	  double t, const Cell & B, double u, const Cell & C, double v, const Cell & D,
	  double w, const Cell & E, double z, const Cell & F ) const;

	// P = sum c_k P_k,  sum c_k == 1     virtual from Manifold::Core
	void interpolate ( Cell::Positive::Vertex * P,
	  const std::vector < double > & coefs, const std::vector < Cell::Positive::Vertex * > & points ) const;
	void pretty_interpolate ( const Cell & P,
	  const std::vector < double > & coefs, const std::vector < Cell > & points ) const;

	// virtual from Manifold::Core, defined in fronal.cpp
	void move_vertex ( const Cell & V, std::vector < double > & delta, double & norm_2 ) const;
	
	// several versions of  frontal_method  defined by Manifold::Core

	Cell search_start_ver ( );  // virtual from  Manifold::Core
	// defined in frontal.cpp, execution forbidden

};  // end of class Manifold::Euclid

//------------------------------------------------------------------------------------------------------//


class Manifold::Implicit : public Manifold::Core

// a submanifold of a Manifold::Euclid defined by one or more equations

// abstract class, specialized in Manifold::Implicit::OneEquation, TwoEquation, ThreeEquationsOrMore

{	public :

	// tag::Util::Metric * metric  inherited from Manifold::Core
	// be warned, the surrounding space has its own metric
	// in Manifold constructor with tag::implicit, we make a copy of the metric

	Manifold surrounding_space;

	inline Implicit ( )	:	Manifold::Core(), surrounding_space ( tag::non_existent ) { }
		
	// the projection will be done by means of the Newton method
	static const short int steps_for_Newton = 10;
	
	Function build_coord_func ( const tag::lagrange &, const tag::OfDegree &, size_t d );
	//   virtual from Manifold::Core, here execution forbidden
	
	Function get_coord_func ( ) const;  // virtual from Manifold::Core

	void set_coords ( const Function co );  // virtual from Manifold::Core

	// size_t topological_dim  stays pure virtual from Manifold::Core

	// measure of the entire manifold, here produces run-time error
	double measure ( ) const;  // virtual from Manifold::Core

	// double sq_dist ( const Cell & A, const Cell & B )
	// virtual from Manifold::Core, defined there

	// void project ( Cell::Positive::Vertex * ) const  stays pure virtual from Manifold::Core

	// P = sA + sB,  s + t == 1     virtual from Manifold::Core
	void interpolate ( Cell::Positive::Vertex * P,
	  double s, Cell::Positive::Vertex * A, double t, Cell::Positive::Vertex * B ) const;
	void interpolate ( Cell::Positive::Vertex * P,
	  double s, Cell::Positive::Vertex * A, double t, Cell::Positive::Vertex * B,
	  const tag::Winding &, const Manifold::Action & exp                    ) const ;

	// P = sA + sB + uC,  s+t+u == 1
	void interpolate ( Cell::Positive::Vertex * P,
	  double s, Cell::Positive::Vertex * A, double t, Cell::Positive::Vertex * B,
	  double u, Cell::Positive::Vertex * C                                       ) const;

	// P = sA + sB + uC,  s+t+u == 1
	void interpolate ( Cell::Positive::Vertex * P,
	  double s, Cell::Positive::Vertex * A,
	  double t, Cell::Positive::Vertex * B,
	  const tag::Winding &, const Manifold::Action &,
	  double u, Cell::Positive::Vertex * C,
	  const tag::Winding &, const Manifold::Action & ) const;

	// P = sA + sB + uC + vD,  s + t + u + v == 1     virtual from Manifold::Core
	void interpolate ( Cell::Positive::Vertex * P,
	  double s, Cell::Positive::Vertex * A, double t, Cell::Positive::Vertex * B,
	  double u, Cell::Positive::Vertex * C, double v, Cell::Positive::Vertex * D  ) const;
	void interpolate ( Cell::Positive::Vertex * P,
	  double s, Cell::Positive::Vertex * A,
	  double t, Cell::Positive::Vertex * B,
	  const tag::Winding &, const Manifold::Action &,
	  double u, Cell::Positive::Vertex * C,
	  const tag::Winding &, const Manifold::Action &,
	  double v, Cell::Positive::Vertex * D,
	  const tag::Winding &, const Manifold::Action & ) const;

	// P = sA + sB + uC + vD + wE + zF,  s + t + u + v + w + z == 1     virtual from Manifold::Core
	void interpolate ( Cell::Positive::Vertex * P,
	  double s, Cell::Positive::Vertex * A, double t, Cell::Positive::Vertex * B,
	  double u, Cell::Positive::Vertex * C, double v, Cell::Positive::Vertex * D,
	  double w, Cell::Positive::Vertex * E, double z, Cell::Positive::Vertex * F ) const;
	void interpolate ( Cell::Positive::Vertex * P,
	  double s, Cell::Positive::Vertex * A,
	  double t, Cell::Positive::Vertex * B,
	  const tag::Winding &, const Manifold::Action &,
	  double u, Cell::Positive::Vertex * C,
	  const tag::Winding &, const Manifold::Action &,
	  double v, Cell::Positive::Vertex * D,
	  const tag::Winding &, const Manifold::Action &,
	  double w, Cell::Positive::Vertex * E,
	  const tag::Winding &, const Manifold::Action &,
	  double z, Cell::Positive::Vertex * F,
	  const tag::Winding &, const Manifold::Action & ) const;

	// P = sum c_k P_k,  sum c_k == 1     virtual from Manifold::Core
  void interpolate ( Cell::Positive::Vertex * P,
	  const std::vector < double > & coefs,
	  const std::vector < Cell::Positive::Vertex * > & points ) const;
	
	// virtual from Manifold::Core, defined in fronal.cpp
	void move_vertex ( const Cell & V, std::vector < double > & delta, double & norm_2 ) const;
	
	// several versions of  frontal_method  defined by Manifold::Core
	// Cell search_start_ver  stays pure virtual from  Manifold::Core
	
	class OneEquation;  class TwoEquations;  class ThreeEquationsOrMore;
	
};  // end of class Manifold::Implicit

//------------------------------------------------------------------------------------------------------//


class Manifold::Implicit::OneEquation : public Manifold::Implicit

// a submanifold of a Manifold::Euclid defined by one equation

{	public :

	// tag::Util::Metric * metric  inherited from Manifold::Core
	// be warned, the surrounding space has its own metric
	// in Manifold constructor with tag::implicit, we make a copy of the metric

	// Manifold surrounding_space  inherited from Manifold::Implicit

	Function level_function, grad_lev_func;
	
	inline OneEquation ( const Manifold & s, const Function & f );
	
	// Function build_coord_func ( const tag::lagrange &, const tag::OfDegree &, size_t d )
	//   defined by Manifold::Implicit (execution forbidden)

	// Function get_coord_func ( ) const  defined by Manifold::Implicit

	// void set_coords ( const Function co )  defined by Manifold::Implicit

	size_t topological_dim () const;  // virtual from Manifold::Core

	// double measure ( ) const  virtual from Manifold::Core
	// defined by Manifold::Implicit, execution forbidden

	// double sq_dist ( const Cell & A, const Cell & B )
	// virtual from Manifold::Core, defined there

	void project ( Cell::Positive::Vertex * ) const;
	// virtual from Manifold::Core, through Manifold::Implicit

	// void interpolate (different overloaded versions) defined by Manifold::Implicit

	// void move_vertex  virtual from Manifold::Core, defined by Manifold::Implicit
	
	// several versions of  frontal_method  defined by Manifold::Core
	
	Cell search_start_ver ( );  // virtual from  Manifold::Core, defined in frontal.cpp

};  // end of class Manifold::Implicit::OneEquation

//------------------------------------------------------------------------------------------------------//


class Manifold::Implicit::TwoEquations : public Manifold::Implicit

// a submanifold of a Manifold::Euclid defined by two equations

{	public :

	// tag::Util::Metric * metric  inherited from Manifold::Core
	// be warned, the surrounding space has its own metric
	// in Manifold constructor with tag::implicit, we make a copy of the metric

	// Manifold surrounding_space  inherited from Manifold::Implicit

	Function level_function_1, grad_lev_func_1;
	Function level_function_2, grad_lev_func_2;
	
	inline TwoEquations ( const Manifold & s, const Function & f );
	inline TwoEquations ( const Manifold & s, const Function & f1, const Function & f2 );

	// Function build_coord_func ( const tag::lagrange &, const tag::OfDegree &, size_t d )
	//   defined by Manifold::Implicit (execution forbidden)
	
	// Function get_coord_func ( ) const  defined by Manifold::Implicit

	// void set_coords ( const Function co )  defined by Manifold::Implicit

	size_t topological_dim () const;  // virtual from Manifold::Core

	// double measure ( ) const  virtual from Manifold::Core
	// defined by Manifold::Implicit, execution forbidden

	// double sq_dist ( const Cell & A, const Cell & B )
	// virtual from Manifold::Core, defined there

	void project ( Cell::Positive::Vertex * ) const;
	// virtual from Manifold::Core, through Manifold::Implicit

	// void interpolate (different overloaded versions) defined by Manifold::Implicit
	
	// void move_vertex  virtual from Manifold::Core, defined by Manifold::Implicit

	// several versions of  frontal_method  defined by Manifold::Core
	
	Cell search_start_ver ( );  // virtual from  Manifold::Core, defined in frontal.cpp

};  // end of class Manifold::Implicit::TwoEquations

//------------------------------------------------------------------------------------------------------//


class Manifold::Implicit::ThreeEquationsOrMore : public Manifold::Implicit

// a submanifold of a Manifold::Euclid defined by three or more equations

{	public :

	// tag::Util::Metric * metric  inherited from Manifold::Core
	// be warned, the surrounding space has its own metric
	// in Manifold constructor with tag::implicit, we make a copy of the metric

	// Manifold surrounding_space  inherited from Manifold::Implicit
	
	std::vector < Function > level_function, grad_lev_func;
	
	inline ThreeEquationsOrMore ( const Manifold & s, const Function & f );
	inline ThreeEquationsOrMore ( const Manifold & s, const Function & f1, const Function & f2 );
	inline ThreeEquationsOrMore
	( const Manifold & s, const Function & f1, const Function & f2, const Function & f3 );

	// Function build_coord_func ( const tag::lagrange &, const tag::OfDegree &, size_t d )
	//   defined by Manifold::Implicit (execution forbidden)
	
	// Function get_coord_func ( ) const  defined by Manifold::Implicit

	// void set_coords ( const Function co )  defined by Manifold::Implicit

	size_t topological_dim () const;  // virtual from Manifold::Core

	// double measure ( ) const  virtual from Manifold::Core
	// defined by Manifold::Implicit, execution forbidden

	// double sq_dist ( const Cell & A, const Cell & B )
	// virtual from Manifold::Core, defined there

	void project ( Cell::Positive::Vertex * ) const;
	// virtual from Manifold::Core, through Manifold::Implicit

	// void interpolate (different overloaded versions) defined by Manifold::Implicit
	
	// void move_vertex  virtual from Manifold::Core, defined by Manifold::Implicit

	// several versions of  frontal_method  defined by Manifold::Core
	
	Cell search_start_ver ( );  // virtual from  Manifold::Core
	// defined in frontal.cpp, execution forbidden

};  // end of class Manifold::Implicit::ThreeEquationsOrMore

//------------------------------------------------------------------------------------------------------//


inline Manifold::Implicit::OneEquation::OneEquation
( const Manifold & m, const Function & f )

: level_function ( f ),
	grad_lev_func ( 0. )  // temporarily zero gradient

{	this->surrounding_space = m;
	this->metric = m .core->metric;
	std::shared_ptr < Manifold::Euclid > m_euclid = tag::Util::assert_shared_cast
		< Manifold::Core, Manifold::Euclid > ( m .core );
	Function coord = m_euclid->coord_func;
	size_t n = coord .number_of ( tag::components );
	Function::Aggregate * grad = new Function::Aggregate ( tag::reserve_size, n );
	for ( size_t i = 0; i < n; i++ )  // grad->components [i] = f .deriv ( coord [i] );
		grad->components .emplace_back ( f .deriv ( coord [i] ) );
	this->grad_lev_func = Function ( tag::whose_core_is, grad );                        }


inline Manifold::Implicit::TwoEquations::TwoEquations
( const Manifold & m, const Function & f )

: level_function_1 ( 0. ),  // temporarily zero function
	grad_lev_func_1 ( 0. ),  // temporarily zero gradient
	level_function_2 ( f ),
	grad_lev_func_2 ( 0. )  // temporarily zero gradient

{	std::shared_ptr < Manifold::Implicit::OneEquation > m_one_eq = tag::Util::assert_shared_cast
		< Manifold::Core, Manifold::Implicit::OneEquation  > ( m .core );
	this->surrounding_space = m_one_eq->surrounding_space;
	this->metric = m .core->metric;
	this->level_function_1 = m_one_eq->level_function;
	this->grad_lev_func_1 = m_one_eq->grad_lev_func;
	Function coord = this->surrounding_space .coordinates();
	size_t n = coord .number_of ( tag::components );
	Function::Aggregate * grad = new Function::Aggregate ( tag::reserve_size, n );
	for ( size_t i = 0; i < n; i++ ) // grad->components[i] = f .deriv ( coord [i] );
		grad->components .emplace_back ( f .deriv ( coord [i] ) );
	this->grad_lev_func_2 = Function ( tag::whose_core_is, grad );                    }


inline Manifold::Implicit::TwoEquations::TwoEquations
( const Manifold & m, const Function & f1, const Function & f2 )

: level_function_1 ( f1 ), 
	grad_lev_func_1 ( 0. ),  // temporarily zero gradient
	level_function_2 ( f2 ),
	grad_lev_func_2 ( 0. )  // temporarily zero gradient

{	this->surrounding_space = m;
	this->metric = m .core->metric;
	std::shared_ptr < Manifold::Euclid > m_euclid = tag::Util::assert_shared_cast
		< Manifold::Core, Manifold::Euclid > ( m .core );
	Function coord = m_euclid->coord_func;
	size_t n = coord .number_of ( tag::components );
	Function::Aggregate * grad = new Function::Aggregate ( tag::reserve_size, n );
	for ( size_t i = 0; i < n; i++ ) // grad->components[i] = f1.deriv(coord[i]);
		grad->components .emplace_back ( f1 .deriv ( coord [i] ) );
	this->grad_lev_func_1 = Function ( tag::whose_core_is, grad );
	grad = new Function::Aggregate ( tag::reserve_size, n );
	for ( size_t i = 0; i < n; i++ ) // grad->components [i] = f2 .deriv ( coord [i] );
		grad->components .emplace_back ( f2 .deriv ( coord [i] ) );
	this->grad_lev_func_2 = Function ( tag::whose_core_is, grad );                      }


inline Manifold::Implicit::ThreeEquationsOrMore::ThreeEquationsOrMore
( const Manifold & m, const Function & f )

// level_function and grad_lev_function initialized by default as empty vectors

{	std::shared_ptr < Manifold::Implicit::TwoEquations > m_two_eq =
		std::dynamic_pointer_cast < Manifold::Implicit::TwoEquations > ( m .core );
	if ( m_two_eq )
	{	this->surrounding_space = m_two_eq->surrounding_space;
		this->level_function .push_back ( m_two_eq->level_function_1 );
		this->level_function .push_back ( m_two_eq->level_function_2 );
		this->grad_lev_func .push_back ( m_two_eq->grad_lev_func_1 );
		this->grad_lev_func .push_back ( m_two_eq->grad_lev_func_2 );    }
	else
	{	std::shared_ptr < Manifold::Implicit::ThreeEquationsOrMore > m_three_eq =
			tag::Util::assert_shared_cast
			< Manifold::Core, Manifold::Implicit::ThreeEquationsOrMore > ( m .core );
		this->surrounding_space = m_three_eq->surrounding_space;
		this->level_function = m_three_eq->level_function;
		this->grad_lev_func = m_three_eq->grad_lev_func;                                }
	this->metric = m .core->metric;
	this->level_function .push_back ( f );
	Function coord = this->surrounding_space .coordinates();
	size_t n = coord .number_of ( tag::components );
	Function::Aggregate * grad = new Function::Aggregate ( tag::reserve_size, n );
	for ( size_t i = 0; i < n; i++ ) // grad->components[i] = f .deriv ( coord [i] );
		grad->components .emplace_back ( f .deriv ( coord [i] ) );
	// this->grad_lev_func .emplace_back ( std::piecewise_construct,
	//                                     std::forward_as_tuple ( tag::whose_core_is ),
	//                                     std::forward_as_tuple ( grad )               );
	this->grad_lev_func .push_back ( Function ( tag::whose_core_is, grad ) );           }


inline Manifold::Implicit::ThreeEquationsOrMore::ThreeEquationsOrMore
( const Manifold & m, const Function & f1, const Function & f2 )

// level_function and grad_lev_function initialized by default as empty vectors

{	std::shared_ptr < Manifold::Implicit::OneEquation > m_one_eq =
		std::dynamic_pointer_cast < Manifold::Implicit::OneEquation > ( m .core );
	std::shared_ptr < Manifold::Implicit::TwoEquations > m_two_eq =
		std::dynamic_pointer_cast < Manifold::Implicit::TwoEquations > ( m .core );
	if ( m_one_eq )
	{	this->surrounding_space = m_one_eq->surrounding_space;
		this->level_function .push_back ( m_one_eq->level_function );
		this->grad_lev_func .push_back ( m_one_eq->grad_lev_func );   }
	else if ( m_two_eq )
	{	this->surrounding_space = m_two_eq->surrounding_space;
		this->level_function .push_back ( m_two_eq->level_function_1 );
		this->level_function .push_back ( m_two_eq->level_function_2 );
		this->grad_lev_func .push_back ( m_two_eq->grad_lev_func_1 );
		this->grad_lev_func .push_back ( m_two_eq->grad_lev_func_2 );    }
	else
	{	std::shared_ptr < Manifold::Implicit::ThreeEquationsOrMore > m_three_eq =
			tag::Util::assert_shared_cast
			< Manifold::Core, Manifold::Implicit::ThreeEquationsOrMore > ( m .core );
		this->surrounding_space = m_three_eq->surrounding_space;
		this->level_function = m_three_eq->level_function;
		this->grad_lev_func = m_three_eq->grad_lev_func;                                }
	this->metric = m .core->metric;
	this->level_function .push_back ( f1 );
	this->level_function .push_back ( f2 );
	Function coord = this->surrounding_space .coordinates();
	size_t n = coord .number_of ( tag::components );
	Function::Aggregate * grad = new Function::Aggregate ( tag::reserve_size, n );
	for ( size_t i = 0; i < n; i++ ) // grad->components[i] = f1 .deriv ( coord [i] );
		grad->components .emplace_back ( f1 .deriv ( coord [i] ) );
	this->grad_lev_func .push_back ( Function ( tag::whose_core_is, grad ) );
	grad = new Function::Aggregate ( tag::reserve_size, n );
	for ( size_t i = 0; i < n; i++ ) // grad->components[i] = f2 .deriv ( coord [i] );
		grad->components .emplace_back ( f2 .deriv ( coord [i] ) );
	this->grad_lev_func .push_back ( Function ( tag::whose_core_is, grad ) );           }


inline Manifold::Implicit::ThreeEquationsOrMore::ThreeEquationsOrMore
( const Manifold & m, const Function & f1, const Function & f2, const Function & f3 )

// level_function and grad_lev_function initialized by default as empty vectors

{	std::shared_ptr < Manifold::Euclid > m_euclid =
		std::dynamic_pointer_cast < Manifold::Euclid > ( m .core );
	std::shared_ptr < Manifold::Implicit::OneEquation > m_one_eq =
		std::dynamic_pointer_cast < Manifold::Implicit::OneEquation > ( m .core );
	std::shared_ptr < Manifold::Implicit::TwoEquations > m_two_eq =
		std::dynamic_pointer_cast < Manifold::Implicit::TwoEquations > ( m .core );
	if ( m_euclid ) this->surrounding_space = m;
	else if ( m_one_eq )
	{	this->surrounding_space = m_one_eq->surrounding_space;
		this->level_function .push_back ( m_one_eq->level_function );
		this->grad_lev_func .push_back ( m_one_eq->grad_lev_func );   }
	else if ( m_two_eq )
	{	this->surrounding_space = m_two_eq->surrounding_space;
		this->level_function .push_back ( m_two_eq->level_function_1 );
		this->level_function .push_back ( m_two_eq->level_function_2 );
		this->grad_lev_func .push_back ( m_two_eq->grad_lev_func_1 );
		this->grad_lev_func .push_back ( m_two_eq->grad_lev_func_2 );    }
	else
	{	std::shared_ptr < Manifold::Implicit::ThreeEquationsOrMore > m_three_eq =
			tag::Util::assert_shared_cast
			< Manifold::Core, Manifold::Implicit::ThreeEquationsOrMore > ( m .core );
		this->surrounding_space = m_three_eq->surrounding_space;
		this->level_function = m_three_eq->level_function;
		this->grad_lev_func = m_three_eq->grad_lev_func;                                }
	this->metric = m .core->metric;
	this->level_function .push_back ( f1 );
	this->level_function .push_back ( f2 );
	this->level_function .push_back ( f3 );
	Function coord = this->surrounding_space .coordinates();
	size_t n = coord .number_of ( tag::components );
	Function::Aggregate * grad = new Function::Aggregate ( tag::reserve_size, n );
	for ( size_t i = 0; i < n; i++ ) // grad->components[i] = f1 .deriv ( coord [i] );
		grad->components .emplace_back ( f1 .deriv ( coord [i] ) );
	this->grad_lev_func .push_back ( Function ( tag::whose_core_is, grad ) );
	grad = new Function::Aggregate ( tag::reserve_size, n );
	for ( size_t i = 0; i < n; i++ ) // grad->components[i] = f2 .deriv ( coord [i] );
		grad->components .emplace_back ( f2 .deriv ( coord [i] ) );
	this->grad_lev_func .push_back ( Function ( tag::whose_core_is, grad ) );
	grad = new Function::Aggregate ( tag::reserve_size, n );
	for ( size_t i = 0; i < n; i++ ) // grad->components[i] = f3 .deriv ( coord [i] );
		grad->components .emplace_back ( f3 .deriv ( coord [i] ) );
	this->grad_lev_func .push_back ( Function ( tag::whose_core_is, grad ) );           }

//------------------------------------------------------------------------------------------------------//


class Manifold::Parametric : public Manifold::Core

// a submanifold of a Manifold::Euclid defined by one or more explicit equations

{	public :

	// tag::Util::Metric * metric  inherited from Manifold::Core
	// be warned, the surrounding space has its own metric
	// in Manifold constructor with tag::parametric, we make a copy of the metric

	Manifold surrounding_space;

	Function coord_func { tag::non_existent };

	std::map < Function, Function, bool (*) ( const Function &, const Function & ) > equations;
	//  decltype(Function::less_for_map)*  equals  bool (*) ( const Function &, const Function & )

	inline Parametric ( )
	: Manifold::Core(),
		surrounding_space ( tag::non_existent ),
		equations ( & Function::less_for_map )
	{ }

	inline Parametric ( const Manifold &, const Function::Equality & );

	inline Parametric ( const Manifold &, const Function::Equality &, const Function::Equality & );

	inline Parametric ( const Manifold &, const Function::Equality &,
                      const Function::Equality &, const Function::Equality & );

	Function build_coord_func ( const tag::lagrange &, const tag::OfDegree &, size_t d );
	//   virtual from Manifold::Core, here execution forbidden
	
	Function get_coord_func ( ) const;  // virtual from Manifold::Core

	void set_coords ( const Function co );  // virtual from Manifold::Core

	size_t topological_dim () const;  // virtual from Manifold::Core

	// measure of the entire manifold, here execution forbidden
	double measure ( ) const;  // virtual from Manifold::Core

	// double sq_dist ( const Cell & A, const Cell & B )
	// virtual from Manifold::Core, defined there

	void project ( Cell::Positive::Vertex * ) const;
	// virtual from Manifold::Core

	// P = sA + sB,  s + t == 1     virtual from Manifold::Core
	void interpolate ( Cell::Positive::Vertex * P,
	  double s, Cell::Positive::Vertex * A, double t, Cell::Positive::Vertex * B ) const;
	void interpolate ( Cell::Positive::Vertex * P,
	  double s, Cell::Positive::Vertex * A, double t, Cell::Positive::Vertex * B,
	  const tag::Winding &, const Manifold::Action & exp                    ) const ;

	// P = sA + sB + uC,  s+t+u == 1
	void interpolate ( Cell::Positive::Vertex * P,
	  double s, Cell::Positive::Vertex * A, double t, Cell::Positive::Vertex * B,
	  double u, Cell::Positive::Vertex * C                                       ) const;

	// P = sA + sB + uC,  s+t+u == 1
	void interpolate ( Cell::Positive::Vertex * P,
	  double s, Cell::Positive::Vertex * A,
	  double t, Cell::Positive::Vertex * B,
	  const tag::Winding &, const Manifold::Action &,
	  double u, Cell::Positive::Vertex * C,
	  const tag::Winding &, const Manifold::Action & ) const;

	// P = sA + sB + uC + vD,  s + t + u + v == 1     virtual from Manifold::Core
	void interpolate ( Cell::Positive::Vertex * P,
	  double s, Cell::Positive::Vertex * A, double t, Cell::Positive::Vertex * B,
	  double u, Cell::Positive::Vertex * C, double v, Cell::Positive::Vertex * D  ) const;
	void interpolate ( Cell::Positive::Vertex * P,
	  double s, Cell::Positive::Vertex * A,
	  double t, Cell::Positive::Vertex * B,
	  const tag::Winding &, const Manifold::Action &,
	  double u, Cell::Positive::Vertex * C,
	  const tag::Winding &, const Manifold::Action &,
	  double v, Cell::Positive::Vertex * D,
	  const tag::Winding &, const Manifold::Action & ) const;

	// P = sA + sB + uC + vD + wE + zF,  s + t + u + v + w + z == 1    virtual from Manifold::Core
	void interpolate ( Cell::Positive::Vertex * P,
	  double s, Cell::Positive::Vertex * A, double t, Cell::Positive::Vertex * B,
	  double u, Cell::Positive::Vertex * C, double v, Cell::Positive::Vertex * D,
	  double w, Cell::Positive::Vertex * E, double z, Cell::Positive::Vertex * F ) const;
	void interpolate ( Cell::Positive::Vertex * P,
	  double s, Cell::Positive::Vertex * A,
	  double t, Cell::Positive::Vertex * B,
	  const tag::Winding &, const Manifold::Action &,
	  double u, Cell::Positive::Vertex * C,
	  const tag::Winding &, const Manifold::Action &,
	  double v, Cell::Positive::Vertex * D,
	  const tag::Winding &, const Manifold::Action &,
	  double w, Cell::Positive::Vertex * E,
	  const tag::Winding &, const Manifold::Action &,
	  double z, Cell::Positive::Vertex * F,
	  const tag::Winding &, const Manifold::Action & ) const;

	// P = sum c_k P_k,  sum c_k == 1     virtual from Manifold::Core
	virtual void interpolate ( Cell::Positive::Vertex * P,
	  const std::vector < double > & coefs, const std::vector < Cell::Positive::Vertex * > & points ) const;
	
	// virtual from Manifold::Core, defined in fronal.cpp
	void move_vertex ( const Cell & V, std::vector < double > & delta, double & norm_2 ) const;

	Cell search_start_ver ( );  // virtual from  Manifold::Core
	// defined in frontal.cpp, execution forbidden

};  // end of class Manifold::Parametric

//------------------------------------------------------------------------------------------------------//


inline Manifold::Parametric::Parametric ( const Manifold & m, const Function::Equality & f_eq )

:	Parametric()

{	this->surrounding_space = m;
	tag::Util::assert_shared_cast < Manifold::Core, Manifold::Euclid > ( m .core );
	assert ( this->equations .find ( f_eq .lhs ) == this->equations .end() );
	this->equations .insert ( std::pair ( f_eq .lhs, f_eq .rhs ) );
	// this->equations [ f_eq .lhs ] = f_eq_1 .rhs;
	this->coord_func = f_eq .lhs;                                               }
	

inline Manifold::Parametric::Parametric
( const Manifold & m, const Function::Equality & f_eq_1, const Function::Equality & f_eq_2 )

:	Parametric()

{	this->surrounding_space = m;
	tag::Util::assert_shared_cast < Manifold::Core, Manifold::Euclid > ( m .core );
	assert ( this->equations .find ( f_eq_1 .lhs ) == this->equations .end() );
	this->equations .insert ( std::pair ( f_eq_1 .lhs, f_eq_1 .rhs ) );
	assert ( this->equations.find ( f_eq_2 .lhs ) == this->equations .end() );
	this->equations .insert ( std::pair ( f_eq_2 .lhs, f_eq_2 .rhs ) );
	// this->equations [ f_eq_1 .lhs ] = f_eq_1 .rhs;
	// this->equations [ f_eq_2 .lhs ] = f_eq_2 .rhs;
	this->coord_func = f_eq_1 .lhs && f_eq_2 .lhs;                               }
	

inline Manifold::Parametric::Parametric
( const Manifold & m, const Function::Equality & f_eq_1,
  const Function::Equality & f_eq_2, const Function::Equality & f_eq_3 )

:	Parametric()

{	this->surrounding_space = m;
	Function surrounding_coords = m .coordinates();
	tag::Util::assert_shared_cast < Manifold::Core, Manifold::Euclid > ( m .core );
	assert ( this->equations .find ( f_eq_1 .lhs ) == this->equations .end() );
	this->equations .insert ( std::pair ( f_eq_1 .lhs, f_eq_1 .rhs ) );
	assert ( this->equations .find ( f_eq_2 .lhs ) == this->equations .end() );
	this->equations .insert ( std::pair ( f_eq_2 .lhs, f_eq_2 .rhs ) );
	assert ( this->equations .find ( f_eq_3 .lhs ) == this->equations .end() );
	this->equations .insert ( std::pair ( f_eq_3 .lhs, f_eq_3 .rhs ) );
	// this->equations [ f_eq_1 .lhs ] = f_eq_1 .rhs;
	// this->equations [ f_eq_2 .lhs ] = f_eq_2 .rhs;
	// this->equations [ f_eq_3 .lhs ] = f_eq_3 .rhs;
	this->coord_func = f_eq_1 .lhs && f_eq_2 .lhs && f_eq_3 .lhs;                }

//------------------------------------------------------------------------------------------------------//
//------------------------------------------------------------------------------------------------------//


#ifndef MANIFEM_NO_QUOTIENT

class Manifold::Quotient : public Manifold::Core

// a Euclidian manifold divided by a group of actions

{	public :

	// tag::Util::Metric * metric  inherited from Manifold::Core
	// be warned, the base space has its own metric
	// in Manifold constructor with tag::implicit, we make a copy of the metric [ implicit ?! ]

	Manifold base_space;  // wrap around a Manifold::Euclid
	
	Function coord_func { tag::non_existent };

	std::vector < Function::ActionGenerator > actions;  // set of generators for a discrete group
	
	std::vector < tag::Util::Field::ShortInt > winding_nbs;  // a jump (exponent) per action

	inline Quotient ( Manifold b, const Manifold::Action & g1 );

	inline Quotient ( Manifold b, const Manifold::Action & g1, const Manifold::Action & g2 );

	inline Quotient ( Manifold b,
	  const Manifold::Action & g1, const Manifold::Action & g2, const Manifold::Action & g3 );

	Function build_coord_func ( const tag::lagrange &, const tag::OfDegree &, size_t d );
	//   virtual from Manifold::Core, here execution forbidden
	
	Function get_coord_func ( ) const;  // virtual from Manifold::Core

	void set_coords ( const Function co );  // virtual from Manifold::Core

	size_t topological_dim () const;  // virtual from Manifold::Core

	// measure of the entire manifold
	double measure ( ) const;  // virtual from Manifold::Core
	
	double sq_dist ( const Cell & A, const Cell & B ) const override;
	// virtual from Manifold::Core, here execution forbidden

	void project ( Cell::Positive::Vertex * ) const;  // virtual from Manifold::Core

	// P = sA + sB,  s + t == 1     virtual from Manifold::Core
	void interpolate ( Cell::Positive::Vertex * P,
		double s, Cell::Positive::Vertex * A, double t, Cell::Positive::Vertex * B ) const;
	void interpolate ( Cell::Positive::Vertex * P,
		double s, Cell::Positive::Vertex * A, double t, Cell::Positive::Vertex * B,
		const tag::Winding &, const Manifold::Action & exp                         ) const ;

	// P = sA + sB + uC,  s+t+u == 1
	void interpolate ( Cell::Positive::Vertex * P,
		double s, Cell::Positive::Vertex * A, double t, Cell::Positive::Vertex * B,
		double u, Cell::Positive::Vertex * C                                       ) const;

	// P = sA + sB + uC,  s+t+u == 1
	void interpolate ( Cell::Positive::Vertex * P,
		double s, Cell::Positive::Vertex * A,
		double t, Cell::Positive::Vertex * B,
		const tag::Winding &, const Manifold::Action &,
		double u, Cell::Positive::Vertex * C,
		const tag::Winding &, const Manifold::Action & ) const;

	// P = sA + sB + uC + vD,  s + t + u + v == 1     virtual from Manifold::Core
	void interpolate ( Cell::Positive::Vertex * P,
		double s, Cell::Positive::Vertex * A, double t, Cell::Positive::Vertex * B,
		double u, Cell::Positive::Vertex * C, double v, Cell::Positive::Vertex * D ) const;
	void interpolate ( Cell::Positive::Vertex * P,
		double s, Cell::Positive::Vertex * A,
		double t, Cell::Positive::Vertex * B,
		const tag::Winding &, const Manifold::Action &,
		double u, Cell::Positive::Vertex * C,
		const tag::Winding &, const Manifold::Action &,
		double v, Cell::Positive::Vertex * D,
		const tag::Winding &, const Manifold::Action & ) const;

	// P = sA + sB + uC + vD + wE + zF,  s + t + u + v + w + z == 1    virtual from Manifold::Core
	void interpolate ( Cell::Positive::Vertex * P,
		double s, Cell::Positive::Vertex * A, double t, Cell::Positive::Vertex * B,
		double u, Cell::Positive::Vertex * C, double v, Cell::Positive::Vertex * D,
		double w, Cell::Positive::Vertex * E, double z, Cell::Positive::Vertex * F ) const;
	void interpolate ( Cell::Positive::Vertex * P,
		double s, Cell::Positive::Vertex * A,
		double t, Cell::Positive::Vertex * B,
		const tag::Winding &, const Manifold::Action &,
		double u, Cell::Positive::Vertex * C,
		const tag::Winding &, const Manifold::Action &,
		double v, Cell::Positive::Vertex * D,
		const tag::Winding &, const Manifold::Action &,
		double w, Cell::Positive::Vertex * E,
		const tag::Winding &, const Manifold::Action &,
		double z, Cell::Positive::Vertex * F,
		const tag::Winding &, const Manifold::Action & ) const;

	// P = sum c_k P_k,  sum c_k == 1     virtual from Manifold::Core
	virtual void interpolate ( Cell::Positive::Vertex * P,
		const std::vector < double > & coefs,
		const std::vector < Cell::Positive::Vertex * > & points ) const;
	
	#ifndef MANIFEM_NO_FRONTAL

	// virtual from Manifold::Core, defined in fronal.cpp
	void move_vertex ( const Cell & V, std::vector < double > & delta, double & norm_2 ) const;

	void frontal_method_1d  // virtual from Manifold::Core
	// defined in frontal.cpp, overrides definition by Manifold::Core
	( Mesh & msh, const tag::StartWithInconsistentMesh &,
	  const tag::StartAt &, const Cell & start,
	  const tag::Towards &, std::vector < double > tangent,
	  const tag::StopAt &,  const Cell & stop            ) override;
	
	void frontal_method_1d  // virtual from Manifold::Core
	// defined in frontal.cpp, overrides definition by Manifold::Core
	( Mesh & msh, const tag::StartWithInconsistentMesh &,
	  const tag::StartAt &, const Cell & start,
	  const tag::StopAt &,  const Cell & stop,
	  const tag::Orientation &, const tag::OrientationChoice & oc ) override;
	
	void frontal_method_1d  // virtual from Manifold::Core
	// defined in frontal.cpp, overrides definition by Manifold::Core
	( Mesh & msh, const tag::StartWithInconsistentMesh &,
	  const tag::StartAt &, const Cell & start,
	  const tag::Orientation &, const tag::OrientationChoice & oc ) override;
	
	void frontal_method_1d  // virtual from Manifold::Core
	// defined in frontal.cpp, overrides definition by Manifold::Core
	( Mesh & msh, const tag::StartWithInconsistentMesh &,
	  const tag::StartAt &, const Cell & start,
	  const tag::StopAt &,  const Cell & stop,
	  const tag::Winding &, const Manifold::Action & g   ) override;
	
	void frontal_method  // virtual from Manifold::Core
	// defined in frontal.cpp, overrides definition by Manifold::Core
	( Mesh & msh, const tag::StartWithNonExistentMesh &,
	  const tag::StartAt &, const Cell & start,
	  const tag::Orientation &, const tag::OrientationChoice & oc ) override;
	
	void frontal_method  // virtual from Manifold::Core
	// defined in frontal.cpp, overrides definition by Manifold::Core
	( Mesh & msh, const tag::Boundary &, const Mesh & interface,
	  const tag::StartAt &, const Cell & start,
	  const tag::Towards &, const std::vector < double > & tangent ) override;
	
	void frontal_method  // virtual from Manifold::Core
	// defined in frontal.cpp, overrides definition by Manifold::Core
	( Mesh & msh, const tag::Boundary &, const Mesh & interface,
	  const tag::StartAt &, const Cell & seg,
	  const tag::Orientation &, const tag::OrientationChoice & oc ) override;

	Cell search_start_ver ( );  // virtual from  Manifold::Core, defined in frontal.cpp

	#endif  // ifndef MANIFEM_NO_FRONTAL

};  // end of class Manifold::Quotient

//------------------------------------------------------------------------------------------------------//


inline Manifold::Quotient::Quotient ( Manifold b, const Manifold::Action & a1 )

// a tag::Util::Field::ShortInt is created and kept in this->winding_nbs
// this increments Cell::Positive::short_int_heap_size [1]
// thus, future segments will have space reserved for a winding number
	
: Manifold::Core(), base_space ( b ), actions { }

{	assert ( a1.index_map.size() == 1 );
	std::map < Function::ActionGenerator, short int > ::const_iterator
		it = a1 .index_map .begin();
	assert ( it->second == 1 );
	const Function::ActionGenerator & g1 = it->first;
	this->actions = { g1 };

	assert ( g1 .coords .core == b .coordinates() .core );
	assert ( this->coord_func .core == nullptr );
	this->winding_nbs .emplace_back ( tag::lives_on_positive_cells, tag::of_dim, 1 );
	assert ( this->coord_func .core == nullptr );

	if ( b .coordinates() .number_of ( tag::components ) == 1 )
	{	std::vector < double > v1 =
			Function::Scalar::MultiValued::JumpIsSum::analyse_linear_expression
			( g1 .transf, g1 .coords );
		assert ( v1 .size() == 1 );
		this->coord_func .core = new Function::Scalar::MultiValued::JumpIsSum
			( tag::associated_with, b .coordinates(), { g1 }, v1 );             }
	else	
	{	assert ( b.coordinates().number_of ( tag::components ) > 1 );
		std::vector < double > v1 =
			Function::Vector::MultiValued::JumpIsSum::analyse_linear_expression
			( g1.transf, g1.coords );
		if ( v1.size() > 0 )
			this->coord_func.core = new Function::Vector::MultiValued::JumpIsSum
				( tag::associated_with, b.coordinates(), { g1 }, { v1 } );
		else
		{	std::pair < std::vector < std::vector < double > >, std::vector < double > >
			p = Function::Vector::MultiValued::JumpIsLinear::analyse_linear_expression
			    ( g1.transf, g1.coords );
			this->coord_func.core = new Function::Vector::MultiValued::JumpIsLinear
				( tag::associated_with, b.coordinates(), { g1 }, { p.first }, { p.second } );  }  }
	assert ( this->coord_func.core );
	this->coord_func.core->nb_of_wrappers = 1;                                                    }


//------------------------------------------------------------------------------------------------------//


inline Manifold::Quotient::Quotient
( Manifold b, const Manifold::Action & a1, const Manifold::Action & a2 )

// two tag::Util::Field::ShortInt are created and kept in this->winding_nbs
// this increments by two Cell::Positive::short_int_heap_size [1]
// thus, future segments will have space reserved for two winding numbers
	
: Manifold::Core(), base_space ( b ), actions { }

{	assert ( a1 .index_map .size() == 1 );
	std::map < Function::ActionGenerator, short int > ::const_iterator
		it = a1 .index_map .begin();
	assert ( it->second == 1 );
	const Function::ActionGenerator & g1 = it->first;
	assert ( a2 .index_map .size() == 1 );
	it = a2 .index_map .begin();
	assert ( it->second == 1 );
	const Function::ActionGenerator & g2 = it->first;
	this->actions = { g1, g2 };

	assert ( g1 .coords .core == b .coordinates() .core );
	assert ( g2 .coords .core == b .coordinates() .core );
	this->winding_nbs .reserve ( 2 );
	this->winding_nbs .emplace_back ( tag::lives_on_positive_cells, tag::of_dim, 1 );
	this->winding_nbs .emplace_back ( tag::lives_on_positive_cells, tag::of_dim, 1 );
	assert ( b .coordinates() .number_of ( tag::components ) >= 2 );

	assert ( this->coord_func.core == nullptr );
	std::vector < double > v1 =
		Function::Vector::MultiValued::JumpIsSum::analyse_linear_expression
		( g1 .transf, g1 .coords );
	std::vector < double > v2 =
		Function::Vector::MultiValued::JumpIsSum::analyse_linear_expression
		( g2 .transf, g2 .coords );
	if ( ( v1.size() > 0 ) and ( v2.size() > 0 ) )
		this->coord_func .core = new Function::Vector::MultiValued::JumpIsSum
			( tag::associated_with, b .coordinates(), { g1, g2 }, { v1, v2 } );
	else
	{	std::pair < std::vector < std::vector < double > >, std::vector < double > >
		p1 = Function::Vector::MultiValued::JumpIsLinear::analyse_linear_expression
		     ( g1 .transf, g1 .coords ),
		p2 = Function::Vector::MultiValued::JumpIsLinear::analyse_linear_expression
		     ( g2 .transf, g2 .coords );
		this->coord_func .core = new Function::Vector::MultiValued::JumpIsLinear
			( tag::associated_with, b.coordinates(), { g1, g2 },
			  { p1 .first, p2 .first }, { p1 .second, p2 .second } );                  }
	assert ( this->coord_func .core );
	this->coord_func .core->nb_of_wrappers = 1;                                         }


//------------------------------------------------------------------------------------------------------//


inline Manifold::Quotient::Quotient ( Manifold b,
  const Manifold::Action & a1, const Manifold::Action & a2, const Manifold::Action & a3 )

// three tag::Util::Field::ShortInt are created and kept in this->winding_nbs
// this increments by three Cell::Positive::short_int_heap_size [1]
// thus, future segments will have space reserved for three winding numbers
	
: Manifold::Core(), base_space ( b ), actions { }

{	assert ( a1 .index_map .size() == 1 );
	std::map < Function::ActionGenerator, short int > ::const_iterator
		it = a1 .index_map .begin();
	assert ( it->second == 1 );
	const Function::ActionGenerator & g1 = it->first;
	assert ( a2 .index_map .size() == 1 );
	it = a2 .index_map .begin();
	assert ( it->second == 1 );
	const Function::ActionGenerator & g2 = it->first;
	assert ( a3 .index_map .size() == 1 );
	it = a3 .index_map .begin();
	assert ( it->second == 1 );
	const Function::ActionGenerator & g3 = it->first;
	this->actions = { g1, g2, g3 };

	assert ( g1 .coords .core == b .coordinates() .core );
	assert ( g2 .coords .core == b .coordinates() .core );
	assert ( g3 .coords .core == b .coordinates() .core );
	this->winding_nbs .reserve ( 3 );
	this->winding_nbs .emplace_back ( tag::lives_on_positive_cells, tag::of_dim, 1 );
	this->winding_nbs .emplace_back ( tag::lives_on_positive_cells, tag::of_dim, 1 );
	this->winding_nbs .emplace_back ( tag::lives_on_positive_cells, tag::of_dim, 1 );
	std::vector < double > v1 =
		Function::Vector::MultiValued::JumpIsSum::analyse_linear_expression
		( g1 .transf, g1 .coords );
	assert ( v1 .size() > 0 );
	std::vector < double > v2 =
		Function::Vector::MultiValued::JumpIsSum::analyse_linear_expression
		( g2 .transf, g2 .coords );
	assert ( v2 .size() > 0 );
	std::vector < double > v3 =
		Function::Vector::MultiValued::JumpIsSum::analyse_linear_expression
		( g3 .transf, g3 .coords );
	assert ( v3 .size() > 0 );
	assert ( b .coordinates() .number_of ( tag::components ) >= 3 );
	assert ( this->coord_func .core == nullptr );
	this->coord_func .core = new Function::Vector::MultiValued::JumpIsSum
		( tag::associated_with, b .coordinates(), { g1, g2, g3 }, { v1, v2, v3 } );
	this->coord_func .core->nb_of_wrappers = 1;                                        }

#endif  // ifndef MANIFEM_NO_QUOTIENT

//------------------------------------------------------------------------------------------------------//
//------------------------------------------------------------------------------------------------------//


}  // end of  namespace maniFEM

#endif  // ifndef MANIFEM_MANIFOLD_XX_H

