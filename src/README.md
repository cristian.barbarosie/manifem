# maniFEM
[ManiFEM](https://maniFEM.rd.ciencias.ulisboa.pt) is a C++ library
for solving partial differential equations through the finite element method.

Copyright 2019 - 2025 Cristian Barbarosie cristian.barbarosie@gmail.com

ManiFEM is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

ManiFEM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

Full text of the GNU Lesser General Public License is available 
in files [COPYING](https://codeberg.org/cristian.barbarosie/maniFEM/src/branch/main/src/COPYING) and
[COPYING.LESSER](https://codeberg.org/cristian.barbarosie/maniFEM/src/branch/main/src/COPYING.LESSER).
It can also be found at <https://www.gnu.org/licenses/>.
