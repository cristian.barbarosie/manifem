
//   field.h  2024.10.04

//   This file is part of maniFEM, a C++ library for meshes and finite elements on manifolds.

//   Copyright  2019 - 2024  Cristian Barbarosie  cristian.barbarosie@gmail.com

//   https://maniFEM.rd.ciencias.ulisboa.pt/
//   https://codeberg.org/cristian.barbarosie/maniFEM

//   ManiFEM is free software: you can redistribute it and/or modify it
//   under the terms of the GNU Lesser General Public License as published
//   by the Free Software Foundation, either version 3 of the License
//   or (at your option) any later version.

//   ManiFEM is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//   See the GNU Lesser General Public License for more details.

//   You should have received a copy of the GNU Lesser General Public License
//   along with maniFEM.  If not, see <https://www.gnu.org/licenses/>.


#ifndef MANIFEM_FIELD_H
#define MANIFEM_FIELD_H

#include <set>

namespace maniFEM {

namespace tag {
	struct LivesOnPositiveCells { };  static const LivesOnPositiveCells lives_on_positive_cells;
	struct LivesOnPoints { };  static const LivesOnPoints lives_on_points;
	struct HasIndexInHeap { };  static const HasIndexInHeap has_index_in_heap;
}

//------------------------------------------------------------------------------------------------------//

	
// we have three types of Fields
// a tag::Util::Field::ShortInt can be used for labeling cells in different regions
//   or for holding the winding number of a segment
// a tag::Util::Field::SizeT can be used for enumerating vertices
// a tag::Util::Field::Double can be used for storing coordinates of vertices or values of solutions

// only tag::Util::Field::Double has wrapper ...

class tag::Util::Field::Core

// core field holding different types of values
// specialized in classes
//    tag::Util::Field::ShortInt::Core, tag::Util::Field::SizeT, tag::Util::Field::Double

{	public :

	std::set < size_t > lives_on_pos_cells_of_dim, lives_on_neg_cells_of_dim;

	inline Core ( const tag::LivesOnPositiveCells &, const tag::OfDimension &, size_t d )
	:	lives_on_pos_cells_of_dim { { d } }, lives_on_neg_cells_of_dim { }
	{	}

	virtual ~Core ( );

	virtual size_t number_of ( const tag::Components & ) = 0;  

};


//------------------------------------------------------------------------------------------------------//


// a tag::Util::Field::ShortInt can be used to label cells in different regions
//   or to hold the winding number of a segment, see class Manifold::Quotient
// copying such an object does not make sense

// note that Cell::Positive::short_int_heap and Cell::Negative::short_int_heap
// are filled with zeros by the Cell::Core constructor (unlike double_heap and size_t_heap)
	
class tag::Util::Field::ShortInt : public tag::Util::Field::Core

{	public :

	// attributes  lives_on_pos_cells_of_dim, lives_on_neg_cells_of_dim
	//   inherited from tag::Util::Field::Core

	size_t index_in_heap;

	inline ShortInt ( const tag::LivesOnPositiveCells &, const tag::OfDimension &, size_t d )
	:	tag::Util::Field::Core ( tag::lives_on_positive_cells, tag::of_dim, d ),
		index_in_heap { Cell::Positive::short_int_heap_size [d] }
	{	Cell::Positive::short_int_heap_size [d] ++;  }

	virtual ~ShortInt ( );

	size_t number_of ( const tag::Components & );  // virtual from tag::Util::Field::Core  
	
	inline short int & on_cell ( Cell::Core * cll );

};

//------------------------------------------------------------------------------------------------------//

	
// a tag::Util::Field::SizeT can be used for enumerating vertices
// copying such an object does not make sense

class tag::Util::Field::SizeT : public tag::Util::Field::Core

{	public :

	// attributes  lives_on_pos_cells_of_dim, lives_on_neg_cells_of_dim
	//   inherited from tag::Util::Field::Core

	size_t index_in_heap;

	inline SizeT ( const tag::LivesOnPositiveCells &, const tag::OfDimension &, size_t d )
	:	tag::Util::Field::Core ( tag::lives_on_positive_cells, tag::of_dim, d ),
		index_in_heap { Cell::Positive::size_t_heap_size [d] }
	{	Cell::Positive::size_t_heap_size [d] ++;  }

	virtual ~SizeT ( );

	size_t number_of ( const tag::Components & );  // virtual from tag::Util::Field::Core  

	inline size_t & on_cell ( Cell::Core * cll );

};

//------------------------------------------------------------------------------------------------------//

	
class Cell::Numbering::Field : public Cell::Numbering::Core

{	public :

	std::unique_ptr < tag::Util::Field::SizeT > field { nullptr };

	size_t counter { 0 }, cell_dim;

	Field ( const Cell::Numbering::Field & ) = delete;
	Field ( const Cell::Numbering::Field && ) = delete;
	Cell::Numbering::Field & operator= ( const Cell::Numbering::Field & ) = delete;
	Cell::Numbering::Field & operator= ( const Cell::Numbering::Field && ) = delete;

	static void set_and_increment ( Cell::Core * cll, void * num );

	inline Field ( const tag::CellsOfDim &, const size_t d )
	:	cell_dim ( d ) 
	{	field = std::make_unique < tag::Util::Field::SizeT >
			( tag::lives_on_positive_cells, tag::of_dim, d );
		Cell::init_pos_cell [d] .push_back ( & Cell::Numbering::Field::set_and_increment );
		Cell::data_for_init_pos [d] .push_back ( static_cast < void* > ( this ) );           }

	inline Field ( const tag::Vertices & )
	:	Field ( tag::cells_of_dim, 0 )
	{	}
	
	inline Field ( const tag::Segments & )
	:	Field ( tag::cells_of_dim, 1 )
	{	}

	virtual ~Field ( );
	
	bool exists ( );  // virtual from Cell::Numbering::Core

	size_t size ( );  // virtual from Cell::Numbering::Core
	
	bool has_cell ( const Cell cll );  // virtual from Cell::Numbering::Core
	
	size_t & index_of ( const Cell );  // virtual from Cell::Numbering::Core
		
};	 // end of  class Cell::Numbering::Field

//------------------------------------------------------------------------------------------------------//


inline Cell::Numbering::Numbering ( const tag::Field & )
:	core ( std::make_shared < Cell::Numbering::Field > ( tag::vertices ) )
{	}

//------------------------------------------------------------------------------------------------------//

// there will be just a few Field objects in a program,
// and they will usually be destroyed only at the end of the program,
// so we do not use here the mechanism of inheriting from tag::Util::Wrapper and tag::Util::Core
	
//------------------------------------------------------------------------------------------------------//


// a tag::Util::Field::Double can be used for storing coordinates of vertices or values of solutions
// only fields of double have wrapper, so they need a core
	
class tag::Util::Field::Double

// wrapper for fields

{	public :

	class Base;
	
	tag::Util::Field::Double::Base * core;

	inline Double ( const tag::WhoseCoreIs &, tag::Util::Field::Double::Base * c )
	:	core { c }
	{	assert ( c );  }

	inline Double ( const tag::LivesOnPoints & );
	inline Double ( const tag::LivesOnPoints &, const tag::HasSize &, size_t s );

	inline ~Double ( ) { }  // should we delete core ?

	inline tag::Util::Field::Double operator[] ( size_t );
	class TakenOnCell;
	inline tag::Util::Field::Double::TakenOnCell operator() ( Cell );
	
	class Block;  class Scalar;
};


class tag::Util::Field::Double::Base : public tag::Util::Field::Core

// base class for several different fields

{	public :

	// attributes  lives_on_pos_cells_of_dim, lives_on_neg_cells_of_dim
	//   inherited from tag::Util::Field::Core

	inline Base ( const tag::LivesOnPositiveCells &, const tag::OfDimension &, size_t d )
	:	tag::Util::Field::Core ( tag::lives_on_positive_cells, tag::of_dim, d )
	{	}

	virtual ~Base ( );

	// size_t number_of ( const tag::Components & )  stays pure virtual from tag::Util::Field::Core

	virtual tag::Util::Field::Double::Scalar * component ( size_t ) = 0;

	inline tag::Util::Field::Double::TakenOnCell on_cell ( Cell::Core * cll );

};


class tag::Util::Field::Double::Scalar : public tag::Util::Field::Double::Base
	
{	public :

	// attributes  lives_on_pos_cells_of_dim, lives_on_neg_cells_of_dim
	//   inherited from tag::Util::Field::Core

	size_t index_in_heap;

	inline Scalar ( const tag::LivesOnPositiveCells &, const tag::OfDimension &, size_t d )
	:	tag::Util::Field::Double::Base ( tag::lives_on_positive_cells, tag::of_dim, d ),
		index_in_heap { Cell::Positive::double_heap_size [d] }
	{	Cell::Positive::double_heap_size [d] ++;  }
	
	inline Scalar ( const tag::LivesOnPositiveCells &, const tag::OfDimension &,
		size_t d, const tag::HasIndexInHeap, size_t i )
	:	tag::Util::Field::Double::Base ( tag::lives_on_positive_cells, tag::of_dim, d ),
		index_in_heap { i }
	{ }
	
	virtual ~Scalar ( );

	size_t number_of ( const tag::Components & );  // virtual from tag::Util::Field::Double::Base

	tag::Util::Field::Double::Scalar * component ( size_t );
	// virtual from tag::Util::Field::Double::Base

	// tag::Util::Field::Double::TakenOnCell on_cell ( Cell::Core * cll )
	//   defined inline by tag::Util::Field::Double::Base

};


class tag::Util::Field::Double::Block : public tag::Util::Field::Double::Base
	
{	public :

	// attributes  lives_on_pos_cells_of_dim, lives_on_neg_cells_of_dim
	//   inherited from tag::Util::Field::Core

	size_t min_index, max_index_p1;
	
	inline Block ( const tag::LivesOnPositiveCells &, const tag::OfDimension &, size_t d,
	                    const tag::HasSize &, size_t s                                   )
	:	tag::Util::Field::Double::Base ( tag::lives_on_positive_cells, tag::of_dim, d )
	{	min_index = Cell::Positive::double_heap_size [d];
		Cell::Positive::double_heap_size [d] += s;
		max_index_p1 = Cell::Positive::double_heap_size [d];  }

	virtual ~Block ( );
	
	size_t number_of ( const tag::Components & );  // virtual from tag::Util::Field::Double::Base
	
	tag::Util::Field::Double::Scalar * component ( size_t );
	// virtual from tag::Util::Field::Double::Base

	// tag::Util::Field::Double::TakenOnCell on_cell ( Cell::Core * cll )
	//   defined inline by tag::Util::Field::Double::Base

};

	
inline tag::Util::Field::Double::Double ( const tag::LivesOnPoints & )
:	core { new tag::Util::Field::Double::Scalar ( tag::lives_on_positive_cells, tag::of_dim, 0 ) }
{	assert ( core );  }

	
inline tag::Util::Field::Double::Double ( const tag::LivesOnPoints &, const tag::HasSize &, size_t s )
:	core { new tag::Util::Field::Double::Block ( tag::lives_on_positive_cells, tag::of_dim, 0,
                                  tag::has_size, s                               ) }
{	assert ( core );  }

//------------------------------------------------------------------------------------------------------//


// class tag::Util::Field::Double::TakenOnCell holds a temporary object,
// the result of tag::Util::Field::Double::operator(), or of tag::Util::Field::Double::Base::on_cell
// it is versatile in the sense that the Field may have one components or several
// if the Field f has one component, we may use the tag::Util::Field::Double::TakenOnCell object like in
// double x = f(cll)  or  f(cll) = 2.0
// if the Field f has several components, we may use the
//   tag::Util::Field::Double::TakenOnCell object like in
// vector<double> vec = f(cll)  or  f(cll) = vec

// We should attend to requests of the kind of : += *=
// or  x = y = F(P)  or  F(P) = F(Q) = 2  or even  F(P) = F(P)

class tag::Util::Field::Double::TakenOnCell	

{	public :

	tag::Util::Field::Double::Base * f;
	Cell::Core * cll;

	inline TakenOnCell ( tag::Util::Field::Double::Base * ff, Cell::Core * cc )
	:	f { ff }, cll { cc }
	{	}
  
	TakenOnCell ( const tag::Util::Field::Double::TakenOnCell & ) = delete;
	TakenOnCell ( const tag::Util::Field::Double::TakenOnCell && ) = delete;
	tag::Util::Field::Double::TakenOnCell operator=
		( const tag::Util::Field::Double::TakenOnCell & ) = delete;
	tag::Util::Field::Double::TakenOnCell operator=
		( const tag::Util::Field::Double::TakenOnCell && ) = delete;
	
	inline operator double ()
	// can be used like in  double x = f(cll)  or  cout << f(cll)
	{	return this->reference();  }
		
	inline double & operator= ( const double & x )
	// can be used like in  f(cll) = 2.0
	{	double & y = this->reference();
		y = x;  return y;               }

	inline double & reference ( )
	// can be used like in  f(cll) = 2.0
	{	tag::Util::Field::Double::Scalar * f_scalar = tag::Util::assert_cast
			< tag::Util::Field::Double::Base*, tag::Util::Field::Double::Scalar* > ( f );
		assert ( f_scalar->index_in_heap < cll->double_heap .size() );
		return cll->double_heap [ f_scalar->index_in_heap ];         }

	inline operator std::vector<double> ()
	// can be used like in  vector<double> vec = f(cll)
	{	tag::Util::Field::Double::Block * f_block = tag::Util::assert_cast
			< tag::Util::Field::Double::Base*, tag::Util::Field::Double::Block* > ( f );
		assert ( f_block->max_index_p1 <= cll->double_heap .size() );
		return std::vector<double> { & cll->double_heap [ f_block->min_index ],
		                             & cll->double_heap [ f_block->max_index_p1 ] };  }
	
	inline std::vector<double> operator= ( const std::vector<double> & x )
	// can be used like in  f(cll) = vec
	{	tag::Util::Field::Double::Block * f_block = tag::Util::assert_cast
			< tag::Util::Field::Double::Base*, tag::Util::Field::Double::Block* > ( f );
		size_t i_min    = f_block->min_index,
		       i_max_p1 = f_block->max_index_p1;
		for ( size_t i = i_min; i < i_max_p1; i++ )
			cll->double_heap [i] = x[i];  // there should be a faster solution
		return x;                                                                        }
};

//------------------------------------------------------------------------------------------------------//


inline short int & tag::Util::Field::ShortInt::on_cell ( Cell::Core * cll )
{
	#ifndef NDEBUG
	if ( cll->is_positive() )
		assert ( this->lives_on_pos_cells_of_dim .find ( cll->get_dim() ) !=
		         this->lives_on_pos_cells_of_dim .end()                      );
	else
		assert ( this->lives_on_neg_cells_of_dim .find ( cll->get_dim() ) !=
		         this->lives_on_neg_cells_of_dim .end()                      );
	#endif  // DEBUG
	assert ( this->index_in_heap < cll->short_int_heap .size() );
	return cll->short_int_heap [ this->index_in_heap ];                         }


inline size_t & tag::Util::Field::SizeT::on_cell ( Cell::Core * cll )
{
	#ifndef NDEBUG
	if ( cll->is_positive() )
		assert ( this->lives_on_pos_cells_of_dim .find ( cll->get_dim() ) !=
		         this->lives_on_pos_cells_of_dim .end()                      );
	else
		assert ( this->lives_on_neg_cells_of_dim .find ( cll->get_dim() ) !=
		         this->lives_on_neg_cells_of_dim .end()                      );
	#endif  // DEBUG
	assert ( this->index_in_heap < cll->size_t_heap .size() );
	return cll->size_t_heap [ this->index_in_heap ];                            }


inline tag::Util::Field::Double::TakenOnCell tag::Util::Field::Double::Base::on_cell ( Cell::Core * cll )
{
	#ifndef NDEBUG
	if ( cll->is_positive() )
		assert ( this->lives_on_pos_cells_of_dim .find ( cll->get_dim() ) !=
		         this->lives_on_pos_cells_of_dim .end()                      );
	else
		assert ( this->lives_on_neg_cells_of_dim .find ( cll->get_dim() ) !=
		         this->lives_on_neg_cells_of_dim .end()                      );
	#endif  // DEBUG
	return tag::Util::Field::Double::TakenOnCell ( this, cll );                 }


inline tag::Util::Field::Double tag::Util::Field::Double::operator[] ( size_t i )
{	return tag::Util::Field::Double ( tag::whose_core_is, this->core->component(i) );  }
	
inline tag::Util::Field::Double::TakenOnCell tag::Util::Field::Double::operator() ( Cell cll )
{	return this->core->on_cell ( cll.core );  }

	
} // namespace maniFEM

#endif  // ifndef MANIFEM_FIELD_H
