
//   function.cpp  2025.02.17

//   This file is part of maniFEM, a C++ library for meshes and finite elements on manifolds.

//   Copyright  2019 - 2025  Cristian Barbarosie  cristian.barbarosie@gmail.com

//   https://maniFEM.rd.ciencias.ulisboa.pt/
//   https://codeberg.org/cristian.barbarosie/maniFEM

//   ManiFEM is free software: you can redistribute it and/or modify it
//   under the terms of the GNU Lesser General Public License as published
//   by the Free Software Foundation, either version 3 of the License
//   or (at your option) any later version.

//   ManiFEM is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//   See the GNU Lesser General Public License for more details.

//   You should have received a copy of the GNU Lesser General Public License
//   along with maniFEM.  If not, see <https://www.gnu.org/licenses/>.


#include "math.h"
#include <sstream>

#include "function.h"
#include "manifold.h"
#include "manifold-xx.h"

using namespace maniFEM;

//------------------------------------------------------------------------------------------------------//

namespace { // anonymous namespace, mimics static linkage

inline Function::Core * function_with_field ( const size_t dim, const size_t s )

// field lives on cells of dim 'dim', has 's' components
// see also  Manifold::Euclid::build_coord_func
		
{	if ( s == 1 )
	{	tag::Util::Field::Double::Scalar * field_scalar = new tag::Util::Field::Double::Scalar
			( tag::lives_on_positive_cells, tag::of_dim, dim );
		return new Function::CoupledWithField::Scalar ( field_scalar );                         }
	assert ( s > 1 );
	tag::Util::Field::Double::Block * field_block = new tag::Util::Field::Double::Block
		( tag::lives_on_positive_cells, tag::of_dim, dim, tag::has_size, s );
	return new Function::CoupledWithField::Vector ( field_block );                               }

}  // end of anonymous namespace


Function::Function ( const tag::LivesOn &, const tag::CellsOfDim &, const size_t dim,
                     const tag::HasSize &, const size_t s                            )
:	Function ( tag::whose_core_is, function_with_field ( dim, s ) )
{	}
	
//------------------------------------------------------------------------------------------------------//


unsigned int Function::total_cores { 0 };

#ifndef MANIFEM_NO_QUOTIENT
size_t Function::ActionGenerator::counter { 0 };
#endif  // ifndef MANIFEM_NO_QUOTIENT

#ifndef NDEBUG  // DEBUG mode
std::map < const Function::Core*, std::string > Function::name;
#endif

//------------------------------------------------------------------------------------------------------//

bool Function::less_for_map ( const Function & f, const Function & g )  //static
{	return f .core < g .core;  }
// needed for map 'jacobian' in class Function::Map
// and for map 'equations' in class Manifold::Parametric

//------------------------------------------------------------------------------------------------------//

size_t Function::Scalar::number_of ( const tag::Components & ) const  // virtual from Function::Core
{	return 1;  }

size_t Function::Aggregate::number_of ( const tag::Components & ) const
// virtual from Function::Core, through Function::Vector
{	return this->components .size();  }

size_t Function::Immersion::number_of ( const tag::Components & ) const
// virtual from Function::Core, through Function::Vector
{	assert ( false );
	// we return zero just to avoid compilation errors
	return 0;           }

size_t Function::CoupledWithField::Vector::number_of ( const tag::Components & ) const
// virtual from Function::Core, defined in Function::Aggregate, here overridden
{	assert ( this->components .size() == this->field->number_of ( tag::components ) );
	return this->field->number_of ( tag::components );                                  }

#ifndef MANIFEM_NO_QUOTIENT

size_t Function::Vector::MultiValued::number_of ( const tag::Components & ) const
// virtual from Function::Core
{	return this->base .number_of ( tag::components );  }

#endif  // ifndef MANIFEM_NO_QUOTIENT

//------------------------------------------------------------------------------------------------------//


Function Function::Scalar::component ( size_t i )
// virtual from Function::Core
// never actually used because Function::operator[] returns self
{	assert ( i == 0 );
	return Function ( tag::whose_core_is, this );  }

Function Function::Aggregate::component ( size_t i )
// virtual from Function::Core
{	assert ( i < this->components .size() );
	return this->components[i];               }

Function Function::Immersion::component ( size_t i )
// virtual from Function::Core
{	assert ( false );
	// we return a non-existent Function just to avoid compilation errors
	return Function ( tag::non_existent );  }

Function Function::CoupledWithField::Vector::component ( size_t i )
// virtual from Function::Core, defined in Function::Aggregate, here overridden
{	assert ( i < this->field->number_of ( tag::components ) );
	assert ( this->components .size() == this->field->number_of ( tag::components ) );
	return this->components [i];                                             }

#ifndef MANIFEM_NO_QUOTIENT

Function Function::Vector::MultiValued::JumpIsLinear::component ( size_t i )
// virtual from Function::Core, defined by Function::Aggregate, here overridden
// difficult case ! it's impossible to compute separately the value of
// a component of such a multifunction, the jump depends on all other components
{	assert ( false ); 
	// we return a non-existent Function just to avoid compilation errors
	return Function ( tag::non_existent );  }

#endif  // ifndef MANIFEM_NO_QUOTIENT

//------------------------------------------------------------------------------------------------------//
	

void Function::Scalar::set_value ( double v )
// virtual, overridden by Function::Costant and by Function::Scalar::MultiValued
{ assert ( false );  }

void Function::Constant::set_value ( double v )
// virtual from Function::Scalar, here overridden
{ this->value = v;  }

double Function::Constant::get_value_on_cell ( Cell::Core * cll ) const
// virtual from Function::Scalar
{ return this->value;  }

double Function::Sum::get_value_on_cell ( Cell::Core * cll ) const
// virtual from Function::Scalar
{ std::forward_list<Function>::const_iterator it = this->terms .begin();
	double sum = 0.;
	for ( ; it != this->terms .end(); it++ )
	{	Function::Scalar * term_scalar = tag::Util::assert_cast
		< Function::Core*, Function::Scalar* > ( it->core );
		sum += term_scalar->get_value_on_cell ( cll );           }
	return sum;                                                            }

double Function::Product::get_value_on_cell ( Cell::Core * cll ) const
// virtual from Function::Scalar
{ std::forward_list<Function>::const_iterator it = this->factors .begin();
	double prod = 1.;
	for ( ; it != this->factors .end(); it++ )
	{	Function::Scalar * fact_scalar = tag::Util::assert_cast
			< Function::Core*, Function::Scalar* > ( it->core );
		prod *= fact_scalar->get_value_on_cell ( cll );          }
	return prod;                                                             }
	
double Function::Power::get_value_on_cell ( Cell::Core * cll ) const
// virtual from Function::Scalar
{	Function::Scalar * base_scalar = tag::Util::assert_cast
		< Function::Core*, Function::Scalar* > ( this->base .core );
	return std::pow ( base_scalar->get_value_on_cell ( cll ), this->exponent );  }
	
double Function::Expo::get_value_on_cell ( Cell::Core * cll ) const
// virtual from Function::Scalar
{	Function::Scalar * exp_scalar = tag::Util::assert_cast
		< Function::Core*, Function::Scalar* > ( this->exponent .core );
	return std::pow ( std::numbers::e, exp_scalar->get_value_on_cell ( cll ) );  }
	
double Function::Sqrt::get_value_on_cell ( Cell::Core * cll ) const
// virtual from Function::Scalar
{	Function::Scalar * base_scalar = tag::Util::assert_cast
		< Function::Core*, Function::Scalar* > ( this->base .core );
	return std::sqrt ( base_scalar->get_value_on_cell ( cll ) );      }
	
double Function::Sin::get_value_on_cell ( Cell::Core * cll ) const
// virtual from Function::Scalar
{	Function::Scalar * base_scalar = tag::Util::assert_cast
		< Function::Core*, Function::Scalar* > ( this->base .core );
	return std::sin ( base_scalar->get_value_on_cell(cll) );         }
	
double Function::Cos::get_value_on_cell ( Cell::Core * cll ) const
// virtual from Function::Scalar
{	Function::Scalar * base_scalar = tag::Util::assert_cast
		< Function::Core*, Function::Scalar* > ( this->base .core );
	return std::cos ( base_scalar->get_value_on_cell ( cll ) );      }
	
double Function::Step::get_value_on_cell ( Cell::Core * cll ) const
// virtual from Function::Scalar
{	Function::Scalar * arg_scalar = tag::Util::assert_cast
			< Function::Core*, Function::Scalar* > ( this->arg .core );
	double arg_v = arg_scalar->get_value_on_cell(cll);
	for ( size_t i = 0; i < this->cuts .size(); i++ )
		if ( arg_v < cuts [i] )
		{	Function::Scalar * val_i_scalar = tag::Util::assert_cast
				< Function::Core*, Function::Scalar* > ( this->values [i] .core );
			return val_i_scalar->get_value_on_cell ( cll );                        }
	Function::Scalar * val_scalar = tag::Util::assert_cast
		< Function::Core*, Function::Scalar* > ( this->values .back() .core );
	return val_scalar->get_value_on_cell ( cll );                                   }
	
std::vector < double > Function::Aggregate::get_value_on_cell ( Cell::Core * cll ) const
// virtual from Function::Vector
{ size_t n = this->number_of ( tag::components );
	std::vector < double > result ( n );
	for ( size_t i = 0; i < n; i++ )
	{	Function::Scalar * comp_scalar = tag::Util::assert_cast
			< Function::Core*, Function::Scalar* > ( this->components [i] .core );
		result[i] = comp_scalar->get_value_on_cell ( cll );                        }
	return result;                                                                   }

double Function::CoupledWithField::Scalar::get_value_on_cell ( Cell::Core * cll ) const
// virtual from Function::Scalar
{ return this->field->on_cell ( cll );  }
// { return this->field->on_cell ( cll ) .reference();  }
	
double Function::Diffeomorphism::OneDim::get_value_on_cell ( Cell::Core * cll ) const
{	assert ( false );   // virtual from Function::Scalar
	// we return zero just to avoid compilation errors
	return 0;          }
	
std::vector < double > Function::CoupledWithField::Vector::get_value_on_cell
( Cell::Core * cll ) const  // virtual from Function::Vector
{ return this->field->on_cell ( cll );  }
// { return this->field->on_cell ( cll ) .reference();  }
	
std::vector < double > Function::Immersion::get_value_on_cell ( Cell::Core * cll ) const
{	assert ( false );    // virtual from Function::Vector
	// we return an empty vector just to avoid compilation errors
	return std::vector < double > ();  }

double Function::Composition::get_value_on_cell ( Cell::Core * cll ) const
// virtual from Function::Scalar
{	Function::Scalar * base_scalar = tag::Util::assert_cast
		< Function::Core*, Function::Scalar* > ( this->base .core );
	// we assume here that cll lives in master coordinates
	return base_scalar->get_value_on_cell ( cll );                  }

#ifndef MANIFEM_NO_QUOTIENT

double Function::Constant::get_value_on_cell
( Cell::Core *, const tag::Winding &, const Function::Action & exp ) const
// virtual from Function::Scalar
{ return this->value;  }

double Function::Sum::get_value_on_cell
	( Cell::Core * cll, const tag::Winding &, const Function::Action & exp ) const
// virtual from Function::Scalar
{ std::forward_list<Function>::const_iterator it = this->terms .begin();
	double sum = 0.;
	for ( ; it != this->terms .end(); it++ )
	{	Function::Scalar * term_scalar = tag::Util::assert_cast
		< Function::Core*, Function::Scalar* > ( it->core );
		sum += term_scalar->get_value_on_cell ( cll, tag::winding, exp );  }
	return sum;                                                              }

double Function::Product::get_value_on_cell
( Cell::Core * cll, const tag::Winding &, const Function::Action & exp ) const
// virtual from Function::Scalar
{ std::forward_list<Function>::const_iterator it = this->factors .begin();
	double prod = 1.;
	for ( ; it != this->factors .end(); it++ )
	{	Function::Scalar * fact_scalar = tag::Util::assert_cast
			< Function::Core*, Function::Scalar* > ( it->core );
		prod *= fact_scalar->get_value_on_cell ( cll, tag::winding, exp );  }
	return prod;                                                              }
	
double Function::Power::get_value_on_cell
( Cell::Core * cll, const tag::Winding &, const Function::Action & exp ) const
// virtual from Function::Scalar
{	Function::Scalar * base_scalar = tag::Util::assert_cast
		< Function::Core*, Function::Scalar* > ( this->base .core );
	return std::pow ( base_scalar->get_value_on_cell ( cll, tag::winding, exp ), this->exponent );  }
	
double Function::Expo::get_value_on_cell
( Cell::Core * cll, const tag::Winding &, const Function::Action & exp ) const
// virtual from Function::Scalar
{	Function::Scalar * exp_scalar = tag::Util::assert_cast
		< Function::Core*, Function::Scalar* > ( this->exponent .core );
	return std::pow ( std::numbers::e, exp_scalar->get_value_on_cell ( cll, tag::winding, exp ) );  }
	
double Function::Sqrt::get_value_on_cell
( Cell::Core * cll, const tag::Winding &, const Function::Action & exp ) const
// virtual from Function::Scalar
{	Function::Scalar * base_scalar = tag::Util::assert_cast
		< Function::Core*, Function::Scalar* > ( this->base .core );
	return std::sqrt ( base_scalar->get_value_on_cell ( cll, tag::winding, exp ) );  }
	
double Function::Sin::get_value_on_cell
( Cell::Core * cll, const tag::Winding &, const Function::Action & exp ) const
// virtual from Function::Scalar
{	Function::Scalar * base_scalar = tag::Util::assert_cast
		< Function::Core*, Function::Scalar* > ( this->base .core );
	return std::sin ( base_scalar->get_value_on_cell ( cll, tag::winding, exp ) );  }
	
double Function::Cos::get_value_on_cell
( Cell::Core * cll, const tag::Winding &, const Function::Action & exp ) const
// virtual from Function::Scalar
{	Function::Scalar * base_scalar = tag::Util::assert_cast
		< Function::Core*, Function::Scalar* > ( this->base .core );
	return std::cos ( base_scalar->get_value_on_cell ( cll, tag::winding, exp ) );  }
	
double Function::Step::get_value_on_cell
( Cell::Core * cll, const tag::Winding &, const Function::Action & exp ) const
// virtual from Function::Scalar
{	Function::Scalar * arg_scalar = tag::Util::assert_cast
			< Function::Core*, Function::Scalar* > ( this->arg .core );
	double arg_v = arg_scalar->get_value_on_cell ( cll, tag::winding, exp );
	for ( size_t i = 0; i < this->cuts .size(); i++ )
		if ( arg_v < cuts [i] )
		{	Function::Scalar * val_i_scalar = tag::Util::assert_cast
				< Function::Core*, Function::Scalar* > ( this->values [i] .core );
			return val_i_scalar->get_value_on_cell ( cll, tag::winding, exp );     }
	Function::Scalar * val_scalar = tag::Util::assert_cast
		< Function::Core*, Function::Scalar* > ( this->values .back() .core );
	return val_scalar->get_value_on_cell ( cll, tag::winding, exp );                }
	
std::vector < double > Function::Aggregate::get_value_on_cell
( Cell::Core * cll, const tag::Winding &, const Function::Action & exp ) const
// virtual from Function::Vector
{	assert ( false );
	// we return an empty vector just to avoid compilation errors
	return std::vector < double > ();   }

double Function::CoupledWithField::Scalar::get_value_on_cell
( Cell::Core * cll, const tag::Winding &, const Function::Action & exp ) const
// virtual from Function::Scalar
{ return this->field->on_cell ( cll );  }
// { return this->field->on_cell ( cll ) .reference();  }

double Function::Diffeomorphism::OneDim::get_value_on_cell
( Cell::Core * cll, const tag::Winding &, const Function::Action & exp ) const
{	assert ( false );    // virtual from Function::Scalar
	// we return zero just to avoid compilation errors
	return 0;         }
	
std::vector < double > Function::CoupledWithField::Vector::get_value_on_cell
( Cell::Core * cll, const tag::Winding &, const Function::Action & exp ) const
{ return this->field->on_cell ( cll );  }  // virtual from Function::Vector
// { return this->field->on_cell ( cll ) .reference();  }
	
std::vector < double > Function::Immersion::get_value_on_cell
( Cell::Core * cll, const tag::Winding &, const Function::Action & exp ) const
{	assert ( false );   // virtual from Function::Vector
	// we return an empty vector just to avoid compilation errors
	return std::vector < double > ();  }

double Function::Composition::get_value_on_cell  // virtual from Function::Scalar
( Cell::Core * cll, const tag::Winding &, const Function::Action & exp ) const
{	assert ( false );
	// we return zero just to avoid compilation errors
	return 0;          }
	
#endif  // ifndef MANIFEM_NO_QUOTIENT

#ifndef MANIFEM_NO_FEM

double Function::MereSymbol::get_value_on_cell ( Cell::Core * cll ) const
{	assert ( false );   // virtual from Function::Scalar
	// we return zero just to avoid compilation errors
	return 0;          }
	
double Function::Unknown::get_value_on_cell ( Cell::Core * cll ) const
{	assert ( false );   // virtual from Function::Scalar
	// we return zero just to avoid compilation errors
	return 0;          }
	
double Function::Test::get_value_on_cell ( Cell::Core * cll ) const
{	assert ( false );   // virtual from Function::Scalar
	// we return zero just to avoid compilation errors
	return 0;          }
	
double Function::DelayedDerivative::get_value_on_cell ( Cell::Core * cll ) const
{	assert ( false );   // virtual from Function::Scalar
	// we return zero just to avoid compilation errors
	return 0;          }

double Function::CoupledWithArray::Scalar::get_value_on_cell ( Cell::Core * cll_p ) const
// virtual from Function::Scalar
{	Cell cll ( tag::whose_core_is, cll_p, tag::previously_existing, tag::surely_not_null );
	size_t i = this->numbering ( cll );
	return  ( * this->vector ) (i);                                                          }
	
#ifndef MANIFEM_NO_QUOTIENT
	
double Function::MereSymbol::get_value_on_cell
( Cell::Core * cll, const tag::Winding &, const Function::Action & exp ) const
{	assert ( false );    // virtual from Function::Scalar
	// we return zero just to avoid compilation errors
	return 0;         }
	
double Function::Unknown::get_value_on_cell
( Cell::Core * cll, const tag::Winding &, const Function::Action & exp ) const
{	assert ( false );    // virtual from Function::Scalar
	// we return zero just to avoid compilation errors
	return 0;         }
	
double Function::Test::get_value_on_cell
( Cell::Core * cll, const tag::Winding &, const Function::Action & exp ) const
{	assert ( false );    // virtual from Function::Scalar
	// we return zero just to avoid compilation errors
	return 0;         }
	
double Function::DelayedDerivative::get_value_on_cell
( Cell::Core * cll, const tag::Winding &, const Function::Action & exp ) const
{	assert ( false );    // virtual from Function::Scalar
	// we return zero just to avoid compilation errors
	return 0;         }

double Function::CoupledWithArray::Scalar::get_value_on_cell  // virtual from Function::Scalar
( Cell::Core * cll_p, const tag::Winding &, const Function::Action & exp ) const
{	assert ( false );  // to implement
	Cell cll ( tag::whose_core_is, cll_p, tag::previously_existing, tag::surely_not_null );
	size_t i = this->numbering ( cll );
	return  ( * this->vector ) (i);                                                          }

#endif  // ifndef MANIFEM_NO_QUOTIENT
	
#endif  // ifndef MANIFEM_NO_FEM

//------------------------------------------------------------------------------------------------------//


#ifndef MANIFEM_NO_QUOTIENT

void Function::Scalar::MultiValued::set_value ( double v )  // virtual from Function::Scalar
{	Function::Scalar * base_scalar = tag::Util::assert_cast
		< Function::Core*, Function::Scalar* > ( this->base .core );
	base_scalar->set_value ( v );                                    }

//------------------------------------------------------------------------------------------------------//


double Function::Scalar::MultiValued::get_value_on_cell ( Cell::Core * cll ) const
// virtual from Function::Scalar
{	Function::Scalar * base_scalar = tag::Util::assert_cast
		< Function::Core*, Function::Scalar* > ( this->base .core );
	return base_scalar->get_value_on_cell ( cll );                   }

//------------------------------------------------------------------------------------------------------//


double Function::Scalar::MultiValued::JumpIsSum::get_value_on_cell
( Cell::Core * cll, const tag::Winding &, const Function::Action & exp ) const
// virtual from Function::Scalar
	
{	Function::Scalar * base_scalar = tag::Util::assert_cast
		< Function::Core*, Function::Scalar* > ( this->base .core );
	#ifndef NDEBUG  // DEBUG mode
	std::shared_ptr < Manifold::Quotient > manif = tag::Util::assert_shared_cast
		< Manifold::Core, Manifold::Quotient > ( Manifold::working.core );
	#endif
	size_t n = this->actions.size();
	assert ( manif->actions == this->actions );
	assert ( this->beta.size() == n );
  double val = base_scalar->get_value_on_cell ( cll );
	for ( size_t i = 0; i < n; i++ )
	{	std::map<Function::ActionGenerator,short int>::const_iterator it =
			exp.index_map.find ( this->actions[i] );
		if ( it == exp.index_map.end() ) continue;
		val += it->second * this->beta[i];                                  }
	return val;                                                               }

//------------------------------------------------------------------------------------------------------//


double Function::Scalar::MultiValued::JumpIsLinear::get_value_on_cell
( Cell::Core * cll, const tag::Winding &, const Function::Action & exp ) const
// virtual from Function::Scalar

{	Function::Scalar * base_scalar = tag::Util::assert_cast
		< Function::Core*, Function::Scalar* > ( this->base .core );
	#ifndef NDEBUG  // DEBUG mode
	std::shared_ptr < Manifold::Quotient > manif = tag::Util::assert_shared_cast
		< Manifold::Core, Manifold::Quotient > ( Manifold::working.core );
	assert ( manif->actions == this->actions );
	#endif  // DEBUG
	size_t n = this->alpha.size();
	assert ( this->actions.size() == n );
  double val = base_scalar->get_value_on_cell ( cll ) - this->gamma;
	for ( size_t i = 0; i < n; i++ )
	{	std::map<Function::ActionGenerator,short int>::const_iterator it =
			exp.index_map.find ( this->actions[i] );
		if ( it == exp.index_map.end() ) continue;
		short int exp_i = it->second;
		assert ( exp_i != 0 );
		double alpha_i = this->alpha[i];
		if ( exp_i > 0 )
		{	size_t abs_exp_i = exp_i;
			for ( size_t j = 0; j < abs_exp_i; j++ ) val *= alpha_i;  }
		if ( exp_i < 0 )
		{	size_t abs_exp_i = -exp_i;
			for ( size_t j = 0; j < abs_exp_i; j++ ) val /= alpha_i;  }       }
	return val + this->gamma;                                                  }

//------------------------------------------------------------------------------------------------------//


std::vector < double > Function::Vector::MultiValued::get_value_on_cell
( Cell::Core * cll ) const  // virtual from Function::Vector
{	Function::Vector * base_vector = tag::Util::assert_cast
		< Function::Core*, Function::Vector* > ( this->base .core );
	return base_vector->get_value_on_cell(cll);                      }

//------------------------------------------------------------------------------------------------------//


std::vector < double > Function::Vector::MultiValued::JumpIsSum::get_value_on_cell
( Cell::Core * cll, const tag::Winding &, const Function::Action & exp ) const
// virtual from Function::Vector
	
{	Function::Vector * base_vector = tag::Util::assert_cast
		< Function::Core*, Function::Vector* > ( this->base .core );
	#ifndef NDEBUG  // DEBUG mode
	std::shared_ptr < Manifold::Quotient > manif = tag::Util::assert_shared_cast
		< Manifold::Core, Manifold::Quotient > ( Manifold::working.core );
	#endif
	size_t n = this->actions.size();
	size_t dim = base_vector->number_of ( tag::components );
	assert ( manif->actions == this->actions );
	assert ( this->beta.size() == n );
	std::vector < double > val = base_vector->get_value_on_cell ( cll );
	for ( size_t i = 0; i < n; i++ )
	{	assert ( beta[i].size() == dim );
		std::map<Function::ActionGenerator,short int>::const_iterator it =
			exp.index_map.find ( this->actions[i] );
		if ( it == exp.index_map.end() ) continue;
		short int exp_i = it->second;
		assert ( exp_i != 0 );
		for ( size_t j = 0; j < dim; j++ )
			val[j] += exp_i * this->beta[i][j];                               }
	return val;                                                                }

//------------------------------------------------------------------------------------------------------//


std::vector < double > Function::Vector::MultiValued::JumpIsLinear::get_value_on_cell
// virtual from Function::Vector
( Cell::Core * cll, const tag::Winding &, const Function::Action & exp ) const
	
{	Function::Vector * base_vector = tag::Util::assert_cast
		< Function::Core*, Function::Vector* > ( this->base .core );
	#ifndef NDEBUG  // DEBUG mode
	std::shared_ptr < Manifold::Quotient > manif = tag::Util::assert_shared_cast
		< Manifold::Core, Manifold::Quotient > ( Manifold::working.core );
	assert ( manif->actions == this->actions );
	#endif  // DEBUG
	size_t n = this->actions.size();
	size_t dim = base_vector->number_of ( tag::components );
	assert ( this->A.size() == n );
	assert ( this->b.size() == n );
	std::vector < double > val = base_vector->get_value_on_cell ( cll );
	for ( size_t i = 0; i < n; i++ )
	{	assert ( this->A[i].size() == dim );
		std::map<Function::ActionGenerator,short int>::const_iterator it =
			exp.index_map.find ( this->actions[i] );
		if ( it == exp.index_map.end() ) continue;
		short int exp_i = it->second;
		if ( exp_i > 0 )
		{	size_t abs_exp_i = exp_i;
			for ( size_t j = 0; j < abs_exp_i; j++ )
			{	std::vector < double > temp ( dim, 0. );
				for ( size_t k = 0; k < dim; k++ )
				for ( size_t l = 0; l < dim; l++ )
					temp[k] += this->A[i][k][l]*val[l];
				for ( size_t k = 0; k < dim; k++ ) val[k] = temp[k] + this->b[i][k];  }  }
		if ( exp_i < 0 )
		{	size_t abs_exp_i = -exp_i;
			for ( size_t j = 0; j < abs_exp_i; j++ )
				{	std::vector < double > temp ( dim, 0. );
				for ( size_t k = 0; k < dim; k++ )
				for ( size_t l = 0; l < dim; l++ )
					temp[k] += this->Ainv[i][k][l]*(val[l]-this->b[i][l]);
				val = std::move ( temp );                                  }  }               }
	return val;                                                                               }

#endif  // ifndef MANIFEM_NO_QUOTIENT

//------------------------------------------------------------------------------------------------------//
//------------------------------------------------------------------------------------------------------//
	

double Function::ArithmeticExpression::set_value_on_cell
( Cell::Core * cll, const double & )  // virtual from Function::Scalar
{	std::cout << __FILE__ << ":" << __LINE__ << ": "
            << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "Cannot assign to an arithmetic expression." << std::endl;
	exit ( 1 );                                                              }
	
#ifndef MANIFEM_NO_FEM
	
double Function::MereSymbol::set_value_on_cell
( Cell::Core * cll, const double & )  // virtual from Function::Scalar
{	std::cout << __FILE__ << ":" << __LINE__ << ": "
            << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "Cannot assign to a mere symbol." << std::endl;
	exit ( 1 );                                                   }
	
double Function::Unknown::set_value_on_cell
( Cell::Core * cll, const double & )  // virtual from Function::Scalar
{	std::cout << __FILE__ << ":" << __LINE__ << ": "
            << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "Cannot assign to a symbolic unknown." << std::endl;
	exit ( 1 );                                                   }
	
double Function::Test::set_value_on_cell
( Cell::Core * cll, const double & )  // virtual from Function::Scalar
{	std::cout << __FILE__ << ":" << __LINE__ << ": "
            << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "Cannot assign to a symbolic test." << std::endl;
	exit ( 1 );                                                   }

double Function::DelayedDerivative::set_value_on_cell
( Cell::Core * cll, const double & )  // virtual from Function::Scalar
{	std::cout << __FILE__ << ":" << __LINE__ << ": "
            << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "Cannot assign to a delayed derivative." << std::endl;
	exit ( 1 );                                                          }
	
#endif  // ifndef MANIFEM_NO_FEM

std::vector < double > Function::Aggregate::set_value_on_cell
( Cell::Core * cll, const std::vector < double > & x )  // virtual from Function::Vector
// there should be a faster way !!
{	size_t n = this->number_of ( tag::components );
	assert ( n == x.size() );
	for ( size_t i = 0; i < n; i++ )
	{	Function::Scalar * comp_scalar = tag::Util::assert_cast
			< Function::Core*, Function::Scalar* > ( this->components[i].core );
		comp_scalar->set_value_on_cell ( cll, x[i] );                            }
	return x;     		                                                             }

double Function::CoupledWithField::Scalar::set_value_on_cell
( Cell::Core * cll, const double & x )  // virtual from Function::Scalar
{	return  this->field->on_cell(cll) = x;  }

double Function::Diffeomorphism::OneDim::set_value_on_cell
( Cell::Core * cll, const double & x )  // virtual from Function::Scalar
{	assert ( false );
	// we return zero just to avoid compilation errors
	return 0;           }

std::vector < double > Function::CoupledWithField::Vector::set_value_on_cell
( Cell::Core * cll, const std::vector < double > & x )  // virtual from Function::Vector
{ return this->field->on_cell(cll) = x;  }

std::vector < double > Function::Immersion::set_value_on_cell
( Cell::Core * cll, const std::vector < double > & x )  // virtual from Function::Vector
{	assert ( false );
	// we return an empty vector just to avoid compilation errors
	return  std::vector < double > ();  }

double Function::Composition::set_value_on_cell
( Cell::Core * cll, const double & x )  // virtual from Function::Scalar
{	assert ( false );
	// we return zero just to avoid compilation errors
	return 0;          }

#ifndef MANIFEM_NO_FEM

double Function::CoupledWithArray::Scalar::set_value_on_cell
( Cell::Core * cll_p, const double & x )  // virtual from Function::Scalar
{	Cell cll ( tag::whose_core_is, cll_p, tag::previously_existing, tag::surely_not_null );
	size_t i = this->numbering ( cll );
	return  ( * this->vector ) (i) = x;                                                      }
	
#endif  // ifndef MANIFEM_NO_FEM

#ifndef MANIFEM_NO_QUOTIENT

double Function::Scalar::MultiValued::set_value_on_cell
( Cell::Core * cll, const double & x )  // virtual from Function::Scalar
{	Function::Scalar * base_scalar = tag::Util::assert_cast
		< Function::Core*, Function::Scalar* > ( this->base .core );
	return  base_scalar->set_value_on_cell ( cll, x );                }

std::vector < double > Function::Vector::MultiValued::set_value_on_cell
( Cell::Core * cll, const std::vector < double > & x )  // virtual from Function::Vector
{	Function::Vector * base_vector = tag::Util::assert_cast
		< Function::Core*, Function::Vector* > ( this->base .core );
	return  base_vector->set_value_on_cell ( cll, x );                }

#endif  // ifndef MANIFEM_NO_QUOTIENT

//------------------------------------------------------------------------------------------------------//

Function maniFEM::operator+ ( const Function & f, const Function & g )

{	// both should be scalar
	assert ( dynamic_cast < Function::Scalar* > ( f .core ) );
	assert ( dynamic_cast < Function::Scalar* > ( g .core ) );

	// if one of them is zero :
	Function::Constant * f_const = dynamic_cast < Function::Constant * > ( f .core );
	if ( f_const )  if ( f_const->value == 0. )  return g;
	Function::Constant * g_const = dynamic_cast < Function::Constant * > ( g .core );
	if ( g_const )  if ( g_const->value == 0. )  return f;
	
	// if one of them is a sum, or both :
	Function::Sum * f_sum = dynamic_cast < Function::Sum * > ( f .core );
	Function::Sum * g_sum = dynamic_cast < Function::Sum * > ( g .core );

	Function::Sum * result = new Function::Sum;  // empty sum
	if ( g_sum )  // g is a sum
	{	std::forward_list<Function>::iterator it_g;
		for ( it_g = g_sum->terms .begin(); it_g != g_sum->terms .end(); it_g++ )
			result->terms .push_front ( *it_g );                                    }
	else  result->terms .push_front ( g );
	if ( f_sum )  // f is a sum
	{	std::forward_list<Function>::iterator it_f;
		for ( it_f = f_sum->terms .begin(); it_f != f_sum->terms .end(); it_f++ )
			result->terms .push_front ( *it_f );                                    }
	else  result->terms .push_front ( f );

	return Function ( tag::whose_core_is, result );                                  }

//------------------------------------------------------------------------------------------------------//


Function maniFEM::operator* ( const Function & f, const Function & g )

{	// both should be scalar
	assert ( dynamic_cast < Function::Scalar* > ( f .core ) );
	assert ( dynamic_cast < Function::Scalar* > ( g .core ) );

	// if any one of them is zero or one :
	Function::Constant * f_const = dynamic_cast < Function::Constant * > ( f .core );
	if ( f_const )
	{	if ( f_const->value == 0. ) return f;
		if ( f_const->value == 1. ) return g;
		// is g a product ?
		Function::Product * g_prod = dynamic_cast < Function::Product * > ( g .core );
		if ( g_prod )
		{	Function::Product * result = new Function::Product;  // empty product
			bool constant_not_yet_used = true;
			// we make a copy of the list of factors so to keep the original order
			std::forward_list < Function > list_fact;
			std::forward_list<Function>::iterator it_g;
			for ( it_g = g_prod->factors .begin(); it_g != g_prod->factors .end(); it_g++ )
				list_fact .push_front ( *it_g );
			for ( it_g = list_fact .begin(); it_g != list_fact .end(); it_g++ )
			{	Function fact = *it_g;
				if ( constant_not_yet_used )
				{	Function::Constant * fact_const =
						dynamic_cast < Function::Constant * > ( fact .core );
					if ( fact_const )
					{	constant_not_yet_used = false;
						result->factors .push_front ( f * fact );  }
					else result->factors .push_front ( fact );                }
				else result->factors .push_front ( fact );                      }
			if ( constant_not_yet_used ) result->factors .push_front ( f );
			return Function ( tag::whose_core_is, result );                                  }  }
	Function::Constant * g_const = dynamic_cast < Function::Constant * > ( g .core );
	if ( g_const )
	{	if ( g_const->value == 0. ) return g;
		if ( g_const->value == 1. ) return f;
		// is f a product ?
		Function::Product * f_prod = dynamic_cast < Function::Product * > ( f .core );
		if ( f_prod )
		{	Function::Product * result = new Function::Product;  // empty product
			bool constant_not_yet_used = true;
			// we make a copy of the list of factors so to keep the original order
			std::forward_list < Function > list_fact;
			std::forward_list<Function>::iterator it_f;
			for ( it_f = f_prod->factors .begin(); it_f != f_prod->factors .end(); it_f++ )
				list_fact .push_front ( *it_f );
			for ( it_f = list_fact .begin(); it_f != list_fact .end(); it_f++ )
			{	Function fact = *it_f;
				if ( constant_not_yet_used )
				{	Function::Constant * fact_const =
						dynamic_cast < Function::Constant * > ( fact .core );
					if ( fact_const )
					{	constant_not_yet_used = false;
						result->factors .push_front ( g * fact );  }
					else result->factors .push_front ( fact );                }
				else result->factors .push_front ( fact );                      }
			if ( constant_not_yet_used ) result->factors .push_front ( g );
			return Function ( tag::whose_core_is, result );                                  }  }

	// if both are constant :
	if ( f_const and g_const ) return Function ( f_const->value * g_const->value );
	
	// if one of them is a product, or both :
	Function::Product * f_prod = dynamic_cast < Function::Product * > ( f .core );
	Function::Product * g_prod = dynamic_cast < Function::Product * > ( g .core );

	Function::Product * result = new Function::Product;  // empty product
	if ( g_prod )  // g is a product
	{	std::forward_list<Function>::iterator it_g;
		for ( it_g = g_prod->factors .begin(); it_g != g_prod->factors .end(); it_g++ )
			result->factors .push_front ( *it_g );                                        }
	else  result->factors .push_front ( g );
	if ( f_prod )  // f is a product
	{	std::forward_list<Function>::iterator it_f;
		for ( it_f = f_prod->factors .begin(); it_f != f_prod->factors .end(); it_f++ )
			result->factors .push_front ( *it_f );                                        }
	else  result->factors .push_front ( f );

	return Function ( tag::whose_core_is, result );                                        }

//------------------------------------------------------------------------------------------------------//


Function maniFEM::power ( const Function & f, double e )

{	assert ( dynamic_cast < Function::Scalar* > ( f.core ) );

	if ( e == 0. ) return Function ( 1. );
	if ( e == 1. ) return f;
	if ( e == 2. ) return f*f;
	
	Function::Constant * f_const = dynamic_cast < Function::Constant * > ( f .core );
	if ( f_const )
	{	if ( f_const->value == 0. ) return Function ( 0. );
		if ( f_const->value == 1. ) return Function ( 1. );
		return Function ( pow ( f_const->value, e ) );       }

	// simplifications below are quite tempting,
	// but they lead to misterious errors
	// for instance, if we replace power(x*x,0.5) by power(x,0.5)*power(x,0.5)
	// we will get in trouble if x is negative
	
	// Function::Power * f_pow = dynamic_cast < Function::Power * > ( f .core );
	// if ( f_pow ) return power ( f_pow->base, f_pow->exponent * e );

	// Function::Product * f_prod = dynamic_cast < Function::Product * > ( f .core );
	// if ( f_prod )  // f is a product
	// { Function::Product * result = new Function::Product;  // empty product
	// 	std::forward_list<Function>::iterator it;
	// 	for ( it = f_prod->factors .begin(); it != f_prod->factors .end(); it++ )
	// 	{	Function g = *it;
	// 		result->factors.push_front ( power ( g, e ) );  }
	// 	return Function ( tag::whose_core_is, result );                              }

	return Function ( tag::whose_core_is, new Function::Power ( f, e ) );              }

//------------------------------------------------------------------------------------------------------//


Function maniFEM::power ( double f, const Function & e )

{	assert ( dynamic_cast < Function::Scalar* > ( e .core ) );

	if ( f == 0. ) return Function ( 0. );
	if ( f == 1. ) return Function ( 1. );
	
	Function::Constant * e_const = dynamic_cast < Function::Constant * > ( e .core );
	if ( e_const )
	{	if ( e_const->value == 0. ) return Function ( 1. );
		if ( e_const->value == 1. ) return Function (f);
		return Function ( pow ( f, e_const->value ) );       }

	return Function ( tag::whose_core_is, new Function::Expo ( e * std::log (f) ) );   }

//------------------------------------------------------------------------------------------------------//


Function Function::Constant::deriv ( Function )  //  virtual from Function::Core
{	return  Function ( 0. );  }

Function Function::Step::deriv ( Function x )  //  virtual from Function::Core
{	std::vector < Function > derivs;
	for ( size_t i = 0; i < this->values.size(); i++ )
		derivs.push_back ( this->values[i].deriv ( x ) );
	return  Function ( tag::whose_core_is,
	                   new Function::Step ( this->arg, derivs, this->cuts ) );  }


Function Function::Sum::deriv ( Function x )  //  virtual from Function::Core
{	std::forward_list<Function>::const_iterator it = this->terms .begin();
	Function result = 0.;
	for ( ; it != this->terms .end(); it++ )
		result += it->core->deriv(x);
	return result;                                                          }


Function Function::Product::deriv ( Function x )  //  virtual from Function::Core
{	Function result = 0.;
	std::forward_list<Function>::const_iterator it1, it2;
	size_t c1, c2;
	for ( it1 = this->factors .begin(), c1 = 0;
        it1 != this->factors .end(); it1++, c1++ )
	{ Function partial_res = 1.;
		for ( it2 = this->factors.begin(), c2 = 0;
	        it2 != this->factors.end(); it2++, c2++ )
		{	if ( c1 == c2 )  // later we can eliminate c1 and c2
			{	assert ( it1 == it2 );
				partial_res *= it2->core->deriv(x);  }
			else				
			{	assert ( it1 != it2 );
				partial_res *= *it2;    }  }
		result += partial_res;                              }
	return result;                                            }


Function Function::Power::deriv ( Function x )  //  virtual from Function::Core
{	Function result = this->exponent;
	result *= power ( this->base, this->exponent - 1. );
	result *= this->base .core->deriv (x);
	return result;                                        }

Function Function::Expo::deriv ( Function x )  //  virtual from Function::Core
{	Function result ( tag::whose_core_is, this );
	result *= this->exponent .core->deriv (x);
	return result;                                 }

Function Function::Sqrt::deriv ( Function x )  //  virtual from Function::Core
{	return  0.5 * this->base .deriv (x) / sqrt ( this->base );  }

Function Function::Sin::deriv ( Function x )  //  virtual from Function::Core
{	return  cos ( this->base ) * this->base .deriv (x);  }

Function Function::Cos::deriv ( Function x )  //  virtual from Function::Core
{	return  - sin ( this->base ) * this->base .deriv (x);  }

Function Function::CoupledWithField::Scalar::deriv ( Function x )
//  virtual from Function::Core
{	if ( this == x.core ) return Function ( 1. );
	return  Function ( 0. );                       }

#ifndef MANIFEM_NO_FEM

Function Function::MereSymbol::deriv ( Function x )  //  virtual from Function::Core
{	return  Function ( tag::whose_core_is, new Function::DelayedDerivative
										  ( Function ( tag::whose_core_is, this ), x ) );  }

Function Function::Unknown::deriv ( Function x )  //  virtual from Function::Core
{	return  Function ( tag::whose_core_is, new Function::DelayedDerivative
										  ( Function ( tag::whose_core_is, this ), x ) );  }

Function Function::Test::deriv ( Function x )  //  virtual from Function::Core
{	return  Function ( tag::whose_core_is, new Function::DelayedDerivative
										  ( Function ( tag::whose_core_is, this ), x ) );  }

Function Function::DelayedDerivative::deriv ( Function x )  //  virtual from Function::Core
{	return  Function ( tag::whose_core_is, new Function::DelayedDerivative
										  ( Function ( tag::whose_core_is, this ), x ) );  }

#endif  // ifndef MANIFEM_NO_FEM

Function Function::Vector::deriv ( Function x )  //  virtual from Function::Core
{	assert ( false ); 
	// we return a non-existent Function just to avoid compilation errors
	return  Function ( tag::non_existent );  }

Function Function::Diffeomorphism::OneDim::deriv ( Function x )
{	assert ( false );  //  virtual from Function::Core
	// we return a non-existent Function just to avoid compilation errors
	return  Function ( tag::non_existent );  }

#ifndef MANIFEM_NO_FEM

Function Function::CoupledWithArray::Scalar::deriv ( Function x )
{	assert ( false );  //  virtual from Function::Core
	// we return a non-existent Function just to avoid compilation errors
	return  Function ( tag::non_existent );  }

#endif  // ifndef MANIFEM_NO_FEM

#ifndef MANIFEM_NO_QUOTIENT

Function Function::Scalar::MultiValued::deriv ( Function x )
{	assert ( false );  //  virtual from Function::Core
	// we return a non-existent Function just to avoid compilation errors
	return  Function ( tag::non_existent );  }

Function Function::Vector::MultiValued::deriv ( Function x )
{	assert ( false );  //  virtual from Function::Core
	// we return a non-existent Function just to avoid compilation errors
	return  Function ( tag::non_existent );   }

#endif  // ifndef MANIFEM_NO_QUOTIENT

//------------------------------------------------------------------------------------------------------//


Function::Diffeomorphism::OneDim::OneDim
( const Function & gc, const Function & mc, const Function & bgc, const tag::BuildJacobian & )
	
:	Function::Scalar ( ), Function::Map ( gc, mc, bgc ), Function::Diffeomorphism ( )
// 'jacobian' initialized as empty map, 'det' as non_existent

{	assert ( dynamic_cast < Function::Scalar* > ( gc.core ) );
	assert ( dynamic_cast < Function::Scalar* > ( mc.core ) );
	assert ( dynamic_cast < Function::Scalar* > ( bgc.core ) );
	assert ( gc.number_of ( tag::components ) == 1 );
	assert ( mc.number_of ( tag::components ) == 1 );
	assert ( bgc.number_of ( tag::components ) == 1 );

	// 'det' should be good for use in integration, see e.g. Integrator::Gauss::action
	this->det = bgc.deriv(mc);

	// code below should match the conventions in Function::Composition::deriv
	this->jacobian.insert ( std::pair < Function, Function > ( gc, 1. / this->det ) );  }


Function::Immersion::Immersion
( const Function & gc, const Function & mc, const Function & bgc, const tag::BuildJacobian & )

:	Function::Immersion ( gc, mc, bgc )
// 'jacobian' initialized as empty map, 'det' as non_existent

{	assert ( dynamic_cast < Function::Vector* > ( gc.core ) );
	assert ( dynamic_cast < Function::Vector* > ( bgc.core ) );
	size_t geom_dim = gc.number_of ( tag::components );
	size_t master_dim = mc.number_of ( tag::components );
	assert ( geom_dim > master_dim );
	assert ( geom_dim == bgc.number_of ( tag::components ) );
	for ( size_t k = 0; k < master_dim; k++ )
	{	Function t = mc[k];
		Function jacob = bgc[0].deriv(t);
		for ( size_t i = 1; i < geom_dim; i++ ) jacob = jacob && bgc[i].deriv(t);
		this->jacobian.insert ( std::pair < Function, Function > ( t, jacob ) );   }
	// this->jacobian is a non-square matrix, so it makes no sense to think about its inverse
	// this is very different from Function::Diffeomorphism::HighDim !

	// 'det' should be good for use in integration, see e.g. Integrator::Gauss::action
	if ( master_dim == 1 )
	{	assert ( dynamic_cast < Function::Scalar* > ( mc.core ) );
		Function v = this->jacobian.find(mc)->second;
		Function norm_v_2 ( 0. );
		for ( size_t i = 0; i < geom_dim; i++ ) norm_v_2 += v[i]*v[i];
		this->det = sqrt ( norm_v_2 );                                  }
	else
	{	assert ( master_dim == 2 );  // sorry, no 3d for now ...
		assert ( dynamic_cast < Function::Vector* > ( mc.core ) );
		Function v = this->jacobian.find(mc[0])->second;
		Function w = this->jacobian.find(mc[1])->second;
		// det = square root of  |v|^2 |w|^2 - (v.w)^2
		Function norm_v_2 ( 0. ), norm_w_2 ( 0. ), vw ( 0. );
		for ( size_t i = 0; i < geom_dim; i++ )
		{	norm_v_2 += v[i]*v[i];
			norm_w_2 += w[i]*w[i];
			vw       += v[i]*w[i];  }
		this->det = sqrt ( norm_v_2 * norm_w_2 - vw * vw );         }                 }


Function::Diffeomorphism::HighDim::HighDim
( const Function & gc, const Function & mc, const Function & bgc, const tag::BuildJacobian & )
	
:	Function::Immersion ( gc, mc, bgc ), Function::Diffeomorphism ( )
// 'jacobian' initialized as empty map, 'det' as non_existent

{	assert ( dynamic_cast < Function::Vector* > ( gc  .core ) );
	assert ( dynamic_cast < Function::Vector* > ( mc  .core ) );
	assert ( dynamic_cast < Function::Vector* > ( bgc .core ) );
	assert ( gc .number_of ( tag::components ) == mc  .number_of ( tag::components ) );
	assert ( gc .number_of ( tag::components ) == bgc .number_of ( tag::components ) );

	if ( mc .number_of ( tag::components ) == 2 )

	{	Function dx_dxi  = bgc[0] .deriv ( mc [0] );
		Function dx_deta = bgc[0] .deriv ( mc [1] );
		Function dy_dxi  = bgc[1] .deriv ( mc [0] );
		Function dy_deta = bgc[1] .deriv ( mc [1] );

		// 'det' should be good for use in integration, see e.g. Integrator::Gauss::action
		this->det = dx_dxi * dy_deta - dx_deta * dy_dxi;
	
		// now we compute the inverse of the matrix
		// this is quite different from Function::Immersion !
		// code below should match the conventions in Function::Composition::deriv
		Function dxi_dx =   dy_deta / this->det,  deta_dx = - dy_dxi / this->det,
		         dxi_dy = - dx_deta / this->det,  deta_dy =   dx_dxi / this->det;
		this->jacobian .insert ( std::pair < Function, Function > ( gc[0], dxi_dx && deta_dx ) );
		this->jacobian .insert ( std::pair < Function, Function > ( gc[1], dxi_dy && deta_dy ) );
		assert ( this->jacobian .find ( geom_coords [0] ) != this->jacobian .end() );
		assert ( this->jacobian .find ( geom_coords [1] ) != this->jacobian .end() );
		return;                                                                                   }

	assert ( mc .number_of ( tag::components ) == 3 );
			
	Function dx_dxi   = bgc[0] .deriv ( mc [0] );
	Function dx_deta  = bgc[0] .deriv ( mc [1] );
	Function dx_dzeta = bgc[0] .deriv ( mc [2] );
	Function dy_dxi   = bgc[1] .deriv ( mc [0] );
	Function dy_deta  = bgc[1] .deriv ( mc [1] );
	Function dy_dzeta = bgc[1] .deriv ( mc [2] );
	Function dz_dxi   = bgc[2] .deriv ( mc [0] );
	Function dz_deta  = bgc[2] .deriv ( mc [1] );
	Function dz_dzeta = bgc[2] .deriv ( mc [2] );

	// 'det' should be good for use in integration, see e.g. Integrator::Gauss::action
	this->det =   dx_dxi * dy_deta * dz_dzeta + dx_deta * dy_dzeta * dz_dxi
	            + dx_dzeta * dy_dxi * dz_deta - dx_dzeta * dy_deta * dz_dxi
	            - dx_deta * dy_dxi * dz_dzeta - dx_dxi * dy_dzeta * dz_deta;

	// now we compute the inverse of the matrix
	// this is quite different from Function::Immersion !
	// code below should match the conventions in Function::Composition::deriv
	Function dxi_dx   = ( dy_deta * dz_dzeta - dy_dzeta * dz_deta ) / this->det,
	         dxi_dy   = ( dx_dzeta * dz_deta - dx_deta * dz_dzeta ) / this->det,
	         dxi_dz   = ( dx_deta * dy_dzeta - dx_dzeta * dy_deta ) / this->det,
	         // dxi_dx * dx_dxi + dxi_dy * dy_dxi + dxi_dz * dz_dxi = 1
	         // dxi_dx * dx_deta + dxi_dy * dy_deta + dxi_dz * dz_deta = 0
	         // dxi_dx * dx_dzeta + dxi_dy * dy_dzeta + dxi_dz * dz_dzeta = 0
	         deta_dx  = ( dy_dzeta * dz_dxi - dy_dxi * dz_dzeta ) / this->det,
	         deta_dy  = ( dx_dxi * dz_dzeta - dx_dzeta * dz_dxi ) / this->det,
	         deta_dz  = ( dx_dzeta * dy_dxi - dx_dxi * dy_dzeta ) / this->det,
	         // deta_dx * dx_dxi + deta_dy * dy_dxi + deta_dz * dz_dxi = 0
	         // deta_dx * dx_deta + deta_dy * dy_deta + deta_dz * dz_deta = 1
	         // deta_dx * dx_dzeta + deta_dy * dy_dzeta + deta_dz * dz_dzeta = 0
	         dzeta_dx = ( dy_dxi * dz_deta - dy_deta * dz_dxi ) / this->det,
	         dzeta_dy = ( dx_deta * dz_dxi - dx_dxi * dz_deta ) / this->det,
	         dzeta_dz = ( dx_dxi * dy_deta - dx_deta * dy_dxi ) / this->det;
	         // dzeta_dx * dx_dxi + dzeta_dy * dy_dxi + dzeta_dz * dz_dxi = 0
	         // dzeta_dx * dx_deta + dzeta_dy * dy_deta + dzeta_dz * dz_deta = 0
	         // dzeta_dx * dx_dzeta + dzeta_dy * dy_dzeta + dzeta_dz * dz_dzeta = 1

	this->jacobian .insert ( std::pair < Function, Function > ( gc[0], dxi_dx && deta_dx && dzeta_dx ) );
	this->jacobian .insert ( std::pair < Function, Function > ( gc[1], dxi_dy && deta_dy && dzeta_dy ) );
	this->jacobian .insert ( std::pair < Function, Function > ( gc[2], dxi_dz && deta_dz && dzeta_dz ) );
	assert ( this->jacobian .find ( geom_coords [0] ) != this->jacobian .end() );
	assert ( this->jacobian .find ( geom_coords [1] ) != this->jacobian .end() );
	assert ( this->jacobian .find ( geom_coords [2] ) != this->jacobian .end() );

}  // end of  Function::Diffeomorphism::HighDim::HighDim

//------------------------------------------------------------------------------------------------------//


Function Function::Composition::deriv ( Function x )
	
// there are two kinds of derivatives for a Function::Composition
// if x is a master coordinate, then we return the usual arithmetic derivative
// if x is a geometric coordinate, we apply the chain rule

// in both cases, we compose the result just before return
// since the calling code may differentiate the result again
	
{	Function::Map * transf_c = dynamic_cast < Function::Map* > ( this->transf.core );
	assert ( transf_c );
	// in the above we must use dynamic_cast
	// below, with -DNDEBUG, assert_cast calls a static_cast which does not compile
	// Function::Map * transf_c = tag::Util::assert_cast
	// 	< Function::Core*, Function::Map* > ( this->transf.core );
	// classes Function::Core and Function::Map are not directly related
	// perhaps we should have used virtual inheritance ?

	// we have a map, which may be a Function::Diffeomorphism
	// or a Function::Immersion
	// in the latter case we cannot differentiate wrt a geometric coordinate
	// only with respect to a master coordinate
	
	if ( dynamic_cast < Function::Diffeomorphism* > ( this->transf.core ) )

	{	// 'this' may be Function::Diffeomorphism::OneDim or Function::Diffeomorphism::HighDim
		// in either case, transf_c->jacobian derivatives of master coordinates
		// with respect to geometric coordinates

		std::map<Function,Function>::const_iterator it = transf_c->jacobian.find(x);
		if ( it != transf_c->jacobian.end() )
			// x is a geometric coordinate, so we apply the chain rule
		{	const Function & jacob = it->second;
			// 'jacob' contains derivatives of the master coordinates with respect to x
			Function result = 0.;
			for ( size_t i = 0; i < transf_c->master_coords.number_of ( tag::components ); i++ )
				result += this->base.deriv(transf_c->master_coords[i]) * jacob[i];
			return Function ( result, tag::composed_with, this->transf );              }
		else  // x is a master coordinate
			return Function ( this->base .deriv (x), tag::composed_with, this->transf );  }

	else  //  not a Diffeomorphism, but an Immersion
		
	{	assert ( dynamic_cast < Function::Immersion* > ( this->transf.core ) );
		#ifndef NDEBUG  // DEBUG mode
		std::map<Function,Function>::const_iterator
			it = transf_c->jacobian.find(x);
		assert ( it != transf_c->jacobian.end() );  // x must be a master coordinate
		#endif  // DEBUG
		return  Function ( this->base .deriv (x), tag::composed_with, this->transf );  }
	
}  // end of  Function::Composition::deriv	

//------------------------------------------------------------------------------------------------------//


Function Function::Constant::replace ( const Function & x, const Function & y )
//  virtual from Function::Core
{	return Function ( tag::whose_core_is, this );  }

Function Function::Sum::replace ( const Function & x, const Function & y )
//  virtual from Function::Core
{	if ( this == x .core ) return y;
	std::forward_list<Function>::const_iterator it = this->terms .begin();
	Function result = 0.;
	for ( ; it != this->terms .end(); it++ )
	{ result += it->replace ( x, y );          }
	return result;                                                         }

Function Function::Product::replace ( const Function & x, const Function & y )
//  virtual from Function::Core
{	if ( this == x .core ) return y;
	std::forward_list<Function>::const_iterator it = this->factors .begin();
	Function result = 1.;
	for ( ; it != this->factors .end(); it++ ) result *= it->replace ( x, y );
	return result;                                                             }

Function Function::Power::replace ( const Function & x, const Function & y )
//  virtual from Function::Core
{	if ( this == x .core ) return y;
	return  power ( this->base .replace ( x, y ), this->exponent );  }

Function Function::Expo::replace ( const Function & x, const Function & y )
//  virtual from Function::Core
{	if ( this == x .core ) return y;
	return  power ( std::numbers::e, this->exponent .replace ( x, y ) );  }

Function Function::Sqrt::replace ( const Function & x, const Function & y )
//  virtual from Function::Core
{	if ( this == x .core ) return y;
	return  sqrt ( this->base .replace ( x, y ) );  }

Function Function::Sin::replace ( const Function & x, const Function & y )
//  virtual from Function::Core
{	if ( this == x .core ) return y;
	return  sin ( this->base .replace ( x, y ) );  }

Function Function::Cos::replace ( const Function & x, const Function & y )
//  virtual from Function::Core
{	if ( this == x .core ) return y;
	return  cos ( this->base .replace ( x, y ) );  }

Function Function::Step::replace ( const Function & x, const Function & y )
{	assert ( false );  //  virtual from Function::Core
	// we return a non-existent Function just to avoid compilation errors
	return  Function ( tag::non_existent );  }

Function Function::Vector::replace ( const Function & x, const Function & y )
{	assert ( false );  //  virtual from Function::Core
	// we return a non-existent Function just to avoid compilation errors
	return  Function ( tag::non_existent );  }

Function Function::CoupledWithField::Scalar::replace
( const Function & x, const Function & y )
//  virtual from Function::Core
{	if ( this == x .core ) return y;
	return  Function ( tag::whose_core_is, this );  }

#ifndef MANIFEM_NO_FEM

Function Function::CoupledWithArray::Scalar::replace ( const Function & x, const Function & y )
{	assert ( false );  //  virtual from Function::Core
	// we return a non-existent Function just to avoid compilation errors
	return  Function ( tag::non_existent );  }

#endif  // ifndef MANIFEM_NO_FEM

#ifndef MANIFEM_NO_FEM

Function Function::MereSymbol::replace ( const Function & x, const Function & y )
//  virtual from Function::Core
{	if ( this == x.core ) return y;
	return  Function ( tag::whose_core_is, this );  }

Function Function::Unknown::replace ( const Function & x, const Function & y )
//  virtual from Function::Core
{	if ( this == x.core ) return y;
	return  Function ( tag::whose_core_is, this );  }

Function Function::Test::replace ( const Function & x, const Function & y )
//  virtual from Function::Core
{	if ( this == x.core ) return y;
	return  Function ( tag::whose_core_is, this );  }

Function Function::DelayedDerivative::replace ( const Function & x, const Function & y )
//  virtual from Function::Core
{	return  this->base .replace ( x, y ) .deriv ( this->variable );  }

#endif  // ifndef MANIFEM_NO_FEM

Function Function::CoupledWithField::Vector::replace
( const Function & x, const Function & y )  //  virtual from Function::Core
{	assert ( false );
	// we return a non-existent Function just to avoid compilation errors
	return  Function ( tag::non_existent );  }

Function Function::Diffeomorphism::OneDim::replace
( const Function & x, const Function & y )  //  virtual from Function::Vector
{	assert ( false );
	// we return a non-existent Function just to avoid compilation errors
	return  Function ( tag::non_existent );  }

Function Function::Composition::replace ( const Function & x, const Function & y )
//  virtual from Function::Core
{	return  Function ( tag::whose_core_is, this );  }

#ifndef MANIFEM_NO_QUOTIENT

Function Function::Scalar::MultiValued::replace ( const Function & x, const Function & y )
{	assert ( false );   //  virtual from Function::Scalar
	// we return a non-existent Function just to avoid compilation errors
	return  Function ( tag::non_existent );  }

Function Function::Vector::MultiValued::replace ( const Function & x, const Function & y )
{	assert ( false );   //  virtual from Function::Vector
	// we return a non-existent Function just to avoid compilation errors
	return  Function ( tag::non_existent );  }

#endif  // ifndef MANIFEM_NO_QUOTIENT

//------------------------------------------------------------------------------------------------------//


#ifndef NDEBUG  // DEBUG mode	

std::string Function::Constant::repr ( const Function::From & from ) const
// virtual from Function::Core
{	std::stringstream ss;
	ss << this->value; 
	std::string s = ss .str();
	if ( ( this->value < 0. ) and ( from != Function::from_void ) ) s = "(" + s + ")";
	return s;                                                                           }

std::string Function::Sum::repr ( const Function::From & from ) const
// virtual from Function::Core
{ std::forward_list<Function>::const_iterator it = this->terms .begin();
	assert ( it != terms .end() );
	std::string s = it->core->repr ( Function::from_sum );
	for ( it++; it != terms .end(); it++ )
		s = s + '+' + it->core->repr ( Function::from_sum );
	if ( ( from == Function::from_power ) or ( from == Function::from_product )
       or ( from == Function::from_function ) )  s = "(" + s + ")";
	return s;                                                                    }

std::string Function::Product::repr ( const Function::From & from ) const
// virtual from Function::Core
{ std::forward_list<Function>::const_iterator it = this->factors.begin();
	assert ( it != factors .end() );
	std::string s = it->core->repr ( Function::from_product );
	for ( it++; it != factors .end(); it++ )
		s = s + '*' + it->core->repr ( Function::from_product );
	if ( ( from == Function::from_power ) or ( from == Function::from_function ) )
		s = "(" + s + ")";
	return s;                                                                       }

std::string Function::Power::repr ( const Function::From & from ) const
// virtual from Function::Core
{	std::string s = this->base .core->repr ( Function::from_power ) + "^";
	std::stringstream ss;
	if ( exponent >= 0. )  ss << exponent;
	else  ss << "(" << exponent << ")";
	return s + ss .str();                                                   }

std::string Function::Expo::repr ( const Function::From & from ) const
// virtual from Function::Core
{	std::stringstream s;
	s << "e^" << this->exponent .core->repr ( Function::from_power );
	return s .str();                                                   }

std::string Function::Sqrt::repr ( const Function::From & from ) const
// virtual from Function::Core
{	return "sqrt" + this->base .core->repr ( Function::from_function );  }

std::string Function::Sin::repr ( const Function::From & from ) const
// virtual from Function::Core
{	return "sin" + this->base .core->repr ( Function::from_function );  }

std::string Function::Cos::repr ( const Function::From & from ) const
// virtual from Function::Core
{	return "cos" + this->base .core->repr ( Function::from_function );  }

std::string Function::Step::repr ( const Function::From & from ) const
{	return "step";  }  // virtual from Function::Core

#ifndef MANIFEM_NO_FEM

std::string Function::MereSymbol::repr ( const Function::From & from ) const
{	return "symbol";  }  // virtual from Function::Core

std::string Function::Unknown::repr ( const Function::From & from ) const
{	return "symbolic unknown";  }  // virtual from Function::Core

std::string Function::Test::repr ( const Function::From & from ) const
{	return "symbolic test";  }  // virtual from Function::Core

std::string Function::DelayedDerivative::repr ( const Function::From & from ) const
// virtual from Function::Core
{	return this->base .core->repr ( Function::from_product ) + "," +
	       this->variable .core->repr ( Function::from_product );     }

#endif  // ifndef MANIFEM_NO_FEM

std::string Function::Vector::repr ( const Function::From & from ) const
{	return "vector";  }  // virtual from Function::Core

std::string Function::CoupledWithField::Scalar::repr ( const Function::From & from ) const
// virtual from Function::Core
{	if ( Function::name .find (this) != Function::name .end() )
		return Function::name[this];
	std::stringstream ss;
	ss << this->field;
	return "scalar" + ss .str();                                 }

std::string Function::Composition::repr ( const Function::From & from ) const
// virtual from Function::Core
{	return this->base .core->repr ( Function::from_product ) + "º";  }

std::string Function::Diffeomorphism::OneDim::repr ( const Function::From & from ) const
{	assert ( false );  }  // virtual from Function::Core

#ifndef MANIFEM_NO_FEM

std::string Function::CoupledWithArray::Scalar::repr ( const Function::From & from ) const
{	assert ( false );  }  // virtual from Function::Core

#endif  // ifndef MANIFEM_NO_FEM

#ifndef MANIFEM_NO_QUOTIENT

std::string Function::Scalar::MultiValued::repr ( const Function::From & from ) const
// virtual from Function::Core
{	return "multi" + this->base .core->repr ( Function::from_function );  }

std::string Function::Vector::MultiValued::repr ( const Function::From & from ) const
// virtual from Function::Core
{	assert ( false );  }

#endif  // ifndef MANIFEM_NO_QUOTIENT

#endif // DEBUG

//------------------------------------------------------------------------------------------------------//


#ifndef MANIFEM_NO_QUOTIENT

Function::Jump Function::Core::jump ( )
// later overridden by Function::***::MultiValued::JumpIsSum
{	assert ( false );  // we return some Jump just to avoid compilation errors
	return Function::Jump ();  }


Function::Jump Function::Scalar::MultiValued::JumpIsSum::jump ( )
{	Function::Jump res;
	res .actions = this->actions;
	res .ju = this->beta;
	return res;                    }


Function::Jump Function::Vector::MultiValued::JumpIsSum::jump ( )
{	assert ( false );   // we return some Jump just to avoid compilation errors
	return Function::Jump ();  }


Function::Jump maniFEM::operator+ ( const Function::Jump & j1, const Function::Jump & j2 )

{	const size_t n = j1 .actions .size();
	assert ( n > 0 );
	assert ( j1 .actions == j2 .actions );
	Function::Jump res;
	res .actions = j1 .actions;
	res .ju = j1 .ju;
	assert ( n == j1 .ju .size() );
	assert ( n == j2 .ju .size() );
	for ( size_t i = 0; i < n; i++ ) res .ju [i] += j2 .ju [i];
	return res;                                                  }


Function::Jump maniFEM::operator+= ( Function::Jump & j1, const Function::Jump & j2 )

{	const size_t n = j1 .actions .size();
	assert ( n > 0 );
	assert ( j1 .actions == j2 .actions );
	assert ( n == j1 .ju .size() );
	assert ( n == j2 .ju .size() );
	for ( size_t i = 0; i < n; i++ ) j1 .ju [i] += j2 .ju [i];
	return j1;                                                  }  


Function::Jump maniFEM::operator- ( const Function::Jump & j1, const Function::Jump & j2 )

{	const size_t n = j1 .actions .size();
	assert ( n > 0 );
	assert ( j1 .actions == j2 .actions );
	Function::Jump res;
	res .actions = j1 .actions;
	res .ju = j1 .ju;
	assert ( n == j1 .ju .size() );
	assert ( n == j2 .ju .size() );
	for ( size_t i = 0; i < n; i++ ) res .ju [i] -= j2 .ju [i];
	return res;                                                  }


Function::Jump maniFEM::operator-= ( Function::Jump & j1, const Function::Jump & j2 )

{	const size_t n = j1 .actions .size();
	assert ( n > 0 );
	assert ( j1 .actions == j2 .actions );
	assert ( n == j1 .ju .size() );
	assert ( n == j2 .ju .size() );
	for ( size_t i = 0; i < n; i++ ) j1 .ju [i] -= j2 .ju [i];
	return j1;                                                  }  


Function::Jump maniFEM::operator* ( double a, const Function::Jump & jj )

{	size_t n = jj.actions.size();
	assert ( n > 0 );
	Function::Jump res;
	res .actions = jj .actions;
	res .ju = jj .ju;
	assert ( n == jj .ju .size() );
	for ( size_t i = 0; i < n; i++ ) res .ju[i] *= a;
	return res;                                        }


Function::Jump maniFEM::operator*= ( Function::Jump & jj, double a )

{	size_t n = jj .actions .size();
	assert ( n > 0 );
	assert ( n == jj .ju .size() );
	for ( size_t i = 0; i < n; i++ ) jj .ju[i] *= a;
	return jj;                                        }


double Function::Jump::operator() ( const Function::Action & a ) const

{	double res = 0.;
	const size_t n = this->ju .size();
	assert ( n == this->actions .size() );
	size_t counter = 0;
	for ( size_t i = 0; i < n; i++ )
	{	const Function::ActionGenerator & g = this->actions[i];
		std::map < Function::ActionGenerator, short int > ::const_iterator
			it = a .index_map .find ( g );
		if ( it == a .index_map .end() ) continue;
		counter ++;
		res += this->ju[i] * it->second;                                    }
	assert ( counter == a .index_map .size() );
	return res;                                                               }

//------------------------------------------------------------------------------------------------------//


std::pair < std::vector < double >, double >  // static
Function::Scalar::MultiValued::JumpIsLinear::analyse_linear_expression
( Function expression, Function base )

// 'base' is a set of variables, say X
// 'expression' is an affine expression in X  ( a1 x1 + a2 x2 + ... + b )
// we want to identify ai and b

{	size_t n_coord = base .number_of ( tag::components );
	assert ( expression .number_of ( tag::components ) == 1 );
	std::pair < std::vector < double >, double > res
		{ std::vector < double > ( n_coord, 0. ), 0. };
	
	// often, 'expression' will be a sum
	Function::Sum * expr_sum = dynamic_cast < Function::Sum* > ( expression .core );
	if ( expr_sum )
	{	std::forward_list < Function > tl = expr_sum->terms;
		for ( std::forward_list < Function > ::const_iterator
		      it = tl .begin(); it != tl .end(); it++         )
		{	Function term = *it;
			std::pair < std::vector < double >, double > local_res =
				Function::Scalar::MultiValued::JumpIsLinear::analyse_linear_expression ( term, base );
			for ( size_t i = 0; i < n_coord; i++ )
				res .first [i] += local_res .first [i];
			res .second += local_res .second;                                                          }
		return  res;                                                                                     }

	// expression is not a sum
	// could be a product
	Function::Product * expr_prod = dynamic_cast < Function::Product* > ( expression .core );
	if ( expr_prod )  // we assume this product has the form  constant * other_expression
	{	std::forward_list < Function > fl = expr_prod->factors;
		std::forward_list < Function > ::const_iterator itt = fl .begin();
		assert ( itt != fl .end() );
		Function first_factor = *itt;
		Function::Constant * cc =
			tag::Util::assert_cast < Function::Core*, Function::Constant* > ( first_factor .core );
		itt++;
		assert ( itt != fl .end() );
		Function second_factor = *itt;
		itt++;
		assert ( itt == fl .end() );
		std::pair < std::vector < double >, double > local_res =
			Function::Scalar::MultiValued::JumpIsLinear::analyse_linear_expression
				( second_factor, base );
		for ( size_t i = 0; i < n_coord; i++ )
			res .first [i] += cc->value * local_res .first [i];
		res .second = cc->value * local_res .second;
		return  res;                                                                                }

	// expression is not a sum, not a product
	// could be a constant
	Function::Constant * cc = dynamic_cast < Function::Constant* > ( expression .core );
	if ( cc )
	{	res .second = cc->value;
		return res;               }
	
	// expression is not a sum, not a product, not a constant
	// must be a variable xi, otherwise return a zero-length vector 'res .first'
	bool found_var = false;
	size_t i = 0;
	for ( ; i < n_coord; i++ )
		if ( base [i] .core == expression .core )
		{	found_var = true;  break;  }
	if ( found_var )
		res .first [i] = 1.;
	else
		res .first .resize ( 0 );
	return res;
	
}  // end of Function::Scalar::MultiValued::JumpIsLinear::analyse_linear_expression

#endif  // ifndef MANIFEM_NO_QUOTIENT

//------------------------------------------------------------------------------------------------------//


Function::Core::~Core ( )  // virtual
{	assert ( Function::total_cores > 0 );
	Function::total_cores--;               }

//------------------------------------------------------------------------------------------------------//


