
//   iterator.cpp  2025.01.05

//   This file is part of maniFEM, a C++ library for meshes and finite elements on manifolds.

//   Copyright  2019 - 2025  Cristian Barbarosie  cristian.barbarosie@gmail.com

//   https://maniFEM.rd.ciencias.ulisboa.pt/
//   https://codeberg.org/cristian.barbarosie/maniFEM

//   ManiFEM is free software: you can redistribute it and/or modify it
//   under the terms of the GNU Lesser General Public License as published
//   by the Free Software Foundation, either version 3 of the License
//   or (at your option) any later version.

//   ManiFEM is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//   See the GNU Lesser General Public License for more details.

//   You should have received a copy of the GNU Lesser General Public License
//   along with maniFEM.  If not, see <https://www.gnu.org/licenses/>.


#include "mesh.h"
#include "iterator.h"
#include "iterator-xx.h"
#include "function.h"

using namespace maniFEM;


Mesh::Iterator::Core * Mesh::ZeroDim::iterator  // virtual from Mesh::Core
( const tag::OverVertices &, const tag::AsFound &, const tag::ThisMeshIsPositive & )
// a positive zero-dimensional mesh is the boundary of a positive segment	
// iterate over the two vertices, first base (negative) then tip (positive)
{	assert ( this->cell_enclosed );
	Cell::PositiveSegment * seg = tag::Util::assert_cast
	  < Cell::Positive*, Cell::PositiveSegment* > ( this->cell_enclosed );
	return  new Mesh::Iterator::Over::TwoVerticesOfSeg::NormalOrder::AsFound ( seg );  }


Mesh::Iterator::Core * Mesh::ZeroDim::iterator  // virtual from Mesh::Core
( const tag::OverVertices &, const tag::ForcePositive &, const tag::ThisMeshIsPositive & )
// a positive zero-dimensional mesh is the boundary of a positive segment	
// iterate over the two vertices, first base then tip (both positive)
{	assert ( this->cell_enclosed );
	Cell::PositiveSegment * seg = tag::Util::assert_cast
	  < Cell::Positive*, Cell::PositiveSegment* > ( this->cell_enclosed );
	return  new Mesh::Iterator::Over::TwoVerticesOfSeg::NormalOrder::ForcePositive ( seg );  }


Mesh::Iterator::Core * Mesh::ZeroDim::iterator  // virtual from Mesh::Core
( const tag::OverVertices &, const tag::ThisMeshIsPositive & )
// a positive zero-dimensional mesh is the boundary of a positive segment	
{	std::cout << "when iterating over the two extremities of a segment," << std::endl;
	std::cout << "please provide tag::orientation_compatible_with_mesh or tag::force_positive"
	          << std::endl;
	exit (1);                                                                                  }


Mesh::Iterator::Core * Mesh::ZeroDim::iterator  // virtual from Mesh::Core
( const tag::OverVertices &, const tag::OrientCompWithMesh &, const tag::ThisMeshIsPositive & )
// a positive zero-dimensional mesh is the boundary of a positive segment	
// iterate over the two vertices, first base (negative) then tip (positive)
{	assert ( this->cell_enclosed );
	Cell::PositiveSegment * seg = tag::Util::assert_cast
	  < Cell::Positive*, Cell::PositiveSegment* > ( this->cell_enclosed );
	return  new Mesh::Iterator::Over::TwoVerticesOfSeg::NormalOrder::AsFound ( seg );  }
// change name of the above class into OrientCompatWithMesh !


Mesh::Iterator::Core * Mesh::ZeroDim::iterator  // virtual from Mesh::Core
( const tag::OverVertices &, const tag::OrientOpposToMesh &, const tag::ThisMeshIsPositive & )
// a positive zero-dimensional mesh is the boundary of a positive segment	
// iterate over the two vertices, first base (positive) then tip (negative)
{	assert ( this->cell_enclosed );
	assert ( false );  // to implement
	Cell::PositiveSegment * seg = tag::Util::assert_cast
	  < Cell::Positive*, Cell::PositiveSegment* > ( this->cell_enclosed );
	return  new Mesh::Iterator::Over::TwoVerticesOfSeg::NormalOrder::AsFound ( seg );  }


Mesh::Iterator::Core * Mesh::ZeroDim::iterator  // virtual from Mesh::Core
( const tag::OverVertices &, const tag::AsFound &,
  const tag::RequireOrder &, const tag::ThisMeshIsPositive & )
// a positive zero-dimensional mesh is the boundary of a positive segment	
// iterate over the two vertices, first base (negative) then tip (positive)
{	assert ( this->cell_enclosed );
	Cell::PositiveSegment * seg = tag::Util::assert_cast
		< Cell::Positive*, Cell::PositiveSegment* > ( this->cell_enclosed );
	return  new Mesh::Iterator::Over::TwoVerticesOfSeg::NormalOrder::AsFound ( seg );  }


Mesh::Iterator::Core * Mesh::ZeroDim::iterator  // virtual from Mesh::Core
( const tag::OverVertices &, const tag::ForcePositive &,
  const tag::RequireOrder &, const tag::ThisMeshIsPositive & )
// a positive zero-dimensional mesh is the boundary of a positive segment	
// iterate over the two vertices, first base then tip (both positive)
{	assert ( this->cell_enclosed );
	Cell::PositiveSegment * seg = tag::Util::assert_cast
		< Cell::Positive*, Cell::PositiveSegment* > ( this->cell_enclosed );
	return  new Mesh::Iterator::Over::TwoVerticesOfSeg::NormalOrder::ForcePositive ( seg );  }


Mesh::Iterator::Core * Mesh::ZeroDim::iterator  // virtual from Mesh::Core
( const tag::OverVertices &, const tag::RequireOrder &, const tag::ThisMeshIsPositive & )
// a positive zero-dimensional mesh is the boundary of a positive segment	
{	std::cout << "when iterating over the two extremities of a segment," << std::endl;
	std::cout << "please provide tag::orientation_compatible_with_mesh or tag::force_positive"
	          << std::endl;
	exit (1);                                                                                  }


Mesh::Iterator::Core * Mesh::ZeroDim::iterator  // virtual from Mesh::Core
( const tag::OverVertices &, const tag::OrientCompWithMesh &,
  const tag::RequireOrder &, const tag::ThisMeshIsPositive & )
// a positive zero-dimensional mesh is the boundary of a positive segment	
// iterate over the two vertices, first base (negative) then tip (positive)
{	assert ( this->cell_enclosed );
	Cell::PositiveSegment * seg = tag::Util::assert_cast
		< Cell::Positive*, Cell::PositiveSegment* > ( this->cell_enclosed );
	return  new Mesh::Iterator::Over::TwoVerticesOfSeg::NormalOrder::AsFound ( seg );  }


Mesh::Iterator::Core * Mesh::ZeroDim::iterator  // virtual from Mesh::Core
( const tag::OverVertices &, const tag::OrientOpposToMesh &,
  const tag::RequireOrder &, const tag::ThisMeshIsPositive & )
// a positive zero-dimensional mesh is the boundary of a positive segment	
// iterate over the two vertices, first base (positive) then tip (negative)
{	assert ( this->cell_enclosed );
	assert ( false );  // to implement
	Cell::PositiveSegment * seg = tag::Util::assert_cast
		< Cell::Positive*, Cell::PositiveSegment* > ( this->cell_enclosed );
	return  new Mesh::Iterator::Over::TwoVerticesOfSeg::NormalOrder::AsFound ( seg );  }


Mesh::Iterator::Core * Mesh::ZeroDim::iterator  // virtual from Mesh::Core
( const tag::OverVertices &, const tag::AsFound &,
  const tag::Backwards &, const tag::ThisMeshIsPositive & )
// a positive zero-dimensional mesh is the boundary of a positive segment	
// iterate over the two vertices, first tip (positive) then base (negative)
{	assert ( this->cell_enclosed );
	Cell::PositiveSegment * seg = tag::Util::assert_cast
		< Cell::Positive*, Cell::PositiveSegment* > ( this->cell_enclosed );
	return  new Mesh::Iterator::Over::TwoVerticesOfSeg::ReverseOrder::AsFound ( seg );  }


Mesh::Iterator::Core * Mesh::ZeroDim::iterator  // virtual from Mesh::Core
( const tag::OverVertices &, const tag::ForcePositive &,
  const tag::Backwards &, const tag::ThisMeshIsPositive & )
// a positive zero-dimensional mesh is the boundary of a positive segment	
// iterate over the two vertices, first tip then base (both positive)
{	assert ( this->cell_enclosed );
	Cell::PositiveSegment * seg = tag::Util::assert_cast
		< Cell::Positive*, Cell::PositiveSegment* > ( this->cell_enclosed );
	return  new Mesh::Iterator::Over::TwoVerticesOfSeg::ReverseOrder::ForcePositive ( seg );  }


Mesh::Iterator::Core * Mesh::ZeroDim::iterator  // virtual from Mesh::Core
( const tag::OverVertices &, const tag::Backwards &, const tag::ThisMeshIsPositive & )
// a positive zero-dimensional mesh is the boundary of a positive segment	
{	std::cout << "when iterating over the two extremities of a segment," << std::endl;
	std::cout << "please provide tag::orientation_compatible_with_mesh or tag::force_positive"
	          << std::endl;
	exit (1);                                                                                  }


Mesh::Iterator::Core * Mesh::ZeroDim::iterator  // virtual from Mesh::Core
( const tag::OverVertices &, const tag::OrientCompWithMesh &,
  const tag::Backwards &, const tag::ThisMeshIsPositive & )
// a positive zero-dimensional mesh is the boundary of a positive segment	
// iterate over the two vertices, first tip (positive) then base (negative)
{	assert ( this->cell_enclosed );
	Cell::PositiveSegment * seg = tag::Util::assert_cast
		< Cell::Positive*, Cell::PositiveSegment* > ( this->cell_enclosed );
	return  new Mesh::Iterator::Over::TwoVerticesOfSeg::ReverseOrder::AsFound ( seg );  }


Mesh::Iterator::Core * Mesh::ZeroDim::iterator  // virtual from Mesh::Core
( const tag::OverVertices &, const tag::OrientOpposToMesh &,
  const tag::Backwards &, const tag::ThisMeshIsPositive & )
// a positive zero-dimensional mesh is the boundary of a positive segment	
// iterate over the two vertices, first tip (negative) then base (positive)
{	assert ( this->cell_enclosed );
	assert ( false );  // to implement
	Cell::PositiveSegment * seg = tag::Util::assert_cast
		< Cell::Positive*, Cell::PositiveSegment* > ( this->cell_enclosed );
	return  new Mesh::Iterator::Over::TwoVerticesOfSeg::ReverseOrder::ForcePositive ( seg );  }


//------------------------------------------------------------------------------------------------------//


Mesh::Iterator::Core * Mesh::ZeroDim::iterator  // virtual from Mesh::Core
( const tag::OverSegments &, const tag::AsFound &, const tag::ThisMeshIsPositive & )
{	std::cout << __FILE__ << ":" << __LINE__ << ": " << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "Zero-dimensional meshes have no segments." << std::endl;
	exit (1);                                                               }


Mesh::Iterator::Core * Mesh::ZeroDim::iterator  // virtual from Mesh::Core
( const tag::OverSegments &, const tag::ForcePositive &, const tag::ThisMeshIsPositive & )
{	std::cout << __FILE__ << ":" << __LINE__ << ": " << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "Zero-dimensional meshes have no segments." << std::endl;
	exit (1);                                                                                     }


Mesh::Iterator::Core * Mesh::ZeroDim::iterator  // virtual from Mesh::Core
( const tag::OverSegments &, const tag::ThisMeshIsPositive & )
{	std::cout << __FILE__ << ":" << __LINE__ << ": " << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "Zero-dimensional meshes have no segments." << std::endl;
	exit (1);                                                                                     }


Mesh::Iterator::Core * Mesh::ZeroDim::iterator  // virtual from Mesh::Core
( const tag::OverSegments &, const tag::OrientCompWithMesh &, const tag::ThisMeshIsPositive & )
{	std::cout << __FILE__ << ":" << __LINE__ << ": " << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "Zero-dimensional meshes have no segments." << std::endl;
	exit (1);                                                                                      }


Mesh::Iterator::Core * Mesh::ZeroDim::iterator  // virtual from Mesh::Core
( const tag::OverSegments &, const tag::OrientOpposToMesh &, const tag::ThisMeshIsPositive & )
{	std::cout << __FILE__ << ":" << __LINE__ << ": " << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "Zero-dimensional meshes have no segments." << std::endl;
	exit (1);                                                                                      }


Mesh::Iterator::Core * Mesh::ZeroDim::iterator  // virtual from Mesh::Core
( const tag::OverSegments &, const tag::AsFound &,
  const tag::RequireOrder &, const tag::ThisMeshIsPositive & )
{	std::cout << __FILE__ << ":" << __LINE__ << ": " << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "Zero-dimensional meshes have no segments." << std::endl;
	exit (1);                                                                                      }


Mesh::Iterator::Core * Mesh::ZeroDim::iterator  // virtual from Mesh::Core
( const tag::OverSegments &, const tag::ForcePositive &,
  const tag::RequireOrder &, const tag::ThisMeshIsPositive & )
{	std::cout << __FILE__ << ":" << __LINE__ << ": " << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "Zero-dimensional meshes have no segments." << std::endl;
	exit (1);                                                                                      }


Mesh::Iterator::Core * Mesh::ZeroDim::iterator  // virtual from Mesh::Core
( const tag::OverSegments &, const tag::RequireOrder &, const tag::ThisMeshIsPositive & )
{	std::cout << __FILE__ << ":" << __LINE__ << ": "  << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "Zero-dimensional meshes have no segments." << std::endl;
	exit (1);                                                                                       }


Mesh::Iterator::Core * Mesh::ZeroDim::iterator  // virtual from Mesh::Core
( const tag::OverSegments &, const tag::OrientCompWithMesh &,
  const tag::RequireOrder &, const tag::ThisMeshIsPositive & )
{	std::cout << __FILE__ << ":" << __LINE__ << ": " << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "Zero-dimensional meshes have no segments." << std::endl;
	exit (1);                                                                                      }


Mesh::Iterator::Core * Mesh::ZeroDim::iterator  // virtual from Mesh::Core
( const tag::OverSegments &, const tag::OrientOpposToMesh &,
  const tag::RequireOrder &, const tag::ThisMeshIsPositive & )
{	std::cout << __FILE__ << ":" << __LINE__ << ": " << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "Zero-dimensional meshes have no segments." << std::endl;
	exit (1);                                                                                      }


Mesh::Iterator::Core * Mesh::ZeroDim::iterator  // virtual from Mesh::Core
( const tag::OverSegments &, const tag::AsFound &,
  const tag::Backwards &, const tag::ThisMeshIsPositive & )
{	std::cout << __FILE__ << ":" << __LINE__ << ": " << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "Zero-dimensional meshes have no segments." << std::endl;
	exit (1);                                                                                      }


Mesh::Iterator::Core * Mesh::ZeroDim::iterator  // virtual from Mesh::Core
( const tag::OverSegments &, const tag::ForcePositive &,
  const tag::Backwards &, const tag::ThisMeshIsPositive & )
{	std::cout << __FILE__ << ":" << __LINE__ << ": " << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "Zero-dimensional meshes have no segments." << std::endl;
	exit (1);                                                                                      }


Mesh::Iterator::Core * Mesh::ZeroDim::iterator  // virtual from Mesh::Core
( const tag::OverSegments &, const tag::Backwards &, const tag::ThisMeshIsPositive & )
{	std::cout << __FILE__ << ":" << __LINE__ << ": " << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "Zero-dimensional meshes have no segments." << std::endl;
	exit (1);                                                                                      }


Mesh::Iterator::Core * Mesh::ZeroDim::iterator  // virtual from Mesh::Core
( const tag::OverSegments &, const tag::OrientCompWithMesh &,
  const tag::Backwards &, const tag::ThisMeshIsPositive & )
{	std::cout << __FILE__ << ":" << __LINE__ << ": " << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "Zero-dimensional meshes have no segments." << std::endl;
	exit (1);                                                                                      }


Mesh::Iterator::Core * Mesh::ZeroDim::iterator  // virtual from Mesh::Core
( const tag::OverSegments &, const tag::OrientOpposToMesh &,
  const tag::Backwards &, const tag::ThisMeshIsPositive & )
{	std::cout << __FILE__ << ":" << __LINE__ << ": " << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "Zero-dimensional meshes have no segments." << std::endl;
	exit (1);                                                                                      }

//------------------------------------------------------------------------------------------------------//


Mesh::Iterator::Core * Mesh::ZeroDim::iterator  // virtual from Mesh::Core
( const tag::OverCellsOfDim &, const size_t d,
  const tag::AsFound &, const tag::ThisMeshIsPositive & )

// a positive zero-dimensional mesh is the boundary of a positive segment	
// iterate over the two vertices, first base (negative) then tip (positive)

{	assert ( d == 0 );
	assert ( this->cell_enclosed );
	Cell::PositiveSegment * seg = tag::Util::assert_cast
		< Cell::Positive*, Cell::PositiveSegment* > ( this->cell_enclosed );
	return  new Mesh::Iterator::Over::TwoVerticesOfSeg::NormalOrder::AsFound ( seg );  }
	

Mesh::Iterator::Core * Mesh::ZeroDim::iterator  // virtual from Mesh::Core
( const tag::OverCellsOfDim &, const size_t d,
  const tag::CellHasLowDim &, const tag::ThisMeshIsPositive & )

// a positive zero-dimensional mesh is the boundary of a positive segment

{	assert ( d == 0 );
	assert ( this->cell_enclosed );
	std::cout << "when iterating over the two extremities of a segment," << std::endl;
	std::cout << "there are no lower-dimensional cells" << std::endl;
	exit (1);                                                                           }

	
Mesh::Iterator::Core * Mesh::ZeroDim::iterator  // virtual from Mesh::Core
( const tag::OverCellsOfDim &, const size_t d,
  const tag::ForcePositive &, const tag::ThisMeshIsPositive & )

// a positive zero-dimensional mesh is the boundary of a positive segment	
// iterate over the two vertices, first base then tip (both positive)

{	assert ( d == 0 );
	assert ( this->cell_enclosed );
	Cell::PositiveSegment * seg = tag::Util::assert_cast
		< Cell::Positive*, Cell::PositiveSegment* > ( this->cell_enclosed );
	return  new Mesh::Iterator::Over::TwoVerticesOfSeg::NormalOrder::ForcePositive ( seg );  }


Mesh::Iterator::Core * Mesh::ZeroDim::iterator  // virtual from Mesh::Core
( const tag::OverCellsOfDim &, const size_t d, const tag::ThisMeshIsPositive & )

// a positive zero-dimensional mesh is the boundary of a positive segment	

{	assert ( d == 0 );
	assert ( this->cell_enclosed );
	std::cout << "when iterating over the two extremities of a segment," << std::endl;
	std::cout << "please provide tag::orientation_compatible_with_mesh or tag::force_positive"
	          << std::endl;
	exit (1);                                                                                  }


Mesh::Iterator::Core * Mesh::ZeroDim::iterator  // virtual from Mesh::Core
( const tag::OverCellsOfDim &, const size_t d, const tag::OrientCompWithMesh &,
  const tag::ThisMeshIsPositive &                                              )

// a positive zero-dimensional mesh is the boundary of a positive segment	
// iterate over the two vertices, first base (negative) then tip (positive)

{	assert ( d == 0 );
	assert ( this->cell_enclosed );
	Cell::PositiveSegment * seg = tag::Util::assert_cast
		< Cell::Positive*, Cell::PositiveSegment* > ( this->cell_enclosed );
	return  new Mesh::Iterator::Over::TwoVerticesOfSeg::NormalOrder::AsFound ( seg );  }


Mesh::Iterator::Core * Mesh::ZeroDim::iterator  // virtual from Mesh::Core
( const tag::OverCellsOfDim &, const size_t d, const tag::OrientOpposToMesh &,
  const tag::ThisMeshIsPositive &                                             )

// a positive zero-dimensional mesh is the boundary of a positive segment	
// iterate over the two vertices, first base (negative) then tip (positive)

{	assert ( d == 0 );
	assert ( this->cell_enclosed );
	assert ( false );  // to implement
	Cell::PositiveSegment * seg = tag::Util::assert_cast
		< Cell::Positive*, Cell::PositiveSegment* > ( this->cell_enclosed );
	return  new Mesh::Iterator::Over::TwoVerticesOfSeg::NormalOrder::AsFound ( seg );  }


Mesh::Iterator::Core * Mesh::ZeroDim::iterator  // virtual from Mesh::Core
( const tag::OverCellsOfDim &, const size_t d, const tag::AsFound &,
  const tag::RequireOrder &, const tag::ThisMeshIsPositive &        )

// a positive zero-dimensional mesh is the boundary of a positive segment	
// iterate over the two vertices, first base (negative) then tip (positive)

{	assert ( d == 0 );
	assert ( this->cell_enclosed );
	Cell::PositiveSegment * seg = tag::Util::assert_cast
		< Cell::Positive*, Cell::PositiveSegment* > ( this->cell_enclosed );
	return  new Mesh::Iterator::Over::TwoVerticesOfSeg::NormalOrder::AsFound ( seg );  }


Mesh::Iterator::Core * Mesh::ZeroDim::iterator  // virtual from Mesh::Core
( const tag::OverCellsOfDim &, const size_t d, const tag::ForcePositive &,
  const tag::RequireOrder &, const tag::ThisMeshIsPositive &              )

// a positive zero-dimensional mesh is the boundary of a positive segment	
// iterate over the two vertices, first base then tip (both positive)

{	assert ( d == 0 );
	assert ( this->cell_enclosed );
	Cell::PositiveSegment * seg = tag::Util::assert_cast
		< Cell::Positive*, Cell::PositiveSegment* > ( this->cell_enclosed );
	return  new Mesh::Iterator::Over::TwoVerticesOfSeg::NormalOrder::ForcePositive ( seg );  }


Mesh::Iterator::Core * Mesh::ZeroDim::iterator  // virtual from Mesh::Core
( const tag::OverCellsOfDim &, const size_t d,
  const tag::RequireOrder &, const tag::ThisMeshIsPositive & )

// a positive zero-dimensional mesh is the boundary of a positive segment	

{	assert ( d == 0 );
	assert ( this->cell_enclosed );
	std::cout << "when iterating over the two extremities of a segment," << std::endl;
	std::cout << "please provide tag::orientation_compatible_with_mesh or tag::force_positive"
	          << std::endl;
	exit (1);                                                                                  }


Mesh::Iterator::Core * Mesh::ZeroDim::iterator  // virtual from Mesh::Core
( const tag::OverCellsOfDim &, const size_t d, const tag::OrientCompWithMesh &,
  const tag::RequireOrder &, const tag::ThisMeshIsPositive &                   )

// a positive zero-dimensional mesh is the boundary of a positive segment	
// iterate over the two vertices, first base (negative) then tip (positive)

{	assert ( d == 0 );
	assert ( this->cell_enclosed );
	Cell::PositiveSegment * seg = tag::Util::assert_cast
		< Cell::Positive*, Cell::PositiveSegment* > ( this->cell_enclosed );
	return  new Mesh::Iterator::Over::TwoVerticesOfSeg::NormalOrder::AsFound ( seg );  }


Mesh::Iterator::Core * Mesh::ZeroDim::iterator  // virtual from Mesh::Core
( const tag::OverCellsOfDim &, const size_t d, const tag::OrientOpposToMesh &,
  const tag::RequireOrder &, const tag::ThisMeshIsPositive &                  )

// a positive zero-dimensional mesh is the boundary of a positive segment	
// iterate over the two vertices, first base (positive) then tip (negative)

{	assert ( d == 0 );
	assert ( this->cell_enclosed );
	assert ( false );  // to implement
	Cell::PositiveSegment * seg = tag::Util::assert_cast
		< Cell::Positive*, Cell::PositiveSegment* > ( this->cell_enclosed );
	return  new Mesh::Iterator::Over::TwoVerticesOfSeg::NormalOrder::AsFound ( seg );  }


Mesh::Iterator::Core * Mesh::ZeroDim::iterator  // virtual from Mesh::Core
( const tag::OverCellsOfDim &, const size_t d, const tag::AsFound &,
  const tag::Backwards &, const tag::ThisMeshIsPositive &          )

// a positive zero-dimensional mesh is the boundary of a positive segment	
// iterate over the two vertices, first tip (positive) then base (negative)

{	assert ( d == 0 );
	assert ( this->cell_enclosed );
	Cell::PositiveSegment * seg = tag::Util::assert_cast
		< Cell::Positive*, Cell::PositiveSegment* > ( this->cell_enclosed );
	return  new Mesh::Iterator::Over::TwoVerticesOfSeg::ReverseOrder::AsFound ( seg );  }


Mesh::Iterator::Core * Mesh::ZeroDim::iterator  // virtual from Mesh::Core
( const tag::OverCellsOfDim &, const size_t d, const tag::ForcePositive &,
  const tag::Backwards &, const tag::ThisMeshIsPositive &              )

// a positive zero-dimensional mesh is the boundary of a positive segment	
// iterate over the two vertices, first tip then base (both positive)

{	assert ( d == 0 );
	assert ( this->cell_enclosed );
	Cell::PositiveSegment * seg = tag::Util::assert_cast
		< Cell::Positive*, Cell::PositiveSegment* > ( this->cell_enclosed );
	return  new Mesh::Iterator::Over::TwoVerticesOfSeg::ReverseOrder::ForcePositive ( seg );  }


Mesh::Iterator::Core * Mesh::ZeroDim::iterator  // virtual from Mesh::Core
( const tag::OverCellsOfDim &, const size_t d,
  const tag::Backwards &, const tag::ThisMeshIsPositive & )

// a positive zero-dimensional mesh is the boundary of a positive segment	

{	assert ( d == 0 );
	assert ( this->cell_enclosed );
	std::cout << "when iterating over the two extremities of a segment," << std::endl;
	std::cout << "please provide tag::orientation_compatible_with_mesh or tag::force_positive"
	          << std::endl;
	exit (1);                                                                                  }


Mesh::Iterator::Core * Mesh::ZeroDim::iterator  // virtual from Mesh::Core
( const tag::OverCellsOfDim &, const size_t d, const tag::OrientCompWithMesh &,
  const tag::Backwards &, const tag::ThisMeshIsPositive &                   )

// a positive zero-dimensional mesh is the boundary of a positive segment	
// iterate over the two vertices, first tip (positive) then base (negative)

{	assert ( d == 0 );
	assert ( this->cell_enclosed );
	Cell::PositiveSegment * seg = tag::Util::assert_cast
		< Cell::Positive*, Cell::PositiveSegment* > ( this->cell_enclosed );
	return  new Mesh::Iterator::Over::TwoVerticesOfSeg::ReverseOrder::AsFound ( seg );  }


Mesh::Iterator::Core * Mesh::ZeroDim::iterator  // virtual from Mesh::Core
( const tag::OverCellsOfDim &, const size_t d, const tag::OrientOpposToMesh &,
  const tag::Backwards &, const tag::ThisMeshIsPositive &                  )

// a positive zero-dimensional mesh is the boundary of a positive segment	
// iterate over the two vertices, first tip (negative) then base (positive)

{	assert ( d == 0 );
	assert ( this->cell_enclosed );
	assert ( false );  // to implement
	Cell::PositiveSegment * seg = tag::Util::assert_cast
		< Cell::Positive*, Cell::PositiveSegment* > ( this->cell_enclosed );
	return  new Mesh::Iterator::Over::TwoVerticesOfSeg::ReverseOrder::AsFound ( seg );  }

//------------------------------------------------------------------------------------------------------//


Mesh::Iterator::Core * Mesh::ZeroDim::iterator  // virtual from Mesh::Core
( const tag::OverCellsOfMaxDim &, const tag::AsFound &, const tag::ThisMeshIsPositive & )
// a positive zero-dimensional mesh is the boundary of a positive segment	
// iterate over the two vertices, first base (negative) then tip (positive)
{	assert ( this->cell_enclosed );
	Cell::PositiveSegment * seg = tag::Util::assert_cast
		< Cell::Positive*, Cell::PositiveSegment* > ( this->cell_enclosed );
	return  new Mesh::Iterator::Over::TwoVerticesOfSeg::NormalOrder::AsFound ( seg );  }


Mesh::Iterator::Core * Mesh::ZeroDim::iterator  // virtual from Mesh::Core
( const tag::OverCellsOfMaxDim &, const tag::ForcePositive &, const tag::ThisMeshIsPositive & )
// a positive zero-dimensional mesh is the boundary of a positive segment	
// iterate over the two vertices, first base then tip (both positive)
{	assert ( this->cell_enclosed );
	Cell::PositiveSegment * seg = tag::Util::assert_cast
		< Cell::Positive*, Cell::PositiveSegment* > ( this->cell_enclosed );
	return  new Mesh::Iterator::Over::TwoVerticesOfSeg::NormalOrder::ForcePositive ( seg );  }


Mesh::Iterator::Core * Mesh::ZeroDim::iterator  // virtual from Mesh::Core
( const tag::OverCellsOfMaxDim &, const tag::OrientCompWithMesh &, const tag::ThisMeshIsPositive & )
// a positive zero-dimensional mesh is the boundary of a positive segment	
// iterate over the two vertices, first base (negative) then tip (positive)
{	assert ( this->cell_enclosed );
	Cell::PositiveSegment * seg = tag::Util::assert_cast
		< Cell::Positive*, Cell::PositiveSegment* > ( this->cell_enclosed );
	return  new Mesh::Iterator::Over::TwoVerticesOfSeg::NormalOrder::AsFound ( seg );  }


Mesh::Iterator::Core * Mesh::ZeroDim::iterator  // virtual from Mesh::Core
( const tag::OverCellsOfMaxDim &, const tag::OrientOpposToMesh &, const tag::ThisMeshIsPositive & )
// a positive zero-dimensional mesh is the boundary of a positive segment	
// iterate over the two vertices, first base (positive) then tip (negative)
{	assert ( this->cell_enclosed );
	Cell::PositiveSegment * seg = tag::Util::assert_cast
		< Cell::Positive*, Cell::PositiveSegment* > ( this->cell_enclosed );
	return  new Mesh::Iterator::Over::TwoVerticesOfSeg::NormalOrder::ReverseEachCell ( seg );  }


Mesh::Iterator::Core * Mesh::ZeroDim::iterator  // virtual from Mesh::Core
( const tag::OverCellsOfMaxDim &, const tag::AsFound &,
  const tag::RequireOrder &, const tag::ThisMeshIsPositive & )
// a positive zero-dimensional mesh is the boundary of a positive segment	
// iterate over the two vertices, first base (negative) then tip (positive)
{	assert ( this->cell_enclosed );
	Cell::PositiveSegment * seg = tag::Util::assert_cast
		< Cell::Positive*, Cell::PositiveSegment* > ( this->cell_enclosed );
	return  new Mesh::Iterator::Over::TwoVerticesOfSeg::NormalOrder::AsFound ( seg );  }


Mesh::Iterator::Core * Mesh::ZeroDim::iterator  // virtual from Mesh::Core
( const tag::OverCellsOfMaxDim &, const tag::ForcePositive &,
  const tag::RequireOrder &, const tag::ThisMeshIsPositive & )
// a positive zero-dimensional mesh is the boundary of a positive segment	
// iterate over the two vertices, first tip then base (both positive)
{	assert ( this->cell_enclosed );
	Cell::PositiveSegment * seg = tag::Util::assert_cast
		< Cell::Positive*, Cell::PositiveSegment* > ( this->cell_enclosed );
	return  new Mesh::Iterator::Over::TwoVerticesOfSeg::NormalOrder::ForcePositive ( seg );  }


Mesh::Iterator::Core * Mesh::ZeroDim::iterator  // virtual from Mesh::Core
( const tag::OverCellsOfMaxDim &, const tag::OrientCompWithMesh &,
  const tag::RequireOrder &, const tag::ThisMeshIsPositive &      )
// a positive zero-dimensional mesh is the boundary of a positive segment	
// iterate over the two vertices, first base (negative) then tip (positive)
{	assert ( this->cell_enclosed );
	Cell::PositiveSegment * seg = tag::Util::assert_cast
		< Cell::Positive*, Cell::PositiveSegment* > ( this->cell_enclosed );
	return  new Mesh::Iterator::Over::TwoVerticesOfSeg::NormalOrder::AsFound ( seg );  }


Mesh::Iterator::Core * Mesh::ZeroDim::iterator  // virtual from Mesh::Core
( const tag::OverCellsOfMaxDim &, const tag::OrientOpposToMesh &,
  const tag::RequireOrder &, const tag::ThisMeshIsPositive &     )
// a positive zero-dimensional mesh is the boundary of a positive segment	
// iterate over the two vertices, first base (positive) then tip (negative)
{	assert ( this->cell_enclosed );
	assert ( false );  // to implement
	Cell::PositiveSegment * seg = tag::Util::assert_cast
		< Cell::Positive*, Cell::PositiveSegment* > ( this->cell_enclosed );
	return  new Mesh::Iterator::Over::TwoVerticesOfSeg::NormalOrder::AsFound ( seg );  }


Mesh::Iterator::Core * Mesh::ZeroDim::iterator  // virtual from Mesh::Core
( const tag::OverCellsOfMaxDim &, const tag::AsFound &,
  const tag::Backwards &, const tag::ThisMeshIsPositive & )
// a positive zero-dimensional mesh is the boundary of a positive segment	
// iterate over the two vertices, first tip (positive) then base (negative)
{	assert ( this->cell_enclosed );
	Cell::PositiveSegment * seg = tag::Util::assert_cast
		< Cell::Positive*, Cell::PositiveSegment* > ( this->cell_enclosed );
	return  new Mesh::Iterator::Over::TwoVerticesOfSeg::ReverseOrder::AsFound ( seg );  }


Mesh::Iterator::Core * Mesh::ZeroDim::iterator  // virtual from Mesh::Core
( const tag::OverCellsOfMaxDim &, const tag::ForcePositive &,
  const tag::Backwards &, const tag::ThisMeshIsPositive & )
// a positive zero-dimensional mesh is the boundary of a positive segment	
// iterate over the two vertices, first tip then base (both positive)
{	assert ( this->cell_enclosed );
	Cell::PositiveSegment * seg = tag::Util::assert_cast
		< Cell::Positive*, Cell::PositiveSegment* > ( this->cell_enclosed );
	return  new Mesh::Iterator::Over::TwoVerticesOfSeg::ReverseOrder::ForcePositive ( seg );  }


Mesh::Iterator::Core * Mesh::ZeroDim::iterator  // virtual from Mesh::Core
( const tag::OverCellsOfMaxDim &, const tag::OrientCompWithMesh &,
  const tag::Backwards &, const tag::ThisMeshIsPositive &      )
// a positive zero-dimensional mesh is the boundary of a positive segment	
// iterate over the two vertices, first tip (positive) then base (negative)
{	assert ( this->cell_enclosed );
	Cell::PositiveSegment * seg = tag::Util::assert_cast
		< Cell::Positive*, Cell::PositiveSegment* > ( this->cell_enclosed );
	return  new Mesh::Iterator::Over::TwoVerticesOfSeg::ReverseOrder::AsFound ( seg );  }


Mesh::Iterator::Core * Mesh::ZeroDim::iterator  // virtual from Mesh::Core
( const tag::OverCellsOfMaxDim &, const tag::OrientOpposToMesh &,
  const tag::Backwards &, const tag::ThisMeshIsPositive &     )
// a positive zero-dimensional mesh is the boundary of a positive segment	
// iterate over the two vertices, first tip (negative) then base (positive)
{	assert ( this->cell_enclosed );
	assert ( false );  // to implement
	Cell::PositiveSegment * seg = tag::Util::assert_cast
		< Cell::Positive*, Cell::PositiveSegment* > ( this->cell_enclosed );
	return  new Mesh::Iterator::Over::TwoVerticesOfSeg::ReverseOrder::AsFound ( seg );  }

//------------------------------------------------------------------------------------------------------//
//------------------------------------------------------------------------------------------------------//


Mesh::Iterator::Core * Mesh::ZeroDim::iterator  // virtual from Mesh::Core
( const tag::OverVertices &, const tag::ConnectedTo &, Cell::Positive::Vertex *,
  const tag::ThisMeshIsPositive &                                               )
{	std::cout << __FILE__ << ":" << __LINE__ << ": " << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "Cannot iterate around a cell in a zero-dimensional mesh." << std::endl;
	exit (1);                                                                                      }


Mesh::Iterator::Core * Mesh::ZeroDim::iterator  // virtual from Mesh::Core
( const tag::OverVertices &, const tag::ConnectedTo &, Cell::Positive::Vertex *,
  const tag::RequireOrder &, const tag::ThisMeshIsPositive &                    )
{	std::cout << __FILE__ << ":" << __LINE__ << ": " << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "Cannot iterate around a cell in a zero-dimensional mesh." << std::endl;
	exit (1);                                                                                      }


Mesh::Iterator::Core * Mesh::ZeroDim::iterator  // virtual from Mesh::Core
( const tag::OverSegments &, const tag::WhoseTipIs &, Cell::Positive::Vertex * const c,
  const tag::ThisMeshIsPositive &                                                      )
{	std::cout << __FILE__ << ":" << __LINE__ << ": " << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "Cannot iterate around a cell in a zero-dimensional mesh." << std::endl;
	exit (1);                                                                                      }


Mesh::Iterator::Core * Mesh::ZeroDim::iterator  // virtual from Mesh::Core
( const tag::OverSegments &, const tag::WhoseTipIs &, Cell::Positive::Vertex * const c,
  const tag::RequireOrder &, const tag::ThisMeshIsPositive &                           )
{	std::cout << __FILE__ << ":" << __LINE__ << ": " << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "Cannot iterate around a cell in a zero-dimensional mesh." << std::endl;
	exit (1);                                                                                      }


Mesh::Iterator::Core * Mesh::ZeroDim::iterator  // virtual from Mesh::Core
( const tag::OverSegments &, const tag::WhoseTipIs &, Cell::Positive::Vertex * const c,
  const tag::Backwards &, const tag::ThisMeshIsPositive &                              )
{	std::cout << __FILE__ << ":" << __LINE__ << ": " << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "Cannot iterate around a cell in a zero-dimensional mesh." << std::endl;
	exit (1);                                                                                      }


Mesh::Iterator::Core * Mesh::ZeroDim::iterator  // virtual from Mesh::Core
( const tag::OverSegments &, const tag::WhoseBaseIs &, Cell::Positive::Vertex * const c,
  const tag::ThisMeshIsPositive &                                                       )
{	std::cout << __FILE__ << ":" << __LINE__ << ": " << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "Cannot iterate around a cell in a zero-dimensional mesh." << std::endl;
	exit (1);                                                                                      }


Mesh::Iterator::Core * Mesh::ZeroDim::iterator  // virtual from Mesh::Core
( const tag::OverSegments &, const tag::WhoseBaseIs &, Cell::Positive::Vertex * const c,
  const tag::RequireOrder &, const tag::ThisMeshIsPositive &                            )
{	std::cout << __FILE__ << ":" << __LINE__ << ": " << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "Cannot iterate around a cell in a zero-dimensional mesh." << std::endl;
	exit (1);                                                                                      }


Mesh::Iterator::Core * Mesh::ZeroDim::iterator  // virtual from Mesh::Core
( const tag::OverSegments &, const tag::WhoseBaseIs &, Cell::Positive::Vertex * const c,
  const tag::Backwards &, const tag::ThisMeshIsPositive &                               )
{	std::cout << __FILE__ << ":" << __LINE__ << ": " << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "Cannot iterate around a cell in a zero-dimensional mesh." << std::endl;
	exit (1);                                                                                      }


Mesh::Iterator::Core * Mesh::ZeroDim::iterator  // virtual from Mesh::Core
( const tag::OverSegments &, const tag::AsFound &, const tag::Around &, Cell::Positive::Vertex *,
  const tag::ThisMeshIsPositive &                                                                )
{	std::cout << __FILE__ << ":" << __LINE__ << ": " << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "Cannot iterate around a cell in a zero-dimensional mesh." << std::endl;
	exit (1);                                                                                      }


Mesh::Iterator::Core * Mesh::ZeroDim::iterator  // virtual from Mesh::Core
( const tag::OverSegments &, const tag::AsFound &, const tag::Around &, Cell::Positive::Vertex *,
  const tag::RequireOrder &, const tag::ThisMeshIsPositive &                                     )
{	std::cout << __FILE__ << ":" << __LINE__ << ": " << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "Cannot iterate around a cell in a zero-dimensional mesh." << std::endl;
	exit (1);                                                                                      }


Mesh::Iterator::Core * Mesh::ZeroDim::iterator  // virtual from Mesh::Core
( const tag::OverSegments &, const tag::AsFound &, const tag::Around &, Cell::Positive::Vertex *,
  const tag::Backwards &, const tag::ThisMeshIsPositive &                                     )
{	std::cout << __FILE__ << ":" << __LINE__ << ": " << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "Cannot iterate around a cell in a zero-dimensional mesh." << std::endl;
	exit (1);                                                                                      }


Mesh::Iterator::Core * Mesh::ZeroDim::iterator  // virtual from Mesh::Core
( const tag::OverSegments &, const tag::ForcePositive &, const tag::Around &, Cell::Positive::Vertex *,
  const tag::ThisMeshIsPositive &                                                                      )
{	std::cout << __FILE__ << ":" << __LINE__ << ": " << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "Cannot iterate around a cell in a zero-dimensional mesh." << std::endl;
	exit (1);                                                                                      }


Mesh::Iterator::Core * Mesh::ZeroDim::iterator  // virtual from Mesh::Core
( const tag::OverSegments &, const tag::ForcePositive &, const tag::Around &, Cell::Positive::Vertex *,
  const tag::RequireOrder &, const tag::ThisMeshIsPositive &                                           )
{	std::cout << __FILE__ << ":" << __LINE__ << ": " << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "Cannot iterate around a cell in a zero-dimensional mesh." << std::endl;
	exit (1);                                                                                      }


Mesh::Iterator::Core * Mesh::ZeroDim::iterator  // virtual from Mesh::Core
( const tag::OverSegments &, const tag::ForcePositive &, const tag::Around &, Cell::Positive::Vertex *,
  const tag::Backwards &, const tag::ThisMeshIsPositive &                                           )
{	std::cout << __FILE__ << ":" << __LINE__ << ": " << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "Cannot iterate around a cell in a zero-dimensional mesh." << std::endl;
	exit (1);                                                                                      }

//------------------------------------------------------------------------------------------------------//


Mesh::Iterator::Core * Mesh::ZeroDim::iterator  // virtual from Mesh::Core
( const tag::OverCellsOfDim &, const size_t d, const tag::AsFound &,
  const tag::Around &, Cell::Core *, const tag::ThisMeshIsPositive & )
{	std::cout << __FILE__ << ":" << __LINE__ << ": " << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "Cannot iterate around a cell in a zero-dimensional mesh." << std::endl;
	exit (1);                                                                                      }


Mesh::Iterator::Core * Mesh::ZeroDim::iterator  // virtual from Mesh::Core
( const tag::OverCellsOfDim &, const size_t d, const tag::AsFound &,
  const tag::Around &, Cell::Core *, const tag::RequireOrder &, const tag::ThisMeshIsPositive & )
{	std::cout << __FILE__ << ":" << __LINE__ << ": " << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "Cannot iterate around a cell in a zero-dimensional mesh." << std::endl;
	exit (1);                                                                                      }


Mesh::Iterator::Core * Mesh::ZeroDim::iterator  // virtual from Mesh::Core
( const tag::OverCellsOfDim &, const size_t d, const tag::AsFound &,
  const tag::Around &, Cell::Core *, const tag::Backwards &, const tag::ThisMeshIsPositive & )
{	std::cout << __FILE__ << ":" << __LINE__ << ": " << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "Cannot iterate around a cell in a zero-dimensional mesh." << std::endl;
	exit (1);                                                                                      }


Mesh::Iterator::Core * Mesh::ZeroDim::iterator  // virtual from Mesh::Core
( const tag::OverCellsOfDim &, const size_t d, const tag::ForcePositive &,
  const tag::Around &, Cell::Core *, const tag::ThisMeshIsPositive &      )
{	std::cout << __FILE__ << ":" << __LINE__ << ": " << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "Cannot iterate around a cell in a zero-dimensional mesh." << std::endl;
	exit (1);                                                                                      }


Mesh::Iterator::Core * Mesh::ZeroDim::iterator  // virtual from Mesh::Core
( const tag::OverCellsOfDim &, const size_t d, const tag::ForcePositive &,
  const tag::Around &, Cell::Core *, const tag::RequireOrder &, const tag::ThisMeshIsPositive & )
{	std::cout << __FILE__ << ":" << __LINE__ << ": " << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "Cannot iterate around a cell in a zero-dimensional mesh." << std::endl;
	exit (1);                                                                                      }


Mesh::Iterator::Core * Mesh::ZeroDim::iterator  // virtual from Mesh::Core
( const tag::OverCellsOfDim &, const size_t d, const tag::ForcePositive &,
  const tag::Around &, Cell::Core *, const tag::Backwards &, const tag::ThisMeshIsPositive & )
{	std::cout << __FILE__ << ":" << __LINE__ << ": " << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "Cannot iterate around a cell in a zero-dimensional mesh." << std::endl;
	exit (1);                                                                                      }


Mesh::Iterator::Core * Mesh::ZeroDim::iterator  // virtual from Mesh::Core
( const tag::OverCellsOfDim &, const size_t d, const tag::OrientCompWithCenter &,
  const tag::Around &, Cell::Core *, const tag::ThisMeshIsPositive &          )
{	std::cout << __FILE__ << ":" << __LINE__ << ": " << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "Cannot iterate around a cell in a zero-dimensional mesh." << std::endl;
	exit (1);                                                                                      }


Mesh::Iterator::Core * Mesh::ZeroDim::iterator  // virtual from Mesh::Core
( const tag::OverCellsOfDim &, const size_t d, const tag::OrientOpposToCenter &,
  const tag::Around &, Cell::Core *, const tag::ThisMeshIsPositive &            )
{	std::cout << __FILE__ << ":" << __LINE__ << ": " << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "Cannot iterate around a cell in a zero-dimensional mesh." << std::endl;
	exit (1);                                                                                      }

//------------------------------------------------------------------------------------------------------//


Mesh::Iterator::Core * Mesh::ZeroDim::iterator  // virtual from Mesh::Core
( const tag::OverCellsOfMaxDim &, const tag::ForcePositive &,
  const tag::Around &, Cell::Core *, const tag::ThisMeshIsPositive & )
{	std::cout << __FILE__ << ":" << __LINE__ << ": " << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "Cannot iterate around a cell in a zero-dimensional mesh." << std::endl;
	exit (1);                                                                                      }


Mesh::Iterator::Core * Mesh::ZeroDim::iterator  // virtual from Mesh::Core
( const tag::OverCellsOfMaxDim &, const tag::ForcePositive &,
  const tag::Around &, Cell::Core *, const tag::RequireOrder &, const tag::ThisMeshIsPositive & )
{	std::cout << __FILE__ << ":" << __LINE__ << ": " << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "Cannot iterate around a cell in a zero-dimensional mesh." << std::endl;
	exit (1);                                                                                      }


Mesh::Iterator::Core * Mesh::ZeroDim::iterator  // virtual from Mesh::Core
( const tag::OverCellsOfMaxDim &, const tag::ForcePositive &,
  const tag::Around &, Cell::Core *, const tag::Backwards &, const tag::ThisMeshIsPositive & )
{	std::cout << __FILE__ << ":" << __LINE__ << ": " << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "Cannot iterate around a cell in a zero-dimensional mesh." << std::endl;
	exit (1);                                                                                      }


Mesh::Iterator::Core * Mesh::ZeroDim::iterator  // virtual from Mesh::Core
( const tag::OverCellsOfMaxDim &, const tag::OrientCompWithCenter &,
  const tag::Around &, Cell::Core *, const tag::ThisMeshIsPositive & )
{	std::cout << __FILE__ << ":" << __LINE__ << ": " << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "Cannot iterate around a cell in a zero-dimensional mesh." << std::endl;
	exit (1);                                                                                      }


Mesh::Iterator::Core * Mesh::ZeroDim::iterator  // virtual from Mesh::Core
( const tag::OverCellsOfMaxDim &, const tag::OrientOpposToCenter &,
  const tag::Around &, Cell::Core *, const tag::ThisMeshIsPositive & )
{	std::cout << __FILE__ << ":" << __LINE__ << ": " << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "Cannot iterate around a cell in a zero-dimensional mesh." << std::endl;
	exit (1);                                                                                      }


Mesh::Iterator::Core * Mesh::ZeroDim::iterator  // virtual from Mesh::Core
( const tag::OverCellsOfMaxDim &, const tag::OrientCompWithMesh &,
  const tag::Around &, Cell::Core *, const tag::ThisMeshIsPositive & )
{	std::cout << __FILE__ << ":" << __LINE__ << ": " << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "Cannot iterate around a cell in a zero-dimensional mesh." << std::endl;
	exit (1);                                                                                      }


Mesh::Iterator::Core * Mesh::ZeroDim::iterator  // virtual from Mesh::Core
( const tag::OverCellsOfMaxDim &, const tag::OrientCompWithMesh &,
  const tag::Around &, Cell::Core *, const tag::RequireOrder &, const tag::ThisMeshIsPositive & )
{	std::cout << __FILE__ << ":" << __LINE__ << ": " << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "Cannot iterate around a cell in a zero-dimensional mesh." << std::endl;
	exit (1);                                                                                      }


Mesh::Iterator::Core * Mesh::ZeroDim::iterator  // virtual from Mesh::Core
( const tag::OverCellsOfMaxDim &, const tag::OrientCompWithMesh &,
  const tag::Around &, Cell::Core *, const tag::Backwards &, const tag::ThisMeshIsPositive & )
{	std::cout << __FILE__ << ":" << __LINE__ << ": " << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "Cannot iterate around a cell in a zero-dimensional mesh." << std::endl;
	exit (1);                                                                                      }


Mesh::Iterator::Core * Mesh::ZeroDim::iterator  // virtual from Mesh::Core
( const tag::OverCellsOfMaxDim &, const tag::OrientOpposToMesh &,
  const tag::Around &, Cell::Core *, const tag::ThisMeshIsPositive & )
{	std::cout << __FILE__ << ":" << __LINE__ << ": " << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "Cannot iterate around a cell in a zero-dimensional mesh." << std::endl;
	exit (1);                                                                                      }


Mesh::Iterator::Core * Mesh::ZeroDim::iterator  // virtual from Mesh::Core
( const tag::OverCellsOfMaxDim &, const tag::AsFound &,
  const tag::Around &, Cell::Core *, const tag::ThisMeshIsPositive & )
{	std::cout << __FILE__ << ":" << __LINE__ << ": " << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "Cannot iterate around a cell in a zero-dimensional mesh." << std::endl;
	exit (1);                                                                                      }


Mesh::Iterator::Core * Mesh::ZeroDim::iterator  // virtual from Mesh::Core
( const tag::OverCellsOfMaxDim &, const tag::OrientOpposToMesh &,
  const tag::Around &, Cell::Core *, const tag::RequireOrder &, const tag::ThisMeshIsPositive & )
{	std::cout << __FILE__ << ":" << __LINE__ << ": " << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "Cannot iterate around a cell in a zero-dimensional mesh." << std::endl;
	exit (1);                                                                                      }


Mesh::Iterator::Core * Mesh::ZeroDim::iterator  // virtual from Mesh::Core
( const tag::OverCellsOfMaxDim &, const tag::OrientOpposToMesh &,
  const tag::Around &, Cell::Core *, const tag::Backwards &, const tag::ThisMeshIsPositive & )
{	std::cout << __FILE__ << ":" << __LINE__ << ": " << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "Cannot iterate around a cell in a zero-dimensional mesh." << std::endl;
	exit (1);                                                                                      }

//------------------------------------------------------------------------------------------------------//
//------------------------------------------------------------------------------------------------------//


Mesh::Iterator::Core * Mesh::Connected::OneDim::iterator  // virtual from Mesh::Core
( const tag::OverVertices &, const tag::AsFound &, const tag::ThisMeshIsPositive & )
// since the cells are of low dimension, returns positive vertices	
{	return  new Mesh::Iterator::OverVertices::OfConnectedOneDimMesh::NormalOrder ( this );  }


Mesh::Iterator::Core * Mesh::Connected::OneDim::iterator  // virtual from Mesh::Core
( const tag::OverVertices &, const tag::ForcePositive &, const tag::ThisMeshIsPositive & )
// in one-dimensional meshes, vertices are positive anyway
{	return  new Mesh::Iterator::OverVertices::OfConnectedOneDimMesh::NormalOrder ( this );  }


Mesh::Iterator::Core * Mesh::Connected::OneDim::iterator  // virtual from Mesh::Core
( const tag::OverVertices &, const tag::ThisMeshIsPositive & )
// since the cells are of low dimension, returns positive vertices	
{	return  new Mesh::Iterator::OverVertices::OfConnectedOneDimMesh::NormalOrder ( this );  }


Mesh::Iterator::Core * Mesh::Connected::OneDim::iterator  // virtual from Mesh::Core
( const tag::OverVertices &, const tag::OrientCompWithMesh &, const tag::ThisMeshIsPositive & )
{	std::cout << __FILE__ << ":" << __LINE__ << ": " << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "vertices in a one-dimensional mesh are positive" << std::endl;
	exit (1);                                                                                      }


Mesh::Iterator::Core * Mesh::Connected::OneDim::iterator  // virtual from Mesh::Core
( const tag::OverVertices &, const tag::OrientOpposToMesh &, const tag::ThisMeshIsPositive & )
{	std::cout << __FILE__ << ":" << __LINE__ << ": " << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "vertices in a one-dimensional mesh are positive" << std::endl;
	exit (1);                                                                                      }


Mesh::Iterator::Core * Mesh::Connected::OneDim::iterator  // virtual from Mesh::Core
( const tag::OverVertices &, const tag::AsFound &,
  const tag::RequireOrder &, const tag::ThisMeshIsPositive & )
{	return  new Mesh::Iterator::OverVertices::OfConnectedOneDimMesh::NormalOrder ( this );  }


Mesh::Iterator::Core * Mesh::Connected::OneDim::iterator  // virtual from Mesh::Core
( const tag::OverVertices &, const tag::ForcePositive &,
  const tag::RequireOrder &, const tag::ThisMeshIsPositive & )
// in one-dimensional meshes, vertices are positive anyway
{	return  new Mesh::Iterator::OverVertices::OfConnectedOneDimMesh::NormalOrder ( this );  }


Mesh::Iterator::Core * Mesh::Connected::OneDim::iterator  // virtual from Mesh::Core
( const tag::OverVertices &, const tag::RequireOrder &, const tag::ThisMeshIsPositive & )
// since the cells are of low dimension, returns positive vertices	
{	return  new Mesh::Iterator::OverVertices::OfConnectedOneDimMesh::NormalOrder ( this );  }


Mesh::Iterator::Core * Mesh::Connected::OneDim::iterator  // virtual from Mesh::Core
( const tag::OverVertices &, const tag::OrientCompWithMesh &,
  const tag::RequireOrder &, const tag::ThisMeshIsPositive & )
{	std::cout << __FILE__ << ":" << __LINE__ << ": " << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "vertices in a one-dimensional mesh are positive" << std::endl;
	exit (1);                                                                                      }


Mesh::Iterator::Core * Mesh::Connected::OneDim::iterator  // virtual from Mesh::Core
( const tag::OverVertices &, const tag::OrientOpposToMesh &,
  const tag::RequireOrder &, const tag::ThisMeshIsPositive & )
{	std::cout << __FILE__ << ":" << __LINE__ << ": " << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "vertices in a one-dimensional mesh are positive" << std::endl;
	exit (1);                                                                                      }


Mesh::Iterator::Core * Mesh::Connected::OneDim::iterator  // virtual from Mesh::Core
( const tag::OverVertices &, const tag::AsFound &,
  const tag::Backwards &, const tag::ThisMeshIsPositive & )
// since the cells are of low dimension, returns positive vertices	
{	return  new Mesh::Iterator::OverVertices::OfConnectedOneDimMesh::ReverseOrder ( this );  }


Mesh::Iterator::Core * Mesh::Connected::OneDim::iterator  // virtual from Mesh::Core
( const tag::OverVertices &, const tag::ForcePositive &,
  const tag::Backwards &, const tag::ThisMeshIsPositive & )
// in one-dimensional meshes, vertices are positive anyway
{	return  new Mesh::Iterator::OverVertices::OfConnectedOneDimMesh::ReverseOrder ( this );  }


Mesh::Iterator::Core * Mesh::Connected::OneDim::iterator  // virtual from Mesh::Core
( const tag::OverVertices &, const tag::Backwards &, const tag::ThisMeshIsPositive & )
// since the cells are of low dimension, returns positive vertices	
{	return  new Mesh::Iterator::OverVertices::OfConnectedOneDimMesh::ReverseOrder ( this );  }


Mesh::Iterator::Core * Mesh::Connected::OneDim::iterator  // virtual from Mesh::Core
( const tag::OverVertices &, const tag::OrientCompWithMesh &,
  const tag::Backwards &, const tag::ThisMeshIsPositive & )
{	std::cout << __FILE__ << ":" << __LINE__ << ": " << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "vertices in a one-dimensional mesh are positive" << std::endl;
	exit (1);                                                                                      }


Mesh::Iterator::Core * Mesh::Connected::OneDim::iterator  // virtual from Mesh::Core
( const tag::OverVertices &, const tag::OrientOpposToMesh &,
  const tag::Backwards &, const tag::ThisMeshIsPositive & )
{	std::cout << __FILE__ << ":" << __LINE__ << ": " << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "vertices in a one-dimensional mesh are positive" << std::endl;
	exit (1);                                                                                      }

//------------------------------------------------------------------------------------------------------//


Mesh::Iterator::Core * Mesh::Connected::OneDim::iterator  // virtual from Mesh::Core
( const tag::OverSegments &, const tag::AsFound &, const tag::ThisMeshIsPositive & )
// return oriented segments
{	return  new Mesh::Iterator::OverSegments::OfConnectedOneDimMesh::NormalOrder::AsFound ( this );  }


Mesh::Iterator::Core * Mesh::Connected::OneDim::iterator  // virtual from Mesh::Core
( const tag::OverSegments &, const tag::ForcePositive &, const tag::ThisMeshIsPositive & )
{	return  new Mesh::Iterator::OverSegments::OfConnectedOneDimMesh::
	                  NormalOrder::ForcePositive ( this );             }


Mesh::Iterator::Core * Mesh::Connected::OneDim::iterator  // virtual from Mesh::Core
( const tag::OverSegments &, const tag::ThisMeshIsPositive & )
// return oriented segments
{	return  new Mesh::Iterator::OverSegments::OfConnectedOneDimMesh::NormalOrder::AsFound ( this );  }


Mesh::Iterator::Core * Mesh::Connected::OneDim::iterator  // virtual from Mesh::Core
( const tag::OverSegments &, const tag::OrientCompWithMesh &, const tag::ThisMeshIsPositive & )
{	return  new Mesh::Iterator::OverSegments::OfConnectedOneDimMesh::NormalOrder::AsFound ( this );  }


Mesh::Iterator::Core * Mesh::Connected::OneDim::iterator  // virtual from Mesh::Core
( const tag::OverSegments &, const tag::OrientOpposToMesh &, const tag::ThisMeshIsPositive & )
{	return  new Mesh::Iterator::OverSegments
	                ::OfConnectedOneDimMesh::NormalOrder::ReverseEachCell ( this );  }


Mesh::Iterator::Core * Mesh::Connected::OneDim::iterator  // virtual from Mesh::Core
( const tag::OverSegments &, const tag::AsFound &,
  const tag::RequireOrder &, const tag::ThisMeshIsPositive & )
// return oriented segments
{	return  new Mesh::Iterator::OverSegments::OfConnectedOneDimMesh::NormalOrder::AsFound ( this );  }


Mesh::Iterator::Core * Mesh::Connected::OneDim::iterator  // virtual from Mesh::Core
( const tag::OverSegments &, const tag::ForcePositive &,
  const tag::RequireOrder &, const tag::ThisMeshIsPositive & )
{	return  new Mesh::Iterator::OverSegments::OfConnectedOneDimMesh::NormalOrder::ForcePositive ( this );  }


Mesh::Iterator::Core * Mesh::Connected::OneDim::iterator  // virtual from Mesh::Core
( const tag::OverSegments &, const tag::RequireOrder &, const tag::ThisMeshIsPositive & )
// return oriented segments
{	return  new Mesh::Iterator::OverSegments::OfConnectedOneDimMesh::NormalOrder::AsFound ( this );  }


Mesh::Iterator::Core * Mesh::Connected::OneDim::iterator  // virtual from Mesh::Core
( const tag::OverSegments &, const tag::OrientCompWithMesh &,
  const tag::RequireOrder &, const tag::ThisMeshIsPositive & )
{	return  new Mesh::Iterator::OverSegments::OfConnectedOneDimMesh::NormalOrder::AsFound ( this );  }


Mesh::Iterator::Core * Mesh::Connected::OneDim::iterator  // virtual from Mesh::Core
( const tag::OverSegments &, const tag::OrientOpposToMesh &,
  const tag::RequireOrder &, const tag::ThisMeshIsPositive & )
{	return  new Mesh::Iterator::OverSegments
	                ::OfConnectedOneDimMesh::NormalOrder::ReverseEachCell ( this );  }


Mesh::Iterator::Core * Mesh::Connected::OneDim::iterator  // virtual from Mesh::Core
( const tag::OverSegments &, const tag::AsFound &,
  const tag::Backwards &, const tag::ThisMeshIsPositive & )
// return oriented segments
{	return  new Mesh::Iterator::OverSegments::OfConnectedOneDimMesh::ReverseOrder::AsFound ( this );  }


Mesh::Iterator::Core * Mesh::Connected::OneDim::iterator  // virtual from Mesh::Core
( const tag::OverSegments &, const tag::ForcePositive &,
  const tag::Backwards &, const tag::ThisMeshIsPositive & )
{	return  new Mesh::Iterator::OverSegments::
		OfConnectedOneDimMesh::ReverseOrder::ForcePositive ( this );  }


Mesh::Iterator::Core * Mesh::Connected::OneDim::iterator  // virtual from Mesh::Core
( const tag::OverSegments &, const tag::Backwards &, const tag::ThisMeshIsPositive & )
// return oriented segments
{	return  new Mesh::Iterator::OverSegments::OfConnectedOneDimMesh::ReverseOrder::AsFound ( this );  }


Mesh::Iterator::Core * Mesh::Connected::OneDim::iterator  // virtual from Mesh::Core
( const tag::OverSegments &, const tag::OrientCompWithMesh &,
  const tag::Backwards &, const tag::ThisMeshIsPositive & )
{	return  new Mesh::Iterator::OverSegments::OfConnectedOneDimMesh::ReverseOrder::AsFound ( this );  }


Mesh::Iterator::Core * Mesh::Connected::OneDim::iterator  // virtual from Mesh::Core
( const tag::OverSegments &, const tag::OrientOpposToMesh &,
  const tag::Backwards &, const tag::ThisMeshIsPositive & )
{	return  new Mesh::Iterator::OverSegments
	                ::OfConnectedOneDimMesh::ReverseOrder::ReverseEachCell ( this );      }

//------------------------------------------------------------------------------------------------------//


Mesh::Iterator::Core * Mesh::Connected::OneDim::iterator  // virtual from Mesh::Core
( const tag::OverCellsOfDim &, const size_t d, const tag::AsFound &, const tag::ThisMeshIsPositive & )

{	if ( d == 0 )
		return  new Mesh::Iterator::OverVertices::OfConnectedOneDimMesh::NormalOrder ( this );
	// else
	assert ( d == 1 );
	return  new Mesh::Iterator::OverSegments::OfConnectedOneDimMesh::NormalOrder::AsFound ( this );  }


Mesh::Iterator::Core * Mesh::Connected::OneDim::iterator  // virtual from Mesh::Core
( const tag::OverCellsOfDim &, const size_t d,
  const tag::CellHasLowDim &, const tag::ThisMeshIsPositive & )

{	assert ( d == 0 );
	return  new Mesh::Iterator::OverVertices::OfConnectedOneDimMesh::NormalOrder ( this );  }

	
Mesh::Iterator::Core * Mesh::Connected::OneDim::iterator  // virtual from Mesh::Core
( const tag::OverCellsOfDim &, const size_t d,
  const tag::ForcePositive &, const tag::ThisMeshIsPositive & )

{	if ( d == 0 )  // in a one-dimensional mesh, vertices are positive anyway
		return  new Mesh::Iterator::OverVertices::OfConnectedOneDimMesh::NormalOrder ( this );
	// else
	assert ( d == 1 );
	return  new Mesh::Iterator::OverSegments::OfConnectedOneDimMesh::
			NormalOrder::ForcePositive ( this );                                                  }


Mesh::Iterator::Core * Mesh::Connected::OneDim::iterator  // virtual from Mesh::Core
( const tag::OverCellsOfDim &, const size_t d, const tag::ThisMeshIsPositive & )

{	if ( d == 0 )
		return  new Mesh::Iterator::OverVertices::OfConnectedOneDimMesh::NormalOrder ( this );
	// else
	assert ( d == 1 );
	return  new Mesh::Iterator::OverSegments::OfConnectedOneDimMesh::NormalOrder::AsFound ( this );  }


Mesh::Iterator::Core * Mesh::Connected::OneDim::iterator  // virtual from Mesh::Core
( const tag::OverCellsOfDim &, const size_t d,
  const tag::OrientCompWithMesh &, const tag::ThisMeshIsPositive & )

{	if ( d == 0 )  // in a one-dimensional mesh, vertices are positive
	{	std::cout << __FILE__ << ":" << __LINE__ << ": " << __extension__ __PRETTY_FUNCTION__ << ": ";
		std::cout << "It makes no sense to require oriented vertices for a 1D mesh." << std::endl;
		exit (1);                                                                                      }
	// else
	assert ( d == 1 );
	return  new Mesh::Iterator::OverSegments::OfConnectedOneDimMesh::NormalOrder::AsFound ( this );     }


Mesh::Iterator::Core * Mesh::Connected::OneDim::iterator  // virtual from Mesh::Core
( const tag::OverCellsOfDim &, const size_t d,
  const tag::OrientOpposToMesh &, const tag::ThisMeshIsPositive & )

{	if ( d == 0 )  // in a one-dimensional mesh, vertices are positive
	{	std::cout << __FILE__ << ":" << __LINE__ << ": " << __extension__ __PRETTY_FUNCTION__ << ": ";
		std::cout << "It makes no sense to require reversed vertices for a 1D mesh." << std::endl;
		exit (1);                                                                                      }
	// else
	assert ( d == 1 );
	return  new Mesh::Iterator::OverSegments
	                ::OfConnectedOneDimMesh::NormalOrder::ReverseEachCell ( this );                      }


Mesh::Iterator::Core * Mesh::Connected::OneDim::iterator  // virtual from Mesh::Core
( const tag::OverCellsOfDim &, const size_t d, const tag::AsFound &,
  const tag::RequireOrder &, const tag::ThisMeshIsPositive &        )

{	if ( d == 0 )
		return  new Mesh::Iterator::OverVertices::OfConnectedOneDimMesh::NormalOrder ( this );
	// else
	assert ( d == 1 );
	return  new Mesh::Iterator::OverSegments::OfConnectedOneDimMesh::NormalOrder::AsFound ( this );  }


Mesh::Iterator::Core * Mesh::Connected::OneDim::iterator  // virtual from Mesh::Core
( const tag::OverCellsOfDim &, const size_t d, const tag::ForcePositive &,
  const tag::RequireOrder &, const tag::ThisMeshIsPositive &              )

{	if ( d == 0 )  // in a one-dimensional mesh, vertices are positive anyway
		return  new Mesh::Iterator::OverVertices::OfConnectedOneDimMesh::NormalOrder ( this );
	// else
	assert ( d == 1 );
	return  new Mesh::Iterator::OverSegments::OfConnectedOneDimMesh::
		NormalOrder::ForcePositive ( this );                                                  }


Mesh::Iterator::Core * Mesh::Connected::OneDim::iterator  // virtual from Mesh::Core
( const tag::OverCellsOfDim &, const size_t d,
  const tag::RequireOrder &, const tag::ThisMeshIsPositive & )

{	if ( d == 0 )
		return  new Mesh::Iterator::OverVertices::OfConnectedOneDimMesh::NormalOrder ( this );
	// else
	assert ( d == 1 );
	return  new Mesh::Iterator::OverSegments::OfConnectedOneDimMesh::NormalOrder::AsFound ( this );  }


Mesh::Iterator::Core * Mesh::Connected::OneDim::iterator  // virtual from Mesh::Core
( const tag::OverCellsOfDim &, const size_t d, const tag::OrientCompWithMesh &,
  const tag::RequireOrder &, const tag::ThisMeshIsPositive &                   )

{	if ( d == 0 )  // in a one-dimensional mesh, vertices are positive
	{	std::cout << __FILE__ << ":" << __LINE__ << ": " << __extension__ __PRETTY_FUNCTION__ << ": ";
		std::cout << "It makes no sense to require oriented vertices for a 1D mesh." << std::endl;
		exit (1);                                                                                      }
	// else
	assert ( d == 1 );
	return  new Mesh::Iterator::OverSegments::OfConnectedOneDimMesh::NormalOrder::AsFound ( this );     }


Mesh::Iterator::Core * Mesh::Connected::OneDim::iterator  // virtual from Mesh::Core
( const tag::OverCellsOfDim &, const size_t d, const tag::OrientOpposToMesh &,
  const tag::RequireOrder &, const tag::ThisMeshIsPositive &                  )

{	if ( d == 0 )  // in a one-dimensional mesh, vertices are positive
	{	std::cout << __FILE__ << ":" << __LINE__ << ": " << __extension__ __PRETTY_FUNCTION__ << ": ";
		std::cout << "It makes no sense to require reversed vertices for a 1D mesh." << std::endl;
		exit (1);                                                                                      }
	// else
	assert ( d == 1 );
	return  new Mesh::Iterator::OverSegments
	                ::OfConnectedOneDimMesh::NormalOrder::ReverseEachCell ( this );                     }


Mesh::Iterator::Core * Mesh::Connected::OneDim::iterator  // virtual from Mesh::Core
( const tag::OverCellsOfDim &, const size_t d, const tag::AsFound &,
  const tag::Backwards &, const tag::ThisMeshIsPositive &           )

{	if ( d == 0 )
		return  new Mesh::Iterator::OverVertices::OfConnectedOneDimMesh::ReverseOrder ( this );
	// else
	assert ( d == 1 );
	return  new Mesh::Iterator::OverSegments::OfConnectedOneDimMesh::ReverseOrder::AsFound ( this );  }


Mesh::Iterator::Core * Mesh::Connected::OneDim::iterator  // virtual from Mesh::Core
( const tag::OverCellsOfDim &, const size_t d, const tag::ForcePositive &,
  const tag::Backwards &, const tag::ThisMeshIsPositive &              )

{	if ( d == 0 )  // in a one-dimensional mesh, vertices are positive anyway
		return  new Mesh::Iterator::OverVertices::OfConnectedOneDimMesh::ReverseOrder ( this );
	// else
	assert ( d == 1 );
	return  new Mesh::Iterator::OverSegments::OfConnectedOneDimMesh::
		ReverseOrder::ForcePositive ( this );                                                   }


Mesh::Iterator::Core * Mesh::Connected::OneDim::iterator  // virtual from Mesh::Core
( const tag::OverCellsOfDim &, const size_t d,
  const tag::Backwards &, const tag::ThisMeshIsPositive & )

{	if ( d == 0 )
		return  new Mesh::Iterator::OverVertices::OfConnectedOneDimMesh::ReverseOrder ( this );
	// else
	assert ( d == 1 );
	return  new Mesh::Iterator::OverSegments::OfConnectedOneDimMesh::ReverseOrder::AsFound ( this );  }


Mesh::Iterator::Core * Mesh::Connected::OneDim::iterator  // virtual from Mesh::Core
( const tag::OverCellsOfDim &, const size_t d, const tag::OrientCompWithMesh &,
  const tag::Backwards &, const tag::ThisMeshIsPositive &                   )

{	if ( d == 0 )  // in a one-dimensional mesh, vertices are positive
	{	std::cout << __FILE__ << ":" << __LINE__ << ": " << __extension__ __PRETTY_FUNCTION__ << ": ";
		std::cout << "It makes no sense to require oriented vertices for a 1D mesh." << std::endl;
		exit (1);                                                                                      }
	// else
	assert ( d == 1 );
	return  new Mesh::Iterator::OverSegments::OfConnectedOneDimMesh::ReverseOrder::AsFound ( this );    }


Mesh::Iterator::Core * Mesh::Connected::OneDim::iterator  // virtual from Mesh::Core
( const tag::OverCellsOfDim &, const size_t d, const tag::OrientOpposToMesh &,
  const tag::Backwards &, const tag::ThisMeshIsPositive &                  )

{	if ( d == 0 )  // in a one-dimensional mesh, vertices are positive
	{	std::cout << __FILE__ << ":" << __LINE__ << ": " << __extension__ __PRETTY_FUNCTION__ << ": ";
		std::cout << "It makes no sense to require reversed vertices for a 1D mesh." << std::endl;
		exit (1);                                                                                      }
	// else
	assert ( d == 1 );
	return  new Mesh::Iterator::OverSegments
	                ::OfConnectedOneDimMesh::ReverseOrder::ReverseEachCell ( this );                     }

//------------------------------------------------------------------------------------------------------//


Mesh::Iterator::Core * Mesh::Connected::OneDim::iterator  // virtual from Mesh::Core
( const tag::OverCellsOfMaxDim &, const tag::AsFound &, const tag::ThisMeshIsPositive & )
{	return  new Mesh::Iterator::OverSegments::
		OfConnectedOneDimMesh::NormalOrder::AsFound ( this );  }


Mesh::Iterator::Core * Mesh::Connected::OneDim::iterator  // virtual from Mesh::Core
( const tag::OverCellsOfMaxDim &, const tag::ForcePositive &, const tag::ThisMeshIsPositive & )
{	return  new Mesh::Iterator::OverSegments::
		OfConnectedOneDimMesh::NormalOrder::ForcePositive ( this );  }
	

Mesh::Iterator::Core * Mesh::Connected::OneDim::iterator  // virtual from Mesh::Core
( const tag::OverCellsOfMaxDim &, const tag::OrientCompWithMesh &, const tag::ThisMeshIsPositive & )
{	return  new Mesh::Iterator::OverSegments::
		OfConnectedOneDimMesh::NormalOrder::AsFound ( this );  }


Mesh::Iterator::Core * Mesh::Connected::OneDim::iterator  // virtual from Mesh::Core
( const tag::OverCellsOfMaxDim &, const tag::OrientOpposToMesh &, const tag::ThisMeshIsPositive & )
{	return  new Mesh::Iterator::OverSegments
	                ::OfConnectedOneDimMesh::NormalOrder::ReverseEachCell ( this );  }


Mesh::Iterator::Core * Mesh::Connected::OneDim::iterator  // virtual from Mesh::Core
( const tag::OverCellsOfMaxDim &, const tag::AsFound &,
  const tag::RequireOrder &, const tag::ThisMeshIsPositive & )
{	return  new Mesh::Iterator::OverSegments::
		OfConnectedOneDimMesh::NormalOrder::AsFound ( this );  }


Mesh::Iterator::Core * Mesh::Connected::OneDim::iterator  // virtual from Mesh::Core
( const tag::OverCellsOfMaxDim &, const tag::ForcePositive &,
  const tag::RequireOrder &, const tag::ThisMeshIsPositive & )
{	return  new Mesh::Iterator::OverSegments::
		OfConnectedOneDimMesh::NormalOrder::ForcePositive ( this );  }


Mesh::Iterator::Core * Mesh::Connected::OneDim::iterator  // virtual from Mesh::Core
( const tag::OverCellsOfMaxDim &, const tag::OrientCompWithMesh &,
  const tag::RequireOrder &, const tag::ThisMeshIsPositive &      )
{	return  new Mesh::Iterator::OverSegments::
		OfConnectedOneDimMesh::NormalOrder::AsFound ( this );  }


Mesh::Iterator::Core * Mesh::Connected::OneDim::iterator  // virtual from Mesh::Core
( const tag::OverCellsOfMaxDim &, const tag::OrientOpposToMesh &,
  const tag::RequireOrder &, const tag::ThisMeshIsPositive &     )
{	return  new Mesh::Iterator::OverSegments
	                ::OfConnectedOneDimMesh::NormalOrder::ReverseEachCell ( this );  }


Mesh::Iterator::Core * Mesh::Connected::OneDim::iterator  // virtual from Mesh::Core
( const tag::OverCellsOfMaxDim &, const tag::AsFound &,
  const tag::Backwards &, const tag::ThisMeshIsPositive & )
{	return  new Mesh::Iterator::OverSegments::
		OfConnectedOneDimMesh::ReverseOrder::AsFound ( this );  }


Mesh::Iterator::Core * Mesh::Connected::OneDim::iterator  // virtual from Mesh::Core
( const tag::OverCellsOfMaxDim &, const tag::ForcePositive &,
  const tag::Backwards &, const tag::ThisMeshIsPositive & )
{	return  new Mesh::Iterator::OverSegments::
		OfConnectedOneDimMesh::ReverseOrder::ForcePositive ( this );  }


Mesh::Iterator::Core * Mesh::Connected::OneDim::iterator  // virtual from Mesh::Core
( const tag::OverCellsOfMaxDim &, const tag::OrientCompWithMesh &,
  const tag::Backwards &, const tag::ThisMeshIsPositive &      )
{	return  new Mesh::Iterator::OverSegments::
		OfConnectedOneDimMesh::ReverseOrder::AsFound ( this );  }


Mesh::Iterator::Core * Mesh::Connected::OneDim::iterator  // virtual from Mesh::Core
( const tag::OverCellsOfMaxDim &, const tag::OrientOpposToMesh &,
  const tag::Backwards &, const tag::ThisMeshIsPositive &     )
{	return  new Mesh::Iterator::OverSegments
	                ::OfConnectedOneDimMesh::ReverseOrder::ReverseEachCell ( this );  }

//------------------------------------------------------------------------------------------------------//


Mesh::Iterator::Core * Mesh::Connected::OneDim::iterator  // virtual from Mesh::Core
( const tag::OverVertices &, const tag::ConnectedTo &, Cell::Positive::Vertex *,
  const tag::ThisMeshIsPositive &                                               )
{	std::cout << __FILE__ << ":" << __LINE__ << ": " << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "Cannot iterate around a cell in a one-dimensional mesh." << std::endl;
	exit (1);                                                                                      }


Mesh::Iterator::Core * Mesh::Connected::OneDim::iterator  // virtual from Mesh::Core
( const tag::OverVertices &, const tag::ConnectedTo &, Cell::Positive::Vertex *,
  const tag::RequireOrder &, const tag::ThisMeshIsPositive &                    )
{	std::cout << __FILE__ << ":" << __LINE__ << ": " << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "Cannot iterate around a cell in a one-dimensional mesh." << std::endl;
	exit (1);                                                                                      }


Mesh::Iterator::Core * Mesh::Connected::OneDim::iterator  // virtual from Mesh::Core
( const tag::OverSegments &, const tag::WhoseTipIs &, Cell::Positive::Vertex * const c,
  const tag::ThisMeshIsPositive &                                                      )
{	std::cout << __FILE__ << ":" << __LINE__ << ": " << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "Cannot iterate around a cell in a one-dimensional mesh." << std::endl;
	exit (1);                                                                                      }


Mesh::Iterator::Core * Mesh::Connected::OneDim::iterator  // virtual from Mesh::Core
( const tag::OverSegments &, const tag::WhoseTipIs &, Cell::Positive::Vertex * const c,
  const tag::RequireOrder &, const tag::ThisMeshIsPositive &                           )
{	std::cout << __FILE__ << ":" << __LINE__ << ": " << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "Cannot iterate around a cell in a one-dimensional mesh." << std::endl;
	exit (1);                                                                                      }


Mesh::Iterator::Core * Mesh::Connected::OneDim::iterator  // virtual from Mesh::Core
( const tag::OverSegments &, const tag::WhoseTipIs &, Cell::Positive::Vertex * const c,
  const tag::Backwards &, const tag::ThisMeshIsPositive &                              )
{	std::cout << __FILE__ << ":" << __LINE__ << ": " << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "Cannot iterate around a cell in a one-dimensional mesh." << std::endl;
	exit (1);                                                                                      }


Mesh::Iterator::Core * Mesh::Connected::OneDim::iterator  // virtual from Mesh::Core
( const tag::OverSegments &, const tag::WhoseBaseIs &, Cell::Positive::Vertex * const c,
  const tag::ThisMeshIsPositive &                                                       )
{	std::cout << __FILE__ << ":" << __LINE__ << ": " << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "Cannot iterate around a cell in a one-dimensional mesh." << std::endl;
	exit (1);                                                                                      }


Mesh::Iterator::Core * Mesh::Connected::OneDim::iterator  // virtual from Mesh::Core
( const tag::OverSegments &, const tag::WhoseBaseIs &, Cell::Positive::Vertex * const c,
  const tag::RequireOrder &, const tag::ThisMeshIsPositive &                            )
{	std::cout << __FILE__ << ":" << __LINE__ << ": " << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "Cannot iterate around a cell in a one-dimensional mesh." << std::endl;
	exit (1);                                                                                      }


Mesh::Iterator::Core * Mesh::Connected::OneDim::iterator  // virtual from Mesh::Core
( const tag::OverSegments &, const tag::WhoseBaseIs &, Cell::Positive::Vertex * const c,
  const tag::Backwards &, const tag::ThisMeshIsPositive &                               )
{	std::cout << __FILE__ << ":" << __LINE__ << ": " << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "Cannot iterate around a cell in a one-dimensional mesh." << std::endl;
	exit (1);                                                                                      }


Mesh::Iterator::Core * Mesh::Connected::OneDim::iterator  // virtual from Mesh::Core
( const tag::OverSegments &, const tag::AsFound &, const tag::Around &, Cell::Positive::Vertex *,
  const tag::RequireOrder &, const tag::ThisMeshIsPositive &                                     )
{	std::cout << __FILE__ << ":" << __LINE__ << ": " << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "Cannot iterate around a cell in a one-dimensional mesh." << std::endl;
	exit (1);                                                                                      }


Mesh::Iterator::Core * Mesh::Connected::OneDim::iterator  // virtual from Mesh::Core
( const tag::OverSegments &, const tag::AsFound &, const tag::Around &, Cell::Positive::Vertex *,
  const tag::ThisMeshIsPositive &                                                                )
{	std::cout << __FILE__ << ":" << __LINE__ << ": " << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "Cannot iterate around a cell in a one-dimensional mesh." << std::endl;
	exit (1);                                                                                      }


Mesh::Iterator::Core * Mesh::Connected::OneDim::iterator  // virtual from Mesh::Core
( const tag::OverSegments &, const tag::AsFound &, const tag::Around &, Cell::Positive::Vertex *,
  const tag::Backwards &, const tag::ThisMeshIsPositive &                                     )
{	std::cout << __FILE__ << ":" << __LINE__ << ": " << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "Cannot iterate around a cell in a one-dimensional mesh." << std::endl;
	exit (1);                                                                                      }


Mesh::Iterator::Core * Mesh::Connected::OneDim::iterator  // virtual from Mesh::Core
( const tag::OverSegments &, const tag::ForcePositive &, const tag::Around &, Cell::Positive::Vertex *,
  const tag::ThisMeshIsPositive &                                                                      )
{	std::cout << __FILE__ << ":" << __LINE__ << ": " << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "Cannot iterate around a cell in a one-dimensional mesh." << std::endl;
	exit (1);                                                                                      }


Mesh::Iterator::Core * Mesh::Connected::OneDim::iterator  // virtual from Mesh::Core
( const tag::OverSegments &, const tag::ForcePositive &, const tag::Around &, Cell::Positive::Vertex *,
  const tag::RequireOrder &, const tag::ThisMeshIsPositive &                                           )
{	std::cout << __FILE__ << ":" << __LINE__ << ": " << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "Cannot iterate around a cell in a one-dimensional mesh." << std::endl;
	exit (1);                                                                                      }


Mesh::Iterator::Core * Mesh::Connected::OneDim::iterator  // virtual from Mesh::Core
( const tag::OverSegments &, const tag::ForcePositive &, const tag::Around &, Cell::Positive::Vertex *,
  const tag::Backwards &, const tag::ThisMeshIsPositive &                                           )
{	std::cout << __FILE__ << ":" << __LINE__ << ": " << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "Cannot iterate around a cell in a one-dimensional mesh." << std::endl;
	exit (1);                                                                                      }

//------------------------------------------------------------------------------------------------------//


Mesh::Iterator::Core * Mesh::Connected::OneDim::iterator  // virtual from Mesh::Core
( const tag::OverCellsOfDim &, const size_t d, const tag::AsFound &,
  const tag::Around &, Cell::Core *, const tag::ThisMeshIsPositive & )
{	std::cout << __FILE__ << ":" << __LINE__ << ": " << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "Cannot iterate around a cell in a one-dimensional mesh." << std::endl;
	exit (1);                                                                                      }


Mesh::Iterator::Core * Mesh::Connected::OneDim::iterator  // virtual from Mesh::Core
( const tag::OverCellsOfDim &, const size_t d, const tag::AsFound &,
  const tag::Around &, Cell::Core *, const tag::RequireOrder &, const tag::ThisMeshIsPositive & )
{	std::cout << __FILE__ << ":" << __LINE__ << ": " << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "Cannot iterate around a cell in a one-dimensional mesh." << std::endl;
	exit (1);                                                                                      }


Mesh::Iterator::Core * Mesh::Connected::OneDim::iterator  // virtual from Mesh::Core
( const tag::OverCellsOfDim &, const size_t d, const tag::AsFound &,
  const tag::Around &, Cell::Core *, const tag::Backwards &, const tag::ThisMeshIsPositive & )
{	std::cout << __FILE__ << ":" << __LINE__ << ": " << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "Cannot iterate around a cell in a one-dimensional mesh." << std::endl;
	exit (1);                                                                                      }


Mesh::Iterator::Core * Mesh::Connected::OneDim::iterator  // virtual from Mesh::Core
( const tag::OverCellsOfDim &, const size_t d, const tag::ForcePositive &,
  const tag::Around &, Cell::Core *, const tag::ThisMeshIsPositive &      )
{	std::cout << __FILE__ << ":" << __LINE__ << ": " << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "Cannot iterate around a cell in a one-dimensional mesh." << std::endl;
	exit (1);                                                                                      }


Mesh::Iterator::Core * Mesh::Connected::OneDim::iterator  // virtual from Mesh::Core
( const tag::OverCellsOfDim &, const size_t d, const tag::ForcePositive &,
  const tag::Around &, Cell::Core *, const tag::RequireOrder &, const tag::ThisMeshIsPositive & )
{	std::cout << __FILE__ << ":" << __LINE__ << ": " << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "Cannot iterate around a cell in a one-dimensional mesh." << std::endl;
	exit (1);                                                                                      }


Mesh::Iterator::Core * Mesh::Connected::OneDim::iterator  // virtual from Mesh::Core
( const tag::OverCellsOfDim &, const size_t d, const tag::ForcePositive &,
  const tag::Around &, Cell::Core *, const tag::Backwards &, const tag::ThisMeshIsPositive & )
{	std::cout << __FILE__ << ":" << __LINE__ << ": " << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "Cannot iterate around a cell in a one-dimensional mesh." << std::endl;
	exit (1);                                                                                      }


Mesh::Iterator::Core * Mesh::Connected::OneDim::iterator  // virtual from Mesh::Core
( const tag::OverCellsOfDim &, const size_t d, const tag::OrientCompWithCenter &,
  const tag::Around &, Cell::Core *, const tag::ThisMeshIsPositive &          )
{	std::cout << __FILE__ << ":" << __LINE__ << ": " << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "Cannot iterate around a cell in a one-dimensional mesh." << std::endl;
	exit (1);                                                                                      }


Mesh::Iterator::Core * Mesh::Connected::OneDim::iterator  // virtual from Mesh::Core
( const tag::OverCellsOfDim &, const size_t d, const tag::OrientOpposToCenter &,
  const tag::Around &, Cell::Core *, const tag::ThisMeshIsPositive &            )
{	std::cout << __FILE__ << ":" << __LINE__ << ": " << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "Cannot iterate around a cell in a one-dimensional mesh." << std::endl;
	exit (1);                                                                                      }

//------------------------------------------------------------------------------------------------------//


Mesh::Iterator::Core * Mesh::Connected::OneDim::iterator  // virtual from Mesh::Core
( const tag::OverCellsOfMaxDim &, const tag::ForcePositive &,
  const tag::Around &, Cell::Core *, const tag::ThisMeshIsPositive & )
{	std::cout << __FILE__ << ":" << __LINE__ << ": " << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "Cannot iterate around a cell in a one-dimensional mesh." << std::endl;
	exit (1);                                                                                      }


Mesh::Iterator::Core * Mesh::Connected::OneDim::iterator  // virtual from Mesh::Core
( const tag::OverCellsOfMaxDim &, const tag::ForcePositive &,
  const tag::Around &, Cell::Core *, const tag::RequireOrder &, const tag::ThisMeshIsPositive & )
{	std::cout << __FILE__ << ":" << __LINE__ << ": " << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "Cannot iterate around a cell in a one-dimensional mesh." << std::endl;
	exit (1);                                                                                      }


Mesh::Iterator::Core * Mesh::Connected::OneDim::iterator  // virtual from Mesh::Core
( const tag::OverCellsOfMaxDim &, const tag::ForcePositive &,
  const tag::Around &, Cell::Core *, const tag::Backwards &, const tag::ThisMeshIsPositive & )
{	std::cout << __FILE__ << ":" << __LINE__ << ": " << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "Cannot iterate around a cell in a one-dimensional mesh." << std::endl;
	exit (1);                                                                                      }


Mesh::Iterator::Core * Mesh::Connected::OneDim::iterator  // virtual from Mesh::Core
( const tag::OverCellsOfMaxDim &, const tag::OrientCompWithCenter &,
  const tag::Around &, Cell::Core *, const tag::ThisMeshIsPositive & )
{	std::cout << __FILE__ << ":" << __LINE__ << ": " << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "Cannot iterate around a cell in a one-dimensional mesh." << std::endl;
	exit (1);                                                                                      }


Mesh::Iterator::Core * Mesh::Connected::OneDim::iterator  // virtual from Mesh::Core
( const tag::OverCellsOfMaxDim &, const tag::OrientOpposToCenter &,
  const tag::Around &, Cell::Core *, const tag::ThisMeshIsPositive & )
{	std::cout << __FILE__ << ":" << __LINE__ << ": " << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "Cannot iterate around a cell in a one-dimensional mesh." << std::endl;
	exit (1);                                                                                      }


Mesh::Iterator::Core * Mesh::Connected::OneDim::iterator  // virtual from Mesh::Core
( const tag::OverCellsOfMaxDim &, const tag::OrientCompWithMesh &,
  const tag::Around &, Cell::Core *, const tag::ThisMeshIsPositive & )
{	std::cout << __FILE__ << ":" << __LINE__ << ": " << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "Cannot iterate around a cell in a one-dimensional mesh." << std::endl;
	exit (1);                                                                                      }


Mesh::Iterator::Core * Mesh::Connected::OneDim::iterator  // virtual from Mesh::Core
( const tag::OverCellsOfMaxDim &, const tag::OrientCompWithMesh &,
  const tag::Around &, Cell::Core *, const tag::RequireOrder &, const tag::ThisMeshIsPositive & )
{	std::cout << __FILE__ << ":" << __LINE__ << ": " << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "Cannot iterate around a cell in a one-dimensional mesh." << std::endl;
	exit (1);                                                                                      }


Mesh::Iterator::Core * Mesh::Connected::OneDim::iterator  // virtual from Mesh::Core
( const tag::OverCellsOfMaxDim &, const tag::OrientCompWithMesh &,
  const tag::Around &, Cell::Core *, const tag::Backwards &, const tag::ThisMeshIsPositive & )
{	std::cout << __FILE__ << ":" << __LINE__ << ": " << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "Cannot iterate around a cell in a one-dimensional mesh." << std::endl;
	exit (1);                                                                                      }


Mesh::Iterator::Core * Mesh::Connected::OneDim::iterator  // virtual from Mesh::Core
( const tag::OverCellsOfMaxDim &, const tag::OrientOpposToMesh &,
  const tag::Around &, Cell::Core *, const tag::ThisMeshIsPositive & )
{	std::cout << __FILE__ << ":" << __LINE__ << ": " << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "Cannot iterate around a cell in a one-dimensional mesh." << std::endl;
	exit (1);                                                                                      }


Mesh::Iterator::Core * Mesh::Connected::OneDim::iterator  // virtual from Mesh::Core
( const tag::OverCellsOfMaxDim &, const tag::OrientOpposToMesh &,
  const tag::Around &, Cell::Core *, const tag::RequireOrder &, const tag::ThisMeshIsPositive & )
{	std::cout << __FILE__ << ":" << __LINE__ << ": " << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "Cannot iterate around a cell in a one-dimensional mesh." << std::endl;
	exit (1);                                                                                      }


Mesh::Iterator::Core * Mesh::Connected::OneDim::iterator  // virtual from Mesh::Core
( const tag::OverCellsOfMaxDim &, const tag::OrientOpposToMesh &,
  const tag::Around &, Cell::Core *, const tag::Backwards &, const tag::ThisMeshIsPositive & )
{	std::cout << __FILE__ << ":" << __LINE__ << ": " << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "Cannot iterate around a cell in a one-dimensional mesh." << std::endl;
	exit (1);                                                                                      }


Mesh::Iterator::Core * Mesh::Connected::OneDim::iterator  // virtual from Mesh::Core
( const tag::OverCellsOfMaxDim &, const tag::AsFound &,
  const tag::Around &, Cell::Core *, const tag::ThisMeshIsPositive & )
{	std::cout << __FILE__ << ":" << __LINE__ << ": " << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "Cannot iterate around a cell in a one-dimensional mesh." << std::endl;
	exit (1);                                                                                      }

//------------------------------------------------------------------------------------------------------//
//------------------------------------------------------------------------------------------------------//


Mesh::Iterator::Core * Mesh::Fuzzy::iterator  // virtual from Mesh::Core
( const tag::OverVertices &, const tag::AsFound &, const tag::ThisMeshIsPositive & )
// since the cells are of low dimension, AsFound returns positive vertices
{	return  new Mesh::Iterator::OverCells::OfFuzzyMesh::AsFound ( this, tag::cells_of_dim, 0 );  }
// perhaps provide a different constructor for calling directly msh->cells.front(),
// it should be slightly faster


Mesh::Iterator::Core * Mesh::Fuzzy::iterator  // virtual from Mesh::Core
( const tag::OverVertices &, const tag::ForcePositive &, const tag::ThisMeshIsPositive & )
// fuzzy meshes have dimension at least one; thus, vertices are positive anyway
{	return  new Mesh::Iterator::OverCells::OfFuzzyMesh::AsFound ( this, tag::cells_of_dim, 0 );  }
// perhaps provide a different constructor for calling directly msh->cells.front(),
// it should be slightly faster


Mesh::Iterator::Core * Mesh::Fuzzy::iterator  // virtual from Mesh::Core
( const tag::OverVertices &, const tag::ThisMeshIsPositive & )
// fuzzy meshes have dimension at least one; thus, vertices are positive anyway
{	return  new Mesh::Iterator::OverCells::OfFuzzyMesh::AsFound ( this, tag::cells_of_dim, 0 );  }
// perhaps provide a different constructor for calling directly msh->cells.front(),
// it should be slightly faster


Mesh::Iterator::Core * Mesh::Fuzzy::iterator  // virtual from Mesh::Core
( const tag::OverVertices &, const tag::OrientCompWithMesh &, const tag::ThisMeshIsPositive & )
{	std::cout << __FILE__ << ":" << __LINE__ << ": " << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "vertices in a fuzzy mesh are positive" << std::endl;
	exit (1);                                                                                      }


Mesh::Iterator::Core * Mesh::Fuzzy::iterator  // virtual from Mesh::Core
( const tag::OverVertices &, const tag::OrientOpposToMesh &, const tag::ThisMeshIsPositive & )
{	std::cout << __FILE__ << ":" << __LINE__ << ": " << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "vertices in a fuzzy mesh are positive" << std::endl;
	exit (1);                                                                                      }


Mesh::Iterator::Core * Mesh::Fuzzy::iterator  // virtual from Mesh::Core
( const tag::OverVertices &, const tag::AsFound &,
  const tag::RequireOrder &, const tag::ThisMeshIsPositive & )
// should we implement an iterator over one-dimensional fuzzy meshes ?
// if yes, should we assume the mesh is connected ? or treat it as multiply connected ?
{	std::cout << __FILE__ << ":" << __LINE__ << ": " << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "It makes no sense to require order for a fuzzy mesh." << std::endl;
	exit (1);                                                                                      }


Mesh::Iterator::Core * Mesh::Fuzzy::iterator  // virtual from Mesh::Core
( const tag::OverVertices &, const tag::ForcePositive &,
  const tag::RequireOrder &, const tag::ThisMeshIsPositive & )
// should we implement an iterator over one-dimensional fuzzy meshes ?
// if yes, should we assume the mesh is connected ? or treat it as multiply connected ?
{	std::cout << __FILE__ << ":" << __LINE__ << ": " << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "It makes no sense to require order for a fuzzy mesh." << std::endl;
	exit (1);                                                                                      }


Mesh::Iterator::Core * Mesh::Fuzzy::iterator  // virtual from Mesh::Core
( const tag::OverVertices &, const tag::RequireOrder &, const tag::ThisMeshIsPositive & )
// should we implement an iterator over one-dimensional fuzzy meshes ?
// if yes, should we assume the mesh is connected ? or treat it as multiply connected ?
{	std::cout << __FILE__ << ":" << __LINE__ << ": " << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "It makes no sense to require order for a fuzzy mesh." << std::endl;
	exit (1);                                                                                      }


Mesh::Iterator::Core * Mesh::Fuzzy::iterator  // virtual from Mesh::Core
( const tag::OverVertices &, const tag::OrientCompWithMesh &,
  const tag::RequireOrder &, const tag::ThisMeshIsPositive & )
{	std::cout << __FILE__ << ":" << __LINE__ << ": " << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "vertices in a fuzzy mesh are positive" << std::endl;
	exit (1);                                                                                      }


Mesh::Iterator::Core * Mesh::Fuzzy::iterator  // virtual from Mesh::Core
( const tag::OverVertices &, const tag::OrientOpposToMesh &,
  const tag::RequireOrder &, const tag::ThisMeshIsPositive & )
{	std::cout << __FILE__ << ":" << __LINE__ << ": " << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "vertices in a fuzzy mesh are positive" << std::endl;
	exit (1);                                                                                      }


Mesh::Iterator::Core * Mesh::Fuzzy::iterator  // virtual from Mesh::Core
// should we implement an iterator over one-dimensional fuzzy meshes ?
// if yes, should we assume the mesh is connected ? or treat it as multiply connected ?
( const tag::OverVertices &, const tag::AsFound &,
  const tag::Backwards &, const tag::ThisMeshIsPositive & )
{	std::cout << __FILE__ << ":" << __LINE__ << ": " << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "It makes no sense to reverse order for a fuzzy mesh." << std::endl;
	exit (1);                                                                                      }


Mesh::Iterator::Core * Mesh::Fuzzy::iterator  // virtual from Mesh::Core
// should we implement an iterator over one-dimensional fuzzy meshes ?
// if yes, should we assume the mesh is connected ? or treat it as multiply connected ?
( const tag::OverVertices &, const tag::ForcePositive &,
  const tag::Backwards &, const tag::ThisMeshIsPositive & )
{	std::cout << __FILE__ << ":" << __LINE__ << ": " << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "It makes no sense to reverse order for a fuzzy mesh." << std::endl;
	exit (1);                                                                                      }


Mesh::Iterator::Core * Mesh::Fuzzy::iterator  // virtual from Mesh::Core
// should we implement an iterator over one-dimensional fuzzy meshes ?
// if yes, should we assume the mesh is connected ? or treat it as multiply connected ?
( const tag::OverVertices &, const tag::Backwards &, const tag::ThisMeshIsPositive & )
{	std::cout << __FILE__ << ":" << __LINE__ << ": " << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "It makes no sense to reverse order for a fuzzy mesh." << std::endl;
	exit (1);                                                                                      }


Mesh::Iterator::Core * Mesh::Fuzzy::iterator  // virtual from Mesh::Core
( const tag::OverVertices &, const tag::OrientCompWithMesh &,
  const tag::Backwards &, const tag::ThisMeshIsPositive & )
{	std::cout << __FILE__ << ":" << __LINE__ << ": " << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "vertices in a fuzzy mesh are positive" << std::endl;
	exit (1);                                                                                      }


Mesh::Iterator::Core * Mesh::Fuzzy::iterator  // virtual from Mesh::Core
( const tag::OverVertices &, const tag::OrientOpposToMesh &,
  const tag::Backwards &, const tag::ThisMeshIsPositive & )
{	std::cout << __FILE__ << ":" << __LINE__ << ": " << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "vertices in a fuzzy mesh are positive" << std::endl;
	exit (1);                                                                                      }

//------------------------------------------------------------------------------------------------------//


Mesh::Iterator::Core * Mesh::Fuzzy::iterator  // virtual from Mesh::Core
( const tag::OverSegments &, const tag::AsFound &, const tag::ThisMeshIsPositive & )

{	return  new Mesh::Iterator::OverCells::OfFuzzyMesh::AsFound ( this, tag::cells_of_dim, 1 );  }


Mesh::Iterator::Core * Mesh::Fuzzy::iterator  // virtual from Mesh::Core
( const tag::OverSegments &, const tag::ForcePositive &, const tag::ThisMeshIsPositive & )

{	if ( this->get_dim_plus_one() == 2 )
		return  new Mesh::Iterator::OverCells::OfFuzzyMesh::ForcePositive ( this, tag::cells_of_dim, 1 );
		// perhaps provide a different constructor for calling directly msh->cells.back(),
		// it should be slightly faster
	// else : dim >= 2, segments are positive anyway
	assert ( this->get_dim_plus_one() > 2 );
	return  new Mesh::Iterator::OverCells::OfFuzzyMesh::AsFound ( this, tag::cells_of_dim, 1 );  }


Mesh::Iterator::Core * Mesh::Fuzzy::iterator  // virtual from Mesh::Core
( const tag::OverSegments &, const tag::ThisMeshIsPositive & )
{	return  new Mesh::Iterator::OverCells::OfFuzzyMesh::AsFound ( this, tag::cells_of_dim, 1 );  }


Mesh::Iterator::Core * Mesh::Fuzzy::iterator  // virtual from Mesh::Core
( const tag::OverSegments &, const tag::OrientCompWithMesh &, const tag::ThisMeshIsPositive & )
{	assert ( this->get_dim_plus_one() == 2 );  // 1D mesh
	return  new Mesh::Iterator::OverCells::OfFuzzyMesh::AsFound ( this, tag::cells_of_dim, 1 );  }
// perhaps provide a different constructor for calling directly msh->cells.back(),
// it should be slightly faster


Mesh::Iterator::Core * Mesh::Fuzzy::iterator  // virtual from Mesh::Core
( const tag::OverSegments &, const tag::OrientOpposToMesh &, const tag::ThisMeshIsPositive & )
{	assert ( this->get_dim_plus_one() == 2 );  // 1D mesh
	return  new Mesh::Iterator::OverCells::OfFuzzyMesh::ReverseEachCell
		( this, tag::cells_of_dim, 1 );                                                }
// perhaps provide a different constructor for calling directly msh->cells.back(),
// it should be slightly faster


Mesh::Iterator::Core * Mesh::Fuzzy::iterator  // virtual from Mesh::Core
( const tag::OverSegments &, const tag::AsFound &,
  const tag::RequireOrder &, const tag::ThisMeshIsPositive & )
// should we implement an iterator over one-dimensional fuzzy meshes ?
// if yes, should we assume the mesh is connected ? or treat it as multiply connected ?

{	std::cout << __FILE__ << ":" << __LINE__ << ": " << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "It makes no sense to require order for a fuzzy mesh." << std::endl;
	exit (1);                                                                                      }


Mesh::Iterator::Core * Mesh::Fuzzy::iterator  // virtual from Mesh::Core
( const tag::OverSegments &, const tag::ForcePositive &,
  const tag::RequireOrder &, const tag::ThisMeshIsPositive & )
// should we implement an iterator over one-dimensional fuzzy meshes ?
// if yes, should we assume the mesh is connected ? or treat it as multiply connected ?

{	std::cout << __FILE__ << ":" << __LINE__ << ": " << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "It makes no sense to require order for a fuzzy mesh." << std::endl;
	exit (1);                                                                                      }


Mesh::Iterator::Core * Mesh::Fuzzy::iterator  // virtual from Mesh::Core
( const tag::OverSegments &, const tag::RequireOrder &, const tag::ThisMeshIsPositive & )
// should we implement an iterator over one-dimensional fuzzy meshes ?
// if yes, should we assume the mesh is connected ? or treat it as multiply connected ?

{	std::cout << __FILE__ << ":" << __LINE__ << ": " << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "It makes no sense to require order for a fuzzy mesh." << std::endl;
	exit (1);                                                                                      }


Mesh::Iterator::Core * Mesh::Fuzzy::iterator  // virtual from Mesh::Core
( const tag::OverSegments &, const tag::OrientCompWithMesh &,
  const tag::RequireOrder &, const tag::ThisMeshIsPositive & )
// should we implement an iterator over one-dimensional fuzzy meshes ?
// if yes, should we assume the mesh is connected ? or treat it as multiply connected ?

{	std::cout << __FILE__ << ":" << __LINE__ << ": " << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "It makes no sense to require order for a fuzzy mesh." << std::endl;
	exit (1);                                                                                      }


Mesh::Iterator::Core * Mesh::Fuzzy::iterator  // virtual from Mesh::Core
( const tag::OverSegments &, const tag::OrientOpposToMesh &,
  const tag::RequireOrder &, const tag::ThisMeshIsPositive & )
// should we implement an iterator over one-dimensional fuzzy meshes ?
// if yes, should we assume the mesh is connected ? or treat it as multiply connected ?

{	std::cout << __FILE__ << ":" << __LINE__ << ": " << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "It makes no sense to require order for a fuzzy mesh." << std::endl;
	exit (1);                                                                                      }


Mesh::Iterator::Core * Mesh::Fuzzy::iterator  // virtual from Mesh::Core
( const tag::OverSegments &, const tag::AsFound &,
  const tag::Backwards &, const tag::ThisMeshIsPositive & )
// should we implement an iterator over one-dimensional fuzzy meshes ?
// if yes, should we assume the mesh is connected ? or treat it as multiply connected ?

{	std::cout << __FILE__ << ":" << __LINE__ << ": " << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "It makes no sense to reverse order for a fuzzy mesh." << std::endl;
	exit (1);                                                                                      }


Mesh::Iterator::Core * Mesh::Fuzzy::iterator  // virtual from Mesh::Core
( const tag::OverSegments &, const tag::ForcePositive &,
  const tag::Backwards &, const tag::ThisMeshIsPositive & )
// should we implement an iterator over one-dimensional fuzzy meshes ?
// if yes, should we assume the mesh is connected ? or treat it as multiply connected ?

{	std::cout << __FILE__ << ":" << __LINE__ << ": " << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "It makes no sense to reverse order for a fuzzy mesh." << std::endl;
	exit (1);                                                                                      }


Mesh::Iterator::Core * Mesh::Fuzzy::iterator  // virtual from Mesh::Core
( const tag::OverSegments &, const tag::Backwards &, const tag::ThisMeshIsPositive & )
// should we implement an iterator over one-dimensional fuzzy meshes ?
// if yes, should we assume the mesh is connected ? or treat it as multiply connected ?

{	std::cout << __FILE__ << ":" << __LINE__ << ": " << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "It makes no sense to reverse order for a fuzzy mesh." << std::endl;
	exit (1);                                                                                      }


Mesh::Iterator::Core * Mesh::Fuzzy::iterator  // virtual from Mesh::Core
( const tag::OverSegments &, const tag::OrientCompWithMesh &,
  const tag::Backwards &, const tag::ThisMeshIsPositive & )
// should we implement an iterator over one-dimensional fuzzy meshes ?
// if yes, should we assume the mesh is connected ? or treat it as multiply connected ?

{	std::cout << __FILE__ << ":" << __LINE__ << ": " << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "It makes no sense to reverse order for a fuzzy mesh." << std::endl;
	exit (1);                                                                                      }


Mesh::Iterator::Core * Mesh::Fuzzy::iterator  // virtual from Mesh::Core
( const tag::OverSegments &, const tag::OrientOpposToMesh &,
  const tag::Backwards &, const tag::ThisMeshIsPositive & )
// should we implement an iterator over one-dimensional fuzzy meshes ?
// if yes, should we assume the mesh is connected ? or treat it as multiply connected ?

{	std::cout << __FILE__ << ":" << __LINE__ << ": " << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "It makes no sense to reverse order for a fuzzy mesh." << std::endl;
	exit (1);                                                                                      }


//------------------------------------------------------------------------------------------------------//


Mesh::Iterator::Core * Mesh::Fuzzy::iterator  // virtual from Mesh::Core
( const tag::OverCellsOfDim &, const size_t d, const tag::AsFound &, const tag::ThisMeshIsPositive & )
// if d == this->dim(), return oriented cells
// if d < this->dim(),  return positive cells
{	assert ( this->get_dim_plus_one() > d );
	return  new Mesh::Iterator::OverCells::OfFuzzyMesh::AsFound ( this, tag::cells_of_dim, d );  }


Mesh::Iterator::Core * Mesh::Fuzzy::iterator  // virtual from Mesh::Core
( const tag::OverCellsOfDim &, const size_t d,
  const tag::CellHasLowDim &, const tag::ThisMeshIsPositive & )
// d < this->dim(),  return positive cells
{	assert ( d + 1 < this->get_dim_plus_one() );
	return  new Mesh::Iterator::OverCells::OfFuzzyMesh::AsFound ( this, tag::cells_of_dim, d );  }


Mesh::Iterator::Core * Mesh::Fuzzy::iterator  // virtual from Mesh::Core
( const tag::OverCellsOfDim &, const size_t d,
  const tag::ForcePositive &, const tag::ThisMeshIsPositive & )
{	assert ( this->get_dim_plus_one() > d );
	if ( this->get_dim_plus_one() == d+1 )
		return  new Mesh::Iterator::OverCells::OfFuzzyMesh::ForcePositive ( this, tag::cells_of_dim, d );
// perhaps provide a different constructor for calling directly msh->cells.back(),
// it should be slightly faster
	// else : cell dim < mesh dim, cells are positive anyway
	return  new Mesh::Iterator::OverCells::OfFuzzyMesh::AsFound ( this, tag::cells_of_dim, d );  }


Mesh::Iterator::Core * Mesh::Fuzzy::iterator  // virtual from Mesh::Core
( const tag::OverCellsOfDim &, const size_t d, const tag::ThisMeshIsPositive & )
// if this->dim() == d, return oriented cells
// if d < this->dim(),  return positive cells
{	assert ( this->get_dim_plus_one() > d );
	return  new Mesh::Iterator::OverCells::OfFuzzyMesh::AsFound ( this, tag::cells_of_dim, d );  }


Mesh::Iterator::Core * Mesh::Fuzzy::iterator  // virtual from Mesh::Core
( const tag::OverCellsOfDim &, const size_t d, const tag::OrientCompWithMesh &,
  const tag::ThisMeshIsPositive &                                              )
{	assert ( this->get_dim_plus_one() == d+1 );
	return  new Mesh::Iterator::OverCells::OfFuzzyMesh::AsFound ( this, tag::cells_of_dim, d );  }
// perhaps provide a different constructor for calling directly msh->cells.back(),
// it should be slightly faster


Mesh::Iterator::Core * Mesh::Fuzzy::iterator  // virtual from Mesh::Core
( const tag::OverCellsOfDim &, const size_t d, const tag::OrientOpposToMesh &,
  const tag::ThisMeshIsPositive &                                             )
{	assert ( this->get_dim_plus_one() == d+1 );
	return  new Mesh::Iterator::OverCells::OfFuzzyMesh::ReverseEachCell
		( this, tag::cells_of_dim, d );                                               }
// perhaps provide a different constructor for calling directly msh->cells.back(),
// it should be slightly faster


Mesh::Iterator::Core * Mesh::Fuzzy::iterator  // virtual from Mesh::Core
( const tag::OverCellsOfDim &, const size_t d, const tag::AsFound &,
  const tag::RequireOrder &, const tag::ThisMeshIsPositive &          )
{	std::cout << __FILE__ << ":" << __LINE__ << ": " << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "It makes no sense to require order for a fuzzy mesh." << std::endl;
	exit (1);                                                                                      }


Mesh::Iterator::Core * Mesh::Fuzzy::iterator  // virtual from Mesh::Core
( const tag::OverCellsOfDim &, const size_t d, const tag::ForcePositive &,
  const tag::RequireOrder &, const tag::ThisMeshIsPositive &              )
{	std::cout << __FILE__ << ":" << __LINE__ << ": " << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "It makes no sense to require order for a fuzzy mesh." << std::endl;
	exit (1);                                                                                      }


Mesh::Iterator::Core * Mesh::Fuzzy::iterator  // virtual from Mesh::Core
( const tag::OverCellsOfDim &, const size_t d,
  const tag::RequireOrder &, const tag::ThisMeshIsPositive &          )
{	std::cout << __FILE__ << ":" << __LINE__ << ": " << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "It makes no sense to require order for a fuzzy mesh." << std::endl;
	exit (1);                                                                                      }


Mesh::Iterator::Core * Mesh::Fuzzy::iterator  // virtual from Mesh::Core
( const tag::OverCellsOfDim &, const size_t d, const tag::OrientCompWithMesh &,
  const tag::RequireOrder &, const tag::ThisMeshIsPositive &                   )
{	std::cout << __FILE__ << ":" << __LINE__ << ": " << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "It makes no sense to require order for a fuzzy mesh." << std::endl;
	exit (1);                                                                                      }


Mesh::Iterator::Core * Mesh::Fuzzy::iterator  // virtual from Mesh::Core
( const tag::OverCellsOfDim &, const size_t d, const tag::OrientOpposToMesh &,
  const tag::RequireOrder &, const tag::ThisMeshIsPositive &                  )
{	std::cout << __FILE__ << ":" << __LINE__ << ": " << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "It makes no sense to require order for a fuzzy mesh." << std::endl;
	exit (1);                                                                                      }


Mesh::Iterator::Core * Mesh::Fuzzy::iterator  // virtual from Mesh::Core
( const tag::OverCellsOfDim &, const size_t d, const tag::AsFound &,
  const tag::Backwards &, const tag::ThisMeshIsPositive &        )
{	std::cout << __FILE__ << ":" << __LINE__ << ": " << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "It makes no sense to reverse order for a fuzzy mesh." << std::endl;
	exit (1);                                                                                      }


Mesh::Iterator::Core * Mesh::Fuzzy::iterator  // virtual from Mesh::Core
( const tag::OverCellsOfDim &, const size_t d, const tag::ForcePositive &,
  const tag::Backwards &, const tag::ThisMeshIsPositive &              )
{	std::cout << __FILE__ << ":" << __LINE__ << ": " << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "It makes no sense to reverse order for a fuzzy mesh." << std::endl;
	exit (1);                                                                                      }


Mesh::Iterator::Core * Mesh::Fuzzy::iterator  // virtual from Mesh::Core
( const tag::OverCellsOfDim &, const size_t d,
  const tag::Backwards &, const tag::ThisMeshIsPositive & )
{	std::cout << __FILE__ << ":" << __LINE__ << ": " << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "It makes no sense to reverse order for a fuzzy mesh." << std::endl;
	exit (1);                                                                                      }


Mesh::Iterator::Core * Mesh::Fuzzy::iterator  // virtual from Mesh::Core
( const tag::OverCellsOfDim &, const size_t d, const tag::OrientCompWithMesh &,
  const tag::Backwards &, const tag::ThisMeshIsPositive &                   )
{	std::cout << __FILE__ << ":" << __LINE__ << ": " << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "It makes no sense to reverse order for a fuzzy mesh." << std::endl;
	exit (1);                                                                                      }


Mesh::Iterator::Core * Mesh::Fuzzy::iterator  // virtual from Mesh::Core
( const tag::OverCellsOfDim &, const size_t d, const tag::OrientOpposToMesh &,
  const tag::Backwards &, const tag::ThisMeshIsPositive &                  )
{	std::cout << __FILE__ << ":" << __LINE__ << ": " << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "It makes no sense to reverse order for a fuzzy mesh." << std::endl;
	exit (1);                                                                                      }

//------------------------------------------------------------------------------------------------------//


Mesh::Iterator::Core * Mesh::Fuzzy::iterator  // virtual from Mesh::Core
( const tag::OverCellsOfMaxDim &, const tag::AsFound &, const tag::ThisMeshIsPositive & )
{	return  new Mesh::Iterator::OverCells::OfFuzzyMesh::AsFound
		( this, tag::cells_of_dim, tag::Util::assert_diff ( this->get_dim_plus_one(), 1 ) );  }
// perhaps provide a different constructor for calling directly msh->cells.back(),
// it should be slightly faster


Mesh::Iterator::Core * Mesh::Fuzzy::iterator  // virtual from Mesh::Core
( const tag::OverCellsOfMaxDim &, const tag::ForcePositive &, const tag::ThisMeshIsPositive & )
{	return  new Mesh::Iterator::OverCells::OfFuzzyMesh::ForcePositive
		( this, tag::cells_of_dim, tag::Util::assert_diff ( this->get_dim_plus_one(), 1 ) );  }
// perhaps provide a different constructor for calling directly msh->cells.back(),
// it should be slightly faster
	

Mesh::Iterator::Core * Mesh::Fuzzy::iterator  // virtual from Mesh::Core
( const tag::OverCellsOfMaxDim &, const tag::OrientCompWithMesh &, const tag::ThisMeshIsPositive & )
{	return  new Mesh::Iterator::OverCells::OfFuzzyMesh::AsFound
		( this, tag::cells_of_dim, tag::Util::assert_diff ( this->get_dim_plus_one(), 1 ) );  }
// perhaps provide a different constructor for calling directly msh->cells.back(),
// it should be slightly faster


Mesh::Iterator::Core * Mesh::Fuzzy::iterator  // virtual from Mesh::Core
( const tag::OverCellsOfMaxDim &, const tag::OrientOpposToMesh &, const tag::ThisMeshIsPositive & )
{	return  new Mesh::Iterator::OverCells::OfFuzzyMesh::ReverseEachCell
		( this, tag::cells_of_dim, tag::Util::assert_diff ( this->get_dim_plus_one(), 1 ) );  }
// perhaps provide a different constructor for calling directly msh->cells.back(),
// it should be slightly faster


Mesh::Iterator::Core * Mesh::Fuzzy::iterator  // virtual from Mesh::Core
( const tag::OverCellsOfMaxDim &, const tag::AsFound &,
  const tag::RequireOrder &, const tag::ThisMeshIsPositive & )
{	std::cout << __FILE__ << ":" << __LINE__ << ": " << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "It makes no sense to require order for a fuzzy mesh." << std::endl;
	exit (1);                                                                                      }


Mesh::Iterator::Core * Mesh::Fuzzy::iterator  // virtual from Mesh::Core
( const tag::OverCellsOfMaxDim &, const tag::ForcePositive &,
  const tag::RequireOrder &, const tag::ThisMeshIsPositive & )
{	std::cout << __FILE__ << ":" << __LINE__ << ": " << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "It makes no sense to require order for a fuzzy mesh." << std::endl;
	exit (1);                                                                                      }


Mesh::Iterator::Core * Mesh::Fuzzy::iterator  // virtual from Mesh::Core
( const tag::OverCellsOfMaxDim &, const tag::OrientCompWithMesh &,
  const tag::RequireOrder &, const tag::ThisMeshIsPositive &      )
{	std::cout << __FILE__ << ":" << __LINE__ << ": " << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "It makes no sense to require order for a fuzzy mesh." << std::endl;
	exit (1);                                                                                      }


Mesh::Iterator::Core * Mesh::Fuzzy::iterator  // virtual from Mesh::Core
( const tag::OverCellsOfMaxDim &, const tag::OrientOpposToMesh &,
  const tag::RequireOrder &, const tag::ThisMeshIsPositive &     )
{	std::cout << __FILE__ << ":" << __LINE__ << ": " << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "It makes no sense to require order for a fuzzy mesh." << std::endl;
	exit (1);                                                                                      }


Mesh::Iterator::Core * Mesh::Fuzzy::iterator  // virtual from Mesh::Core
( const tag::OverCellsOfMaxDim &, const tag::AsFound &,
  const tag::Backwards &, const tag::ThisMeshIsPositive & )
{	std::cout << __FILE__ << ":" << __LINE__ << ": " << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "It makes no sense to reverse order for a fuzzy mesh." << std::endl;
	exit (1);                                                                                      }


Mesh::Iterator::Core * Mesh::Fuzzy::iterator  // virtual from Mesh::Core
( const tag::OverCellsOfMaxDim &, const tag::ForcePositive &,
  const tag::Backwards &, const tag::ThisMeshIsPositive & )
{	std::cout << __FILE__ << ":" << __LINE__ << ": " << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "It makes no sense to reverse order for a fuzzy mesh." << std::endl;
	exit (1);                                                                                      }


Mesh::Iterator::Core * Mesh::Fuzzy::iterator  // virtual from Mesh::Core
( const tag::OverCellsOfMaxDim &, const tag::OrientCompWithMesh &,
  const tag::Backwards &, const tag::ThisMeshIsPositive &      )
{	std::cout << __FILE__ << ":" << __LINE__ << ": " << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "It makes no sense to reverse order for a fuzzy mesh." << std::endl;
	exit (1);                                                                                      }


Mesh::Iterator::Core * Mesh::Fuzzy::iterator  // virtual from Mesh::Core
( const tag::OverCellsOfMaxDim &, const tag::OrientOpposToMesh &,
  const tag::Backwards &, const tag::ThisMeshIsPositive &     )
{	std::cout << __FILE__ << ":" << __LINE__ << ": " << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "It makes no sense to reverse order for a fuzzy mesh." << std::endl;
	exit (1);                                                                                      }

//------------------------------------------------------------------------------------------------------//


Mesh::Iterator::Core * Mesh::Fuzzy::iterator  // virtual from Mesh::Core
( const tag::OverVertices &, const tag::ConnectedTo &, Cell::Positive::Vertex * c,
  const tag::ThisMeshIsPositive &                                                 )
{	assert ( c->belongs_to_ld ( this, tag::cell_has_low_dim ) );
	assert ( this->get_dim_plus_one() > 2 );  // 2D or 3D mesh
	return  new Mesh::Iterator::OverVertices::ConnectedToCenter ( this, c );  }


Mesh::Iterator::Core * Mesh::Fuzzy::iterator  // virtual from Mesh::Core
( const tag::OverVertices &, const tag::ConnectedTo &, Cell::Positive::Vertex * c,
  const tag::RequireOrder &, const tag::ThisMeshIsPositive &                      )
{	assert ( c->belongs_to_ld ( this, tag::cell_has_low_dim ) );
	assert ( this->get_dim_plus_one() == 3 );  // 2D mesh
	return  new Mesh::Iterator::OverVertices::ConnectedToCenter::Ordered ( this, c );  }


Mesh::Iterator::Core * Mesh::Fuzzy::iterator  // virtual from Mesh::Core
( const tag::OverSegments &, const tag::WhoseTipIs &, Cell::Positive::Vertex * const c,
  const tag::ThisMeshIsPositive &                                                      )
{	assert ( c->belongs_to_ld ( this, tag::cell_has_low_dim ) );
	assert ( this->get_dim_plus_one() > 2 );  // 2D or 3D mesh
	return  new Mesh::Iterator::OverSegments::WhoseTipIsC ( this, c );  }


Mesh::Iterator::Core * Mesh::Fuzzy::iterator  // virtual from Mesh::Core
( const tag::OverSegments &, const tag::WhoseBaseIs &, Cell::Positive::Vertex * const c,
  const tag::ThisMeshIsPositive &                                                       )
{	assert ( c->belongs_to_ld ( this, tag::cell_has_low_dim ) );
	assert ( this->get_dim_plus_one() > 2 );  // 2D or 3D mesh
	return  new Mesh::Iterator::OverSegments::WhoseTipIsC::ReverseEachCell ( this, c );  }


Mesh::Iterator::Core * Mesh::Fuzzy::iterator  // virtual from Mesh::Core
( const tag::OverSegments &, const tag::WhoseTipIs &, Cell::Positive::Vertex * const c,
  const tag::RequireOrder &, const tag::ThisMeshIsPositive &                           )
{	assert ( c->belongs_to_ld ( this, tag::cell_has_low_dim ) );
	assert ( this->get_dim_plus_one() == 3 );  // 2D mesh
	return  new Mesh::Iterator::Around::OneCell::OfCodimTwo
	                ::OverCoVertices::NormalOrder::BuildReverseCells::AsFound ( this, c );  }


Mesh::Iterator::Core * Mesh::Fuzzy::iterator  // virtual from Mesh::Core
( const tag::OverSegments &, const tag::WhoseBaseIs &, Cell::Positive::Vertex * const c,
  const tag::RequireOrder &, const tag::ThisMeshIsPositive &                            )
{	assert ( c->belongs_to_ld ( this, tag::cell_has_low_dim ) );
	assert ( this->get_dim_plus_one() == 3 );  // 2D mesh
	return  new Mesh::Iterator::Around::OneCell::OfCodimTwo
	                ::OverCoVertices::NormalOrder::BuildReverseCells::ReverseEachCell ( this, c );  }


Mesh::Iterator::Core * Mesh::Fuzzy::iterator  // virtual from Mesh::Core
( const tag::OverSegments &, const tag::WhoseTipIs &, Cell::Positive::Vertex * const c,
  const tag::Backwards &, const tag::ThisMeshIsPositive &                              )
{	assert ( c->belongs_to_ld ( this, tag::cell_has_low_dim ) );
	assert ( this->get_dim_plus_one() == 3 );  // 2D mesh
	return  new Mesh::Iterator::Around::OneCell::OfCodimTwo
	                ::OverCoVertices::ReverseOrder::BuildReverseCells::AsFound ( this, c );  }


Mesh::Iterator::Core * Mesh::Fuzzy::iterator  // virtual from Mesh::Core
( const tag::OverSegments &, const tag::WhoseBaseIs &, Cell::Positive::Vertex * const c,
  const tag::Backwards &, const tag::ThisMeshIsPositive &                               )
{	assert ( c->belongs_to_ld ( this, tag::cell_has_low_dim ) );
	assert ( this->get_dim_plus_one() == 3 );  // 2D mesh
	return  new Mesh::Iterator::Around::OneCell::OfCodimTwo
	                ::OverCoVertices::ReverseOrder::BuildReverseCells::ReverseEachCell ( this, c );  }


Mesh::Iterator::Core * Mesh::Fuzzy::iterator  // virtual from Mesh::Core
( const tag::OverSegments &, const tag::AsFound &, const tag::Around &, Cell::Positive::Vertex * c,
  const tag::ThisMeshIsPositive &                                                                  )
{	assert ( c->belongs_to_ld ( this, tag::cell_has_low_dim ) );
	assert ( this->get_dim_plus_one() > 2 );  // 2D or 3D mesh
	return  new Mesh::Iterator::Around::OneVertex::ReturnSegments::AsFound ( this, c );  }


Mesh::Iterator::Core * Mesh::Fuzzy::iterator  // virtual from Mesh::Core
( const tag::OverSegments &, const tag::AsFound &, const tag::Around &, Cell::Positive::Vertex * c,
  const tag::RequireOrder &, const tag::ThisMeshIsPositive &                                       )
{	assert ( c->belongs_to_ld ( this, tag::cell_has_low_dim ) );
	assert ( this->get_dim_plus_one() == 3 );  // 2D mesh
	return  new Mesh::Iterator::Around::OneCell::OfCodimTwo
		::OverCoVertices::NormalOrder::BuildReverseCells::AsFound ( this, c );  }


Mesh::Iterator::Core * Mesh::Fuzzy::iterator  // virtual from Mesh::Core
( const tag::OverSegments &, const tag::AsFound &, const tag::Around &, Cell::Positive::Vertex * c,
  const tag::Backwards &, const tag::ThisMeshIsPositive &                                       )
{	assert ( c->belongs_to_ld ( this, tag::cell_has_low_dim ) );
	assert ( this->get_dim_plus_one() == 3 );  // 2D mesh
	return  new Mesh::Iterator::Around::OneCell::OfCodimTwo
		::OverCoVertices::ReverseOrder::BuildReverseCells::AsFound ( this, c );  }


Mesh::Iterator::Core * Mesh::Fuzzy::iterator  // virtual from Mesh::Core
( const tag::OverSegments &, const tag::ForcePositive &, const tag::Around &, Cell::Positive::Vertex * c,
  const tag::ThisMeshIsPositive &                                                                        )
// in the future, change to Mesh::Iterator::Around::OneVertex::OfAnyCodim::OverSegments
{	assert ( c->belongs_to_ld ( this, tag::cell_has_low_dim ) );
	assert ( this->get_dim_plus_one() == 3 );  // 2D mesh
	return  new Mesh::Iterator::Around::OneCell::OfCodimTwo
		::OverCoVertices::NormalOrder::BuildReverseCells::ForcePositive ( this, c );  }


Mesh::Iterator::Core * Mesh::Fuzzy::iterator  // virtual from Mesh::Core
( const tag::OverSegments &, const tag::ForcePositive &, const tag::Around &, Cell::Positive::Vertex * c,
  const tag::RequireOrder &, const tag::ThisMeshIsPositive &                                             )
{	assert ( c->belongs_to_ld ( this, tag::cell_has_low_dim ) );
	assert ( this->get_dim_plus_one() == 3 );  // 2D mesh
	return  new Mesh::Iterator::Around::OneCell::OfCodimTwo
		::OverCoVertices::NormalOrder::BuildReverseCells::ForcePositive ( this, c );  }


Mesh::Iterator::Core * Mesh::Fuzzy::iterator  // virtual from Mesh::Core
( const tag::OverSegments &, const tag::ForcePositive &, const tag::Around &, Cell::Positive::Vertex * c,
  const tag::Backwards &, const tag::ThisMeshIsPositive &                                             )
{	assert ( c->belongs_to_ld ( this, tag::cell_has_low_dim ) );
	assert ( this->get_dim_plus_one() == 3 );  // 2D mesh
	return  new Mesh::Iterator::Around::OneCell::OfCodimTwo
		::OverCoVertices::ReverseOrder::BuildReverseCells::ForcePositive ( this, c );  }

//------------------------------------------------------------------------------------------------------//


Mesh::Iterator::Core * Mesh::Fuzzy::iterator  // virtual from Mesh::Core
( const tag::OverCellsOfDim &, const size_t d, const tag::AsFound &,
  const tag::Around &, Cell::Core * c, const tag::ThisMeshIsPositive & )

// since no order is required, we return an iterator which does not use neighbourhood relations
// this is important if the mesh is STSI since in this case
// following the neighbourhood relations may not produce all neighbours
// it makes no difference for a usual Fuzzy mesh
// but may be important for strangely configured Fuzzy meshes,
// e.g. for a 2D mesh having two triangles touching at a vertex
	
{	assert ( c->belongs_to_ld ( this, tag::cell_has_low_dim ) );
	assert ( c->get_dim() < d );
	assert ( d < this->get_dim_plus_one() );
	assert ( d >= 2 );  // for segments, a different iterator is invoked
	Cell::Positive * const cc = tag::Util::assert_cast
		< Cell::Core * const, Cell::Positive * const > ( c->get_positive() .core );
	if ( cc->get_dim() + 2 == this->get_dim_plus_one() )  // center has co-dimension one
	{	assert ( d == cc->get_dim() + 1 );
		return  new Mesh::Iterator::Around::OneCell::
		            OfCodimOne::ReturnCellsOfMaxDim::AsFound ( this, cc );  }
	// else  // center has co-dimension at least two
	if ( d == cc->get_dim() + 1 )
		return  new Mesh::Iterator::Around::OneCell::OfCodimAtLeastTwo::
		            ReturnCellsOfDimPlusOne::AsFound ( this, cc );
	// else  // d >= cc->get_dim() + 2
	if ( d + 1 == this->get_dim_plus_one() )
		return  new Mesh::Iterator::Around::OneCell::OfCodimAtLeastTwo::
		            ReturnCellsOfMaxDim::AsFound ( this, cc );
	// else
	return  new Mesh::Iterator::Around::OneCell::OfCodimAtLeastTwo::
	            ReturnCellsOfHighDim::AsFound ( this, cc, d );                          }
	

Mesh::Iterator::Core * Mesh::Fuzzy::iterator  // virtual from Mesh::Core
( const tag::OverCellsOfDim &, const size_t d, const tag::AsFound &,
  const tag::Around &, Cell::Core * c, const tag::RequireOrder &, const tag::ThisMeshIsPositive & )

// here "as found" is equivalent with "orientation compatible with mesh"

{	assert ( c->belongs_to_ld ( this, tag::cell_has_low_dim ) );
	assert ( c->get_dim() + 3 == this->get_dim_plus_one() );  // co-dimension == 2
	assert ( c->get_dim() < d );
	assert ( d < this->get_dim_plus_one() );
	if ( d == c->get_dim() + 1 )
		return  new Mesh::Iterator::Around::OneCell::OfCodimTwo
		            ::OverCoVertices::NormalOrder::BuildReverseCells::AsFound ( this,
			tag::Util::assert_cast < Cell::Core * const, Cell::Positive * const> ( c ) );
	else
	{	assert ( d == c->get_dim() + 2 );
		return  new Mesh::Iterator::Around::OneCell::OfCodimTwo
		            ::OverCoSegments::NormalOrder::AsFound ( this,
      tag::Util::assert_cast < Cell::Core * const, Cell::Positive * const> ( c ) );  }  }
 

Mesh::Iterator::Core * Mesh::Fuzzy::iterator  // virtual from Mesh::Core
( const tag::OverCellsOfDim &, const size_t d, const tag::AsFound &,
  const tag::Around &, Cell::Core * c, const tag::Backwards &, const tag::ThisMeshIsPositive & )

// here "as found" is equivalent with "orientation compatible with mesh"
	
{	assert ( c->belongs_to_ld ( this, tag::cell_has_low_dim ) );
	assert ( c->get_dim() + 3 == this->get_dim_plus_one() );  // co-dimension == 2
	assert ( c->get_dim() < d );
	assert ( d < this->get_dim_plus_one() );
	if ( d == c->get_dim() + 1 )
		return  new Mesh::Iterator::Around::OneCell::OfCodimTwo
		            ::OverCoVertices::ReverseOrder::BuildReverseCells::AsFound ( this,
			tag::Util::assert_cast < Cell::Core * const, Cell::Positive * const> ( c ) );
	else
	{	assert ( d == c->get_dim() + 2 );
		return  new Mesh::Iterator::Around::OneCell::OfCodimTwo
		            ::OverCoSegments::ReverseOrder::AsFound ( this,
        tag::Util::assert_cast < Cell::Core * const, Cell::Positive * const> ( c ) );  }  }


Mesh::Iterator::Core * Mesh::Fuzzy::iterator  // virtual from Mesh::Core
( const tag::OverCellsOfDim &, const size_t d, const tag::ForcePositive &,
  const tag::Around &, Cell::Core * c, const tag::ThisMeshIsPositive &    )

{	assert ( c->belongs_to_ld ( this, tag::cell_has_low_dim ) );
	assert ( c->get_dim() + 2 < this->get_dim_plus_one() );  // co-dimension >= 2
	assert ( c->get_dim() < d );
	assert ( d < this->get_dim_plus_one() );
	// Mesh::Iterators OfAnyCodim are not implemented yet, so ...
	assert ( c->get_dim() + 3 == this->get_dim_plus_one() );  // co-dimension == 2
	if ( d == c->get_dim() + 1 )
		return  new Mesh::Iterator::Around::OneCell::OfCodimTwo
		            ::OverCoVertices::NormalOrder::BuildReverseCells::ForcePositive
			( this, tag::Util::assert_cast < Cell::Core * const, Cell::Positive * const> ( c ) );
	// else
	assert ( d == c->get_dim() + 2 );
	return  new Mesh::Iterator::Around::OneCell::OfCodimTwo
	//    ::OverSegments::NormalOrder::ForcePositive ( this,
	            ::OverCoSegments::NormalOrder::AsFound ( this,
       tag::Util::assert_cast < Cell::Core * const, Cell::Positive * const> ( c ) );         }


Mesh::Iterator::Core * Mesh::Fuzzy::iterator  // virtual from Mesh::Core
( const tag::OverCellsOfDim &, const size_t d, const tag::ForcePositive &,
  const tag::Around &, Cell::Core * c, const tag::RequireOrder &, const tag::ThisMeshIsPositive & )

{	assert ( c->belongs_to_ld ( this, tag::cell_has_low_dim ) );
	assert ( c->get_dim() + 3 == this->get_dim_plus_one() );  // co-dimension == 2
	assert ( c->get_dim() < d );
	assert ( d < this->get_dim_plus_one() );
	if ( d == c->get_dim() + 1 )
		return  new Mesh::Iterator::Around::OneCell::OfCodimTwo
		            ::OverCoVertices::NormalOrder::BuildReverseCells::ForcePositive ( this,
			tag::Util::assert_cast < Cell::Core * const, Cell::Positive * const> ( c ) );
	// else
	assert ( d == c->get_dim() + 2 );
	return  new Mesh::Iterator::Around::OneCell::OfCodimTwo
	//    ::OverSegments::NormalOrder::ForcePositive ( this,
	            ::OverCoSegments::NormalOrder::AsFound ( this,
       tag::Util::assert_cast < Cell::Core * const, Cell::Positive * const> ( c ) );      }


Mesh::Iterator::Core * Mesh::Fuzzy::iterator  // virtual from Mesh::Core
( const tag::OverCellsOfDim &, const size_t d, const tag::ForcePositive &,
  const tag::Around &, Cell::Core * c, const tag::Backwards &, const tag::ThisMeshIsPositive & )

{	assert ( c->belongs_to_ld ( this, tag::cell_has_low_dim ) );
	assert ( c->get_dim() + 3 == this->get_dim_plus_one() );  // co-dimension == 2
	assert ( c->get_dim() < d );
	assert ( d < this->get_dim_plus_one() );
	if ( d == c->get_dim() + 1 )
		return  new Mesh::Iterator::Around::OneCell::OfCodimTwo
		         ::OverCoVertices::ReverseOrder::BuildReverseCells::ForcePositive ( this,
			tag::Util::assert_cast < Cell::Core * const, Cell::Positive * const> ( c ) );
	// else
	assert ( d == c->get_dim() + 2 );
	return  new Mesh::Iterator::Around::OneCell::OfCodimTwo
	//    ::OverSegments::ReverseOrder::ForcePositive ( this,
	            ::OverCoSegments::ReverseOrder::AsFound ( this,
       tag::Util::assert_cast < Cell::Core * const, Cell::Positive * const> ( c ) );     }


Mesh::Iterator::Core * Mesh::Fuzzy::iterator  // virtual from Mesh::Core
( const tag::OverCellsOfDim &, const size_t d, const tag::OrientCompWithCenter &,
  const tag::Around &, Cell::Core * c, const tag::ThisMeshIsPositive &           )

// since no order is required, we return an iterator which does not use neighbourhood relations
// this is important if the mesh is STSI since in this case
// following the neighbourhood relations may not produce all neighbours
// it makes no difference for a usual Fuzzy mesh
// but may be important for strangely configured Fuzzy meshes,
// e.g. for a 2D mesh having two triangles touching at a vertex

{	assert ( c->belongs_to_ld ( this, tag::cell_has_low_dim ) );
	assert ( c->get_dim() < d );
	assert ( d < this->get_dim_plus_one() );
	assert ( d >= 2 );  // for segments, a different iterator is invoked
	assert ( d == c->get_dim() + 1 );
	Cell::Positive * const cc = tag::Util::assert_cast
		< Cell::Core * const, Cell::Positive * const > ( c->get_positive() .core );
	if ( cc->get_dim() + 2 == this->get_dim_plus_one() )  // center has co-dimension one
		return  new Mesh::Iterator::Around::OneCell::OfCodimOne::
		            ReturnCellsOfMaxDim::OrientCompatWithCenter ( this, cc );
	// else  // center has co-dimension at least two
	return  new Mesh::Iterator::Around::OneCell::OfCodimAtLeastTwo::
	            ReturnCellsOfDimPlusOne::OrientCompatWithCenter ( this, cc );          }


Mesh::Iterator::Core * Mesh::Fuzzy::iterator  // virtual from Mesh::Core
( const tag::OverCellsOfDim &, const size_t d, const tag::OrientOpposToCenter &,
  const tag::Around &, Cell::Core * c, const tag::ThisMeshIsPositive &          )

// since no order is required, we return an iterator which does not use neighbourhood relations
// this is important if the mesh is STSI since in this case
// following the neighbourhood relations may not produce all neighbours
// it makes no difference for a usual Fuzzy mesh
// but may be important for strangely configured Fuzzy meshes,
// e.g. for a 2D mesh having two triangles touching at a vertex

{	assert ( c->belongs_to_ld ( this, tag::cell_has_low_dim ) );
	assert ( c->get_dim() < d );
	assert ( d < this->get_dim_plus_one() );
	assert ( d >= 2 );  // for segments, a different iterator is invoked
	assert ( d == c->get_dim() + 1 );
	Cell::Positive * const cc = tag::Util::assert_cast
		< Cell::Core * const, Cell::Positive * const > ( c->get_positive() .core );
	if ( cc->get_dim() + 2 == this->get_dim_plus_one() )  // center has co-dimension one
		return  new Mesh::Iterator::Around::OneCell::OfCodimOne::
		            ReturnCellsOfMaxDim::OrientOpposToCenter ( this, cc );
	// else  // center has co-dimension at least two
	return  new Mesh::Iterator::Around::OneCell::OfCodimAtLeastTwo::
	            ReturnCellsOfDimPlusOne::OrientOpposToCenter ( this, cc );          }

//------------------------------------------------------------------------------------------------------//


Mesh::Iterator::Core * Mesh::Fuzzy::iterator  // virtual from Mesh::Core
( const tag::OverCellsOfMaxDim &, const tag::ForcePositive &,
  const tag::Around &, Cell::Core * c, const tag::ThisMeshIsPositive & )
{	std::cout << __FILE__ << ":" << __LINE__ << ": " << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "not yet implemented" << std::endl;
	exit (1);                                                                                     }


Mesh::Iterator::Core * Mesh::Fuzzy::iterator  // virtual from Mesh::Core
( const tag::OverCellsOfMaxDim &, const tag::ForcePositive &,
  const tag::Around &, Cell::Core * c, const tag::RequireOrder &, const tag::ThisMeshIsPositive & )
{	std::cout << __FILE__ << ":" << __LINE__ << ": " << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "not yet implemented" << std::endl;
	exit (1);                                                                                     }


Mesh::Iterator::Core * Mesh::Fuzzy::iterator  // virtual from Mesh::Core
( const tag::OverCellsOfMaxDim &, const tag::ForcePositive &,
  const tag::Around &, Cell::Core * c, const tag::Backwards &, const tag::ThisMeshIsPositive & )
{	std::cout << __FILE__ << ":" << __LINE__ << ": " << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "not yet implemented" << std::endl;
	exit (1);                                                                                     }


Mesh::Iterator::Core * Mesh::Fuzzy::iterator  // virtual from Mesh::Core
( const tag::OverCellsOfMaxDim &, const tag::OrientCompWithCenter &,
  const tag::Around &, Cell::Core * c, const tag::ThisMeshIsPositive & )
// no order
{	assert ( c->belongs_to_ld ( this, tag::cell_has_low_dim ) );
	assert ( c->get_dim() + 2 == this->get_dim_plus_one() );
	return  new Mesh::Iterator::Around::OneCell::OfCodimOne::ReturnCellsOfMaxDim::OrientCompatWithCenter
		( this, tag::Util::assert_cast < Cell::Core * const, Cell::Positive * const> ( c ) );     }
	

Mesh::Iterator::Core * Mesh::Fuzzy::iterator  // virtual from Mesh::Core
( const tag::OverCellsOfMaxDim &, const tag::OrientOpposToCenter &,
  const tag::Around &, Cell::Core * c, const tag::ThisMeshIsPositive & )
// no order
{	assert ( c->belongs_to_ld ( this, tag::cell_has_low_dim ) );
	assert ( c->get_dim() + 2 == this->get_dim_plus_one() );
	return  new Mesh::Iterator::Around::OneCell::OfCodimOne::ReturnCellsOfMaxDim::OrientOpposToCenter
		( this, tag::Util::assert_cast < Cell::Core * const, Cell::Positive * const> ( c ) );    }
	

Mesh::Iterator::Core * Mesh::Fuzzy::iterator  // virtual from Mesh::Core
( const tag::OverCellsOfMaxDim &, const tag::OrientCompWithMesh &,
  const tag::Around &, Cell::Core * c, const tag::ThisMeshIsPositive & )
// no order
{	assert ( c->belongs_to_ld ( this, tag::cell_has_low_dim ) );
	if ( c->get_dim() + 2 == this->get_dim_plus_one() )
		return  new Mesh::Iterator::Around::OneCell::OfCodimOne::ReturnCellsOfMaxDim::OrientCompatWithMesh
			( this, tag::Util::assert_cast < Cell::Core * const, Cell::Positive * const> ( c ) );
	return  new Mesh::Iterator::Around::OneCell::OfCodimAtLeastTwo::
	            ReturnCellsOfMaxDim::OrientCompatWithMesh
		( this, tag::Util::assert_cast < Cell::Core * const, Cell::Positive * const> ( c ) );             }
	

Mesh::Iterator::Core * Mesh::Fuzzy::iterator  // virtual from Mesh::Core
( const tag::OverCellsOfMaxDim &, const tag::OrientCompWithMesh &,
  const tag::Around &, Cell::Core * c, const tag::RequireOrder &, const tag::ThisMeshIsPositive & )
{	assert ( c->belongs_to_ld ( this, tag::cell_has_low_dim ) );
	assert ( c->get_dim() + 3 == this->get_dim_plus_one() );  // codimension two
	return  new Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoSegments::NormalOrder::AsFound
		( this, tag::Util::assert_cast < Cell::Core * const, Cell::Positive * const> ( c ) );        }
	// here "as found" is equivalent with "orientation compatible with mesh"


Mesh::Iterator::Core * Mesh::Fuzzy::iterator  // virtual from Mesh::Core
( const tag::OverCellsOfMaxDim &, const tag::OrientCompWithMesh &,
  const tag::Around &, Cell::Core * c, const tag::Backwards &, const tag::ThisMeshIsPositive & )
{	assert ( c->belongs_to_ld ( this, tag::cell_has_low_dim ) );
	assert ( c->get_dim() + 3 == this->get_dim_plus_one() );  // codimension two
	return  new Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoSegments::ReverseOrder::AsFound
		( this, tag::Util::assert_cast < Cell::Core * const, Cell::Positive * const> ( c ) );        }
	// here "as found" is equivalent with "orientation compatible with mesh"
	

Mesh::Iterator::Core * Mesh::Fuzzy::iterator  // virtual from Mesh::Core
( const tag::OverCellsOfMaxDim &, const tag::OrientOpposToMesh &,
  const tag::Around &, Cell::Core * c, const tag::ThisMeshIsPositive & )
// no order
{	assert ( c->belongs_to_ld ( this, tag::cell_has_low_dim ) );
	if ( c->get_dim() + 2 == this->get_dim_plus_one() )
		return  new Mesh::Iterator::Around::OneCell::OfCodimOne::ReturnCellsOfMaxDim::OrientOpposToMesh
			( this, tag::Util::assert_cast < Cell::Core * const, Cell::Positive * const> ( c ) );
	return  new Mesh::Iterator::Around::OneCell::OfCodimAtLeastTwo::ReturnCellsOfMaxDim::OrientOpposToMesh
		( this, tag::Util::assert_cast < Cell::Core * const, Cell::Positive * const> ( c ) );            }
	

Mesh::Iterator::Core * Mesh::Fuzzy::iterator  // virtual from Mesh::Core
( const tag::OverCellsOfMaxDim &, const tag::OrientOpposToMesh &,
  const tag::Around &, Cell::Core * c, const tag::RequireOrder &, const tag::ThisMeshIsPositive & )

{	assert ( false );  // to implement
	if ( c->get_dim() + 2 == this->get_dim_plus_one() )
		return  new Mesh::Iterator::Around::OneCell::OfCodimOne::ReturnCellsOfMaxDim::OrientOpposToMesh
			( this, tag::Util::assert_cast < Cell::Core * const, Cell::Positive * const> ( c ) );
	return  new Mesh::Iterator::Around::OneCell::OfCodimAtLeastTwo::ReturnCellsOfMaxDim::OrientOpposToMesh
		( this, tag::Util::assert_cast < Cell::Core * const, Cell::Positive * const> ( c ) );            }
	

Mesh::Iterator::Core * Mesh::Fuzzy::iterator  // virtual from Mesh::Core
( const tag::OverCellsOfMaxDim &, const tag::OrientOpposToMesh &,
  const tag::Around &, Cell::Core * c, const tag::Backwards &, const tag::ThisMeshIsPositive & )

{	assert ( false );  // to implement
	if ( c->get_dim() + 2 == this->get_dim_plus_one() )
		return  new Mesh::Iterator::Around::OneCell::OfCodimOne::ReturnCellsOfMaxDim::OrientOpposToMesh
			( this, tag::Util::assert_cast < Cell::Core * const, Cell::Positive * const> ( c ) );
	return  new Mesh::Iterator::Around::OneCell::OfCodimAtLeastTwo::ReturnCellsOfMaxDim::OrientOpposToMesh
		( this, tag::Util::assert_cast < Cell::Core * const, Cell::Positive * const> ( c ) );            }
	

Mesh::Iterator::Core * Mesh::Fuzzy::iterator  // virtual from Mesh::Core
( const tag::OverCellsOfMaxDim &, const tag::AsFound &,
  const tag::Around &, Cell::Core * c, const tag::ThisMeshIsPositive & )

// since no order is required, we return an iterator which does not use neighbourhood relations
// this is important if the mesh is STSI since in this case
// following the neighbourhood relations may not produce all neighbours
// it makes no difference for a usual Fuzzy mesh
// but may be important for strangely configured Fuzzy meshes,
// e.g. for a 2D mesh having two triangles touching at a vertex
	
{	assert ( c->belongs_to_ld ( this, tag::cell_has_low_dim ) );
	Cell::Positive * const cc = tag::Util::assert_cast
		< Cell::Core * const, Cell::Positive * const > ( c->get_positive() .core );
	if ( cc->get_dim() + 2 == this->get_dim_plus_one() )  // center has co-dimension one
		return  new Mesh::Iterator::Around::OneCell::
		            OfCodimOne::ReturnCellsOfMaxDim::AsFound ( this, cc );
	// else  // center has co-dimension at least two
	return  new Mesh::Iterator::Around::OneCell::OfCodimAtLeastTwo::
	            ReturnCellsOfMaxDim::AsFound ( this, cc );                         }


//------------------------------------------------------------------------------------------------------//
//------------------------------------------------------------------------------------------------------//


Cell::Iterator::Core * Cell::Core::iterator_1  // virtual, later overridden
( const tag::OverMeshesAbove &, const tag::SameDim &, const tag::OrientCompWithCell &,
  const tag::ThisCellIsPositive &, const tag::DoNotBuildCells &                       )

{	std::cout << __FILE__ << ":" << __LINE__ << ": " << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "This cell does not support iterators over meshes of same dim." << std::endl;
	exit (1);                                                                                       }


Cell::Iterator::Core * Cell::Positive::NotVertex::iterator_1  // virtual from Cell::Core, here overridden
( const tag::OverMeshesAbove &, const tag::SameDim &, const tag::OrientCompWithCell &,
  const tag::ThisCellIsPositive &, const tag::DoNotBuildCells &                       )

{	return  new Cell::Iterator::OverMeshesOfSameDim::OrientationCompatWithCell ( this );  }
	

Cell::Iterator::Core * Cell::Core::iterator_2  // virtual, later overridden
( const tag::OverMeshesAbove &, const tag::SameDim &, const tag::OrientOpposToCell &,
  const tag::ThisCellIsPositive &, const tag::DoNotBuildCells &                      )

{	std::cout << __FILE__ << ":" << __LINE__ << ": " << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "This cell does not support iterators over meshes of same dim." << std::endl;
	exit (1);                                                                                       }


Cell::Iterator::Core * Cell::Positive::NotVertex::iterator_2  // virtual from Cell::Core, here overridden
( const tag::OverMeshesAbove &, const tag::SameDim &, const tag::OrientOpposToCell &,
  const tag::ThisCellIsPositive &, const tag::DoNotBuildCells &                      )

{	return  new Cell::Iterator::OverMeshesOfSameDim::OrientationOpposToCell ( this );  }
	

Cell::Iterator::Core * Cell::Core::iterator_3  // virtual, later overridden
( const tag::OverMeshesAbove &, const tag::OfDimension &, const size_t d,
  const tag::OrientationIrrelevant &, const tag::ThisCellIsPositive &    )

{	std::cout << __FILE__ << ":" << __LINE__ << ": " << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "This cell does not support iterators over meshes above." << std::endl;
	exit (1);                                                                                       }


Cell::Iterator::Core * Cell::Positive::iterator_3  // virtual from Cell::Core, here overridden
( const tag::OverMeshesAbove &, const tag::OfDimension &, const size_t d,
  const tag::OrientationIrrelevant &, const tag::ThisCellIsPositive &    )

{	return  new Cell::Iterator::OverMeshesOfGivenDim::OrientationIrrelevant::ReturnPositiveMesh
	            ( this, tag::over_meshes_of_dim, d );                                            }

//------------------------------------------------------------------------------------------------------//
//------------------------------------------------------------------------------------------------------//


void Mesh::Iterator::Over::TwoVerticesOfSeg::reset ( ) // virtual from Mesh::Iterator::Core

{	assert ( this->seg_p );
	this->passage = 0;       }


void Mesh::Iterator::Over::TwoVerticesOfSeg::reset ( const tag::StartAt &, Cell::Core * cll )
// virtual from Mesh::Iterator::Core

{	std::cout << __FILE__ << ":" << __LINE__ << ": " << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "Cannot reset an iterator this way." << std::endl;
	exit (1);                                                                                       }


void Mesh::Iterator::Over::TwoVerticesOfSeg::advance ( )
// virtual from Mesh::Iterator::Core

{	assert ( this->passage < 2 );
	this->passage++;               }


bool Mesh::Iterator::Over::TwoVerticesOfSeg::in_range ( )
// virtual from Mesh::Iterator::Core

{	return this->passage < 2;   }


Cell Mesh::Iterator::Over::TwoVerticesOfSeg::NormalOrder::AsFound::deref ( )

// virtual from Mesh::Iterator::Core
// iterate over the two vertices, first base (negative) then tip (positive)

{	assert ( this->seg_p );
	assert ( this->passage < 2 );
	if ( this->passage == 0 )
	{	assert ( not this->seg_p->base_attr.is_positive() );
		return this->seg_p->base_attr;                        }  // negative vertex
	else
	{	assert ( this->seg_p->tip_attr.is_positive() );
		return this->seg_p->tip_attr;                    }      }  // positive vertex


Cell Mesh::Iterator::Over::TwoVerticesOfSeg::ReverseOrder::AsFound::deref ( )

// virtual from Mesh::Iterator::Core
// iterate over the two vertices, first tip (positive) then base (negative)

{	assert ( this->seg_p );
	assert ( this->passage < 2 );
	if ( this->passage == 0 )
  {	assert ( this->seg_p->tip_attr.is_positive() );
		return this->seg_p->tip_attr;                   }    // positive vertex
	else
	{	assert ( not this->seg_p->base_attr.is_positive() );
		return this->seg_p->base_attr;                        }  } // negative vertex


Cell Mesh::Iterator::Over::TwoVerticesOfSeg::NormalOrder::ForcePositive::deref ( )

// virtual from Mesh::Iterator::Core
// iterate over the two vertices, first base then tip (both positive)

{	assert ( this->seg_p );
	assert ( this->passage < 2 );
	if ( this->passage == 0 )
		return this->seg_p->base_attr.reverse ( tag::surely_exists );      // positive vertex
	else
		return this->seg_p->tip_attr;                                   }  // positive vertex


Cell Mesh::Iterator::Over::TwoVerticesOfSeg::ReverseOrder::ForcePositive::deref ( )

// virtual from Mesh::Iterator::Core
// iterate over the two vertices, first tip then base (both positive)

{	assert ( this->seg_p );
	assert ( this->passage < 2 );
	if ( this->passage == 0 )
		return this->seg_p->tip_attr;                      // positive vertex
	else
		return this->seg_p->base_attr.reverse ( tag::surely_exists );  }   // positive vertex


Cell Mesh::Iterator::Over::TwoVerticesOfSeg::NormalOrder::ReverseEachCell::deref ( )

// virtual from Mesh::Iterator::Core
// iterate over the two vertices, first base (negative) then tip (positive)

{	assert ( this->seg_p );
	assert ( this->passage < 2 );
	if ( this->passage == 0 )
		return this->seg_p->base_attr.reverse ( tag::surely_exists );     // positive vertex
	else
		return this->seg_p->tip_attr.reverse ( tag::build_if_not_exists );  }  // negative vertex
	

Cell Mesh::Iterator::Over::TwoVerticesOfSeg::ReverseOrder::ReverseEachCell::deref ( )

// virtual from Mesh::Iterator::Core
// iterate over the two vertices, first base (negative) then tip (positive)

{	assert ( this->seg_p );
	assert ( this->passage < 2 );
	if ( this->passage == 0 )
		return this->seg_p->base_attr.reverse ( tag::surely_exists );     // positive vertex
	else
		return this->seg_p->tip_attr.reverse ( tag::build_if_not_exists );  }  // negative vertex
	

//------------------------------------------------------------------------------------------------------//
//------------------------------------------------------------------------------------------------------//


void Mesh::Iterator::OverVertices::OfConnectedOneDimMesh::NormalOrder::reset ( )
// virtual from Mesh::Iterator::Core

{	assert ( this->msh );
	if ( this->msh->nb_of_segs == 0 )
	{	this->current_vertex = nullptr;  return;  }
	this->current_vertex = tag::Util::assert_cast
		< Cell::Core*, Cell::Positive::Vertex* > ( this->msh->first_ver .core );
	this->last_vertex = this->msh->last_ver .core;
	if ( this->msh->first_ver == this->msh->last_ver ) // loop
	{	Cell rev_ver = this->current_vertex->reverse_attr;
		assert ( rev_ver .exists() );
		assert ( not rev_ver .is_positive() );
		std::map < Mesh::Core *, Cell > ::iterator it =
			rev_ver .core->cell_behind_within .find ( this->msh );
		assert ( it != rev_ver .core->cell_behind_within .end() );
		Cell seg = it->second;
		// Cell seg = rev_ver .core->cell_behind_within [ msh ];
		this->current_vertex = tag::Util::assert_cast
			< Cell::Core*, Cell::Positive::Vertex* > ( seg .tip() .core );  }
	assert ( this->current_vertex->is_positive() );
	assert ( this->last_vertex->is_positive() );                                        }


void Mesh::Iterator::OverVertices::OfConnectedOneDimMesh::ReverseOrder::reset ( )
// virtual from Mesh::Iterator::Core

{	assert ( this->msh );
	if ( this->msh->nb_of_segs == 0 )
	{	this->current_vertex = nullptr;  return;  }
	this->last_vertex = this->msh->first_ver .core;
	this->current_vertex = tag::Util::assert_cast
		< Cell::Core*, Cell::Positive::Vertex* > ( this->msh->last_ver .core );
	if ( this->msh->first_ver == this->msh->last_ver ) // loop
	{	std::map < Mesh::Core *, Cell > ::iterator it =
			this->current_vertex->cell_behind_within .find ( this->msh );
		assert ( it != this->current_vertex->cell_behind_within .end() );
		Cell seg = it->second;
		// Cell seg = this->current_vertex->cell_behind_within [ msh ];
		this->current_vertex = tag::Util::assert_cast
			< Cell::Core*, Cell::Positive::Vertex* >
			( seg .base() .reverse ( tag::surely_exists ) .core );           }
	assert ( this->current_vertex->is_positive() );
	assert ( this->last_vertex->is_positive() );                                    }


void Mesh::Iterator::OverVertices::OfConnectedOneDimMesh::NormalOrder::reset
( const tag::StartAt &, Cell::Core * ver )  // virtual from Mesh::Iterator::Core

{	assert ( this->msh );
	assert ( this->msh->nb_of_segs > 0 );
	assert ( ver );
	assert ( ver->get_dim() == 0 );
	this->current_vertex = tag::Util::assert_cast
		< Cell::Core*, Cell::Positive::Vertex* > ( ver );
	if ( this->msh->first_ver == this->msh->last_ver ) // loop
	{	std::map < Mesh::Core *, Cell > ::iterator it =
		  ver->cell_behind_within .find ( this->msh );
		assert ( it != ver->cell_behind_within .end() );
		Cell seg = it->second;
		// Cell seg = ver->cell_behind_within [ msh ];
		this->last_vertex = tag::Util::assert_cast
			< Cell::Core*, Cell::Positive::Vertex* >
			( seg .base() .reverse ( tag::surely_exists ) .core );  }
	else // open chain
		this->last_vertex = this->msh->last_ver .core;
	assert ( this->current_vertex->is_positive() );
	assert ( this->last_vertex->is_positive() );                                        }


void Mesh::Iterator::OverVertices::OfConnectedOneDimMesh::ReverseOrder::reset
( const tag::StartAt &, Cell::Core * ver )  // virtual from Mesh::Iterator::Core

{	assert ( this->msh );
	assert ( this->msh->nb_of_segs > 0 );
	assert ( ver );
	assert ( ver->get_dim() == 0 );
	this->current_vertex = tag::Util::assert_cast
		< Cell::Core*, Cell::Positive::Vertex* > ( ver );
	if ( this->msh->first_ver == this->msh->last_ver ) // loop
	{	Cell rev_ver = ver->reverse_attr;
		assert ( rev_ver .exists() );
		std::map < Mesh::Core *, Cell > ::iterator it =
			rev_ver .core->cell_behind_within .find ( this->msh );
		assert ( it != rev_ver .core->cell_behind_within .end() );
		Cell seg = it->second;
		// Cell seg = rev_ver .core->cell_behind_within [ msh ];
		this->last_vertex = tag::Util::assert_cast
			< Cell::Core*, Cell::Positive::Vertex* > ( seg.tip().core );  }
	else // open chain
		this->last_vertex = this->msh->first_ver .core;
	assert ( this->current_vertex->is_positive() );
	assert ( this->last_vertex->is_positive() );                                        }


Cell Mesh::Iterator::OverVertices::OfConnectedOneDimMesh::deref ( )
// virtual from Mesh::Iterator::Core

{	return Cell ( tag::whose_core_is, this->current_vertex,
	              tag::previously_existing, tag::surely_not_null );  }


void Mesh::Iterator::OverVertices::OfConnectedOneDimMesh::NormalOrder::advance ( )
// virtual from Mesh::Iterator::Core

{	assert ( this->msh );
	assert ( this->current_vertex );
	if ( this->current_vertex == this->last_vertex )              
	{	this->current_vertex = nullptr;  return;  }  // becomes out of range
	Cell rev_ver = this->current_vertex->reverse_attr;
	assert ( rev_ver .exists() );
	std::map < Mesh::Core *, Cell > ::iterator it =
		rev_ver .core->cell_behind_within .find ( this->msh );
	assert ( it != rev_ver .core->cell_behind_within .end() );
	Cell rev_seg = it->second;
	// Cell rev_seg = rev_ver .core->cell_behind_within [ msh ];
	this->current_vertex = tag::Util::assert_cast
		< Cell::Core*, Cell::Positive::Vertex* > ( rev_seg .tip() .core );
	assert ( this->current_vertex->is_positive() );                       }


void Mesh::Iterator::OverVertices::OfConnectedOneDimMesh::ReverseOrder::advance ( )
// virtual from Mesh::Iterator::Core

{	assert ( this->msh );
	assert ( this->current_vertex );
	if ( this->current_vertex == this->last_vertex )              
	{	this->current_vertex = nullptr;  return;  }  // becomes out of range
	std::map < Mesh::Core *, Cell > ::iterator it =
		this->current_vertex->cell_behind_within .find ( this->msh );
	assert ( it != this->current_vertex->cell_behind_within .end() );
	Cell seg = it->second;
	// Cell seg = this->current_vertex->cell_behind_within [ msh ];
	this->current_vertex = tag::Util::assert_cast
		< Cell::Core*, Cell::Positive::Vertex* >
		( seg .base() .reverse ( tag::surely_exists ) .core );
	assert ( this->current_vertex->is_positive() );                    }


bool Mesh::Iterator::OverVertices::OfConnectedOneDimMesh::in_range ( )
// virtual from Mesh::Iterator::Core
{	return this->current_vertex;  }

//------------------------------------------------------------------------------------------------------//


void Mesh::Iterator::OverSegments::OfConnectedOneDimMesh::NormalOrder::reset ( )
// virtual from Mesh::Iterator::Core

{	assert ( this->msh );
	if ( this->msh->nb_of_segs == 0 )
	{ this->current_segment = nullptr;  return;  }
	Cell neg_ver = this->msh->first_ver .reverse ( tag::surely_exists );
	assert ( neg_ver .exists() );  assert ( not neg_ver .is_positive() );
	std::map < Mesh::Core *, Cell > ::iterator it =
		neg_ver .core->cell_behind_within .find ( this->msh );
	assert ( it != neg_ver .core->cell_behind_within .end() );
	this->current_segment = ( it->second ) .core;
	assert ( this->current_segment );
	this->last_vertex = this->msh->last_ver .core;
	assert ( this->last_vertex->is_positive() );                           }


void Mesh::Iterator::OverSegments::OfConnectedOneDimMesh::ReverseOrder::reset ( )
// virtual from Mesh::Iterator::Core

{	assert ( this->msh );
	if ( this->msh->nb_of_segs == 0 )
	{ this->current_segment = nullptr;  return;  }
	Cell pos_ver = this->msh->last_ver;
	assert ( pos_ver .exists() );  assert ( pos_ver .is_positive() );
	std::map < Mesh::Core *, Cell > ::iterator it =
		pos_ver .core->cell_behind_within .find ( this->msh );
	assert ( it != pos_ver .core->cell_behind_within .end() );
	this->current_segment = ( it->second ) .core;
	assert ( this->current_segment );
	this->last_vertex = this->msh->first_ver .core;
	assert ( this->last_vertex->is_positive() );                       }


void Mesh::Iterator::OverSegments::OfConnectedOneDimMesh::NormalOrder::reset
( const tag::StartAt &, Cell::Core * seg )  // virtual from Mesh::Iterator::Core

{	assert ( this->msh );  assert ( this->msh->nb_of_segs > 0 );
	assert ( seg );  assert ( seg->get_dim() == 1 );
	this->current_segment = seg;
	if ( this->msh->first_ver == this->msh->last_ver ) // loop
		this->last_vertex = seg->base().reverse(tag::surely_exists).core;
	else  // open chain
		this->last_vertex = this->msh->last_ver.core;
	assert ( this->last_vertex->is_positive() );                                        }


void Mesh::Iterator::OverSegments::OfConnectedOneDimMesh::ReverseOrder::reset
( const tag::StartAt &, Cell::Core * seg )  // virtual from Mesh::Iterator::Core

{	assert ( this->msh );  assert ( this->msh->nb_of_segs > 0 );
	assert ( seg );  assert ( seg->get_dim() == 1 );
	this->current_segment = seg;
	if ( this->msh->first_ver == this->msh->last_ver ) // loop
		this->last_vertex = seg->tip() .core;
	else  // open chain
		this->last_vertex = this->msh->first_ver .core;
	assert ( this->last_vertex->is_positive() );                  }


Cell Mesh::Iterator::OverSegments::OfConnectedOneDimMesh::NormalOrder::AsFound::deref ( )
// virtual from Mesh::Iterator::Core

{	return Cell ( tag::whose_core_is, this->current_segment,
                tag::previously_existing, tag::surely_not_null );  }


Cell Mesh::Iterator::OverSegments::OfConnectedOneDimMesh::ReverseOrder::AsFound::deref ( )
// virtual from Mesh::Iterator::Core

{	return Cell ( tag::whose_core_is, this->current_segment,
                tag::previously_existing, tag::surely_not_null );  }


Cell Mesh::Iterator::OverSegments::OfConnectedOneDimMesh::NormalOrder::ForcePositive::deref ( )
// virtual from Mesh::Iterator::Core

{	assert ( this->current_segment );
	return this->current_segment->get_positive();  }


Cell Mesh::Iterator::OverSegments::OfConnectedOneDimMesh::ReverseOrder::ForcePositive::deref ( )
// virtual from Mesh::Iterator::Core

{	assert ( this->current_segment );
	return this->current_segment->get_positive();  }


Cell Mesh::Iterator::OverSegments::OfConnectedOneDimMesh::NormalOrder::ReverseEachCell::deref ( )
// virtual from Mesh::Iterator::Core

{	assert ( this->current_segment );
	if ( not this->current_segment->reverse_attr .exists() )
		//	this->current_segment->reverse_attr =
		//		Cell ( tag::whose_core_is, this->current_segment->build_reverse
		//	         ( tag::one_dummy_wrapper ), tag::freshly_created         );
	{	Cell::Positive * tcs = tag::Util::assert_cast
			< Cell::Core*, Cell::Positive* > ( this->current_segment );
		tcs->reverse_attr.core = tcs->build_reverse ( tag::one_dummy_wrapper );  }
	assert ( this->current_segment->reverse_attr .exists() );
	return this->current_segment->reverse_attr;                                   }


Cell Mesh::Iterator::OverSegments::OfConnectedOneDimMesh::ReverseOrder::ReverseEachCell::deref ( )
// virtual from Mesh::Iterator::Core

{	assert ( this->current_segment );
	if ( not this->current_segment->reverse_attr .exists() )
		//	this->current_segment->reverse_attr =
		//		Cell ( tag::whose_core_is, this->current_segment->build_reverse
		//	         ( tag::one_dummy_wrapper ), tag::freshly_created         );
	{	Cell::Positive * tcs = tag::Util::assert_cast
			< Cell::Core*, Cell::Positive* > ( this->current_segment );
		tcs->reverse_attr.core = tcs->build_reverse ( tag::one_dummy_wrapper );  }
	assert ( this->current_segment->reverse_attr .exists() );
	return this->current_segment->reverse_attr;                                   }


void Mesh::Iterator::OverSegments::OfConnectedOneDimMesh::NormalOrder::advance ( )
// virtual from Mesh::Iterator::Core

{	assert ( this->current_segment );
	Cell::Core * ver = this->current_segment->tip() .core;
	assert ( ver );  assert ( ver->is_positive() );
	if ( ver == this->last_vertex )  // becomes out of range
	{	this->current_segment = nullptr;  return;  }
	Cell::Core * neg_ver = ver->reverse_attr .core;
	assert ( neg_ver );
	this->current_segment = neg_ver->cell_behind_within [ msh ] .core;
	assert ( this->current_segment );                                   }


void Mesh::Iterator::OverSegments::OfConnectedOneDimMesh::ReverseOrder::advance ( )
// virtual from Mesh::Iterator::Core

{	assert ( this->current_segment );
	Cell::Core * ver = this->current_segment->base() .core;
	assert ( ver );  assert ( not ver->is_positive() );
	Cell::Core * pos_ver = ver->reverse_attr .core;
	assert ( pos_ver );  assert ( pos_ver->is_positive() );
	if ( pos_ver == this->last_vertex )  // becomes out of range
	{	this->current_segment = nullptr;  return;  }
	this->current_segment = pos_ver->cell_behind_within [ msh ] .core;
	assert ( this->current_segment );                                   }


bool Mesh::Iterator::OverSegments::OfConnectedOneDimMesh::in_range ( )
// virtual from Mesh::Iterator::Core
{	return this->current_segment;  }

//------------------------------------------------------------------------------------------------------//


void Mesh::Iterator::OverCells::OfFuzzyMesh::reset ( ) // virtual from Mesh::Iterator::Core
{	this->iter = this->list .begin();  }


void Mesh::Iterator::OverCells::OfFuzzyMesh::reset ( const tag::StartAt &, Cell::Core * cll )
// virtual from Mesh::Iterator::Core
{	std::cout << __FILE__ << ":" << __LINE__ << ": " << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "Cannot reset an iterator this way." << std::endl;
	exit (1);                                                                                       }


void Mesh::Iterator::OverCells::OfFuzzyMesh::advance ( )
// virtual from Mesh::Iterator::Core
{	this->iter++;  }


bool Mesh::Iterator::OverCells::OfFuzzyMesh::in_range ( )
// virtual from Mesh::Iterator::Core
{	return this->iter != this->list .end();  }


Cell Mesh::Iterator::OverCells::OfFuzzyMesh::AsFound::deref ( )
// virtual from Mesh::Iterator::Core
{	return * this->iter;  }


Cell Mesh::Iterator::OverCells::OfFuzzyMesh::ReverseEachCell::deref ( )
// virtual from Mesh::Iterator::Core
{	return ( * this->iter ) .reverse ( tag::build_if_not_exists );  }


Cell Mesh::Iterator::OverCells::OfFuzzyMesh::ForcePositive::deref ( )
// virtual from Mesh::Iterator::Core
{	return ( * this->iter ) .get_positive();  }

//------------------------------------------------------------------------------------------------------//
//------------------------------------------------------------------------------------------------------//


void Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoVertices::NormalOrder::reset ( )
// virtual from Mesh::Iterator::Core

{	this->current_covertex = this->first_covertex;
	Mesh m ( tag::whose_core_is, this->msh_p, tag::previously_existing, tag::is_positive );
	Cell new_ver ( tag::whose_core_is, this->current_covertex,
	               tag::previously_existing, tag::surely_not_null );
	assert ( new_ver .tip() .core == this->center );
	this->current_cosegment = m .cell_in_front_of ( new_ver, tag::may_not_exist ) .core;     }

// the above could get simpler and faster by avoiding the use of wrappers


void Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoVertices::ReverseOrder::reset ( )
// virtual from Mesh::Iterator::Core

{	this->current_covertex = this->first_covertex;
	Mesh m ( tag::whose_core_is, this->msh_p, tag::previously_existing, tag::is_positive );
	Cell new_ver ( tag::whose_core_is, this->current_covertex,
	               tag::previously_existing, tag::surely_not_null );
	assert ( new_ver .tip() .core == this->center );
	this->current_cosegment = m .cell_behind ( new_ver, tag::may_not_exist ) .core;          }

// the above could get simpler and faster by avoiding the use of wrappers


void Mesh::Iterator::Around::OneCell::OfCodimTwo
	::OverCoVertices::NormalOrder::BuildReverseCells::AsFound::reset
( const tag::StartAt &, Cell::Core * cll )
// virtual from Mesh::Iterator::Core

{ // closed loop or open chain ?
	// in other words : central cell is interior or on the boundary ?
	Mesh m ( tag::whose_core_is, this->msh_p, tag::previously_existing, tag::is_positive );
	Cell fv ( tag::whose_core_is, this->first_covertex,
	          tag::previously_existing, tag::surely_not_null );
	this->current_covertex = cll;
	Cell new_ver ( tag::whose_core_is, cll, tag::previously_existing, tag::surely_not_null );
	// center may have dimension different from zero, so we should not use 'tip' !!
	if ( this->center->is_positive() ) assert ( new_ver .tip() .core == this->center );
	else assert ( new_ver .base() .core == this->center );
	this->current_cosegment = m .cell_in_front_of ( new_ver, tag::may_not_exist ) .core;
	if ( this->last_cosegment )
	if ( this->last_cosegment == m .cell_behind ( fv, tag::may_not_exist ) .core )
	// closed loop, i.e., center is interior to the mesh
	{	this->first_covertex = cll;
		this->last_cosegment = m .cell_behind ( new_ver, tag::may_not_exist ) .core;  }          }


void Mesh::Iterator::Around::OneCell::OfCodimTwo
	::OverCoVertices::NormalOrder::BuildReverseCells::ForcePositive::reset
( const tag::StartAt &, Cell::Core * cll )
// virtual from Mesh::Iterator::Core

{ // closed loop or open chain ?
	// in other words : central cell is interior or on the boundary ?
	Mesh m ( tag::whose_core_is, this->msh_p, tag::previously_existing, tag::is_positive );
	Cell fv ( tag::whose_core_is, this->first_covertex,
	          tag::previously_existing, tag::surely_not_null );
	this->current_covertex = cll;
	Cell new_ver ( tag::whose_core_is, cll, tag::previously_existing, tag::surely_not_null );
	// center may have dimension different from zero, so we should not use 'tip' !!
	if ( this->center->is_positive() ) assert ( new_ver .tip() .core == this->center );
	else assert ( new_ver .base() .core == this->center );
	this->current_cosegment = m.cell_in_front_of ( new_ver, tag::may_not_exist ) .core;
	if ( this->last_cosegment )
	if ( this->last_cosegment == m .cell_behind ( fv, tag::may_not_exist ) .core )
	// closed loop, i.e., center is interior to the mesh
	{	this->first_covertex = cll;
		this->last_cosegment = m .cell_behind ( new_ver, tag::may_not_exist ) .core;  }          }


void Mesh::Iterator::Around::OneCell::OfCodimTwo
	::OverCoVertices::NormalOrder::BuildReverseCells::ReverseEachCell::reset
( const tag::StartAt &, Cell::Core * cll )
// virtual from Mesh::Iterator::Core

{ // closed loop or open chain ?
	// in other words : central cell is interior or on the boundary ?
	Mesh m ( tag::whose_core_is, this->msh_p, tag::previously_existing, tag::is_positive );
	Cell fv ( tag::whose_core_is, this->first_covertex,
	          tag::previously_existing, tag::surely_not_null );
	this->current_covertex = cll;
	Cell new_ver ( tag::whose_core_is, cll, tag::previously_existing, tag::surely_not_null );
	// center may have dimension different from zero, so we should not use 'tip' !!
	if ( this->center->is_positive() ) assert ( new_ver .tip() .core == this->center );
	else assert ( new_ver .base() .core == this->center );
	this->current_cosegment = m .cell_in_front_of ( new_ver, tag::may_not_exist ) .core;
	if ( this->last_cosegment )
	if ( this->last_cosegment == m .cell_behind ( fv, tag::may_not_exist ) .core )
	// closed loop, i.e., center is interior to the mesh
	{	this->first_covertex = cll;
		this->last_cosegment = m .cell_behind ( new_ver, tag::may_not_exist ) .core;  }          }


void Mesh::Iterator::Around::OneCell::OfCodimTwo
	::OverCoVertices::ReverseOrder::BuildReverseCells::AsFound::reset
( const tag::StartAt &, Cell::Core * cll )
// virtual from Mesh::Iterator::Core

{ // closed loop or open chain ?
	// in other words : central cell is interior or on the boundary ?
	Mesh m ( tag::whose_core_is, this->msh_p, tag::previously_existing, tag::is_positive );
	Cell fv ( tag::whose_core_is, this->first_covertex,
            tag::previously_existing, tag::surely_not_null );
	this->current_covertex = cll;
	Cell new_ver ( tag::whose_core_is, cll, tag::previously_existing, tag::surely_not_null );
	assert ( new_ver .tip() .core == this->center );
	this->current_cosegment = m .cell_behind ( new_ver, tag::may_not_exist ).core;
	if ( this->last_cosegment )
	if ( this->last_cosegment == m .cell_in_front_of ( fv, tag::may_not_exist ).core )
	// closed loop, i.e., center is interior to the mesh
	{	this->first_covertex = cll;
		this->last_cosegment = m .cell_in_front_of ( new_ver, tag::may_not_exist ) .core;  }     }
	

void Mesh::Iterator::Around::OneCell::OfCodimTwo
	::OverCoVertices::ReverseOrder::BuildReverseCells::ForcePositive::reset
( const tag::StartAt &, Cell::Core * cll )
// virtual from Mesh::Iterator::Core

{ // closed loop or open chain ?
	// in other words : central cell is interior or on the boundary ?
	Mesh m ( tag::whose_core_is, this->msh_p, tag::previously_existing, tag::is_positive );
	Cell fv ( tag::whose_core_is, this->first_covertex,
            tag::previously_existing, tag::surely_not_null );
	this->current_covertex = cll;
	Cell new_ver ( tag::whose_core_is, cll, tag::previously_existing, tag::surely_not_null );
	assert ( new_ver .tip() .core == this->center );
	this->current_cosegment = m .cell_behind ( new_ver, tag::may_not_exist ).core;
	if ( this->last_cosegment )
	if ( this->last_cosegment == m .cell_in_front_of ( fv, tag::may_not_exist ).core )
	// closed loop, i.e., center is interior to the mesh
	{	this->first_covertex = cll;
		this->last_cosegment = m .cell_in_front_of ( new_ver, tag::may_not_exist ) .core;  }     }
	

void Mesh::Iterator::Around::OneCell::OfCodimTwo
	::OverCoVertices::ReverseOrder::BuildReverseCells::ReverseEachCell::reset
( const tag::StartAt &, Cell::Core * cll )
// virtual from Mesh::Iterator::Core

{ // closed loop or open chain ?
	// in other words : central cell is interior or on the boundary ?
	Mesh m ( tag::whose_core_is, this->msh_p, tag::previously_existing, tag::is_positive );
	Cell fv ( tag::whose_core_is, this->first_covertex,
            tag::previously_existing, tag::surely_not_null );
	this->current_covertex = cll;
	Cell new_ver ( tag::whose_core_is, cll, tag::previously_existing, tag::surely_not_null );
	assert ( new_ver .tip() .core == this->center );
	this->current_cosegment = m .cell_behind ( new_ver, tag::may_not_exist ) .core;
	if ( this->last_cosegment )
	if ( this->last_cosegment == m .cell_in_front_of ( fv, tag::may_not_exist ) .core )
	// closed loop, i.e., center is interior to the mesh
	{	this->first_covertex = cll;
		this->last_cosegment = m.cell_in_front_of ( new_ver, tag::may_not_exist ) .core;  }      }
	

Cell Mesh::Iterator::Around::OneCell::OfCodimTwo
         ::OverCoVertices::NormalOrder::BuildReverseCells::AsFound::deref ( )
// virtual from Mesh::Iterator::Core
{	return Cell ( tag::whose_core_is, this->current_covertex,
                tag::previously_existing, tag::surely_not_null );  }


Cell Mesh::Iterator::Around::OneCell::OfCodimTwo
         ::OverCoVertices::NormalOrder::BuildReverseCells::ForcePositive::deref ( )
// virtual from Mesh::Iterator::Core
{	assert ( this->current_covertex );
	return this->current_covertex->get_positive();  }


Cell Mesh::Iterator::Around::OneCell::OfCodimTwo
	::OverCoVertices::NormalOrder::BuildReverseCells::ReverseEachCell::deref ( )
// virtual from Mesh::Iterator::Core

{	assert ( this->current_covertex );
	if ( not this->current_covertex->reverse_attr .exists() )
	//	this->current_covertex->reverse_attr =
	//		Cell ( tag::whose_core_is, this->current_covertex->build_reverse
	//	         ( tag::one_dummy_wrapper ), tag::freshly_created         );
	{	Cell::Positive * tcv = tag::Util::assert_cast
			< Cell::Core*, Cell::Positive* > ( this->current_covertex );
		tcv->reverse_attr.core = tcv->build_reverse ( tag::one_dummy_wrapper );  }
	assert ( this->current_covertex->reverse_attr .exists() );
	return this->current_covertex->reverse_attr;                                  }


Cell Mesh::Iterator::Around::OneCell::OfCodimTwo
         ::OverCoVertices::ReverseOrder::BuildReverseCells::AsFound::deref ( )
// virtual from Mesh::Iterator::Core
{	return Cell ( tag::whose_core_is, this->current_covertex,
                tag::previously_existing, tag::surely_not_null );  }


Cell Mesh::Iterator::Around::OneCell::OfCodimTwo
         ::OverCoVertices::ReverseOrder::BuildReverseCells::ForcePositive::deref ( )
// virtual from Mesh::Iterator::Core
{	assert ( this->current_covertex );
	return this->current_covertex->get_positive();  }


Cell Mesh::Iterator::Around::OneCell::OfCodimTwo
         ::OverCoVertices::ReverseOrder::BuildReverseCells::ReverseEachCell::deref ( )
// virtual from Mesh::Iterator::Core

{	assert ( this->current_covertex );
	if ( not this->current_covertex->reverse_attr .exists() )
	//	this->current_covertex->reverse_attr =
	//		Cell ( tag::whose_core_is, this->current_covertex->build_reverse
	//	         ( tag::one_dummy_wrapper ), tag::freshly_created         );
	{	Cell::Positive * tcv = tag::Util::assert_cast
			< Cell::Core*, Cell::Positive* > ( this->current_covertex );
		tcv->reverse_attr .core = tcv->build_reverse ( tag::one_dummy_wrapper );  }
	assert ( this->current_covertex->reverse_attr .exists() );
	return this->current_covertex->reverse_attr;                                   }


Cell Mesh::Iterator::Around::OneCell::OfCodimTwo
         ::WorkAround2D::NormalOrder::BuildReverseCells::deref ( )

// virtual from Mesh::Iterator::Core,
// defined by Mesh::Iterator::Around::OneCell::OfCodimTwo::OverVertices::NormalOrder::
// ::BuildReverseCells::AsFound, here overriden to return the reversed base of the segment

{	Cell seg = Mesh::Iterator::Around::OneCell::OfCodimTwo
		::OverCoVertices::NormalOrder::BuildReverseCells::AsFound::deref();
	return seg .base() .reverse();                                         }


Cell Mesh::Iterator::Around::OneCell::OfCodimTwo
         ::WorkAround2D::ReverseOrder::BuildReverseCells::deref ( )

// virtual from Mesh::Iterator::Core,
// defined by Mesh::Iterator::Around::OneCell::OfCodimTwo::OverVertices::NormalOrder::
// ::BuildReverseCells::AsFound, here overriden to return the reversed base of the segment

{	Cell seg = Mesh::Iterator::Around::OneCell::OfCodimTwo
		::OverCoVertices::ReverseOrder::BuildReverseCells::AsFound::deref();
	return seg .base() .reverse();                                          }


void Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoVertices::NormalOrder::advance ( )
// virtual from Mesh::Iterator::Core

{	Cell this_center ( tag::whose_core_is, this->center,
	                   tag::previously_existing, tag::surely_not_null );
	Mesh m ( tag::whose_core_is, this->msh_p, tag::previously_existing, tag::is_positive );
	if ( this->current_cosegment == nullptr )  // out of range
	{	this->current_covertex = nullptr;  return;  }
	Cell new_ver = this->current_cosegment->boundary() .cell_behind
		( this_center, tag::surely_exists );
	this->current_covertex = new_ver .core;
	this->current_cosegment = m .cell_in_front_of ( new_ver, tag::may_not_exist ) .core;
	if ( this->current_cosegment == this->last_cosegment )  this->current_cosegment = nullptr;  }
	// if we are near the boundary, current_cosegment may be nullptr
	// the calling program must check this using the 'in_range' method

// the above could get simpler and faster by avoiding the use of wrappers


void Mesh::Iterator::Around::OneCell::OfCodimTwo
         ::OverCoVertices::ReverseOrder::BuildReverseCells::advance ( )
// virtual from Mesh::Iterator::Core

{	Cell this_center ( tag::whose_core_is, this->center,
	                   tag::previously_existing, tag::surely_not_null );
	Mesh m ( tag::whose_core_is, this->msh_p, tag::previously_existing, tag::is_positive );
	if ( this->current_cosegment == nullptr )  // out of range
	{	this->current_covertex = nullptr;  return;  }
	Cell new_ver = this->current_cosegment->boundary() .cell_in_front_of
		( this_center, tag::surely_exists ) .reverse ( tag::build_if_not_exists );
	this->current_covertex = new_ver .core;
	this->current_cosegment = m .cell_behind ( new_ver, tag::may_not_exist ) .core;
	if ( this->current_cosegment == this->last_cosegment )  this->current_cosegment = nullptr;  }
	// if we are near the boundary, current_cosegment may be nullptr
	// the calling program must check this using the 'in_range' method

// the above could get simpler and faster by avoiding the use of wrappers


bool Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoVertices::in_range ( )
// virtual from Mesh::Iterator::Core
{	return this->current_covertex;   }

//------------------------------------------------------------------------------------------------------//


void Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoSegments::reset ( )
// virtual from Mesh::Iterator::Core

{	this->current_cosegment = this->first_cosegment;
	assert ( this->center->is_positive() );
	assert ( this->current_cosegment );              }	


void Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoSegments::NormalOrder::reset
( const tag::StartAt &, Cell::Core * cll_p )  // virtual from Mesh::Iterator::Core

{	assert ( this->center->is_positive() );
	Cell cll ( tag::whose_core_is, cll_p,
             tag::previously_existing, tag::surely_not_null );
	Mesh m ( tag::whose_core_is, this->msh_p, tag::previously_existing, tag::is_positive );
	assert ( cll .belongs_to ( m, tag::same_dim, tag::orientation_compatible_with_mesh ) );
	this->current_cosegment = cll_p;
	if ( this->last_covertex )  // closed loop
	{	this->first_cosegment = cll_p;
		Cell this_center ( tag::whose_core_is, this->center,
		                   tag::previously_existing, tag::surely_not_null );
		this->last_covertex = cll .boundary() .cell_in_front_of
			( this_center, tag::surely_exists ) .reverse ( tag::surely_exists ) .core;  }
	assert ( this->current_cosegment );                                                      }

// the above could get simpler and faster by using a method like
// inline Cell::Core * Mesh::Core::cell_behind_ptr
// ( const Cell::Core * face, const tag::MayNotExist & ) const
// which is not difficult to implement


void Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoSegments::ReverseOrder::reset
( const tag::StartAt &, Cell::Core * cll_p )  // virtual from Mesh::Iterator::Core

{	assert ( this->center->is_positive() );
	Cell cll ( tag::whose_core_is, cll_p,
             tag::previously_existing, tag::surely_not_null );
	Mesh m ( tag::whose_core_is, this->msh_p, tag::previously_existing, tag::is_positive );
	assert ( cll.belongs_to ( m, tag::same_dim, tag::orientation_compatible_with_mesh ) );
	this->current_cosegment = cll_p;
	if ( this->last_covertex )  // closed loop
	{	this->first_cosegment = cll_p;
		Cell this_center ( tag::whose_core_is, this->center,
		                   tag::previously_existing, tag::surely_not_null );
		this->last_covertex = cll .boundary() .cell_behind
			( this_center, tag::surely_exists ) .reverse ( tag::surely_exists ) .core;  }
	assert ( this->current_cosegment );                                                      }

// the above could get simpler and faster by using a method like
// inline Cell::Core * Mesh::Core::cell_behind_ptr
// ( const Cell::Core * face, const tag::MayNotExist & ) const
// which is not difficult to implement


void Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoSegments::NormalOrder::advance ( )
// virtual from Mesh::Iterator::Core

{	Cell this_center ( tag::whose_core_is, this->center,
	                   tag::previously_existing, tag::surely_not_null );
	Mesh m ( tag::whose_core_is, this->msh_p, tag::previously_existing, tag::is_positive );
	assert ( this->current_cosegment );
	Cell new_ver = this->current_cosegment->boundary() .cell_behind
		( this_center, tag::surely_exists );
	if ( new_ver .core == this->last_covertex )
		this->current_cosegment = nullptr;
	else
		this->current_cosegment = m .cell_in_front_of ( new_ver, tag::may_not_exist ) .core;   }

// the above could get simpler and faster by avoiding the use of wrappers


void Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoSegments::ReverseOrder::advance ( )
// virtual from Mesh::Iterator::Core

{	Cell this_center ( tag::whose_core_is, this->center,
	                   tag::previously_existing, tag::surely_not_null );
	Mesh m ( tag::whose_core_is, this->msh_p, tag::previously_existing, tag::is_positive );
	assert ( this->current_cosegment );
	Cell new_ver = this->current_cosegment->boundary() .cell_in_front_of
		( this_center, tag::surely_exists );
	if ( new_ver .core == this->last_covertex )
		this->current_cosegment = nullptr;
	else
		this->current_cosegment = m .cell_in_front_of ( new_ver, tag::may_not_exist ) .core;   }

// the above could get simpler and faster by avoiding the use of wrappers


Cell Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoSegments::NormalOrder::AsFound::deref ( )
// virtual from Mesh::Iterator::Core

{	return Cell ( tag::whose_core_is, this->current_cosegment,
	              tag::previously_existing, tag::surely_not_null );  }


Cell Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoSegments::ReverseOrder::AsFound::deref ( )
// virtual from Mesh::Iterator::Core

{	return Cell ( tag::whose_core_is, this->current_cosegment,
                 tag::previously_existing, tag::surely_not_null );  }


bool Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoSegments::in_range ( )
// virtual from Mesh::Iterator::Core

{	return this->current_cosegment;  }

	// for an open chain, last_vertex stays nullptr
	// for a closed loop, it is used as stopping criterion

//------------------------------------------------------------------------------------------------------//


void Mesh::Iterator::Around::OneCell::OfCodimAtLeastTwo::reset  // virtual from Mesh::Iterator::Core
( const tag::StartAt &, Cell::Core * cll )
{	std::cout << __FILE__ << ":" << __LINE__ << ": " << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "Cannot reset an iterator this way." << std::endl;
	exit (1);                                                                                       }


inline void Mesh::Iterator::Around::OneVertex::ReturnSegments::further_advance ( )

// this->iter points towards a segment but we need to search further
// the segment must belong to msh_p

{	Cell::Positive::Vertex * centre = tag::Util::assert_cast
		< Cell::Positive *, Cell::Positive::Vertex * > ( this->center );
	for ( ; this->iter != centre->segments .end(); this->iter++ )
	{	Cell::Positive::Segment * seg_p = this->iter->first;
		// does seg_p belong to this->msh_p ? note that this->msh_p has dimension at least two
		maptype & seg_msd = seg_p->meshes  // keep this difference as an attribute of the class !
			[ tag::Util::assert_diff ( this->msh_p->get_dim_plus_one(), 2 ) ];
		if ( seg_msd .find ( this->msh_p ) != seg_msd .end() )  return;       }
	// iterator out of range
	return;                                                                    }
		
		
void Mesh::Iterator::Around::OneVertex::ReturnSegments::reset ( )
// virtual from Mesh::Iterator::Core
{	Cell::Positive::Vertex * centre = tag::Util::assert_cast
		< Cell::Positive *, Cell::Positive::Vertex * > ( this->center );
	this->iter = centre->segments .begin();
	this->further_advance();                                            }


inline void
Mesh::Iterator::Around::OneCell::OfCodimAtLeastTwo::ReturnCellsOfDimPlusOne::further_advance ( )

// this->iter points towards a Cell but we need to search further
// we must ensure that the cell belongs to msh_p

{	Cell::Positive::NotVertex * centre = tag::Util::assert_cast
		< Cell::Positive *, Cell::Positive::NotVertex * > ( this->center );
	maptype_same_dim & mshes = centre->meshes_same_dim;
	for ( ; this->iter != mshes .end(); this->iter ++ )
	{	Mesh::Core * msh_loc = this->iter->first;
		// is 'msh_loc' the boundary of some cell ?
		Cell::Positive * encl = msh_loc->cell_enclosed;
		if ( encl == nullptr )  continue;
		Cell::Positive::NotVertex * encl_nv = tag::Util::assert_cast
			< Cell::Positive *, Cell::Positive::NotVertex * > ( encl );
		// does 'encl' belong to this->msh_p ? note that this->msh_p has dimension at least two
		maptype & encl_md = encl_nv->meshes [ this->index_diff_msh ];
		if ( encl_md .find ( this->msh_p ) != encl_md .end() )  return;  }
	// iterator out of range
	return;                                                                                  }
		
		
void Mesh::Iterator::Around::OneCell::OfCodimAtLeastTwo::ReturnCellsOfDimPlusOne::reset ( )
// virtual from Mesh::Iterator::Core
{	Cell::Positive::NotVertex * centr = tag::Util::assert_cast
		< Cell::Positive *, Cell::Positive::NotVertex * > ( this->center );
	this->iter = centr->meshes_same_dim .begin();
	this->further_advance();                                               }


inline void
Mesh::Iterator::Around::OneCell::OfCodimAtLeastTwo::ReturnCellsOfHighDim::AsFound::further_advance ( )

// this->iter points towards a cell but we need to search further
// we must ensure that the cell belongs to msh_p

{	const maptype & tcmd = this->center->meshes [ this->index_diff_center ];
	for ( ; this->iter != tcmd .end(); this->iter++ )
	{	Mesh::Core * msh_loc = this->iter->first;
		// is 'msh_loc' the boundary of some cell ?
		Cell::Positive * encl = msh_loc->cell_enclosed;
		if ( encl == nullptr )  continue;
		// does 'encl' belong to this->msh_p ? note that this->msh_p has dimension at least two
		const maptype & encl_md = encl->meshes [ this->index_diff_msh ];
		if ( encl_md .find ( this->msh_p ) != encl_md .end() )  return;   }
	// iterator out of range
	return;                                                                   }
		
		
void Mesh::Iterator::Around::OneCell::OfCodimAtLeastTwo::ReturnCellsOfHighDim::AsFound::reset ( )
// virtual from Mesh::Iterator::Core
{	this->iter = this->center->meshes [ this->index_diff_center ] .begin();
	this->further_advance();                                                 }


inline void Mesh::Iterator::Around::OneCell::OfCodimAtLeastTwo::ReturnCellsOfMaxDim::
further_advance ( )

// this->iter points towards a cell but we need to search further
// we must ensure that the cell belongs to msh_p

{	maptype & mshes = this->center->meshes [ this->index_diff_center ];
	for ( ; this->iter != mshes .end(); this->iter ++ )
	{	Mesh::Core * msh_loc = this->iter->first;
		// is 'msh_loc' the boundary of some cell ?
		Cell::Positive * encl = msh_loc->cell_enclosed;
		if ( encl == nullptr )  continue;
		Cell::Positive::NotVertex * encl_nv = tag::Util::assert_cast
			< Cell::Positive *, Cell::Positive::NotVertex * > ( encl );
		// does 'encl' belong to this->msh_p ? note that this->msh_p has dimension at least two
		maptype_same_dim & encl_msd = encl_nv->meshes_same_dim;
		if ( encl_msd .find ( this->msh_p ) != encl_msd .end() )  return;  }
	// iterator out of range
	return;                                                                 }
		
		
void Mesh::Iterator::Around::OneCell::OfCodimAtLeastTwo::ReturnCellsOfMaxDim::reset ( )
// virtual from Mesh::Iterator::Core
{	this->iter = this->center->meshes [ this->index_diff_center ] .begin();
	this->further_advance();                                                 }


void Mesh::Iterator::Around::OneCell::OfCodimOne::ReturnCellsOfMaxDim::reset
( const tag::StartAt &, Cell::Core * cll )  // virtual from Mesh::Iterator::Core
{	std::cout << __FILE__ << ":" << __LINE__ << ": " << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "Cannot reset an iterator this way." << std::endl;
	exit (1);                                                                                       }


inline void Mesh::Iterator::Around::OneCell::OfCodimOne::ReturnCellsOfMaxDim::
further_advance ( )

// this->iter points towards a cell but we need to search further
// we must ensure that the cell belongs to msh_p

{	Cell::Positive::NotVertex * centre = tag::Util::assert_cast
			< Cell::Positive *, Cell::Positive::NotVertex * > ( this->center );
	const maptype_same_dim & mshes = centre->meshes_same_dim;
	for ( ; this->iter != mshes .end(); this->iter ++ )
	{	Mesh::Core * msh_loc = this->iter->first;
		// is 'msh_loc' the boundary of some cell ?
		Cell::Positive * encl = msh_loc->cell_enclosed;
		if ( encl == nullptr )  continue;
		Cell::Positive::NotVertex * encl_nv = tag::Util::assert_cast
			< Cell::Positive *, Cell::Positive::NotVertex * > ( encl );
		// does 'encl' belong to this->msh_p ? note that this->msh_p has dimension at least two
		const maptype_same_dim & encl_msd = encl_nv->meshes_same_dim;
		if ( encl_msd .find ( this->msh_p ) != encl_msd .end() )  return;  }
	// iterator out of range
	return;                                                                  }
		
		
void Mesh::Iterator::Around::OneCell::OfCodimOne::ReturnCellsOfMaxDim::reset ( )
// virtual from Mesh::Iterator::Core
{	Cell::Positive::NotVertex * centr = tag::Util::assert_cast
		< Cell::Positive *, Cell::Positive::NotVertex * > ( this->center );
	this->iter = centr->meshes_same_dim .begin();
	this->further_advance();                                               }


void Mesh::Iterator::Around::OneVertex::ReturnSegments::advance ( )
// virtual from Mesh::Iterator::Core
{	this->iter ++;
	this->further_advance();  }


void Mesh::Iterator::Around::OneCell::OfCodimAtLeastTwo::ReturnCellsOfDimPlusOne::advance ( )
// virtual from Mesh::Iterator::Core
{	this->iter ++;
	this->further_advance();  }


void Mesh::Iterator::Around::OneCell::OfCodimAtLeastTwo::ReturnCellsOfHighDim::AsFound::advance ( )
// virtual from Mesh::Iterator::Core
{	this->iter ++;
	this->further_advance();  }


void Mesh::Iterator::Around::OneCell::OfCodimAtLeastTwo::ReturnCellsOfMaxDim::advance ( )
// virtual from Mesh::Iterator::Core
{	this->iter ++;
	this->further_advance();  }


void Mesh::Iterator::Around::OneCell::OfCodimOne::ReturnCellsOfMaxDim::advance ( )
// virtual from Mesh::Iterator::Core
{	this->iter ++;
	this->further_advance();  }


bool Mesh::Iterator::Around::OneVertex::ReturnSegments::in_range ( )
// virtual from Mesh::Iterator::Core
{	Cell::Positive::Vertex * centre = tag::Util::assert_cast
		< Cell::Positive *, Cell::Positive::Vertex * > ( this->center );
	return  this->iter != centre->segments .end();  }


bool Mesh::Iterator::Around::OneCell::OfCodimAtLeastTwo::ReturnCellsOfDimPlusOne::in_range ( )
// virtual from Mesh::Iterator::Core
{	Cell::Positive::NotVertex * centre = tag::Util::assert_cast
		< Cell::Positive *, Cell::Positive::NotVertex * > ( this->center );
	return  this->iter != centre->meshes_same_dim .end();                  }


bool Mesh::Iterator::Around::OneCell::OfCodimAtLeastTwo::ReturnCellsOfHighDim::AsFound::in_range ( )
// virtual from Mesh::Iterator::Core
{	return  this->iter != this->center->meshes [ this->index_diff_center ] .end();  }


bool Mesh::Iterator::Around::OneCell::OfCodimAtLeastTwo::ReturnCellsOfMaxDim::in_range ( )
// virtual from Mesh::Iterator::Core
{	return  this->iter != this->center->meshes [ this->index_diff_center ] .end();  }


bool Mesh::Iterator::Around::OneCell::OfCodimOne::ReturnCellsOfMaxDim::in_range ( )
// virtual from Mesh::Iterator::Core
{	Cell::Positive::NotVertex * centre = tag::Util::assert_cast
		< Cell::Positive *, Cell::Positive::NotVertex * > ( this->center );
	return  this->iter != centre->meshes_same_dim .end();                  }


Cell Mesh::Iterator::Around::OneVertex::ReturnSegments::AsFound::deref ( )
// virtual from Mesh::Iterator::Core

{	Cell::Positive::Segment * const & seg = this->iter->first;
	// we do not care about the orientation, return seg which happens to be positive
	return  Cell ( tag::whose_core_is, seg, tag::previously_existing, tag::surely_not_null );   }


Cell Mesh::Iterator::OverSegments::WhoseTipIsC::deref ( )
// virtual from Mesh::Iterator::Core

{	Cell::Positive::Segment * const & seg = this->iter->first;
	short int s = this->iter->second;
	if ( s == 1 )
		return  Cell ( tag::whose_core_is, seg, tag::previously_existing, tag::surely_not_null );
	// else
	assert ( s == -1 );
	if ( not seg->reverse_attr .exists() )
		seg->reverse_attr = Cell
			( tag::whose_core_is, seg->build_reverse ( tag::one_dummy_wrapper ), tag::move );
	return seg->reverse_attr;                                                                    }


Cell Mesh::Iterator::OverSegments::WhoseTipIsC::ReverseEachCell::deref ( )
// virtual from Mesh::Iterator::Core

{	Cell::Positive::Segment * const & seg = this->iter->first;
	short int s = this->iter->second;
	if ( s == -1 )
		return  Cell ( tag::whose_core_is, seg, tag::previously_existing, tag::surely_not_null );
	// else
	assert ( s == 1 );
	if ( not seg->reverse_attr .exists() )
		seg->reverse_attr = Cell
			( tag::whose_core_is, seg->build_reverse ( tag::one_dummy_wrapper ), tag::move );
	return seg->reverse_attr;                                                                    }


Cell Mesh::Iterator::OverVertices::ConnectedToCenter::deref ( )
// virtual from Mesh::Iterator::Core

{	Cell::Positive::Segment * const & seg = this->iter->first;
	short int s = this->iter->second;
	if ( s == 1 )
		return  seg->base_attr .reverse ( tag::surely_exists );
	// else
	assert ( s == -1 );
	return seg->tip_attr;                                       }


Cell Mesh::Iterator::OverVertices::ConnectedToCenter::Ordered::deref ( )
// virtual from Mesh::Iterator::Core, overrides definition from
// Mesh::Iterator::Around::OneCell::OfCodimTwo
//    ::OverCoVertices::NormalOrder::BuildReverseCells::AsFound
{	return this->current_covertex->base() .reverse ( tag::surely_exists );  }


Cell Mesh::Iterator::Around::OneCell::OfCodimAtLeastTwo::ReturnCellsOfDimPlusOne::AsFound::deref ( )
// virtual from Mesh::Iterator::Core

{	Mesh::Core * msh_loc = this->iter->first;
	// 'msh_loc' must be the boundary of some cell, 'advance' took care of this
	Cell::Positive * encl = msh_loc->cell_enclosed;
	assert ( encl );
	// 'encl' must belong to this->msh_p;  note that this->msh_p has dimension at least two
	#ifndef NDEBUG  // DEBUG mode
	Cell::Positive::NotVertex * encl_nv = tag::Util::assert_cast
		< Cell::Positive *, Cell::Positive::NotVertex * > ( encl );
	maptype & encl_md = encl_nv->meshes [ this->index_diff_msh ];
	#endif  // DEBUG
	assert ( encl_md .find ( this->msh_p ) != encl_md .end() );
	return  Cell ( tag::whose_core_is, encl, tag::previously_existing, tag::surely_not_null );  }


Cell Mesh::Iterator::Around::OneCell::  // virtual from Mesh::Iterator::Core
OfCodimAtLeastTwo::ReturnCellsOfDimPlusOne::OrientCompatWithCenter::deref ( )

{	Mesh::Core * msh_loc = this->iter->first;
	// 'msh_loc' must be the boundary of some cell, 'advance' took care of this
	Cell::Positive * encl = msh_loc->cell_enclosed;
	assert ( encl );
	// 'encl' must belong to this->msh_p;  note that this->msh_p has dimension at least two
	#ifndef NDEBUG  // DEBUG mode
	Cell::Positive::NotVertex * encl_nv = tag::Util::assert_cast
		< Cell::Positive *, Cell::Positive::NotVertex * > ( encl );
	assert ( encl_nv->meshes_same_dim .find ( this->msh_p ) != encl_nv->meshes_same_dim .end() );
	#endif  // DEBUG
	Cell res ( tag::whose_core_is, encl, tag::previously_existing, tag::surely_not_null );
	if ( this->iter->second .sign == 1 )  return res;
	assert ( this->iter->second .sign == -1 );
	return  res .reverse ( tag::build_if_not_exists );                                             }


Cell Mesh::Iterator::Around::OneCell::  // virtual from Mesh::Iterator::Core
OfCodimAtLeastTwo::ReturnCellsOfDimPlusOne::OrientOpposToCenter::deref ( )

{	Mesh::Core * msh_loc = this->iter->first;
	// 'msh_loc' must be the boundary of some cell, 'advance' took care of this
	Cell::Positive * encl = msh_loc->cell_enclosed;
	assert ( encl );
	// 'encl' must belong to this->msh_p;  note that this->msh_p has dimension at least two
	#ifndef NDEBUG  // DEBUG mode
	Cell::Positive::NotVertex * encl_nv = tag::Util::assert_cast
		< Cell::Positive *, Cell::Positive::NotVertex * > ( encl );
	assert ( encl_nv->meshes_same_dim .find ( this->msh_p ) != encl_nv->meshes_same_dim .end() );
	#endif  // DEBUG
	Cell res ( tag::whose_core_is, encl, tag::previously_existing, tag::surely_not_null );
	if ( this->iter->second .sign == -1 )  return res;
	assert ( this->iter->second .sign == 1 );
	return  res .reverse ( tag::build_if_not_exists );                                             }


Cell Mesh::Iterator::Around::OneCell::OfCodimAtLeastTwo::ReturnCellsOfHighDim::AsFound::deref ( )
// virtual from Mesh::Iterator::Core

{	Mesh::Core * msh_loc = this->iter->first;
	// 'msh_loc' must be the boundary of some cell, 'advance' took care of this
	Cell::Positive * encl = msh_loc->cell_enclosed;
	assert ( encl );
	// 'encl' must belong to this->msh_p;  note that this->msh_p has dimension at least two
	assert ( encl->meshes [ this->index_diff_msh ] .find ( this->msh_p )
	         != encl->meshes [ this->index_diff_msh ] .end()             );
	return Cell ( tag::whose_core_is, encl, tag::previously_existing, tag::surely_not_null );  }


Cell Mesh::Iterator::Around::OneCell::OfCodimAtLeastTwo::ReturnCellsOfMaxDim::AsFound::deref ( )
// virtual from Mesh::Iterator::Core

{	Mesh::Core * msh_loc = this->iter->first;
	// 'msh_loc' must be the boundary of some cell, 'advance' took care of this
	Cell::Positive * encl = msh_loc->cell_enclosed;
	assert ( encl );
	// 'encl' must belong to this->msh_p;  note that this->msh_p has dimension at least two
	#ifndef NDEBUG  // DEBUG mode
	Cell::Positive::NotVertex * encl_nv = tag::Util::assert_cast
		< Cell::Positive *, Cell::Positive::NotVertex * > ( encl );
	#endif  // DEBUG
	assert ( encl_nv->meshes_same_dim .find ( this->msh_p ) != encl_nv->meshes_same_dim .end() );
	return  Cell ( tag::whose_core_is, encl, tag::previously_existing, tag::surely_not_null );  }


Cell Mesh::Iterator::Around::OneCell::OfCodimAtLeastTwo::
ReturnCellsOfMaxDim::OrientCompatWithMesh::deref ( )    // virtual from Mesh::Iterator::Core

{	Mesh::Core * msh_loc = this->iter->first;
	// 'msh_loc' must be the boundary of some cell, 'advance' took care of this
	Cell::Positive * encl = msh_loc->cell_enclosed;
	assert ( encl );
	// 'encl' must belong to this->msh_p;  note that this->msh_p has dimension at least two
	Cell::Positive::NotVertex * encl_nv = tag::Util::assert_cast
		< Cell::Positive *, Cell::Positive::NotVertex * > ( encl );
	std::map < Mesh::Core*, Cell::field_to_meshes_same_dim > ::const_iterator
		it = encl_nv->meshes_same_dim .find ( this->msh_p );
	assert ( it != encl_nv->meshes_same_dim .end() );
	Cell res ( tag::whose_core_is, encl, tag::previously_existing, tag::surely_not_null );
	if ( it->second .sign == 1 )  return res;
	assert ( it->second .sign == -1 );
	return  res .reverse ( tag::surely_exists );                                            }


Cell Mesh::Iterator::Around::OneCell::OfCodimAtLeastTwo::ReturnCellsOfMaxDim::OrientOpposToMesh::deref ( )
// virtual from Mesh::Iterator::Core

{	Mesh::Core * msh_loc = this->iter->first;
	// 'msh_loc' must be the boundary of some cell, 'advance' took care of this
	Cell::Positive * encl = msh_loc->cell_enclosed;
	assert ( encl );
	// 'encl' must belong to this->msh_p;  note that this->msh_p has dimension at least two
	Cell::Positive::NotVertex * encl_nv = tag::Util::assert_cast
		< Cell::Positive *, Cell::Positive::NotVertex * > ( encl );
	std::map < Mesh::Core*, Cell::field_to_meshes_same_dim > ::const_iterator
		it = encl_nv->meshes_same_dim .find ( this->msh_p );
	assert ( it != encl_nv->meshes_same_dim .end() );
	Cell res ( tag::whose_core_is, encl, tag::previously_existing, tag::surely_not_null );
	if ( it->second .sign == -1 )  return res;
	assert ( it->second .sign == 1 );
	return  res .reverse ( tag::build_if_not_exists );                                      }


Cell Mesh::Iterator::Around::OneCell::OfCodimOne::ReturnCellsOfMaxDim::AsFound::deref ( )
// virtual from Mesh::Iterator::Core

{	Mesh::Core * msh_loc = this->iter->first;
	// 'msh_loc' must be the boundary of some cell, 'advance' took care of this
	Cell::Positive * encl = msh_loc->cell_enclosed;
	assert ( encl );
	#ifndef NDEBUG  // DEBUG mode
	// 'encl' must belong to this->msh_p;  note that this->msh_p has dimension at least two
	Cell::Positive::NotVertex * encl_nv = tag::Util::assert_cast
		< Cell::Positive *, Cell::Positive::NotVertex * > ( encl );
	std::map < Mesh::Core*, Cell::field_to_meshes_same_dim > ::const_iterator
		it = encl_nv->meshes_same_dim .find ( this->msh_p );
	assert ( it != encl_nv->meshes_same_dim .end() );
	#endif  // DEBUG
	return  Cell ( tag::whose_core_is, encl, tag::previously_existing, tag::surely_not_null );  }


Cell Mesh::Iterator::Around::OneCell::OfCodimOne::ReturnCellsOfMaxDim::OrientCompatWithMesh::deref ( )
// virtual from Mesh::Iterator::Core

{	Mesh::Core * msh_loc = this->iter->first;
	// 'msh_loc' must be the boundary of some cell, 'advance' took care of this
	Cell::Positive * encl = msh_loc->cell_enclosed;
	assert ( encl );
	// 'encl' must belong to this->msh_p;  note that this->msh_p has dimension at least two
	Cell::Positive::NotVertex * encl_nv = tag::Util::assert_cast
		< Cell::Positive *, Cell::Positive::NotVertex * > ( encl );
	std::map < Mesh::Core*, Cell::field_to_meshes_same_dim > ::const_iterator
		it = encl_nv->meshes_same_dim .find ( this->msh_p );
	assert ( it != encl_nv->meshes_same_dim .end() );
	Cell res ( tag::whose_core_is, encl, tag::previously_existing, tag::surely_not_null );
	if ( it->second .sign == 1 )  return res;
	assert ( it->second .sign == -1 );
	return  res .reverse ( tag::surely_exists );                                            }


Cell Mesh::Iterator::Around::OneCell::OfCodimOne::ReturnCellsOfMaxDim::OrientOpposToMesh::deref ( )
// virtual from Mesh::Iterator::Core

{	Mesh::Core * msh_loc = this->iter->first;
	// 'msh_loc' must be the boundary of some cell, 'advance' took care of this
	Cell::Positive * encl = msh_loc->cell_enclosed;
	assert ( encl );
	// 'encl' must belong to this->msh_p;  note that this->msh_p has dimension at least two
	Cell::Positive::NotVertex * encl_nv = tag::Util::assert_cast
		< Cell::Positive *, Cell::Positive::NotVertex * > ( encl );
	std::map < Mesh::Core*, Cell::field_to_meshes_same_dim > ::const_iterator
		it = encl_nv->meshes_same_dim .find ( this->msh_p );
	assert ( it != encl_nv->meshes_same_dim .end() );
	Cell res ( tag::whose_core_is, encl, tag::previously_existing, tag::surely_not_null );
	if ( it->second .sign == -1 )  return res;
	assert ( it->second .sign == 1 );
	return  res .reverse ( tag::build_if_not_exists );                                      }


Cell Mesh::Iterator::Around::OneCell::OfCodimOne::ReturnCellsOfMaxDim::OrientCompatWithCenter::deref ( )
// virtual from Mesh::Iterator::Core

{	Mesh::Core * msh_loc = this->iter->first;
	// 'msh_loc' must be the boundary of some cell, 'advance' took care of this
	Cell::Positive * encl = msh_loc->cell_enclosed;
	assert ( encl );
	// 'encl' must belong to this->msh_p;  note that this->msh_p has dimension at least two
	#ifndef NDEBUG  // DEBUG mode
	Cell::Positive::NotVertex * encl_nv = tag::Util::assert_cast
		< Cell::Positive *, Cell::Positive::NotVertex * > ( encl );
	std::map < Mesh::Core*, Cell::field_to_meshes_same_dim > ::const_iterator
		it = encl_nv->meshes_same_dim .find ( this->msh_p );
	assert ( it != encl_nv->meshes_same_dim .end() );
	#endif  // DEBUG
	Cell res ( tag::whose_core_is, encl, tag::previously_existing, tag::surely_not_null );
	if ( this->iter->second .sign == 1 )  return res;
	assert ( this->iter->second .sign == -1 );
	return  res .reverse ( tag::build_if_not_exists );                                      }


Cell Mesh::Iterator::Around::OneCell::OfCodimOne::ReturnCellsOfMaxDim::OrientOpposToCenter::deref ( )
// virtual from Mesh::Iterator::Core   !!!!!!!

{	Mesh::Core * msh_loc = this->iter->first;
	// 'msh_loc' must be the boundary of some cell, 'advance' took care of this
	Cell::Positive * encl = msh_loc->cell_enclosed;
	assert ( encl );
	// 'encl' must belong to this->msh_p;  note that this->msh_p has dimension at least two
	#ifndef NDEBUG  // DEBUG mode
	Cell::Positive::NotVertex * encl_nv = tag::Util::assert_cast
		< Cell::Positive *, Cell::Positive::NotVertex * > ( encl );
	std::map < Mesh::Core*, Cell::field_to_meshes_same_dim > ::const_iterator
		it = encl_nv->meshes_same_dim .find ( this->msh_p );
	assert ( it != encl_nv->meshes_same_dim .end() );
	#endif  // DEBUG
	Cell res ( tag::whose_core_is, encl, tag::previously_existing, tag::surely_not_null );
	if ( this->iter->second .sign == -1 )  return res;
	assert ( this->iter->second .sign == 1 );
	return  res .reverse ( tag::build_if_not_exists );                                      }

//------------------------------------------------------------------------------------------------------//


void Mesh::Iterator::ReturnOneCell::reset ( )  // virtual from Mesh::Iterator::Core
{	this->in_range_attr = true;  }


void Mesh::Iterator::AlwaysOutOfRange::reset ( )  // virtual from Mesh::Iterator::Core
{	}


void Mesh::Iterator::ReturnOneCell::reset ( const tag::StartAt &, Cell::Core * cll )
// virtual from Mesh::Iterator::Core

{	std::cout << __FILE__ << ":" << __LINE__ << ": " << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "Cannot reset an iterator this way." << std::endl;
	exit (1);                                                                                       }


void Mesh::Iterator::AlwaysOutOfRange::reset ( const tag::StartAt &, Cell::Core * cll )
// virtual from Mesh::Iterator::Core

{	std::cout << __FILE__ << ":" << __LINE__ << ": " << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "Cannot reset an iterator this way." << std::endl;
	exit (1);                                                                                       }


void Mesh::Iterator::ReturnOneCell::advance ( )  // virtual from Mesh::Iterator::Core
{	this->in_range_attr = false;  }


void Mesh::Iterator::AlwaysOutOfRange::advance ( )  // virtual from Mesh::Iterator::Core
{	}

	
bool Mesh::Iterator::ReturnOneCell::in_range ( )  // virtual from Mesh::Iterator::Core
{	return  this->in_range_attr;  }


bool Mesh::Iterator::AlwaysOutOfRange::in_range ( )  // virtual from Mesh::Iterator::Core
{	return false;  }

	
Cell Mesh::Iterator::ReturnOneCell::deref ( )  // virtual from Mesh::Iterator::Core
{	assert ( this->in_range_attr );
	return Cell ( tag::whose_core_is, this->the_only_cell,
                tag::previously_existing, tag::surely_not_null );  }


Cell Mesh::Iterator::AlwaysOutOfRange::deref ( )  // virtual from Mesh::Iterator::Core
{	assert ( false );
	return  Cell ( tag::non_existent );  }

//------------------------------------------------------------------------------------------------------//


void Cell::Iterator::OverMeshesOfSameDim::reset ( )  // virtual from Cell::Iterator::Core
{	this->iter = this->meshes_same_dim .begin();  }


void Cell::Iterator::OverMeshesOfGivenDim::OrientationIrrelevant::ReturnPositiveMesh::reset ( )
// virtual from Cell::Iterator::Core
{	this->iter = this->meshes .begin();  }


inline void Cell::Iterator::OverMeshesOfSameDim
                ::PositiveMeshesOnly::OrientationCompatWithCell::further_advance ( )
// to do: replace methods 'advance' and 'in_range' by corresponding code

{	while ( this->in_range() )
	{	assert ( ( this->iter->second .sign == 1 ) or ( this->iter->second .sign == -1 ) );
		if ( this->iter->second .sign == 1 )  return;
		this->advance();                                                                     }  }


inline void Cell::Iterator::OverMeshesOfSameDim
                ::PositiveMeshesOnly::OrientationOpposToCell::further_advance ( )
// to do: replace methods 'advance' and 'in_range' by corresponding code

{	while ( this->in_range() )
	{	assert ( ( this->iter->second .sign == 1 ) or ( this->iter->second .sign == -1 ) );
		if ( this->iter->second .sign == -1 )  return;
		this->advance();                                                                     }  }


void Cell::Iterator::OverMeshesOfSameDim::PositiveMeshesOnly::OrientationCompatWithCell::reset ( )
// virtual from Cell::Iterator::Core, here overridden
{	this->iter = this->meshes_same_dim .begin();
	this->further_advance();                      }


void Cell::Iterator::OverMeshesOfSameDim::PositiveMeshesOnly::OrientationOpposToCell::reset ( )
// virtual from Cell::Iterator::Core, here overridden
{	this->iter = this->meshes_same_dim .begin();
	this->further_advance();                      }


void Cell::Iterator::OverMeshesOfSameDim::advance ( )  // virtual from Cell::Iterator::Core
{	this->iter ++;  }


void Cell::Iterator::OverMeshesOfSameDim::PositiveMeshesOnly::OrientationCompatWithCell::advance ( )
// virtual from Cell::Iterator::Core, here overridden
{	this->iter ++;
	this->further_advance();  }


void Cell::Iterator::OverMeshesOfSameDim::PositiveMeshesOnly::OrientationOpposToCell::advance ( )
// virtual from Cell::Iterator::Core, here overridden
{	this->iter ++;
	this->further_advance();  }


void Cell::Iterator::OverMeshesOfGivenDim::OrientationIrrelevant::ReturnPositiveMesh::advance ( )
// virtual from Cell::Iterator::Core
{	this->iter ++;  }


bool Cell::Iterator::OverMeshesOfSameDim::in_range ( )  // virtual from Mesh::Iterator::Core
{	return  this->iter != this->meshes_same_dim .end();  }

	
bool Cell::Iterator::OverMeshesOfGivenDim::OrientationIrrelevant::ReturnPositiveMesh::in_range ( )
// virtual from Mesh::Iterator::Core
{	return  this->iter != this->meshes .end();  }

	
Mesh Cell::Iterator::OverMeshesOfSameDim::OrientationCompatWithCell::deref ( )
// virtual from Mesh::Iterator::Core
{	assert ( ( this->iter->second .sign == 1 ) or ( this->iter->second .sign == -1 ) );
	if ( this->iter->second .sign == 1 )
		return  Mesh ( tag::whose_core_is, this->iter->first, tag::previously_existing );
	return  Mesh ( tag::from_positive_mesh, tag::whose_core_is, this->iter->first,
	               tag::build_negative, tag::do_not_build_cells                   );      }


Mesh Cell::Iterator::OverMeshesOfSameDim::OrientationOpposToCell::deref ( )
// virtual from Mesh::Iterator::Core
{	assert ( ( this->iter->second .sign == 1 ) or ( this->iter->second .sign == -1 ) );
	if ( this->iter->second .sign == -1 )
		return  Mesh ( tag::whose_core_is, this->iter->first, tag::previously_existing );
	return  Mesh ( tag::from_positive_mesh, tag::whose_core_is, this->iter->first,
	               tag::build_negative, tag::do_not_build_cells                   );      }


Mesh Cell::Iterator::OverMeshesOfSameDim::OrientationIrrelevant::ReturnPositiveMesh::deref ( )
// virtual from Mesh::Iterator::Core
{	return  Mesh ( tag::whose_core_is, this->iter->first, tag::previously_existing );  }


Mesh Cell::Iterator::OverMeshesOfSameDim::PositiveMeshesOnly::deref ( )
// virtual from Mesh::Iterator::Core
{	return  Mesh ( tag::whose_core_is, this->iter->first, tag::previously_existing );  }


Mesh Cell::Iterator::OverMeshesOfGivenDim::OrientationIrrelevant::ReturnPositiveMesh::deref ( )
// virtual from Mesh::Iterator::Core
{	return  Mesh ( tag::whose_core_is, this->iter->first, tag::previously_existing );  }

//------------------------------------------------------------------------------------------------------//


// 'clone' is virtual from Mesh::Iterator::Core

Mesh::Iterator::Core * Mesh::Iterator::Over::TwoVerticesOfSeg::NormalOrder::AsFound::clone ( )
{	return  new Mesh::Iterator::Over::TwoVerticesOfSeg::NormalOrder::AsFound ( * this );  }

Mesh::Iterator::Core * Mesh::Iterator::Over::TwoVerticesOfSeg::NormalOrder::ForcePositive::clone ( )
{	return  new Mesh::Iterator::Over::TwoVerticesOfSeg::NormalOrder::ForcePositive ( * this );  }

Mesh::Iterator::Core * Mesh::Iterator::Over::TwoVerticesOfSeg::ReverseOrder::AsFound::clone ( )
{	return  new Mesh::Iterator::Over::TwoVerticesOfSeg::ReverseOrder::AsFound ( * this );  }

Mesh::Iterator::Core * Mesh::Iterator::Over::TwoVerticesOfSeg::ReverseOrder::ForcePositive::clone ( )
{	return  new Mesh::Iterator::Over::TwoVerticesOfSeg::ReverseOrder::ForcePositive ( * this );  }

Mesh::Iterator::Core *
Mesh::Iterator::Over::TwoVerticesOfSeg::NormalOrder::ReverseEachCell::clone ( )
{	return  new Mesh::Iterator::Over::TwoVerticesOfSeg::NormalOrder::ReverseEachCell ( * this );  }

Mesh::Iterator::Core *
Mesh::Iterator::Over::TwoVerticesOfSeg::ReverseOrder::ReverseEachCell::clone ( )
{	return  new Mesh::Iterator::Over::TwoVerticesOfSeg::ReverseOrder::ReverseEachCell ( * this );  }

Mesh::Iterator::Core * Mesh::Iterator::OverVertices::OfConnectedOneDimMesh::NormalOrder::clone ( )
{	return  new Mesh::Iterator::OverVertices::OfConnectedOneDimMesh::NormalOrder ( * this );  }

Mesh::Iterator::Core * Mesh::Iterator::OverVertices::OfConnectedOneDimMesh::ReverseOrder::clone ( )
{	return  new Mesh::Iterator::OverVertices::OfConnectedOneDimMesh::ReverseOrder ( * this );  }

Mesh::Iterator::Core *
Mesh::Iterator::OverSegments::OfConnectedOneDimMesh::NormalOrder::AsFound::clone ( )
{	return  new Mesh::Iterator::OverSegments::OfConnectedOneDimMesh::NormalOrder::AsFound ( * this );  }

Mesh::Iterator::Core *
Mesh::Iterator::OverSegments::OfConnectedOneDimMesh::NormalOrder::ForcePositive::clone ( )
{	return
		new Mesh::Iterator::OverSegments::OfConnectedOneDimMesh::NormalOrder::ForcePositive ( * this );  }

Mesh::Iterator::Core * Mesh::Iterator::OverSegments::OfConnectedOneDimMesh
                           ::NormalOrder::ReverseEachCell::clone ( )
{	return  new Mesh::Iterator::OverSegments::OfConnectedOneDimMesh
	                ::NormalOrder::ReverseEachCell ( * this );       }

Mesh::Iterator::Core *
Mesh::Iterator::OverSegments::OfConnectedOneDimMesh::ReverseOrder::AsFound::clone ( )
{	return  new Mesh::Iterator::OverSegments::OfConnectedOneDimMesh::ReverseOrder::AsFound ( * this );  }

Mesh::Iterator::Core *
Mesh::Iterator::OverSegments::OfConnectedOneDimMesh::ReverseOrder::ForcePositive::clone ( )
{	return  new Mesh::Iterator::OverSegments::OfConnectedOneDimMesh
	                ::ReverseOrder::ForcePositive ( * this );        }

Mesh::Iterator::Core * Mesh::Iterator::OverSegments::OfConnectedOneDimMesh
                           ::ReverseOrder::ReverseEachCell::clone ( )
{	return  new Mesh::Iterator::OverSegments::OfConnectedOneDimMesh
	                ::ReverseOrder::ReverseEachCell ( * this );      }

Mesh::Iterator::Core * Mesh::Iterator::OverCells::OfFuzzyMesh::AsFound::clone ( )
{	return  new Mesh::Iterator::OverCells::OfFuzzyMesh::AsFound ( * this );  }

Mesh::Iterator::Core * Mesh::Iterator::OverCells::OfFuzzyMesh::ForcePositive::clone ( )
{	return  new Mesh::Iterator::OverCells::OfFuzzyMesh::ForcePositive ( * this );  }

Mesh::Iterator::Core *
Mesh::Iterator::OverCells::OfFuzzyMesh::ReverseEachCell::clone ( )
{	return  new Mesh::Iterator::OverCells::OfFuzzyMesh::ReverseEachCell ( * this );  }

Mesh::Iterator::Core * Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoVertices
                           ::NormalOrder::BuildReverseCells::AsFound::clone ( )
{	return  new Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoVertices
	                ::NormalOrder::BuildReverseCells::AsFound ( * this );  }

Mesh::Iterator::Core * Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoVertices
                           ::NormalOrder::BuildReverseCells::ForcePositive::clone ( )
{	return  new Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoVertices
	                ::NormalOrder::BuildReverseCells::ForcePositive ( * this );  }

Mesh::Iterator::Core * Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoVertices
                           ::NormalOrder::BuildReverseCells::ReverseEachCell::clone ( )
{	return  new Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoVertices
	                ::NormalOrder::BuildReverseCells::ReverseEachCell ( * this );  }

Mesh::Iterator::Core * Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoVertices
                           ::ReverseOrder::BuildReverseCells::AsFound::clone ( )
{	return  new Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoVertices
	                ::ReverseOrder::BuildReverseCells::AsFound ( * this );  }

Mesh::Iterator::Core * Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoVertices
                           ::ReverseOrder::BuildReverseCells::ForcePositive::clone ( )
{	return  new Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoVertices
	                ::ReverseOrder::BuildReverseCells::ForcePositive ( * this );  }

Mesh::Iterator::Core * Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoVertices
                           ::ReverseOrder::BuildReverseCells::ReverseEachCell::clone ( )
{	return  new Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoVertices
	                ::ReverseOrder::BuildReverseCells::ReverseEachCell ( * this );  }

Mesh::Iterator::Core *
Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoSegments::NormalOrder::AsFound::clone ( )
{	return  new Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoSegments
	                ::NormalOrder::AsFound ( * this );                     }

Mesh::Iterator::Core *
Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoSegments::ReverseOrder::AsFound::clone ( )
{	return  new Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoSegments
	                ::ReverseOrder::AsFound ( * this );                    }

Mesh::Iterator::Core *
Mesh::Iterator::Around::OneCell::OfCodimTwo::WorkAround2D::NormalOrder::BuildReverseCells::clone ( )
{	return  new Mesh::Iterator::Around::OneCell::OfCodimTwo::WorkAround2D
	                ::NormalOrder::BuildReverseCells ( * this );           }

Mesh::Iterator::Core *
Mesh::Iterator::Around::OneCell::OfCodimTwo::WorkAround2D::ReverseOrder::BuildReverseCells::clone ( )
{	return  new Mesh::Iterator::Around::OneCell::OfCodimTwo::WorkAround2D
	                ::ReverseOrder::BuildReverseCells ( * this );           }

Mesh::Iterator::Core *
Mesh::Iterator::Around::OneVertex::ReturnSegments::AsFound::clone ( )
{	return  new Mesh::Iterator::Around::OneVertex::ReturnSegments::AsFound ( * this );  }

Mesh::Iterator::Core * Mesh::Iterator::OverSegments::WhoseTipIsC::clone ( )
{	return  new Mesh::Iterator::OverSegments::WhoseTipIsC ( * this );  }

Mesh::Iterator::Core *
Mesh::Iterator::OverSegments::WhoseTipIsC::ReverseEachCell::clone ( )
{	return  new Mesh::Iterator::OverSegments::WhoseTipIsC::ReverseEachCell ( * this );  }

Mesh::Iterator::Core * Mesh::Iterator::OverVertices::ConnectedToCenter::clone ( )
{	return  new Mesh::Iterator::OverVertices::ConnectedToCenter ( * this );  }

Mesh::Iterator::Core * Mesh::Iterator::OverVertices::ConnectedToCenter::Ordered::clone ( )
{	return  new Mesh::Iterator::OverVertices::ConnectedToCenter::Ordered ( * this );  }

Mesh::Iterator::Core *
Mesh::Iterator::Around::OneCell::OfCodimAtLeastTwo::ReturnCellsOfDimPlusOne::AsFound::clone ( )
{	return  new Mesh::Iterator::Around::OneCell::
		OfCodimAtLeastTwo::ReturnCellsOfDimPlusOne::AsFound ( * this );  }

Mesh::Iterator::Core * Mesh::Iterator::Around::
OneCell::OfCodimAtLeastTwo::ReturnCellsOfDimPlusOne::OrientCompatWithCenter::clone ( )
{	return  new Mesh::Iterator::Around::OneCell::
		OfCodimAtLeastTwo::ReturnCellsOfDimPlusOne::OrientCompatWithCenter ( * this );  }

Mesh::Iterator::Core * Mesh::Iterator::Around::OneCell::
OfCodimAtLeastTwo::ReturnCellsOfDimPlusOne::OrientOpposToCenter::clone ( )
{	return  new Mesh::Iterator::Around::OneCell::
		OfCodimAtLeastTwo::ReturnCellsOfDimPlusOne::OrientOpposToCenter ( * this );  }

Mesh::Iterator::Core *
Mesh::Iterator::Around::OneCell::OfCodimAtLeastTwo::ReturnCellsOfHighDim::AsFound::clone ( )
{	return  new Mesh::Iterator::Around::OneCell::
		OfCodimAtLeastTwo::ReturnCellsOfHighDim::AsFound ( * this );  }

Mesh::Iterator::Core *
Mesh::Iterator::Around::OneCell::OfCodimAtLeastTwo::ReturnCellsOfMaxDim::AsFound::clone ( )
{	return  new Mesh::Iterator::Around::OneCell::
	            OfCodimAtLeastTwo::ReturnCellsOfMaxDim::AsFound ( * this );  }

Mesh::Iterator::Core *
Mesh::Iterator::Around::OneCell::OfCodimAtLeastTwo::ReturnCellsOfMaxDim::OrientCompatWithMesh::clone ( )
{	return  new Mesh::Iterator::Around::OneCell::
	            OfCodimAtLeastTwo::ReturnCellsOfMaxDim::OrientCompatWithMesh ( * this );  }

Mesh::Iterator::Core *
Mesh::Iterator::Around::OneCell::OfCodimAtLeastTwo::ReturnCellsOfMaxDim::OrientOpposToMesh::clone ( )
{	return  new Mesh::Iterator::Around::OneCell::OfCodimAtLeastTwo::ReturnCellsOfMaxDim::OrientOpposToMesh
	              ( * this );                                                                        }

Mesh::Iterator::Core *
Mesh::Iterator::Around::OneCell::OfCodimOne::ReturnCellsOfMaxDim::AsFound::clone ( )
{	return  new Mesh::Iterator::Around::OneCell::OfCodimOne::ReturnCellsOfMaxDim::AsFound
	              ( * this );                                                              }

Mesh::Iterator::Core *
Mesh::Iterator::Around::OneCell::OfCodimOne::ReturnCellsOfMaxDim::OrientCompatWithMesh::clone ( )
{	return  new Mesh::Iterator::Around::OneCell::
	            OfCodimOne::ReturnCellsOfMaxDim::OrientCompatWithMesh ( * this );  }

Mesh::Iterator::Core *
Mesh::Iterator::Around::OneCell::OfCodimOne::ReturnCellsOfMaxDim::OrientOpposToMesh::clone ( )
{	return  new Mesh::Iterator::Around::OneCell::
	            OfCodimOne::ReturnCellsOfMaxDim::OrientOpposToMesh ( * this );  }

Mesh::Iterator::Core *
Mesh::Iterator::Around::OneCell::OfCodimOne::ReturnCellsOfMaxDim::OrientCompatWithCenter::clone ( )
{	return  new Mesh::Iterator::Around::OneCell::
	            OfCodimOne::ReturnCellsOfMaxDim::OrientCompatWithCenter ( * this );  }

Mesh::Iterator::Core *
Mesh::Iterator::Around::OneCell::OfCodimOne::ReturnCellsOfMaxDim::OrientOpposToCenter::clone ( )
{	return  new Mesh::Iterator::Around::OneCell::
	            OfCodimOne::ReturnCellsOfMaxDim::OrientOpposToCenter ( * this );  }

Mesh::Iterator::Core * Mesh::Iterator::ReturnOneCell::clone ( )
{	return  new Mesh::Iterator::ReturnOneCell ( * this );  }

Mesh::Iterator::Core * Mesh::Iterator::AlwaysOutOfRange::clone ( )
{	return  new Mesh::Iterator::AlwaysOutOfRange ( * this );  }

Cell::Iterator::Core * Cell::Iterator::OverMeshesOfSameDim::OrientationCompatWithCell::clone ( )
{	return  new Cell::Iterator::OverMeshesOfSameDim::OrientationCompatWithCell ( * this );  }

Cell::Iterator::Core * Cell::Iterator::OverMeshesOfSameDim::OrientationOpposToCell::clone ( )
{	return  new Cell::Iterator::OverMeshesOfSameDim::OrientationOpposToCell ( * this );  }

Cell::Iterator::Core *
Cell::Iterator::OverMeshesOfSameDim::OrientationIrrelevant::ReturnPositiveMesh::clone ( )
{	return  new Cell::Iterator::OverMeshesOfSameDim::OrientationIrrelevant::ReturnPositiveMesh
		( * this );                                                                              }

Cell::Iterator::Core *
Cell::Iterator::OverMeshesOfSameDim::PositiveMeshesOnly::OrientationCompatWithCell::clone ( )
{	return  new Cell::Iterator::OverMeshesOfSameDim::PositiveMeshesOnly::OrientationCompatWithCell
		( * this );                                                                                  }

Cell::Iterator::Core *
Cell::Iterator::OverMeshesOfSameDim::PositiveMeshesOnly::OrientationOpposToCell::clone ( )
{	return  new Cell::Iterator::OverMeshesOfSameDim::PositiveMeshesOnly::OrientationOpposToCell
		( * this );                                                                               }

Cell::Iterator::Core *
Cell::Iterator::OverMeshesOfGivenDim::OrientationIrrelevant::ReturnPositiveMesh::clone ( )
{	return  new Cell::Iterator::OverMeshesOfGivenDim::OrientationIrrelevant::ReturnPositiveMesh
		( * this );                                                                               }

//------------------------------------------------------------------------------------------------------//


bool Cell::is_inner_to ( const Mesh & msh ) const

// we want to know whether we are in the interior or on the boundary of the mesh
// so we search for some first_cosegment, then rotate
	
{	assert ( msh .exists() );
	assert ( this->exists() );
	assert ( this->dim() < msh .dim() );

	if ( this->dim() + 2 == msh .core->get_dim_plus_one() )  // co-dimension one
		return ( msh .cell_in_front_of ( *this, tag::may_not_exist ) .exists() and
		         msh .cell_behind      ( *this, tag::may_not_exist ) .exists()     );

	assert ( this->dim() + 3 == msh .core->get_dim_plus_one() );  // co-dimension two
	// to do: add code for co-dimension three or higher ! how do we do that ?

	Cell::Positive * this_pos = tag::Util::assert_cast
		< Cell::Core *, Cell::Positive * > ( this->core );
	Cell::Core * first_cosegment =
		tag::local_functions::iterator::find_first_coseg ( this_pos, msh .core );
	assert ( first_cosegment );

	// now we rotate forward until we meet the boundary or close the loop
	Cell covertex_stop = first_cosegment->boundary() .cell_in_front_of
		( * this, tag::surely_exists );

	while ( true )
	{	Cell covertex = first_cosegment->boundary() .cell_behind ( * this, tag::surely_exists );
		Cell new_coseg = msh .cell_in_front_of ( covertex, tag::may_not_exist );
		if ( not new_coseg .exists() ) return false;  // we have hit the boundary
		if ( covertex .reverse ( tag::surely_exists ) == covertex_stop )  // completed loop
			return true;
		first_cosegment = new_coseg .core;                                                        }  }

//------------------------------------------------------------------------------------------------------//


