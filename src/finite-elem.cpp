
//   finite-elem.cpp  2025.01.29

//   This file is part of maniFEM, a C++ library for meshes and finite elements on manifolds.

//   Copyright  2019 - 2025  Cristian Barbarosie  cristian.barbarosie@gmail.com

//   https://maniFEM.rd.ciencias.ulisboa.pt/
//   https://codeberg.org/cristian.barbarosie/maniFEM

//   ManiFEM is free software: you can redistribute it and/or modify it
//   under the terms of the GNU Lesser General Public License as published
//   by the Free Software Foundation, either version 3 of the License
//   or (at your option) any later version.

//   ManiFEM is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//   See the GNU Lesser General Public License for more details.

//   You should have received a copy of the GNU Lesser General Public License
//   along with maniFEM.  If not, see <https://www.gnu.org/licenses/>.


#ifndef MANIFEM_NO_FEM

#include "math.h"
#include <sstream>
#include <typeinfo>

#include "mesh.h"
#include "iterator.h"
#include "function.h"
#include "manifold.h"
#include "finite-elem.h"
#include "finite-elem-xx.h"

using namespace maniFEM;


void Integrator::conditionally_dispose_core ( )

{	if ( this->core )
	if ( not this->weak )
	{	assert ( this->core->counter > 0 );
		this->core->counter --;
		if ( this->core->counter == 0 ) delete this->core;  }  }


Integrator::~Integrator ( )

{	this->conditionally_dispose_core();  }


Integrator::Integrator ( const Integrator & arg )
// copying an Integrator makes the copy strong
	
:	core { arg .core }, weak { false }

{ this->core->counter ++;  }

	
Integrator::Integrator ( const Integrator && arg )
// cannot move a weak Integrator
	
:	core { arg .core }, weak { false }

{ assert ( not arg .weak );
	this->core->counter ++;   }


Integrator & Integrator::operator= ( const Integrator & arg )
// copying an Integrator makes the copy strong

{	this->conditionally_dispose_core();
	this->core = arg .core;
	this->weak = false;
	this->core->counter ++;
	return * this;          }


Integrator & Integrator::operator= ( const Integrator && arg )
// cannot move a weak Integrator

{	this->conditionally_dispose_core();
	assert ( not arg .weak );
	this->core = arg .core;
	this->weak = false;
	this->core->counter ++;   
	return * this;            }

//------------------------------------------------------------------------------------------------------//


Integrator::Gauss::Gauss ( const tag::gauss_quadrature & q,
                           const tag::FromFiniteElementWithMaster &, FiniteElement & fe )

:	Integrator::Core (), finite_element ( fe )

// J.E. Flaherty, Finite Element Analysis, Lecture Notes : Spring 2000
// https://manifem.rd.ciencias.ulisboa.pt/flaherty-06.pdf
	
// E.B. Becker, G.F. Carey, J.T. Oden, Finite Elements, an introduction, vol 1

{	FiniteElement::WithMaster * fe_core = tag::Util::assert_cast
		< FiniteElement::Core*, FiniteElement::WithMaster* > ( fe .core );
	Manifold master_manifold = fe_core->master_manif;
	switch ( q )
	{	case tag::seg_1 :  // Gauss quadrature with one point on a segment
		// exact for linear functions 
		{	assert ( dynamic_cast < FiniteElement::WithMaster::Segment* > ( fe .core ) );
			#ifndef NDEBUG  // DEBUG mode
			this->info_string = "Gauss quadrature with one point on a segment\n";
			this->info_string += "exact for linear functions\n";
			#endif
			Function t = master_manifold .coordinates();
			assert ( t .number_of ( tag::components ) == 1 );
			Cell Gauss_1 ( tag::vertex );  t ( Gauss_1 ) = 0.;
			this->points .reserve (1);
			this->points .push_back ( Gauss_1 );
			assert ( this->points .size() == 1 );
			this->weights .reserve (1);
			this->weights .push_back ( 2. );
			assert ( this->weights .size() == 1 );
			break;                                                                         }
		case tag::seg_2 :  // Gauss quadrature with two points on a segment
		{	assert ( dynamic_cast < FiniteElement::WithMaster::Segment* > ( fe .core ) );
			#ifndef NDEBUG  // DEBUG mode
			this->info_string = "Gauss quadrature with two points on a segment\n";
			this->info_string += "exact for polynomials of degree 3\n";
			#endif
			Function t = master_manifold .coordinates();
			assert ( t .number_of ( tag::components ) == 1 );
			constexpr double one_over_sqrt_three = 1. / std::sqrt (3.);
			Cell Gauss_2_A ( tag::vertex );  t ( Gauss_2_A ) = - one_over_sqrt_three;
			Cell Gauss_2_B ( tag::vertex );  t ( Gauss_2_B ) =   one_over_sqrt_three;
			this->points .reserve (2);
			this->points .push_back ( Gauss_2_A );
			this->points .push_back ( Gauss_2_B );
			assert ( this->points .size() == 2 );
			this->weights .reserve (2);
			this->weights .push_back ( 1. );
			this->weights .push_back ( 1. );
			assert ( this->weights .size() == 2 );
			break;                                                                         }
		case tag::seg_3 :  // Gauss quadrature with three points on a segment
		{	assert ( dynamic_cast < FiniteElement::WithMaster::Segment* > ( fe .core ) );
			#ifndef NDEBUG  // DEBUG mode
			this->info_string = "Gauss quadrature with three points on a segment\n";
			this->info_string += "exact for polynomials of degree 5\n";
			#endif
			Function t = master_manifold .coordinates();
			assert ( t .number_of ( tag::components ) == 1 );
			const double sqrt3over5 = std::sqrt (0.6);
			Cell Gauss_3_A ( tag::vertex );  t ( Gauss_3_A ) = - sqrt3over5;
			Cell Gauss_3_B ( tag::vertex );  t ( Gauss_3_B ) =   0.;
			Cell Gauss_3_C ( tag::vertex );  t ( Gauss_3_C ) =   sqrt3over5;
			this->points .reserve (3);
			this->points .push_back ( Gauss_3_A );
			this->points .push_back ( Gauss_3_B );
			this->points .push_back ( Gauss_3_C );
			assert ( this->points .size() == 3 );
			this->weights .reserve (3);
			this->weights .push_back ( 5./9. );
			this->weights .push_back ( 8./9. );
			this->weights .push_back ( 5./9. );
			assert ( this->weights .size() == 3 );
			break;                                                                         }
		case tag::seg_4 :  // Gauss quadrature with four points on a segment
		{	assert ( dynamic_cast < FiniteElement::WithMaster::Segment* > ( fe .core ) );
			#ifndef NDEBUG  // DEBUG mode
			this->info_string = "Gauss quadrature with four points on a segment\n";
			this->info_string += "exact for polynomials of degree 7\n";
			#endif
			Function t = master_manifold .coordinates();
			assert ( t .number_of ( tag::components ) == 1 );
			Cell Gauss_4_A ( tag::vertex );  t ( Gauss_4_A ) = - 0.861136311594053;
			Cell Gauss_4_B ( tag::vertex );  t ( Gauss_4_B ) = - 0.339981043584856;
			Cell Gauss_4_C ( tag::vertex );  t ( Gauss_4_C ) =   0.339981043584856;
			Cell Gauss_4_D ( tag::vertex );  t ( Gauss_4_D ) =   0.861136311594053;
			this->points .reserve (4);
			this->points .push_back ( Gauss_4_A );
			this->points .push_back ( Gauss_4_B );
			this->points .push_back ( Gauss_4_C );
			this->points .push_back ( Gauss_4_D );
			assert ( this->points .size() == 4 );
			this->weights .reserve (4);
			this->weights .push_back ( 0.347854845137454 );
			this->weights .push_back ( 0.652145154862546 );
			this->weights .push_back ( 0.652145154862546 );
			this->weights .push_back ( 0.347854845137454 );
			assert ( this->weights .size() == 4 );
			break;                                                                         }
		case tag::seg_5 :  // Gauss quadrature with five points on a segment
		{	assert ( dynamic_cast < FiniteElement::WithMaster::Segment* > ( fe .core ) );
			#ifndef NDEBUG  // DEBUG mode
			this->info_string = "Gauss quadrature with five points on a segment\n";
			this->info_string += "exact for polynomials of degree 9\n";
			#endif
			Function t = master_manifold .coordinates();
			assert ( t .number_of ( tag::components ) == 1 );
			Cell Gauss_5_A ( tag::vertex );  t ( Gauss_5_A ) = - 0.906179845938664;
			Cell Gauss_5_B ( tag::vertex );  t ( Gauss_5_B ) = - 0.538469310105683;
			Cell Gauss_5_C ( tag::vertex );  t ( Gauss_5_C ) =   0.;
			Cell Gauss_5_D ( tag::vertex );  t ( Gauss_5_D ) =   0.538469310105683;
			Cell Gauss_5_E ( tag::vertex );  t ( Gauss_5_E ) =   0.906179845938664;
			this->points .reserve (5);
			this->points .push_back ( Gauss_5_A );
			this->points .push_back ( Gauss_5_B );
			this->points .push_back ( Gauss_5_C );
			this->points .push_back ( Gauss_5_D );
			this->points .push_back ( Gauss_5_E );
			assert ( this->points .size() == 5 );
			this->weights .reserve (5);
			this->weights .push_back ( 0.236926885056189 );
			this->weights .push_back ( 0.478628670499366 );
			this->weights .push_back ( 0.568888888888889 );
			this->weights .push_back ( 0.478628670499366 );
			this->weights .push_back ( 0.236926885056189 );
			assert ( this->weights .size() == 5 );
			break;                                                                         }
		case tag::seg_6 :  // Gauss quadrature with six points on a segment
		{	assert ( dynamic_cast < FiniteElement::WithMaster::Segment* > ( fe .core ) );
			#ifndef NDEBUG  // DEBUG mode
			this->info_string = "Gauss quadrature with six points on a segment\n";
			#endif
			Function t = master_manifold .coordinates();
			assert ( t .number_of ( tag::components ) == 1 );
			Cell Gauss_6_A ( tag::vertex );  t ( Gauss_6_A ) = - 0.932469514203152;
			Cell Gauss_6_B ( tag::vertex );  t ( Gauss_6_B ) = - 0.661209386466265;
			Cell Gauss_6_C ( tag::vertex );  t ( Gauss_6_C ) = - 0.238619186083197;
			Cell Gauss_6_D ( tag::vertex );  t ( Gauss_6_D ) =   0.238619186083197;
			Cell Gauss_6_E ( tag::vertex );  t ( Gauss_6_E ) =   0.661209386466265;
			Cell Gauss_6_F ( tag::vertex );  t ( Gauss_6_F ) =   0.932469514203152;
			this->points .reserve (6);
			this->points .push_back ( Gauss_6_A );
			this->points .push_back ( Gauss_6_B );
			this->points .push_back ( Gauss_6_C );
			this->points .push_back ( Gauss_6_D );
			this->points .push_back ( Gauss_6_E );
			this->points .push_back ( Gauss_6_F );
			assert ( this->points .size() == 6 );
			this->weights .reserve (6);
			this->weights .push_back ( 0.171324492379170 );
			this->weights .push_back ( 0.360761573048139 );
			this->weights .push_back ( 0.467913934572691 );
			this->weights .push_back ( 0.467913934572691 );
			this->weights .push_back ( 0.360761573048139 );
			this->weights .push_back ( 0.171324492379170 );
			assert ( this->weights .size() == 6 );
			break;                                                                         }
		case tag::tri_1 :  // Gauss quadrature with one point on a triangle
		// exact for linear functions
		{	assert ( dynamic_cast < FiniteElement::WithMaster::Triangle* > ( fe .core ) );
			#ifndef NDEBUG  // DEBUG mode
			this->info_string = "Gauss quadrature with one point on a triangle\n";
			this->info_string += "exact for linear functions\n";
			#endif
			Function xi_eta = master_manifold .coordinates();
			assert ( xi_eta .number_of ( tag::components ) == 2 );
			Function xi = xi_eta [0], eta = xi_eta [1];
			// points
			Cell Gauss_1 ( tag::vertex );  xi ( Gauss_1 ) = 1./3.;  eta ( Gauss_1 ) = 1./3.;
			this->points .reserve (1);
			this->points .push_back ( Gauss_1 );
			assert ( this->points .size() == 1 );
			// weights
			this->weights .reserve (1);
			this->weights .push_back ( 0.5 );
			assert ( this->weights .size() == 1 );
			break;                                                                            }
		case tag::tri_3 :  // Gauss quadrature with three points on a triangle
		// exact up to degree 2 according to the book of Flaherty
		{	assert ( dynamic_cast < FiniteElement::WithMaster::Triangle* > ( fe .core ) );
			#ifndef NDEBUG  // DEBUG mode
			this->info_string = "Gauss quadrature with three points on a triangle\n";
			this->info_string += "exact up to degree 2\n";
			#endif
			Function xi_eta = master_manifold .coordinates();
			assert ( xi_eta .number_of ( tag::components ) == 2 );
			Function xi = xi_eta [0], eta = xi_eta [1];
			// points
			Cell Gauss_3_O ( tag::vertex );  xi ( Gauss_3_O ) = 1./6.;  eta ( Gauss_3_O ) = 1./6.;
			Cell Gauss_3_A ( tag::vertex );  xi ( Gauss_3_A ) = 2./3.;  eta ( Gauss_3_A ) = 1./6.;
			Cell Gauss_3_B ( tag::vertex );  xi ( Gauss_3_B ) = 1./6.;  eta ( Gauss_3_B ) = 2./3.;
			this->points .reserve (3);
			this->points .push_back ( Gauss_3_O );
			this->points .push_back ( Gauss_3_A );
			this->points .push_back ( Gauss_3_B );
			assert ( this->points .size() == 3 );
			// weights
			constexpr double Gw_3_O = 1./6., Gw_3_A = 1./6., Gw_3_B = 1./6.;
			// in the book of Oden we have the same weights but a different distribution of points
			// instead of  2/3 1/6 1/6  Oden has  middle of segments 0 0.5 0.5
			// maybe the points are not unique ?  see below
			this->weights .reserve (3);
			this->weights .push_back ( Gw_3_O );
			this->weights .push_back ( Gw_3_A );
			this->weights .push_back ( Gw_3_B );
			assert ( this->weights .size() == 3 );
			break;                                                                                  }
		case tag::tri_3_Oden :
		// Gauss quadrature with three points on a triangle, points presented in the book of Oden
		{	assert ( dynamic_cast < FiniteElement::WithMaster::Triangle* > ( fe .core ) );
			#ifndef NDEBUG  // DEBUG mode
			this->info_string = "Gauss quadrature with three points on a triangle (Oden)\n";
			this->info_string += "exact up to degree 2\n";
			#endif
			Function xi_eta = master_manifold .coordinates();
			assert ( xi_eta .number_of ( tag::components ) == 2 );
			Function xi = xi_eta [0], eta = xi_eta [1];
			// points
			Cell Gauss_3_O ( tag::vertex );  xi ( Gauss_3_O ) = 0.5;  eta ( Gauss_3_O ) = 0.5;
			Cell Gauss_3_A ( tag::vertex );  xi ( Gauss_3_A ) = 0.;   eta ( Gauss_3_A ) = 0.5;
			Cell Gauss_3_B ( tag::vertex );  xi ( Gauss_3_B ) = 0.5;  eta ( Gauss_3_B ) = 0.;
			this->points .reserve (3);
			this->points .push_back ( Gauss_3_O );
			this->points .push_back ( Gauss_3_A );
			this->points .push_back ( Gauss_3_B );
			assert ( this->points .size() == 3 );
			// weights
			constexpr double Gw_3_O = 1./6., Gw_3_A = 1./6., Gw_3_B = 1./6.;
			this->weights .reserve (3);
			this->weights .push_back ( Gw_3_O );
			this->weights .push_back ( Gw_3_A );
			this->weights .push_back ( Gw_3_B );
			assert ( this->weights .size() == 3 );
			break;                                                                              }
		case tag::tri_4 :  // Gauss quadrature with four points on a triangle
		// exact up to degree 3 according to the book of Flaherty
		{	assert ( dynamic_cast < FiniteElement::WithMaster::Triangle* > ( fe .core ) );
			#ifndef NDEBUG  // DEBUG mode
			this->info_string = "Gauss quadrature with four points on a triangle\n";
			this->info_string += "exact up to degree 3\n";
			#endif
			Function xi_eta = master_manifold .coordinates();
			assert ( xi_eta .number_of ( tag::components ) == 2 );
			Function xi = xi_eta [0], eta = xi_eta [1];
			// points
			Cell Gauss_4_c ( tag::vertex );  xi ( Gauss_4_c ) = 1./3.;  eta ( Gauss_4_c ) = 1./3.;
			Cell Gauss_4_O ( tag::vertex );  xi ( Gauss_4_O ) = 0.2;    eta ( Gauss_4_O ) = 0.2;
			Cell Gauss_4_A ( tag::vertex );  xi ( Gauss_4_A ) = 0.6;    eta ( Gauss_4_A ) = 0.2;
			Cell Gauss_4_B ( tag::vertex );  xi ( Gauss_4_B ) = 0.2;    eta ( Gauss_4_B ) = 0.6;
			this->points .reserve (4);
			this->points .push_back ( Gauss_4_c );
			this->points .push_back ( Gauss_4_O );
			this->points .push_back ( Gauss_4_A );
			this->points .push_back ( Gauss_4_B );
			assert ( this->points .size() == 4 );
			// weights
			constexpr double Gw_4_c = -27./96.;  // yes, negative weight
			constexpr double Gw_4_O = 25./96., Gw_4_A = 25./96., Gw_4_B = 25./96.;
			// in the book of Oden we have the same weights but a different distribution of points
			// instead of  0.2 0.2 0.6  we have  2/15 2/15 11/15
			// maybe the points are not unique ?  see below
			this->weights .reserve (4);
			this->weights .push_back ( Gw_4_c );
			this->weights .push_back ( Gw_4_O );
			this->weights .push_back ( Gw_4_A );
			this->weights .push_back ( Gw_4_B );
			assert ( this->weights .size() == 4 );
			break;                                                                                  }
		case tag::tri_4_Oden :
		// Gauss quadrature with four points on a triangle, points presented in the book of Oden
		// exact up to degree 3 according to the book of Oden
		{	assert ( dynamic_cast < FiniteElement::WithMaster::Triangle* > ( fe .core ) );
			#ifndef NDEBUG  // DEBUG mode
			this->info_string = "Gauss quadrature with four points on a triangle (Oden)\n";
			this->info_string += "exact up to degree 3\n";
			#endif
			Function xi_eta = master_manifold .coordinates();
			assert ( xi_eta .number_of ( tag::components ) == 2 );
			Function xi = xi_eta [0], eta = xi_eta [1];
			// points
			Cell Gauss_4_c ( tag::vertex );  xi(Gauss_4_c) =  1./ 3.;  eta(Gauss_4_c) =  1./ 3.;
			Cell Gauss_4_O ( tag::vertex );  xi(Gauss_4_O) =  2./15.;  eta(Gauss_4_O) =  2./15.;
			Cell Gauss_4_A ( tag::vertex );  xi(Gauss_4_A) = 11./15.;  eta(Gauss_4_A) =  2./15.;
			Cell Gauss_4_B ( tag::vertex );  xi(Gauss_4_B) =  2./15.;  eta(Gauss_4_B) = 11./15.;
			this->points .reserve (4);
			this->points .push_back ( Gauss_4_c );
			this->points .push_back ( Gauss_4_O );
			this->points .push_back ( Gauss_4_A );
			this->points .push_back ( Gauss_4_B );
			assert ( this->points .size() == 4 );
			// weights
			constexpr double Gw_4_c = -27./96.;  // yes, negative weight
			constexpr double Gw_4_O = 25./96., Gw_4_A = 25./96., Gw_4_B = 25./96.;
			this->weights .reserve (4);
			this->weights .push_back ( Gw_4_c );
			this->weights .push_back ( Gw_4_O );
			this->weights .push_back ( Gw_4_A );
			this->weights .push_back ( Gw_4_B );
			assert ( this->weights .size() == 4 );
			break;                                                                                }
		case tag::tri_6 :  // Gauss quadrature with six points on a triangle
		// exact up to degree 4 according to the book of Flaherty
		{	assert ( dynamic_cast < FiniteElement::WithMaster::Triangle* > ( fe .core ) );
			#ifndef NDEBUG  // DEBUG mode
			this->info_string = "Gauss quadrature with six points on a triangle\n";
			this->info_string += "exact up to degree 4\n";
			#endif
			Function xi_eta = master_manifold .coordinates();
			assert ( xi_eta .number_of ( tag::components ) == 2 );
			Function xi = xi_eta [0], eta = xi_eta [1];
			// points
			Cell Gauss_6_O  ( tag::vertex );  xi  ( Gauss_6_O  ) = 0.091576213509771;
			                                  eta ( Gauss_6_O  ) = 0.091576213509771;
			Cell Gauss_6_A  ( tag::vertex );  xi  ( Gauss_6_A  ) = 0.816847572980459;
			                                  eta ( Gauss_6_A  ) = 0.091576213509771;
			Cell Gauss_6_B  ( tag::vertex );  xi  ( Gauss_6_B  ) = 0.091576213509771;
			                                  eta ( Gauss_6_B  ) = 0.816847572980459;
			Cell Gauss_6_OA ( tag::vertex );  xi  ( Gauss_6_OA ) = 0.445948490915965;
			                                  eta ( Gauss_6_OA ) = 0.108103018168070;
			Cell Gauss_6_OB ( tag::vertex );  xi  ( Gauss_6_OB ) = 0.108103018168070;
			                                  eta ( Gauss_6_OB ) = 0.445948490915965;
			Cell Gauss_6_AB ( tag::vertex );  xi  ( Gauss_6_AB ) = 0.445948490915965;
			                                  eta ( Gauss_6_AB ) = 0.445948490915965;
			this->points .reserve (6);
			this->points .push_back ( Gauss_6_O  );
			this->points .push_back ( Gauss_6_A  );
			this->points .push_back ( Gauss_6_B  );
			this->points .push_back ( Gauss_6_OA );
			this->points .push_back ( Gauss_6_OB );
			this->points .push_back ( Gauss_6_AB );
			assert ( this->points .size() == 6 );
			// weights
			constexpr double Gw_6_O = 0.054975871827661, Gw_6_A = 0.054975871827661,
			      Gw_6_B = 0.054975871827661;
			constexpr double Gw_6_OA = 0.1116907948390055, Gw_6_OB = 0.1116907948390055,
			       Gw_6_AB = 0.1116907948390055;
			this->weights .reserve (6);
			this->weights .push_back ( Gw_6_O  );
			this->weights .push_back ( Gw_6_A  );
			this->weights .push_back ( Gw_6_B  );
			this->weights .push_back ( Gw_6_OA );
			this->weights .push_back ( Gw_6_OB );
			this->weights .push_back ( Gw_6_AB );
			assert ( this->weights .size() == 6 );
			break;                                                                          }
		case tag::tri_7 :  // Gauss quadrature with seven points on a triangle
		// exact up to degree 5 according to the book of Flaherty
		{	assert ( dynamic_cast < FiniteElement::WithMaster::Triangle* > ( fe .core ) );
			#ifndef NDEBUG  // DEBUG mode
			this->info_string = "Gauss quadrature with seven points on a triangle\n";
			this->info_string += "exact up to degree 5\n";
			#endif
			Function xi_eta = master_manifold .coordinates();
			assert ( xi_eta .number_of ( tag::components ) == 2 );
			Function xi = xi_eta [0], eta = xi_eta [1];
			// points
			// 0.797426985353087 + 2*0.101286507323456 == 1
			// 2*0.470142064105115 + 0.059715871789770 == 1
			Cell Gauss_7_cen ( tag::vertex );  xi  ( Gauss_7_cen ) = 1./3.;
			                                   eta ( Gauss_7_cen ) = 1./3.;
			Cell Gauss_7_O   ( tag::vertex );  xi  ( Gauss_7_O   ) = 0.101286507323456;
			                                   eta ( Gauss_7_O   ) = 0.101286507323456;
			Cell Gauss_7_A   ( tag::vertex );  xi  ( Gauss_7_A   ) = 0.797426985353087;
			                                   eta ( Gauss_7_A   ) = 0.101286507323456;
			Cell Gauss_7_B   ( tag::vertex );  xi  ( Gauss_7_B   ) = 0.101286507323456;
			                                   eta ( Gauss_7_B   ) = 0.797426985353087;
			Cell Gauss_7_OA  ( tag::vertex );  xi  ( Gauss_7_OA  ) = 0.470142064105115;
			                                   eta ( Gauss_7_OA  ) = 0.059715871789770;
			Cell Gauss_7_OB  ( tag::vertex );  xi  ( Gauss_7_OB  ) = 0.059715871789770;
			                                   eta ( Gauss_7_OB  ) = 0.470142064105115;
			Cell Gauss_7_AB  ( tag::vertex );  xi  ( Gauss_7_AB  ) = 0.470142064105115;
			                                   eta ( Gauss_7_AB  ) = 0.470142064105115;
			this->points .reserve (7);
			this->points .push_back ( Gauss_7_cen );
			this->points .push_back ( Gauss_7_O   );
			this->points .push_back ( Gauss_7_A   );
			this->points .push_back ( Gauss_7_B   );
			this->points .push_back ( Gauss_7_OA  );
			this->points .push_back ( Gauss_7_OB  );
			this->points .push_back ( Gauss_7_AB  );
			assert ( this->points  .size() == 7 );
			// weights
			// 0.1125 + 3*0.062969590272413 + 3*0.066197076394253 == 0.5
			constexpr double Gw_7_cen = 0.1125;
			constexpr double Gw_7_O = 0.062969590272413, Gw_7_A = 0.062969590272413,
			      Gw_7_B = 0.062969590272413;
			constexpr double Gw_7_OA = 0.066197076394253, Gw_7_OB = 0.066197076394253,
			      Gw_7_AB = 0.066197076394253;
			this->weights .reserve (7);
			this->weights .push_back ( Gw_7_cen );
			this->weights .push_back ( Gw_7_O   );
			this->weights .push_back ( Gw_7_A   );
			this->weights .push_back ( Gw_7_B   );
			this->weights .push_back ( Gw_7_OA  );
			this->weights .push_back ( Gw_7_OB  );
			this->weights .push_back ( Gw_7_AB  );
			assert ( this->weights .size() == 7 );
			break;                                                                      }
		case tag::tri_12 :  // Gauss quadrature with twelve points on a triangle
		// exact up to degree 6 according to the book of Flaherty
		{	assert ( dynamic_cast < FiniteElement::WithMaster::Triangle* > ( fe .core ) );
			#ifndef NDEBUG  // DEBUG mode
			this->info_string = "Gauss quadrature with twelve points on a triangle\n";
			this->info_string += "exact up to degree 6\n";
			#endif
			Function xi_eta = master_manifold .coordinates();
			assert ( xi_eta .number_of ( tag::components ) == 2 );
			Function xi = xi_eta [0], eta = xi_eta [1];
			// points
			// 0.873821971016996 + 2*0.063089014491502 == 1
			// 0.501426509658179 + 2*0.249286745170910 == 1
			// 0.636502499121399 + 0.310352451033785 + 0.053145049844016 == 1
			Cell Gauss_12_OO  ( tag::vertex );  xi  ( Gauss_12_OO  ) = 0.063089014491502;
			                                    eta ( Gauss_12_OO  ) = 0.063089014491502;
			Cell Gauss_12_AA  ( tag::vertex );  xi  ( Gauss_12_AA  ) = 0.873821971016996;
			                                    eta ( Gauss_12_AA  ) = 0.063089014491502;
			Cell Gauss_12_BB  ( tag::vertex );  xi  ( Gauss_12_BB  ) = 0.063089014491502;
			                                    eta ( Gauss_12_BB  ) = 0.873821971016996;
			Cell Gauss_12_O   ( tag::vertex );  xi  ( Gauss_12_O   ) = 0.249286745170910;
			                                    eta ( Gauss_12_O   ) = 0.249286745170910;
			Cell Gauss_12_A   ( tag::vertex );  xi  ( Gauss_12_A   ) = 0.501426509658179;
			                                    eta ( Gauss_12_A   ) = 0.249286745170910;
			Cell Gauss_12_B   ( tag::vertex );  xi  ( Gauss_12_B   ) = 0.249286745170910;
			                                    eta ( Gauss_12_B   ) = 0.501426509658179;
			Cell Gauss_12_OAB ( tag::vertex );  xi  ( Gauss_12_OAB ) = 0.310352451033785;
			                                    eta ( Gauss_12_OAB ) = 0.053145049844016;
			Cell Gauss_12_OBA ( tag::vertex );  xi  ( Gauss_12_OBA ) = 0.053145049844016;
			                                    eta ( Gauss_12_OBA ) = 0.310352451033785;
			Cell Gauss_12_ABO ( tag::vertex );  xi  ( Gauss_12_ABO ) = 0.636502499121399;
			                                    eta ( Gauss_12_ABO ) = 0.310352451033785;
			Cell Gauss_12_AOB ( tag::vertex );  xi  ( Gauss_12_AOB ) = 0.636502499121399;
			                                    eta ( Gauss_12_AOB ) = 0.053145049844016;
			Cell Gauss_12_BOA ( tag::vertex );  xi  ( Gauss_12_BOA ) = 0.053145049844016;
			                                    eta ( Gauss_12_BOA ) = 0.636502499121399;
			Cell Gauss_12_BAO ( tag::vertex );  xi  ( Gauss_12_BAO ) = 0.310352451033785;
			                                    eta ( Gauss_12_BAO ) = 0.636502499121399;
			this->points .reserve (12);
			this->points .push_back ( Gauss_12_OO  );
			this->points .push_back ( Gauss_12_AA  );
			this->points .push_back ( Gauss_12_BB  );
			this->points .push_back ( Gauss_12_O   );
			this->points .push_back ( Gauss_12_A   );
			this->points .push_back ( Gauss_12_B   );
			this->points .push_back ( Gauss_12_OAB );
			this->points .push_back ( Gauss_12_OBA );
			this->points .push_back ( Gauss_12_ABO );
			this->points .push_back ( Gauss_12_AOB );
			this->points .push_back ( Gauss_12_BOA );
			this->points .push_back ( Gauss_12_BAO );
			assert ( this->points .size() == 12 );
			// weights
			// 3*0.050844906370207 + 3*0.116786275726379 + 6*0.082851075618374 == 1
			// 3*0.025422453185103 + 3*0.058393137863189 + 6*0.041425537809187 == 0.5
			constexpr double Gw_12_OO = 0.025422453185103, Gw_12_AA = 0.025422453185103,
			      Gw_12_BB = 0.025422453185103;
			constexpr double Gw_12_O = 0.058393137863189, Gw_12_A = 0.058393137863189,
			      Gw_12_B = 0.058393137863189;
			constexpr double Gw_12_OAB = 0.041425537809187, Gw_12_OBA = 0.041425537809187,
			      Gw_12_ABO = 0.041425537809187, Gw_12_AOB = 0.041425537809187,
			      Gw_12_BOA = 0.041425537809187, Gw_12_BAO = 0.041425537809187;
			this->weights .reserve (12);
			this->weights .push_back ( Gw_12_OO  );
			this->weights .push_back ( Gw_12_AA  );
			this->weights .push_back ( Gw_12_BB  );
			this->weights .push_back ( Gw_12_O   );
			this->weights .push_back ( Gw_12_A   );
			this->weights .push_back ( Gw_12_B   );
			this->weights .push_back ( Gw_12_OAB );
			this->weights .push_back ( Gw_12_OBA );
			this->weights .push_back ( Gw_12_ABO );
			this->weights .push_back ( Gw_12_AOB );
			this->weights .push_back ( Gw_12_BOA );
			this->weights .push_back ( Gw_12_BAO );
			assert ( this->weights .size() == 12 );
			break;                                                                          }
		case tag::tri_13 :  // Gauss quadrature with thirteen points on a triangle
		// exact up to degree 7 according to the book of Flaherty
		// to be tested
		{	assert ( dynamic_cast < FiniteElement::WithMaster::Triangle* > ( fe .core ) );
			#ifndef NDEBUG  // DEBUG mode
			this->info_string = "Gauss quadrature with thirteen points on a triangle\n";
			this->info_string += "exact up to degree 7\n";
			#endif
			Function xi_eta = master_manifold .coordinates();
			assert ( xi_eta .number_of ( tag::components ) == 2 );
			Function xi = xi_eta [0], eta = xi_eta [1];
			// points
			// 1/3 + 1/3 + 1/3 == 1
			// 0.479308067841923 + 2*0.260345966079038 == 1
			// 0.869739794195568 + 2*0.065130102902216 == 1
			// 0.638444188569809 + 0.312865496004875 + 0.048690315425316 == 1
			Cell Gauss_13_cen ( tag::vertex );  xi  ( Gauss_13_cen ) = 1./3.;
			                                    eta ( Gauss_13_cen ) = 1./3.;
			Cell Gauss_13_O   ( tag::vertex );  xi  ( Gauss_13_O   ) = 0.260345966079038;
			                                    eta ( Gauss_13_O   ) = 0.260345966079038;
			Cell Gauss_13_A   ( tag::vertex );  xi  ( Gauss_13_A   ) = 0.479308067841923;
			                                    eta ( Gauss_13_A   ) = 0.260345966079038;
			Cell Gauss_13_B   ( tag::vertex );  xi  ( Gauss_13_B   ) = 0.260345966079038;
			                                    eta ( Gauss_13_B   ) = 0.479308067841923;
			Cell Gauss_13_OO  ( tag::vertex );  xi  ( Gauss_13_OO  ) = 0.065130102902216;
			                                    eta ( Gauss_13_OO  ) = 0.065130102902216;
			Cell Gauss_13_AA  ( tag::vertex );  xi  ( Gauss_13_AA  ) = 0.869739794195568;
			                                    eta ( Gauss_13_AA  ) = 0.065130102902216;
			Cell Gauss_13_BB  ( tag::vertex );  xi  ( Gauss_13_BB  ) = 0.065130102902216;
			                                    eta ( Gauss_13_BB  ) = 0.869739794195568;
			Cell Gauss_13_OAB ( tag::vertex );  xi  ( Gauss_13_OAB ) = 0.312865496004875;
			                                    eta ( Gauss_13_OAB ) = 0.048690315425316;
			Cell Gauss_13_OBA ( tag::vertex );  xi  ( Gauss_13_OBA ) = 0.048690315425316;
			                                    eta ( Gauss_13_OBA ) = 0.312865496004875;
			Cell Gauss_13_ABO ( tag::vertex );  xi  ( Gauss_13_ABO ) = 0.638444188569809;
			                                    eta ( Gauss_13_ABO ) = 0.312865496004875;
			Cell Gauss_13_AOB ( tag::vertex );  xi  ( Gauss_13_AOB ) = 0.638444188569809;
			                                    eta ( Gauss_13_AOB ) = 0.048690315425316;
			Cell Gauss_13_BOA ( tag::vertex );  xi  ( Gauss_13_BOA ) = 0.048690315425316;
			                                    eta ( Gauss_13_BOA ) = 0.638444188569809;
			Cell Gauss_13_BAO ( tag::vertex );  xi  ( Gauss_13_BAO ) = 0.312865496004875;
			                                    eta ( Gauss_13_BAO ) = 0.638444188569809;
			this->points .reserve (13);
			this->points .push_back ( Gauss_13_cen );
			this->points .push_back ( Gauss_13_O   );
			this->points .push_back ( Gauss_13_A   );
			this->points .push_back ( Gauss_13_B   );
			this->points .push_back ( Gauss_13_OO  );
			this->points .push_back ( Gauss_13_AA  );
			this->points .push_back ( Gauss_13_BB  );
			this->points .push_back ( Gauss_13_OAB );
			this->points .push_back ( Gauss_13_OBA );
			this->points .push_back ( Gauss_13_ABO );
			this->points .push_back ( Gauss_13_AOB );
			this->points .push_back ( Gauss_13_BOA );
			this->points .push_back ( Gauss_13_BAO );
			assert ( this->points .size() == 13 );
			// weights
			// -0.149570044467670 + 3*0.175615257433204 + 3*0.053347235608839 + 6*0.077113760890257 == 1
			// -0.074785022233835 + 3*0.087807628716602 + 3*0.026673617804419 + 6*0.038556880445128 == 0.5
			constexpr double Gw_13_cen = -0.074785022233835;  // yes, negative weight
			constexpr double Gw_13_O = 0.087807628716602, Gw_13_A = 0.087807628716602,
			      Gw_13_B = 0.087807628716602;
			constexpr double Gw_13_OO = 0.026673617804419, Gw_13_AA = 0.026673617804419,
			      Gw_13_BB = 0.026673617804419;
			constexpr double Gw_13_OAB = 0.038556880445128, Gw_13_OBA = 0.038556880445128,
			      Gw_13_ABO = 0.038556880445128, Gw_13_AOB = 0.038556880445128,
			      Gw_13_BOA = 0.038556880445128, Gw_13_BAO = 0.038556880445128;
			this->weights .reserve (13);
			this->weights .push_back ( Gw_13_cen );
			this->weights .push_back ( Gw_13_O   );
			this->weights .push_back ( Gw_13_A   );
			this->weights .push_back ( Gw_13_B   );
			this->weights .push_back ( Gw_13_OO  );
			this->weights .push_back ( Gw_13_AA  );
			this->weights .push_back ( Gw_13_BB  );
			this->weights .push_back ( Gw_13_OAB );
			this->weights .push_back ( Gw_13_OBA );
			this->weights .push_back ( Gw_13_ABO );
			this->weights .push_back ( Gw_13_AOB );
			this->weights .push_back ( Gw_13_BOA );
			this->weights .push_back ( Gw_13_BAO );
			assert ( this->weights .size() == 13 );
			break;                                                                          }
		case tag::quad_1 :  // Gauss quadrature with one point on a square
		// exact for linear functions
		{	assert ( dynamic_cast < FiniteElement::WithMaster::Quadrangle* > ( fe .core ) );
			#ifndef NDEBUG  // DEBUG mode
			this->info_string = "Gauss quadrature with one point on a quadrangle\n";
			this->info_string += "exact for linear functions\n";
			#endif
			Function xi_eta = master_manifold .coordinates();
			assert ( xi_eta .number_of ( tag::components ) == 2 );
			Function xi = xi_eta [0], eta = xi_eta [1];
			Cell Gauss_1 ( tag::vertex );  xi ( Gauss_1 ) = 0.;  eta ( Gauss_1 ) = 0.;
			this->points .reserve (1);
			this->points  .push_back ( Gauss_1 );
			assert ( this->points  .size() == 1 );
			this->weights .reserve (1);
			this->weights .push_back ( 4. );
			assert ( this->weights .size() == 1 );
			break;                                                                            }
		case tag::quad_4 :  // Gauss quadrature with four points on a quadrangle
		// exact for polynomials of degree 3 in each variable
		{	assert ( dynamic_cast < FiniteElement::WithMaster::Quadrangle* > ( fe .core ) );
			#ifndef NDEBUG  // DEBUG mode
			this->info_string = "Gauss quadrature with four points on a quadrangle\n";
			this->info_string += "exact for polynomials of degree 3 in each variable\n";
			#endif
			Function xi_eta = master_manifold .coordinates();
			assert ( xi_eta .number_of ( tag::components ) == 2 );
			Function xi = xi_eta [0], eta = xi_eta [1];
			constexpr double sqrt_of_one_third = std::sqrt (1./3.);
			Cell Gauss_4_a ( tag::vertex );   xi  ( Gauss_4_a ) = -sqrt_of_one_third;
			                                  eta ( Gauss_4_a ) = -sqrt_of_one_third;
			Cell Gauss_4_b ( tag::vertex );   xi  ( Gauss_4_b ) = -sqrt_of_one_third;
			                                  eta ( Gauss_4_b ) =  sqrt_of_one_third;
			Cell Gauss_4_c ( tag::vertex );   xi  ( Gauss_4_c ) =  sqrt_of_one_third;
			                                  eta ( Gauss_4_c ) = -sqrt_of_one_third;
			Cell Gauss_4_d ( tag::vertex );   xi  ( Gauss_4_d ) =  sqrt_of_one_third;
			                                  eta ( Gauss_4_d ) =  sqrt_of_one_third;
			this->points .reserve (4);
			this->points  .push_back ( Gauss_4_a );
			this->points  .push_back ( Gauss_4_b );
			this->points  .push_back ( Gauss_4_c );
			this->points  .push_back ( Gauss_4_d );
			assert ( this->points  .size() == 4 );
			this->weights .reserve (4);
			this->weights .push_back ( 1. );
			this->weights .push_back ( 1. );
			this->weights .push_back ( 1. );
			this->weights .push_back ( 1. );
			assert ( this->weights .size() == 4 );
			break;                                                                            }
		case tag::quad_9 :  // Gauss quadrature with nine points on a square
		// exact for polynomials of degree 5 in each variable
		{	assert ( dynamic_cast < FiniteElement::WithMaster::Quadrangle* > ( fe .core ) );
			#ifndef NDEBUG  // DEBUG mode
			this->info_string = "Gauss quadrature with nine points on a quadrangle\n";
			this->info_string += "exact for polynomials of degree 5 in each variable\n";
			#endif
			Function xi_eta = master_manifold .coordinates();
			assert ( xi_eta .number_of ( tag::components ) == 2 );
			Function xi = xi_eta [0], eta = xi_eta [1];
			// points :
			constexpr double sqrt3over5 = std::sqrt (0.6);
			Cell Gauss_9_a ( tag::vertex );  xi  ( Gauss_9_a ) = -sqrt3over5;
			                                 eta ( Gauss_9_a ) = -sqrt3over5;
			Cell Gauss_9_b ( tag::vertex );  xi  ( Gauss_9_b ) =  0.;
			                                 eta ( Gauss_9_b ) = -sqrt3over5;
			Cell Gauss_9_c ( tag::vertex );  xi  ( Gauss_9_c ) =  sqrt3over5;
			                                 eta ( Gauss_9_c ) = -sqrt3over5;
			Cell Gauss_9_d ( tag::vertex );  xi  ( Gauss_9_d ) = -sqrt3over5;
			                                 eta ( Gauss_9_d ) =  0.;
			Cell Gauss_9_e ( tag::vertex );  xi  ( Gauss_9_e ) =  0.;
			                                 eta ( Gauss_9_e ) =  0.;
			Cell Gauss_9_f ( tag::vertex );  xi  ( Gauss_9_f ) =  sqrt3over5;
			                                 eta ( Gauss_9_f ) =  0.;
			Cell Gauss_9_g ( tag::vertex );  xi  ( Gauss_9_g ) = -sqrt3over5;
			                                 eta ( Gauss_9_g ) =  sqrt3over5;
			Cell Gauss_9_h ( tag::vertex );  xi  ( Gauss_9_h ) =  0.;
			                                 eta ( Gauss_9_h ) =  sqrt3over5;
			Cell Gauss_9_i ( tag::vertex );  xi  ( Gauss_9_i ) =  sqrt3over5;
			                                 eta ( Gauss_9_i ) =  sqrt3over5;
			//  a {-sqrt3over5,-sqrt3over5}, b {0, -sqrt3over5}, c {sqrt3over5, -sqrt3over5},
			//  d {-sqrt3over5,0}, e {0,0}, f {sqrt3over5, 0}, g {-sqrt3over5,sqrt3over5},
			//  h {0,sqrt3over5}, i {sqrt3over5, sqrt3over5}
			this->points .reserve (9);
			this->points .push_back ( Gauss_9_a );
			this->points .push_back ( Gauss_9_b );
			this->points .push_back ( Gauss_9_c );
			this->points .push_back ( Gauss_9_d );
			this->points .push_back ( Gauss_9_e );
			this->points .push_back ( Gauss_9_f );	
			this->points .push_back ( Gauss_9_g );
			this->points .push_back ( Gauss_9_h );
			this->points .push_back ( Gauss_9_i );
			assert ( this->points .size() == 9 );
			// weights :
			//  25./81., 40./81., 25./81., 40./81, 64./81., 40./81, 25./81., 40./81, 25./81.
			this->weights .reserve( 9);
			this->weights .push_back ( 25./81.);
			this->weights .push_back ( 40./81.);
			this->weights .push_back ( 25./81.);
			this->weights .push_back ( 40./81.);
			this->weights .push_back ( 64./81.);
			this->weights .push_back ( 40./81.);
			this->weights .push_back ( 25./81.);
			this->weights .push_back ( 40./81.);
			this->weights .push_back ( 25./81.);
			assert ( this->weights .size() == 9 );
			break;                                                                            }
		case tag::tetra_1 :  // Gauss quadrature with one point on a tetrahedron
		// exact for linear functions
		{	assert ( dynamic_cast < FiniteElement::WithMaster::Tetrahedron* > ( fe .core ) );
			#ifndef NDEBUG  // DEBUG mode
			this->info_string = "Gauss quadrature with one point on a tetrahedron\n";
			this->info_string += "exact for linear functions\n";
			#endif
			Function xi_eta_zeta = master_manifold .coordinates();
			assert ( xi_eta_zeta .number_of ( tag::components ) == 3 );
			Function xi = xi_eta_zeta [0], eta = xi_eta_zeta [1], zeta = xi_eta_zeta [2];
			// points
			Cell Gauss_1 ( tag::vertex );  xi ( Gauss_1 ) = 0.25;  
				eta ( Gauss_1 ) = 0.25; zeta ( Gauss_1 ) = 0.25;
			this->points .reserve (1);
			this->points .push_back ( Gauss_1 );
			assert ( this->points .size() == 1 );
			// weights
			this->weights .reserve (1);
			this->weights .push_back ( 1./6. );
			assert ( this->weights .size() == 1 );
			break;                                                                             }
		case tag::tetra_4 :  // Gauss quadrature with four points on a tetrahedron
		// exact up to degree 2 according to the book of Flaherty
		{	assert ( dynamic_cast < FiniteElement::WithMaster::Tetrahedron* > ( fe .core ) );
			#ifndef NDEBUG  // DEBUG mode
			this->info_string = "Gauss quadrature with four points on a tetrahedron\n";
			this->info_string += "exact up to degree 2\n";
			#endif
			Function xi_eta_zeta = master_manifold .coordinates();
			assert ( xi_eta_zeta .number_of ( tag::components ) == 3 );
			Function xi = xi_eta_zeta [0], eta = xi_eta_zeta [1], zeta = xi_eta_zeta [2];
			// points
			Cell Gauss_4_O ( tag::vertex );  xi ( Gauss_4_O ) = 0.138196601125011;  
				eta ( Gauss_4_O ) = 0.138196601125011; zeta ( Gauss_4_O ) = 0.138196601125011;
			Cell Gauss_4_A ( tag::vertex );  xi ( Gauss_4_A ) = 0.585410196624969;   
				eta ( Gauss_4_A ) = 0.138196601125011;  zeta ( Gauss_4_A ) = 0.138196601125011;
			Cell Gauss_4_B ( tag::vertex );  xi ( Gauss_4_B ) = 0.138196601125011;  
				eta ( Gauss_4_B ) = 0.585410196624969; zeta ( Gauss_4_B ) = 0.138196601125011;
			Cell Gauss_4_C ( tag::vertex );  xi ( Gauss_4_C ) = 0.138196601125011;  
				eta ( Gauss_4_C ) = 0.138196601125011;  zeta ( Gauss_4_C ) = 0.585410196624969;
			this->points .reserve (4);
			this->points .push_back ( Gauss_4_O );
			this->points .push_back ( Gauss_4_A );
			this->points .push_back ( Gauss_4_B );
			this->points .push_back ( Gauss_4_C );
			assert ( this->points .size() == 4 );
			// weights
			constexpr double Gw_4_O = 1./24., Gw_4_A = 1./24., Gw_4_B = 1./24., Gw_4_C = 1./24.;
			this->weights .reserve (4);
			this->weights .push_back ( Gw_4_O );
			this->weights .push_back ( Gw_4_A );
			this->weights .push_back ( Gw_4_B );
			this->weights .push_back ( Gw_4_C );
			assert ( this->weights .size() == 4 );
			break;                                                                                }
		case tag::tetra_5 :  // Gauss quadrature with five points on a tetrahedron
		// exact up to degree 3 according to the book of Flaherty
		{	assert ( dynamic_cast < FiniteElement::WithMaster::Tetrahedron* > ( fe .core ) );
			#ifndef NDEBUG  // DEBUG mode
			this->info_string = "Gauss quadrature with five points on a tetrahedron\n";
			this->info_string += "exact up to degree 3\n";
			#endif
			Function xi_eta_zeta = master_manifold .coordinates();
			assert ( xi_eta_zeta .number_of ( tag::components ) == 3 );
			Function xi = xi_eta_zeta [0], eta = xi_eta_zeta [1], zeta = xi_eta_zeta [2];
			// points
			Cell Gauss_5_cen ( tag::vertex );  xi ( Gauss_5_cen ) = 0.25;  
				eta ( Gauss_5_cen ) = 0.25; zeta ( Gauss_5_cen ) = 0.25;
			Cell Gauss_5_O ( tag::vertex );  xi ( Gauss_5_O ) = 1./6.;  
				eta ( Gauss_5_O ) = 1./6.; zeta ( Gauss_5_O ) = 1./6.;
			Cell Gauss_5_A ( tag::vertex );  xi ( Gauss_5_A ) = 0.5;   
				eta ( Gauss_5_A ) = 1./6.;  zeta ( Gauss_5_A ) = 1./6.;
			Cell Gauss_5_B ( tag::vertex );  xi ( Gauss_5_B ) = 1./6.;  
				eta ( Gauss_5_B ) = 0.5; zeta ( Gauss_5_B ) = 1./6.;
			Cell Gauss_5_C ( tag::vertex );  xi ( Gauss_5_C ) = 1./6.;  
				eta ( Gauss_5_C ) = 1./6.;  zeta ( Gauss_5_C ) = 0.5;
			this->points .reserve (5);
			this->points .push_back ( Gauss_5_cen );
			this->points .push_back ( Gauss_5_O   );
			this->points .push_back ( Gauss_5_A   );
			this->points .push_back ( Gauss_5_B   );
			this->points .push_back ( Gauss_5_C   );
			assert ( this->points .size() == 5 );
			// weights
			constexpr double Gw_5_cen = - 2./15.;  // yes, negative
			constexpr double Gw_5_O = 3./40., Gw_5_A = 3./40., Gw_5_B = 3./40., Gw_5_C = 3./40.;
			this->weights .reserve (5);
			this->weights .push_back ( Gw_5_cen );
			this->weights .push_back ( Gw_5_O   );
			this->weights .push_back ( Gw_5_A   );
			this->weights .push_back ( Gw_5_B   );
			this->weights .push_back ( Gw_5_C   );
			assert ( this->weights .size() == 5 );
			break;                                                                                }
		case tag::tetra_11 :  // Gauss quadrature with eleven points on a tetrahedron
		// exact up to degree 4 according to the book of Flaherty
		{	assert ( dynamic_cast < FiniteElement::WithMaster::Tetrahedron* > ( fe .core ) );
			#ifndef NDEBUG  // DEBUG mode
			this->info_string = "Gauss quadrature with eleven points on a tetrahedron\n";
			this->info_string += "exact up to degree 4\n";
			#endif
			Function xi_eta_zeta = master_manifold .coordinates();
			assert ( xi_eta_zeta .number_of ( tag::components ) == 3 );
			Function xi = xi_eta_zeta [0], eta = xi_eta_zeta [1], zeta = xi_eta_zeta [2];
			// points
			Cell Gauss_11_cen ( tag::vertex );  xi ( Gauss_11_cen ) = 0.25;  
				eta ( Gauss_11_cen ) = 0.25; zeta ( Gauss_11_cen ) = 0.25;
			Cell Gauss_11_O ( tag::vertex );  xi ( Gauss_11_O ) = 0.07142857142857;  
				eta ( Gauss_11_O ) = 0.07142857142857; zeta ( Gauss_11_O ) = 0.07142857142857;
			Cell Gauss_11_A ( tag::vertex );  xi ( Gauss_11_A ) = 0.785714285714286;   
				eta ( Gauss_11_A ) = 0.07142857142857;  zeta ( Gauss_11_A ) = 0.07142857142857;
			Cell Gauss_11_B ( tag::vertex );  xi ( Gauss_11_B ) = 0.07142857142857;  
				eta ( Gauss_11_B ) = 0.785714285714286; zeta ( Gauss_11_B ) = 0.07142857142857;
			Cell Gauss_11_C ( tag::vertex );  xi ( Gauss_11_C ) = 0.07142857142857;  
				eta ( Gauss_11_C ) = 0.07142857142857;  zeta ( Gauss_11_C ) = 0.785714285714286;
			Cell Gauss_11_OA ( tag::vertex );  xi ( Gauss_11_OA ) = 0.399403576166799;  
				eta ( Gauss_11_OA ) = 0.100596423833201; zeta ( Gauss_11_OA ) = 0.100596423833201;
			Cell Gauss_11_OB ( tag::vertex );  xi ( Gauss_11_OB ) = 0.100596423833201;   
				eta ( Gauss_11_OB ) = 0.399403576166799;  zeta ( Gauss_11_OB ) = 0.100596423833201;
			Cell Gauss_11_OC ( tag::vertex );  xi ( Gauss_11_OC ) = 0.100596423833201;  
				eta ( Gauss_11_OC ) = 0.100596423833201; zeta ( Gauss_11_OC ) = 0.399403576166799;
			Cell Gauss_11_AB ( tag::vertex );  xi ( Gauss_11_AB ) = 0.399403576166799;  
				eta ( Gauss_11_AB ) = 0.399403576166799;  zeta ( Gauss_11_AB ) = 0.100596423833201;
			Cell Gauss_11_BC ( tag::vertex );  xi ( Gauss_11_BC ) = 0.100596423833201;  
				eta ( Gauss_11_BC ) = 0.399403576166799; zeta ( Gauss_11_BC ) = 0.399403576166799;
			Cell Gauss_11_AC ( tag::vertex );  xi ( Gauss_11_AC ) = 0.399403576166799;  
				eta ( Gauss_11_AC ) = 0.100596423833201;  zeta ( Gauss_11_AC ) = 0.399403576166799;
			this->points .reserve (11);
			this->points .push_back ( Gauss_11_cen );
			this->points .push_back ( Gauss_11_O   );
			this->points .push_back ( Gauss_11_A   );
			this->points .push_back ( Gauss_11_B   );
			this->points .push_back ( Gauss_11_C   );
			this->points .push_back ( Gauss_11_OA  );
			this->points .push_back ( Gauss_11_OB  );
			this->points .push_back ( Gauss_11_OC  );
			this->points .push_back ( Gauss_11_AB  );
			this->points .push_back ( Gauss_11_BC  );
			this->points .push_back ( Gauss_11_AC  );
			assert ( this->points .size() == 11 );
			// weights
			constexpr double Gw_11_cen = - 0.01315555555556;  // yes, negative weight
			constexpr double Gw_11_O = 0.007622222222222, Gw_11_A = 0.007622222222222,
			             Gw_11_B = 0.007622222222222, Gw_11_C = 0.007622222222222;
			constexpr double Gw_11_OA = 0.024888888888889, Gw_11_OB = 0.024888888888889,
			             Gw_11_OC = 0.024888888888889, Gw_11_AB = 0.024888888888889,
			             Gw_11_BC = 0.024888888888889, Gw_11_AC = 0.024888888888889;
			this->weights .reserve (11);
			this->weights .push_back ( Gw_11_cen );
			this->weights .push_back ( Gw_11_O   );
			this->weights .push_back ( Gw_11_A   );
			this->weights .push_back ( Gw_11_B   );
			this->weights .push_back ( Gw_11_C   );
			this->weights .push_back ( Gw_11_OA  );
			this->weights .push_back ( Gw_11_OB  );
			this->weights .push_back ( Gw_11_OC  );
			this->weights .push_back ( Gw_11_AB  );
			this->weights .push_back ( Gw_11_BC  );
			this->weights .push_back ( Gw_11_AC  );
			assert ( this->weights .size() == 11 );
			break;                                                                                  }
		case tag::tetra_15 :  // Gauss quadrature with fifteen points on a tetrahedron
		// exact up to degree 5 according to the book of Flaherty
		{	assert ( dynamic_cast < FiniteElement::WithMaster::Tetrahedron* > ( fe .core ) );
			#ifndef NDEBUG  // DEBUG mode
			this->info_string = "Gauss quadrature with fifteen points on a tetrahedron\n";
			this->info_string += "exact up to degree 5\n";
			#endif
			Function xi_eta_zeta = master_manifold .coordinates();
			assert ( xi_eta_zeta .number_of ( tag::components ) == 3 );
			Function xi = xi_eta_zeta [0], eta = xi_eta_zeta [1], zeta = xi_eta_zeta [2];
			// points
			Cell Gauss_11_cen ( tag::vertex );  xi ( Gauss_11_cen ) = 0.25;  
				eta ( Gauss_11_cen ) = 0.25; zeta ( Gauss_11_cen ) = 0.25;
			Cell Gauss_11_O ( tag::vertex );  xi ( Gauss_11_O ) = 0.090909090909091;  
				eta ( Gauss_11_O ) = 0.090909090909091; zeta ( Gauss_11_O ) = 0.090909090909091;
			Cell Gauss_11_A ( tag::vertex );  xi ( Gauss_11_A ) = 0.72727272727272727;   
				eta ( Gauss_11_A ) = 0.090909090909091;  zeta ( Gauss_11_A ) = 0.090909090909091;
			Cell Gauss_11_B ( tag::vertex );  xi ( Gauss_11_B ) = 0.090909090909091;  
				eta ( Gauss_11_B ) = 0.72727272727272727; zeta ( Gauss_11_B ) = 0.090909090909091;
			Cell Gauss_11_C ( tag::vertex );  xi ( Gauss_11_C ) = 0.090909090909091;  
				eta ( Gauss_11_C ) = 0.090909090909091;  zeta ( Gauss_11_C ) = 0.72727272727272727;
			Cell Gauss_11_ABC ( tag::vertex );  xi ( Gauss_11_ABC ) = 1./3.;  
				eta ( Gauss_11_ABC ) = 1./3.; zeta ( Gauss_11_ABC ) = 1./3.;
			Cell Gauss_11_OAB ( tag::vertex );  xi ( Gauss_11_OAB ) = 1./3.;   
				eta ( Gauss_11_OAB ) = 1./3.;  zeta ( Gauss_11_OAB ) = 0.;
			Cell Gauss_11_OBC ( tag::vertex );  xi ( Gauss_11_OBC ) = 0.;  
				eta ( Gauss_11_OBC ) = 1./3.; zeta ( Gauss_11_OBC ) = 1./3.;
			Cell Gauss_11_OAC ( tag::vertex );  xi ( Gauss_11_OAC ) = 1./3.;  
				eta ( Gauss_11_OAC ) = 0.;  zeta ( Gauss_11_OAC ) = 1./3.;
			Cell Gauss_11_OA ( tag::vertex );  xi ( Gauss_11_OA ) = 0.433449846426336;  
				eta ( Gauss_11_OA ) = 0.066550153573664; zeta ( Gauss_11_OA ) = 0.066550153573664;
			Cell Gauss_11_OB ( tag::vertex );  xi ( Gauss_11_OB ) = 0.066550153573664;   
				eta ( Gauss_11_OB ) = 0.433449846426336;  zeta ( Gauss_11_OB ) = 0.066550153573664;
			Cell Gauss_11_OC ( tag::vertex );  xi ( Gauss_11_OC ) = 0.066550153573664;  
				eta ( Gauss_11_OC ) = 0.066550153573664; zeta ( Gauss_11_OC ) = 0.433449846426336;
			Cell Gauss_11_AB ( tag::vertex );  xi ( Gauss_11_AB ) = 0.433449846426336;  
				eta ( Gauss_11_AB ) = 0.433449846426336;  zeta ( Gauss_11_AB ) = 0.066550153573664;
			Cell Gauss_11_BC ( tag::vertex );  xi ( Gauss_11_BC ) = 0.066550153573664;  
				eta ( Gauss_11_BC ) = 0.433449846426336; zeta ( Gauss_11_BC ) = 0.433449846426336;
			Cell Gauss_11_AC ( tag::vertex );  xi ( Gauss_11_AC ) = 0.433449846426336;  
				eta ( Gauss_11_AC ) = 0.066550153573664;  zeta ( Gauss_11_AC ) = 0.433449846426336;
			this->points .reserve (15);
			this->points .push_back ( Gauss_11_cen );
			this->points .push_back ( Gauss_11_O   );
			this->points .push_back ( Gauss_11_A   );
			this->points .push_back ( Gauss_11_B   );
			this->points .push_back ( Gauss_11_C   );
			this->points .push_back ( Gauss_11_ABC );
			this->points .push_back ( Gauss_11_OAB );
			this->points .push_back ( Gauss_11_OAC );
			this->points .push_back ( Gauss_11_OBC );
			this->points .push_back ( Gauss_11_OA  );
			this->points .push_back ( Gauss_11_OB  );
			this->points .push_back ( Gauss_11_OC  );
			this->points .push_back ( Gauss_11_AB  );
			this->points .push_back ( Gauss_11_BC  );
			this->points .push_back ( Gauss_11_AC  );
			assert ( this->points .size() == 15 );
			// weights
			constexpr double Gw_11_cen = 0.030283678097089;
			constexpr double Gw_11_OAB = 0.006026785714286, Gw_11_OAC = 0.006026785714286,
			             Gw_11_OBC = 0.006026785714286, Gw_11_ABC = 0.006026785714286;
			constexpr double Gw_11_O = 0.011645249086029, Gw_11_A = 0.011645249086029,
			             Gw_11_B = 0.011645249086029, Gw_11_C = 0.011645249086029;
			constexpr double Gw_11_OA = 0.010949141561386, Gw_11_OB = 0.010949141561386,
			             Gw_11_OC = 0.010949141561386, Gw_11_AB = 0.010949141561386,
			             Gw_11_AC = 0.010949141561386, Gw_11_BC = 0.010949141561386;
			this->weights .reserve (15);
			this->weights .push_back ( Gw_11_cen );
			this->weights .push_back ( Gw_11_O   );
			this->weights .push_back ( Gw_11_A   );
			this->weights .push_back ( Gw_11_B   );
			this->weights .push_back ( Gw_11_C   );
			this->weights .push_back ( Gw_11_ABC );
			this->weights .push_back ( Gw_11_OAB );
			this->weights .push_back ( Gw_11_OAC );
			this->weights .push_back ( Gw_11_OBC );
			this->weights .push_back ( Gw_11_OA  );
			this->weights .push_back ( Gw_11_OB  );
			this->weights .push_back ( Gw_11_OC  );
			this->weights .push_back ( Gw_11_AB  );
			this->weights .push_back ( Gw_11_BC  );
			this->weights .push_back ( Gw_11_AC  );
			assert ( this->weights .size() == 15 );
			break;                                                                                  }
		case tag::cube_1 :  // Gauss quadrature with one point on a cube
		// exact for linear functions
		{	assert ( dynamic_cast < FiniteElement::WithMaster::Hexahedron* > ( fe .core ) );
			#ifndef NDEBUG  // DEBUG mode
			this->info_string = "Gauss quadrature with one point on a hexahedron\n";
			this->info_string += "exact for linear functions\n";
			#endif
			Function xi_eta_zeta = master_manifold .coordinates();
			assert ( xi_eta_zeta .number_of ( tag::components ) == 3 );
			Function xi = xi_eta_zeta [0], eta = xi_eta_zeta [1], zeta = xi_eta_zeta [2];
			Cell Gauss_1 ( tag::vertex );
			xi ( Gauss_1 ) = 0.;  eta ( Gauss_1 ) = 0.;  zeta ( Gauss_1 ) = 0.;
			this->points .reserve (1);
			this->points  .push_back ( Gauss_1 );
			assert ( this->points  .size() == 1 );
			this->weights .reserve (1);
			this->weights .push_back ( 8. );
			assert ( this->weights .size() == 1 );
			break;                                                                            }
		case tag::cube_8 :  // Gauss quadrature with eight points on a cube
		// exact for polynomials of degree 3 in each variable
		{	assert ( dynamic_cast < FiniteElement::WithMaster::Hexahedron* > ( fe .core ) );
			#ifndef NDEBUG  // DEBUG mode
			this->info_string = "Gauss quadrature with eight points on a hexahedron\n";
			this->info_string += "exact for polynomials of degree 3 in each variable\n";
			#endif
			Function xi_eta_zeta = master_manifold .coordinates();
			assert ( xi_eta_zeta .number_of ( tag::components ) == 3 );
			Function xi = xi_eta_zeta [0], eta = xi_eta_zeta [1], zeta = xi_eta_zeta [2];
			constexpr double sqrt_of_one_third = std::sqrt (1./3.);
			Cell Gauss_8_a ( tag::vertex );   xi   ( Gauss_8_a ) = -sqrt_of_one_third;
			                                  eta  ( Gauss_8_a ) = -sqrt_of_one_third;
			                                  zeta ( Gauss_8_a ) = -sqrt_of_one_third;
			Cell Gauss_8_b ( tag::vertex );   xi   ( Gauss_8_b ) = -sqrt_of_one_third;
			                                  eta  ( Gauss_8_b ) = -sqrt_of_one_third;
			                                  zeta ( Gauss_8_b ) =  sqrt_of_one_third;
			Cell Gauss_8_c ( tag::vertex );   xi   ( Gauss_8_c ) = -sqrt_of_one_third;
			                                  eta  ( Gauss_8_c ) =  sqrt_of_one_third;
			                                  zeta ( Gauss_8_c ) = -sqrt_of_one_third;
			Cell Gauss_8_d ( tag::vertex );   xi   ( Gauss_8_d ) = -sqrt_of_one_third;
			                                  eta  ( Gauss_8_d ) =  sqrt_of_one_third;
			                                  zeta ( Gauss_8_d ) =  sqrt_of_one_third;
			Cell Gauss_8_e ( tag::vertex );   xi   ( Gauss_8_e ) =  sqrt_of_one_third;
			                                  eta  ( Gauss_8_e ) = -sqrt_of_one_third;
			                                  zeta ( Gauss_8_e ) = -sqrt_of_one_third;
			Cell Gauss_8_f ( tag::vertex );   xi   ( Gauss_8_f ) =  sqrt_of_one_third;
			                                  eta  ( Gauss_8_f ) = -sqrt_of_one_third;
			                                  zeta ( Gauss_8_f ) =  sqrt_of_one_third;
			Cell Gauss_8_g ( tag::vertex );   xi   ( Gauss_8_g ) =  sqrt_of_one_third;
			                                  eta  ( Gauss_8_g ) =  sqrt_of_one_third;
			                                  zeta ( Gauss_8_g ) = -sqrt_of_one_third;
			Cell Gauss_8_h ( tag::vertex );   xi   ( Gauss_8_h ) =  sqrt_of_one_third;
			                                  eta  ( Gauss_8_h ) =  sqrt_of_one_third;
			                                  zeta ( Gauss_8_h ) =  sqrt_of_one_third;
			this->points .reserve (8);
			this->points  .push_back ( Gauss_8_a );
			this->points  .push_back ( Gauss_8_b );
			this->points  .push_back ( Gauss_8_c );
			this->points  .push_back ( Gauss_8_d );
			this->points  .push_back ( Gauss_8_e );
			this->points  .push_back ( Gauss_8_f );
			this->points  .push_back ( Gauss_8_g );
			this->points  .push_back ( Gauss_8_h );
			assert ( this->points  .size() == 8 );
			this->weights .reserve (8);
			this->weights .push_back ( 1. );
			this->weights .push_back ( 1. );
			this->weights .push_back ( 1. );
			this->weights .push_back ( 1. );
			this->weights .push_back ( 1. );
			this->weights .push_back ( 1. );
			this->weights .push_back ( 1. );
			this->weights .push_back ( 1. );
			assert ( this->weights .size() == 8 );
			break;                                                                            }
		case tag::cube_27 :  // Gauss quadrature with twenty-seven points on a cube
		// exact for polynomials of degree 5 in each variable
		{	assert ( dynamic_cast < FiniteElement::WithMaster::Hexahedron* > ( fe .core ) );
			#ifndef NDEBUG  // DEBUG mode
			this->info_string = "Gauss quadrature with twenty-seven points on a hexahedron\n";
			this->info_string += "exact for polynomials of degree 5 in each variable\n";
			#endif
			Function xi_eta_zeta = master_manifold .coordinates();
			assert ( xi_eta_zeta .number_of ( tag::components ) == 3 );
			Function xi = xi_eta_zeta [0], eta = xi_eta_zeta [1], zeta = xi_eta_zeta [2];
			constexpr double sqrt3over5 = std::sqrt (0.6);
			const std::vector < double > coord_1d { -sqrt3over5, 0., sqrt3over5 };
			const std::vector < double > weights_1d { 5./9., 8./9., 5./9. };
			this->points .reserve (27);
			this->weights .reserve (27);
			for ( size_t i = 0; i < 3; i++ )
			for ( size_t j = 0; j < 3; j++ )
			for ( size_t k = 0; k < 3; k++ )
			{	Cell Gauss_27 ( tag::vertex );  xi   ( Gauss_27 ) = coord_1d [i];
				                                eta  ( Gauss_27 ) = coord_1d [j];
				                                zeta ( Gauss_27 ) = coord_1d [k];
				this->points  .push_back ( Gauss_27 );
				this->weights .push_back ( weights_1d [i] * weights_1d [j] * weights_1d [k] );  }
			assert ( this->points  .size() == 27 );
			assert ( this->weights .size() == 27 );
			break;                                                                                }
		case tag::cube_64 :  // Gauss quadrature with sixty-four points on a cube
		// exact for polynomials of degree 7 in each variable
		{	assert ( dynamic_cast < FiniteElement::WithMaster::Hexahedron* > ( fe .core ) );
			#ifndef NDEBUG  // DEBUG mode
			this->info_string = "Gauss quadrature with twenty-seven points on a hexahedron\n";
			this->info_string += "exact for polynomials of degree 5 in each variable\n";
			#endif
			Function xi_eta_zeta = master_manifold .coordinates();
			assert ( xi_eta_zeta .number_of ( tag::components ) == 3 );
			Function xi = xi_eta_zeta [0], eta = xi_eta_zeta [1], zeta = xi_eta_zeta [2];
			const std::vector < double > coord_1d
				{ -0.861136311594053, -0.339981043584856, 0.339981043584856, 0.861136311594053 };
			const std::vector < double > weights_1d
				{ -0.347854845137454, -0.652145154862546, 0.652145154862546, 0.347854845137454 };
			this->points .reserve (64);
			this->weights .reserve (64);
			for ( size_t i = 0; i < 4; i++ )
			for ( size_t j = 0; j < 4; j++ )
			for ( size_t k = 0; k < 4; k++ )
			{	Cell Gauss_64 ( tag::vertex );  xi   ( Gauss_64 ) = coord_1d [i];
				                                eta  ( Gauss_64 ) = coord_1d [j];
				                                zeta ( Gauss_64 ) = coord_1d [k];
				this->points  .push_back ( Gauss_64 );
				this->weights .push_back ( weights_1d [i] * weights_1d [j] * weights_1d [k] );  }
			assert ( this->points  .size() == 64 );
			assert ( this->weights .size() == 64 );
			break;                                                                                }
		default :	assert ( false );

	}  // end of  switch ( q )

}  // end of  Integrator::Gauss::Gauss
	
//------------------------------------------------------------------------------------------------------//


void Integrator::Gauss::pre_compute ( const std::vector < Function > & bf,
                                      const std::vector < Function > & v  )
// virtual from Integrator::Core
{	std::cout << __FILE__ << ":" << __LINE__ << ": "
	          << __extension__ __PRETTY_FUNCTION__ << ": " << std::endl;
	std::cout << "pre_compute is not necessary for Gauss integrators" << std::endl;
	exit ( 1 );                                                                      }


std::vector < double > Integrator::Gauss::retrieve_precomputed
( const Function & bf, const Function & psi )  // virtual from Integrator::Core
{	std::cout << __FILE__ << ":" << __LINE__ << ": "
	          << __extension__ __PRETTY_FUNCTION__ << ": " << std::endl;
	std::cout << "Gauss integrators do not pre-compute expressions" << std::endl;
	exit ( 1 );                                                                    }


std::vector < double > Integrator::Gauss::retrieve_precomputed
( const Function & bf1, const Function & psi1,
  const Function & bf2, const Function & psi2 )  // virtual from Integrator::Core
{	std::cout << __FILE__ << ":" << __LINE__ << ": "
	          << __extension__ __PRETTY_FUNCTION__ << ": " << std::endl;
	std::cout << "Gauss integrators do not pre-compute expressions" << std::endl;
	exit ( 1 );                                                                    }


void Integrator::HandCoded::pre_compute ( const std::vector < Function > & bf,
                                          const std::vector < Function > & v  )
// virtual from Integrator::Core

// bf is an arbitrary basis function (Function::MereSymbol) in the finite element
// we prepare computations for fast evaluation of integrals of expressions listed in 'v'

// we delegate this analysis to the finite element

{	this->finite_element .core->pre_compute ( bf, v );  }


std::vector < double > FiniteElement::WithMaster::retrieve_precomputed
( const Function & bf, const Function & psi )  // virtual from FiniteElement::Core
{	std::cout << __FILE__ << ":" << __LINE__ << ": "
	          << __extension__ __PRETTY_FUNCTION__ << ": " << std::endl;
	std::cout << "finite elements with master do not pre-compute expressions" << std::endl;
	exit ( 1 );                                                                              }


std::vector < double > FiniteElement::WithMaster::retrieve_precomputed
( const Function & bf1, const Function & bf2,
  const Function & psi1, const Function & psi2 )  // virtual from FiniteElement::Core
{	std::cout << __FILE__ << ":" << __LINE__ << ": "
	          << __extension__ __PRETTY_FUNCTION__ << ": " << std::endl;
	std::cout << "finite elements with master do not pre-compute expressions" << std::endl;
	exit ( 1 );                                                                              }


std::vector < double > FiniteElement::StandAlone::TypeOne::Triangle::retrieve_precomputed
( const Function & dbf, const Function & psi )  // virtual from FiniteElement::Core

{	assert ( this->dummy_bf .size() == 1 );
	assert ( this->dummy_bf [0] .core == dbf .core );
	assert ( this->basis_numbering [ psi .core ] < this->base_dim );
	const size_t i = FiniteElement::StandAlone::index
		( 0, this->basis_numbering [ psi .core ], 0, this->base_dim, this->case_16 );
	std::vector < double > res ( this->selector .size() );
	for ( size_t k = 0; k < this->selector .size(); k++ )
	{	assert ( i + this->selector [k] < this->result_of_integr .size() );
		res [k] = this->result_of_integr [ i + this->selector [k] ];         }
	return res;                                                                       }


std::vector < double > FiniteElement::StandAlone::TypeOne::Triangle::retrieve_precomputed
( const Function & dbf1, const Function & dbf2,
  const Function & psi1, const Function & psi2 )  // virtual from FiniteElement::Core

{	assert ( this->dummy_bf .size() == 2 );
	assert ( this->dummy_bf [0] .core == dbf1 .core );
	assert ( this->dummy_bf [1] .core == dbf2 .core );
	assert ( this->basis_numbering [ psi1 .core ] < this->base_dim  );
	assert ( this->basis_numbering [ psi2 .core ] < this->base_dim );
	const size_t i = FiniteElement::StandAlone::index
		( this->basis_numbering [ psi1 .core ],
		  this->basis_numbering [ psi2 .core ], 0, this->base_dim, this->case_16 );
	std::vector < double > res ( this->selector .size() );
	for ( size_t k = 0; k < this->selector .size(); k++ )
	{	assert ( i + this->selector [k] < this->result_of_integr .size() );
		res [k] = this->result_of_integr [ i + this->selector [k] ];         }
	return res;                                                                     }
	

std::vector < double > FiniteElement::StandAlone::TypeOne::Quadrangle::retrieve_precomputed
( const Function & dbf, const Function & psi )  // virtual from FiniteElement::Core

{	assert ( this->dummy_bf .size() == 1 );
	assert ( this->dummy_bf [0] .core == dbf .core );
	assert ( this->basis_numbering [ psi .core ] < this->base_dim );
	const size_t i = FiniteElement::StandAlone::index
		( 0, this->basis_numbering [ psi .core ], 0, this->base_dim, this->case_16 );
	std::vector < double > res ( this->selector .size() );
	for ( size_t k = 0; k < this->selector .size(); k++ )
	{	assert ( i + this->selector [k] < this->result_of_integr .size() );
		res [k] = this->result_of_integr [ i + this->selector [k] ];         }
	return res;                                                                       }


std::vector < double > FiniteElement::StandAlone::TypeOne::Quadrangle::retrieve_precomputed
( const Function & dbf1, const Function & dbf2,
  const Function & psi1, const Function & psi2 )  // virtual from FiniteElement::Core

{	assert ( this->dummy_bf .size() == 2 );
	assert ( this->dummy_bf [0] .core == dbf1 .core );
	assert ( this->dummy_bf [1] .core == dbf2 .core );
	assert ( this->basis_numbering [ psi1 .core ] < this->base_dim  );
	assert ( this->basis_numbering [ psi2 .core ] < this->base_dim );
	const size_t i = FiniteElement::StandAlone::index
		( this->basis_numbering [ psi1 .core ],
		  this->basis_numbering [ psi2 .core ], 0, this->base_dim, this->case_16 );
	std::vector < double > res ( this->selector .size() );
	for ( size_t k = 0; k < this->selector .size(); k++ )
	{	assert ( i + this->selector [k] < this->result_of_integr .size() );
		res [k] = this->result_of_integr [ i + this->selector [k] ];         }
	return res;                                                                     }
	

std::vector < double > FiniteElement::StandAlone::TypeOne::Tetrahedron::retrieve_precomputed
( const Function & dbf, const Function & psi )  // virtual from FiniteElement::Core

{	assert ( this->dummy_bf .size() == 1 );
	assert ( this->dummy_bf [0] .core == dbf .core );
	assert ( this->basis_numbering [ psi .core ] < this->base_dim );
	const size_t i = FiniteElement::StandAlone::index
		( 0, this->basis_numbering [ psi .core ], 0, this->base_dim, this->case_16 );
	std::vector < double > res ( this->selector .size() );
	for ( size_t k = 0; k < this->selector .size(); k++ )
	{	assert ( i + this->selector [k] < this->result_of_integr .size() );
		res [k] = this->result_of_integr [ i + this->selector [k] ];         }
	return res;                                                                       }


std::vector < double > FiniteElement::StandAlone::TypeOne::Tetrahedron::retrieve_precomputed
( const Function & dbf1, const Function & dbf2,
  const Function & psi1, const Function & psi2 )  // virtual from FiniteElement::Core

{	assert ( this->dummy_bf .size() == 2 );
	assert ( this->dummy_bf [0] .core == dbf1 .core );
	assert ( this->dummy_bf [1] .core == dbf2 .core );
	assert ( this->basis_numbering [ psi1 .core ] < this->base_dim  );
	assert ( this->basis_numbering [ psi2 .core ] < this->base_dim );
	const size_t i = FiniteElement::StandAlone::index
		( this->basis_numbering [ psi1 .core ],
		  this->basis_numbering [ psi2 .core ], 0, this->base_dim, this->case_16 );
	std::vector < double > res ( this->selector .size() );
	for ( size_t k = 0; k < this->selector .size(); k++ )
	{	assert ( i + this->selector [k] < this->result_of_integr .size() );
		res [k] = this->result_of_integr [ i + this->selector [k] ];         }
	return res;                                                                     }
	
//------------------------------------------------------------------------------------------------------//


double Integrator::Gauss::action ( Function f, const FiniteElement & fe )
// virtual from Integrator::Core

// assumes the finite element is already docked on a cell
// thus, fe_core->transf is well defined

{	FiniteElement::WithMaster * fe_core = tag::Util::assert_cast
		< FiniteElement::Core*, FiniteElement::WithMaster* > ( fe .core );
	assert ( fe_core->transf .core );
	Function::Map * tran = dynamic_cast < Function::Map* > ( fe_core->transf .core );
	assert ( tran );
	// in the above we must use dynamic_cast
	// below, with -DNDEBUG, assert_cast calls a static_cast which does not compile
	//   Function::Map * tran = tag::Util::assert_cast
	// 	   < Function::Core*, Function::Map* > ( fe_core->transf .core );
	// classes Function::Core and Function::Map are not directly related
	// perhaps we should have used virtual inheritance ?

	size_t geom_dim = tran->geom_coords .number_of ( tag::components );
	assert ( geom_dim == tran->back_geom_coords .number_of ( tag::components ) );
	for ( size_t i = 0; i < geom_dim; i++ )
		f = f .replace ( tran->geom_coords [i], tran->back_geom_coords [i] );

	double res = 0.;
	std::vector < double > ::const_iterator it_weight = this->weights .begin();
	for ( std::vector < Cell > ::const_iterator it_point = this->points .begin();
	      it_point != this->points .end(); it_point++                            )
	{	assert ( it_weight != this->weights .end() );
		Cell Gauss_point = *it_point;
		const double w = *it_weight;
		res += w * f ( Gauss_point ) * tran->det ( Gauss_point );
		it_weight++;                                               }
	assert ( it_weight == this->weights .end() );
	return res;                                                                         }


double Integrator::Gauss::action ( Function f )
// virtual from Integrator::Core

// assumes the finite element is already docked on a cell
// thus, fe_core->transf is well defined

{	return this->action ( f, this->finite_element );  }


double Integrator::Gauss::action ( const Function & f, const tag::On &, const Cell & cll )
// virtual from Integrator::Core

{	this->finite_element .dock_on ( cll );
	return this->action ( f, this->finite_element );  }


double Integrator::Gauss::action ( const Function & f, const tag::On &, const Mesh & msh )
// virtual from Integrator::Core

{	Mesh::Iterator it = msh .iterator ( tag::over_cells_of_max_dim );
	double res = 0.;
	for ( it .reset(); it .in_range(); it++ )
	{	Cell cll = *it;
		res += this->action ( f, tag::on, cll );  }
	return res;                                                                            }


double Integrator::HandCoded::action ( const FiniteElement & )
// virtual from Integrator::Core
{	std::cout << __FILE__ << ":" << __LINE__ << ": "
	          << __extension__ __PRETTY_FUNCTION__ << ": " << std::endl;
	std::cout << "hand-coded integrators cannot be invoked this way" << std::endl;
	exit ( 1 );                                                                     }


double Integrator::HandCoded::action ( const double f, const FiniteElement & )
// virtual from Integrator::Core
{	std::cout << __FILE__ << ":" << __LINE__ << ": "
	          << __extension__ __PRETTY_FUNCTION__ << ": " << std::endl;
	std::cout << "hand-coded integrators cannot be invoked this way" << std::endl;
	exit ( 1 );                                                                     }


double Integrator::HandCoded::action ( Function f, const FiniteElement & )
// virtual from Integrator::Core
{	std::cout << __FILE__ << ":" << __LINE__ << ": "
	          << __extension__ __PRETTY_FUNCTION__ << ": " << std::endl;
	std::cout << "hand-coded integrators cannot be invoked this way" << std::endl;
	exit ( 1 );                                                                     }


double Integrator::HandCoded::action ( Function f )
// virtual from Integrator::Core
{	std::cout << __FILE__ << ":" << __LINE__ << ": "
	          << __extension__ __PRETTY_FUNCTION__ << ": " << std::endl;
	std::cout << "hand-coded integrators cannot be invoked this way" << std::endl;
	exit ( 1 );                                                                     }


double Integrator::HandCoded::action ( const Function & f, const tag::On &, const Cell & cll )
// virtual from Integrator::Core
{	std::cout << __FILE__ << ":" << __LINE__ << ": "
	          << __extension__ __PRETTY_FUNCTION__ << ": " << std::endl;
	std::cout << "hand-coded integrators cannot be invoked this way" << std::endl;
	exit ( 1 );                                                                     }


double Integrator::HandCoded::action ( const Function & f, const tag::On &, const Mesh & msh )
// virtual from Integrator::Core
{	std::cout << __FILE__ << ":" << __LINE__ << ": "
	          << __extension__ __PRETTY_FUNCTION__ << ": " << std::endl;
	std::cout << "hand-coded integrators cannot be invoked this way" << std::endl;
	exit ( 1 );                                                                     }

//------------------------------------------------------------------------------------------------------//


void FiniteElement::WithMaster::Segment::dock_on ( const Cell & cll )
// virtual from FiniteElement::Core

{	assert ( cll .dim() == 1 );
	this->docked_on = cll;
	Cell P = cll .base() .reverse();
	Cell Q = cll .tip();

	Function t = this->master_manif  .coordinates();
	assert ( t .number_of ( tag::components ) == 1 );

	Function xyz = Manifold::working .coordinates();
	const size_t geom_dim = xyz .number_of ( tag::components );
	if ( geom_dim == 1 )
	{	const double xP = xyz (P), xQ = xyz (Q);
		Function x_c = ( xP * (1.-t) + xQ * (1.+t) ) / 2.;
		this->transf = Function ( tag::diffeomorphism, tag::one_dim, xyz, t, x_c );  }
	else  // geometric dimension >= 2
	{	Function x = xyz [0];
		double xP = x(P), xQ = x(Q);
		Function xyz_c = ( xP * (1.-t) + xQ * (1.+t) ) / 2.;
		for ( size_t d = 1; d < geom_dim; d++ )
		{	x = xyz [d];
			xP = x(P);  xQ = x(Q);
			xyz_c = xyz_c && ( ( xP * (1.-t) + xQ * (1.+t) ) / 2. );  }
		assert ( xyz_c .number_of ( tag::components ) == geom_dim );
		this->transf = Function ( tag::immersion, xyz, t, xyz_c );      }

	this->base_fun_1 .clear();
	this->base_fun_1 .insert ( std::pair < Cell::Core*, Function >
		( P.core, Function ( (1.-t)/2., tag::composed_with, this->transf ) ) );
	this->base_fun_1 .insert ( std::pair < Cell::Core*, Function >
		( Q.core, Function ( (1.+t)/2., tag::composed_with, this->transf ) ) );
	
}  // end of  FiniteElement::WithMaster::Segment::dock_on

//------------------------------------------------------------------------------------------------------//


#ifndef MANIFEM_NO_QUOTIENT

void FiniteElement::WithMaster::Segment::dock_on ( const Cell & cll, const tag::Winding & )
// virtual from FiniteElement::Core

{	assert ( cll. dim() == 1 );
	this->docked_on = cll;
	Cell P = cll .base() .reverse();
	Cell Q = cll .tip();

	Function t = this->master_manif .coordinates();
	assert ( t .number_of ( tag::components ) == 1 );

	Function xyz = Manifold::working .coordinates();
	const size_t geom_dim = xyz .number_of ( tag::components );
	if ( geom_dim == 1 )
	{	const double xP = xyz (P), xQ = xyz ( Q, tag::winding, cll .winding() );
		Function x_c = ( xP * (1.-t) + xQ * (1.+t) ) / 2.;
		this->transf = Function ( tag::diffeomorphism, tag::one_dim, xyz, t, x_c ); }
	else  // geometric dimension >= 2
	{	Function x = xyz [0];
		double xP = x (P), xQ = x ( Q, tag::winding, cll .winding() );
		Function xyz_c = ( xP * (1.-t) + xQ * (1.+t) ) / 2.;
		for ( size_t d = 1; d < geom_dim; d++ )
		{	x = xyz [d];
			xP = x (P);
			xQ = x ( Q, tag::winding, cll .winding() );
			xyz_c = xyz_c && ( ( xP * (1.-t) + xQ * (1.+t) ) / 2. );  }
		assert ( xyz_c .number_of ( tag::components ) == geom_dim );
		this->transf = Function ( tag::immersion, xyz, t, xyz_c );      }

	this->base_fun_1 .clear();
	this->base_fun_1 .insert ( std::pair < Cell::Core*, Function >
		( P.core, Function ( (1.-t)/2., tag::composed_with, this->transf ) ) );
	this->base_fun_1 .insert ( std::pair < Cell::Core*, Function >
		( Q.core, Function ( (1.+t)/2., tag::composed_with, this->transf ) ) );
	
}  // end of  FiniteElement::WithMaster::Segment::dock_on  with tag::winding

#endif  // ifndef MANIFEM_NO_QUOTIENT

//------------------------------------------------------------------------------------------------------//


void FiniteElement::WithMaster::Triangle::P1::dock_on ( const Cell & cll )
// virtual from FiniteElement::Core

{	assert ( cll .dim() == 2 );
	this->docked_on = cll;

	Mesh::Iterator it = cll .boundary() .iterator ( tag::over_vertices, tag::require_order );
	it .reset();  assert ( it .in_range() );  Cell P = *it;
	it++;  assert ( it .in_range() );  Cell Q = *it;
	it++;  assert ( it .in_range() );  Cell R = *it;
	it++;  assert ( not it .in_range() );
	
	Function xi_eta = this->master_manif .coordinates();
	assert ( xi_eta .number_of ( tag::components ) == 2 );
	Function xi = xi_eta [0], eta = xi_eta [1];
	Function one_m_xi_m_eta = 1. - xi - eta;

	// xi, eta and 1-xi-eta are a base of functions defined on the master element
	// we may need to differentiate them with respect to x and y (physical coordinates)
	// which is equivalent to differentiating x_c and y_c with respect to xi and eta
	// and then taking the inverse matrix

	Function xyz = Manifold::working .coordinates();
	const size_t geom_dim = xyz .number_of ( tag::components );
	assert ( geom_dim >= 2 );

	if ( geom_dim == 2 )

	{	Function x = xyz [0],  y = xyz [1];
		const double xP = x (P), xQ = x (Q), xR = x (R);
		const double yP = y (P), yQ = y (Q), yR = y (R);

		Function x_c = xP * one_m_xi_m_eta + xQ * xi + xR * eta;
		Function y_c = yP * one_m_xi_m_eta + yQ * xi + yR * eta;

		this->transf = Function ( tag::diffeomorphism, tag::high_dim, xyz, xi_eta, x_c && y_c );  }
	
	else  // geometric dimension >= 3
		
	{	Function x = xyz [0];
		double xP = x(P), xQ = x(Q), xR = x(R);
		Function xyz_c = xP * one_m_xi_m_eta + xQ * xi + xR * eta;

		for ( size_t d = 1; d < geom_dim; d++ )
		{	x = xyz [d];
			xP = x (P);  xQ = x (Q);  xR = x (R);
			xyz_c = xyz_c && ( xP * one_m_xi_m_eta + xQ * xi + xR * eta );  }
		assert ( xyz_c .number_of ( tag::components ) == geom_dim );

		this->transf = Function ( tag::immersion, xyz, xi_eta, xyz_c );       }

	this->base_fun_1 .clear();
	this->base_fun_1 .insert ( std::pair < Cell::Core*, Function >
	       ( P .core, Function ( one_m_xi_m_eta, tag::composed_with, this->transf ) ) );
	this->base_fun_1 .insert ( std::pair < Cell::Core*, Function >
	       ( Q .core, Function ( xi, tag::composed_with, this->transf ) ) );
	this->base_fun_1 .insert ( std::pair < Cell::Core*, Function >
	       ( R .core, Function ( eta, tag::composed_with, this->transf ) ) );

	// the only use of composing is to allow the calling code to
	// differentiate base functions with respect to geometric coordinates
	// and anyway this only works for a diffeomorpsm, not for an immersion
	
}  // end of  FiniteElement::WithMaster::Triangle::P1::dock_on

//------------------------------------------------------------------------------------------------------//


#ifndef MANIFEM_NO_QUOTIENT

void FiniteElement::WithMaster::Triangle::P1::dock_on ( const Cell & cll, const tag::Winding & )
// virtual from FiniteElement::Core

{	assert ( cll .dim() == 2 );
	this->docked_on = cll;

	// perhaps implement a special iterator returning points and segments
	Mesh::Iterator it = cll .boundary() .iterator ( tag::over_vertices, tag::require_order );
	it .reset();  assert ( it .in_range() );  Cell P = *it;
	Cell PQ = cll .boundary() .cell_in_front_of ( P );
	it++;  assert ( it .in_range() );  Cell Q = *it;
	Cell QR = cll .boundary() .cell_in_front_of ( Q );
	it++;  assert ( it .in_range() );  Cell R = *it;
	#ifndef NDEBUG  // DEBUG mode
	Cell RP = cll .boundary() .cell_in_front_of ( R );
	it++;  assert ( not it .in_range() );
	#endif

	Function::Action winding_Q = PQ .winding(), winding_R = winding_Q + QR .winding();
	assert ( winding_R + RP .winding() == 0 );
	
	Function xi_eta = this->master_manif .coordinates();
	assert ( xi_eta .number_of ( tag::components ) == 2 );
	Function xi = xi_eta [0], eta = xi_eta [1];
	Function one_m_xi_m_eta = 1.- xi - eta;

	// xi, eta and 1-xi-eta are a base of functions defined on the master element
	// we may need to differentiate them with respect to x and y (physical coordinates)
	// which is equivalent to differentiating x_c and y_c with respect to xi and eta
	// and then taking the inverse matrix

	Function xyz = Manifold::working .coordinates();
	const size_t geom_dim = xyz .number_of ( tag::components );
	assert ( geom_dim >= 2 );

	if ( geom_dim == 2 )

	{	const std::vector < double > xyz_P = xyz ( P ),
		                             xyz_Q = xyz ( Q, tag::winding, winding_Q ),
		                             xyz_R = xyz ( R, tag::winding, winding_R );

		Function x_c = xyz_P [0] * one_m_xi_m_eta + xyz_Q [0] * xi + xyz_R [0] * eta;
		Function y_c = xyz_P [1] * one_m_xi_m_eta + xyz_Q [1] * xi + xyz_R [1] * eta;

		this->transf =
			Function ( tag::diffeomorphism, tag::high_dim, xyz, xi_eta, x_c && y_c );   }
	
	else  // geometric dimension >= 3
		
	{	Function x = xyz [0];
		double xP = x (P),
		       xQ = x ( Q, tag::winding, winding_Q ),
		       xR = x ( R, tag::winding, winding_R );
		Function xyz_c = xP * one_m_xi_m_eta + xQ * xi + xR * eta;

		for ( size_t d = 1; d < geom_dim; d++ )
		{	x = xyz [d];
			xP = x (P);
			xQ = x ( Q, tag::winding, winding_Q );
			xR = x ( R, tag::winding, winding_R );
			xyz_c = xyz_c && ( xP * one_m_xi_m_eta + xQ * xi + xR * eta );  }
		assert ( xyz_c .number_of ( tag::components ) == geom_dim );

		this->transf = Function ( tag::immersion, xyz, xi_eta, xyz_c );       }

	this->base_fun_1 .clear();
	this->base_fun_1 .insert ( std::pair < Cell::Core*, Function >
	       ( P .core, Function ( one_m_xi_m_eta, tag::composed_with, this->transf ) ) );
	this->base_fun_1 .insert ( std::pair < Cell::Core*, Function >
	       ( Q .core, Function ( xi, tag::composed_with, this->transf ) ) );
	this->base_fun_1 .insert ( std::pair < Cell::Core*, Function >
	       ( R .core, Function ( eta, tag::composed_with, this->transf ) ) );

	// the only use of composing is to allow the calling code to
	// differentiate base functions with respect to geometric coordinates
	// and anyway this only works for a diffeomorpsm, not for an immersion
	
}  // end of  FiniteElement::WithMaster::Triangle::P1::dock_on  with tag::winding

#endif  // ifndef MANIFEM_NO_QUOTIENT

//------------------------------------------------------------------------------------------------------//


void FiniteElement::WithMaster::Triangle::P2::Straight::dock_on ( const Cell & cll )
// virtual from FiniteElement::Core

{	assert ( cll .dim() == 2 );
	this->docked_on = cll;

	// implement a hybrid iterator, to be used like this :
	// Mesh::Iterator it = cll .boundary() .iterator
	// 	( tag::over_vertices_and_segments, tag::require_order );
	// it .reset( tag::vertex );  assert ( it .in_range() );  Cell P = *it;
	// it .advance_half_step();  assert ( it .in_range() );  Cell PQ = *it;
	// it .advance_half_step();  assert ( it .in_range() );  Cell Q = *it;
	// it .advance_half_step();  assert ( it .in_range() );  Cell QR = *it;
	// it .advance_half_step();  assert ( it .in_range() );  Cell R = *it;
	// it .advance_half_step();  assert ( it .in_range() );  Cell RP = *it;
	// it .advance_half_step();  assert ( not it .in_range() );
	// assert ( RP .tip() == P );
	
	Mesh::Iterator it = cll .boundary() .iterator ( tag::over_vertices, tag::require_order );
	it .reset();  assert ( it .in_range() );  Cell P = *it;
	Cell PQ = cll .boundary() .cell_in_front_of ( P );
	it++;  assert ( it .in_range() );  Cell Q = *it;
	Cell QR = cll .boundary() .cell_in_front_of ( Q );
	it++;  assert ( it .in_range() );  Cell R = *it;
	Cell RP = cll .boundary() .cell_in_front_of ( R );
	it++;  assert ( not it .in_range() );

	Function xi_eta = this->master_manif .coordinates();
	assert ( xi_eta .number_of ( tag::components ) == 2 );
	Function xi = xi_eta [0], eta = xi_eta [1];
	Function one_m_xi_m_eta = 1. - xi - eta;

	// xi, eta and 1-xi-eta are a base of functions defined on the master element
	// we may need to differentiate them with respect to x and y (physical coordinates)
	// which is equivalent to differentiating x_c and y_c with respect to xi and eta
	// and then taking the inverse matrix

	Function xyz = Manifold::working .coordinates();
	const size_t geom_dim = xyz .number_of ( tag::components );
	assert ( geom_dim >= 2 );

	if ( geom_dim == 2 )

	{	Function x = xyz [0],  y = xyz [1];
		const double xP = x (P), xQ = x (Q), xR = x (R);
		const double yP = y (P), yQ = y (Q), yR = y (R);

		Function x_c = xP * one_m_xi_m_eta + xQ * xi + xR * eta;
		Function y_c = yP * one_m_xi_m_eta + yQ * xi + yR * eta;

		this->transf =
			Function ( tag::diffeomorphism, tag::high_dim, xyz, xi_eta, x_c && y_c );  }
	
	else  // geometric dimension >= 3
		
	{	Function x = xyz [0];
		double xP = x(P), xQ = x(Q), xR = x(R);
		Function xyz_c = xP * one_m_xi_m_eta + xQ * xi + xR * eta;

		for ( size_t d = 1; d < geom_dim; d++ )
		{	x = xyz [d];
			xP = x (P);  xQ = x (Q);  xR = x (R);
			xyz_c = xyz_c && ( xP * one_m_xi_m_eta + xQ * xi + xR * eta );  }
		assert ( xyz_c .number_of ( tag::components ) == geom_dim );

		this->transf = Function ( tag::immersion, xyz, xi_eta, xyz_c );       }

	// PQ = PQ .get_positive();  QR = QR .get_positive();  RP = RP .get_positive();
	this->base_fun_1 .clear();
	this->base_fun_1 .insert ( std::pair < Cell::Core*, Function >
	       ( P.core, Function ( (1.-2.*xi-2.*eta) * one_m_xi_m_eta,
	                            tag::composed_with, this->transf   ) ) );
	this->base_fun_1 .insert ( std::pair < Cell::Core*, Function >
	       ( Q .core, Function ( (2.*xi-1.) * xi, tag::composed_with, this->transf ) ) );
	this->base_fun_1 .insert ( std::pair < Cell::Core*, Function >
	       ( R .core, Function ( (2.*eta-1.) * eta, tag::composed_with, this->transf ) ) );
	this->base_fun_1 .insert ( std::pair < Cell::Core*, Function >
	       ( PQ .core, Function ( 4. * one_m_xi_m_eta * xi, tag::composed_with, this->transf ) ) );
	this->base_fun_1 .insert ( std::pair < Cell::Core*, Function >
	       ( QR .core, Function ( 4. * xi * eta, tag::composed_with, this->transf ) ) );
	this->base_fun_1 .insert ( std::pair < Cell::Core*, Function >
	       ( RP .core, Function ( 4. * one_m_xi_m_eta * eta, tag::composed_with, this->transf ) ) );

	// the only use of composing is to allow the calling code to
	// differentiate base functions with respect to geometric coordinates
	// and anyway this only works for a diffeomorpsm, not for an immersion
	
}  // end of  FiniteElement::WithMaster::Triangle::P2::Straight::dock_on

//------------------------------------------------------------------------------------------------------//


#ifndef MANIFEM_NO_QUOTIENT

void FiniteElement::WithMaster::Triangle::P2::Straight::dock_on
( const Cell & cll, const tag::Winding & ) // virtual from FiniteElement::Core

{	assert ( cll .dim() == 2 );
	this->docked_on = cll;

	// perhaps implement a special iterator returning points and segments
	Mesh::Iterator it = cll .boundary() .iterator ( tag::over_vertices, tag::require_order );
	it .reset();  assert ( it .in_range() );  Cell P = *it;
	Cell PQ = cll .boundary() .cell_in_front_of ( P );
	it++;  assert ( it .in_range() );  Cell Q = *it;
	Cell QR = cll .boundary() .cell_in_front_of ( Q );
	it++;  assert ( it .in_range() );  Cell R = *it;
	Cell RP = cll .boundary() .cell_in_front_of ( R );
	#ifndef NDEBUG  // DEBUG mode
	it++;  assert ( not it .in_range() );
	#endif

	Function::Action winding_Q = PQ .winding(), winding_R = winding_Q + QR .winding();
	assert ( winding_R + RP .winding() == 0 );
	
	Function xi_eta = this->master_manif .coordinates();
	assert ( xi_eta .number_of ( tag::components ) == 2 );
	Function xi = xi_eta [0], eta = xi_eta [1];
	Function one_m_xi_m_eta = 1.- xi - eta;

	// xi, eta and 1-xi-eta are a base of functions defined on the master element
	// we may need to differentiate them with respect to x and y (physical coordinates)
	// which is equivalent to differentiating x_c and y_c with respect to xi and eta
	// and then taking the inverse matrix

	Function xyz = Manifold::working .coordinates();
	const size_t geom_dim = xyz .number_of ( tag::components );
	assert ( geom_dim >= 2 );

	if ( geom_dim == 2 )

	{	const std::vector < double > xyz_P = xyz ( P ),
		                             xyz_Q = xyz ( Q, tag::winding, winding_Q ),
		                             xyz_R = xyz ( R, tag::winding, winding_R );

		Function x_c = xyz_P [0] * one_m_xi_m_eta + xyz_Q [0] * xi + xyz_R [0] * eta;
		Function y_c = xyz_P [1] * one_m_xi_m_eta + xyz_Q [1] * xi + xyz_R [1] * eta;

		this->transf =
			Function ( tag::diffeomorphism, tag::high_dim, xyz, xi_eta, x_c && y_c );   }
	
	else  // geometric dimension >= 3
		
	{	Function x = xyz [0];
		double xP = x (P),
		       xQ = x ( Q, tag::winding, winding_Q ),
		       xR = x ( R, tag::winding, winding_R );
		Function xyz_c = xP * one_m_xi_m_eta + xQ * xi + xR * eta;

		for ( size_t d = 1; d < geom_dim; d++ )
		{	x = xyz [d];
			xP = x (P);
			xQ = x ( Q, tag::winding, winding_Q );
			xR = x ( R, tag::winding, winding_R );
			xyz_c = xyz_c && ( xP * one_m_xi_m_eta + xQ * xi + xR * eta );  }
		assert ( xyz_c .number_of ( tag::components ) == geom_dim );

		this->transf = Function ( tag::immersion, xyz, xi_eta, xyz_c );       }

	// PQ = PQ .get_positive();  QR = QR .get_positive();  RP = RP .get_positive();
	this->base_fun_1 .clear();
	this->base_fun_1 .insert ( std::pair < Cell::Core*, Function >
	       ( P.core, Function ( (1.-2.*xi-2.*eta) * one_m_xi_m_eta,
                              tag::composed_with, this->transf   ) ) );
	this->base_fun_1 .insert ( std::pair < Cell::Core*, Function >
	       ( Q .core, Function ( (2.*xi-1.) * xi, tag::composed_with, this->transf ) ) );
	this->base_fun_1 .insert ( std::pair < Cell::Core*, Function >
	       ( R .core, Function ( (2.*eta-1.) * eta, tag::composed_with, this->transf ) ) );
	this->base_fun_1 .insert ( std::pair < Cell::Core*, Function >
	       ( PQ .core, Function ( 4. * one_m_xi_m_eta * xi, tag::composed_with, this->transf ) ) );
	this->base_fun_1 .insert ( std::pair < Cell::Core*, Function >
	       ( QR .core, Function ( 4. * xi * eta, tag::composed_with, this->transf ) ) );
	this->base_fun_1 .insert ( std::pair < Cell::Core*, Function >
	       ( RP .core, Function ( 4. * one_m_xi_m_eta * eta, tag::composed_with, this->transf ) ) );

	// the only use of composing is to allow the calling code to
	// differentiate base functions with respect to geometric coordinates
	// and anyway this only works for a diffeomorpsm, not for an immersion
	
}  // end of  FiniteElement::WithMaster::Triangle::P2::Straight::dock_on  with tag::winding

#endif  // ifndef MANIFEM_NO_QUOTIENT

//------------------------------------------------------------------------------------------------------//


void FiniteElement::WithMaster::Triangle::P2::Straight::Incremental::dock_on
( const Cell & cll )  // virtual from FiniteElement::Core

{	assert ( cll .dim() == 2 );
	this->docked_on = cll;

	Mesh::Iterator it = cll .boundary() .iterator ( tag::over_vertices, tag::require_order );
	it .reset();  assert ( it .in_range() );  Cell P = *it;
	Cell PQ = cll .boundary() .cell_in_front_of ( P );
	it++;  assert ( it .in_range() );  Cell Q = *it;
	Cell QR = cll .boundary() .cell_in_front_of ( Q );
	it++;  assert ( it .in_range() );  Cell R = *it;
	Cell RP = cll .boundary() .cell_in_front_of ( R );
	it++;  assert ( not it .in_range() );
	
	Function xi_eta = this->master_manif .coordinates();
	assert ( xi_eta .number_of ( tag::components ) == 2 );
	Function xi = xi_eta [0], eta = xi_eta [1];
	Function one_m_xi_m_eta = 1. - xi - eta;

	// xi, eta and 1-xi-eta are a base of functions defined on the master element
	// we may need to differentiate them with respect to x and y (physical coordinates)
	// which is equivalent to differentiating x_c and y_c with respect to xi and eta
	// and then taking the inverse matrix

	Function xyz = Manifold::working .coordinates();
	const size_t geom_dim = xyz .number_of ( tag::components );
	assert ( geom_dim >= 2 );

	if ( geom_dim == 2 )

	{	Function x = xyz [0],  y = xyz [1];
		const double xP = x (P), xQ = x (Q), xR = x (R);
		const double yP = y (P), yQ = y (Q), yR = y (R);

		Function x_c = xP * one_m_xi_m_eta + xQ * xi + xR * eta;
		Function y_c = yP * one_m_xi_m_eta + yQ * xi + yR * eta;

		this->transf =
			Function ( tag::diffeomorphism, tag::high_dim, xyz, xi_eta, x_c && y_c );  }
	
	else  // geometric dimension >= 3
		
	{	Function x = xyz [0];
		double xP = x(P), xQ = x(Q), xR = x(R);
		Function xyz_c = xP * one_m_xi_m_eta + xQ * xi + xR * eta;

		for ( size_t d = 1; d < geom_dim; d++ )
		{	x = xyz [d];
			xP = x (P);  xQ = x (Q);  xR = x (R);
			xyz_c = xyz_c && ( xP * one_m_xi_m_eta + xQ * xi + xR * eta );  }
		assert ( xyz_c .number_of ( tag::components ) == geom_dim );

		this->transf = Function ( tag::immersion, xyz, xi_eta, xyz_c );       }

	// PQ = PQ .get_positive();  QR = QR .get_positive();  RP = RP .get_positive();
	this->base_fun_1 .clear();
	this->base_fun_1 .insert ( std::pair < Cell::Core*, Function >
	       ( P .core, Function ( one_m_xi_m_eta, tag::composed_with, this->transf ) ) );
	this->base_fun_1 .insert ( std::pair < Cell::Core*, Function >
	       ( Q .core, Function ( xi, tag::composed_with, this->transf ) ) );
	this->base_fun_1 .insert ( std::pair < Cell::Core*, Function >
	       ( R .core, Function ( eta, tag::composed_with, this->transf ) ) );
	this->base_fun_1 .insert ( std::pair < Cell::Core*, Function >
	       ( PQ .core, Function ( one_m_xi_m_eta * xi, tag::composed_with, this->transf ) ) );
	this->base_fun_1 .insert ( std::pair < Cell::Core*, Function >
	       ( QR .core, Function ( xi * eta, tag::composed_with, this->transf ) ) );
	this->base_fun_1 .insert ( std::pair < Cell::Core*, Function >
	       ( RP .core, Function ( one_m_xi_m_eta * eta, tag::composed_with, this->transf ) ) );

	// the only use of composing is to allow the calling code to
	// differentiate base functions with respect to geometric coordinates
	// and anyway this only works for a diffeomorpsm, not for an immersion
	
}  // end of  FiniteElement::WithMaster::Triangle::P2::Straight::Incremental::dock_on

//------------------------------------------------------------------------------------------------------//


#ifndef MANIFEM_NO_QUOTIENT

void FiniteElement::WithMaster::Triangle::P2::Straight::Incremental::dock_on
( const Cell & cll, const tag::Winding & )  // virtual from FiniteElement::Core

{	assert ( cll .dim() == 2 );
	this->docked_on = cll;

	// perhaps implement a special iterator returning points and segments
	Mesh::Iterator it = cll .boundary() .iterator ( tag::over_vertices, tag::require_order );
	it .reset();  assert ( it .in_range() );  Cell P = *it;
	Cell PQ = cll .boundary() .cell_in_front_of ( P );
	it++;  assert ( it .in_range() );  Cell Q = *it;
	Cell QR = cll .boundary() .cell_in_front_of ( Q );
	it++;  assert ( it .in_range() );  Cell R = *it;
	Cell RP = cll .boundary() .cell_in_front_of ( R );
	#ifndef NDEBUG  // DEBUG mode
	it++;  assert ( not it .in_range() );
	#endif

	Function::Action winding_Q = PQ .winding(), winding_R = winding_Q + QR .winding();
	assert ( winding_R + RP .winding() == 0 );
	
	Function xi_eta = this->master_manif .coordinates();
	assert ( xi_eta .number_of ( tag::components ) == 2 );
	Function xi = xi_eta [0], eta = xi_eta [1];
	Function one_m_xi_m_eta = 1.- xi - eta;

	// xi, eta and 1-xi-eta are a base of functions defined on the master element
	// we may need to differentiate them with respect to x and y (physical coordinates)
	// which is equivalent to differentiating x_c and y_c with respect to xi and eta
	// and then taking the inverse matrix

	Function xyz = Manifold::working .coordinates();
	const size_t geom_dim = xyz .number_of ( tag::components );
	assert ( geom_dim >= 2 );

	if ( geom_dim == 2 )

	{	const std::vector < double > xyz_P = xyz ( P ),
		                             xyz_Q = xyz ( Q, tag::winding, winding_Q ),
		                             xyz_R = xyz ( R, tag::winding, winding_R );

		Function x_c = xyz_P [0] * one_m_xi_m_eta + xyz_Q [0] * xi + xyz_R [0] * eta;
		Function y_c = xyz_P [1] * one_m_xi_m_eta + xyz_Q [1] * xi + xyz_R [1] * eta;

		this->transf =
			Function ( tag::diffeomorphism, tag::high_dim, xyz, xi_eta, x_c && y_c );   }
	
	else  // geometric dimension >= 3
		
	{	Function x = xyz [0];
		double xP = x (P),
		       xQ = x ( Q, tag::winding, winding_Q ),
		       xR = x ( R, tag::winding, winding_R );
		Function xyz_c = xP * one_m_xi_m_eta + xQ * xi + xR * eta;

		for ( size_t d = 1; d < geom_dim; d++ )
		{	x = xyz [d];
			xP = x (P);
			xQ = x ( Q, tag::winding, winding_Q );
			xR = x ( R, tag::winding, winding_R );
			xyz_c = xyz_c && ( xP * one_m_xi_m_eta + xQ * xi + xR * eta );  }
		assert ( xyz_c .number_of ( tag::components ) == geom_dim );

		this->transf = Function ( tag::immersion, xyz, xi_eta, xyz_c );            }

	// PQ = PQ .get_positive();  QR = QR .get_positive();  RP = RP .get_positive();
	this->base_fun_1 .clear();
	this->base_fun_1 .insert ( std::pair < Cell::Core*, Function >
	       ( P .core, Function ( one_m_xi_m_eta, tag::composed_with, this->transf ) ) );
	this->base_fun_1 .insert ( std::pair < Cell::Core*, Function >
	       ( Q .core, Function ( xi, tag::composed_with, this->transf ) ) );
	this->base_fun_1 .insert ( std::pair < Cell::Core*, Function >
	       ( R .core, Function ( eta, tag::composed_with, this->transf ) ) );
	this->base_fun_1 .insert ( std::pair < Cell::Core*, Function >
	       ( PQ .core, Function ( one_m_xi_m_eta * xi, tag::composed_with, this->transf ) ) );
	this->base_fun_1 .insert ( std::pair < Cell::Core*, Function >
	       ( QR .core, Function ( xi * eta, tag::composed_with, this->transf ) ) );
	this->base_fun_1 .insert ( std::pair < Cell::Core*, Function >
	       ( RP .core, Function ( one_m_xi_m_eta * eta, tag::composed_with, this->transf ) ) );

	// the only use of composing is to allow the calling code to
	// differentiate base functions with respect to geometric coordinates
	// and anyway this only works for a diffeomorpsm, not for an immersion
	
}  // end of  FiniteElement::WithMaster::Triangle::P2::Straight::Incremental::dock_on
   //         with tag::winding

#endif  // ifndef MANIFEM_NO_QUOTIENT

//------------------------------------------------------------------------------------------------------//


void FiniteElement::WithMaster::Triangle::P2::Curved::dock_on ( const Cell & cll )
// virtual from FiniteElement::Core

{	assert ( cll .dim() == 2 );
	assert ( false );            }

//------------------------------------------------------------------------------------------------------//


void FiniteElement::WithMaster::Triangle::P2::Curved::dock_on ( const Cell & cll, const tag::Winding & )

{	assert ( cll .dim() == 2 );
	assert ( false );            }

//------------------------------------------------------------------------------------------------------//


void FiniteElement::WithMaster::Quadrangle::Q1::dock_on ( const Cell & cll )
// virtual from FiniteElement::Core

{	assert ( cll .dim() == 2 );
	this->docked_on = cll;

	Mesh::Iterator it = cll .boundary() .iterator ( tag::over_vertices, tag::require_order );
	it .reset();  assert ( it .in_range() );  Cell P = *it;
	it++;  assert ( it .in_range() );  Cell Q = *it;
	it++;  assert ( it .in_range() );  Cell R = *it;
	it++;  assert ( it .in_range() );  Cell S = *it;
	it++;  assert ( not it .in_range() );
	
	Function xi_eta = this->master_manif .coordinates();
	assert ( xi_eta .number_of ( tag::components ) == 2 );
	Function xi = xi_eta [0], eta = xi_eta [1];

	Function psi_P = (1.-xi) * (1.-eta), psi_Q = (1.+xi) * (1.-eta),
	         psi_R = (1.+xi) * (1.+eta), psi_S = (1.-xi) * (1.+eta);
	// psi* are a base of functions defined on the master element
	// each takes value 4 in the associated vertex, zero on the others
	// we may need to differentiate them with respect to x and y (physical coordinates)
	// which is equivalent to differentiating x_c and y_c with respect to xi and eta
	// and then taking the inverse matrix
	
	Function xyz = Manifold::working .coordinates();
	const size_t geom_dim = xyz .number_of ( tag::components );
	assert ( geom_dim >= 2 );

	if ( geom_dim == 2 )

	{	Function x = xyz [0],  y = xyz [1];
		const double xP = x (P), xQ = x (Q), xR = x (R), xS = x (S);
		const double yP = y (P), yQ = y (Q), yR = y (R), yS = y (S);

		Function x_c = ( xP * psi_P + xQ * psi_Q + xR * psi_R + xS * psi_S ) / 4.;
		Function y_c = ( yP * psi_P + yQ * psi_Q + yR * psi_R + yS * psi_S ) / 4.;

		this->transf =
			Function ( tag::diffeomorphism, tag::high_dim, xyz, xi_eta, x_c && y_c );  }

	else  // geometric dimension >= 3
		
	{	Function x = xyz [0];
		double xP = x(P), xQ = x(Q), xR = x(R), xS = x(S);
		Function xyz_c = ( xP * psi_P + xQ * psi_Q + xR * psi_R + xS * psi_S ) / 4.;

		for ( size_t d = 1; d < geom_dim; d++ )
		{	x = xyz [d];
			xP = x(P);  xQ = x(Q);  xR = x(R);  xS = x(S);
			xyz_c = xyz_c && ( ( xP * psi_P + xQ * psi_Q + xR * psi_R + xS * psi_S ) / 4. );  }
		assert ( xyz_c .number_of ( tag::components ) == geom_dim );

		this->transf = Function ( tag::immersion, xyz, xi_eta, xyz_c );                        }

	this->base_fun_1 .clear();
	this->base_fun_1 .insert ( std::pair < Cell::Core*, Function >
	       ( P .core, Function ( psi_P/4., tag::composed_with, this->transf ) ) );
	this->base_fun_1 .insert ( std::pair < Cell::Core*, Function >
	       ( Q .core, Function ( psi_Q/4., tag::composed_with, this->transf ) ) );
	this->base_fun_1 .insert ( std::pair < Cell::Core*, Function >
	       ( R .core, Function ( psi_R/4., tag::composed_with, this->transf ) ) );
	this->base_fun_1 .insert ( std::pair < Cell::Core*, Function >
	       ( S .core, Function ( psi_S/4., tag::composed_with, this->transf ) ) );

	// the only use of composing is to allow the calling code to
	// differentiate base functions with respect to geometric coordinates
	// and anyway this only works for a diffeomorphism, not for an immersion
	
}  // end of  FiniteElement::WithMaster::Quadrangle::dock_on

//------------------------------------------------------------------------------------------------------//


#ifndef MANIFEM_NO_QUOTIENT

void FiniteElement::WithMaster::Quadrangle::Q1::dock_on ( const Cell & cll, const tag::Winding & )
// virtual from FiniteElement::Core

{	assert ( cll .dim() == 2 );
	this->docked_on = cll;

	Mesh::Iterator it = cll .boundary() .iterator ( tag::over_vertices, tag::require_order );
	it. reset();  assert ( it .in_range() );  Cell P = *it;
	Cell PQ = cll .boundary() .cell_in_front_of ( P );
	it++;  assert ( it .in_range() );  Cell Q = *it;
	Cell QR = cll .boundary() .cell_in_front_of ( Q );
	it++;  assert ( it .in_range() );  Cell R = *it;
	Cell RS = cll .boundary() .cell_in_front_of ( R );
	it++;  assert ( it .in_range() );  Cell S = *it;
	#ifndef NDEBUG  // DEBUG mode
	Cell SP = cll .boundary() .cell_in_front_of ( S );
	it++;  assert ( not it .in_range() );
	#endif

	Function::Action winding_Q = PQ .winding(),
	                 winding_R = winding_Q + QR .winding(),
	                 winding_S = winding_R + RS .winding();
	assert ( winding_S + SP .winding() == 0 );
	
	Function xi_eta = this->master_manif .coordinates();
	assert ( xi_eta .number_of ( tag::components ) == 2 );
	Function xi = xi_eta [0], eta = xi_eta [1];

	Function psi_P = (1.-xi) * (1.-eta), psi_Q = (1.+xi) * (1.-eta),
	         psi_R = (1.+xi) * (1.+eta), psi_S = (1.-xi) * (1.+eta);
	// psi_* are a base of functions defined on the master element
	// each takes value 4 in the associated vertex, zero on the others
	// we may need to differentiate them with respect to x and y (physical coordinates)
	// which is equivalent to differentiating x_c and y_c with respect to xi and eta
	// and then taking the inverse matrix
	
	Function xyz = Manifold::working .coordinates();
	const size_t geom_dim = xyz .number_of ( tag::components );
	assert ( geom_dim >= 2 );

	if ( geom_dim == 2 )

	{	const std::vector < double > xyz_P = xyz ( P ),
		                             xyz_Q = xyz ( Q, tag::winding, winding_Q ),
		                             xyz_R = xyz ( R, tag::winding, winding_R ),
		                             xyz_S = xyz ( S, tag::winding, winding_S );

		Function x_c = ( xyz_P[0] * psi_P + xyz_Q[0] * psi_Q +
	                   xyz_R[0] * psi_R + xyz_S[0] * psi_S ) / 4.;
		Function y_c = ( xyz_P[1] * psi_P + xyz_Q[1] * psi_Q +
	                   xyz_R[1] * psi_R + xyz_S[1] * psi_S ) / 4.;

	 this->transf =
		 Function ( tag::diffeomorphism, tag::high_dim, xyz, xi_eta, x_c && y_c );   }

	else  // geometric dimension >= 3
		
	{	Function x = xyz [0];
		double xP = x (P),
		       xQ = x ( Q, tag::winding, winding_Q ),
		       xR = x ( R, tag::winding, winding_R ),
		       xS = x ( S, tag::winding, winding_S );
		Function xyz_c = ( xP * psi_P + xQ * psi_Q + xR * psi_R + xS * psi_S ) / 4.;

		for ( size_t d = 1; d < geom_dim; d++ )
		{	x = xyz [d];
			xP = x (P);
			xQ = x ( Q, tag::winding, winding_Q );
			xR = x ( R, tag::winding, winding_R );
			xS = x ( S, tag::winding, winding_S );
			xyz_c = xyz_c && ( ( xP * psi_P + xQ * psi_Q + xR * psi_R + xS * psi_S ) / 4. );  }
		assert ( xyz_c .number_of ( tag::components ) == geom_dim );

		this->transf = Function ( tag::immersion, xyz, xi_eta, xyz_c );                        }

	this->base_fun_1 .clear();
	this->base_fun_1 .insert ( std::pair < Cell::Core*, Function >
	       ( P .core, Function ( psi_P/4., tag::composed_with, this->transf ) ) );
	this->base_fun_1 .insert ( std::pair < Cell::Core*, Function >
	       ( Q .core, Function ( psi_Q/4., tag::composed_with, this->transf ) ) );
	this->base_fun_1 .insert ( std::pair < Cell::Core*, Function >
	       ( R .core, Function ( psi_R/4., tag::composed_with, this->transf ) ) );
	this->base_fun_1 .insert ( std::pair < Cell::Core*, Function >
	       ( S .core, Function ( psi_S/4., tag::composed_with, this->transf ) ) );

	// the only use of composing is to allow the calling code to
	// differentiate base functions with respect to geometric coordinates
	// and anyway this only works for a diffeomorphism, not for an immersion

}  // end of  FiniteElement::WithMaster::Quadrangle::Q1::dock_on  with tag::winding

#endif  // ifndef MANIFEM_NO_QUOTIENT

//------------------------------------------------------------------------------------------------------//


void FiniteElement::WithMaster::Quadrangle::Q2::Straight::dock_on ( const Cell & cll )
// virtual from FiniteElement::Core

{	assert ( cll .dim() == 2 );
	this->docked_on = cll;

	Mesh::Iterator it = cll .boundary() .iterator ( tag::over_vertices, tag::require_order );
	it. reset();  assert ( it .in_range() );  Cell P = *it;
	Cell PQ = cll .boundary() .cell_in_front_of ( P );
	it++;  assert ( it .in_range() );  Cell Q = *it;
	Cell QR = cll .boundary() .cell_in_front_of ( Q );
	it++;  assert ( it .in_range() );  Cell R = *it;
	Cell RS = cll .boundary() .cell_in_front_of ( R );
	it++;  assert ( it .in_range() );  Cell S = *it;
	Cell SP = cll .boundary() .cell_in_front_of ( S );
	it++;  assert ( not it .in_range() );
	
	Function xi_eta = this->master_manif .coordinates();
	assert ( xi_eta .number_of ( tag::components ) == 2 );
	Function xi = xi_eta [0], eta = xi_eta [1];

	Function psi_P = (1.-xi) * (1.-eta), psi_Q = (1.+xi) * (1.-eta),
	         psi_R = (1.+xi) * (1.+eta), psi_S = (1.-xi) * (1.+eta);
	// psi_* are a base of functions defined on the master element
	// each takes value 4 in the associated vertex, zero on the others
	// we may need to differentiate them with respect to x and y (physical coordinates)
	// which is equivalent to differentiating x_c and y_c with respect to xi and eta
	// and then taking the inverse matrix
	
	Function xyz = Manifold::working .coordinates();
	const size_t geom_dim = xyz .number_of ( tag::components );
	assert ( geom_dim >= 2 );

	if ( geom_dim == 2 )

	{	Function x = xyz [0],  y = xyz [1];
		const double xP = x (P), xQ = x (Q), xR = x (R), xS = x (S);
		const double yP = y (P), yQ = y (Q), yR = y (R), yS = y (S);

		Function x_c = ( xP * psi_P + xQ * psi_Q + xR * psi_R + xS * psi_S ) / 4.;
		Function y_c = ( yP * psi_P + yQ * psi_Q + yR * psi_R + yS * psi_S ) / 4.;

		this->transf =
			Function ( tag::diffeomorphism, tag::high_dim, xyz, xi_eta, x_c && y_c );  }

	else  // geometric dimension >= 3
		
	{	Function x = xyz [0];
		double xP = x (P), xQ = x (Q), xR = x (R), xS = x (S);
		Function xyz_c = ( xP * psi_P + xQ * psi_Q + xR * psi_R + xS * psi_S ) / 4.;

		for ( size_t d = 1; d < geom_dim; d++ )
		{	x = xyz [d];
			xP = x (P);  xQ = x (Q);  xR = x (R);  xS = x (S);
			xyz_c = xyz_c && ( ( xP * psi_P + xQ * psi_Q + xR * psi_R + xS * psi_S ) / 4. );  }
		assert ( xyz_c .number_of ( tag::components ) == geom_dim );

		this->transf = Function ( tag::immersion, xyz, xi_eta, xyz_c );                        }

	this->base_fun_1 .clear();
	this->base_fun_1 .insert ( std::pair < Cell::Core*, Function >
	       ( P .core, Function ( psi_P*xi*eta/4., tag::composed_with, this->transf ) ) );
	this->base_fun_1 .insert ( std::pair < Cell::Core*, Function >
	       ( Q .core, Function ( -psi_Q*xi*eta/4., tag::composed_with, this->transf ) ) );
	this->base_fun_1 .insert ( std::pair < Cell::Core*, Function >
	       ( R .core, Function ( psi_R*xi*eta/4., tag::composed_with, this->transf ) ) );
	this->base_fun_1 .insert ( std::pair < Cell::Core*, Function >
	       ( S .core, Function ( -psi_S*xi*eta/4., tag::composed_with, this->transf ) ) );
	this->base_fun_1 .insert ( std::pair < Cell::Core*, Function >
	       ( PQ .core, Function ( -(1.-xi)*(1.+xi)*eta*(1.-eta)/2.,
                               tag::composed_with, this->transf ) ) );
	this->base_fun_1 .insert ( std::pair < Cell::Core*, Function >
	       ( QR .core, Function ( (1.+xi)*xi*(1.-eta)*(1.+eta)/2.,
                               tag::composed_with, this->transf ) ) );
	this->base_fun_1 .insert ( std::pair < Cell::Core*, Function >
	       ( RS .core, Function ( (1.-xi)*(1.+xi)*(1.+eta)*eta/2.,
                               tag::composed_with, this->transf ) ) );
	this->base_fun_1 .insert ( std::pair < Cell::Core*, Function >
	       ( SP .core, Function ( -xi*(1.-xi)*(1.-eta)*(1.+eta)/2.,
                               tag::composed_with, this->transf ) ) );
	this->base_fun_1 .insert ( std::pair < Cell::Core*, Function >
	       ( cll .core, Function ( (1.-xi)*(1.+xi)*(1.-eta)*(1.+eta),
                               tag::composed_with, this->transf  ) ) );

	// the only use of composing is to allow the calling code to
	// differentiate base functions with respect to geometric coordinates
	// and anyway this only works for a diffeomorphism, not for an immersion
	
}  // end of  FiniteElement::WithMaster::Quadrangle::Q2::Straight::dock_on

//------------------------------------------------------------------------------------------------------//


#ifndef MANIFEM_NO_QUOTIENT

void FiniteElement::WithMaster::Quadrangle::Q2::Straight::dock_on
( const Cell & cll, const tag::Winding & ) // virtual from FiniteElement::Core

{	assert ( cll .dim() == 2 );
	this->docked_on = cll;

	Mesh::Iterator it = cll .boundary() .iterator ( tag::over_vertices, tag::require_order );
	it. reset();  assert ( it .in_range() );  Cell P = *it;
	Cell PQ = cll .boundary() .cell_in_front_of ( P );
	it++;  assert ( it .in_range() );  Cell Q = *it;
	Cell QR = cll .boundary() .cell_in_front_of ( Q );
	it++;  assert ( it .in_range() );  Cell R = *it;
	Cell RS = cll .boundary() .cell_in_front_of ( R );
	it++;  assert ( it .in_range() );  Cell S = *it;
	Cell SP = cll .boundary() .cell_in_front_of ( S );
	it++;  assert ( not it .in_range() );

	Function::Action winding_Q = PQ .winding(),
	                 winding_R = winding_Q + QR .winding(),
	                 winding_S = winding_R + RS .winding();
	assert ( winding_S + SP .winding() == 0 );
	
	Function xi_eta = this->master_manif .coordinates();
	assert ( xi_eta .number_of ( tag::components ) == 2 );
	Function xi = xi_eta [0], eta = xi_eta [1];

	Function psi_P = (1.-xi) * (1.-eta), psi_Q = (1.+xi) * (1.-eta),
	         psi_R = (1.+xi) * (1.+eta), psi_S = (1.-xi) * (1.+eta);
	// psi_* are a base of functions defined on the master element
	// each takes value 4 in the associated vertex, zero on the others
	// we may need to differentiate them with respect to x and y (physical coordinates)
	// which is equivalent to differentiating x_c and y_c with respect to xi and eta
	// and then taking the inverse matrix
	
	Function xyz = Manifold::working .coordinates();
	const size_t geom_dim = xyz .number_of ( tag::components );
	assert ( geom_dim >= 2 );

	if ( geom_dim == 2 )

	{	const std::vector < double > xyz_P = xyz ( P ),
		                             xyz_Q = xyz ( Q, tag::winding, winding_Q ),
		                             xyz_R = xyz ( R, tag::winding, winding_R ),
		                             xyz_S = xyz ( S, tag::winding, winding_S );

		Function x_c = ( xyz_P[0] * psi_P + xyz_Q[0] * psi_Q +
	                   xyz_R[0] * psi_R + xyz_S[0] * psi_S ) / 4.;
		Function y_c = ( xyz_P[1] * psi_P + xyz_Q[1] * psi_Q +
	                   xyz_R[1] * psi_R + xyz_S[1] * psi_S ) / 4.;

	 this->transf =
		 Function ( tag::diffeomorphism, tag::high_dim, xyz, xi_eta, x_c && y_c );   }

	else  // geometric dimension >= 3
		
	{	Function x = xyz [0];
		double xP = x (P),
		       xQ = x ( Q, tag::winding, winding_Q ),
		       xR = x ( R, tag::winding, winding_R ),
		       xS = x ( S, tag::winding, winding_S );
		Function xyz_c = ( xP * psi_P + xQ * psi_Q + xR * psi_R + xS * psi_S ) / 4.;

		for ( size_t d = 1; d < geom_dim; d++ )
		{	x = xyz [d];
			xP = x (P);
			xQ = x ( Q, tag::winding, winding_Q );
			xR = x ( R, tag::winding, winding_R );
			xS = x ( S, tag::winding, winding_S );
			xyz_c = xyz_c && ( ( xP * psi_P + xQ * psi_Q + xR * psi_R + xS * psi_S ) / 4. );  }
		assert ( xyz_c .number_of ( tag::components ) == geom_dim );

		this->transf = Function ( tag::immersion, xyz, xi_eta, xyz_c );                        }

	this->base_fun_1 .clear();
	this->base_fun_1 .insert ( std::pair < Cell::Core*, Function >
	       ( P .core, Function ( psi_P*xi*eta/4., tag::composed_with, this->transf ) ) );
	this->base_fun_1 .insert ( std::pair < Cell::Core*, Function >
	       ( Q .core, Function ( -psi_Q*xi*eta/4., tag::composed_with, this->transf ) ) );
	this->base_fun_1 .insert ( std::pair < Cell::Core*, Function >
	       ( R .core, Function ( psi_R*xi*eta/4., tag::composed_with, this->transf ) ) );
	this->base_fun_1 .insert ( std::pair < Cell::Core*, Function >
	       ( S .core, Function ( -psi_S*xi*eta/4., tag::composed_with, this->transf ) ) );
	this->base_fun_1 .insert ( std::pair < Cell::Core*, Function >
	       ( PQ .core, Function ( -(1.-xi)*(1.+xi)*eta*(1.-eta)/2.,
                               tag::composed_with, this->transf ) ) );
	this->base_fun_1 .insert ( std::pair < Cell::Core*, Function >
	       ( QR .core, Function ( (1.+xi)*xi*(1.-eta)*(1.+eta)/2.,
                               tag::composed_with, this->transf ) ) );
	this->base_fun_1 .insert ( std::pair < Cell::Core*, Function >
	       ( RS .core, Function ( (1.-xi)*(1.+xi)*(1.+eta)*eta/2.,
                               tag::composed_with, this->transf ) ) );
	this->base_fun_1 .insert ( std::pair < Cell::Core*, Function >
	       ( SP .core, Function ( -xi*(1.-xi)*(1.-eta)*(1.+eta)/2.,
                               tag::composed_with, this->transf ) ) );
	this->base_fun_1 .insert ( std::pair < Cell::Core*, Function >
	       ( cll .core, Function ( (1.-xi)*(1.+xi)*(1.-eta)*(1.+eta),
                               tag::composed_with, this->transf  ) ) );
	
	// the only use of composing is to allow the calling code to
	// differentiate base functions with respect to geometric coordinates
	// and anyway this only works for a diffeomorphism, not for an immersion

}  // end of  FiniteElement::WithMaster::Quadrangle::Q2::Straight::dock_on  with tag::winding

#endif  // ifndef MANIFEM_NO_QUOTIENT

//------------------------------------------------------------------------------------------------------//


void FiniteElement::WithMaster::Quadrangle::Q2::Straight::Incremental::dock_on ( const Cell & cll )
// virtual from FiniteElement::Core

{	assert ( cll .dim() == 2 );
	this->docked_on = cll;

	Mesh::Iterator it = cll .boundary() .iterator ( tag::over_vertices, tag::require_order );
	it. reset();  assert ( it .in_range() );  Cell P = *it;
	Cell PQ = cll .boundary() .cell_in_front_of ( P );
	it++;  assert ( it .in_range() );  Cell Q = *it;
	Cell QR = cll .boundary() .cell_in_front_of ( Q );
	it++;  assert ( it .in_range() );  Cell R = *it;
	Cell RS = cll .boundary() .cell_in_front_of ( R );
	it++;  assert ( it .in_range() );  Cell S = *it;
	Cell SP = cll .boundary() .cell_in_front_of ( S );
	it++;  assert ( not it .in_range() );
	
	Function xi_eta = this->master_manif .coordinates();
	assert ( xi_eta .number_of ( tag::components ) == 2 );
	Function xi = xi_eta [0], eta = xi_eta [1];

	Function psi_P = (1.-xi) * (1.-eta), psi_Q = (1.+xi) * (1.-eta),
	         psi_R = (1.+xi) * (1.+eta), psi_S = (1.-xi) * (1.+eta);
	// psi* are a base of functions defined on the master element
	// each takes value 4 in the associated vertex, zero on the others
	// we may need to differentiate them with respect to x and y (physical coordinates)
	// which is equivalent to differentiating x_c and y_c with respect to xi and eta
	// and then taking the inverse matrix
	
	Function xyz = Manifold::working .coordinates();
	const size_t geom_dim = xyz .number_of ( tag::components );
	assert ( geom_dim >= 2 );

	if ( geom_dim == 2 )

	{	Function x = xyz [0],  y = xyz [1];
		const double xP = x (P), xQ = x (Q), xR = x (R), xS = x (S);
		const double yP = y (P), yQ = y (Q), yR = y (R), yS = y (S);

		Function x_c = ( xP * psi_P + xQ * psi_Q + xR * psi_R + xS * psi_S ) / 4.;
		Function y_c = ( yP * psi_P + yQ * psi_Q + yR * psi_R + yS * psi_S ) / 4.;

		this->transf =
			Function ( tag::diffeomorphism, tag::high_dim, xyz, xi_eta, x_c && y_c );  }

	else  // geometric dimension >= 3
		
	{	Function x = xyz [0];
		double xP = x (P), xQ = x (Q), xR = x (R), xS = x (S);
		Function xyz_c = ( xP * psi_P + xQ * psi_Q + xR * psi_R + xS * psi_S ) / 4.;

		for ( size_t d = 1; d < geom_dim; d++ )
		{	x = xyz [d];
			xP = x (P);  xQ = x (Q);  xR = x (R);  xS = x (S);
			xyz_c = xyz_c && ( ( xP * psi_P + xQ * psi_Q + xR * psi_R + xS * psi_S ) / 4. );  }
		assert ( xyz_c .number_of ( tag::components ) == geom_dim );

		this->transf = Function ( tag::immersion, xyz, xi_eta, xyz_c );                        }

	this->base_fun_1 .clear();
	this->base_fun_1 .insert ( std::pair < Cell::Core*, Function >
	       ( P.core, Function ( psi_P/4., tag::composed_with, this->transf ) ) );
	this->base_fun_1 .insert ( std::pair < Cell::Core*, Function >
	       ( Q.core, Function ( psi_Q/4., tag::composed_with, this->transf ) ) );
	this->base_fun_1 .insert ( std::pair < Cell::Core*, Function >
	       ( R.core, Function ( psi_R/4., tag::composed_with, this->transf ) ) );
	this->base_fun_1 .insert ( std::pair < Cell::Core*, Function >
	       ( S.core, Function ( psi_S/4., tag::composed_with, this->transf ) ) );
	this->base_fun_1 .insert ( std::pair < Cell::Core*, Function >
	       ( PQ.core, Function ( (1.-xi)*(1.+xi)*(1.-eta),
                               tag::composed_with, this->transf ) ) );
	this->base_fun_1 .insert ( std::pair < Cell::Core*, Function >
	       ( QR.core, Function ( (1.+xi)*(1.-eta)*(1.+eta),
                               tag::composed_with, this->transf ) ) );
	this->base_fun_1 .insert ( std::pair < Cell::Core*, Function >
	       ( RS.core, Function ( (1.-xi)*(1.+xi)*(1.+eta),
                               tag::composed_with, this->transf ) ) );
	this->base_fun_1 .insert ( std::pair < Cell::Core*, Function >
	       ( SP.core, Function ( (1.-xi)*(1.-eta)*(1.+eta),
                               tag::composed_with, this->transf ) ) );
	this->base_fun_1 .insert ( std::pair < Cell::Core*, Function >
	       ( cll.core, Function ( (1.-xi)*(1.+xi)*(1.-eta)*(1.+eta),
                                tag::composed_with, this->transf  ) ) );

	// the only use of composing is to allow the calling code to
	// differentiate base functions with respect to geometric coordinates
	// and anyway this only works for a diffeomorphism, not for an immersion
	
}  // end of  FiniteElement::WithMaster::Quadrangle::Q2::Straight::Incremental::dock_on

//------------------------------------------------------------------------------------------------------//


#ifndef MANIFEM_NO_QUOTIENT

void FiniteElement::WithMaster::Quadrangle::Q2::Straight::Incremental::dock_on
( const Cell & cll, const tag::Winding & ) // virtual from FiniteElement::Core

{	assert ( cll .dim() == 2 );
	this->docked_on = cll;

	Mesh::Iterator it = cll .boundary() .iterator ( tag::over_vertices, tag::require_order );
	it. reset();  assert ( it .in_range() );  Cell P = *it;
	Cell PQ = cll .boundary() .cell_in_front_of ( P );
	it++;  assert ( it .in_range() );  Cell Q = *it;
	Cell QR = cll .boundary() .cell_in_front_of ( Q );
	it++;  assert ( it .in_range() );  Cell R = *it;
	Cell RS = cll .boundary() .cell_in_front_of ( R );
	it++;  assert ( it .in_range() );  Cell S = *it;
	Cell SP = cll .boundary() .cell_in_front_of ( S );
	it++;  assert ( not it .in_range() );

	Function::Action winding_Q = PQ .winding(),
	                 winding_R = winding_Q + QR .winding(),
	                 winding_S = winding_R + RS .winding();
	assert ( winding_S + SP .winding() == 0 );
	
	Function xi_eta = this->master_manif .coordinates();
	assert ( xi_eta .number_of ( tag::components ) == 2 );
	Function xi = xi_eta [0], eta = xi_eta [1];

	Function psi_P = (1.-xi) * (1.-eta), psi_Q = (1.+xi) * (1.-eta),
	         psi_R = (1.+xi) * (1.+eta), psi_S = (1.-xi) * (1.+eta);
	// psi_* are a base of functions defined on the master element
	// each takes value 4 in the associated vertex, zero on the others
	// we may need to differentiate them with respect to x and y (physical coordinates)
	// which is equivalent to differentiating x_c and y_c with respect to xi and eta
	// and then taking the inverse matrix
	
	Function xyz = Manifold::working .coordinates();
	const size_t geom_dim = xyz .number_of ( tag::components );
	assert ( geom_dim >= 2 );

	if ( geom_dim == 2 )

	{	const std::vector < double > xyz_P = xyz ( P ),
		                             xyz_Q = xyz ( Q, tag::winding, winding_Q ),
		                             xyz_R = xyz ( R, tag::winding, winding_R ),
		                             xyz_S = xyz ( S, tag::winding, winding_S );

		Function x_c = ( xyz_P[0] * psi_P + xyz_Q[0] * psi_Q +
	                   xyz_R[0] * psi_R + xyz_S[0] * psi_S ) / 4.;
		Function y_c = ( xyz_P[1] * psi_P + xyz_Q[1] * psi_Q +
	                   xyz_R[1] * psi_R + xyz_S[1] * psi_S ) / 4.;

	 this->transf =
		 Function ( tag::diffeomorphism, tag::high_dim, xyz, xi_eta, x_c && y_c );   }

	else  // geometric dimension >= 3
		
	{	Function x = xyz [0];
		double xP = x (P),
		       xQ = x ( Q, tag::winding, winding_Q ),
		       xR = x ( R, tag::winding, winding_R ),
		       xS = x ( S, tag::winding, winding_S );
		Function xyz_c = ( xP * psi_P + xQ * psi_Q + xR * psi_R + xS * psi_S ) / 4.;

		for ( size_t d = 1; d < geom_dim; d++ )
		{	x = xyz [d];
			xP = x (P);
			xQ = x ( Q, tag::winding, winding_Q );
			xR = x ( R, tag::winding, winding_R );
			xS = x ( S, tag::winding, winding_S );
			xyz_c = xyz_c && ( ( xP * psi_P + xQ * psi_Q + xR * psi_R + xS * psi_S ) / 4. );  }
		assert ( xyz_c .number_of ( tag::components ) == geom_dim );

		this->transf = Function ( tag::immersion, xyz, xi_eta, xyz_c );                        }

	this->base_fun_1 .clear();
	this->base_fun_1 .insert ( std::pair < Cell::Core*, Function >
	       ( P.core, Function ( psi_P/4., tag::composed_with, this->transf ) ) );
	this->base_fun_1 .insert ( std::pair < Cell::Core*, Function >
	       ( Q.core, Function ( psi_Q/4., tag::composed_with, this->transf ) ) );
	this->base_fun_1 .insert ( std::pair < Cell::Core*, Function >
	       ( R.core, Function ( psi_R/4., tag::composed_with, this->transf ) ) );
	this->base_fun_1 .insert ( std::pair < Cell::Core*, Function >
	       ( S.core, Function ( psi_S/4., tag::composed_with, this->transf ) ) );
	this->base_fun_1 .insert ( std::pair < Cell::Core*, Function >
	       ( PQ.core, Function ( (1.-xi)*(1.+xi)*(1.-eta),
                               tag::composed_with, this->transf ) ) );
	this->base_fun_1 .insert ( std::pair < Cell::Core*, Function >
	       ( QR.core, Function ( (1.+xi)*(1.-eta)*(1.+eta),
                               tag::composed_with, this->transf ) ) );
	this->base_fun_1 .insert ( std::pair < Cell::Core*, Function >
	       ( RS.core, Function ( (1.-xi)*(1.+xi)*(1.+eta),
                               tag::composed_with, this->transf ) ) );
	this->base_fun_1 .insert ( std::pair < Cell::Core*, Function >
	       ( SP.core, Function ( (1.-xi)*(1.-eta)*(1.+eta),
                               tag::composed_with, this->transf ) ) );
	this->base_fun_1 .insert ( std::pair < Cell::Core*, Function >
	       ( cll.core, Function ( (1.-xi)*(1.+xi)*(1.-eta)*(1.+eta),
                                tag::composed_with, this->transf  ) ) );
	
	// the only use of composing is to allow the calling code to
	// differentiate base functions with respect to geometric coordinates
	// and anyway this only works for a diffeomorphism, not for an immersion

}  // end of  FiniteElement::WithMaster::Quadrangle::Q2::Straight::Incremental::dock_on
   //         with tag::winding

#endif  // ifndef MANIFEM_NO_QUOTIENT

//------------------------------------------------------------------------------------------------------//


void FiniteElement::WithMaster::Quadrangle::Q2::Curved::dock_on ( const Cell & cll )
// virtual from FiniteElement::Core

{	assert ( cll .dim() == 2 );
	assert ( false );            }

//------------------------------------------------------------------------------------------------------//


void FiniteElement::WithMaster::Quadrangle::Q2::Curved::dock_on ( const Cell & cll, const tag::Winding & )

{	assert ( cll .dim() == 2 );
	assert ( false );            }

//------------------------------------------------------------------------------------------------------//


void FiniteElement::WithMaster::Tetrahedron::P1::dock_on ( const Cell & cll )
// virtual from FiniteElement::Core

{	assert ( cll .dim() == 3 );
	this->docked_on = cll;

	Mesh::Iterator it_faces = cll .boundary() .iterator
			( tag::over_cells_of_max_dim, tag::orientation_compatible_with_mesh );
	it_faces .reset();  assert ( it_faces .in_range() );  Cell QRS = * it_faces;

	Mesh::Iterator it = QRS .boundary() .iterator ( tag::over_vertices, tag::require_order );
	it .reset();  assert ( it .in_range() );  Cell Q = *it;
	it++;  assert ( it .in_range() );  Cell R = *it;
	it++;  assert ( it .in_range() );  Cell S = *it;
	it++;  assert ( not it .in_range() );

	Cell QR = QRS .boundary() .cell_behind ( R, tag::surely_exists );
	Cell PRQ = cll .boundary() .cell_in_front_of ( QR, tag::surely_exists );
	Cell QP = PRQ .boundary() .cell_in_front_of ( Q, tag::surely_exists );
	Cell P = QP .tip();
	// PQRS are 1234 in Figure 6.2 in the book
	// E.B. Becker, G.F. Carey, J.T. Oden, Finite Elements, an introduction, vol 1
	
	Function xi_eta_zeta = this->master_manif .coordinates();
	assert ( xi_eta_zeta .number_of ( tag::components ) == 3 );
	Function xi = xi_eta_zeta [0], eta = xi_eta_zeta [1], zeta = xi_eta_zeta[2];
	Function one_m_xi_m_eta_m_zeta = 1. - xi - eta - zeta;

	// xi, eta, zeta and 1-xi-eta-zeta are a base of functions defined on the master element
	// we may need to differentiate them with respect to x, y and z (physical coordinates)
	// which is equivalent to differentiating x_c, y_c and z_c with respect to xi, eta and zeta
	// and then taking the inverse matrix

	Function xyz = Manifold::working .coordinates();
	assert ( xyz .number_of ( tag::components ) == 3 );

	Function x = xyz [0],  y = xyz [1], z = xyz [2];
	double xP = x (P), xQ = x (Q), xR = x (R), xS = x (S);
	double yP = y (P), yQ = y (Q), yR = y (R), yS = y (S);
	double zP = z (P), zQ = z (Q), zR = z (R), zS = z (S);
	
	Function x_c = xP * one_m_xi_m_eta_m_zeta + xQ * xi + xR * eta + xS * zeta;
	Function y_c = yP * one_m_xi_m_eta_m_zeta + yQ * xi + yR * eta + yS * zeta;
	Function z_c = zP * one_m_xi_m_eta_m_zeta + zQ * xi + zR * eta + zS * zeta;
		
	this->transf = Function ( tag::diffeomorphism, tag::high_dim, xyz, xi_eta_zeta, x_c && y_c && z_c );  
	
	this->base_fun_1 .clear();
	this->base_fun_1 .insert ( std::pair < Cell::Core*, Function >
	       ( P .core, Function ( one_m_xi_m_eta_m_zeta, tag::composed_with, this->transf ) ) );
	this->base_fun_1 .insert ( std::pair < Cell::Core*, Function >
	       ( Q .core, Function ( xi, tag::composed_with, this->transf ) ) );
	this->base_fun_1 .insert ( std::pair < Cell::Core*, Function >
	       ( R .core, Function ( eta, tag::composed_with, this->transf ) ) );
	this->base_fun_1 .insert ( std::pair < Cell::Core*, Function >
	       ( S .core, Function ( zeta, tag::composed_with, this->transf ) ) );
		   
	// the only use of composing is to allow the calling code to
	// differentiate base functions with respect to geometric coordinates
	
}  // end of  FiniteElement::WithMaster::Tetrahedron::P1::dock_on 

//------------------------------------------------------------------------------------------------------//


#ifndef MANIFEM_NO_QUOTIENT

void FiniteElement::WithMaster::Tetrahedron::P1::dock_on ( const Cell & cll , const tag::Winding & )

{	assert ( cll .dim() == 3 );
	this->docked_on = cll;

	Mesh::Iterator it_faces = cll .boundary() .iterator
			( tag::over_cells_of_max_dim, tag::orientation_compatible_with_mesh );
	it_faces .reset();  assert ( it_faces .in_range() );  Cell QRS = * it_faces;

	Mesh::Iterator it = QRS .boundary() .iterator ( tag::over_vertices, tag::require_order );
	it .reset();  assert ( it .in_range() );  Cell Q = *it;
	it++;  assert ( it .in_range() );  Cell R = *it;
	it++;  assert ( it .in_range() );  Cell S = *it;
	it++;  assert ( not it .in_range() );
	Cell RS = QRS .boundary() .cell_behind ( S, tag::surely_exists );

	Cell QR = QRS .boundary() .cell_behind ( R, tag::surely_exists );
	Cell PRQ = cll .boundary() .cell_in_front_of ( QR, tag::surely_exists );
	Cell QP = PRQ .boundary() .cell_in_front_of ( Q, tag::surely_exists );
	Cell P = QP .tip();
	// PQRS are 1234 in Figure 6.2 in the book
	// E.B. Becker, G.F. Carey, J.T. Oden, Finite Elements, an introduction, vol 1
	
	Function::Action winding_Q = - QP .winding(),
	                 winding_R = winding_Q + QR .winding(),
	                 winding_S = winding_R + RS .winding();
	  
	Function xi_eta_zeta = this->master_manif .coordinates();
	assert ( xi_eta_zeta .number_of ( tag::components ) == 3 );
	Function xi = xi_eta_zeta [0], eta = xi_eta_zeta [1], zeta = xi_eta_zeta[2];
	Function one_m_xi_m_eta_m_zeta = 1. - xi - eta - zeta;

	// xi, eta, zeta and 1-xi-eta-zeta are a base of functions defined on the master element
	// we may need to differentiate them with respect to x, y and z (physical coordinates)
	// which is equivalent to differentiating x_c, y_c and z_c with respect to xi, eta and zeta
	// and then taking the inverse matrix

	Function xyz = Manifold::working .coordinates();
	assert ( xyz .number_of ( tag::components ) == 3 );

	std::vector < double > xyz_P = xyz ( P ),
	                       xyz_Q = xyz ( Q, tag::winding, winding_Q ),
	                       xyz_R = xyz ( R, tag::winding, winding_R ),
	                       xyz_S = xyz ( S, tag::winding, winding_S );
	
	Function x_c = xyz_P[0] * one_m_xi_m_eta_m_zeta + xyz_Q[0] * xi + xyz_R[0] * eta + xyz_S[0] * zeta;
	Function y_c = xyz_P[1] * one_m_xi_m_eta_m_zeta + xyz_Q[1] * xi + xyz_R[1] * eta + xyz_S[1] * zeta;
	Function z_c = xyz_P[2] * one_m_xi_m_eta_m_zeta + xyz_Q[2] * xi + xyz_R[2] * eta + xyz_S[2] * zeta;
		
	this->transf = Function ( tag::diffeomorphism, tag::high_dim, xyz, xi_eta_zeta, x_c && y_c && z_c );  
	

	this->base_fun_1 .clear();
	this->base_fun_1 .insert ( std::pair < Cell::Core*, Function >
	       ( P .core, Function ( one_m_xi_m_eta_m_zeta, tag::composed_with, this->transf ) ) );
	this->base_fun_1 .insert ( std::pair < Cell::Core*, Function >
	       ( Q .core, Function ( xi, tag::composed_with, this->transf ) ) );
	this->base_fun_1 .insert ( std::pair < Cell::Core*, Function >
	       ( R .core, Function ( eta, tag::composed_with, this->transf ) ) );
	this->base_fun_1 .insert ( std::pair < Cell::Core*, Function >
	       ( S .core, Function ( zeta, tag::composed_with, this->transf ) ) );
		   
	// the only use of composing is to allow the calling code to
	// differentiate base functions with respect to geometric coordinates
	
}  // end of  FiniteElement::WithMaster::Tetrahedron::P1::dock_on  with tag::winding

#endif  // ifndef MANIFEM_NO_QUOTIENT

//------------------------------------------------------------------------------------------------------//


void FiniteElement::WithMaster::Hexahedron::Q1::dock_on ( const Cell & cll )
// virtual from FiniteElement::Core

{	assert ( cll .dim() == 3 );
	this->docked_on = cll;

	// nodes numbered as in figure 6.3 in
	// E.B. Becker, G.F. Carey, J.T. Oden, Finite Elements, an introduction, vol 1
	
	Mesh::Iterator it = cll .boundary() .iterator ( tag::over_cells_of_max_dim );
	it .reset();  assert ( it .in_range() );  Cell face_4321 = * it;
	it = face_4321 .boundary() .iterator ( tag::over_cells_of_max_dim, tag::require_order );
	it .reset();  assert ( it .in_range() );  Cell edge_14 = * it;
	Cell P4 = edge_14 .tip();
	it++;  assert ( it .in_range() );  Cell edge_43 = * it;
	Cell P3 = edge_43 .tip();
	it++;  assert ( it .in_range() );  Cell edge_32 = * it;
	Cell P2 = edge_32 .tip();
	it++;  assert ( it .in_range() );  Cell edge_21 = * it;
	Cell P1 = edge_21 .tip();
	it++;  assert ( not it .in_range() );
	Cell face_2376 = cll .boundary() .cell_in_front_of ( edge_32, tag::surely_exists );
	Cell edge_37 = face_2376 .boundary() .cell_in_front_of ( P3, tag::surely_exists );
	Cell P7 = edge_37 .tip();
	Cell edge_76 = face_2376 .boundary() .cell_in_front_of ( P7, tag::surely_exists );
	Cell face_5678 = cll .boundary() .cell_in_front_of ( edge_76, tag::surely_exists );
	Cell edge_78 = face_5678 .boundary() .cell_in_front_of ( P7, tag::surely_exists );
	Cell P8 = edge_78 .tip();
	Cell edge_85 = face_5678 .boundary() .cell_in_front_of ( P8, tag::surely_exists );
	Cell P5 = edge_85 .tip();
	Cell edge_56 = face_5678 .boundary() .cell_in_front_of ( P5, tag::surely_exists );
	Cell P6 = edge_56 .tip();
	#ifndef NDEBUG  // DEBUG mode
	Cell edge_67 = face_5678 .boundary() .cell_in_front_of ( P6, tag::surely_exists );
	assert ( P7 == edge_67 .tip() );
	#endif	
	
	Function xi_eta_zeta = this->master_manif .coordinates();
	assert ( xi_eta_zeta .number_of ( tag::components ) == 3 );
	Function xi = xi_eta_zeta [0], eta = xi_eta_zeta [1], zeta = xi_eta_zeta [2];

	Function psi_P1 = (1.+xi) * (1.-eta) * (1.-zeta),
	         psi_P2 = (1.+xi) * (1.+eta) * (1.-zeta),
	         psi_P3 = (1.-xi) * (1.+eta) * (1.-zeta),
	         psi_P4 = (1.-xi) * (1.-eta) * (1.-zeta),
	         psi_P5 = (1.+xi) * (1.-eta) * (1.+zeta),
	         psi_P6 = (1.+xi) * (1.+eta) * (1.+zeta),
	         psi_P7 = (1.-xi) * (1.+eta) * (1.+zeta),
	         psi_P8 = (1.-xi) * (1.-eta) * (1.+zeta);
	// psi* are a base of functions defined on the master element
	// each takes value 8 in the associated vertex, zero on the others
	// we may need to differentiate them with respect to x, y and z (physical coordinates)
	// which is equivalent to differentiating x_c, y_c and z_c with respect to xi, eta and zeta
	// and then taking the inverse matrix
	
	Function xyz = Manifold::working .coordinates();
	assert ( xyz .number_of ( tag::components ) == 3 );
	Function x = xyz [0],  y = xyz [1],  z = xyz [2];
	const double xP1 = x (P1), xP2 = x (P2), xP3 = x (P3), xP4 = x (P4);
	const double xP5 = x (P5), xP6 = x (P6), xP7 = x (P7), xP8 = x (P8);
	const double yP1 = y (P1), yP2 = y (P2), yP3 = y (P3), yP4 = y (P4);
	const double yP5 = y (P5), yP6 = y (P6), yP7 = y (P7), yP8 = y (P8);
	const double zP1 = z (P1), zP2 = z (P2), zP3 = z (P3), zP4 = z (P4);
	const double zP5 = z (P5), zP6 = z (P6), zP7 = z (P7), zP8 = z (P8);

	Function x_c = ( xP1 * psi_P1 + xP2 * psi_P2 + xP3 * psi_P3 + xP4 * psi_P4 +
	                 xP5 * psi_P5 + xP6 * psi_P6 + xP7 * psi_P7 + xP8 * psi_P8   ) / 8.;
	Function y_c = ( yP1 * psi_P1 + yP2 * psi_P2 + yP3 * psi_P3 + yP4 * psi_P4 +
	                 yP5 * psi_P5 + yP6 * psi_P6 + yP7 * psi_P7 + yP8 * psi_P8   ) / 8.;
	Function z_c = ( zP1 * psi_P1 + zP2 * psi_P2 + zP3 * psi_P3 + zP4 * psi_P4 +
	                 zP5 * psi_P5 + zP6 * psi_P6 + zP7 * psi_P7 + zP8 * psi_P8   ) / 8.;

	this->transf = Function ( tag::diffeomorphism, tag::high_dim, xyz, xi_eta_zeta, x_c && y_c && z_c );  

	this->base_fun_1 .clear();
	this->base_fun_1 .insert ( std::pair < Cell::Core*, Function >
	       ( P1 .core, Function ( psi_P1 / 8., tag::composed_with, this->transf ) ) );
	this->base_fun_1 .insert ( std::pair < Cell::Core*, Function >
	       ( P2 .core, Function ( psi_P2 / 8., tag::composed_with, this->transf ) ) );
	this->base_fun_1 .insert ( std::pair < Cell::Core*, Function >
	       ( P3 .core, Function ( psi_P3 / 8., tag::composed_with, this->transf ) ) );
	this->base_fun_1 .insert ( std::pair < Cell::Core*, Function >
	       ( P4 .core, Function ( psi_P4 / 8., tag::composed_with, this->transf ) ) );
	this->base_fun_1 .insert ( std::pair < Cell::Core*, Function >
	       ( P5 .core, Function ( psi_P5 / 8., tag::composed_with, this->transf ) ) );
	this->base_fun_1 .insert ( std::pair < Cell::Core*, Function >
	       ( P6 .core, Function ( psi_P6 / 8., tag::composed_with, this->transf ) ) );
	this->base_fun_1 .insert ( std::pair < Cell::Core*, Function >
	       ( P7 .core, Function ( psi_P7 / 8., tag::composed_with, this->transf ) ) );
	this->base_fun_1 .insert ( std::pair < Cell::Core*, Function >
	       ( P8 .core, Function ( psi_P8 / 8., tag::composed_with, this->transf ) ) );

	// the only use of composing is to allow the calling code to
	// differentiate base functions with respect to geometric coordinates
	// and anyway this only works for a diffeomorphism, not for an immersion
	
}  // end of  FiniteElement::WithMaster::Hexahedron::dock_on

//------------------------------------------------------------------------------------------------------//


#ifndef MANIFEM_NO_QUOTIENT

void FiniteElement::WithMaster::Hexahedron::Q1::dock_on ( const Cell & cll, const tag::Winding & )
// virtual from FiniteElement::Core

{	assert ( cll .dim() == 3 );
	this->docked_on = cll;

	// nodes numbered as in figure 6.3 in
	// E.B. Becker, G.F. Carey, J.T. Oden, Finite Elements, an introduction, vol 1
	
	Mesh::Iterator it = cll .boundary() .iterator ( tag::over_cells_of_max_dim );
	it .reset();  assert ( it .in_range() );  Cell face_4321 = * it;
	it = face_4321 .boundary() .iterator ( tag::over_cells_of_max_dim, tag::require_order );
	it .reset();  assert ( it .in_range() );  Cell edge_14 = * it;
	Cell P4 = edge_14 .tip();
	it++;  assert ( it .in_range() );  Cell edge_43 = * it;
	Cell P3 = edge_43 .tip();
	it++;  assert ( it .in_range() );  Cell edge_32 = * it;
	Cell P2 = edge_32 .tip();
	it++;  assert ( it .in_range() );  Cell edge_21 = * it;
	Cell P1 = edge_21 .tip();
	it++;  assert ( not it .in_range() );
	Cell face_2376 = cll .boundary() .cell_in_front_of ( edge_32, tag::surely_exists );
	Cell edge_37 = face_2376 .boundary() .cell_in_front_of ( P3, tag::surely_exists );
	Cell P7 = edge_37 .tip();
	Cell edge_76 = face_2376 .boundary() .cell_in_front_of ( P7, tag::surely_exists );
	Cell face_5678 = cll .boundary() .cell_in_front_of ( edge_76, tag::surely_exists );
	Cell edge_78 = face_5678 .boundary() .cell_in_front_of ( P7, tag::surely_exists );
	Cell P8 = edge_78 .tip();
	Cell edge_85 = face_5678 .boundary() .cell_in_front_of ( P8, tag::surely_exists );
	Cell P5 = edge_85 .tip();
	Cell edge_56 = face_5678 .boundary() .cell_in_front_of ( P5, tag::surely_exists );
	Cell P6 = edge_56 .tip();
	#ifndef NDEBUG  // DEBUG mode
	Cell edge_67 = face_5678 .boundary() .cell_in_front_of ( P6, tag::surely_exists );
	assert ( P7 == edge_67 .tip() );
	#endif	

	// take P1 as origin
	Function::Action winding_2 =           - edge_21 .winding(),
	                 winding_3 = winding_2 - edge_32 .winding(),
	                 winding_4 = winding_3 - edge_43 .winding(),
	                 winding_7 = winding_3 + edge_37 .winding(),
	                 winding_8 = winding_7 + edge_78 .winding(),
	                 winding_5 = winding_8 + edge_85 .winding(),
	                 winding_6 = winding_5 + edge_56 .winding();
	assert ( winding_6 + edge_67 .winding() == winding_7 );
	assert ( winding_4 == edge_14 .winding() );
	
	Function xi_eta_zeta = this->master_manif .coordinates();
	assert ( xi_eta_zeta .number_of ( tag::components ) == 3 );
	Function xi = xi_eta_zeta [0], eta = xi_eta_zeta [1], zeta = xi_eta_zeta [2];

	Function psi_P1 = (1.+xi) * (1.-eta) * (1.-zeta),
	         psi_P2 = (1.+xi) * (1.+eta) * (1.-zeta),
	         psi_P3 = (1.-xi) * (1.+eta) * (1.-zeta),
	         psi_P4 = (1.-xi) * (1.-eta) * (1.-zeta),
	         psi_P5 = (1.+xi) * (1.-eta) * (1.+zeta),
	         psi_P6 = (1.+xi) * (1.+eta) * (1.+zeta),
	         psi_P7 = (1.-xi) * (1.+eta) * (1.+zeta),
	         psi_P8 = (1.-xi) * (1.-eta) * (1.+zeta);
	// psi* are a base of functions defined on the master element
	// each takes value 8 in the associated vertex, zero on the others
	// we may need to differentiate them with respect to x, y and z (physical coordinates)
	// which is equivalent to differentiating x_c, y_c and z_c with respect to xi, eta and zeta
	// and then taking the inverse matrix
	
	Function xyz = Manifold::working .coordinates();
	assert ( xyz .number_of ( tag::components ) == 3 );
	const std::vector < double > xyz_P1 = xyz ( P1 ),
	                             xyz_P2 = xyz ( P2, tag::winding, winding_2 ),
	                             xyz_P3 = xyz ( P3, tag::winding, winding_3 ),
	                             xyz_P4 = xyz ( P4, tag::winding, winding_4 ),
	                             xyz_P5 = xyz ( P5, tag::winding, winding_5 ),
	                             xyz_P6 = xyz ( P6, tag::winding, winding_6 ),
	                             xyz_P7 = xyz ( P7, tag::winding, winding_7 ),
	                             xyz_P8 = xyz ( P8, tag::winding, winding_8 );

	Function x_c = ( xyz_P1 [0] * psi_P1 + xyz_P2 [0] * psi_P2 +
	                 xyz_P3 [0] * psi_P3 + xyz_P4 [0] * psi_P4 +
	                 xyz_P5 [0] * psi_P5 + xyz_P6 [0] * psi_P6 +
	                 xyz_P7 [0] * psi_P7 + xyz_P8 [0] * psi_P8   );
	Function y_c = ( xyz_P1 [1] * psi_P1 + xyz_P2 [1] * psi_P2 +
	                 xyz_P3 [1] * psi_P3 + xyz_P4 [1] * psi_P4 +
	                 xyz_P5 [1] * psi_P5 + xyz_P6 [1] * psi_P6 +
	                 xyz_P7 [1] * psi_P7 + xyz_P8 [1] * psi_P8   );
	Function z_c = ( xyz_P1 [2] * psi_P1 + xyz_P2 [2] * psi_P2 +
	                 xyz_P3 [2] * psi_P3 + xyz_P4 [2] * psi_P4 +
	                 xyz_P5 [2] * psi_P5 + xyz_P6 [2] * psi_P6 +
	                 xyz_P7 [2] * psi_P7 + xyz_P8 [2] * psi_P8   );

	this->transf = Function ( tag::diffeomorphism, tag::high_dim, xyz, xi_eta_zeta, x_c && y_c && z_c );  

	this->base_fun_1 .clear();
	this->base_fun_1 .insert ( std::pair < Cell::Core*, Function >
	       ( P1 .core, Function ( psi_P1 / 8., tag::composed_with, this->transf ) ) );
	this->base_fun_1 .insert ( std::pair < Cell::Core*, Function >
	       ( P2 .core, Function ( psi_P2 / 8., tag::composed_with, this->transf ) ) );
	this->base_fun_1 .insert ( std::pair < Cell::Core*, Function >
	       ( P3 .core, Function ( psi_P3 / 8., tag::composed_with, this->transf ) ) );
	this->base_fun_1 .insert ( std::pair < Cell::Core*, Function >
	       ( P4 .core, Function ( psi_P4 / 8., tag::composed_with, this->transf ) ) );
	this->base_fun_1 .insert ( std::pair < Cell::Core*, Function >
	       ( P5 .core, Function ( psi_P5 / 8., tag::composed_with, this->transf ) ) );
	this->base_fun_1 .insert ( std::pair < Cell::Core*, Function >
	       ( P6 .core, Function ( psi_P6 / 8., tag::composed_with, this->transf ) ) );
	this->base_fun_1 .insert ( std::pair < Cell::Core*, Function >
	       ( P7 .core, Function ( psi_P7 / 8., tag::composed_with, this->transf ) ) );
	this->base_fun_1 .insert ( std::pair < Cell::Core*, Function >
	       ( P8 .core, Function ( psi_P8 / 8., tag::composed_with, this->transf ) ) );

	// the only use of composing is to allow the calling code to
	// differentiate base functions with respect to geometric coordinates
	// and anyway this only works for a diffeomorphism, not for an immersion

}  // end of  FiniteElement::WithMaster::Hexahedron::Q1::dock_on  with tag::winding

#endif  // ifndef MANIFEM_NO_QUOTIENT

//------------------------------------------------------------------------------------------------------//


void FiniteElement::Core::dock_on
( const Cell & cll, const tag::FirstVertex &, const Cell & P )

// virtual, here execution forbidden
// later overridden by FiniteElement::StandAlone::TypeOne::{Rectangle,Square}

{	std::cout << __FILE__ << ":" << __LINE__ << ": "
	          << __extension__ __PRETTY_FUNCTION__ << ": " << std::endl;
	std::cout << "you don't have to provide first vertex for this kind of finite element"
	          << std::endl;
	exit ( 1 );                                                                            }


#ifndef MANIFEM_NO_QUOTIENT

void FiniteElement::Core::dock_on
( const Cell & cll, const tag::FirstVertex &, const Cell & P, const tag::Winding & )

// virtual, here execution forbidden
// later overridden by FiniteElement::StandAlone::TypeOne::{Rectangle,Square}

{	std::cout << __FILE__ << ":" << __LINE__ << ": "
	          << __extension__ __PRETTY_FUNCTION__ << ": " << std::endl;
	std::cout << "you don't have to provide first vertex for this kind of finite element"
	          << std::endl;
	exit ( 1 );                                                                            }

#endif  // ifndef MANIFEM_NO_QUOTIENT

//------------------------------------------------------------------------------------------------------//


// macro RESULT, defined below, is intended to make the code more compact, while allowing
// the compiler to take advantage of constexpr indices and optimize the code for speed

#ifndef NDEBUG  // DEBUG mode

#define RESULT(i,j,k) this->result_of_integr [ FiniteElement::StandAlone::assert_index ( i, j, k, base_d, c16, this->result_of_integr .size() ) ]
// use:  RESULT ( psi_P, psi_Q, int_psi_psi ) = value

#else // NDEBUG

#define RESULT(i,j,k) this->result_of_integr [ FiniteElement::StandAlone::index ( i, j, k, base_d, c16 ) ]
// use:  RESULT ( psi_P, psi_Q, int_psi_psi ) = value

#endif // NDEBUG

//------------------------------------------------------------------------------------------------------//
	

inline void FiniteElement::StandAlone::TypeOne::Triangle::dock_on_comput
( const double & xP, const double & yP,
  const double & xQ, const double & yQ,
  const double & xR, const double & yR )

// this function is speed-critical
	
{	double J_c0 = xQ - xP, J_c1 = xR - xP,
	       J_c2 = yQ - yP, J_c3 = yR - yP;

	constexpr size_t base_d = this->base_dim;
	constexpr size_t c16 = this->case_16;
	constexpr size_t psi_P = 0, psi_Q = 1, psi_R = 2;

	// below we use a switch statement
	// we trust that the compiler implements it by means of a list of addresses
	// and not as a cascade of ifs (which would be rather slow)
	// we could use a vector of pointers to functions instead
			
	switch ( this->cas )
		
	{	case  0 :
			std::cout << "hand-coded integrators require pre_compute" << std::endl;
			exit ( 1 );

		case 1 :  // { int psi }
			
		{	constexpr size_t int_psi = 0;

			assert ( this->implemented_cases .find (1) != this->implemented_cases .end() );
			
			const double det = J_c0 * J_c3 - J_c1 * J_c2;
			assert ( det > 0. );  // det = 2. * area

			RESULT ( 0, psi_P, int_psi ) =
			RESULT ( 0, psi_Q, int_psi ) =
			RESULT ( 0, psi_R, int_psi ) = det / 6.;                                         }
		
		break;  // end of case 1 --  Triangle::dock_on_comput

		case 2 :  // { int psi1 * psi2 }
			
		// expressions computed by hand
			
		{	constexpr size_t int_psi_psi = 0;

			assert ( this->implemented_cases .find (2) != this->implemented_cases .end() );
			
			const double det = J_c0 * J_c3 - J_c1 * J_c2;
			assert ( det > 0. );  // det = 2. * area

			RESULT ( psi_P, psi_P, int_psi_psi ) =
			RESULT ( psi_Q, psi_Q, int_psi_psi ) =
			RESULT ( psi_R, psi_R, int_psi_psi ) = 0.08333333333333333 * det;
			RESULT ( psi_P, psi_Q, int_psi_psi ) =
			RESULT ( psi_P, psi_R, int_psi_psi ) =
			RESULT ( psi_Q, psi_P, int_psi_psi ) =
			RESULT ( psi_Q, psi_R, int_psi_psi ) =
			RESULT ( psi_R, psi_P, int_psi_psi ) =
			RESULT ( psi_R, psi_Q, int_psi_psi ) = 0.04166666666666666 * det;                }
		
		break;  // end of case 2 --  Triangle::dock_on_comput

		case 3 :  // { int psi .deriv(x) }
			
		// computations inspired in UFL and FFC
		// https://fenics.readthedocs.io/projects/ufl/en/latest/
		// https://fenics.readthedocs.io/projects/ffc/en/latest/
			
		{	constexpr size_t int_psi_x = 0, int_psi_y = 1;

			assert ( this->implemented_cases .find (3) != this->implemented_cases .end() );
			
			#ifndef NDEBUG  // DEBUG mode
			const double det = J_c0 * J_c3 - J_c1 * J_c2;
			assert ( det > 0. );  // det = 2. * area
			#endif

			J_c0 *= 0.5;  J_c1 *= 0.5;  J_c2 *= 0.5;  J_c3 *= 0.5;
	
			RESULT ( 0, psi_P, int_psi_x ) =   J_c2 - J_c3;
			RESULT ( 0, psi_P, int_psi_y ) =   J_c1 - J_c0;
			RESULT ( 0, psi_Q, int_psi_x ) =   J_c3;
			RESULT ( 0, psi_Q, int_psi_y ) = - J_c1;
			RESULT ( 0, psi_R, int_psi_x ) = - J_c2;
			RESULT ( 0, psi_R, int_psi_y ) =   J_c0;                                         }
		
		break;  // end of case 3 --  Triangle::dock_on_comput

		case 4 :  // { int psi1 .deriv(x) * psi2 .deriv(y) }
			
		// computations inspired in UFL and FFC
		// https://fenics.readthedocs.io/projects/ufl/en/latest/
		// https://fenics.readthedocs.io/projects/ffc/en/latest/
			
		{	constexpr size_t int_psi_x_psi_x = 0, int_psi_x_psi_y = 1,
			                 int_psi_y_psi_x = 2, int_psi_y_psi_y = 3;

			assert ( this->implemented_cases .find (4) != this->implemented_cases .end() );
			
			double det = J_c0 * J_c3 - J_c1 * J_c2;
			assert ( det > 0. );  // det = 2. * area
			det += det;  // we double det to avoid later divisions by two
			const double p00 = J_c0 * J_c0 / det,
			             p01 = J_c0 * J_c1 / det,
			             p02 = J_c0 * J_c2 / det,
			             p03 = J_c0 * J_c3 / det,
			             p11 = J_c1 * J_c1 / det,
			             p12 = J_c1 * J_c2 / det,
			             p13 = J_c1 * J_c3 / det,
			             p22 = J_c2 * J_c2 / det,
			             p23 = J_c2 * J_c3 / det,
			             p33 = J_c3 * J_c3 / det;
			RESULT ( psi_P, psi_P, int_psi_x_psi_x ) =   p33 - p23 - p23 + p22;
			RESULT ( psi_P, psi_Q, int_psi_x_psi_x ) =
			RESULT ( psi_Q, psi_P, int_psi_x_psi_x ) =   p23 - p33;
			RESULT ( psi_Q, psi_Q, int_psi_x_psi_x ) =   p33;
			RESULT ( psi_P, psi_R, int_psi_x_psi_x ) =
			RESULT ( psi_R, psi_P, int_psi_x_psi_x ) =   p23 - p22;
			RESULT ( psi_Q, psi_R, int_psi_x_psi_x ) =
			RESULT ( psi_R, psi_Q, int_psi_x_psi_x ) = - p23;
			RESULT ( psi_R, psi_R, int_psi_x_psi_x ) =   p22;
			RESULT ( psi_P, psi_P, int_psi_x_psi_y ) =
			RESULT ( psi_P, psi_P, int_psi_y_psi_x ) =   p12 + p03 - p13 - p02;
			RESULT ( psi_P, psi_Q, int_psi_x_psi_y ) =
			RESULT ( psi_Q, psi_P, int_psi_y_psi_x ) =   p13 - p12;
			RESULT ( psi_P, psi_R, int_psi_x_psi_y ) =
			RESULT ( psi_R, psi_P, int_psi_y_psi_x ) =   p02 - p03;
			RESULT ( psi_Q, psi_P, int_psi_x_psi_y ) =
			RESULT ( psi_P, psi_Q, int_psi_y_psi_x ) =   p13 - p03;
			RESULT ( psi_Q, psi_Q, int_psi_x_psi_y ) =
			RESULT ( psi_Q, psi_Q, int_psi_y_psi_x ) = - p13;
			RESULT ( psi_Q, psi_R, int_psi_x_psi_y ) =
			RESULT ( psi_R, psi_Q, int_psi_y_psi_x ) =   p03;
			RESULT ( psi_R, psi_P, int_psi_x_psi_y ) =
			RESULT ( psi_P, psi_R, int_psi_y_psi_x ) =   p02 - p12;
			RESULT ( psi_R, psi_Q, int_psi_x_psi_y ) =
			RESULT ( psi_Q, psi_R, int_psi_y_psi_x ) =   p12;
			RESULT ( psi_R, psi_R, int_psi_x_psi_y ) =
			RESULT ( psi_R, psi_R, int_psi_y_psi_x ) = - p02;
			RESULT ( psi_P, psi_P, int_psi_y_psi_y ) =   p11 - p01 - p01 + p00;
			RESULT ( psi_P, psi_Q, int_psi_y_psi_y ) =
			RESULT ( psi_Q, psi_P, int_psi_y_psi_y ) =   p01 - p11;
			RESULT ( psi_Q, psi_Q, int_psi_y_psi_y ) =   p11;
			RESULT ( psi_P, psi_R, int_psi_y_psi_y ) =
			RESULT ( psi_R, psi_P, int_psi_y_psi_y ) =   p01 - p00;
			RESULT ( psi_Q, psi_R, int_psi_y_psi_y ) =
			RESULT ( psi_R, psi_Q, int_psi_y_psi_y ) = - p01;
			RESULT ( psi_R, psi_R, int_psi_y_psi_y ) =   p00;
			
		}  // end of case 4
		
		break;  // end of case 4 --  Triangle::dock_on_comput

		case 5 :  // { int grad psi1 * grad psi2 }
		// { int psi1 .deriv(x) * psi2 .dervi(x) + psi1 .deriv(y) * psi2 .deriv(y) }

		// computations inspired in UFL and FFC
		// https://fenics.readthedocs.io/projects/ufl/en/latest/
		// https://fenics.readthedocs.io/projects/ffc/en/latest/

		{	constexpr size_t int_grad_psi_grad_psi = 0;
				
			assert ( this->implemented_cases .find (5) != this->implemented_cases .end() );
			
			double det = J_c0 * J_c3 - J_c1 * J_c2;
			assert ( det > 0. );  // det = 2. * area
			det += det;  // we double det to avoid later divisions by two
			const double sp17 =   ( J_c0 * J_c0 + J_c2 * J_c2 ) / det,
			             sp18 = - ( J_c0 * J_c1 + J_c3 * J_c2 ) / det,
			             sp19 =   ( J_c1 * J_c1 + J_c3 * J_c3 ) / det;
		
			RESULT ( psi_P, psi_P, int_grad_psi_grad_psi ) =   sp19 + sp18 + sp18 + sp17;
			RESULT ( psi_Q, psi_P, int_grad_psi_grad_psi ) =
			RESULT ( psi_P, psi_Q, int_grad_psi_grad_psi ) = - sp19 - sp18;
			RESULT ( psi_Q, psi_Q, int_grad_psi_grad_psi ) =   sp19;
			RESULT ( psi_R, psi_P, int_grad_psi_grad_psi ) =
			RESULT ( psi_P, psi_R, int_grad_psi_grad_psi ) = - sp18 - sp17;
			RESULT ( psi_R, psi_Q, int_grad_psi_grad_psi ) =
			RESULT ( psi_Q, psi_R, int_grad_psi_grad_psi ) =   sp18;
			RESULT ( psi_R, psi_R, int_grad_psi_grad_psi ) =   sp17;
			
		}  // end of case 5
		
		break;  // end of case 5 --  Triangle::dock_on_comput

		case 6 :  // { int psi1 * psi2 .deriv(x) }
			
		// computations inspired in UFL and FFC
		// https://fenics.readthedocs.io/projects/ufl/en/latest/
		// https://fenics.readthedocs.io/projects/ffc/en/latest/
			
		{	constexpr size_t int_psi_psi_x = 0, int_psi_psi_y = 1;
			
			assert ( this->implemented_cases .find (6) != this->implemented_cases .end() );
			
			#ifndef NDEBUG  // DEBUG mode
			const double det = J_c0 * J_c3 - J_c1 * J_c2;
			assert ( det > 0. );  // det = 2. * area
			#endif

			J_c0 /= 6.;  J_c1 /= 6.;  J_c2 /= 6.;  J_c3 /= 6;
	
			RESULT ( psi_P, psi_P, int_psi_psi_x ) =
			RESULT ( psi_Q, psi_P, int_psi_psi_x ) =
			RESULT ( psi_R, psi_P, int_psi_psi_x ) =   J_c2 - J_c3;
			RESULT ( psi_P, psi_Q, int_psi_psi_x ) =
			RESULT ( psi_Q, psi_Q, int_psi_psi_x ) =
			RESULT ( psi_R, psi_Q, int_psi_psi_x ) =   J_c3;
			RESULT ( psi_P, psi_R, int_psi_psi_x ) =
			RESULT ( psi_Q, psi_R, int_psi_psi_x ) =
			RESULT ( psi_R, psi_R, int_psi_psi_x ) = - J_c2;
			RESULT ( psi_P, psi_P, int_psi_psi_y ) =
			RESULT ( psi_Q, psi_P, int_psi_psi_y ) =
			RESULT ( psi_R, psi_P, int_psi_psi_y ) =   J_c1 - J_c0;
			RESULT ( psi_P, psi_Q, int_psi_psi_y ) =
			RESULT ( psi_Q, psi_Q, int_psi_psi_y ) =
			RESULT ( psi_R, psi_Q, int_psi_psi_y ) = - J_c1;
			RESULT ( psi_P, psi_R, int_psi_psi_y ) =
			RESULT ( psi_Q, psi_R, int_psi_psi_y ) =
			RESULT ( psi_R, psi_R, int_psi_psi_y ) =   J_c0;

		}  // end of case 6
		
		break;  // end of case 6 --  Triangle::dock_on_comput

		case  7 :  // { int psi1, int psi1 * psi2 }

		{	constexpr size_t int_psi = 0, int_psi_psi = 1;

			// selector is never 1 ? if so, eliminate int_psi2 (leave vector with zeros)
				
			assert ( this->implemented_cases .find (7) != this->implemented_cases .end() );
			
			double det = J_c0 * J_c3 - J_c1 * J_c2;
			assert ( det > 0. );  // det = 2. * area
			RESULT ( psi_P, psi_P, int_psi ) =
			RESULT ( psi_Q, psi_P, int_psi ) =
			RESULT ( psi_R, psi_P, int_psi ) =
			RESULT ( psi_P, psi_Q, int_psi ) =
			RESULT ( psi_Q, psi_Q, int_psi ) =
			RESULT ( psi_R, psi_Q, int_psi ) =
			RESULT ( psi_P, psi_R, int_psi ) =
			RESULT ( psi_Q, psi_R, int_psi ) =
			RESULT ( psi_R, psi_R, int_psi ) = det / 6.;
			RESULT ( psi_P, psi_P, int_psi_psi ) =
			RESULT ( psi_Q, psi_Q, int_psi_psi ) =
			RESULT ( psi_R, psi_R, int_psi_psi ) = 0.08333333333333333 * det;
			RESULT ( psi_P, psi_Q, int_psi_psi ) =
			RESULT ( psi_P, psi_R, int_psi_psi ) =
			RESULT ( psi_Q, psi_P, int_psi_psi ) =
			RESULT ( psi_Q, psi_R, int_psi_psi ) =
			RESULT ( psi_R, psi_P, int_psi_psi ) =
			RESULT ( psi_R, psi_Q, int_psi_psi ) =	0.04166666666666666 * det;

		}  // end of case 7
		
		break;  // end of case 7 --  Triangle::dock_on_comput

		case 8 :  // { int psi, int psi .deriv(x) }
			
		{	constexpr size_t int_psi = 0, int_psi_x = 1, int_psi_y = 2;

			assert ( this->implemented_cases .find (8) != this->implemented_cases .end() );
			
			const double det = J_c0 * J_c3 - J_c1 * J_c2;
			assert ( det > 0. );  // det = 2. * area
			
			J_c0 *= 0.5;  J_c1 *= 0.5;  J_c2 *= 0.5;  J_c3 *= 0.5;

			RESULT ( 0, psi_P, int_psi ) =
			RESULT ( 0, psi_Q, int_psi ) =
			RESULT ( 0, psi_R, int_psi ) = det / 6.;
			RESULT ( 0, psi_P, int_psi_x ) =   J_c2 - J_c3;
			RESULT ( 0, psi_P, int_psi_y ) =   J_c1 - J_c0;
			RESULT ( 0, psi_Q, int_psi_x ) =   J_c3;
			RESULT ( 0, psi_Q, int_psi_y ) = - J_c1;
			RESULT ( 0, psi_R, int_psi_x ) = - J_c2;
			RESULT ( 0, psi_R, int_psi_y ) =   J_c0;                                         }
		
		break;  // end of case 8 --  Triangle::dock_on_comput

		case 11 :  // { int psi .deriv(x), int psi1 .deriv(x) * psi2 .deriv(y) }
			
		{	constexpr size_t int_psi_x = 0, int_psi_y = 1,
			                 int_psi_x_psi_x = 2, int_psi_x_psi_y = 3,
			                 int_psi_y_psi_x = 4, int_psi_y_psi_y = 5;

			assert ( this->implemented_cases .find (11) != this->implemented_cases .end() );

			double det = J_c0 * J_c3 - J_c1 * J_c2;
			assert ( det > 0. );  // det = 2. * area
			det += det;  // we double det to avoid later divisions by two

			const double p00 = J_c0 * J_c0 / det,
			             p01 = J_c0 * J_c1 / det,
			             p02 = J_c0 * J_c2 / det,
			             p03 = J_c0 * J_c3 / det,
			             p11 = J_c1 * J_c1 / det,
			             p12 = J_c1 * J_c2 / det,
			             p13 = J_c1 * J_c3 / det,
			             p22 = J_c2 * J_c2 / det,
			             p23 = J_c2 * J_c3 / det,
			             p33 = J_c3 * J_c3 / det;

			J_c0 *= 0.5;  J_c1 *= 0.5;  J_c2 *= 0.5;  J_c3 *= 0.5;

			RESULT ( psi_P, psi_P, int_psi_x ) =
			RESULT ( psi_P, psi_Q, int_psi_x ) =
			RESULT ( psi_P, psi_R, int_psi_x ) =   J_c2 - J_c3;
			RESULT ( psi_P, psi_P, int_psi_y ) =
			RESULT ( psi_P, psi_Q, int_psi_y ) =
			RESULT ( psi_P, psi_R, int_psi_y ) =   J_c1 - J_c0;
			RESULT ( psi_Q, psi_P, int_psi_x ) =
			RESULT ( psi_Q, psi_Q, int_psi_x ) =
			RESULT ( psi_Q, psi_R, int_psi_x ) =   J_c3;
			RESULT ( psi_Q, psi_P, int_psi_y ) =
			RESULT ( psi_Q, psi_Q, int_psi_y ) =
			RESULT ( psi_Q, psi_R, int_psi_y ) = - J_c1;
			RESULT ( psi_R, psi_P, int_psi_x ) =
			RESULT ( psi_R, psi_Q, int_psi_x ) =
			RESULT ( psi_R, psi_R, int_psi_x ) = - J_c2;
			RESULT ( psi_R, psi_P, int_psi_y ) =
			RESULT ( psi_R, psi_Q, int_psi_y ) =
			RESULT ( psi_R, psi_R, int_psi_y ) =   J_c0;
			RESULT ( psi_P, psi_P, int_psi_x_psi_x ) =   p33 - p23 - p23 + p22;
			RESULT ( psi_P, psi_Q, int_psi_x_psi_x ) =
			RESULT ( psi_Q, psi_P, int_psi_x_psi_x ) =   p23 - p33;
			RESULT ( psi_Q, psi_Q, int_psi_x_psi_x ) =   p33;
			RESULT ( psi_P, psi_R, int_psi_x_psi_x ) =
			RESULT ( psi_R, psi_P, int_psi_x_psi_x ) =   p23 - p22;
			RESULT ( psi_Q, psi_R, int_psi_x_psi_x ) =
			RESULT ( psi_R, psi_Q, int_psi_x_psi_x ) = - p23;
			RESULT ( psi_R, psi_R, int_psi_x_psi_x ) =   p22;
			RESULT ( psi_P, psi_P, int_psi_x_psi_y ) =
			RESULT ( psi_P, psi_P, int_psi_y_psi_x ) =   p12 + p03 - p13 - p02;
			RESULT ( psi_P, psi_Q, int_psi_x_psi_y ) =
			RESULT ( psi_Q, psi_P, int_psi_y_psi_x ) =   p13 - p12;
			RESULT ( psi_P, psi_R, int_psi_x_psi_y ) =
			RESULT ( psi_R, psi_P, int_psi_y_psi_x ) =   p02 - p03;
			RESULT ( psi_Q, psi_P, int_psi_x_psi_y ) =
			RESULT ( psi_P, psi_Q, int_psi_y_psi_x ) =   p13 - p03;
			RESULT ( psi_Q, psi_Q, int_psi_x_psi_y ) =
			RESULT ( psi_Q, psi_Q, int_psi_y_psi_x ) = - p13;
			RESULT ( psi_Q, psi_R, int_psi_x_psi_y ) =
			RESULT ( psi_R, psi_Q, int_psi_y_psi_x ) =   p03;
			RESULT ( psi_R, psi_P, int_psi_x_psi_y ) =
			RESULT ( psi_P, psi_R, int_psi_y_psi_x ) =   p02 - p12;
			RESULT ( psi_R, psi_Q, int_psi_x_psi_y ) =
			RESULT ( psi_Q, psi_R, int_psi_y_psi_x ) =   p12;
			RESULT ( psi_R, psi_R, int_psi_x_psi_y ) =
			RESULT ( psi_R, psi_R, int_psi_y_psi_x ) = - p02;
			RESULT ( psi_P, psi_P, int_psi_y_psi_y ) =   p11 - p01 - p01 + p00;
			RESULT ( psi_P, psi_Q, int_psi_y_psi_y ) =
			RESULT ( psi_Q, psi_P, int_psi_y_psi_y ) =   p01 - p11;
			RESULT ( psi_Q, psi_Q, int_psi_y_psi_y ) =   p11;
			RESULT ( psi_P, psi_R, int_psi_y_psi_y ) =
			RESULT ( psi_R, psi_P, int_psi_y_psi_y ) =   p01 - p00;
			RESULT ( psi_Q, psi_R, int_psi_y_psi_y ) =
			RESULT ( psi_R, psi_Q, int_psi_y_psi_y ) = - p01;
			RESULT ( psi_R, psi_R, int_psi_y_psi_y ) =   p00;

		}  // end of case 11
		
		break;  // end of case 11 --  Triangle::dock_on_comput

		case 12 :  // { int psi1 * psi2, int psi1 .deriv(x) * psi2 .deriv(y) }

		{	constexpr size_t int_psi_psi = 0, int_psi_x_psi_x = 1,
			                 int_psi_x_psi_y = 2, int_psi_y_psi_x = 3, int_psi_y_psi_y = 4;
				
			assert ( this->implemented_cases .find (12) != this->implemented_cases .end() );
			

			double det = J_c0 * J_c3 - J_c1 * J_c2;
			assert ( det > 0. );  // det = 2. * area
			
			RESULT ( psi_P, psi_P, int_psi_psi ) =
			RESULT ( psi_Q, psi_Q, int_psi_psi ) =
			RESULT ( psi_R, psi_R, int_psi_psi ) = 0.08333333333333333 * det;
			RESULT ( psi_P, psi_Q, int_psi_psi ) =
			RESULT ( psi_P, psi_R, int_psi_psi ) =
			RESULT ( psi_Q, psi_P, int_psi_psi ) =
			RESULT ( psi_Q, psi_R, int_psi_psi ) =
			RESULT ( psi_R, psi_P, int_psi_psi ) =
			RESULT ( psi_R, psi_Q, int_psi_psi ) = 0.04166666666666666 * det;
			
			det += det;  // we double det to avoid later divisions by two
			const double p00 = J_c0 * J_c0 / det,
			             p01 = J_c0 * J_c1 / det,
			             p02 = J_c0 * J_c2 / det,
			             p03 = J_c0 * J_c3 / det,
			             p11 = J_c1 * J_c1 / det,
			             p12 = J_c1 * J_c2 / det,
			             p13 = J_c1 * J_c3 / det,
			             p22 = J_c2 * J_c2 / det,
			             p23 = J_c2 * J_c3 / det,
			             p33 = J_c3 * J_c3 / det;
			RESULT ( psi_P, psi_P, int_psi_x_psi_x ) =   p33 - p23 - p23 + p22;
			RESULT ( psi_P, psi_Q, int_psi_x_psi_x ) =
			RESULT ( psi_Q, psi_P, int_psi_x_psi_x ) =   p23 - p33;
			RESULT ( psi_Q, psi_Q, int_psi_x_psi_x ) =   p33;
			RESULT ( psi_P, psi_R, int_psi_x_psi_x ) =
			RESULT ( psi_R, psi_P, int_psi_x_psi_x ) =   p23 - p22;
			RESULT ( psi_Q, psi_R, int_psi_x_psi_x ) =
			RESULT ( psi_R, psi_Q, int_psi_x_psi_x ) = - p23;
			RESULT ( psi_R, psi_R, int_psi_x_psi_x ) =   p22;
			RESULT ( psi_P, psi_P, int_psi_x_psi_y ) =
			RESULT ( psi_P, psi_P, int_psi_y_psi_x ) =   p12 + p03 - p13 - p02;
			RESULT ( psi_P, psi_Q, int_psi_x_psi_y ) =
			RESULT ( psi_Q, psi_P, int_psi_y_psi_x ) =   p13 - p12;
			RESULT ( psi_P, psi_R, int_psi_x_psi_y ) =
			RESULT ( psi_R, psi_P, int_psi_y_psi_x ) =   p02 - p03;
			RESULT ( psi_Q, psi_P, int_psi_x_psi_y ) =
			RESULT ( psi_P, psi_Q, int_psi_y_psi_x ) =   p13 - p03;
			RESULT ( psi_Q, psi_Q, int_psi_x_psi_y ) =
			RESULT ( psi_Q, psi_Q, int_psi_y_psi_x ) = - p13;
			RESULT ( psi_Q, psi_R, int_psi_x_psi_y ) =
			RESULT ( psi_R, psi_Q, int_psi_y_psi_x ) =   p03;
			RESULT ( psi_R, psi_P, int_psi_x_psi_y ) =
			RESULT ( psi_P, psi_R, int_psi_y_psi_x ) =   p02 - p12;
			RESULT ( psi_R, psi_Q, int_psi_x_psi_y ) =
			RESULT ( psi_Q, psi_R, int_psi_y_psi_x ) =   p12;
			RESULT ( psi_R, psi_R, int_psi_x_psi_y ) =
			RESULT ( psi_R, psi_R, int_psi_y_psi_x ) = - p02;
			RESULT ( psi_P, psi_P, int_psi_y_psi_y ) =   p11 - p01 - p01 + p00;
			RESULT ( psi_P, psi_Q, int_psi_y_psi_y ) =
			RESULT ( psi_Q, psi_P, int_psi_y_psi_y ) =   p01 - p11;
			RESULT ( psi_Q, psi_Q, int_psi_y_psi_y ) =   p11;
			RESULT ( psi_P, psi_R, int_psi_y_psi_y ) =
			RESULT ( psi_R, psi_P, int_psi_y_psi_y ) =   p01 - p00;
			RESULT ( psi_Q, psi_R, int_psi_y_psi_y ) =
			RESULT ( psi_R, psi_Q, int_psi_y_psi_y ) = - p01;
			RESULT ( psi_R, psi_R, int_psi_y_psi_y ) =   p00;

		}  // end of case 12
		
			break;  // end of case 12 --  Triangle::dock_on_comput

		case 13 :  // { int psi1 * psi2, int grad psi1 * grad psi2 }

		{	constexpr size_t int_psi_psi = 0, int_grad_psi_grad_psi = 1;
				
			assert ( this->implemented_cases .find (13) != this->implemented_cases .end() );
			
			double det = J_c0 * J_c3 - J_c1 * J_c2;
			assert ( det > 0. );  // det = 2. * area
			
			RESULT ( psi_P, psi_P, int_psi_psi ) =
			RESULT ( psi_Q, psi_Q, int_psi_psi ) =
			RESULT ( psi_R, psi_R, int_psi_psi ) = 0.08333333333333333 * det;
			RESULT ( psi_P, psi_Q, int_psi_psi ) =
			RESULT ( psi_P, psi_R, int_psi_psi ) =
			RESULT ( psi_Q, psi_P, int_psi_psi ) =
			RESULT ( psi_Q, psi_R, int_psi_psi ) =
			RESULT ( psi_R, psi_P, int_psi_psi ) =
			RESULT ( psi_R, psi_Q, int_psi_psi ) = 0.04166666666666666 * det;
			
			det += det;  // we double det to avoid later divisions by two
			const double sp17 =   ( J_c0 * J_c0 + J_c2 * J_c2 ) / det,
			             sp18 = - ( J_c0 * J_c1 + J_c3 * J_c2 ) / det,
			             sp19 =   ( J_c1 * J_c1 + J_c3 * J_c3 ) / det;
		
			RESULT ( psi_P, psi_P, int_grad_psi_grad_psi ) =
				sp19 + sp18 + sp18 + sp17;
			RESULT ( psi_Q, psi_P, int_grad_psi_grad_psi ) =
			RESULT ( psi_P, psi_Q, int_grad_psi_grad_psi ) = - sp19 - sp18;
			RESULT ( psi_Q, psi_Q, int_grad_psi_grad_psi ) =   sp19;
			RESULT ( psi_R, psi_P, int_grad_psi_grad_psi ) =
			RESULT ( psi_P, psi_R, int_grad_psi_grad_psi ) = - sp18 - sp17;
			RESULT ( psi_R, psi_Q, int_grad_psi_grad_psi ) =
			RESULT ( psi_Q, psi_R, int_grad_psi_grad_psi ) =   sp18;
			RESULT ( psi_R, psi_R, int_grad_psi_grad_psi ) =   sp17;

		}  // end of case 13
		
		break;  // end of case 13 --  Triangle::dock_on_comput

		case 14 :  // 1, 2, 3 and 4

		{	constexpr size_t int_psi = 0;
			constexpr size_t int_psi_psi = 1;
			constexpr size_t int_psi_x = 2, int_psi_y = 3;
			constexpr size_t int_psi_x_psi_x = 4, int_psi_x_psi_y = 5,
			                 int_psi_y_psi_x = 6, int_psi_y_psi_y = 7;
				
			assert ( this->implemented_cases .find (14) != this->implemented_cases .end() );
			
			//  case 1 (within 14) :  { int psi }

			double det = J_c0 * J_c3 - J_c1 * J_c2;
			assert ( det > 0. );  // det = 2. * area

			RESULT ( psi_P, psi_P, int_psi ) =
			RESULT ( psi_Q, psi_P, int_psi ) =
			RESULT ( psi_R, psi_P, int_psi ) =
			RESULT ( psi_P, psi_Q, int_psi ) =
			RESULT ( psi_Q, psi_Q, int_psi ) =
			RESULT ( psi_R, psi_Q, int_psi ) =
			RESULT ( psi_P, psi_R, int_psi ) =
			RESULT ( psi_Q, psi_R, int_psi ) =
			RESULT ( psi_R, psi_R, int_psi ) = det / 6.;

			// case 2 (within 14) :  { int psi1 * psi2 }

			RESULT ( psi_P, psi_P, int_psi_psi ) =
			RESULT ( psi_Q, psi_Q, int_psi_psi ) =
			RESULT ( psi_R, psi_R, int_psi_psi ) = 0.08333333333333333 * det;
			RESULT ( psi_P, psi_Q, int_psi_psi ) =
			RESULT ( psi_P, psi_R, int_psi_psi ) =
			RESULT ( psi_Q, psi_P, int_psi_psi ) =
			RESULT ( psi_Q, psi_R, int_psi_psi ) =
			RESULT ( psi_R, psi_P, int_psi_psi ) =
			RESULT ( psi_R, psi_Q, int_psi_psi ) = 0.04166666666666666 * det;

			// case 4 (within 14) :  { int psi1 .deriv(x) * psi2 .deriv(y) }
			
			det += det;  // we double det to avoid later divisions by two
			const double p00 = J_c0 * J_c0 / det,
			             p01 = J_c0 * J_c1 / det,
			             p02 = J_c0 * J_c2 / det,
			             p03 = J_c0 * J_c3 / det,
			             p11 = J_c1 * J_c1 / det,
			             p12 = J_c1 * J_c2 / det,
			             p13 = J_c1 * J_c3 / det,
			             p22 = J_c2 * J_c2 / det,
			             p23 = J_c2 * J_c3 / det,
			             p33 = J_c3 * J_c3 / det;
			RESULT ( psi_P, psi_P, int_psi_x_psi_x ) =   p33 - p23 - p23 + p22;
			RESULT ( psi_P, psi_Q, int_psi_x_psi_x ) =
			RESULT ( psi_Q, psi_P, int_psi_x_psi_x ) =   p23 - p33;
			RESULT ( psi_Q, psi_Q, int_psi_x_psi_x ) =   p33;
			RESULT ( psi_P, psi_R, int_psi_x_psi_x ) =
			RESULT ( psi_R, psi_P, int_psi_x_psi_x ) =   p23 - p22;
			RESULT ( psi_Q, psi_R, int_psi_x_psi_x ) =
			RESULT ( psi_R, psi_Q, int_psi_x_psi_x ) = - p23;
			RESULT ( psi_R, psi_R, int_psi_x_psi_x ) =   p22;
			RESULT ( psi_P, psi_P, int_psi_x_psi_y ) =
			RESULT ( psi_P, psi_P, int_psi_y_psi_x ) =   p12 + p03 - p13 - p02;
			RESULT ( psi_P, psi_Q, int_psi_x_psi_y ) =
			RESULT ( psi_Q, psi_P, int_psi_y_psi_x ) =   p13 - p12;
			RESULT ( psi_P, psi_R, int_psi_x_psi_y ) =
			RESULT ( psi_R, psi_P, int_psi_y_psi_x ) =   p02 - p03;
			RESULT ( psi_Q, psi_P, int_psi_x_psi_y ) =
			RESULT ( psi_P, psi_Q, int_psi_y_psi_x ) =   p13 - p03;
			RESULT ( psi_Q, psi_Q, int_psi_x_psi_y ) =
			RESULT ( psi_Q, psi_Q, int_psi_y_psi_x ) = - p13;
			RESULT ( psi_Q, psi_R, int_psi_x_psi_y ) =
			RESULT ( psi_R, psi_Q, int_psi_y_psi_x ) =   p03;
			RESULT ( psi_R, psi_P, int_psi_x_psi_y ) =
			RESULT ( psi_P, psi_R, int_psi_y_psi_x ) =   p02 - p12;
			RESULT ( psi_R, psi_Q, int_psi_x_psi_y ) =
			RESULT ( psi_Q, psi_R, int_psi_y_psi_x ) =   p12;
			RESULT ( psi_R, psi_R, int_psi_x_psi_y ) =
			RESULT ( psi_R, psi_R, int_psi_y_psi_x ) = - p02;
			RESULT ( psi_P, psi_P, int_psi_y_psi_y ) =   p11 - p01 - p01 + p00;
			RESULT ( psi_P, psi_Q, int_psi_y_psi_y ) =
			RESULT ( psi_Q, psi_P, int_psi_y_psi_y ) =   p01 - p11;
			RESULT ( psi_Q, psi_Q, int_psi_y_psi_y ) =   p11;
			RESULT ( psi_P, psi_R, int_psi_y_psi_y ) =
			RESULT ( psi_R, psi_P, int_psi_y_psi_y ) =   p01 - p00;
			RESULT ( psi_Q, psi_R, int_psi_y_psi_y ) =
			RESULT ( psi_R, psi_Q, int_psi_y_psi_y ) = - p01;
			RESULT ( psi_R, psi_R, int_psi_y_psi_y ) =   p00;
	
			// case 3 (within 14) :  { int psi .deriv(x) }

			J_c0 *= 0.5;  J_c1 *= 0.5;  J_c2 *= 0.5;  J_c3 *= 0.5;
	
			RESULT ( psi_P, psi_P, int_psi_x ) =
			RESULT ( psi_P, psi_Q, int_psi_x ) =
			RESULT ( psi_P, psi_R, int_psi_x ) =   J_c2 - J_c3;
			RESULT ( psi_P, psi_P, int_psi_y ) =
			RESULT ( psi_P, psi_Q, int_psi_y ) =
			RESULT ( psi_P, psi_R, int_psi_y ) =   J_c1 - J_c0;
			RESULT ( psi_Q, psi_P, int_psi_x ) =
			RESULT ( psi_Q, psi_Q, int_psi_x ) =
			RESULT ( psi_Q, psi_R, int_psi_x ) =   J_c3;
			RESULT ( psi_Q, psi_P, int_psi_y ) =
			RESULT ( psi_Q, psi_Q, int_psi_y ) =
			RESULT ( psi_Q, psi_R, int_psi_y ) = - J_c1;
			RESULT ( psi_R, psi_P, int_psi_x ) =
			RESULT ( psi_R, psi_Q, int_psi_x ) =
			RESULT ( psi_R, psi_R, int_psi_x ) = - J_c2;
			RESULT ( psi_R, psi_P, int_psi_y ) =
			RESULT ( psi_R, psi_Q, int_psi_y ) =
			RESULT ( psi_R, psi_R, int_psi_y ) =   J_c0;

		}  // end of case 14

		break;  // end of case 14 --  Triangle::dock_on_comput

		case 15 :  // 1, 2, 3, 4 and 5

		{	constexpr size_t int_psi = 0, int_psi_psi = 1, int_psi_x = 2, int_psi_y = 3,
				              int_psi_x_psi_x = 4, int_psi_x_psi_y = 5,
				              int_psi_y_psi_x = 6, int_psi_y_psi_y = 7,
			                 int_grad_psi_grad_psi = 8;
				
			assert ( this->implemented_cases .find (15) != this->implemented_cases .end() );
			
			//  case 1 (within 15) :  { int psi }

			double det = J_c0 * J_c3 - J_c1 * J_c2;
			assert ( det > 0. );  // det = 2. * area

			RESULT ( psi_P, psi_P, int_psi ) =
			RESULT ( psi_Q, psi_P, int_psi ) =
			RESULT ( psi_R, psi_P, int_psi ) =
			RESULT ( psi_P, psi_Q, int_psi ) =
			RESULT ( psi_Q, psi_Q, int_psi ) =
			RESULT ( psi_R, psi_Q, int_psi ) =
			RESULT ( psi_P, psi_R, int_psi ) =
			RESULT ( psi_Q, psi_R, int_psi ) =
			RESULT ( psi_R, psi_R, int_psi ) = det / 6.;

			// case 2 (within 15) :  { int psi1 * psi2 }

			RESULT ( psi_P, psi_P, int_psi_psi ) =
			RESULT ( psi_Q, psi_Q, int_psi_psi ) =
			RESULT ( psi_R, psi_R, int_psi_psi ) = 0.08333333333333333 * det;
			RESULT ( psi_P, psi_Q, int_psi_psi ) =
			RESULT ( psi_P, psi_R, int_psi_psi ) =
			RESULT ( psi_Q, psi_P, int_psi_psi ) =
			RESULT ( psi_Q, psi_R, int_psi_psi ) =
			RESULT ( psi_R, psi_P, int_psi_psi ) =
			RESULT ( psi_R, psi_Q, int_psi_psi ) = 0.04166666666666666 * det;

			// case 4 (within 15) :  { int psi1 .deriv(x) * psi2 .deriv(y) }
			
			det += det;  // we double det to avoid later divisions by two
			const double p00 = J_c0 * J_c0 / det,
			             p01 = J_c0 * J_c1 / det,
			             p02 = J_c0 * J_c2 / det,
			             p03 = J_c0 * J_c3 / det,
			             p11 = J_c1 * J_c1 / det,
			             p12 = J_c1 * J_c2 / det,
			             p13 = J_c1 * J_c3 / det,
			             p22 = J_c2 * J_c2 / det,
			             p23 = J_c2 * J_c3 / det,
			             p33 = J_c3 * J_c3 / det;
			RESULT ( psi_P, psi_P, int_psi_x_psi_x ) =   p33 - p23 - p23 + p22;
			RESULT ( psi_P, psi_Q, int_psi_x_psi_x ) =
			RESULT ( psi_Q, psi_P, int_psi_x_psi_x ) =   p23 - p33;
			RESULT ( psi_Q, psi_Q, int_psi_x_psi_x ) =   p33;
			RESULT ( psi_P, psi_R, int_psi_x_psi_x ) =
			RESULT ( psi_R, psi_P, int_psi_x_psi_x ) =   p23 - p22;
			RESULT ( psi_Q, psi_R, int_psi_x_psi_x ) =
			RESULT ( psi_R, psi_Q, int_psi_x_psi_x ) = - p23;
			RESULT ( psi_R, psi_R, int_psi_x_psi_x ) =   p22;
			RESULT ( psi_P, psi_P, int_psi_x_psi_y ) =
			RESULT ( psi_P, psi_P, int_psi_y_psi_x ) =   p12 + p03 - p13 - p02;
			RESULT ( psi_P, psi_Q, int_psi_x_psi_y ) =
			RESULT ( psi_Q, psi_P, int_psi_y_psi_x ) =   p13 - p12;
			RESULT ( psi_P, psi_R, int_psi_x_psi_y ) =
			RESULT ( psi_R, psi_P, int_psi_y_psi_x ) =   p02 - p03;
			RESULT ( psi_Q, psi_P, int_psi_x_psi_y ) =
			RESULT ( psi_P, psi_Q, int_psi_y_psi_x ) =   p13 - p03;
			RESULT ( psi_Q, psi_Q, int_psi_x_psi_y ) =
			RESULT ( psi_Q, psi_Q, int_psi_y_psi_x ) = - p13;
			RESULT ( psi_Q, psi_R, int_psi_x_psi_y ) =
			RESULT ( psi_R, psi_Q, int_psi_y_psi_x ) =   p03;
			RESULT ( psi_R, psi_P, int_psi_x_psi_y ) =
			RESULT ( psi_P, psi_R, int_psi_y_psi_x ) =   p02 - p12;
			RESULT ( psi_R, psi_Q, int_psi_x_psi_y ) =
			RESULT ( psi_Q, psi_R, int_psi_y_psi_x ) =   p12;
			RESULT ( psi_R, psi_R, int_psi_x_psi_y ) =
			RESULT ( psi_R, psi_R, int_psi_y_psi_x ) = - p02;
			RESULT ( psi_P, psi_P, int_psi_y_psi_y ) =   p11 - p01 - p01 + p00;
			RESULT ( psi_P, psi_Q, int_psi_y_psi_y ) =
			RESULT ( psi_Q, psi_P, int_psi_y_psi_y ) =   p01 - p11;
			RESULT ( psi_Q, psi_Q, int_psi_y_psi_y ) =   p11;
			RESULT ( psi_P, psi_R, int_psi_y_psi_y ) =
			RESULT ( psi_R, psi_P, int_psi_y_psi_y ) =   p01 - p00;
			RESULT ( psi_Q, psi_R, int_psi_y_psi_y ) =
			RESULT ( psi_R, psi_Q, int_psi_y_psi_y ) = - p01;
			RESULT ( psi_R, psi_R, int_psi_y_psi_y ) =   p00;
	
			// case 5 (within 15) :  { int grad psi grad psi }

			const double sp17 =   ( J_c0 * J_c0 + J_c2 * J_c2 ) / det,
			             sp18 = - ( J_c0 * J_c1 + J_c3 * J_c2 ) / det,
			             sp19 =   ( J_c1 * J_c1 + J_c3 * J_c3 ) / det;
		
			RESULT ( psi_P, psi_P, int_grad_psi_grad_psi ) =
				sp19 + sp18 + sp18 + sp17;
			RESULT ( psi_Q, psi_P, int_grad_psi_grad_psi ) =
			RESULT ( psi_P, psi_Q, int_grad_psi_grad_psi ) = - sp19 - sp18;
			RESULT ( psi_Q, psi_Q, int_grad_psi_grad_psi ) =   sp19;
			RESULT ( psi_R, psi_P, int_grad_psi_grad_psi ) =
			RESULT ( psi_P, psi_R, int_grad_psi_grad_psi ) = - sp18 - sp17;
			RESULT ( psi_R, psi_Q, int_grad_psi_grad_psi ) =
			RESULT ( psi_Q, psi_R, int_grad_psi_grad_psi ) =   sp18;
			RESULT ( psi_R, psi_R, int_grad_psi_grad_psi ) =   sp17;
		
			// case 3 (within 15) :  { int psi .deriv(x) }

			J_c0 *= 0.5;  J_c1 *= 0.5;  J_c2 *= 0.5;  J_c3 *= 0.5;
	
			RESULT ( psi_P, psi_P, int_psi_x ) =
			RESULT ( psi_P, psi_Q, int_psi_x ) =
			RESULT ( psi_P, psi_R, int_psi_x ) =   J_c2 - J_c3;
			RESULT ( psi_P, psi_P, int_psi_y ) =
			RESULT ( psi_P, psi_Q, int_psi_y ) =
			RESULT ( psi_P, psi_R, int_psi_y ) =   J_c1 - J_c0;
			RESULT ( psi_Q, psi_P, int_psi_x ) =
			RESULT ( psi_Q, psi_Q, int_psi_x ) =
			RESULT ( psi_Q, psi_R, int_psi_x ) =   J_c3;
			RESULT ( psi_Q, psi_P, int_psi_y ) =
			RESULT ( psi_Q, psi_Q, int_psi_y ) =
			RESULT ( psi_Q, psi_R, int_psi_y ) = - J_c1;
			RESULT ( psi_R, psi_P, int_psi_x ) =
			RESULT ( psi_R, psi_Q, int_psi_x ) =
			RESULT ( psi_R, psi_R, int_psi_x ) = - J_c2;
			RESULT ( psi_R, psi_P, int_psi_y ) =
			RESULT ( psi_R, psi_Q, int_psi_y ) =
			RESULT ( psi_R, psi_R, int_psi_y ) =   J_c0;

		}  // end of case 15

		break;  // end of case 15 --  Triangle::dock_on_comput

		case 16 :  // everything

		{	constexpr size_t int_psi = 0, int_psi_psi = 1, int_psi_x = 2, int_psi_y = 3,
				              int_psi_x_psi_x = 4, int_psi_x_psi_y = 5,
				              int_psi_y_psi_x = 6, int_psi_y_psi_y = 7,
			                 int_grad_psi_grad_psi = 8;
			constexpr size_t int_psi_psi_x = 9, int_psi_psi_y = 10;
				
			assert ( this->implemented_cases .find (16) != this->implemented_cases .end() );
			
			//  case 1 (within 16) :  { int psi }

			double det = J_c0 * J_c3 - J_c1 * J_c2;
			assert ( det > 0. );  // det = 2. * area

			RESULT ( psi_P, psi_P, int_psi ) =
			RESULT ( psi_Q, psi_P, int_psi ) =
			RESULT ( psi_R, psi_P, int_psi ) =
			RESULT ( psi_P, psi_Q, int_psi ) =
			RESULT ( psi_Q, psi_Q, int_psi ) =
			RESULT ( psi_R, psi_Q, int_psi ) =
			RESULT ( psi_P, psi_R, int_psi ) =
			RESULT ( psi_Q, psi_R, int_psi ) =
			RESULT ( psi_R, psi_R, int_psi ) = det / 6.;

			// case 2 (within 16) :  { int psi1 * psi2 }

			RESULT ( psi_P, psi_P, int_psi_psi ) =
			RESULT ( psi_Q, psi_Q, int_psi_psi ) =
			RESULT ( psi_R, psi_R, int_psi_psi ) = 0.08333333333333333 * det;
			RESULT ( psi_P, psi_Q, int_psi_psi ) =
			RESULT ( psi_P, psi_R, int_psi_psi ) =
			RESULT ( psi_Q, psi_P, int_psi_psi ) =
			RESULT ( psi_Q, psi_R, int_psi_psi ) =
			RESULT ( psi_R, psi_P, int_psi_psi ) =
			RESULT ( psi_R, psi_Q, int_psi_psi ) = 0.04166666666666666 * det;

			// case 4 (within 16) :  { int psi1 .deriv(x) * psi2 .deriv(y) }
			
			det += det;  // we double det to avoid later divisions by two
			const double p00 = J_c0 * J_c0 / det,
			             p01 = J_c0 * J_c1 / det,
			             p02 = J_c0 * J_c2 / det,
			             p03 = J_c0 * J_c3 / det,
			             p11 = J_c1 * J_c1 / det,
			             p12 = J_c1 * J_c2 / det,
			             p13 = J_c1 * J_c3 / det,
			             p22 = J_c2 * J_c2 / det,
			             p23 = J_c2 * J_c3 / det,
			             p33 = J_c3 * J_c3 / det;
			RESULT ( psi_P, psi_P, int_psi_x_psi_x ) =   p33 - p23 - p23 + p22;
			RESULT ( psi_P, psi_Q, int_psi_x_psi_x ) =
			RESULT ( psi_Q, psi_P, int_psi_x_psi_x ) =   p23 - p33;
			RESULT ( psi_Q, psi_Q, int_psi_x_psi_x ) =   p33;
			RESULT ( psi_P, psi_R, int_psi_x_psi_x ) =
			RESULT ( psi_R, psi_P, int_psi_x_psi_x ) =   p23 - p22;
			RESULT ( psi_Q, psi_R, int_psi_x_psi_x ) =
			RESULT ( psi_R, psi_Q, int_psi_x_psi_x ) = - p23;
			RESULT ( psi_R, psi_R, int_psi_x_psi_x ) =   p22;
			RESULT ( psi_P, psi_P, int_psi_x_psi_y ) =
			RESULT ( psi_P, psi_P, int_psi_y_psi_x ) =   p12 + p03 - p13 - p02;
			RESULT ( psi_P, psi_Q, int_psi_x_psi_y ) =
			RESULT ( psi_Q, psi_P, int_psi_y_psi_x ) =   p13 - p12;
			RESULT ( psi_P, psi_R, int_psi_x_psi_y ) =
			RESULT ( psi_R, psi_P, int_psi_y_psi_x ) =   p02 - p03;
			RESULT ( psi_Q, psi_P, int_psi_x_psi_y ) =
			RESULT ( psi_P, psi_Q, int_psi_y_psi_x ) =   p13 - p03;
			RESULT ( psi_Q, psi_Q, int_psi_x_psi_y ) =
			RESULT ( psi_Q, psi_Q, int_psi_y_psi_x ) = - p13;
			RESULT ( psi_Q, psi_R, int_psi_x_psi_y ) =
			RESULT ( psi_R, psi_Q, int_psi_y_psi_x ) =   p03;
			RESULT ( psi_R, psi_P, int_psi_x_psi_y ) =
			RESULT ( psi_P, psi_R, int_psi_y_psi_x ) =   p02 - p12;
			RESULT ( psi_R, psi_Q, int_psi_x_psi_y ) =
			RESULT ( psi_Q, psi_R, int_psi_y_psi_x ) =   p12;
			RESULT ( psi_R, psi_R, int_psi_x_psi_y ) =
			RESULT ( psi_R, psi_R, int_psi_y_psi_x ) = - p02;
			RESULT ( psi_P, psi_P, int_psi_y_psi_y ) =   p11 - p01 - p01 + p00;
			RESULT ( psi_P, psi_Q, int_psi_y_psi_y ) =
			RESULT ( psi_Q, psi_P, int_psi_y_psi_y ) =   p01 - p11;
			RESULT ( psi_Q, psi_Q, int_psi_y_psi_y ) =   p11;
			RESULT ( psi_P, psi_R, int_psi_y_psi_y ) =
			RESULT ( psi_R, psi_P, int_psi_y_psi_y ) =   p01 - p00;
			RESULT ( psi_Q, psi_R, int_psi_y_psi_y ) =
			RESULT ( psi_R, psi_Q, int_psi_y_psi_y ) = - p01;
			RESULT ( psi_R, psi_R, int_psi_y_psi_y ) =   p00;
	
			// case 5 (within 16) :  { int grad psi grad psi }

			const double sp17 =   ( J_c0 * J_c0 + J_c2 * J_c2 ) / det,
			             sp18 = - ( J_c0 * J_c1 + J_c3 * J_c2 ) / det,
			             sp19 =   ( J_c1 * J_c1 + J_c3 * J_c3 ) / det;
		
			RESULT ( psi_P, psi_P, int_grad_psi_grad_psi ) =   sp19 + sp18 + sp18 + sp17;
			RESULT ( psi_Q, psi_P, int_grad_psi_grad_psi ) =
			RESULT ( psi_P, psi_Q, int_grad_psi_grad_psi ) = - sp19 - sp18;
			RESULT ( psi_Q, psi_Q, int_grad_psi_grad_psi ) =   sp19;
			RESULT ( psi_R, psi_P, int_grad_psi_grad_psi ) =
			RESULT ( psi_P, psi_R, int_grad_psi_grad_psi ) = - sp18 - sp17;
			RESULT ( psi_R, psi_Q, int_grad_psi_grad_psi ) =
			RESULT ( psi_Q, psi_R, int_grad_psi_grad_psi ) =   sp18;
			RESULT ( psi_R, psi_R, int_grad_psi_grad_psi ) =   sp17;
		
			// case 3 (within 16) :  { int psi .deriv(x) }

			J_c0 *= 0.5;  J_c1 *= 0.5;  J_c2 *= 0.5;  J_c3 *= 0.5;
	
			RESULT ( psi_P, psi_P, int_psi_x ) =
			RESULT ( psi_P, psi_Q, int_psi_x ) =
			RESULT ( psi_P, psi_R, int_psi_x ) =   J_c2 - J_c3;
			RESULT ( psi_P, psi_P, int_psi_y ) =
			RESULT ( psi_P, psi_Q, int_psi_y ) =
			RESULT ( psi_P, psi_R, int_psi_y ) =   J_c1 - J_c0;
			RESULT ( psi_Q, psi_P, int_psi_x ) =
			RESULT ( psi_Q, psi_Q, int_psi_x ) =
			RESULT ( psi_Q, psi_R, int_psi_x ) =   J_c3;
			RESULT ( psi_Q, psi_P, int_psi_y ) =
			RESULT ( psi_Q, psi_Q, int_psi_y ) =
			RESULT ( psi_Q, psi_R, int_psi_y ) = - J_c1;
			RESULT ( psi_R, psi_P, int_psi_x ) =
			RESULT ( psi_R, psi_Q, int_psi_x ) =
			RESULT ( psi_R, psi_R, int_psi_x ) = - J_c2;
			RESULT ( psi_R, psi_P, int_psi_y ) =
			RESULT ( psi_R, psi_Q, int_psi_y ) =
			RESULT ( psi_R, psi_R, int_psi_y ) =   J_c0;

			// case 6 (within 16) :  { int psi * psi .deriv(x) }

			J_c0 /= 3.;  J_c1 /= 3.;  J_c2 /= 3.;  J_c3 /= 3;
	
			RESULT ( psi_P, psi_P, int_psi_psi_x ) =
			RESULT ( psi_Q, psi_P, int_psi_psi_x ) =
			RESULT ( psi_R, psi_P, int_psi_psi_x ) =   J_c2 - J_c3;
			RESULT ( psi_P, psi_Q, int_psi_psi_x ) =
			RESULT ( psi_Q, psi_Q, int_psi_psi_x ) =
			RESULT ( psi_R, psi_Q, int_psi_psi_x ) =   J_c3;
			RESULT ( psi_P, psi_R, int_psi_psi_x ) =
			RESULT ( psi_Q, psi_R, int_psi_psi_x ) =
			RESULT ( psi_R, psi_R, int_psi_psi_x ) = - J_c2;
			RESULT ( psi_P, psi_P, int_psi_psi_y ) =
			RESULT ( psi_Q, psi_P, int_psi_psi_y ) =
			RESULT ( psi_R, psi_P, int_psi_psi_y ) =   J_c1 - J_c0;
			RESULT ( psi_P, psi_Q, int_psi_psi_y ) =
			RESULT ( psi_Q, psi_Q, int_psi_psi_y ) =
			RESULT ( psi_R, psi_Q, int_psi_psi_y ) = - J_c1;
			RESULT ( psi_P, psi_R, int_psi_psi_y ) =
			RESULT ( psi_Q, psi_R, int_psi_psi_y ) =
			RESULT ( psi_R, psi_R, int_psi_psi_y ) =   J_c0;

		}  // end of case 16
			
		break;  // end of case 16 --  Triangle::dock_on_comput

		default : assert ( false );
	}  // end of  switch  statement

}  // end of  FiniteElement::StandAlone::TypeOne::Triangle::dock_on_comput

//------------------------------------------------------------------------------------------------------//

  
inline void FiniteElement::StandAlone::TypeOne::Quadrangle::dock_on_comput
( const double & xP, const double & yP, const double & xQ, const double & yQ,
  const double & xR, const double & yR, const double & xS, const double & yS )

// this function is speed-critical

{	const double xRmxQ = xR - xQ, xSmxP = xS - xP, xPmxQ = xP - xQ, xSmxR = xS - xR,
	             yRmyQ = yR - yQ, ySmyP = yS - yP, yPmyQ = yP - yQ, ySmyR = yS - yR;

	constexpr size_t base_d = this->base_dim;
	constexpr size_t c16 = this->case_16;
	constexpr size_t psi_P = 0, psi_Q = 1, psi_R = 2, psi_S = 3;

	// below we use a switch statement
	// we trust that the compiler implements it by means of a list of addresses
	// and not as a cascade of ifs (which would be rather slow)
	// we could use a vector of pointers to functions instead
			
	switch ( this->cas )
		
	{	case  0 :
			std::cout << "hand-coded integrators require pre_compute" << std::endl;
			exit ( 1 );

		case 1 :  // { int psi }
			
		{
			assert ( this->implemented_cases .find (1) != this->implemented_cases .end() );			
			assert ( false );
		}
		break;  // end of case 1

		case 2 :  // { int psi1 * psi2 }
			
		{
			assert ( this->implemented_cases .find (2) != this->implemented_cases .end() );			
			assert ( false );
		}
		break;  // end of case 2

		case 3 :  // { int psi .deriv(x) }
		
			// computations inspired in UFL and FFC, further optimized by hand
			// https://fenics.readthedocs.io/projects/ufl/en/latest/
			// https://fenics.readthedocs.io/projects/ffc/en/latest/

		{	constexpr size_t int_psi_x = 0, int_psi_y = 1;

			assert ( this->implemented_cases .find (3) != this->implemented_cases .end() );
			
			const double alph = 0.7886751345948129,
			             beta = 0.2113248654051871;
			double J_c0_02 = alph * xRmxQ + beta * xSmxP,
			       J_c0_13 = beta * xRmxQ + alph * xSmxP,
			       J_c1_01 = alph * xPmxQ + beta * xSmxR,
			       J_c1_23 = beta * xPmxQ + alph * xSmxR,
			       J_c2_02 = alph * yRmyQ + beta * ySmyP,
			       J_c2_13 = beta * yRmyQ + alph * ySmyP,
			       J_c3_01 = alph * yPmyQ + beta * ySmyR,
			       J_c3_23 = beta * yPmyQ + alph * ySmyR;
			
			#ifndef NDEBUG  // DEBUG mode
			const double det_0 = J_c0_02 * J_c3_01 - J_c1_01 * J_c2_02,
			             det_1 = J_c0_13 * J_c3_01 - J_c1_01 * J_c2_13,
			             det_2 = J_c0_02 * J_c3_23 - J_c1_23 * J_c2_02,
			             det_3 = J_c0_13 * J_c3_23 - J_c1_23 * J_c2_13;
			assert ( det_0 > 0. );  // det = area
			assert ( det_1 > 0. );  // det = area
			assert ( det_2 > 0. );  // det = area
			assert ( det_3 > 0. );  // det = area
			#endif  // DEBUG

			const double J_c0 = 0.25 * ( J_c0_02 + J_c0_13 ),
			             J_c1 = 0.25 * ( J_c1_01 + J_c1_23 ),
			             J_c2 = 0.25 * ( J_c2_02 + J_c2_13 ),
			             J_c3 = 0.25 * ( J_c3_01 + J_c3_23 );
			RESULT ( 0, psi_P, int_psi_x ) = - J_c3 + J_c2;
			RESULT ( 0, psi_P, int_psi_y ) =   J_c1 - J_c0;
			RESULT ( 0, psi_Q, int_psi_x ) = - J_c3 - J_c2;
			RESULT ( 0, psi_Q, int_psi_y ) =   J_c1 + J_c0;
			RESULT ( 0, psi_R, int_psi_x ) =   J_c3 + J_c2;
			RESULT ( 0, psi_R, int_psi_y ) = - J_c1 - J_c0;
			RESULT ( 0, psi_S, int_psi_x ) =   J_c3 - J_c2;
			RESULT ( 0, psi_S, int_psi_y ) = - J_c1 + J_c0;

		}  // end of case 3
		
		break;  // end of case 3 --  Quadrangle::dock_on_comput
	
		case 4 :  // { int psi1 .deriv(x) * psi2 .deriv(y) }
		
		// computations inspired in UFL and FFC, further optimized by hand
		// https://fenics.readthedocs.io/projects/ufl/en/latest/
		// https://fenics.readthedocs.io/projects/ffc/en/latest/

		{	constexpr size_t int_psi_x_psi_x = 0, int_psi_x_psi_y = 1,
			                 int_psi_y_psi_x = 2, int_psi_y_psi_y = 3;

			assert ( this->implemented_cases .find (4) != this->implemented_cases .end() );
			
			const double w_077 = 0.07716049382716043,  // 25/81/4
			             w_123 = 0.1234567901234567,   // 10/81
			             w_197 = 0.1975308641975309;   // 16/81
			const double alph = 0.8872983346207417,    // 0.5 + sqrt(0.15)
			             beta = 0.1127016653792583;    // 0.5 - sqrt(0.15)
			double J_c0_036 = alph * xRmxQ + beta * xSmxP,
			       J_c0_147 = 0.5 * ( xRmxQ +  xSmxP ),
			       J_c0_258 = beta * xRmxQ + alph * xSmxP,
			       J_c1_012 = alph * xPmxQ + beta * xSmxR,
			       J_c1_345 = 0.5 * ( xPmxQ + xSmxR ),
			       J_c1_678 = beta * xPmxQ + alph * xSmxR,
			       J_c2_036 = alph * yRmyQ + beta * ySmyP,
			       J_c2_147 = 0.5 * ( yRmyQ + ySmyP ),
			       J_c2_258 = beta * yRmyQ + alph * ySmyP,
			       J_c3_012 = alph * yPmyQ + beta * ySmyR,
			       J_c3_345 = 0.5 * ( yPmyQ + ySmyR ),
			       J_c3_678 = beta * yPmyQ + alph * ySmyR;
			
			const double det_0 = J_c0_036 * J_c3_012 - J_c1_012 * J_c2_036,
			             det_1 = J_c0_147 * J_c3_012 - J_c1_012 * J_c2_147,
			             det_2 = J_c0_258 * J_c3_012 - J_c1_012 * J_c2_258,
			             det_3 = J_c0_036 * J_c3_345 - J_c1_345 * J_c2_036,
			             det_4 = J_c0_147 * J_c3_345 - J_c1_345 * J_c2_147,
			             det_5 = J_c0_258 * J_c3_345 - J_c1_345 * J_c2_258,
			             det_6 = J_c0_036 * J_c3_678 - J_c1_678 * J_c2_036,
			             det_7 = J_c0_147 * J_c3_678 - J_c1_678 * J_c2_147,
			             det_8 = J_c0_258 * J_c3_678 - J_c1_678 * J_c2_258;
			assert ( det_0 > 0. );  // det = area
			assert ( det_1 > 0. );  // det = area
			assert ( det_2 > 0. );  // det = area
			assert ( det_3 > 0. );  // det = area
			assert ( det_4 > 0. );  // det = area
			assert ( det_5 > 0. );  // det = area
			assert ( det_6 > 0. );  // det = area
			assert ( det_7 > 0. );  // det = area
			assert ( det_8 > 0. );  // det = area

			const double wd_0 = w_077 / det_0, wd_1 = w_123 / det_1, wd_2 = w_077 / det_2,
			             wd_3 = w_123 / det_3, wd_4 = w_197 / det_4, wd_5 = w_123 / det_5,
			             wd_6 = w_077 / det_6, wd_7 = w_123 / det_7, wd_8 = w_077 / det_8;

			const double alph_alph = alph * alph, beta_beta = beta * beta,
			             alph_beta = alph * beta, beta_alph = alph_beta,
			             half_alph = 0.5 * alph,  alph_half = half_alph,
			             half_beta = 0.5 * beta,  beta_half = half_beta;

			const double J_c3_012_c3_012 = J_c3_012 * J_c3_012,
			             J_c3_345_c3_345 = J_c3_345 * J_c3_345,
			             J_c3_678_c3_678 = J_c3_678 * J_c3_678;
			
			double tmp =    J_c3_012_c3_012 * ( alph_alph * wd_0 + 0.25 * wd_1 + beta_beta * wd_2 )
			             + J_c3_345_c3_345 * ( alph_alph * wd_3 + 0.25 * wd_4 + beta_beta * wd_5 )
			             + J_c3_678_c3_678 * ( alph_alph * wd_6 + 0.25 * wd_7 + beta_beta * wd_8 );
			RESULT ( psi_Q, psi_Q, int_psi_x_psi_x ) =
			RESULT ( psi_R, psi_R, int_psi_x_psi_x ) = tmp;
			RESULT ( psi_Q, psi_R, int_psi_x_psi_x ) =
			RESULT ( psi_R, psi_Q, int_psi_x_psi_x ) = - tmp;
			tmp =   J_c3_012_c3_012 * ( beta_beta * wd_0 + 0.25 * wd_1 + alph_alph * wd_2 )
			      + J_c3_345_c3_345 * ( beta_beta * wd_3 + 0.25 * wd_4 + alph_alph * wd_5 )
			      + J_c3_678_c3_678 * ( beta_beta * wd_6 + 0.25 * wd_7 + alph_alph * wd_8 );
			RESULT ( psi_P, psi_P, int_psi_x_psi_x ) =
			RESULT ( psi_S, psi_S, int_psi_x_psi_x ) = tmp;
			RESULT ( psi_P, psi_S, int_psi_x_psi_x ) =
			RESULT ( psi_S, psi_P, int_psi_x_psi_x ) = - tmp;
			tmp =   J_c3_012_c3_012 * ( alph_beta * wd_0 + 0.25 * wd_1 + beta_alph * wd_2 )
			      + J_c3_345_c3_345 * ( alph_beta * wd_3 + 0.25 * wd_4 + beta_alph * wd_5 )
			      + J_c3_678_c3_678 * ( alph_beta * wd_6 + 0.25 * wd_7 + beta_alph * wd_8 );
			RESULT ( psi_Q, psi_P, int_psi_x_psi_x ) =
			RESULT ( psi_P, psi_Q, int_psi_x_psi_x ) =
			RESULT ( psi_R, psi_S, int_psi_x_psi_x ) =
			RESULT ( psi_S, psi_R, int_psi_x_psi_x ) = tmp;
			RESULT ( psi_Q, psi_S, int_psi_x_psi_x ) =
			RESULT ( psi_S, psi_Q, int_psi_x_psi_x ) =
			RESULT ( psi_P, psi_R, int_psi_x_psi_x ) =
			RESULT ( psi_R, psi_P, int_psi_x_psi_x ) = - tmp;

			const double J_c2_036_c3_012 = J_c2_036 * J_c3_012,
			             J_c2_147_c3_012 = J_c2_147 * J_c3_012,
			             J_c2_258_c3_012 = J_c2_258 * J_c3_012,
			             J_c2_036_c3_345 = J_c2_036 * J_c3_345,
			             J_c2_147_c3_345 = J_c2_147 * J_c3_345,
			             J_c2_258_c3_345 = J_c2_258 * J_c3_345,
			             J_c2_036_c3_678 = J_c2_036 * J_c3_678,
			             J_c2_147_c3_678 = J_c2_147 * J_c3_678,
			             J_c2_258_c3_678 = J_c2_258 * J_c3_678;
				
			tmp =   J_c2_036_c3_012 * alph_alph * wd_0
			      + J_c2_147_c3_012 * alph_half * wd_1
			      + J_c2_258_c3_012 * alph_beta * wd_2
			      + J_c2_036_c3_345 * half_alph * wd_3
			      + J_c2_147_c3_345 * 0.25      * wd_4
			      + J_c2_258_c3_345 * half_beta * wd_5
			      + J_c2_036_c3_678 * beta_alph * wd_6
			      + J_c2_147_c3_678 * beta_half * wd_7
			      + J_c2_258_c3_678 * beta_beta * wd_8;
			RESULT ( psi_Q, psi_Q, int_psi_x_psi_x ) -= tmp + tmp;
			RESULT ( psi_Q, psi_P, int_psi_x_psi_x ) += tmp;
			RESULT ( psi_P, psi_Q, int_psi_x_psi_x ) += tmp;
			RESULT ( psi_Q, psi_R, int_psi_x_psi_x ) += tmp;
			RESULT ( psi_R, psi_Q, int_psi_x_psi_x ) += tmp;
			RESULT ( psi_P, psi_R, int_psi_x_psi_x ) -= tmp;
			RESULT ( psi_R, psi_P, int_psi_x_psi_x ) -= tmp;
			tmp =   J_c2_036_c3_012 * alph_beta * wd_0
			      + J_c2_147_c3_012 * alph_half * wd_1
			      + J_c2_258_c3_012 * alph_alph * wd_2
			      + J_c2_036_c3_345 * half_beta * wd_3
			      + J_c2_147_c3_345 * 0.25      * wd_4
			      + J_c2_258_c3_345 * half_alph * wd_5
			      + J_c2_036_c3_678 * beta_beta * wd_6
			      + J_c2_147_c3_678 * beta_half * wd_7
			      + J_c2_258_c3_678 * beta_alph * wd_8;
			RESULT ( psi_P, psi_P, int_psi_x_psi_x ) += tmp + tmp;
			RESULT ( psi_Q, psi_P, int_psi_x_psi_x ) -= tmp;
			RESULT ( psi_P, psi_Q, int_psi_x_psi_x ) -= tmp;
			RESULT ( psi_P, psi_S, int_psi_x_psi_x ) -= tmp;
			RESULT ( psi_S, psi_P, int_psi_x_psi_x ) -= tmp;
			RESULT ( psi_Q, psi_S, int_psi_x_psi_x ) += tmp;
			RESULT ( psi_S, psi_Q, int_psi_x_psi_x ) += tmp;
			tmp =   J_c2_036_c3_012 * beta_alph * wd_0
			      + J_c2_147_c3_012 * beta_half * wd_1
			      + J_c2_258_c3_012 * beta_beta * wd_2
			      + J_c2_036_c3_345 * half_alph * wd_3
			      + J_c2_147_c3_345 * 0.25      * wd_4
			      + J_c2_258_c3_345 * half_beta * wd_5
			      + J_c2_036_c3_678 * alph_alph * wd_6
			      + J_c2_147_c3_678 * alph_half * wd_7
			      + J_c2_258_c3_678 * alph_beta * wd_8;
			RESULT ( psi_R, psi_R, int_psi_x_psi_x ) += tmp + tmp;
			RESULT ( psi_Q, psi_R, int_psi_x_psi_x ) -= tmp;
			RESULT ( psi_R, psi_Q, int_psi_x_psi_x ) -= tmp;
			RESULT ( psi_R, psi_S, int_psi_x_psi_x ) -= tmp;
			RESULT ( psi_S, psi_R, int_psi_x_psi_x ) -= tmp;
			RESULT ( psi_Q, psi_S, int_psi_x_psi_x ) += tmp;
			RESULT ( psi_S, psi_Q, int_psi_x_psi_x ) += tmp;
			tmp =   J_c2_036_c3_012 * beta_beta * wd_0
			      + J_c2_147_c3_012 * beta_half * wd_1
			      + J_c2_258_c3_012 * beta_alph * wd_2
			      + J_c2_036_c3_345 * half_beta * wd_3
			      + J_c2_147_c3_345 * 0.25      * wd_4
			      + J_c2_258_c3_345 * half_alph * wd_5
			      + J_c2_036_c3_678 * alph_beta * wd_6
			      + J_c2_147_c3_678 * alph_half * wd_7
			      + J_c2_258_c3_678 * alph_alph * wd_8;
			RESULT ( psi_S, psi_S, int_psi_x_psi_x ) -= tmp + tmp;
			RESULT ( psi_P, psi_S, int_psi_x_psi_x ) += tmp;
			RESULT ( psi_S, psi_P, int_psi_x_psi_x ) += tmp;
			RESULT ( psi_R, psi_S, int_psi_x_psi_x ) += tmp;
			RESULT ( psi_S, psi_R, int_psi_x_psi_x ) += tmp;
			RESULT ( psi_P, psi_R, int_psi_x_psi_x ) -= tmp;
			RESULT ( psi_R, psi_P, int_psi_x_psi_x ) -= tmp;

			const double J_c2_036_c2_036 = J_c2_036 * J_c2_036,
			             J_c2_147_c2_147 = J_c2_147 * J_c2_147,
			             J_c2_258_c2_258 = J_c2_258 * J_c2_258;
				
			tmp =   J_c2_036_c2_036 * alph_alph * wd_0
			      + J_c2_036_c2_036 * 0.25      * wd_3
			      + J_c2_036_c2_036 * beta_beta * wd_6
			      + J_c2_147_c2_147 * alph_alph * wd_1
			      + J_c2_147_c2_147 * 0.25      * wd_4
			      + J_c2_147_c2_147 * beta_beta * wd_7
			      + J_c2_258_c2_258 * alph_alph * wd_2
			      + J_c2_258_c2_258 * 0.25      * wd_5
			      + J_c2_258_c2_258 * beta_beta * wd_8;
			RESULT ( psi_Q, psi_Q, int_psi_x_psi_x ) += tmp;
			RESULT ( psi_P, psi_P, int_psi_x_psi_x ) += tmp;
			RESULT ( psi_Q, psi_P, int_psi_x_psi_x ) -= tmp;
			RESULT ( psi_P, psi_Q, int_psi_x_psi_x ) -= tmp;
			tmp = J_c2_036_c2_036 * ( beta_beta * wd_0 + 0.25 * wd_3 + alph_alph * wd_6 )
			    + J_c2_147_c2_147 * ( beta_beta * wd_1 + 0.25 * wd_4 + alph_alph * wd_7 )
			    + J_c2_258_c2_258 * ( beta_beta * wd_2 + 0.25 * wd_5 + alph_alph * wd_8 );
			RESULT ( psi_R, psi_R, int_psi_x_psi_x ) += tmp;
			RESULT ( psi_S, psi_S, int_psi_x_psi_x ) += tmp;
			RESULT ( psi_R, psi_S, int_psi_x_psi_x ) -= tmp;
			RESULT ( psi_S, psi_R, int_psi_x_psi_x ) -= tmp;
			tmp = J_c2_036_c2_036 * ( alph_beta * wd_0 + 0.25 * wd_3 + beta_alph * wd_6 )
			    + J_c2_147_c2_147 * ( alph_beta * wd_1 + 0.25 * wd_4 + beta_alph * wd_7 )
			    + J_c2_258_c2_258 * ( alph_beta * wd_2 + 0.25 * wd_5 + beta_alph * wd_8 );
			RESULT ( psi_Q, psi_R, int_psi_x_psi_x ) += tmp;
			RESULT ( psi_R, psi_Q, int_psi_x_psi_x ) += tmp;
			RESULT ( psi_Q, psi_S, int_psi_x_psi_x ) -= tmp;
			RESULT ( psi_S, psi_Q, int_psi_x_psi_x ) -= tmp;
			RESULT ( psi_P, psi_R, int_psi_x_psi_x ) -= tmp;
			RESULT ( psi_R, psi_P, int_psi_x_psi_x ) -= tmp;
			RESULT ( psi_P, psi_S, int_psi_x_psi_x ) += tmp;
			RESULT ( psi_S, psi_P, int_psi_x_psi_x ) += tmp;

			const double J_c1_012_c3_012 = J_c1_012 * J_c3_012,
			             J_c1_345_c3_345 = J_c1_345 * J_c3_345,
			             J_c1_678_c3_678 = J_c1_678 * J_c3_678;
			
			tmp =   J_c1_012_c3_012 * ( alph_alph * wd_0 + 0.25 * wd_1 + beta_beta * wd_2 )
			      + J_c1_345_c3_345 * ( alph_alph * wd_3 + 0.25 * wd_4 + beta_beta * wd_5 )
			      + J_c1_678_c3_678 * ( alph_alph * wd_6 + 0.25 * wd_7 + beta_beta * wd_8 );
			RESULT ( psi_Q, psi_Q, int_psi_x_psi_y ) =
			RESULT ( psi_R, psi_R, int_psi_x_psi_y ) =
			RESULT ( psi_Q, psi_Q, int_psi_y_psi_x ) =
			RESULT ( psi_R, psi_R, int_psi_y_psi_x ) = - tmp;
			RESULT ( psi_Q, psi_R, int_psi_x_psi_y ) =
			RESULT ( psi_R, psi_Q, int_psi_x_psi_y ) =
			RESULT ( psi_Q, psi_R, int_psi_y_psi_x ) =
			RESULT ( psi_R, psi_Q, int_psi_y_psi_x ) = tmp;
			tmp =   J_c1_012_c3_012 * ( beta_beta * wd_0 + 0.25 * wd_1 + alph_alph * wd_2 )
			      + J_c1_345_c3_345 * ( beta_beta * wd_3 + 0.25 * wd_4 + alph_alph * wd_5 )
			      + J_c1_678_c3_678 * ( beta_beta * wd_6 + 0.25 * wd_7 + alph_alph * wd_8 );
			RESULT ( psi_P, psi_P, int_psi_x_psi_y ) =
			RESULT ( psi_S, psi_S, int_psi_x_psi_y ) =
			RESULT ( psi_P, psi_P, int_psi_y_psi_x ) =
			RESULT ( psi_S, psi_S, int_psi_y_psi_x ) = - tmp;
			RESULT ( psi_P, psi_S, int_psi_x_psi_y ) =
			RESULT ( psi_S, psi_P, int_psi_x_psi_y ) =
			RESULT ( psi_P, psi_S, int_psi_y_psi_x ) =
			RESULT ( psi_S, psi_P, int_psi_y_psi_x ) = tmp;
			tmp =   J_c1_012_c3_012 * ( alph_beta * wd_0 + 0.25 * wd_1 + beta_alph * wd_2 )
			      + J_c1_345_c3_345 * ( alph_beta * wd_3 + 0.25 * wd_4 + beta_alph * wd_5 )
			      + J_c1_678_c3_678 * ( alph_beta * wd_6 + 0.25 * wd_7 + beta_alph * wd_8 );
			RESULT ( psi_Q, psi_P, int_psi_y_psi_x ) =
			RESULT ( psi_P, psi_Q, int_psi_y_psi_x ) =
			RESULT ( psi_R, psi_S, int_psi_y_psi_x ) =
			RESULT ( psi_S, psi_R, int_psi_y_psi_x ) =
			RESULT ( psi_Q, psi_P, int_psi_x_psi_y ) =
			RESULT ( psi_P, psi_Q, int_psi_x_psi_y ) =
			RESULT ( psi_R, psi_S, int_psi_x_psi_y ) =
			RESULT ( psi_S, psi_R, int_psi_x_psi_y ) = - tmp;
			RESULT ( psi_Q, psi_S, int_psi_y_psi_x ) =
			RESULT ( psi_S, psi_Q, int_psi_y_psi_x ) =
			RESULT ( psi_P, psi_R, int_psi_y_psi_x ) =
			RESULT ( psi_R, psi_P, int_psi_y_psi_x ) =
			RESULT ( psi_Q, psi_S, int_psi_x_psi_y ) =
			RESULT ( psi_S, psi_Q, int_psi_x_psi_y ) =
			RESULT ( psi_P, psi_R, int_psi_x_psi_y ) =
			RESULT ( psi_R, psi_P, int_psi_x_psi_y ) = tmp;

			const double J_c1_012_c2_036 = J_c1_012 * J_c2_036,
			             J_c1_012_c2_147 = J_c1_012 * J_c2_147,
			             J_c1_012_c2_258 = J_c1_012 * J_c2_258,
			             J_c1_345_c2_036 = J_c1_345 * J_c2_036,
			             J_c1_345_c2_147 = J_c1_345 * J_c2_147,
			             J_c1_345_c2_258 = J_c1_345 * J_c2_258,
			             J_c1_678_c2_036 = J_c1_678 * J_c2_036,
			             J_c1_678_c2_147 = J_c1_678 * J_c2_147,
			             J_c1_678_c2_258 = J_c1_678 * J_c2_258;
				
			tmp =   J_c1_012_c2_036 * alph_alph * wd_0
			      + J_c1_012_c2_147 * alph_half * wd_1
			      + J_c1_012_c2_258 * alph_beta * wd_2
			      + J_c1_345_c2_036 * half_alph * wd_3
			      + J_c1_345_c2_147 * 0.25      * wd_4
			      + J_c1_345_c2_258 * half_beta * wd_5
			      + J_c1_678_c2_036 * beta_alph * wd_6
			      + J_c1_678_c2_147 * beta_half * wd_7
			      + J_c1_678_c2_258 * beta_beta * wd_8;
			RESULT ( psi_Q, psi_Q, int_psi_y_psi_x ) += tmp;
			RESULT ( psi_Q, psi_P, int_psi_y_psi_x ) -= tmp;
			RESULT ( psi_R, psi_Q, int_psi_y_psi_x ) -= tmp;
			RESULT ( psi_R, psi_P, int_psi_y_psi_x ) += tmp;
			RESULT ( psi_Q, psi_Q, int_psi_x_psi_y ) += tmp;
			RESULT ( psi_P, psi_Q, int_psi_x_psi_y ) -= tmp;
			RESULT ( psi_Q, psi_R, int_psi_x_psi_y ) -= tmp;
			RESULT ( psi_P, psi_R, int_psi_x_psi_y ) += tmp;
			tmp =   J_c1_012_c2_036 * alph_beta * wd_0
			      + J_c1_012_c2_147 * alph_half * wd_1
			      + J_c1_012_c2_258 * alph_alph * wd_2
			      + J_c1_345_c2_036 * half_beta * wd_3
			      + J_c1_345_c2_147 * 0.25      * wd_4
			      + J_c1_345_c2_258 * half_alph * wd_5
			      + J_c1_678_c2_036 * beta_beta * wd_6
			      + J_c1_678_c2_147 * beta_half * wd_7
			      + J_c1_678_c2_258 * beta_alph * wd_8;
			RESULT ( psi_P, psi_Q, int_psi_y_psi_x ) += tmp;
			RESULT ( psi_P, psi_P, int_psi_y_psi_x ) -= tmp;
			RESULT ( psi_S, psi_Q, int_psi_y_psi_x ) -= tmp;
			RESULT ( psi_S, psi_P, int_psi_y_psi_x ) += tmp;
			RESULT ( psi_Q, psi_P, int_psi_x_psi_y ) += tmp;
			RESULT ( psi_P, psi_P, int_psi_x_psi_y ) -= tmp;
			RESULT ( psi_Q, psi_S, int_psi_x_psi_y ) -= tmp;
			RESULT ( psi_P, psi_S, int_psi_x_psi_y ) += tmp;
			tmp =   J_c1_012_c2_036 * beta_alph * wd_0
			      + J_c1_012_c2_147 * beta_half * wd_1
			      + J_c1_012_c2_258 * beta_beta * wd_2
			      + J_c1_345_c2_036 * half_alph * wd_3
			      + J_c1_345_c2_147 * 0.25      * wd_4
			      + J_c1_345_c2_258 * half_beta * wd_5
			      + J_c1_678_c2_036 * alph_alph * wd_6
			      + J_c1_678_c2_147 * alph_half * wd_7
			      + J_c1_678_c2_258 * alph_beta * wd_8;
			RESULT ( psi_Q, psi_R, int_psi_y_psi_x ) += tmp;
			RESULT ( psi_Q, psi_S, int_psi_y_psi_x ) -= tmp;
			RESULT ( psi_R, psi_R, int_psi_y_psi_x ) -= tmp;
			RESULT ( psi_R, psi_S, int_psi_y_psi_x ) += tmp;
			RESULT ( psi_R, psi_Q, int_psi_x_psi_y ) += tmp;
			RESULT ( psi_S, psi_Q, int_psi_x_psi_y ) -= tmp;
			RESULT ( psi_R, psi_R, int_psi_x_psi_y ) -= tmp;
			RESULT ( psi_S, psi_R, int_psi_x_psi_y ) += tmp;
			tmp =   J_c1_012_c2_036 * beta_beta * wd_0
			      + J_c1_012_c2_147 * beta_half * wd_1
			      + J_c1_012_c2_258 * beta_alph * wd_2
			      + J_c1_345_c2_036 * half_beta * wd_3
			      + J_c1_345_c2_147 * 0.25      * wd_4
			      + J_c1_345_c2_258 * half_alph * wd_5
			      + J_c1_678_c2_036 * alph_beta * wd_6
			      + J_c1_678_c2_147 * alph_half * wd_7
			      + J_c1_678_c2_258 * alph_alph * wd_8;
			RESULT ( psi_P, psi_R, int_psi_y_psi_x ) += tmp;
			RESULT ( psi_P, psi_S, int_psi_y_psi_x ) -= tmp;
			RESULT ( psi_S, psi_R, int_psi_y_psi_x ) -= tmp;
			RESULT ( psi_S, psi_S, int_psi_y_psi_x ) += tmp;
			RESULT ( psi_R, psi_P, int_psi_x_psi_y ) += tmp;
			RESULT ( psi_S, psi_P, int_psi_x_psi_y ) -= tmp;
			RESULT ( psi_R, psi_S, int_psi_x_psi_y ) -= tmp;
			RESULT ( psi_S, psi_S, int_psi_x_psi_y ) += tmp;

			const double J_c0_036_c3_012 = J_c0_036 * J_c3_012,
			             J_c0_147_c3_012 = J_c0_147 * J_c3_012,
			             J_c0_258_c3_012 = J_c0_258 * J_c3_012,
			             J_c0_036_c3_345 = J_c0_036 * J_c3_345,
			             J_c0_147_c3_345 = J_c0_147 * J_c3_345,
			             J_c0_258_c3_345 = J_c0_258 * J_c3_345,
			             J_c0_036_c3_678 = J_c0_036 * J_c3_678,
			             J_c0_147_c3_678 = J_c0_147 * J_c3_678,
			             J_c0_258_c3_678 = J_c0_258 * J_c3_678;
				
			tmp =   J_c0_036_c3_012 * alph_alph * wd_0
			      + J_c0_147_c3_012 * alph_half * wd_1
			      + J_c0_258_c3_012 * alph_beta * wd_2
			      + J_c0_036_c3_345 * half_alph * wd_3
			      + J_c0_147_c3_345 * 0.25      * wd_4
			      + J_c0_258_c3_345 * half_beta * wd_5
			      + J_c0_036_c3_678 * beta_alph * wd_6
			      + J_c0_147_c3_678 * beta_half * wd_7
			      + J_c0_258_c3_678 * beta_beta * wd_8;
			RESULT ( psi_Q, psi_Q, int_psi_y_psi_x ) += tmp;
			RESULT ( psi_P, psi_Q, int_psi_y_psi_x ) -= tmp;
			RESULT ( psi_Q, psi_R, int_psi_y_psi_x ) -= tmp;
			RESULT ( psi_P, psi_R, int_psi_y_psi_x ) += tmp;
			RESULT ( psi_Q, psi_Q, int_psi_x_psi_y ) += tmp;
			RESULT ( psi_Q, psi_P, int_psi_x_psi_y ) -= tmp;
			RESULT ( psi_R, psi_Q, int_psi_x_psi_y ) -= tmp;
			RESULT ( psi_R, psi_P, int_psi_x_psi_y ) += tmp;
			tmp =   J_c0_036_c3_012 * alph_beta * wd_0
			      + J_c0_147_c3_012 * alph_half * wd_1
			      + J_c0_258_c3_012 * alph_alph * wd_2
			      + J_c0_036_c3_345 * half_beta * wd_3
			      + J_c0_147_c3_345 * 0.25      * wd_4
			      + J_c0_258_c3_345 * half_alph * wd_5
			      + J_c0_036_c3_678 * beta_beta * wd_6
			      + J_c0_147_c3_678 * beta_half * wd_7
			      + J_c0_258_c3_678 * beta_alph * wd_8;
			RESULT ( psi_Q, psi_P, int_psi_y_psi_x ) += tmp;
			RESULT ( psi_Q, psi_S, int_psi_y_psi_x ) -= tmp;
			RESULT ( psi_P, psi_P, int_psi_y_psi_x ) -= tmp;
			RESULT ( psi_P, psi_S, int_psi_y_psi_x ) += tmp;
			RESULT ( psi_P, psi_Q, int_psi_x_psi_y ) += tmp;
			RESULT ( psi_S, psi_Q, int_psi_x_psi_y ) -= tmp;
			RESULT ( psi_P, psi_P, int_psi_x_psi_y ) -= tmp;
			RESULT ( psi_S, psi_P, int_psi_x_psi_y ) += tmp;
			tmp =   J_c0_036_c3_012 * beta_alph * wd_0
			      + J_c0_147_c3_012 * beta_half * wd_1
			      + J_c0_258_c3_012 * beta_beta * wd_2
			      + J_c0_036_c3_345 * half_alph * wd_3
			      + J_c0_147_c3_345 * 0.25      * wd_4
			      + J_c0_258_c3_345 * half_beta * wd_5
			      + J_c0_036_c3_678 * alph_alph * wd_6
			      + J_c0_147_c3_678 * alph_half * wd_7
			      + J_c0_258_c3_678 * alph_beta * wd_8;
			RESULT ( psi_R, psi_Q, int_psi_y_psi_x ) += tmp;
			RESULT ( psi_R, psi_R, int_psi_y_psi_x ) -= tmp;
			RESULT ( psi_S, psi_Q, int_psi_y_psi_x ) -= tmp;
			RESULT ( psi_S, psi_R, int_psi_y_psi_x ) += tmp;
			RESULT ( psi_Q, psi_R, int_psi_x_psi_y ) += tmp;
			RESULT ( psi_R, psi_R, int_psi_x_psi_y ) -= tmp;
			RESULT ( psi_Q, psi_S, int_psi_x_psi_y ) -= tmp;
			RESULT ( psi_R, psi_S, int_psi_x_psi_y ) += tmp;
			tmp =   J_c0_036_c3_012 * beta_beta * wd_0
			      + J_c0_147_c3_012 * beta_half * wd_1
			      + J_c0_258_c3_012 * beta_alph * wd_2
			      + J_c0_036_c3_345 * half_beta * wd_3
			      + J_c0_147_c3_345 * 0.25      * wd_4
			      + J_c0_258_c3_345 * half_alph * wd_5
			      + J_c0_036_c3_678 * alph_beta * wd_6
			      + J_c0_147_c3_678 * alph_half * wd_7
			      + J_c0_258_c3_678 * alph_alph * wd_8;
			RESULT ( psi_R, psi_P, int_psi_y_psi_x ) += tmp;
			RESULT ( psi_R, psi_S, int_psi_y_psi_x ) -= tmp;
			RESULT ( psi_S, psi_P, int_psi_y_psi_x ) -= tmp;
			RESULT ( psi_S, psi_S, int_psi_y_psi_x ) += tmp;
			RESULT ( psi_P, psi_R, int_psi_x_psi_y ) += tmp;
			RESULT ( psi_S, psi_R, int_psi_x_psi_y ) -= tmp;
			RESULT ( psi_P, psi_S, int_psi_x_psi_y ) -= tmp;
			RESULT ( psi_S, psi_S, int_psi_x_psi_y ) += tmp;

			const double J_c0_036_c2_036 = J_c0_036 * J_c2_036,
			             J_c0_147_c2_147 = J_c0_147 * J_c2_147,
			             J_c0_258_c2_258 = J_c0_258 * J_c2_258;
				
			tmp =   J_c0_036_c2_036 * ( alph_alph * wd_0 + 0.25 * wd_3 + beta_beta * wd_6 )
			      + J_c0_147_c2_147 * ( alph_alph * wd_1 + 0.25 * wd_4 + beta_beta * wd_7 )
			      + J_c0_258_c2_258 * ( alph_alph * wd_2 + 0.25 * wd_5 + beta_beta * wd_8 );
			RESULT ( psi_Q, psi_Q, int_psi_x_psi_y ) -= tmp;
			RESULT ( psi_P, psi_P, int_psi_x_psi_y ) -= tmp;
			RESULT ( psi_Q, psi_Q, int_psi_y_psi_x ) -= tmp;
			RESULT ( psi_P, psi_P, int_psi_y_psi_x ) -= tmp;
			RESULT ( psi_Q, psi_P, int_psi_x_psi_y ) += tmp;
			RESULT ( psi_P, psi_Q, int_psi_x_psi_y ) += tmp;
			RESULT ( psi_Q, psi_P, int_psi_y_psi_x ) += tmp;
			RESULT ( psi_P, psi_Q, int_psi_y_psi_x ) += tmp;
			tmp =   J_c0_036_c2_036 * ( alph_beta * wd_0 + 0.25 * wd_3 + beta_alph * wd_6 )
			      + J_c0_147_c2_147 * ( alph_beta * wd_1 + 0.25 * wd_4 + beta_alph * wd_7 )
			      + J_c0_258_c2_258 * ( alph_beta * wd_2 + 0.25 * wd_5 + beta_alph * wd_8 );
			RESULT ( psi_Q, psi_R, int_psi_x_psi_y ) -= tmp;
			RESULT ( psi_R, psi_Q, int_psi_x_psi_y ) -= tmp;
			RESULT ( psi_Q, psi_S, int_psi_x_psi_y ) += tmp;
			RESULT ( psi_S, psi_Q, int_psi_x_psi_y ) += tmp;
			RESULT ( psi_P, psi_R, int_psi_x_psi_y ) += tmp;
			RESULT ( psi_R, psi_P, int_psi_x_psi_y ) += tmp;
			RESULT ( psi_P, psi_S, int_psi_x_psi_y ) -= tmp;
			RESULT ( psi_S, psi_P, int_psi_x_psi_y ) -= tmp;
			RESULT ( psi_Q, psi_R, int_psi_y_psi_x ) -= tmp;
			RESULT ( psi_R, psi_Q, int_psi_y_psi_x ) -= tmp;
			RESULT ( psi_Q, psi_S, int_psi_y_psi_x ) += tmp;
			RESULT ( psi_S, psi_Q, int_psi_y_psi_x ) += tmp;
			RESULT ( psi_P, psi_R, int_psi_y_psi_x ) += tmp;
			RESULT ( psi_R, psi_P, int_psi_y_psi_x ) += tmp;
			RESULT ( psi_P, psi_S, int_psi_y_psi_x ) -= tmp;
			RESULT ( psi_S, psi_P, int_psi_y_psi_x ) -= tmp;
			tmp =   J_c0_036_c2_036 * ( beta_beta * wd_0 + 0.25 * wd_3 + alph_alph * wd_6 )
			      + J_c0_147_c2_147 * ( beta_beta * wd_1 + 0.25 * wd_4 + alph_alph * wd_7 )
			      + J_c0_258_c2_258 * ( beta_beta * wd_2 + 0.25 * wd_5 + alph_alph * wd_8 );
			RESULT ( psi_R, psi_R, int_psi_x_psi_y ) -= tmp;
			RESULT ( psi_S, psi_S, int_psi_x_psi_y ) -= tmp;
			RESULT ( psi_R, psi_R, int_psi_y_psi_x ) -= tmp;
			RESULT ( psi_S, psi_S, int_psi_y_psi_x ) -= tmp;
			RESULT ( psi_R, psi_S, int_psi_x_psi_y ) += tmp;
			RESULT ( psi_S, psi_R, int_psi_x_psi_y ) += tmp;
			RESULT ( psi_R, psi_S, int_psi_y_psi_x ) += tmp;
			RESULT ( psi_S, psi_R, int_psi_y_psi_x ) += tmp;

			const double J_c1_012_c1_012 = J_c1_012 * J_c1_012,
			             J_c1_345_c1_345 = J_c1_345 * J_c1_345,
			             J_c1_678_c1_678 = J_c1_678 * J_c1_678;
				
			tmp =   J_c1_012_c1_012 * ( alph_alph * wd_0 + 0.25 * wd_1 + beta_beta * wd_2 )
			      + J_c1_345_c1_345 * ( alph_alph * wd_3 + 0.25 * wd_4 + beta_beta * wd_5 )
			      + J_c1_678_c1_678 * ( alph_alph * wd_6 + 0.25 * wd_7 + beta_beta * wd_8 );
			RESULT ( psi_Q, psi_Q, int_psi_y_psi_y ) += tmp;
			RESULT ( psi_Q, psi_R, int_psi_y_psi_y ) -= tmp;
			RESULT ( psi_R, psi_Q, int_psi_y_psi_y ) -= tmp;
			RESULT ( psi_R, psi_R, int_psi_y_psi_y ) += tmp;
			tmp = J_c1_012_c1_012 * ( alph_beta * wd_0 + 0.25 * wd_1 + beta_alph * wd_2 )
			    + J_c1_345_c1_345 * ( alph_beta * wd_3 + 0.25 * wd_4 + beta_alph * wd_5 )
			    + J_c1_678_c1_678 * ( alph_beta * wd_6 + 0.25 * wd_7 + beta_alph * wd_8 );
			RESULT ( psi_Q, psi_P, int_psi_y_psi_y ) += tmp;
			RESULT ( psi_P, psi_Q, int_psi_y_psi_y ) += tmp;
			RESULT ( psi_Q, psi_S, int_psi_y_psi_y ) -= tmp;
			RESULT ( psi_S, psi_Q, int_psi_y_psi_y ) -= tmp;
			RESULT ( psi_P, psi_R, int_psi_y_psi_y ) -= tmp;
			RESULT ( psi_R, psi_P, int_psi_y_psi_y ) -= tmp;
			RESULT ( psi_R, psi_S, int_psi_y_psi_y ) += tmp;
			RESULT ( psi_S, psi_R, int_psi_y_psi_y ) += tmp;
			tmp =   J_c1_012_c1_012 * ( beta_beta * wd_0 + 0.25 * wd_1 + alph_alph * wd_2 )
			      + J_c1_345_c1_345 * ( beta_beta * wd_3 + 0.25 * wd_4 + alph_alph * wd_5 )
			      + J_c1_678_c1_678 * ( beta_beta * wd_6 + 0.25 * wd_7 + alph_alph * wd_8 );
			RESULT ( psi_P, psi_P, int_psi_y_psi_y ) += tmp;
			RESULT ( psi_S, psi_S, int_psi_y_psi_y ) += tmp;
			RESULT ( psi_P, psi_S, int_psi_y_psi_y ) -= tmp;
			RESULT ( psi_S, psi_P, int_psi_y_psi_y ) -= tmp;

			const double J_c0_036_c1_012 = J_c0_036 * J_c1_012,
			             J_c0_147_c1_012 = J_c0_147 * J_c1_012,
			             J_c0_258_c1_012 = J_c0_258 * J_c1_012,
			             J_c0_036_c1_345 = J_c0_036 * J_c1_345,
			             J_c0_147_c1_345 = J_c0_147 * J_c1_345,
			             J_c0_258_c1_345 = J_c0_258 * J_c1_345,
			             J_c0_036_c1_678 = J_c0_036 * J_c1_678,
			             J_c0_147_c1_678 = J_c0_147 * J_c1_678,
			             J_c0_258_c1_678 = J_c0_258 * J_c1_678;
			
			tmp =   J_c0_036_c1_012 * alph_alph * wd_0
			      + J_c0_147_c1_012 * alph_half * wd_1
			      + J_c0_258_c1_012 * alph_beta * wd_2
			      + J_c0_036_c1_345 * half_alph * wd_3
			      + J_c0_147_c1_345 * 0.25      * wd_4
			      + J_c0_258_c1_345 * half_beta * wd_5
			      + J_c0_036_c1_678 * beta_alph * wd_6
			      + J_c0_147_c1_678 * beta_half * wd_7
			      + J_c0_258_c1_678 * beta_beta * wd_8;
			RESULT ( psi_Q, psi_Q, int_psi_y_psi_y ) -= tmp + tmp;
			RESULT ( psi_Q, psi_P, int_psi_y_psi_y ) += tmp;
			RESULT ( psi_P, psi_Q, int_psi_y_psi_y ) += tmp;
			RESULT ( psi_Q, psi_R, int_psi_y_psi_y ) += tmp;
			RESULT ( psi_R, psi_Q, int_psi_y_psi_y ) += tmp;
			RESULT ( psi_P, psi_R, int_psi_y_psi_y ) -= tmp;
			RESULT ( psi_R, psi_P, int_psi_y_psi_y ) -= tmp;
			tmp =   J_c0_036_c1_012 * alph_beta * wd_0
			      + J_c0_147_c1_012 * alph_half * wd_1
			      + J_c0_258_c1_012 * alph_alph * wd_2
			      + J_c0_036_c1_345 * half_beta * wd_3
			      + J_c0_147_c1_345 * 0.25      * wd_4
			      + J_c0_258_c1_345 * half_alph * wd_5
			      + J_c0_036_c1_678 * beta_beta * wd_6
			      + J_c0_147_c1_678 * beta_half * wd_7
			      + J_c0_258_c1_678 * beta_alph * wd_8;
			RESULT ( psi_Q, psi_P, int_psi_y_psi_y ) -= tmp;
			RESULT ( psi_P, psi_Q, int_psi_y_psi_y ) -= tmp;
			RESULT ( psi_Q, psi_S, int_psi_y_psi_y ) += tmp;
			RESULT ( psi_S, psi_Q, int_psi_y_psi_y ) += tmp;
			RESULT ( psi_P, psi_P, int_psi_y_psi_y ) += tmp + tmp;
			RESULT ( psi_P, psi_S, int_psi_y_psi_y ) -= tmp;
			RESULT ( psi_S, psi_P, int_psi_y_psi_y ) -= tmp;
			tmp =   J_c0_036_c1_012 * beta_alph * wd_0
			      + J_c0_147_c1_012 * beta_half * wd_1
			      + J_c0_258_c1_012 * beta_beta * wd_2
			      + J_c0_036_c1_345 * half_alph * wd_3
			      + J_c0_147_c1_345 * 0.25      * wd_4
			      + J_c0_258_c1_345 * half_beta * wd_5
			      + J_c0_036_c1_678 * alph_alph * wd_6
			      + J_c0_147_c1_678 * alph_half * wd_7
			      + J_c0_258_c1_678 * alph_beta * wd_8;
			RESULT ( psi_R, psi_Q, int_psi_y_psi_y ) -= tmp;
			RESULT ( psi_Q, psi_R, int_psi_y_psi_y ) -= tmp;
			RESULT ( psi_R, psi_R, int_psi_y_psi_y ) += tmp + tmp;
			RESULT ( psi_S, psi_Q, int_psi_y_psi_y ) += tmp;
			RESULT ( psi_Q, psi_S, int_psi_y_psi_y ) += tmp;
			RESULT ( psi_S, psi_R, int_psi_y_psi_y ) -= tmp;
			RESULT ( psi_R, psi_S, int_psi_y_psi_y ) -= tmp;
			tmp =   J_c0_036_c1_012 * beta_beta * wd_0
			      + J_c0_147_c1_012 * beta_half * wd_1
			      + J_c0_258_c1_012 * beta_alph * wd_2
			      + J_c0_036_c1_345 * half_beta * wd_3
			      + J_c0_147_c1_345 * 0.25      * wd_4
			      + J_c0_258_c1_345 * half_alph * wd_5
			      + J_c0_036_c1_678 * alph_beta * wd_6
			      + J_c0_147_c1_678 * alph_half * wd_7
			      + J_c0_258_c1_678 * alph_alph * wd_8;
			RESULT ( psi_R, psi_P, int_psi_y_psi_y ) -= tmp;
			RESULT ( psi_P, psi_R, int_psi_y_psi_y ) -= tmp;
			RESULT ( psi_R, psi_S, int_psi_y_psi_y ) += tmp;
			RESULT ( psi_S, psi_R, int_psi_y_psi_y ) += tmp;
			RESULT ( psi_S, psi_P, int_psi_y_psi_y ) += tmp;
			RESULT ( psi_P, psi_S, int_psi_y_psi_y ) += tmp;
			RESULT ( psi_S, psi_S, int_psi_y_psi_y ) -= tmp + tmp;

			const double J_c0_036_c0_036 = J_c0_036 * J_c0_036,
			             J_c0_147_c0_147 = J_c0_147 * J_c0_147,
			             J_c0_258_c0_258 = J_c0_258 * J_c0_258;
			
			tmp =   J_c0_036_c0_036 * ( alph_alph * wd_0 + 0.25 * wd_3 + beta_beta * wd_6 )
			      + J_c0_147_c0_147 * ( alph_alph * wd_1 + 0.25 * wd_4 + beta_beta * wd_7 )
			      + J_c0_258_c0_258 * ( alph_alph * wd_2 + 0.25 * wd_5 + beta_beta * wd_8 );
			RESULT ( psi_Q, psi_Q, int_psi_y_psi_y ) += tmp;
			RESULT ( psi_Q, psi_P, int_psi_y_psi_y ) -= tmp;
			RESULT ( psi_P, psi_Q, int_psi_y_psi_y ) -= tmp;
			RESULT ( psi_P, psi_P, int_psi_y_psi_y ) += tmp;
			tmp =   J_c0_036_c0_036 * ( alph_beta * wd_0 + 0.25 * wd_3 + beta_alph * wd_6 )
			      + J_c0_147_c0_147 * ( alph_beta * wd_1 + 0.25 * wd_4 + beta_alph * wd_7 )
			      + J_c0_258_c0_258 * ( alph_beta * wd_2 + 0.25 * wd_5 + beta_alph * wd_8 );
			RESULT ( psi_Q, psi_R, int_psi_y_psi_y ) += tmp;
			RESULT ( psi_R, psi_Q, int_psi_y_psi_y ) += tmp;
			RESULT ( psi_Q, psi_S, int_psi_y_psi_y ) -= tmp;
			RESULT ( psi_S, psi_Q, int_psi_y_psi_y ) -= tmp;
			RESULT ( psi_P, psi_R, int_psi_y_psi_y ) -= tmp;
			RESULT ( psi_R, psi_P, int_psi_y_psi_y ) -= tmp;
			RESULT ( psi_P, psi_S, int_psi_y_psi_y ) += tmp;
			RESULT ( psi_S, psi_P, int_psi_y_psi_y ) += tmp;
			tmp =   J_c0_036_c0_036 * ( beta_beta * wd_0 + 0.25 * wd_3 + alph_alph * wd_6 )
			      + J_c0_147_c0_147 * ( beta_beta * wd_1 + 0.25 * wd_4 + alph_alph * wd_7 )
			      + J_c0_258_c0_258 * ( beta_beta * wd_2 + 0.25 * wd_5 + alph_alph * wd_8 );
			RESULT ( psi_R, psi_R, int_psi_y_psi_y ) += tmp;
			RESULT ( psi_R, psi_S, int_psi_y_psi_y ) -= tmp;
			RESULT ( psi_S, psi_R, int_psi_y_psi_y ) -= tmp;
			RESULT ( psi_S, psi_S, int_psi_y_psi_y ) += tmp;            }
			
		break;  // end of case 4 --  Quadrangle::dock_on_comput

		default : assert ( false );
	}  // end of  switch  statement
	
}  // end of  FiniteElement::StandAlone::TypeOne::Quadrangle::dock_on_comput

//------------------------------------------------------------------------------------------------------//

	
inline void FiniteElement::StandAlone::TypeOne::Rectangle::dock_on_comput
( const double & xP, const double & yP, const double & xQ, const double & yQ,
  const double & xR, const double & yR, const double & xS, const double & yS )

// this function is speed-critical
	
{	const double delta_x = xQ - xP, delta_y = yS - yP, area = delta_x * delta_y;
	// delta_x > 0., delta_y > 0.
	
	constexpr size_t base_d = this->base_dim;
	constexpr size_t c16 = this->case_16;
	constexpr size_t psi_P = 0, psi_Q = 1, psi_R = 2, psi_S = 3;

	#ifndef NDEBUG  // DEBUG mode
	const double tol = 1.e-6 * ( delta_x + delta_y );
	assert ( std::abs ( xP - xS ) < tol );
	assert ( std::abs ( xR - xQ ) < tol );
	assert ( std::abs ( yP - yQ ) < tol );
	assert ( std::abs ( yR - yS ) < tol );
	assert ( xQ > xP );
	assert ( xR > xS );
	assert ( yS > yP );
	assert ( yR > yQ );
	#endif  // DEBUG
	
	// below we use a switch statement
	// we trust that the compiler implements it by means of a list of addresses
	// and not as a cascade of ifs (which would be rather slow)
	// we could use a vector of pointers to functions instead
			
	switch ( this->cas )
		
	{	case  0 :
			std::cout << "hand-coded integrators require pre_compute" << std::endl;
			exit ( 1 );

		case 1 :  // { int psi }

		{	constexpr size_t int_psi = 0;

			assert ( this->implemented_cases .find (1) != this->implemented_cases .end() );
			
			RESULT ( 0, psi_P, int_psi ) =
			RESULT ( 0, psi_Q, int_psi ) =
			RESULT ( 0, psi_R, int_psi ) =
			RESULT ( 0, psi_S, int_psi ) = area / 4.;                                        }
		
		break;  // end of case 1 --  Rectangle::dock_on_comput
			
		case 2 :  // { int psi1 * psi2 }

		{	constexpr size_t int_psi_psi = 0;
			
			assert ( this->implemented_cases .find (2) != this->implemented_cases .end() );

			RESULT ( psi_P, psi_P, int_psi_psi ) =
			RESULT ( psi_Q, psi_Q, int_psi_psi ) =
			RESULT ( psi_R, psi_R, int_psi_psi ) =
			RESULT ( psi_S, psi_S, int_psi_psi ) = area / 9.;
			
			RESULT ( psi_P, psi_Q, int_psi_psi ) =
			RESULT ( psi_Q, psi_P, int_psi_psi ) =
			RESULT ( psi_Q, psi_R, int_psi_psi ) =
			RESULT ( psi_R, psi_Q, int_psi_psi ) =
			RESULT ( psi_R, psi_S, int_psi_psi ) =
			RESULT ( psi_S, psi_R, int_psi_psi ) =
			RESULT ( psi_P, psi_S, int_psi_psi ) =
			RESULT ( psi_S, psi_P, int_psi_psi ) = area / 18.;
			
			RESULT ( psi_P, psi_R, int_psi_psi ) =
			RESULT ( psi_R, psi_P, int_psi_psi ) =
			RESULT ( psi_Q, psi_S, int_psi_psi ) =
			RESULT ( psi_S, psi_Q, int_psi_psi ) = area / 36.;                               }

		break;  // end of case 2 --  Rectangle::dock_on_comput

		case 3 :  // { int psi .deriv(x) }
			
		// expressions computed by hand

		{	constexpr size_t int_psi_x = 0, int_psi_y = 1;

			assert ( this->implemented_cases .find (3) != this->implemented_cases .end() );
			
			const double half_dx = delta_x / 2., half_dy = delta_y / 2.;
			
			RESULT ( 0, psi_P, int_psi_x ) =
			RESULT ( 0, psi_S, int_psi_x ) = - half_dy;
			RESULT ( 0, psi_R, int_psi_y ) =
			RESULT ( 0, psi_S, int_psi_y ) =   half_dx;
			RESULT ( 0, psi_Q, int_psi_x ) =
			RESULT ( 0, psi_R, int_psi_x ) =   half_dy;
			RESULT ( 0, psi_P, int_psi_y ) =
			RESULT ( 0, psi_Q, int_psi_y ) = - half_dx;                                      }
		
		break;  // end of case 3

		case 4 :  // { int psi1 .deriv(x) * psi2 .deriv(y) }
			
		// expressions computed by hand

		{	constexpr size_t int_psi_x_psi_x = 0, int_psi_x_psi_y = 1,
			                 int_psi_y_psi_x = 2, int_psi_y_psi_y = 3;

			assert ( this->implemented_cases .find (4) != this->implemented_cases .end() );
			
			const double dx_over_dy = delta_x / delta_y, dy_over_dx = delta_y / delta_x,
			             dy_dx_3 = dy_over_dx / 3., dx_dy_3 = dx_over_dy / 3.,
			             dy_dx_6 = dy_over_dx / 6., dx_dy_6 = dx_over_dy / 6.;

			RESULT ( psi_P, psi_P, int_psi_x_psi_x ) =
			RESULT ( psi_Q, psi_Q, int_psi_x_psi_x ) =
			RESULT ( psi_R, psi_R, int_psi_x_psi_x ) =
			RESULT ( psi_S, psi_S, int_psi_x_psi_x ) =   dy_dx_3;

			RESULT ( psi_P, psi_Q, int_psi_x_psi_x ) =
			RESULT ( psi_Q, psi_P, int_psi_x_psi_x ) =
			RESULT ( psi_R, psi_S, int_psi_x_psi_x ) =
			RESULT ( psi_S, psi_R, int_psi_x_psi_x ) = - dy_dx_3;
				
			RESULT ( psi_P, psi_R, int_psi_x_psi_x ) =
			RESULT ( psi_R, psi_P, int_psi_x_psi_x ) =
			RESULT ( psi_Q, psi_S, int_psi_x_psi_x ) =
			RESULT ( psi_S, psi_Q, int_psi_x_psi_x ) = - dy_dx_6;
			
			RESULT ( psi_R, psi_Q, int_psi_x_psi_x ) =
			RESULT ( psi_Q, psi_R, int_psi_x_psi_x ) =
			RESULT ( psi_P, psi_S, int_psi_x_psi_x ) =
			RESULT ( psi_S, psi_P, int_psi_x_psi_x ) =   dy_dx_6;
			
			RESULT ( psi_P, psi_P, int_psi_x_psi_y ) =
			RESULT ( psi_P, psi_P, int_psi_y_psi_x ) =
			RESULT ( psi_P, psi_Q, int_psi_x_psi_y ) =
			RESULT ( psi_Q, psi_P, int_psi_y_psi_x ) =
			RESULT ( psi_R, psi_R, int_psi_x_psi_y ) =
			RESULT ( psi_R, psi_R, int_psi_y_psi_x ) =
			RESULT ( psi_R, psi_S, int_psi_x_psi_y ) =
			RESULT ( psi_S, psi_R, int_psi_y_psi_x ) =
			RESULT ( psi_S, psi_P, int_psi_x_psi_y ) =
			RESULT ( psi_P, psi_S, int_psi_y_psi_x ) =
			RESULT ( psi_S, psi_Q, int_psi_x_psi_y ) =
			RESULT ( psi_Q, psi_S, int_psi_y_psi_x ) =
			RESULT ( psi_Q, psi_R, int_psi_x_psi_y ) =
			RESULT ( psi_R, psi_Q, int_psi_y_psi_x ) =
			RESULT ( psi_Q, psi_S, int_psi_x_psi_y ) =
			RESULT ( psi_S, psi_Q, int_psi_y_psi_x ) =   0.25;
			RESULT ( psi_Q, psi_Q, int_psi_x_psi_y ) =
			RESULT ( psi_Q, psi_Q, int_psi_y_psi_x ) =
			RESULT ( psi_Q, psi_P, int_psi_x_psi_y ) =
			RESULT ( psi_P, psi_Q, int_psi_y_psi_x ) =
			RESULT ( psi_P, psi_R, int_psi_x_psi_y ) =
			RESULT ( psi_R, psi_P, int_psi_y_psi_x ) =
			RESULT ( psi_P, psi_S, int_psi_x_psi_y ) =
			RESULT ( psi_S, psi_P, int_psi_y_psi_x ) =
			RESULT ( psi_R, psi_P, int_psi_x_psi_y ) =
			RESULT ( psi_P, psi_R, int_psi_y_psi_x ) =
			RESULT ( psi_R, psi_Q, int_psi_x_psi_y ) =
			RESULT ( psi_Q, psi_R, int_psi_y_psi_x ) =
			RESULT ( psi_S, psi_R, int_psi_x_psi_y ) =
			RESULT ( psi_R, psi_S, int_psi_y_psi_x ) =
			RESULT ( psi_S, psi_S, int_psi_x_psi_y ) =
			RESULT ( psi_S, psi_S, int_psi_y_psi_x ) = - 0.25;

			RESULT ( psi_P, psi_P, int_psi_y_psi_y ) =
			RESULT ( psi_Q, psi_Q, int_psi_y_psi_y ) =
			RESULT ( psi_R, psi_R, int_psi_y_psi_y ) =
			RESULT ( psi_S, psi_S, int_psi_y_psi_y ) =   dx_dy_3;

			RESULT ( psi_P, psi_Q, int_psi_y_psi_y ) =
			RESULT ( psi_Q, psi_P, int_psi_y_psi_y ) =
			RESULT ( psi_R, psi_S, int_psi_y_psi_y ) =
			RESULT ( psi_S, psi_R, int_psi_y_psi_y ) =   dx_dy_6;
				
			RESULT ( psi_P, psi_R, int_psi_y_psi_y ) =
			RESULT ( psi_R, psi_P, int_psi_y_psi_y ) =
			RESULT ( psi_Q, psi_S, int_psi_y_psi_y ) =
			RESULT ( psi_S, psi_Q, int_psi_y_psi_y ) = - dx_dy_6;
			
			RESULT ( psi_R, psi_Q, int_psi_y_psi_y ) =
			RESULT ( psi_Q, psi_R, int_psi_y_psi_y ) =
			RESULT ( psi_P, psi_S, int_psi_y_psi_y ) =
			RESULT ( psi_S, psi_P, int_psi_y_psi_y ) = - dx_dy_3;

		}  // end of case 4
			
		break;  // end of case 4 --  Rectangle::dock_on_comput

		case 5 :  // { int grad psi1 * grad psi2 }
		// { int psi1 .deriv(x) * psi2 .deriv(x) + psi1 .deriv(y) * psi2 .deriv(y) }

		// expressions computed by hand
			
		{	constexpr size_t int_grad_psi_grad_psi = 0;
				
			assert ( this->implemented_cases .find (5) != this->implemented_cases .end() );
			
			const double dx_over_dy = delta_x / delta_y, dy_over_dx = delta_y / delta_x,
			             dy_dx_3 = dy_over_dx / 3., dx_dy_3 = dx_over_dy / 3.,
			             dy_dx_6 = dy_over_dx / 6., dx_dy_6 = dx_over_dy / 6.;

			RESULT ( psi_P, psi_P, int_grad_psi_grad_psi ) =
			RESULT ( psi_Q, psi_Q, int_grad_psi_grad_psi ) =
			RESULT ( psi_R, psi_R, int_grad_psi_grad_psi ) =
			RESULT ( psi_S, psi_S, int_grad_psi_grad_psi ) =   dx_dy_3 + dy_dx_3;

			RESULT ( psi_P, psi_Q, int_grad_psi_grad_psi ) =
			RESULT ( psi_Q, psi_P, int_grad_psi_grad_psi ) =
			RESULT ( psi_R, psi_S, int_grad_psi_grad_psi ) =
			RESULT ( psi_S, psi_R, int_grad_psi_grad_psi ) =   dx_dy_6 - dy_dx_3;
				
			RESULT ( psi_P, psi_R, int_grad_psi_grad_psi ) =
			RESULT ( psi_R, psi_P, int_grad_psi_grad_psi ) =
			RESULT ( psi_Q, psi_S, int_grad_psi_grad_psi ) =
			RESULT ( psi_S, psi_Q, int_grad_psi_grad_psi ) = - dx_dy_6 - dy_dx_6;
			
			RESULT ( psi_R, psi_Q, int_grad_psi_grad_psi ) =
			RESULT ( psi_Q, psi_R, int_grad_psi_grad_psi ) =
			RESULT ( psi_P, psi_S, int_grad_psi_grad_psi ) =
			RESULT ( psi_S, psi_P, int_grad_psi_grad_psi ) =   dy_dx_6 - dx_dy_3;            }
		
		break;  // end of case 5 --  Rectangle::dock_on_comput

		case 6 :  // { int psi1 * psi2 .deriv(x) }

		// expressions computed by hand
			
		{	constexpr size_t int_psi_psi_x = 0, int_psi_psi_y = 1;
				
			assert ( this->implemented_cases .find (6) != this->implemented_cases .end() );
			
			const double dx_6 = delta_x / 6., dy_6 = delta_y / 6.;
			const double dx_12 = delta_x / 12., dy_12 = delta_y / 12.;

			RESULT ( psi_P, psi_Q, int_psi_psi_x ) =
			RESULT ( psi_Q, psi_Q, int_psi_psi_x ) =
			RESULT ( psi_R, psi_R, int_psi_psi_x ) =
			RESULT ( psi_S, psi_R, int_psi_psi_x ) =   dy_6;
			RESULT ( psi_P, psi_P, int_psi_psi_x ) =
			RESULT ( psi_Q, psi_P, int_psi_psi_x ) =
			RESULT ( psi_R, psi_S, int_psi_psi_x ) =
			RESULT ( psi_S, psi_S, int_psi_psi_x ) = - dy_6;
			
			RESULT ( psi_P, psi_R, int_psi_psi_x ) =
			RESULT ( psi_Q, psi_R, int_psi_psi_x ) =
			RESULT ( psi_R, psi_Q, int_psi_psi_x ) =
			RESULT ( psi_S, psi_Q, int_psi_psi_x ) =   dy_12;
			RESULT ( psi_P, psi_S, int_psi_psi_x ) =
			RESULT ( psi_Q, psi_S, int_psi_psi_x ) =
			RESULT ( psi_R, psi_P, int_psi_psi_x ) =
			RESULT ( psi_S, psi_P, int_psi_psi_x ) = - dy_12;

			RESULT ( psi_P, psi_S, int_psi_psi_y ) =
			RESULT ( psi_Q, psi_R, int_psi_psi_y ) =
			RESULT ( psi_R, psi_R, int_psi_psi_y ) =
			RESULT ( psi_S, psi_S, int_psi_psi_y ) =   dx_6;
			RESULT ( psi_P, psi_P, int_psi_psi_y ) =
			RESULT ( psi_Q, psi_Q, int_psi_psi_y ) =
			RESULT ( psi_R, psi_Q, int_psi_psi_y ) =
			RESULT ( psi_S, psi_P, int_psi_psi_y ) = - dx_6;

			RESULT ( psi_P, psi_R, int_psi_psi_y ) =
			RESULT ( psi_Q, psi_S, int_psi_psi_y ) =
			RESULT ( psi_R, psi_S, int_psi_psi_y ) =
			RESULT ( psi_S, psi_R, int_psi_psi_y ) =   dx_12;
			RESULT ( psi_P, psi_Q, int_psi_psi_y ) =
			RESULT ( psi_Q, psi_P, int_psi_psi_y ) =
			RESULT ( psi_R, psi_P, int_psi_psi_y ) =
			RESULT ( psi_S, psi_Q, int_psi_psi_y ) = - dx_12;
					
		}  // end of case 6
			
		break;  // end of case 6 --  Rectangle::dock_on_comput

		case 7 :  // { int psi1, int psi1 * psi2 }  // 1 and 2

		// expressions computed by hand
			
		{	constexpr size_t int_psi = 0;
			constexpr size_t int_psi_psi = 1;

			assert ( this->implemented_cases .find (7) != this->implemented_cases .end() );
			
			RESULT ( psi_P, psi_P, int_psi ) =
			RESULT ( psi_P, psi_Q, int_psi ) =
			RESULT ( psi_P, psi_R, int_psi ) =
			RESULT ( psi_P, psi_S, int_psi ) =
			RESULT ( psi_Q, psi_P, int_psi ) =
			RESULT ( psi_Q, psi_Q, int_psi ) =
			RESULT ( psi_Q, psi_R, int_psi ) =
			RESULT ( psi_Q, psi_S, int_psi ) =
			RESULT ( psi_R, psi_P, int_psi ) =
			RESULT ( psi_R, psi_Q, int_psi ) =
			RESULT ( psi_R, psi_R, int_psi ) =
			RESULT ( psi_R, psi_S, int_psi ) =
			RESULT ( psi_S, psi_P, int_psi ) =
			RESULT ( psi_S, psi_Q, int_psi ) =
			RESULT ( psi_S, psi_R, int_psi ) =
			RESULT ( psi_S, psi_S, int_psi ) = area / 4.;

			RESULT ( psi_P, psi_P, int_psi_psi ) =
			RESULT ( psi_Q, psi_Q, int_psi_psi ) =
			RESULT ( psi_R, psi_R, int_psi_psi ) =
			RESULT ( psi_S, psi_S, int_psi_psi ) = area / 9.;
			
			RESULT ( psi_P, psi_Q, int_psi_psi ) =
			RESULT ( psi_Q, psi_P, int_psi_psi ) =
			RESULT ( psi_Q, psi_R, int_psi_psi ) =
			RESULT ( psi_R, psi_Q, int_psi_psi ) =
			RESULT ( psi_R, psi_S, int_psi_psi ) =
			RESULT ( psi_S, psi_R, int_psi_psi ) =
			RESULT ( psi_P, psi_S, int_psi_psi ) =
			RESULT ( psi_S, psi_P, int_psi_psi ) = area / 18.;
			
			RESULT ( psi_P, psi_R, int_psi_psi ) =
			RESULT ( psi_R, psi_P, int_psi_psi ) =
			RESULT ( psi_Q, psi_S, int_psi_psi ) =
			RESULT ( psi_S, psi_Q, int_psi_psi ) = area / 36.;
		
		}  // end of case 7
			
		break;  // end of case 7 --  Rectangle::dock_on_comput

		case 8 :  // { int psi, int psi .deriv(x) }  // 1 and 3

		// expressions computed by hand
			
		{	constexpr size_t int_psi = 0;
			constexpr size_t int_psi_x = 1, int_psi_y = 2;

			assert ( this->implemented_cases .find (7) != this->implemented_cases .end() );
			
			RESULT ( 0, psi_P, int_psi ) =
			RESULT ( 0, psi_Q, int_psi ) =
			RESULT ( 0, psi_R, int_psi ) =
			RESULT ( 0, psi_S, int_psi ) = area / 4.;

			const double half_dx = delta_x / 2., half_dy = delta_y / 2.;
			
			RESULT ( 0, psi_P, int_psi_x ) =
			RESULT ( 0, psi_S, int_psi_x ) = - half_dy;
			RESULT ( 0, psi_R, int_psi_y ) =
			RESULT ( 0, psi_S, int_psi_y ) =   half_dx;
			RESULT ( 0, psi_Q, int_psi_x ) =
			RESULT ( 0, psi_R, int_psi_x ) =   half_dy;
			RESULT ( 0, psi_P, int_psi_y ) =
			RESULT ( 0, psi_Q, int_psi_y ) = - half_dx;
			
		}  // end of case 8
			
		break;  // end of case 8 --  Rectangle::dock_on_comput

		case 9 :  // { int psi1, int psi1 .deriv(x) * psi2 .deriv(y) }  // 1 and 4 (elasticity)

		{	constexpr size_t int_psi = 0;
			constexpr size_t int_psi_x_psi_x = 1, int_psi_x_psi_y = 2,
			                 int_psi_y_psi_x = 3, int_psi_y_psi_y = 4;
		  
			assert ( this->implemented_cases .find (9) != this->implemented_cases .end() );
			
			RESULT ( psi_P, psi_P, int_psi ) =
			RESULT ( psi_P, psi_Q, int_psi ) =
			RESULT ( psi_P, psi_R, int_psi ) =
			RESULT ( psi_P, psi_S, int_psi ) =
			RESULT ( psi_Q, psi_P, int_psi ) =
			RESULT ( psi_Q, psi_Q, int_psi ) =
			RESULT ( psi_Q, psi_R, int_psi ) =
			RESULT ( psi_Q, psi_S, int_psi ) =
			RESULT ( psi_R, psi_P, int_psi ) =
			RESULT ( psi_R, psi_Q, int_psi ) =
			RESULT ( psi_R, psi_R, int_psi ) =
			RESULT ( psi_R, psi_S, int_psi ) =
			RESULT ( psi_S, psi_P, int_psi ) =
			RESULT ( psi_S, psi_Q, int_psi ) =
			RESULT ( psi_S, psi_R, int_psi ) =
			RESULT ( psi_S, psi_S, int_psi ) = area / 4.;

			const double dx_over_dy = delta_x / delta_y, dy_over_dx = delta_y / delta_x,
			             dy_dx_3 = dy_over_dx / 3., dx_dy_3 = dx_over_dy / 3.,
			             dy_dx_6 = dy_over_dx / 6., dx_dy_6 = dx_over_dy / 6.;

			RESULT ( psi_P, psi_P, int_psi_x_psi_x ) =
			RESULT ( psi_Q, psi_Q, int_psi_x_psi_x ) =
			RESULT ( psi_R, psi_R, int_psi_x_psi_x ) =
			RESULT ( psi_S, psi_S, int_psi_x_psi_x ) =   dy_dx_3;

			RESULT ( psi_P, psi_Q, int_psi_x_psi_x ) =
			RESULT ( psi_Q, psi_P, int_psi_x_psi_x ) =
			RESULT ( psi_R, psi_S, int_psi_x_psi_x ) =
			RESULT ( psi_S, psi_R, int_psi_x_psi_x ) = - dy_dx_3;
				
			RESULT ( psi_P, psi_R, int_psi_x_psi_x ) =
			RESULT ( psi_R, psi_P, int_psi_x_psi_x ) =
			RESULT ( psi_Q, psi_S, int_psi_x_psi_x ) =
			RESULT ( psi_S, psi_Q, int_psi_x_psi_x ) = - dy_dx_6;
			
			RESULT ( psi_R, psi_Q, int_psi_x_psi_x ) =
			RESULT ( psi_Q, psi_R, int_psi_x_psi_x ) =
			RESULT ( psi_P, psi_S, int_psi_x_psi_x ) =
			RESULT ( psi_S, psi_P, int_psi_x_psi_x ) =   dy_dx_6;
			
			RESULT ( psi_P, psi_P, int_psi_x_psi_y ) =
			RESULT ( psi_P, psi_P, int_psi_y_psi_x ) =
			RESULT ( psi_P, psi_Q, int_psi_x_psi_y ) =
			RESULT ( psi_Q, psi_P, int_psi_y_psi_x ) =
			RESULT ( psi_R, psi_R, int_psi_x_psi_y ) =
			RESULT ( psi_R, psi_R, int_psi_y_psi_x ) =
			RESULT ( psi_R, psi_S, int_psi_x_psi_y ) =
			RESULT ( psi_S, psi_R, int_psi_y_psi_x ) =
			RESULT ( psi_S, psi_P, int_psi_x_psi_y ) =
			RESULT ( psi_P, psi_S, int_psi_y_psi_x ) =
			RESULT ( psi_S, psi_Q, int_psi_x_psi_y ) =
			RESULT ( psi_Q, psi_S, int_psi_y_psi_x ) =
			RESULT ( psi_Q, psi_R, int_psi_x_psi_y ) =
			RESULT ( psi_R, psi_Q, int_psi_y_psi_x ) =
			RESULT ( psi_Q, psi_S, int_psi_x_psi_y ) =
			RESULT ( psi_S, psi_Q, int_psi_y_psi_x ) =   0.25;
			RESULT ( psi_Q, psi_Q, int_psi_x_psi_y ) =
			RESULT ( psi_Q, psi_Q, int_psi_y_psi_x ) =
			RESULT ( psi_Q, psi_P, int_psi_x_psi_y ) =
			RESULT ( psi_P, psi_Q, int_psi_y_psi_x ) =
			RESULT ( psi_P, psi_R, int_psi_x_psi_y ) =
			RESULT ( psi_R, psi_P, int_psi_y_psi_x ) =
			RESULT ( psi_P, psi_S, int_psi_x_psi_y ) =
			RESULT ( psi_S, psi_P, int_psi_y_psi_x ) =
			RESULT ( psi_R, psi_P, int_psi_x_psi_y ) =
			RESULT ( psi_P, psi_R, int_psi_y_psi_x ) =
			RESULT ( psi_R, psi_Q, int_psi_x_psi_y ) =
			RESULT ( psi_Q, psi_R, int_psi_y_psi_x ) =
			RESULT ( psi_S, psi_R, int_psi_x_psi_y ) =
			RESULT ( psi_R, psi_S, int_psi_y_psi_x ) =
			RESULT ( psi_S, psi_S, int_psi_x_psi_y ) =
			RESULT ( psi_S, psi_S, int_psi_y_psi_x ) = - 0.25;

			RESULT ( psi_P, psi_P, int_psi_y_psi_y ) =
			RESULT ( psi_Q, psi_Q, int_psi_y_psi_y ) =
			RESULT ( psi_R, psi_R, int_psi_y_psi_y ) =
			RESULT ( psi_S, psi_S, int_psi_y_psi_y ) =   dx_dy_3;

			RESULT ( psi_P, psi_Q, int_psi_y_psi_y ) =
			RESULT ( psi_Q, psi_P, int_psi_y_psi_y ) =
			RESULT ( psi_R, psi_S, int_psi_y_psi_y ) =
			RESULT ( psi_S, psi_R, int_psi_y_psi_y ) =   dx_dy_6;
				
			RESULT ( psi_P, psi_R, int_psi_y_psi_y ) =
			RESULT ( psi_R, psi_P, int_psi_y_psi_y ) =
			RESULT ( psi_Q, psi_S, int_psi_y_psi_y ) =
			RESULT ( psi_S, psi_Q, int_psi_y_psi_y ) = - dx_dy_6;
			
			RESULT ( psi_R, psi_Q, int_psi_y_psi_y ) =
			RESULT ( psi_Q, psi_R, int_psi_y_psi_y ) =
			RESULT ( psi_P, psi_S, int_psi_y_psi_y ) =
			RESULT ( psi_S, psi_P, int_psi_y_psi_y ) = - dx_dy_3;

		}  // end of case 9
			
		break;  // end of case 9 --  Rectangle::dock_on_comput

		case 10 :  // { int psi1, int grad psi1 * grad psi2 }  // 1 and 5 (laplacian)

		{	constexpr size_t int_psi = 0;
			constexpr size_t int_grad_psi_grad_psi = 1;
		  
			assert ( this->implemented_cases .find (10) != this->implemented_cases .end() );
			
			RESULT ( psi_P, psi_P, int_psi ) =
			RESULT ( psi_P, psi_Q, int_psi ) =
			RESULT ( psi_P, psi_R, int_psi ) =
			RESULT ( psi_P, psi_S, int_psi ) =
			RESULT ( psi_Q, psi_P, int_psi ) =
			RESULT ( psi_Q, psi_Q, int_psi ) =
			RESULT ( psi_Q, psi_R, int_psi ) =
			RESULT ( psi_Q, psi_S, int_psi ) =
			RESULT ( psi_R, psi_P, int_psi ) =
			RESULT ( psi_R, psi_Q, int_psi ) =
			RESULT ( psi_R, psi_R, int_psi ) =
			RESULT ( psi_R, psi_S, int_psi ) =
			RESULT ( psi_S, psi_P, int_psi ) =
			RESULT ( psi_S, psi_Q, int_psi ) =
			RESULT ( psi_S, psi_R, int_psi ) =
			RESULT ( psi_S, psi_S, int_psi ) = area / 4.;

			const double dx_over_dy = delta_x / delta_y, dy_over_dx = delta_y / delta_x,
			             dy_dx_3 = dy_over_dx / 3., dx_dy_3 = dx_over_dy / 3.,
			             dy_dx_6 = dy_over_dx / 6., dx_dy_6 = dx_over_dy / 6.;

			RESULT ( psi_P, psi_P, int_grad_psi_grad_psi ) =
			RESULT ( psi_Q, psi_Q, int_grad_psi_grad_psi ) =
			RESULT ( psi_R, psi_R, int_grad_psi_grad_psi ) =
			RESULT ( psi_S, psi_S, int_grad_psi_grad_psi ) =   dx_dy_3 + dy_dx_3;

			RESULT ( psi_P, psi_Q, int_grad_psi_grad_psi ) =
			RESULT ( psi_Q, psi_P, int_grad_psi_grad_psi ) =
			RESULT ( psi_R, psi_S, int_grad_psi_grad_psi ) =
			RESULT ( psi_S, psi_R, int_grad_psi_grad_psi ) =   dx_dy_6 - dy_dx_3;
				
			RESULT ( psi_P, psi_R, int_grad_psi_grad_psi ) =
			RESULT ( psi_R, psi_P, int_grad_psi_grad_psi ) =
			RESULT ( psi_Q, psi_S, int_grad_psi_grad_psi ) =
			RESULT ( psi_S, psi_Q, int_grad_psi_grad_psi ) = - dx_dy_6 - dy_dx_6;
			
			RESULT ( psi_R, psi_Q, int_grad_psi_grad_psi ) =
			RESULT ( psi_Q, psi_R, int_grad_psi_grad_psi ) =
			RESULT ( psi_P, psi_S, int_grad_psi_grad_psi ) =
			RESULT ( psi_S, psi_P, int_grad_psi_grad_psi ) =   dy_dx_6 - dx_dy_3;
		
		}  // end of case 10
			
		break;  // end of case 10 --  Rectangle::dock_on_comput

		case 11 :  // { int psi1 .deriv(x), int psi1 .deriv(x) * psi2 .deriv(y) }  // 3 and 4

		{	constexpr size_t int_psi_x = 0, int_psi_y = 1;
			constexpr size_t int_psi_x_psi_x = 2, int_psi_x_psi_y = 3,
			                 int_psi_y_psi_x = 4, int_psi_y_psi_y = 5;
		  
			assert ( this->implemented_cases .find (11) != this->implemented_cases .end() );

			const double half_dx = delta_x / 2., half_dy = delta_y / 2.;
			
			RESULT ( psi_P, psi_P, int_psi_x ) =
			RESULT ( psi_P, psi_Q, int_psi_x ) =
			RESULT ( psi_P, psi_R, int_psi_x ) =
			RESULT ( psi_P, psi_S, int_psi_x ) =
			RESULT ( psi_S, psi_P, int_psi_x ) =
			RESULT ( psi_S, psi_Q, int_psi_x ) =
			RESULT ( psi_S, psi_R, int_psi_x ) =
			RESULT ( psi_S, psi_S, int_psi_x ) = - half_dy;
			RESULT ( psi_R, psi_P, int_psi_y ) =
			RESULT ( psi_R, psi_Q, int_psi_y ) =
			RESULT ( psi_R, psi_R, int_psi_y ) =
			RESULT ( psi_R, psi_S, int_psi_y ) =
			RESULT ( psi_S, psi_P, int_psi_y ) =
			RESULT ( psi_S, psi_Q, int_psi_y ) =
			RESULT ( psi_S, psi_R, int_psi_y ) =
			RESULT ( psi_S, psi_S, int_psi_y ) =   half_dx;
			RESULT ( psi_Q, psi_P, int_psi_x ) =
			RESULT ( psi_Q, psi_Q, int_psi_x ) =
			RESULT ( psi_Q, psi_R, int_psi_x ) =
			RESULT ( psi_Q, psi_S, int_psi_x ) =
			RESULT ( psi_R, psi_P, int_psi_x ) =
			RESULT ( psi_R, psi_Q, int_psi_x ) =
			RESULT ( psi_R, psi_R, int_psi_x ) =
			RESULT ( psi_R, psi_S, int_psi_x ) =   half_dy;
			RESULT ( psi_P, psi_P, int_psi_y ) =
			RESULT ( psi_P, psi_Q, int_psi_y ) =
			RESULT ( psi_P, psi_R, int_psi_y ) =
			RESULT ( psi_P, psi_S, int_psi_y ) =
			RESULT ( psi_Q, psi_P, int_psi_y ) =
			RESULT ( psi_Q, psi_Q, int_psi_y ) =
			RESULT ( psi_Q, psi_R, int_psi_y ) =
			RESULT ( psi_Q, psi_S, int_psi_y ) = - half_dx;
			
			const double dx_over_dy = delta_x / delta_y, dy_over_dx = delta_y / delta_x,
			             dy_dx_3 = dy_over_dx / 3., dx_dy_3 = dx_over_dy / 3.,
			             dy_dx_6 = dy_over_dx / 6., dx_dy_6 = dx_over_dy / 6.;

			RESULT ( psi_P, psi_P, int_psi_x_psi_x ) =
			RESULT ( psi_Q, psi_Q, int_psi_x_psi_x ) =
			RESULT ( psi_R, psi_R, int_psi_x_psi_x ) =
			RESULT ( psi_S, psi_S, int_psi_x_psi_x ) =   dy_dx_3;

			RESULT ( psi_P, psi_Q, int_psi_x_psi_x ) =
			RESULT ( psi_Q, psi_P, int_psi_x_psi_x ) =
			RESULT ( psi_R, psi_S, int_psi_x_psi_x ) =
			RESULT ( psi_S, psi_R, int_psi_x_psi_x ) = - dy_dx_3;
				
			RESULT ( psi_P, psi_R, int_psi_x_psi_x ) =
			RESULT ( psi_R, psi_P, int_psi_x_psi_x ) =
			RESULT ( psi_Q, psi_S, int_psi_x_psi_x ) =
			RESULT ( psi_S, psi_Q, int_psi_x_psi_x ) = - dy_dx_6;
			
			RESULT ( psi_R, psi_Q, int_psi_x_psi_x ) =
			RESULT ( psi_Q, psi_R, int_psi_x_psi_x ) =
			RESULT ( psi_P, psi_S, int_psi_x_psi_x ) =
			RESULT ( psi_S, psi_P, int_psi_x_psi_x ) =   dy_dx_6;
			
			RESULT ( psi_P, psi_P, int_psi_x_psi_y ) =
			RESULT ( psi_P, psi_P, int_psi_y_psi_x ) =
			RESULT ( psi_P, psi_Q, int_psi_x_psi_y ) =
			RESULT ( psi_Q, psi_P, int_psi_y_psi_x ) =
			RESULT ( psi_R, psi_R, int_psi_x_psi_y ) =
			RESULT ( psi_R, psi_R, int_psi_y_psi_x ) =
			RESULT ( psi_R, psi_S, int_psi_x_psi_y ) =
			RESULT ( psi_S, psi_R, int_psi_y_psi_x ) =
			RESULT ( psi_S, psi_P, int_psi_x_psi_y ) =
			RESULT ( psi_P, psi_S, int_psi_y_psi_x ) =
			RESULT ( psi_S, psi_Q, int_psi_x_psi_y ) =
			RESULT ( psi_Q, psi_S, int_psi_y_psi_x ) =
			RESULT ( psi_Q, psi_R, int_psi_x_psi_y ) =
			RESULT ( psi_R, psi_Q, int_psi_y_psi_x ) =
			RESULT ( psi_Q, psi_S, int_psi_x_psi_y ) =
			RESULT ( psi_S, psi_Q, int_psi_y_psi_x ) =   0.25;
			RESULT ( psi_Q, psi_Q, int_psi_x_psi_y ) =
			RESULT ( psi_Q, psi_Q, int_psi_y_psi_x ) =
			RESULT ( psi_Q, psi_P, int_psi_x_psi_y ) =
			RESULT ( psi_P, psi_Q, int_psi_y_psi_x ) =
			RESULT ( psi_P, psi_R, int_psi_x_psi_y ) =
			RESULT ( psi_R, psi_P, int_psi_y_psi_x ) =
			RESULT ( psi_P, psi_S, int_psi_x_psi_y ) =
			RESULT ( psi_S, psi_P, int_psi_y_psi_x ) =
			RESULT ( psi_R, psi_P, int_psi_x_psi_y ) =
			RESULT ( psi_P, psi_R, int_psi_y_psi_x ) =
			RESULT ( psi_R, psi_Q, int_psi_x_psi_y ) =
			RESULT ( psi_Q, psi_R, int_psi_y_psi_x ) =
			RESULT ( psi_S, psi_R, int_psi_x_psi_y ) =
			RESULT ( psi_R, psi_S, int_psi_y_psi_x ) =
			RESULT ( psi_S, psi_S, int_psi_x_psi_y ) =
			RESULT ( psi_S, psi_S, int_psi_y_psi_x ) = - 0.25;

			RESULT ( psi_P, psi_P, int_psi_y_psi_y ) =
			RESULT ( psi_Q, psi_Q, int_psi_y_psi_y ) =
			RESULT ( psi_R, psi_R, int_psi_y_psi_y ) =
			RESULT ( psi_S, psi_S, int_psi_y_psi_y ) =   dx_dy_3;

			RESULT ( psi_P, psi_Q, int_psi_y_psi_y ) =
			RESULT ( psi_Q, psi_P, int_psi_y_psi_y ) =
			RESULT ( psi_R, psi_S, int_psi_y_psi_y ) =
			RESULT ( psi_S, psi_R, int_psi_y_psi_y ) =   dx_dy_6;
				
			RESULT ( psi_P, psi_R, int_psi_y_psi_y ) =
			RESULT ( psi_R, psi_P, int_psi_y_psi_y ) =
			RESULT ( psi_Q, psi_S, int_psi_y_psi_y ) =
			RESULT ( psi_S, psi_Q, int_psi_y_psi_y ) = - dx_dy_6;
			
			RESULT ( psi_R, psi_Q, int_psi_y_psi_y ) =
			RESULT ( psi_Q, psi_R, int_psi_y_psi_y ) =
			RESULT ( psi_P, psi_S, int_psi_y_psi_y ) =
			RESULT ( psi_S, psi_P, int_psi_y_psi_y ) = - dx_dy_3;
		
		}  // end of case 11
			
		break;  // end of case 11 --  Rectangle::dock_on_comput

		case 12 :  // { int psi1 * psi2, int psi1 .deriv(x) * psi2 .deriv(y) }  // 2 and 4

		{	constexpr size_t int_psi_psi = 0;
			constexpr size_t int_psi_x_psi_x = 1, int_psi_x_psi_y = 2,
			                 int_psi_y_psi_x = 3, int_psi_y_psi_y = 4;
			
			assert ( this->implemented_cases .find (12) != this->implemented_cases .end() );

			RESULT ( psi_P, psi_P, int_psi_psi ) =
			RESULT ( psi_Q, psi_Q, int_psi_psi ) =
			RESULT ( psi_R, psi_R, int_psi_psi ) =
			RESULT ( psi_S, psi_S, int_psi_psi ) = area / 9.;
			
			RESULT ( psi_P, psi_Q, int_psi_psi ) =
			RESULT ( psi_Q, psi_P, int_psi_psi ) =
			RESULT ( psi_Q, psi_R, int_psi_psi ) =
			RESULT ( psi_R, psi_Q, int_psi_psi ) =
			RESULT ( psi_R, psi_S, int_psi_psi ) =
			RESULT ( psi_S, psi_R, int_psi_psi ) =
			RESULT ( psi_P, psi_S, int_psi_psi ) =
			RESULT ( psi_S, psi_P, int_psi_psi ) = area / 18.;
			
			RESULT ( psi_P, psi_R, int_psi_psi ) =
			RESULT ( psi_R, psi_P, int_psi_psi ) =
			RESULT ( psi_Q, psi_S, int_psi_psi ) =
			RESULT ( psi_S, psi_Q, int_psi_psi ) = area / 36.;
			
			const double dx_over_dy = delta_x / delta_y, dy_over_dx = delta_y / delta_x,
			             dy_dx_3 = dy_over_dx / 3., dx_dy_3 = dx_over_dy / 3.,
			             dy_dx_6 = dy_over_dx / 6., dx_dy_6 = dx_over_dy / 6.;

			RESULT ( psi_P, psi_P, int_psi_x_psi_x ) =
			RESULT ( psi_Q, psi_Q, int_psi_x_psi_x ) =
			RESULT ( psi_R, psi_R, int_psi_x_psi_x ) =
			RESULT ( psi_S, psi_S, int_psi_x_psi_x ) =   dy_dx_3;

			RESULT ( psi_P, psi_Q, int_psi_x_psi_x ) =
			RESULT ( psi_Q, psi_P, int_psi_x_psi_x ) =
			RESULT ( psi_R, psi_S, int_psi_x_psi_x ) =
			RESULT ( psi_S, psi_R, int_psi_x_psi_x ) = - dy_dx_3;
				
			RESULT ( psi_P, psi_R, int_psi_x_psi_x ) =
			RESULT ( psi_R, psi_P, int_psi_x_psi_x ) =
			RESULT ( psi_Q, psi_S, int_psi_x_psi_x ) =
			RESULT ( psi_S, psi_Q, int_psi_x_psi_x ) = - dy_dx_6;
			
			RESULT ( psi_R, psi_Q, int_psi_x_psi_x ) =
			RESULT ( psi_Q, psi_R, int_psi_x_psi_x ) =
			RESULT ( psi_P, psi_S, int_psi_x_psi_x ) =
			RESULT ( psi_S, psi_P, int_psi_x_psi_x ) =   dy_dx_6;
			
			RESULT ( psi_P, psi_P, int_psi_x_psi_y ) =
			RESULT ( psi_P, psi_P, int_psi_y_psi_x ) =
			RESULT ( psi_P, psi_Q, int_psi_x_psi_y ) =
			RESULT ( psi_Q, psi_P, int_psi_y_psi_x ) =
			RESULT ( psi_R, psi_R, int_psi_x_psi_y ) =
			RESULT ( psi_R, psi_R, int_psi_y_psi_x ) =
			RESULT ( psi_R, psi_S, int_psi_x_psi_y ) =
			RESULT ( psi_S, psi_R, int_psi_y_psi_x ) =
			RESULT ( psi_S, psi_P, int_psi_x_psi_y ) =
			RESULT ( psi_P, psi_S, int_psi_y_psi_x ) =
			RESULT ( psi_S, psi_Q, int_psi_x_psi_y ) =
			RESULT ( psi_Q, psi_S, int_psi_y_psi_x ) =
			RESULT ( psi_Q, psi_R, int_psi_x_psi_y ) =
			RESULT ( psi_R, psi_Q, int_psi_y_psi_x ) =
			RESULT ( psi_Q, psi_S, int_psi_x_psi_y ) =
			RESULT ( psi_S, psi_Q, int_psi_y_psi_x ) =   0.25;
			RESULT ( psi_Q, psi_Q, int_psi_x_psi_y ) =
			RESULT ( psi_Q, psi_Q, int_psi_y_psi_x ) =
			RESULT ( psi_Q, psi_P, int_psi_x_psi_y ) =
			RESULT ( psi_P, psi_Q, int_psi_y_psi_x ) =
			RESULT ( psi_P, psi_R, int_psi_x_psi_y ) =
			RESULT ( psi_R, psi_P, int_psi_y_psi_x ) =
			RESULT ( psi_P, psi_S, int_psi_x_psi_y ) =
			RESULT ( psi_S, psi_P, int_psi_y_psi_x ) =
			RESULT ( psi_R, psi_P, int_psi_x_psi_y ) =
			RESULT ( psi_P, psi_R, int_psi_y_psi_x ) =
			RESULT ( psi_R, psi_Q, int_psi_x_psi_y ) =
			RESULT ( psi_Q, psi_R, int_psi_y_psi_x ) =
			RESULT ( psi_S, psi_R, int_psi_x_psi_y ) =
			RESULT ( psi_R, psi_S, int_psi_y_psi_x ) =
			RESULT ( psi_S, psi_S, int_psi_x_psi_y ) =
			RESULT ( psi_S, psi_S, int_psi_y_psi_x ) = - 0.25;

			RESULT ( psi_P, psi_P, int_psi_y_psi_y ) =
			RESULT ( psi_Q, psi_Q, int_psi_y_psi_y ) =
			RESULT ( psi_R, psi_R, int_psi_y_psi_y ) =
			RESULT ( psi_S, psi_S, int_psi_y_psi_y ) =   dx_dy_3;

			RESULT ( psi_P, psi_Q, int_psi_y_psi_y ) =
			RESULT ( psi_Q, psi_P, int_psi_y_psi_y ) =
			RESULT ( psi_R, psi_S, int_psi_y_psi_y ) =
			RESULT ( psi_S, psi_R, int_psi_y_psi_y ) =   dx_dy_6;
				
			RESULT ( psi_P, psi_R, int_psi_y_psi_y ) =
			RESULT ( psi_R, psi_P, int_psi_y_psi_y ) =
			RESULT ( psi_Q, psi_S, int_psi_y_psi_y ) =
			RESULT ( psi_S, psi_Q, int_psi_y_psi_y ) = - dx_dy_6;
			
			RESULT ( psi_R, psi_Q, int_psi_y_psi_y ) =
			RESULT ( psi_Q, psi_R, int_psi_y_psi_y ) =
			RESULT ( psi_P, psi_S, int_psi_y_psi_y ) =
			RESULT ( psi_S, psi_P, int_psi_y_psi_y ) = - dx_dy_3;
		
		}  // end of case 12
			
		break;  // end of case 12 --  Rectangle::dock_on_comput

		case 13 :  // { int psi1 * psi2, int grad psi1 * grad psi2 }  // 2 and 5

		{	constexpr size_t int_psi_psi = 0;
			constexpr size_t int_grad_psi_grad_psi = 1;
			
			assert ( this->implemented_cases .find (13) != this->implemented_cases .end() );

			RESULT ( psi_P, psi_P, int_psi_psi ) =
			RESULT ( psi_Q, psi_Q, int_psi_psi ) =
			RESULT ( psi_R, psi_R, int_psi_psi ) =
			RESULT ( psi_S, psi_S, int_psi_psi ) = area / 9.;
			
			RESULT ( psi_P, psi_Q, int_psi_psi ) =
			RESULT ( psi_Q, psi_P, int_psi_psi ) =
			RESULT ( psi_Q, psi_R, int_psi_psi ) =
			RESULT ( psi_R, psi_Q, int_psi_psi ) =
			RESULT ( psi_R, psi_S, int_psi_psi ) =
			RESULT ( psi_S, psi_R, int_psi_psi ) =
			RESULT ( psi_P, psi_S, int_psi_psi ) =
			RESULT ( psi_S, psi_P, int_psi_psi ) = area / 18.;
			
			RESULT ( psi_P, psi_R, int_psi_psi ) =
			RESULT ( psi_R, psi_P, int_psi_psi ) =
			RESULT ( psi_Q, psi_S, int_psi_psi ) =
			RESULT ( psi_S, psi_Q, int_psi_psi ) = area / 36.;
			
			const double dx_over_dy = delta_x / delta_y, dy_over_dx = delta_y / delta_x,
			             dy_dx_3 = dy_over_dx / 3., dx_dy_3 = dx_over_dy / 3.,
			             dy_dx_6 = dy_over_dx / 6., dx_dy_6 = dx_over_dy / 6.;

			RESULT ( psi_P, psi_P, int_grad_psi_grad_psi ) =
			RESULT ( psi_Q, psi_Q, int_grad_psi_grad_psi ) =
			RESULT ( psi_R, psi_R, int_grad_psi_grad_psi ) =
			RESULT ( psi_S, psi_S, int_grad_psi_grad_psi ) =   dx_dy_3 + dy_dx_3;

			RESULT ( psi_P, psi_Q, int_grad_psi_grad_psi ) =
			RESULT ( psi_Q, psi_P, int_grad_psi_grad_psi ) =
			RESULT ( psi_R, psi_S, int_grad_psi_grad_psi ) =
			RESULT ( psi_S, psi_R, int_grad_psi_grad_psi ) =   dx_dy_6 - dy_dx_3;
				
			RESULT ( psi_P, psi_R, int_grad_psi_grad_psi ) =
			RESULT ( psi_R, psi_P, int_grad_psi_grad_psi ) =
			RESULT ( psi_Q, psi_S, int_grad_psi_grad_psi ) =
			RESULT ( psi_S, psi_Q, int_grad_psi_grad_psi ) = - dx_dy_6 - dy_dx_6;
			
			RESULT ( psi_R, psi_Q, int_grad_psi_grad_psi ) =
			RESULT ( psi_Q, psi_R, int_grad_psi_grad_psi ) =
			RESULT ( psi_P, psi_S, int_grad_psi_grad_psi ) =
			RESULT ( psi_S, psi_P, int_grad_psi_grad_psi ) =   dy_dx_6 - dx_dy_3;

		}  // end of case 13
			
		break;  // end of case 13 --  Rectangle::dock_on_comput

		case 14 :  // 1, 2, 3 and 4 

		{	constexpr size_t int_psi = 0;
			constexpr size_t int_psi_psi = 1;
			constexpr size_t int_psi_x = 2, int_psi_y = 3;
			constexpr size_t int_psi_x_psi_x = 4, int_psi_x_psi_y = 5,
			                 int_psi_y_psi_x = 6, int_psi_y_psi_y = 7;
			
			assert ( this->implemented_cases .find (14) != this->implemented_cases .end() );

			// case 1 (within 14)

			RESULT ( psi_P, psi_P, int_psi ) =
			RESULT ( psi_P, psi_Q, int_psi ) =
			RESULT ( psi_P, psi_R, int_psi ) =
			RESULT ( psi_P, psi_S, int_psi ) =
			RESULT ( psi_Q, psi_P, int_psi ) =
			RESULT ( psi_Q, psi_Q, int_psi ) =
			RESULT ( psi_Q, psi_R, int_psi ) =
			RESULT ( psi_Q, psi_S, int_psi ) =
			RESULT ( psi_R, psi_P, int_psi ) =
			RESULT ( psi_R, psi_Q, int_psi ) =
			RESULT ( psi_R, psi_R, int_psi ) =
			RESULT ( psi_R, psi_S, int_psi ) =
			RESULT ( psi_S, psi_P, int_psi ) =
			RESULT ( psi_S, psi_Q, int_psi ) =
			RESULT ( psi_S, psi_R, int_psi ) =
			RESULT ( psi_S, psi_S, int_psi ) = area / 4.;

			// case 2 (within 14)

			RESULT ( psi_P, psi_P, int_psi_psi ) =
			RESULT ( psi_Q, psi_Q, int_psi_psi ) =
			RESULT ( psi_R, psi_R, int_psi_psi ) =
			RESULT ( psi_S, psi_S, int_psi_psi ) = area / 9.;
			
			RESULT ( psi_P, psi_Q, int_psi_psi ) =
			RESULT ( psi_Q, psi_P, int_psi_psi ) =
			RESULT ( psi_Q, psi_R, int_psi_psi ) =
			RESULT ( psi_R, psi_Q, int_psi_psi ) =
			RESULT ( psi_R, psi_S, int_psi_psi ) =
			RESULT ( psi_S, psi_R, int_psi_psi ) =
			RESULT ( psi_P, psi_S, int_psi_psi ) =
			RESULT ( psi_S, psi_P, int_psi_psi ) = area / 18.;
			
			RESULT ( psi_P, psi_R, int_psi_psi ) =
			RESULT ( psi_R, psi_P, int_psi_psi ) =
			RESULT ( psi_Q, psi_S, int_psi_psi ) =
			RESULT ( psi_S, psi_Q, int_psi_psi ) = area / 36.;

			// case 3 (within 14)

			const double half_dx = delta_x / 2., half_dy = delta_y / 2.;
			
			RESULT ( psi_P, psi_P, int_psi_x ) =
			RESULT ( psi_P, psi_Q, int_psi_x ) =
			RESULT ( psi_P, psi_R, int_psi_x ) =
			RESULT ( psi_P, psi_S, int_psi_x ) =
			RESULT ( psi_S, psi_P, int_psi_x ) =
			RESULT ( psi_S, psi_Q, int_psi_x ) =
			RESULT ( psi_S, psi_R, int_psi_x ) =
			RESULT ( psi_S, psi_S, int_psi_x ) = - half_dy;
			RESULT ( psi_R, psi_P, int_psi_y ) =
			RESULT ( psi_R, psi_Q, int_psi_y ) =
			RESULT ( psi_R, psi_R, int_psi_y ) =
			RESULT ( psi_R, psi_S, int_psi_y ) =
			RESULT ( psi_S, psi_P, int_psi_y ) =
			RESULT ( psi_S, psi_Q, int_psi_y ) =
			RESULT ( psi_S, psi_R, int_psi_y ) =
			RESULT ( psi_S, psi_S, int_psi_y ) =   half_dx;
			RESULT ( psi_Q, psi_P, int_psi_x ) =
			RESULT ( psi_Q, psi_Q, int_psi_x ) =
			RESULT ( psi_Q, psi_R, int_psi_x ) =
			RESULT ( psi_Q, psi_S, int_psi_x ) =
			RESULT ( psi_R, psi_P, int_psi_x ) =
			RESULT ( psi_R, psi_Q, int_psi_x ) =
			RESULT ( psi_R, psi_R, int_psi_x ) =
			RESULT ( psi_R, psi_S, int_psi_x ) =   half_dy;
			RESULT ( psi_P, psi_P, int_psi_y ) =
			RESULT ( psi_P, psi_Q, int_psi_y ) =
			RESULT ( psi_P, psi_R, int_psi_y ) =
			RESULT ( psi_P, psi_S, int_psi_y ) =
			RESULT ( psi_Q, psi_P, int_psi_y ) =
			RESULT ( psi_Q, psi_Q, int_psi_y ) =
			RESULT ( psi_Q, psi_R, int_psi_y ) =
			RESULT ( psi_Q, psi_S, int_psi_y ) = - half_dx;

			// case 4 (within 14)
			
			const double dx_over_dy = delta_x / delta_y, dy_over_dx = delta_y / delta_x,
			             dy_dx_3 = dy_over_dx / 3., dx_dy_3 = dx_over_dy / 3.,
			             dy_dx_6 = dy_over_dx / 6., dx_dy_6 = dx_over_dy / 6.;

			RESULT ( psi_P, psi_P, int_psi_x_psi_x ) =
			RESULT ( psi_Q, psi_Q, int_psi_x_psi_x ) =
			RESULT ( psi_R, psi_R, int_psi_x_psi_x ) =
			RESULT ( psi_S, psi_S, int_psi_x_psi_x ) =   dy_dx_3;

			RESULT ( psi_P, psi_Q, int_psi_x_psi_x ) =
			RESULT ( psi_Q, psi_P, int_psi_x_psi_x ) =
			RESULT ( psi_R, psi_S, int_psi_x_psi_x ) =
			RESULT ( psi_S, psi_R, int_psi_x_psi_x ) = - dy_dx_3;
				
			RESULT ( psi_P, psi_R, int_psi_x_psi_x ) =
			RESULT ( psi_R, psi_P, int_psi_x_psi_x ) =
			RESULT ( psi_Q, psi_S, int_psi_x_psi_x ) =
			RESULT ( psi_S, psi_Q, int_psi_x_psi_x ) = - dy_dx_6;
			
			RESULT ( psi_R, psi_Q, int_psi_x_psi_x ) =
			RESULT ( psi_Q, psi_R, int_psi_x_psi_x ) =
			RESULT ( psi_P, psi_S, int_psi_x_psi_x ) =
			RESULT ( psi_S, psi_P, int_psi_x_psi_x ) =   dy_dx_6;
			
			RESULT ( psi_P, psi_P, int_psi_x_psi_y ) =
			RESULT ( psi_P, psi_P, int_psi_y_psi_x ) =
			RESULT ( psi_P, psi_Q, int_psi_x_psi_y ) =
			RESULT ( psi_Q, psi_P, int_psi_y_psi_x ) =
			RESULT ( psi_R, psi_R, int_psi_x_psi_y ) =
			RESULT ( psi_R, psi_R, int_psi_y_psi_x ) =
			RESULT ( psi_R, psi_S, int_psi_x_psi_y ) =
			RESULT ( psi_S, psi_R, int_psi_y_psi_x ) =
			RESULT ( psi_S, psi_P, int_psi_x_psi_y ) =
			RESULT ( psi_P, psi_S, int_psi_y_psi_x ) =
			RESULT ( psi_S, psi_Q, int_psi_x_psi_y ) =
			RESULT ( psi_Q, psi_S, int_psi_y_psi_x ) =
			RESULT ( psi_Q, psi_R, int_psi_x_psi_y ) =
			RESULT ( psi_R, psi_Q, int_psi_y_psi_x ) =
			RESULT ( psi_Q, psi_S, int_psi_x_psi_y ) =
			RESULT ( psi_S, psi_Q, int_psi_y_psi_x ) =   0.25;
			RESULT ( psi_Q, psi_Q, int_psi_x_psi_y ) =
			RESULT ( psi_Q, psi_Q, int_psi_y_psi_x ) =
			RESULT ( psi_Q, psi_P, int_psi_x_psi_y ) =
			RESULT ( psi_P, psi_Q, int_psi_y_psi_x ) =
			RESULT ( psi_P, psi_R, int_psi_x_psi_y ) =
			RESULT ( psi_R, psi_P, int_psi_y_psi_x ) =
			RESULT ( psi_P, psi_S, int_psi_x_psi_y ) =
			RESULT ( psi_S, psi_P, int_psi_y_psi_x ) =
			RESULT ( psi_R, psi_P, int_psi_x_psi_y ) =
			RESULT ( psi_P, psi_R, int_psi_y_psi_x ) =
			RESULT ( psi_R, psi_Q, int_psi_x_psi_y ) =
			RESULT ( psi_Q, psi_R, int_psi_y_psi_x ) =
			RESULT ( psi_S, psi_R, int_psi_x_psi_y ) =
			RESULT ( psi_R, psi_S, int_psi_y_psi_x ) =
			RESULT ( psi_S, psi_S, int_psi_x_psi_y ) =
			RESULT ( psi_S, psi_S, int_psi_y_psi_x ) = - 0.25;

			RESULT ( psi_P, psi_P, int_psi_y_psi_y ) =
			RESULT ( psi_Q, psi_Q, int_psi_y_psi_y ) =
			RESULT ( psi_R, psi_R, int_psi_y_psi_y ) =
			RESULT ( psi_S, psi_S, int_psi_y_psi_y ) =   dx_dy_3;

			RESULT ( psi_P, psi_Q, int_psi_y_psi_y ) =
			RESULT ( psi_Q, psi_P, int_psi_y_psi_y ) =
			RESULT ( psi_R, psi_S, int_psi_y_psi_y ) =
			RESULT ( psi_S, psi_R, int_psi_y_psi_y ) =   dx_dy_6;
				
			RESULT ( psi_P, psi_R, int_psi_y_psi_y ) =
			RESULT ( psi_R, psi_P, int_psi_y_psi_y ) =
			RESULT ( psi_Q, psi_S, int_psi_y_psi_y ) =
			RESULT ( psi_S, psi_Q, int_psi_y_psi_y ) = - dx_dy_6;
			
			RESULT ( psi_R, psi_Q, int_psi_y_psi_y ) =
			RESULT ( psi_Q, psi_R, int_psi_y_psi_y ) =
			RESULT ( psi_P, psi_S, int_psi_y_psi_y ) =
			RESULT ( psi_S, psi_P, int_psi_y_psi_y ) = - dx_dy_3;
			
		}  // end of case 14
	
		break;  // end of case 14 --  Rectangle::dock_on_comput

		default : assert ( false );
	}  // end of  switch  statement
	
}  // end of  FiniteElement::StandAlone::TypeOne::Rectangle::dock_on_comput

//------------------------------------------------------------------------------------------------------//

	
inline void FiniteElement::StandAlone::TypeOne::Square::dock_on_comput
( const double & xP, const double & yP, const double & xQ, const double & yQ,
  const double & xR, const double & yR, const double & xS, const double & yS )

// this function is speed-critical
	
{	const double ell = xQ - xP, area = ell * ell;
	// ell > 0.
	
	constexpr size_t base_d = this->base_dim;
	constexpr size_t c16 = this->case_16;
	constexpr size_t psi_P = 0, psi_Q = 1, psi_R = 2, psi_S = 3;

	#ifndef NDEBUG  // DEBUG mode
	const double tol = 1.e-6 * ell;
	assert ( std::abs ( xP - xS ) < tol );
	assert ( std::abs ( xR - xQ ) < tol );
	assert ( std::abs ( yP - yQ ) < tol );
	assert ( std::abs ( yR - yS ) < tol );
	assert ( xQ > xP );
	assert ( xR > xS );
	assert ( yS > yP );
	assert ( yR > yQ );
	assert ( std::abs ( ell - yS + yP ) < tol );  // we are on a square
	#endif  // DEBUG
	
	// below we use a switch statement
	// we trust that the compiler implements it by means of a list of addresses
	// and not as a cascade of ifs (which would be rather slow)
	// we could use a vector of pointers to functions instead
			
	switch ( this->cas )
		
	{	case  0 :
			std::cout << "hand-coded integrators require pre_compute" << std::endl;
			exit ( 1 );

		case 1 :  // { int psi }

		// expressions computed by hand

		{	constexpr size_t int_psi = 0;

			assert ( this->implemented_cases .find (1) != this->implemented_cases .end() );
			
			RESULT ( 0, psi_P, int_psi ) =
			RESULT ( 0, psi_Q, int_psi ) =
			RESULT ( 0, psi_R, int_psi ) =
			RESULT ( 0, psi_S, int_psi ) = area / 4.;                                        }
		
		break;  // end of case 1 --  Square::dock_on_comput
			
		case 2 :  // { int psi1 * psi2 }
			
		// expressions computed by hand

		{	constexpr size_t int_psi_psi = 0;

			assert ( this->implemented_cases .find (2) != this->implemented_cases .end() );
			
			RESULT ( psi_P, psi_P, int_psi_psi ) =
			RESULT ( psi_Q, psi_Q, int_psi_psi ) =
			RESULT ( psi_R, psi_R, int_psi_psi ) =
			RESULT ( psi_S, psi_S, int_psi_psi ) = area / 9.;

			RESULT ( psi_P, psi_Q, int_psi_psi ) =
			RESULT ( psi_Q, psi_P, int_psi_psi ) =
			RESULT ( psi_Q, psi_R, int_psi_psi ) =
			RESULT ( psi_R, psi_Q, int_psi_psi ) =
			RESULT ( psi_R, psi_S, int_psi_psi ) =
			RESULT ( psi_S, psi_R, int_psi_psi ) =
			RESULT ( psi_S, psi_P, int_psi_psi ) =
			RESULT ( psi_P, psi_S, int_psi_psi ) = area / 18.;

			RESULT ( psi_P, psi_R, int_psi_psi ) =
			RESULT ( psi_Q, psi_S, int_psi_psi ) =
			RESULT ( psi_R, psi_P, int_psi_psi ) =
			RESULT ( psi_S, psi_Q, int_psi_psi ) = area / 36.;                               }
		
		break;  // end of case 2 --  Square::dock_on_comput

		case 3 :  // { int psi .deriv(x) }
			
		// expressions computed by hand

		{	constexpr size_t int_psi_x = 0, int_psi_y = 1;

			assert ( this->implemented_cases .find (3) != this->implemented_cases .end() );
			
			const double half_ell = ell / 2.;
			
			RESULT ( 0, psi_R, int_psi_y ) =
			RESULT ( 0, psi_S, int_psi_y ) =
			RESULT ( 0, psi_Q, int_psi_x ) =
			RESULT ( 0, psi_R, int_psi_x ) =   half_ell;
			
			RESULT ( 0, psi_P, int_psi_x ) =
			RESULT ( 0, psi_S, int_psi_x ) =
			RESULT ( 0, psi_P, int_psi_y ) =
			RESULT ( 0, psi_Q, int_psi_y ) = - half_ell;                                     }

		break;  // end of case 3 --  Square::dock_on_comput

		case 4 :  // { int psi1 .deriv(x) * psi2 .deriv(y) }
			
		// expressions computed by hand

		{	constexpr size_t int_psi_x_psi_x = 0, int_psi_x_psi_y = 1,
			                 int_psi_y_psi_x = 2, int_psi_y_psi_y = 3;

			assert ( this->implemented_cases .find (4) != this->implemented_cases .end() );
			
			RESULT ( psi_P, psi_P, int_psi_x_psi_x ) =
			RESULT ( psi_Q, psi_Q, int_psi_x_psi_x ) =
			RESULT ( psi_R, psi_R, int_psi_x_psi_x ) =
			RESULT ( psi_S, psi_S, int_psi_x_psi_x ) =
			RESULT ( psi_P, psi_P, int_psi_y_psi_y ) =
			RESULT ( psi_Q, psi_Q, int_psi_y_psi_y ) =
			RESULT ( psi_R, psi_R, int_psi_y_psi_y ) =
			RESULT ( psi_S, psi_S, int_psi_y_psi_y ) =   tag::Util::one_third;

			RESULT ( psi_P, psi_Q, int_psi_x_psi_x ) =
			RESULT ( psi_Q, psi_P, int_psi_x_psi_x ) =
			RESULT ( psi_R, psi_S, int_psi_x_psi_x ) =
			RESULT ( psi_S, psi_R, int_psi_x_psi_x ) =
			RESULT ( psi_R, psi_Q, int_psi_y_psi_y ) =
			RESULT ( psi_Q, psi_R, int_psi_y_psi_y ) =
			RESULT ( psi_P, psi_S, int_psi_y_psi_y ) =
			RESULT ( psi_S, psi_P, int_psi_y_psi_y ) = - tag::Util::one_third;
				
			RESULT ( psi_P, psi_R, int_psi_x_psi_x ) =
			RESULT ( psi_R, psi_P, int_psi_x_psi_x ) =
			RESULT ( psi_Q, psi_S, int_psi_x_psi_x ) =
			RESULT ( psi_S, psi_Q, int_psi_x_psi_x ) =
			RESULT ( psi_P, psi_R, int_psi_y_psi_y ) =
			RESULT ( psi_R, psi_P, int_psi_y_psi_y ) =
			RESULT ( psi_Q, psi_S, int_psi_y_psi_y ) =
			RESULT ( psi_S, psi_Q, int_psi_y_psi_y ) = - tag::Util::one_sixth;
			
			RESULT ( psi_R, psi_Q, int_psi_x_psi_x ) =
			RESULT ( psi_Q, psi_R, int_psi_x_psi_x ) =
			RESULT ( psi_P, psi_S, int_psi_x_psi_x ) =
			RESULT ( psi_S, psi_P, int_psi_x_psi_x ) =
			RESULT ( psi_P, psi_Q, int_psi_y_psi_y ) =
			RESULT ( psi_Q, psi_P, int_psi_y_psi_y ) =
			RESULT ( psi_R, psi_S, int_psi_y_psi_y ) =
			RESULT ( psi_S, psi_R, int_psi_y_psi_y ) =   tag::Util::one_sixth;
			
			RESULT ( psi_P, psi_P, int_psi_x_psi_y ) =
			RESULT ( psi_P, psi_P, int_psi_y_psi_x ) =
			RESULT ( psi_P, psi_Q, int_psi_x_psi_y ) =
			RESULT ( psi_Q, psi_P, int_psi_y_psi_x ) =
			RESULT ( psi_R, psi_R, int_psi_x_psi_y ) =
			RESULT ( psi_R, psi_R, int_psi_y_psi_x ) =
			RESULT ( psi_R, psi_S, int_psi_x_psi_y ) =
			RESULT ( psi_S, psi_R, int_psi_y_psi_x ) =
			RESULT ( psi_S, psi_P, int_psi_x_psi_y ) =
			RESULT ( psi_P, psi_S, int_psi_y_psi_x ) =
			RESULT ( psi_S, psi_Q, int_psi_x_psi_y ) =
			RESULT ( psi_Q, psi_S, int_psi_y_psi_x ) =
			RESULT ( psi_Q, psi_R, int_psi_x_psi_y ) =
			RESULT ( psi_R, psi_Q, int_psi_y_psi_x ) =
			RESULT ( psi_Q, psi_S, int_psi_x_psi_y ) =
			RESULT ( psi_S, psi_Q, int_psi_y_psi_x ) =   0.25;
			RESULT ( psi_Q, psi_Q, int_psi_x_psi_y ) =
			RESULT ( psi_Q, psi_Q, int_psi_y_psi_x ) =
			RESULT ( psi_Q, psi_P, int_psi_x_psi_y ) =
			RESULT ( psi_P, psi_Q, int_psi_y_psi_x ) =
			RESULT ( psi_P, psi_R, int_psi_x_psi_y ) =
			RESULT ( psi_R, psi_P, int_psi_y_psi_x ) =
			RESULT ( psi_P, psi_S, int_psi_x_psi_y ) =
			RESULT ( psi_S, psi_P, int_psi_y_psi_x ) =
			RESULT ( psi_R, psi_P, int_psi_x_psi_y ) =
			RESULT ( psi_P, psi_R, int_psi_y_psi_x ) =
			RESULT ( psi_R, psi_Q, int_psi_x_psi_y ) =
			RESULT ( psi_Q, psi_R, int_psi_y_psi_x ) =
			RESULT ( psi_S, psi_R, int_psi_x_psi_y ) =
			RESULT ( psi_R, psi_S, int_psi_y_psi_x ) =
			RESULT ( psi_S, psi_S, int_psi_x_psi_y ) =
			RESULT ( psi_S, psi_S, int_psi_y_psi_x ) = - 0.25;

		}  // end of case 4
			
		break;  // end of case 4 --  Square::dock_on_comput

		case 5 :  // { int grad psi1 * grad psi2 }

		// expressions computed by hand
		
		{	constexpr size_t int_grad_psi_grad_psi = 0;
				
			assert ( this->implemented_cases .find (5) != this->implemented_cases .end() );
			
			RESULT ( psi_P, psi_P, int_grad_psi_grad_psi ) =
			RESULT ( psi_Q, psi_Q, int_grad_psi_grad_psi ) =
			RESULT ( psi_R, psi_R, int_grad_psi_grad_psi ) =
			RESULT ( psi_S, psi_S, int_grad_psi_grad_psi ) = tag::Util::two_thirds;

			RESULT ( psi_P, psi_R, int_grad_psi_grad_psi ) =
			RESULT ( psi_R, psi_P, int_grad_psi_grad_psi ) =
			RESULT ( psi_Q, psi_S, int_grad_psi_grad_psi ) =
			RESULT ( psi_S, psi_Q, int_grad_psi_grad_psi ) = - tag::Util::one_third;
			
			RESULT ( psi_P, psi_Q, int_grad_psi_grad_psi ) =
			RESULT ( psi_Q, psi_P, int_grad_psi_grad_psi ) =
			RESULT ( psi_R, psi_S, int_grad_psi_grad_psi ) =
			RESULT ( psi_S, psi_R, int_grad_psi_grad_psi ) =
				
			RESULT ( psi_R, psi_Q, int_grad_psi_grad_psi ) =
			RESULT ( psi_Q, psi_R, int_grad_psi_grad_psi ) =
			RESULT ( psi_P, psi_S, int_grad_psi_grad_psi ) =
			RESULT ( psi_S, psi_P, int_grad_psi_grad_psi ) = - tag::Util::one_sixth;         }

		break;  // end of case 5 --  Square::dock_on_comput

		case 6 :  // { int psi1 * psi2 .deriv(x) }

		// expressions computed by hand
		
		{	constexpr size_t int_psi_psi_x = 0, int_psi_psi_y = 1;
			
			assert ( this->implemented_cases .find (6) != this->implemented_cases .end() );
			
			// verificar !!

			RESULT ( psi_Q, psi_Q, int_psi_psi_x ) =
			RESULT ( psi_R, psi_R, int_psi_psi_x ) =
			RESULT ( psi_R, psi_R, int_psi_psi_y ) =
			RESULT ( psi_S, psi_S, int_psi_psi_y ) =
			RESULT ( psi_S, psi_R, int_psi_psi_x ) =
			RESULT ( psi_P, psi_S, int_psi_psi_y ) =
			RESULT ( psi_P, psi_Q, int_psi_psi_x ) =
			RESULT ( psi_Q, psi_R, int_psi_psi_y ) = ell / 6.;

			RESULT ( psi_P, psi_P, int_psi_psi_x ) =
			RESULT ( psi_P, psi_P, int_psi_psi_y ) =
			RESULT ( psi_Q, psi_Q, int_psi_psi_y ) =
			RESULT ( psi_S, psi_S, int_psi_psi_x ) =
			RESULT ( psi_Q, psi_P, int_psi_psi_x ) =
			RESULT ( psi_R, psi_Q, int_psi_psi_y ) =
			RESULT ( psi_R, psi_S, int_psi_psi_x ) =
			RESULT ( psi_S, psi_P, int_psi_psi_y ) = - ell / 6.;

			RESULT ( psi_P, psi_R, int_psi_psi_x ) =
			RESULT ( psi_P, psi_R, int_psi_psi_y ) =
			RESULT ( psi_S, psi_Q, int_psi_psi_x ) =
			RESULT ( psi_Q, psi_S, int_psi_psi_y ) =
			RESULT ( psi_R, psi_Q, int_psi_psi_x ) =
			RESULT ( psi_Q, psi_R, int_psi_psi_x ) =
			RESULT ( psi_S, psi_R, int_psi_psi_y ) =
			RESULT ( psi_R, psi_S, int_psi_psi_y ) = ell / 12.;

			RESULT ( psi_Q, psi_S, int_psi_psi_x ) =
			RESULT ( psi_S, psi_Q, int_psi_psi_y ) =
			RESULT ( psi_R, psi_P, int_psi_psi_x ) =
			RESULT ( psi_R, psi_P, int_psi_psi_y ) =
			RESULT ( psi_P, psi_S, int_psi_psi_x ) =
			RESULT ( psi_S, psi_P, int_psi_psi_x ) =
			RESULT ( psi_Q, psi_P, int_psi_psi_y ) =
			RESULT ( psi_P, psi_Q, int_psi_psi_y ) = - ell / 12.;

		}  // end of case 6

		break;  // end of case 6 --  Square::dock_on_comput

		default : assert ( false );
	}  // end of  switch  statement
	
}  // end of  FiniteElement::StandAlone::TypeOne::Square::dock_on_comput

//------------------------------------------------------------------------------------------------------//

	
inline void FiniteElement::StandAlone::TypeOne::Tetrahedron::dock_on_comput
( const double & xP, const double & yP, const double & zP,
  const double & xQ, const double & yQ, const double & zQ,
  const double & xR, const double & yR, const double & zR,
  const double & xS, const double & yS, const double & zS )

// this function is speed-critical

{	const double PQ_x = xQ - xP, PR_x = xR - xP, PS_x = xS - xP,
	             PQ_y = yQ - yP, PR_y = yR - yP, PS_y = yS - yP,
	             PQ_z = zQ - zP, PR_z = zR - zP, PS_z = zS - zP;

	constexpr size_t base_d = this->base_dim;
	constexpr size_t c16 = this->case_16;
	constexpr size_t psi_P = 0, psi_Q = 1, psi_R = 2, psi_S = 3;

	// below we use a switch statement
	// we trust that the compiler implements it by means of a list of addresses
	// and not as a cascade of ifs (which would be rather slow)
	// we could use a vector of pointers to functions instead
			
	switch ( this->cas )
		
	{	case  0 :
			std::cout << "hand-coded integrators require pre_compute" << std::endl;
			exit ( 1 );

		case 1 :  // { int psi }
			
		{	constexpr size_t int_psi = 0;

			assert ( this->implemented_cases .find (1) != this->implemented_cases .end() );

			const double det_PRSyz = PR_y * PS_z - PS_y * PR_z,
			             det_PQRyz = PQ_y * PR_z - PR_y * PQ_z,
			             det_PQSyz = PS_y * PQ_z - PQ_y * PS_z;

			double det = PQ_x * det_PRSyz + PR_x * det_PQSyz + PS_x * det_PQRyz;
			assert ( det > 0. );

			RESULT ( 0, psi_P, int_psi ) =
			RESULT ( 0, psi_Q, int_psi ) =
			RESULT ( 0, psi_R, int_psi ) =
			RESULT ( 0, psi_S, int_psi ) = det / 24.;                                        }
		
		break;  // end of case 1 --  Tetrahedron::dock_on_comput

		case 2 :  // { int psi1 * psi2 }
			
		// computations inspired in UFL and FFC, further optimized by hand
		// https://fenics.readthedocs.io/projects/ufl/en/latest/
		// https://fenics.readthedocs.io/projects/ffc/en/latest/
		// see https://codeberg.org/cristian.barbarosie/manifem-manual, file 'form-tetra-P1-c02.txt'

		{	constexpr size_t int_psi_psi = 0;

			assert ( this->implemented_cases .find (2) != this->implemented_cases .end() );

			const double det_PRSyz = PR_y * PS_z - PS_y * PR_z,
			             det_PQRyz = PQ_y * PR_z - PR_y * PQ_z,
			             det_PQSyz = PS_y * PQ_z - PQ_y * PS_z;

			double det = PQ_x * det_PRSyz + PR_x * det_PQSyz + PS_x * det_PQRyz;
			assert ( det > 0. );

			RESULT ( psi_P, psi_P, int_psi_psi ) =
			RESULT ( psi_Q, psi_Q, int_psi_psi ) =
			RESULT ( psi_R, psi_R, int_psi_psi ) =
			RESULT ( psi_S, psi_S, int_psi_psi ) = det / 60.;
			
			RESULT ( psi_P, psi_Q, int_psi_psi ) =
			RESULT ( psi_P, psi_R, int_psi_psi ) =
			RESULT ( psi_P, psi_S, int_psi_psi ) =
			RESULT ( psi_Q, psi_P, int_psi_psi ) =
			RESULT ( psi_Q, psi_R, int_psi_psi ) =
			RESULT ( psi_Q, psi_S, int_psi_psi ) =
			RESULT ( psi_R, psi_P, int_psi_psi ) =
			RESULT ( psi_R, psi_Q, int_psi_psi ) =
			RESULT ( psi_R, psi_S, int_psi_psi ) =
			RESULT ( psi_S, psi_P, int_psi_psi ) =
			RESULT ( psi_S, psi_Q, int_psi_psi ) =
			RESULT ( psi_S, psi_R, int_psi_psi ) = det / 120.;                               }
			
		break;  // end of case 2 --  Tetrahedron::dock_on_comput

		case 3 :  // { int psi .deriv(x) }
		
		// computations inspired in UFL and FFC, further optimized by hand
		// https://fenics.readthedocs.io/projects/ufl/en/latest/
		// https://fenics.readthedocs.io/projects/ffc/en/latest/
		// see https://codeberg.org/cristian.barbarosie/manifem-manual, file 'form-tetra-P1-c03.txt'

		{	constexpr size_t int_psi_x = 0, int_psi_y = 1, int_psi_z = 2;

			assert ( this->implemented_cases .find (3) != this->implemented_cases .end() );

			const double det_PRSyz = ( PR_y * PS_z - PS_y * PR_z ) / 6.,
			             det_PQRyz = ( PQ_y * PR_z - PR_y * PQ_z ) / 6.,
			             det_PRSxz = ( PS_x * PR_z - PS_z * PR_x ) / 6.,
			             det_PQSxz = ( PQ_x * PS_z - PQ_z * PS_x ) / 6.,
			             det_PQRxz = ( PR_x * PQ_z - PQ_x * PR_z ) / 6.,
			             det_PQSyz = ( PS_y * PQ_z - PQ_y * PS_z ) / 6.,
			             det_PRSxy = ( PR_x * PS_y - PS_x * PR_y ) / 6.,
			             det_PQSxy = ( PS_x * PQ_y - PQ_x * PS_y ) / 6.,
			             det_PQRxy = ( PQ_x * PR_y - PR_x * PQ_y ) / 6.;

			RESULT ( 0, psi_P, int_psi_x ) = - det_PRSyz - det_PQSyz - det_PQRyz;
			RESULT ( 0, psi_Q, int_psi_x ) =   det_PRSyz;
			RESULT ( 0, psi_R, int_psi_x ) =   det_PQSyz;
			RESULT ( 0, psi_S, int_psi_x ) =   det_PQRyz;

			RESULT ( 0, psi_P, int_psi_y ) = - det_PRSxz - det_PQSxz - det_PQRxz;
			RESULT ( 0, psi_Q, int_psi_y ) =   det_PRSxz;
			RESULT ( 0, psi_R, int_psi_y ) =   det_PQSxz;
			RESULT ( 0, psi_S, int_psi_y ) =   det_PQRxz;

			RESULT ( 0, psi_P, int_psi_z ) = - det_PRSxy - det_PQSxy - det_PQRxy;
			RESULT ( 0, psi_Q, int_psi_z ) =   det_PRSxy;
			RESULT ( 0, psi_R, int_psi_z ) =   det_PQSxy;
			RESULT ( 0, psi_S, int_psi_z ) =   det_PQRxy;
			
		}  // end of case 3
		
		break;  // end of case 3 --  Tetrahedron::dock_on_comput
	
		case 4 :  // { int psi1 .deriv(x) * psi2 .deriv(y) }
		
		// computations inspired in UFL and FFC, further optimized by hand
		// https://fenics.readthedocs.io/projects/ufl/en/latest/
		// https://fenics.readthedocs.io/projects/ffc/en/latest/
		// see https://codeberg.org/cristian.barbarosie/manifem-manual, file 'form-tetra-P1-c04.txt'

		{	constexpr size_t int_psi_x_psi_x = 0, int_psi_x_psi_y = 1, int_psi_x_psi_z = 2,
			                 int_psi_y_psi_x = 3, int_psi_y_psi_y = 4, int_psi_y_psi_z = 5,
			                 int_psi_z_psi_x = 6, int_psi_z_psi_y = 7, int_psi_z_psi_z = 8;

			assert ( this->implemented_cases .find (4) != this->implemented_cases .end() );
			
			const double det_PRSyz = PR_y * PS_z - PS_y * PR_z,
			             det_PQRyz = PQ_y * PR_z - PR_y * PQ_z,
			             det_PRSxz = PS_x * PR_z - PS_z * PR_x,
			             det_PQSxz = PQ_x * PS_z - PQ_z * PS_x,
			             det_PQRxz = PR_x * PQ_z - PQ_x * PR_z,
			             det_PQSyz = PS_y * PQ_z - PQ_y * PS_z,
			             det_PRSxy = PR_x * PS_y - PS_x * PR_y,
			             det_PQSxy = PS_x * PQ_y - PQ_x * PS_y,
			             det_PQRxy = PQ_x * PR_y - PR_x * PQ_y;

			double det = PQ_x * det_PRSyz + PR_x * det_PQSyz + PS_x * det_PQRyz;
			assert ( det > 0. );
			det *= 6.;

			{ // just a block of code for hiding 'spi'
			const double sp1 = det_PRSyz * det_PRSyz / det,
			             sp2 = det_PQSyz * det_PRSyz / det,
			             sp3 = det_PQRyz * det_PRSyz / det,
			             sp5 = det_PQSyz * det_PQSyz / det,
			             sp6 = det_PQRyz * det_PQSyz / det,
			             sp9 = det_PQRyz * det_PQRyz / det;
			// equivalently, we may divide the second-order determinants by square root of det
			// but is it faster ?  is computing one square root faster than performing 36 divisions ?

			RESULT ( psi_P, psi_P, int_psi_x_psi_x ) =
					sp1 + sp2 + sp3 + sp2 + sp5 + sp6 + sp3 + sp6 + sp9;
			RESULT ( psi_P, psi_Q, int_psi_x_psi_x ) = 
			RESULT ( psi_Q, psi_P, int_psi_x_psi_x ) = - sp1 - sp2 - sp3;
			RESULT ( psi_P, psi_R, int_psi_x_psi_x ) = 
			RESULT ( psi_R, psi_P, int_psi_x_psi_x ) = - sp2 - sp5 - sp6;
			RESULT ( psi_P, psi_S, int_psi_x_psi_x ) = 
			RESULT ( psi_S, psi_P, int_psi_x_psi_x ) = - sp3 - sp6 - sp9;
			RESULT ( psi_Q, psi_Q, int_psi_x_psi_x ) =   sp1;
			RESULT ( psi_Q, psi_R, int_psi_x_psi_x ) = 
			RESULT ( psi_R, psi_Q, int_psi_x_psi_x ) =   sp2;
			RESULT ( psi_Q, psi_S, int_psi_x_psi_x ) = 
			RESULT ( psi_S, psi_Q, int_psi_x_psi_x ) =   sp3;
			RESULT ( psi_R, psi_R, int_psi_x_psi_x ) =   sp5;
			RESULT ( psi_R, psi_S, int_psi_x_psi_x ) = 
			RESULT ( psi_S, psi_R, int_psi_x_psi_x ) =   sp6;
			RESULT ( psi_S, psi_S, int_psi_x_psi_x ) =   sp9;
			} // just a block of code

			{ // just a block of code for hiding 'spi'
			const double sp1 = det_PRSxz * det_PRSxz / det,
			             sp2 = det_PQSxz * det_PRSxz / det,
			             sp3 = det_PQRxz * det_PRSxz / det,
			             sp5 = det_PQSxz * det_PQSxz / det,
			             sp6 = det_PQRxz * det_PQSxz / det,
			             sp9 = det_PQRxz * det_PQRxz / det;
			// equivalently, we may divide the second-order determinants by square root of det
			// but is it faster ?  is computing one square root faster than performing 36 divisions ?

			RESULT ( psi_P, psi_P, int_psi_y_psi_y ) =
					sp1 + sp2 + sp3 + sp2 + sp5 + sp6 + sp3 + sp6 + sp9;
			RESULT ( psi_P, psi_Q, int_psi_y_psi_y ) = 
			RESULT ( psi_Q, psi_P, int_psi_y_psi_y ) = - sp1 - sp2 - sp3;
			RESULT ( psi_P, psi_R, int_psi_y_psi_y ) = 
			RESULT ( psi_R, psi_P, int_psi_y_psi_y ) = - sp2 - sp5 - sp6;
			RESULT ( psi_P, psi_S, int_psi_y_psi_y ) = 
			RESULT ( psi_S, psi_P, int_psi_y_psi_y ) = - sp3 - sp6 - sp9;
			RESULT ( psi_Q, psi_Q, int_psi_y_psi_y ) =   sp1;
			RESULT ( psi_Q, psi_R, int_psi_y_psi_y ) = 
			RESULT ( psi_R, psi_Q, int_psi_y_psi_y ) =   sp2;
			RESULT ( psi_Q, psi_S, int_psi_y_psi_y ) = 
			RESULT ( psi_S, psi_Q, int_psi_y_psi_y ) =   sp3;
			RESULT ( psi_R, psi_R, int_psi_y_psi_y ) =   sp5;
			RESULT ( psi_R, psi_S, int_psi_y_psi_y ) = 
			RESULT ( psi_S, psi_R, int_psi_y_psi_y ) =   sp6;
			RESULT ( psi_S, psi_S, int_psi_y_psi_y ) =   sp9;
			} // just a block of code

			{ // just a block of code for hiding 'spi'
			const double sp1 = det_PRSxy * det_PRSxy / det,
			             sp2 = det_PQSxy * det_PRSxy / det,
			             sp3 = det_PQRxy * det_PRSxy / det,
			             sp5 = det_PQSxy * det_PQSxy / det,
			             sp6 = det_PQRxy * det_PQSxy / det,
			             sp9 = det_PQRxy * det_PQRxy / det;
			// equivalently, we may divide the second-order determinants by square root of det
			// but is it faster ?  is computing one square root faster than performing 36 divisions ?

			RESULT ( psi_P, psi_P, int_psi_z_psi_z ) =
					sp1 + sp2 + sp3 + sp2 + sp5 + sp6 + sp3 + sp6 + sp9;
			RESULT ( psi_P, psi_Q, int_psi_z_psi_z ) = 
			RESULT ( psi_Q, psi_P, int_psi_z_psi_z ) = - sp1 - sp2 - sp3;
			RESULT ( psi_P, psi_R, int_psi_z_psi_z ) = 
			RESULT ( psi_R, psi_P, int_psi_z_psi_z ) = - sp2 - sp5 - sp6;
			RESULT ( psi_P, psi_S, int_psi_z_psi_z ) = 
			RESULT ( psi_S, psi_P, int_psi_z_psi_z ) = - sp3 - sp6 - sp9;
			RESULT ( psi_Q, psi_Q, int_psi_z_psi_z ) =   sp1;
			RESULT ( psi_Q, psi_R, int_psi_z_psi_z ) = 
			RESULT ( psi_R, psi_Q, int_psi_z_psi_z ) =   sp2;
			RESULT ( psi_Q, psi_S, int_psi_z_psi_z ) = 
			RESULT ( psi_S, psi_Q, int_psi_z_psi_z ) =   sp3;
			RESULT ( psi_R, psi_R, int_psi_z_psi_z ) =   sp5;
			RESULT ( psi_R, psi_S, int_psi_z_psi_z ) = 
			RESULT ( psi_S, psi_R, int_psi_z_psi_z ) =   sp6;
			RESULT ( psi_S, psi_S, int_psi_z_psi_z ) =   sp9;
			} // just a block of code

			{ // just a block of code for hiding 'spi'
			const double sp1 = det_PRSyz * det_PRSxz / det,
			             sp2 = det_PQSyz * det_PRSxz / det,
			             sp3 = det_PQRyz * det_PRSxz / det,
			             sp4 = det_PRSyz * det_PQSxz / det,
			             sp5 = det_PQSyz * det_PQSxz / det,
			             sp6 = det_PQRyz * det_PQSxz / det,
			             sp7 = det_PRSyz * det_PQRxz / det,
			             sp8 = det_PQSyz * det_PQRxz / det,
			             sp9 = det_PQRyz * det_PQRxz / det;
			// equivalently, we may divide the second-order determinants by square root of det
			// but is it faster ?  is computing one square root faster than performing 36 divisions ?

			RESULT ( psi_P, psi_P, int_psi_x_psi_y ) =
			RESULT ( psi_P, psi_P, int_psi_y_psi_x ) =
					sp1 + sp4 + sp7 + sp2 + sp5 + sp8 + sp3 + sp6 + sp9;
			RESULT ( psi_P, psi_Q, int_psi_x_psi_y ) =
			RESULT ( psi_Q, psi_P, int_psi_y_psi_x ) = - sp1 - sp2 - sp3;
			RESULT ( psi_P, psi_R, int_psi_x_psi_y ) =
			RESULT ( psi_R, psi_P, int_psi_y_psi_x ) = - sp4 - sp5 - sp6;
			RESULT ( psi_P, psi_S, int_psi_x_psi_y ) =
			RESULT ( psi_S, psi_P, int_psi_y_psi_x ) = - sp7 - sp8 - sp9; 
			RESULT ( psi_Q, psi_P, int_psi_x_psi_y ) =
			RESULT ( psi_P, psi_Q, int_psi_y_psi_x ) = - sp1 - sp4 - sp7;
			RESULT ( psi_Q, psi_Q, int_psi_x_psi_y ) =
			RESULT ( psi_Q, psi_Q, int_psi_y_psi_x ) =   sp1;
			RESULT ( psi_Q, psi_R, int_psi_x_psi_y ) =
			RESULT ( psi_R, psi_Q, int_psi_y_psi_x ) =   sp4;
			RESULT ( psi_Q, psi_S, int_psi_x_psi_y ) =
			RESULT ( psi_S, psi_Q, int_psi_y_psi_x ) =   sp7;
			RESULT ( psi_R, psi_P, int_psi_x_psi_y ) =
			RESULT ( psi_P, psi_R, int_psi_y_psi_x ) = - sp2 - sp5 - sp8;
			RESULT ( psi_R, psi_Q, int_psi_x_psi_y ) =
			RESULT ( psi_Q, psi_R, int_psi_y_psi_x ) =   sp2;
			RESULT ( psi_R, psi_R, int_psi_x_psi_y ) =
			RESULT ( psi_R, psi_R, int_psi_y_psi_x ) =   sp5;
			RESULT ( psi_R, psi_S, int_psi_x_psi_y ) =
			RESULT ( psi_S, psi_R, int_psi_y_psi_x ) =   sp8;
			RESULT ( psi_S, psi_P, int_psi_x_psi_y ) =
			RESULT ( psi_P, psi_S, int_psi_y_psi_x ) = - sp3 - sp6 - sp9;
			RESULT ( psi_S, psi_Q, int_psi_x_psi_y ) =
			RESULT ( psi_Q, psi_S, int_psi_y_psi_x ) =   sp3;
			RESULT ( psi_S, psi_R, int_psi_x_psi_y ) =
			RESULT ( psi_R, psi_S, int_psi_y_psi_x ) =   sp6;
			RESULT ( psi_S, psi_S, int_psi_x_psi_y ) =
			RESULT ( psi_S, psi_S, int_psi_y_psi_x ) =   sp9;
			} // just a block of code

			{ // just a block of code for hiding 'spi'
			const double sp1 = det_PRSyz * det_PRSxy / det,
			             sp2 = det_PQSyz * det_PRSxy / det,
			             sp3 = det_PQRyz * det_PRSxy / det,
			             sp4 = det_PRSyz * det_PQSxy / det,
			             sp5 = det_PQSyz * det_PQSxy / det,
			             sp6 = det_PQRyz * det_PQSxy / det,
			             sp7 = det_PRSyz * det_PQRxy / det,
			             sp8 = det_PQSyz * det_PQRxy / det,
			             sp9 = det_PQRyz * det_PQRxy / det;
			// equivalently, we may divide the second-order determinants by square root of det
			// but is it faster ?  is computing one square root faster than performing 36 divisions ?

			RESULT ( psi_P, psi_P, int_psi_x_psi_z ) =
			RESULT ( psi_P, psi_P, int_psi_z_psi_x ) =
					sp1 + sp4 + sp7 + sp2 + sp5 + sp8 + sp3 + sp6 + sp9;
			RESULT ( psi_P, psi_Q, int_psi_x_psi_z ) =
			RESULT ( psi_Q, psi_P, int_psi_z_psi_x ) = - sp1 - sp2 - sp3;
			RESULT ( psi_P, psi_R, int_psi_x_psi_z ) =
			RESULT ( psi_R, psi_P, int_psi_z_psi_x ) = - sp4 - sp5 - sp6;
			RESULT ( psi_P, psi_S, int_psi_x_psi_z ) =
			RESULT ( psi_S, psi_P, int_psi_z_psi_x ) = - sp7 - sp8 - sp9; 
			RESULT ( psi_Q, psi_P, int_psi_x_psi_z ) =
			RESULT ( psi_P, psi_Q, int_psi_z_psi_x ) = - sp1 - sp4 - sp7;
			RESULT ( psi_Q, psi_Q, int_psi_x_psi_z ) =
			RESULT ( psi_Q, psi_Q, int_psi_z_psi_x ) =   sp1;
			RESULT ( psi_Q, psi_R, int_psi_x_psi_z ) =
			RESULT ( psi_R, psi_Q, int_psi_z_psi_x ) =   sp4;
			RESULT ( psi_Q, psi_S, int_psi_x_psi_z ) =
			RESULT ( psi_S, psi_Q, int_psi_z_psi_x ) =   sp7;
			RESULT ( psi_R, psi_P, int_psi_x_psi_z ) =
			RESULT ( psi_P, psi_R, int_psi_z_psi_x ) = - sp2 - sp5 - sp8;
			RESULT ( psi_R, psi_Q, int_psi_x_psi_z ) =
			RESULT ( psi_Q, psi_R, int_psi_z_psi_x ) =   sp2;
			RESULT ( psi_R, psi_R, int_psi_x_psi_z ) =
			RESULT ( psi_R, psi_R, int_psi_z_psi_x ) =   sp5;
			RESULT ( psi_R, psi_S, int_psi_x_psi_z ) =
			RESULT ( psi_S, psi_R, int_psi_z_psi_x ) =   sp8;
			RESULT ( psi_S, psi_P, int_psi_x_psi_z ) =
			RESULT ( psi_P, psi_S, int_psi_z_psi_x ) = - sp3 - sp6 - sp9;
			RESULT ( psi_S, psi_Q, int_psi_x_psi_z ) =
			RESULT ( psi_Q, psi_S, int_psi_z_psi_x ) =   sp3;
			RESULT ( psi_S, psi_R, int_psi_x_psi_z ) =
			RESULT ( psi_R, psi_S, int_psi_z_psi_x ) =   sp6;
			RESULT ( psi_S, psi_S, int_psi_x_psi_z ) =
			RESULT ( psi_S, psi_S, int_psi_z_psi_x ) =   sp9;
			} // just a block of code

			{ // just a block of code for hiding 'spi'
			const double sp1 = det_PRSxz * det_PRSxy / det,
			             sp2 = det_PQSxz * det_PRSxy / det,
			             sp3 = det_PQRxz * det_PRSxy / det,
			             sp4 = det_PRSxz * det_PQSxy / det,
			             sp5 = det_PQSxz * det_PQSxy / det,
			             sp6 = det_PQRxz * det_PQSxy / det,
			             sp7 = det_PRSxz * det_PQRxy / det,
			             sp8 = det_PQSxz * det_PQRxy / det,
			             sp9 = det_PQRxz * det_PQRxy / det;
			// equivalently, we may divide the second-order determinants by square root of det
			// but is it faster ?  is computing one square root faster than performing 36 divisions ?

			RESULT ( psi_P, psi_P, int_psi_y_psi_z ) =
			RESULT ( psi_P, psi_P, int_psi_z_psi_y ) =
					sp1 + sp4 + sp7 + sp2 + sp5 + sp8 + sp3 + sp6 + sp9;
			RESULT ( psi_P, psi_Q, int_psi_y_psi_z ) =
			RESULT ( psi_Q, psi_P, int_psi_z_psi_y ) = - sp1 - sp2 - sp3;
			RESULT ( psi_P, psi_R, int_psi_y_psi_z ) =
			RESULT ( psi_R, psi_P, int_psi_z_psi_y ) = - sp4 - sp5 - sp6;
			RESULT ( psi_P, psi_S, int_psi_y_psi_z ) =
			RESULT ( psi_S, psi_P, int_psi_z_psi_y ) = - sp7 - sp8 - sp9; 
			RESULT ( psi_Q, psi_P, int_psi_y_psi_z ) =
			RESULT ( psi_P, psi_Q, int_psi_z_psi_y ) = - sp1 - sp4 - sp7;
			RESULT ( psi_Q, psi_Q, int_psi_y_psi_z ) =
			RESULT ( psi_Q, psi_Q, int_psi_z_psi_y ) =   sp1;
			RESULT ( psi_Q, psi_R, int_psi_y_psi_z ) =
			RESULT ( psi_R, psi_Q, int_psi_z_psi_y ) =   sp4;
			RESULT ( psi_Q, psi_S, int_psi_y_psi_z ) =
			RESULT ( psi_S, psi_Q, int_psi_z_psi_y ) =   sp7;
			RESULT ( psi_R, psi_P, int_psi_y_psi_z ) =
			RESULT ( psi_P, psi_R, int_psi_z_psi_y ) = - sp2 - sp5 - sp8;
			RESULT ( psi_R, psi_Q, int_psi_y_psi_z ) =
			RESULT ( psi_Q, psi_R, int_psi_z_psi_y ) =   sp2;
			RESULT ( psi_R, psi_R, int_psi_y_psi_z ) =
			RESULT ( psi_R, psi_R, int_psi_z_psi_y ) =   sp5;
			RESULT ( psi_R, psi_S, int_psi_y_psi_z ) =
			RESULT ( psi_S, psi_R, int_psi_z_psi_y ) =   sp8;
			RESULT ( psi_S, psi_P, int_psi_y_psi_z ) =
			RESULT ( psi_P, psi_S, int_psi_z_psi_y ) = - sp3 - sp6 - sp9;
			RESULT ( psi_S, psi_Q, int_psi_y_psi_z ) =
			RESULT ( psi_Q, psi_S, int_psi_z_psi_y ) =   sp3;
			RESULT ( psi_S, psi_R, int_psi_y_psi_z ) =
			RESULT ( psi_R, psi_S, int_psi_z_psi_y ) =   sp6;
			RESULT ( psi_S, psi_S, int_psi_y_psi_z ) =
			RESULT ( psi_S, psi_S, int_psi_z_psi_y ) =   sp9;
			}  // just a block of code

		}	// end of case 4
				
		break;  // end of case 4 --  Tetrahedron::dock_on_comput

		case 5 :  // { int grad psi1 * grad psi2 }
		
		// computations inspired in UFL and FFC, further optimized by hand
		// https://fenics.readthedocs.io/projects/ufl/en/latest/
		// https://fenics.readthedocs.io/projects/ffc/en/latest/
		// see https://codeberg.org/cristian.barbarosie/manifem-manual, file 'form-tetra-P1-c05.txt'

		{	constexpr size_t int_grad_psi_grad_psi = 0;

			assert ( this->implemented_cases .find (5) != this->implemented_cases .end() );
			
			const double det_PRSyz = PR_y * PS_z - PS_y * PR_z,
			             det_PQRyz = PQ_y * PR_z - PR_y * PQ_z,
			             det_PRSxz = PS_x * PR_z - PS_z * PR_x,
			             det_PQSxz = PQ_x * PS_z - PQ_z * PS_x,
			             det_PQRxz = PR_x * PQ_z - PQ_x * PR_z,
			             det_PQSyz = PS_y * PQ_z - PQ_y * PS_z,
			             det_PRSxy = PR_x * PS_y - PS_x * PR_y,
			             det_PQSxy = PS_x * PQ_y - PQ_x * PS_y,
			             det_PQRxy = PQ_x * PR_y - PR_x * PQ_y;

			double det = PQ_x * det_PRSyz + PR_x * det_PQSyz + PS_x * det_PQRyz;
			assert ( det > 0. );
			det *= 6.;

			const double
				sp1 = ( det_PRSyz * det_PRSyz + det_PRSxz * det_PRSxz + det_PRSxy * det_PRSxy ) / det,
				sp2 = ( det_PQSyz * det_PRSyz + det_PQSxz * det_PRSxz + det_PQSxy * det_PRSxy ) / det,
				sp3 = ( det_PQRyz * det_PRSyz + det_PQRxz * det_PRSxz + det_PQRxy * det_PRSxy ) / det,
				sp5 = ( det_PQSyz * det_PQSyz + det_PQSxz * det_PQSxz + det_PQSxy * det_PQSxy ) / det,
				sp6 = ( det_PQRyz * det_PQSyz + det_PQRxz * det_PQSxz + det_PQRxy * det_PQSxy ) / det,
				sp9 = ( det_PQRyz * det_PQRyz + det_PQRxz * det_PQRxz + det_PQRxy * det_PQRxy ) / det;
			
			RESULT ( psi_P, psi_P, int_grad_psi_grad_psi ) =
					sp1 + sp2 + sp3 + sp2 + sp5 + sp6 + sp3 + sp6 + sp9;
			RESULT ( psi_P, psi_Q, int_grad_psi_grad_psi ) = 
			RESULT ( psi_Q, psi_P, int_grad_psi_grad_psi ) = - sp1 - sp2 - sp3;
			RESULT ( psi_P, psi_R, int_grad_psi_grad_psi ) = 
			RESULT ( psi_R, psi_P, int_grad_psi_grad_psi ) = - sp2 - sp5 - sp6;
			RESULT ( psi_P, psi_S, int_grad_psi_grad_psi ) = 
			RESULT ( psi_S, psi_P, int_grad_psi_grad_psi ) = - sp3 - sp6 - sp9;
			RESULT ( psi_Q, psi_Q, int_grad_psi_grad_psi ) =   sp1;
			RESULT ( psi_Q, psi_R, int_grad_psi_grad_psi ) = 
			RESULT ( psi_R, psi_Q, int_grad_psi_grad_psi ) =   sp2;
			RESULT ( psi_Q, psi_S, int_grad_psi_grad_psi ) = 
			RESULT ( psi_S, psi_Q, int_grad_psi_grad_psi ) =   sp3;
			RESULT ( psi_R, psi_R, int_grad_psi_grad_psi ) =   sp5;
			RESULT ( psi_R, psi_S, int_grad_psi_grad_psi ) = 
			RESULT ( psi_S, psi_R, int_grad_psi_grad_psi ) =   sp6;
			RESULT ( psi_S, psi_S, int_grad_psi_grad_psi ) =   sp9;
			
		} // end of case 5
				
		break;  // end of case 5 --  Tetrahedron::dock_on_comput

		case 7 :  // { int psi, int psi1 * psi2 }  // 1 and 2

		{	constexpr size_t int_psi = 0, int_psi_psi = 1;
		  
			assert ( this->implemented_cases .find (7) != this->implemented_cases .end() );
			
			const double det_PRSyz = PR_y * PS_z - PS_y * PR_z,
			             det_PQRyz = PQ_y * PR_z - PR_y * PQ_z,
			             det_PQSyz = PS_y * PQ_z - PQ_y * PS_z;

			double det = PQ_x * det_PRSyz + PR_x * det_PQSyz + PS_x * det_PQRyz;
			assert ( det > 0. );

			RESULT ( psi_P, psi_P, int_psi ) =
			RESULT ( psi_P, psi_Q, int_psi ) =
			RESULT ( psi_P, psi_R, int_psi ) =
			RESULT ( psi_P, psi_S, int_psi ) =
			RESULT ( psi_Q, psi_P, int_psi ) =
			RESULT ( psi_Q, psi_Q, int_psi ) =
			RESULT ( psi_Q, psi_R, int_psi ) =
			RESULT ( psi_Q, psi_S, int_psi ) =
			RESULT ( psi_R, psi_P, int_psi ) =
			RESULT ( psi_R, psi_Q, int_psi ) =
			RESULT ( psi_R, psi_R, int_psi ) =
			RESULT ( psi_R, psi_S, int_psi ) =
			RESULT ( psi_S, psi_P, int_psi ) =
			RESULT ( psi_S, psi_Q, int_psi ) =
			RESULT ( psi_S, psi_R, int_psi ) =
			RESULT ( psi_S, psi_S, int_psi ) = det / 24.;
			
			RESULT ( psi_P, psi_P, int_psi_psi ) =
			RESULT ( psi_Q, psi_Q, int_psi_psi ) =
			RESULT ( psi_R, psi_R, int_psi_psi ) =
			RESULT ( psi_S, psi_S, int_psi_psi ) = det / 60.;
			
			RESULT ( psi_P, psi_Q, int_psi_psi ) =
			RESULT ( psi_P, psi_R, int_psi_psi ) =
			RESULT ( psi_P, psi_S, int_psi_psi ) =
			RESULT ( psi_Q, psi_P, int_psi_psi ) =
			RESULT ( psi_Q, psi_R, int_psi_psi ) =
			RESULT ( psi_Q, psi_S, int_psi_psi ) =
			RESULT ( psi_R, psi_P, int_psi_psi ) =
			RESULT ( psi_R, psi_Q, int_psi_psi ) =
			RESULT ( psi_R, psi_S, int_psi_psi ) =
			RESULT ( psi_S, psi_P, int_psi_psi ) =
			RESULT ( psi_S, psi_Q, int_psi_psi ) =
			RESULT ( psi_S, psi_R, int_psi_psi ) = det / 120.;

		}  // end of case 7
				
		break;  // end of case 7 --  Tetrahedron::dock_on_comput

		case 8 :  // { int psi, int dpsi }  // 1, 3

		{	constexpr size_t int_psi = 0;
			constexpr size_t int_psi_x = 1, int_psi_y = 2, int_psi_z = 3;

			assert ( this->implemented_cases .find (8) != this->implemented_cases .end() );

			const double det_PRSyz = ( PR_y * PS_z - PS_y * PR_z ) / 6.,
			             det_PQRyz = ( PQ_y * PR_z - PR_y * PQ_z ) / 6.,
			             det_PRSxz = ( PS_x * PR_z - PS_z * PR_x ) / 6.,
			             det_PQSxz = ( PQ_x * PS_z - PQ_z * PS_x ) / 6.,
			             det_PQRxz = ( PR_x * PQ_z - PQ_x * PR_z ) / 6.,
			             det_PQSyz = ( PS_y * PQ_z - PQ_y * PS_z ) / 6.,
			             det_PRSxy = ( PR_x * PS_y - PS_x * PR_y ) / 6.,
			             det_PQSxy = ( PS_x * PQ_y - PQ_x * PS_y ) / 6.,
			             det_PQRxy = ( PQ_x * PR_y - PR_x * PQ_y ) / 6.;

			double det = PQ_x * det_PRSyz + PR_x * det_PQSyz + PS_x * det_PQRyz;
			assert ( det > 0. );  // divided by 6 above

			RESULT ( 0, psi_P, int_psi ) =
			RESULT ( 0, psi_Q, int_psi ) =
			RESULT ( 0, psi_R, int_psi ) =
			RESULT ( 0, psi_S, int_psi ) = det / 4.;  // divided by 6 above
		
			RESULT ( 0, psi_P, int_psi_x ) = - det_PRSyz - det_PQSyz - det_PQRyz;
			RESULT ( 0, psi_Q, int_psi_x ) =   det_PRSyz;
			RESULT ( 0, psi_R, int_psi_x ) =   det_PQSyz;
			RESULT ( 0, psi_S, int_psi_x ) =   det_PQRyz;

			RESULT ( 0, psi_P, int_psi_y ) = - det_PRSxz - det_PQSxz - det_PQRxz;
			RESULT ( 0, psi_Q, int_psi_y ) =   det_PRSxz;
			RESULT ( 0, psi_R, int_psi_y ) =   det_PQSxz;
			RESULT ( 0, psi_S, int_psi_y ) =   det_PQRxz;

			RESULT ( 0, psi_P, int_psi_z ) = - det_PRSxy - det_PQSxy - det_PQRxy;
			RESULT ( 0, psi_Q, int_psi_z ) =   det_PRSxy;
			RESULT ( 0, psi_R, int_psi_z ) =   det_PQSxy;
			RESULT ( 0, psi_S, int_psi_z ) =   det_PQRxy;
			
		}  // end of case 8
				
		break;  // end of case 8 --  Tetrahedron::dock_on_comput

		case 9 :  // { int psi, int dpsi_dpsi }  // 1 and 4 (elasticity)

		{	constexpr size_t int_psi = 0;
			constexpr size_t int_psi_x_psi_x = 1, int_psi_x_psi_y = 2, int_psi_x_psi_z = 3,
			                 int_psi_y_psi_x = 4, int_psi_y_psi_y = 5, int_psi_y_psi_z = 6,
			                 int_psi_z_psi_x = 7, int_psi_z_psi_y = 8, int_psi_z_psi_z = 9;

			assert ( this->implemented_cases .find (9) != this->implemented_cases .end() );
			
			const double det_PRSyz = PR_y * PS_z - PS_y * PR_z,
			             det_PQRyz = PQ_y * PR_z - PR_y * PQ_z,
			             det_PRSxz = PS_x * PR_z - PS_z * PR_x,
			             det_PQSxz = PQ_x * PS_z - PQ_z * PS_x,
			             det_PQRxz = PR_x * PQ_z - PQ_x * PR_z,
			             det_PQSyz = PS_y * PQ_z - PQ_y * PS_z,
			             det_PRSxy = PR_x * PS_y - PS_x * PR_y,
			             det_PQSxy = PS_x * PQ_y - PQ_x * PS_y,
			             det_PQRxy = PQ_x * PR_y - PR_x * PQ_y;

			double det = PQ_x * det_PRSyz + PR_x * det_PQSyz + PS_x * det_PQRyz;
			assert ( det > 0. );

			RESULT ( psi_P, psi_P, int_psi ) =
			RESULT ( psi_P, psi_Q, int_psi ) =
			RESULT ( psi_P, psi_R, int_psi ) =
			RESULT ( psi_P, psi_S, int_psi ) =
			RESULT ( psi_Q, psi_P, int_psi ) =
			RESULT ( psi_Q, psi_Q, int_psi ) =
			RESULT ( psi_Q, psi_R, int_psi ) =
			RESULT ( psi_Q, psi_S, int_psi ) =
			RESULT ( psi_R, psi_P, int_psi ) =
			RESULT ( psi_R, psi_Q, int_psi ) =
			RESULT ( psi_R, psi_R, int_psi ) =
			RESULT ( psi_R, psi_S, int_psi ) =
			RESULT ( psi_S, psi_P, int_psi ) =
			RESULT ( psi_S, psi_Q, int_psi ) =
			RESULT ( psi_S, psi_R, int_psi ) =
			RESULT ( psi_S, psi_S, int_psi ) = det / 24.;
		
			det *= 6.;

			{ // just a block of code for hiding 'spi'
			const double sp1 = det_PRSyz * det_PRSyz / det,
			             sp2 = det_PQSyz * det_PRSyz / det,
			             sp3 = det_PQRyz * det_PRSyz / det,
			             sp5 = det_PQSyz * det_PQSyz / det,
			             sp6 = det_PQRyz * det_PQSyz / det,
			             sp9 = det_PQRyz * det_PQRyz / det;
			// equivalently, we may divide the second-order determinants by square root of det
			// but is it faster ?  is computing one square root faster than performing 36 divisions ?

			RESULT ( psi_P, psi_P, int_psi_x_psi_x ) =
					sp1 + sp2 + sp3 + sp2 + sp5 + sp6 + sp3 + sp6 + sp9;
			RESULT ( psi_P, psi_Q, int_psi_x_psi_x ) = 
			RESULT ( psi_Q, psi_P, int_psi_x_psi_x ) = - sp1 - sp2 - sp3;
			RESULT ( psi_P, psi_R, int_psi_x_psi_x ) = 
			RESULT ( psi_R, psi_P, int_psi_x_psi_x ) = - sp2 - sp5 - sp6;
			RESULT ( psi_P, psi_S, int_psi_x_psi_x ) = 
			RESULT ( psi_S, psi_P, int_psi_x_psi_x ) = - sp3 - sp6 - sp9;
			RESULT ( psi_Q, psi_Q, int_psi_x_psi_x ) =   sp1;
			RESULT ( psi_Q, psi_R, int_psi_x_psi_x ) = 
			RESULT ( psi_R, psi_Q, int_psi_x_psi_x ) =   sp2;
			RESULT ( psi_Q, psi_S, int_psi_x_psi_x ) = 
			RESULT ( psi_S, psi_Q, int_psi_x_psi_x ) =   sp3;
			RESULT ( psi_R, psi_R, int_psi_x_psi_x ) =   sp5;
			RESULT ( psi_R, psi_S, int_psi_x_psi_x ) = 
			RESULT ( psi_S, psi_R, int_psi_x_psi_x ) =   sp6;
			RESULT ( psi_S, psi_S, int_psi_x_psi_x ) =   sp9;
			} // just a block of code

			{ // just a block of code for hiding 'spi'
			const double sp1 = det_PRSxz * det_PRSxz / det,
			             sp2 = det_PQSxz * det_PRSxz / det,
			             sp3 = det_PQRxz * det_PRSxz / det,
			             sp5 = det_PQSxz * det_PQSxz / det,
			             sp6 = det_PQRxz * det_PQSxz / det,
			             sp9 = det_PQRxz * det_PQRxz / det;
			// equivalently, we may divide the second-order determinants by square root of det
			// but is it faster ?  is computing one square root faster than performing 36 divisions ?

			RESULT ( psi_P, psi_P, int_psi_y_psi_y ) =
					sp1 + sp2 + sp3 + sp2 + sp5 + sp6 + sp3 + sp6 + sp9;
			RESULT ( psi_P, psi_Q, int_psi_y_psi_y ) = 
			RESULT ( psi_Q, psi_P, int_psi_y_psi_y ) = - sp1 - sp2 - sp3;
			RESULT ( psi_P, psi_R, int_psi_y_psi_y ) = 
			RESULT ( psi_R, psi_P, int_psi_y_psi_y ) = - sp2 - sp5 - sp6;
			RESULT ( psi_P, psi_S, int_psi_y_psi_y ) = 
			RESULT ( psi_S, psi_P, int_psi_y_psi_y ) = - sp3 - sp6 - sp9;
			RESULT ( psi_Q, psi_Q, int_psi_y_psi_y ) =   sp1;
			RESULT ( psi_Q, psi_R, int_psi_y_psi_y ) = 
			RESULT ( psi_R, psi_Q, int_psi_y_psi_y ) =   sp2;
			RESULT ( psi_Q, psi_S, int_psi_y_psi_y ) = 
			RESULT ( psi_S, psi_Q, int_psi_y_psi_y ) =   sp3;
			RESULT ( psi_R, psi_R, int_psi_y_psi_y ) =   sp5;
			RESULT ( psi_R, psi_S, int_psi_y_psi_y ) = 
			RESULT ( psi_S, psi_R, int_psi_y_psi_y ) =   sp6;
			RESULT ( psi_S, psi_S, int_psi_y_psi_y ) =   sp9;
			} // just a block of code

			{ // just a block of code for hiding 'spi'
			const double sp1 = det_PRSxy * det_PRSxy / det,
			             sp2 = det_PQSxy * det_PRSxy / det,
			             sp3 = det_PQRxy * det_PRSxy / det,
			             sp5 = det_PQSxy * det_PQSxy / det,
			             sp6 = det_PQRxy * det_PQSxy / det,
			             sp9 = det_PQRxy * det_PQRxy / det;
			// equivalently, we may divide the second-order determinants by square root of det
			// but is it faster ?  is computing one square root faster than performing 36 divisions ?

			RESULT ( psi_P, psi_P, int_psi_z_psi_z ) =
					sp1 + sp2 + sp3 + sp2 + sp5 + sp6 + sp3 + sp6 + sp9;
			RESULT ( psi_P, psi_Q, int_psi_z_psi_z ) = 
			RESULT ( psi_Q, psi_P, int_psi_z_psi_z ) = - sp1 - sp2 - sp3;
			RESULT ( psi_P, psi_R, int_psi_z_psi_z ) = 
			RESULT ( psi_R, psi_P, int_psi_z_psi_z ) = - sp2 - sp5 - sp6;
			RESULT ( psi_P, psi_S, int_psi_z_psi_z ) = 
			RESULT ( psi_S, psi_P, int_psi_z_psi_z ) = - sp3 - sp6 - sp9;
			RESULT ( psi_Q, psi_Q, int_psi_z_psi_z ) =   sp1;
			RESULT ( psi_Q, psi_R, int_psi_z_psi_z ) = 
			RESULT ( psi_R, psi_Q, int_psi_z_psi_z ) =   sp2;
			RESULT ( psi_Q, psi_S, int_psi_z_psi_z ) = 
			RESULT ( psi_S, psi_Q, int_psi_z_psi_z ) =   sp3;
			RESULT ( psi_R, psi_R, int_psi_z_psi_z ) =   sp5;
			RESULT ( psi_R, psi_S, int_psi_z_psi_z ) = 
			RESULT ( psi_S, psi_R, int_psi_z_psi_z ) =   sp6;
			RESULT ( psi_S, psi_S, int_psi_z_psi_z ) =   sp9;
			} // just a block of code

			{ // just a block of code for hiding 'spi'
			const double sp1 = det_PRSyz * det_PRSxz / det,
			             sp2 = det_PQSyz * det_PRSxz / det,
			             sp3 = det_PQRyz * det_PRSxz / det,
			             sp4 = det_PRSyz * det_PQSxz / det,
			             sp5 = det_PQSyz * det_PQSxz / det,
			             sp6 = det_PQRyz * det_PQSxz / det,
			             sp7 = det_PRSyz * det_PQRxz / det,
			             sp8 = det_PQSyz * det_PQRxz / det,
			             sp9 = det_PQRyz * det_PQRxz / det;
			// equivalently, we may divide the second-order determinants by square root of det
			// but is it faster ?  is computing one square root faster than performing 36 divisions ?

			RESULT ( psi_P, psi_P, int_psi_x_psi_y ) =
			RESULT ( psi_P, psi_P, int_psi_y_psi_x ) =
					sp1 + sp4 + sp7 + sp2 + sp5 + sp8 + sp3 + sp6 + sp9;
			RESULT ( psi_P, psi_Q, int_psi_x_psi_y ) =
			RESULT ( psi_Q, psi_P, int_psi_y_psi_x ) = - sp1 - sp2 - sp3;
			RESULT ( psi_P, psi_R, int_psi_x_psi_y ) =
			RESULT ( psi_R, psi_P, int_psi_y_psi_x ) = - sp4 - sp5 - sp6;
			RESULT ( psi_P, psi_S, int_psi_x_psi_y ) =
			RESULT ( psi_S, psi_P, int_psi_y_psi_x ) = - sp7 - sp8 - sp9; 
			RESULT ( psi_Q, psi_P, int_psi_x_psi_y ) =
			RESULT ( psi_P, psi_Q, int_psi_y_psi_x ) = - sp1 - sp4 - sp7;
			RESULT ( psi_Q, psi_Q, int_psi_x_psi_y ) =
			RESULT ( psi_Q, psi_Q, int_psi_y_psi_x ) =   sp1;
			RESULT ( psi_Q, psi_R, int_psi_x_psi_y ) =
			RESULT ( psi_R, psi_Q, int_psi_y_psi_x ) =   sp4;
			RESULT ( psi_Q, psi_S, int_psi_x_psi_y ) =
			RESULT ( psi_S, psi_Q, int_psi_y_psi_x ) =   sp7;
			RESULT ( psi_R, psi_P, int_psi_x_psi_y ) =
			RESULT ( psi_P, psi_R, int_psi_y_psi_x ) = - sp2 - sp5 - sp8;
			RESULT ( psi_R, psi_Q, int_psi_x_psi_y ) =
			RESULT ( psi_Q, psi_R, int_psi_y_psi_x ) =   sp2;
			RESULT ( psi_R, psi_R, int_psi_x_psi_y ) =
			RESULT ( psi_R, psi_R, int_psi_y_psi_x ) =   sp5;
			RESULT ( psi_R, psi_S, int_psi_x_psi_y ) =
			RESULT ( psi_S, psi_R, int_psi_y_psi_x ) =   sp8;
			RESULT ( psi_S, psi_P, int_psi_x_psi_y ) =
			RESULT ( psi_P, psi_S, int_psi_y_psi_x ) = - sp3 - sp6 - sp9;
			RESULT ( psi_S, psi_Q, int_psi_x_psi_y ) =
			RESULT ( psi_Q, psi_S, int_psi_y_psi_x ) =   sp3;
			RESULT ( psi_S, psi_R, int_psi_x_psi_y ) =
			RESULT ( psi_R, psi_S, int_psi_y_psi_x ) =   sp6;
			RESULT ( psi_S, psi_S, int_psi_x_psi_y ) =
			RESULT ( psi_S, psi_S, int_psi_y_psi_x ) =   sp9;
			} // just a block of code

			{ // just a block of code for hiding 'spi'
			const double sp1 = det_PRSyz * det_PRSxy / det,
			             sp2 = det_PQSyz * det_PRSxy / det,
			             sp3 = det_PQRyz * det_PRSxy / det,
			             sp4 = det_PRSyz * det_PQSxy / det,
			             sp5 = det_PQSyz * det_PQSxy / det,
			             sp6 = det_PQRyz * det_PQSxy / det,
			             sp7 = det_PRSyz * det_PQRxy / det,
			             sp8 = det_PQSyz * det_PQRxy / det,
			             sp9 = det_PQRyz * det_PQRxy / det;
			// equivalently, we may divide the second-order determinants by square root of det
			// but is it faster ?  is computing one square root faster than performing 36 divisions ?

			RESULT ( psi_P, psi_P, int_psi_x_psi_z ) =
			RESULT ( psi_P, psi_P, int_psi_z_psi_x ) =
					sp1 + sp4 + sp7 + sp2 + sp5 + sp8 + sp3 + sp6 + sp9;
			RESULT ( psi_P, psi_Q, int_psi_x_psi_z ) =
			RESULT ( psi_Q, psi_P, int_psi_z_psi_x ) = - sp1 - sp2 - sp3;
			RESULT ( psi_P, psi_R, int_psi_x_psi_z ) =
			RESULT ( psi_R, psi_P, int_psi_z_psi_x ) = - sp4 - sp5 - sp6;
			RESULT ( psi_P, psi_S, int_psi_x_psi_z ) =
			RESULT ( psi_S, psi_P, int_psi_z_psi_x ) = - sp7 - sp8 - sp9; 
			RESULT ( psi_Q, psi_P, int_psi_x_psi_z ) =
			RESULT ( psi_P, psi_Q, int_psi_z_psi_x ) = - sp1 - sp4 - sp7;
			RESULT ( psi_Q, psi_Q, int_psi_x_psi_z ) =
			RESULT ( psi_Q, psi_Q, int_psi_z_psi_x ) =   sp1;
			RESULT ( psi_Q, psi_R, int_psi_x_psi_z ) =
			RESULT ( psi_R, psi_Q, int_psi_z_psi_x ) =   sp4;
			RESULT ( psi_Q, psi_S, int_psi_x_psi_z ) =
			RESULT ( psi_S, psi_Q, int_psi_z_psi_x ) =   sp7;
			RESULT ( psi_R, psi_P, int_psi_x_psi_z ) =
			RESULT ( psi_P, psi_R, int_psi_z_psi_x ) = - sp2 - sp5 - sp8;
			RESULT ( psi_R, psi_Q, int_psi_x_psi_z ) =
			RESULT ( psi_Q, psi_R, int_psi_z_psi_x ) =   sp2;
			RESULT ( psi_R, psi_R, int_psi_x_psi_z ) =
			RESULT ( psi_R, psi_R, int_psi_z_psi_x ) =   sp5;
			RESULT ( psi_R, psi_S, int_psi_x_psi_z ) =
			RESULT ( psi_S, psi_R, int_psi_z_psi_x ) =   sp8;
			RESULT ( psi_S, psi_P, int_psi_x_psi_z ) =
			RESULT ( psi_P, psi_S, int_psi_z_psi_x ) = - sp3 - sp6 - sp9;
			RESULT ( psi_S, psi_Q, int_psi_x_psi_z ) =
			RESULT ( psi_Q, psi_S, int_psi_z_psi_x ) =   sp3;
			RESULT ( psi_S, psi_R, int_psi_x_psi_z ) =
			RESULT ( psi_R, psi_S, int_psi_z_psi_x ) =   sp6;
			RESULT ( psi_S, psi_S, int_psi_x_psi_z ) =
			RESULT ( psi_S, psi_S, int_psi_z_psi_x ) =   sp9;
			} // just a block of code

			{ // just a block of code for hiding 'spi'
			const double sp1 = det_PRSxz * det_PRSxy / det,
			             sp2 = det_PQSxz * det_PRSxy / det,
			             sp3 = det_PQRxz * det_PRSxy / det,
			             sp4 = det_PRSxz * det_PQSxy / det,
			             sp5 = det_PQSxz * det_PQSxy / det,
			             sp6 = det_PQRxz * det_PQSxy / det,
			             sp7 = det_PRSxz * det_PQRxy / det,
			             sp8 = det_PQSxz * det_PQRxy / det,
			             sp9 = det_PQRxz * det_PQRxy / det;
			// equivalently, we may divide the second-order determinants by square root of det
			// but is it faster ?  is computing one square root faster than performing 36 divisions ?

			RESULT ( psi_P, psi_P, int_psi_y_psi_z ) =
			RESULT ( psi_P, psi_P, int_psi_z_psi_y ) =
					sp1 + sp4 + sp7 + sp2 + sp5 + sp8 + sp3 + sp6 + sp9;
			RESULT ( psi_P, psi_Q, int_psi_y_psi_z ) =
			RESULT ( psi_Q, psi_P, int_psi_z_psi_y ) = - sp1 - sp2 - sp3;
			RESULT ( psi_P, psi_R, int_psi_y_psi_z ) =
			RESULT ( psi_R, psi_P, int_psi_z_psi_y ) = - sp4 - sp5 - sp6;
			RESULT ( psi_P, psi_S, int_psi_y_psi_z ) =
			RESULT ( psi_S, psi_P, int_psi_z_psi_y ) = - sp7 - sp8 - sp9; 
			RESULT ( psi_Q, psi_P, int_psi_y_psi_z ) =
			RESULT ( psi_P, psi_Q, int_psi_z_psi_y ) = - sp1 - sp4 - sp7;
			RESULT ( psi_Q, psi_Q, int_psi_y_psi_z ) =
			RESULT ( psi_Q, psi_Q, int_psi_z_psi_y ) =   sp1;
			RESULT ( psi_Q, psi_R, int_psi_y_psi_z ) =
			RESULT ( psi_R, psi_Q, int_psi_z_psi_y ) =   sp4;
			RESULT ( psi_Q, psi_S, int_psi_y_psi_z ) =
			RESULT ( psi_S, psi_Q, int_psi_z_psi_y ) =   sp7;
			RESULT ( psi_R, psi_P, int_psi_y_psi_z ) =
			RESULT ( psi_P, psi_R, int_psi_z_psi_y ) = - sp2 - sp5 - sp8;
			RESULT ( psi_R, psi_Q, int_psi_y_psi_z ) =
			RESULT ( psi_Q, psi_R, int_psi_z_psi_y ) =   sp2;
			RESULT ( psi_R, psi_R, int_psi_y_psi_z ) =
			RESULT ( psi_R, psi_R, int_psi_z_psi_y ) =   sp5;
			RESULT ( psi_R, psi_S, int_psi_y_psi_z ) =
			RESULT ( psi_S, psi_R, int_psi_z_psi_y ) =   sp8;
			RESULT ( psi_S, psi_P, int_psi_y_psi_z ) =
			RESULT ( psi_P, psi_S, int_psi_z_psi_y ) = - sp3 - sp6 - sp9;
			RESULT ( psi_S, psi_Q, int_psi_y_psi_z ) =
			RESULT ( psi_Q, psi_S, int_psi_z_psi_y ) =   sp3;
			RESULT ( psi_S, psi_R, int_psi_y_psi_z ) =
			RESULT ( psi_R, psi_S, int_psi_z_psi_y ) =   sp6;
			RESULT ( psi_S, psi_S, int_psi_y_psi_z ) =
			RESULT ( psi_S, psi_S, int_psi_z_psi_y ) =   sp9;
			}	// just a block of code
			
		}  // end of case 9
				
		break;  // end of case 9 --  Tetrahedron::dock_on_comput
	
		case 10 :  // { int psi, int grad psi1 * grad psi2 }  // 1 and 5 (Laplacian)
		
		{	constexpr size_t int_psi = 0;
			constexpr size_t int_grad_psi_grad_psi = 1;

			assert ( this->implemented_cases .find (10) != this->implemented_cases .end() );

			const double det_PRSyz = PR_y * PS_z - PS_y * PR_z,
			             det_PQRyz = PQ_y * PR_z - PR_y * PQ_z,
			             det_PRSxz = PS_x * PR_z - PS_z * PR_x,
			             det_PQSxz = PQ_x * PS_z - PQ_z * PS_x,
			             det_PQRxz = PR_x * PQ_z - PQ_x * PR_z,
			             det_PQSyz = PS_y * PQ_z - PQ_y * PS_z,
			             det_PRSxy = PR_x * PS_y - PS_x * PR_y,
			             det_PQSxy = PS_x * PQ_y - PQ_x * PS_y,
			             det_PQRxy = PQ_x * PR_y - PR_x * PQ_y;

			double det = PQ_x * det_PRSyz + PR_x * det_PQSyz + PS_x * det_PQRyz;
			assert ( det > 0. );
			
			RESULT ( psi_P, psi_P, int_psi ) = 
			RESULT ( psi_P, psi_Q, int_psi ) = 
			RESULT ( psi_P, psi_R, int_psi ) = 
			RESULT ( psi_P, psi_S, int_psi ) = 
			RESULT ( psi_Q, psi_P, int_psi ) = 
			RESULT ( psi_Q, psi_Q, int_psi ) = 
			RESULT ( psi_Q, psi_R, int_psi ) = 
			RESULT ( psi_Q, psi_S, int_psi ) = 
			RESULT ( psi_R, psi_P, int_psi ) = 
			RESULT ( psi_R, psi_Q, int_psi ) = 
			RESULT ( psi_R, psi_R, int_psi ) = 
			RESULT ( psi_R, psi_S, int_psi ) = 
			RESULT ( psi_S, psi_P, int_psi ) = 
			RESULT ( psi_S, psi_Q, int_psi ) = 
			RESULT ( psi_S, psi_R, int_psi ) = 
			RESULT ( psi_S, psi_S, int_psi ) = det / 24.;
		
			det *= 6.;

			const double
				sp1 = ( det_PRSyz * det_PRSyz + det_PRSxz * det_PRSxz + det_PRSxy * det_PRSxy ) / det,
				sp2 = ( det_PQSyz * det_PRSyz + det_PQSxz * det_PRSxz + det_PQSxy * det_PRSxy ) / det,
				sp3 = ( det_PQRyz * det_PRSyz + det_PQRxz * det_PRSxz + det_PQRxy * det_PRSxy ) / det,
				sp5 = ( det_PQSyz * det_PQSyz + det_PQSxz * det_PQSxz + det_PQSxy * det_PQSxy ) / det,
				sp6 = ( det_PQRyz * det_PQSyz + det_PQRxz * det_PQSxz + det_PQRxy * det_PQSxy ) / det,
				sp9 = ( det_PQRyz * det_PQRyz + det_PQRxz * det_PQRxz + det_PQRxy * det_PQRxy ) / det;
			
			RESULT ( psi_P, psi_P, int_grad_psi_grad_psi ) =
					sp1 + sp2 + sp3 + sp2 + sp5 + sp6 + sp3 + sp6 + sp9;
			RESULT ( psi_P, psi_Q, int_grad_psi_grad_psi ) = 
			RESULT ( psi_Q, psi_P, int_grad_psi_grad_psi ) = - sp1 - sp2 - sp3;
			RESULT ( psi_P, psi_R, int_grad_psi_grad_psi ) = 
			RESULT ( psi_R, psi_P, int_grad_psi_grad_psi ) = - sp2 - sp5 - sp6;
			RESULT ( psi_P, psi_S, int_grad_psi_grad_psi ) = 
			RESULT ( psi_S, psi_P, int_grad_psi_grad_psi ) = - sp3 - sp6 - sp9;
			RESULT ( psi_Q, psi_Q, int_grad_psi_grad_psi ) =   sp1;
			RESULT ( psi_Q, psi_R, int_grad_psi_grad_psi ) = 
			RESULT ( psi_R, psi_Q, int_grad_psi_grad_psi ) =   sp2;
			RESULT ( psi_Q, psi_S, int_grad_psi_grad_psi ) = 
			RESULT ( psi_S, psi_Q, int_grad_psi_grad_psi ) =   sp3;
			RESULT ( psi_R, psi_R, int_grad_psi_grad_psi ) =   sp5;
			RESULT ( psi_R, psi_S, int_grad_psi_grad_psi ) = 
			RESULT ( psi_S, psi_R, int_grad_psi_grad_psi ) =   sp6;
			RESULT ( psi_S, psi_S, int_grad_psi_grad_psi ) =   sp9;
			
		}  // end of case 10
				
		break;  // end of case 10 --  Tetrahedron::dock_on_comput
	
		case 11 :  // { int psi .deriv(x), int psi1 .deriv(x) * psi2 .deriv(y) }  // 3 and 4
		
		{	constexpr size_t int_psi_x = 0, int_psi_y = 1, int_psi_z = 2;
			constexpr size_t int_psi_x_psi_x = 3, int_psi_x_psi_y = 4, int_psi_x_psi_z = 5,
			                 int_psi_y_psi_x = 6, int_psi_y_psi_y = 7, int_psi_y_psi_z = 8,
			                 int_psi_z_psi_x = 9, int_psi_z_psi_y = 10, int_psi_z_psi_z = 11;

			assert ( this->implemented_cases .find (11) != this->implemented_cases .end() );
			
			const double det_PRSyz = ( PR_y * PS_z - PS_y * PR_z ) / 6.,
			             det_PQRyz = ( PQ_y * PR_z - PR_y * PQ_z ) / 6.,
			             det_PRSxz = ( PS_x * PR_z - PS_z * PR_x ) / 6.,
			             det_PQSxz = ( PQ_x * PS_z - PQ_z * PS_x ) / 6.,
			             det_PQRxz = ( PR_x * PQ_z - PQ_x * PR_z ) / 6.,
			             det_PQSyz = ( PS_y * PQ_z - PQ_y * PS_z ) / 6.,
			             det_PRSxy = ( PR_x * PS_y - PS_x * PR_y ) / 6.,
			             det_PQSxy = ( PS_x * PQ_y - PQ_x * PS_y ) / 6.,
			             det_PQRxy = ( PQ_x * PR_y - PR_x * PQ_y ) / 6.;

			RESULT ( psi_P, psi_P, int_psi_x ) =
			RESULT ( psi_P, psi_Q, int_psi_x ) =
			RESULT ( psi_P, psi_R, int_psi_x ) =
			RESULT ( psi_P, psi_S, int_psi_x ) = - det_PRSyz - det_PQSyz - det_PQRyz;
			RESULT ( psi_Q, psi_P, int_psi_x ) =
			RESULT ( psi_Q, psi_Q, int_psi_x ) =
			RESULT ( psi_Q, psi_R, int_psi_x ) =
			RESULT ( psi_Q, psi_S, int_psi_x ) =   det_PRSyz;
			RESULT ( psi_R, psi_P, int_psi_x ) =
			RESULT ( psi_R, psi_Q, int_psi_x ) =
			RESULT ( psi_R, psi_R, int_psi_x ) =
			RESULT ( psi_R, psi_S, int_psi_x ) =   det_PQSyz;
			RESULT ( psi_S, psi_P, int_psi_x ) =
			RESULT ( psi_S, psi_Q, int_psi_x ) =
			RESULT ( psi_S, psi_R, int_psi_x ) =
			RESULT ( psi_S, psi_S, int_psi_x ) =   det_PQRyz;

			RESULT ( psi_P, psi_P, int_psi_y ) =
			RESULT ( psi_P, psi_Q, int_psi_y ) =
			RESULT ( psi_P, psi_R, int_psi_y ) =
			RESULT ( psi_P, psi_S, int_psi_y ) = - det_PRSxz - det_PQSxz - det_PQRxz;
			RESULT ( psi_Q, psi_P, int_psi_y ) =
			RESULT ( psi_Q, psi_Q, int_psi_y ) =
			RESULT ( psi_Q, psi_R, int_psi_y ) =
			RESULT ( psi_Q, psi_S, int_psi_y ) =   det_PRSxz;
			RESULT ( psi_R, psi_P, int_psi_y ) =
			RESULT ( psi_R, psi_Q, int_psi_y ) =
			RESULT ( psi_R, psi_R, int_psi_y ) =
			RESULT ( psi_R, psi_S, int_psi_y ) =   det_PQSxz;
			RESULT ( psi_S, psi_P, int_psi_y ) =
			RESULT ( psi_S, psi_Q, int_psi_y ) =
			RESULT ( psi_S, psi_R, int_psi_y ) =
			RESULT ( psi_S, psi_S, int_psi_y ) =   det_PQRxz;

			RESULT ( psi_P, psi_P, int_psi_z ) =
			RESULT ( psi_P, psi_Q, int_psi_z ) =
			RESULT ( psi_P, psi_R, int_psi_z ) =
			RESULT ( psi_P, psi_S, int_psi_z ) = - det_PRSxy - det_PQSxy - det_PQRxy;
			RESULT ( psi_Q, psi_P, int_psi_z ) =
			RESULT ( psi_Q, psi_Q, int_psi_z ) =
			RESULT ( psi_Q, psi_R, int_psi_z ) =
			RESULT ( psi_Q, psi_S, int_psi_z ) =   det_PRSxy;
			RESULT ( psi_R, psi_P, int_psi_z ) =
			RESULT ( psi_R, psi_Q, int_psi_z ) =
			RESULT ( psi_R, psi_R, int_psi_z ) =
			RESULT ( psi_R, psi_S, int_psi_z ) =   det_PQSxy;
			RESULT ( psi_S, psi_P, int_psi_z ) =
			RESULT ( psi_S, psi_Q, int_psi_z ) =
			RESULT ( psi_S, psi_R, int_psi_z ) =
			RESULT ( psi_S, psi_S, int_psi_z ) =   det_PQRxy;
			
			double det = PQ_x * det_PRSyz + PR_x * det_PQSyz + PS_x * det_PQRyz;
			assert ( det > 0. );
			// second order determinants have been divided by 6 above

			{ // just a block of code for hiding 'spi'
			const double sp1 = det_PRSyz * det_PRSyz / det,
			             sp2 = det_PQSyz * det_PRSyz / det,
			             sp3 = det_PQRyz * det_PRSyz / det,
			             sp5 = det_PQSyz * det_PQSyz / det,
			             sp6 = det_PQRyz * det_PQSyz / det,
			             sp9 = det_PQRyz * det_PQRyz / det;
			// equivalently, we may divide the second-order determinants by square root of det
			// but is it faster ?  is computing one square root faster than performing 36 divisions ?

			RESULT ( psi_P, psi_P, int_psi_x_psi_x ) =
					sp1 + sp2 + sp3 + sp2 + sp5 + sp6 + sp3 + sp6 + sp9;
			RESULT ( psi_P, psi_Q, int_psi_x_psi_x ) = 
			RESULT ( psi_Q, psi_P, int_psi_x_psi_x ) = - sp1 - sp2 - sp3;
			RESULT ( psi_P, psi_R, int_psi_x_psi_x ) = 
			RESULT ( psi_R, psi_P, int_psi_x_psi_x ) = - sp2 - sp5 - sp6;
			RESULT ( psi_P, psi_S, int_psi_x_psi_x ) = 
			RESULT ( psi_S, psi_P, int_psi_x_psi_x ) = - sp3 - sp6 - sp9;
			RESULT ( psi_Q, psi_Q, int_psi_x_psi_x ) =   sp1;
			RESULT ( psi_Q, psi_R, int_psi_x_psi_x ) = 
			RESULT ( psi_R, psi_Q, int_psi_x_psi_x ) =   sp2;
			RESULT ( psi_Q, psi_S, int_psi_x_psi_x ) = 
			RESULT ( psi_S, psi_Q, int_psi_x_psi_x ) =   sp3;
			RESULT ( psi_R, psi_R, int_psi_x_psi_x ) =   sp5;
			RESULT ( psi_R, psi_S, int_psi_x_psi_x ) = 
			RESULT ( psi_S, psi_R, int_psi_x_psi_x ) =   sp6;
			RESULT ( psi_S, psi_S, int_psi_x_psi_x ) =   sp9;
			} // just a block of code

			{ // just a block of code for hiding 'spi'
			const double sp1 = det_PRSxz * det_PRSxz / det,
			             sp2 = det_PQSxz * det_PRSxz / det,
			             sp3 = det_PQRxz * det_PRSxz / det,
			             sp5 = det_PQSxz * det_PQSxz / det,
			             sp6 = det_PQRxz * det_PQSxz / det,
			             sp9 = det_PQRxz * det_PQRxz / det;
			// equivalently, we may divide the second-order determinants by square root of det
			// but is it faster ?  is computing one square root faster than performing 36 divisions ?

			RESULT ( psi_P, psi_P, int_psi_y_psi_y ) =
					sp1 + sp2 + sp3 + sp2 + sp5 + sp6 + sp3 + sp6 + sp9;
			RESULT ( psi_P, psi_Q, int_psi_y_psi_y ) = 
			RESULT ( psi_Q, psi_P, int_psi_y_psi_y ) = - sp1 - sp2 - sp3;
			RESULT ( psi_P, psi_R, int_psi_y_psi_y ) = 
			RESULT ( psi_R, psi_P, int_psi_y_psi_y ) = - sp2 - sp5 - sp6;
			RESULT ( psi_P, psi_S, int_psi_y_psi_y ) = 
			RESULT ( psi_S, psi_P, int_psi_y_psi_y ) = - sp3 - sp6 - sp9;
			RESULT ( psi_Q, psi_Q, int_psi_y_psi_y ) =   sp1;
			RESULT ( psi_Q, psi_R, int_psi_y_psi_y ) = 
			RESULT ( psi_R, psi_Q, int_psi_y_psi_y ) =   sp2;
			RESULT ( psi_Q, psi_S, int_psi_y_psi_y ) = 
			RESULT ( psi_S, psi_Q, int_psi_y_psi_y ) =   sp3;
			RESULT ( psi_R, psi_R, int_psi_y_psi_y ) =   sp5;
			RESULT ( psi_R, psi_S, int_psi_y_psi_y ) = 
			RESULT ( psi_S, psi_R, int_psi_y_psi_y ) =   sp6;
			RESULT ( psi_S, psi_S, int_psi_y_psi_y ) =   sp9;
			} // just a block of code

			{ // just a block of code for hiding 'spi'
			const double sp1 = det_PRSxy * det_PRSxy / det,
			             sp2 = det_PQSxy * det_PRSxy / det,
			             sp3 = det_PQRxy * det_PRSxy / det,
			             sp5 = det_PQSxy * det_PQSxy / det,
			             sp6 = det_PQRxy * det_PQSxy / det,
			             sp9 = det_PQRxy * det_PQRxy / det;
			// equivalently, we may divide the second-order determinants by square root of det
			// but is it faster ?  is computing one square root faster than performing 36 divisions ?

			RESULT ( psi_P, psi_P, int_psi_z_psi_z ) =
					sp1 + sp2 + sp3 + sp2 + sp5 + sp6 + sp3 + sp6 + sp9;
			RESULT ( psi_P, psi_Q, int_psi_z_psi_z ) = 
			RESULT ( psi_Q, psi_P, int_psi_z_psi_z ) = - sp1 - sp2 - sp3;
			RESULT ( psi_P, psi_R, int_psi_z_psi_z ) = 
			RESULT ( psi_R, psi_P, int_psi_z_psi_z ) = - sp2 - sp5 - sp6;
			RESULT ( psi_P, psi_S, int_psi_z_psi_z ) = 
			RESULT ( psi_S, psi_P, int_psi_z_psi_z ) = - sp3 - sp6 - sp9;
			RESULT ( psi_Q, psi_Q, int_psi_z_psi_z ) =   sp1;
			RESULT ( psi_Q, psi_R, int_psi_z_psi_z ) = 
			RESULT ( psi_R, psi_Q, int_psi_z_psi_z ) =   sp2;
			RESULT ( psi_Q, psi_S, int_psi_z_psi_z ) = 
			RESULT ( psi_S, psi_Q, int_psi_z_psi_z ) =   sp3;
			RESULT ( psi_R, psi_R, int_psi_z_psi_z ) =   sp5;
			RESULT ( psi_R, psi_S, int_psi_z_psi_z ) = 
			RESULT ( psi_S, psi_R, int_psi_z_psi_z ) =   sp6;
			RESULT ( psi_S, psi_S, int_psi_z_psi_z ) =   sp9;
			} // just a block of code

			{ // just a block of code for hiding 'spi'
			const double sp1 = det_PRSyz * det_PRSxz / det,
			             sp2 = det_PQSyz * det_PRSxz / det,
			             sp3 = det_PQRyz * det_PRSxz / det,
			             sp4 = det_PRSyz * det_PQSxz / det,
			             sp5 = det_PQSyz * det_PQSxz / det,
			             sp6 = det_PQRyz * det_PQSxz / det,
			             sp7 = det_PRSyz * det_PQRxz / det,
			             sp8 = det_PQSyz * det_PQRxz / det,
			             sp9 = det_PQRyz * det_PQRxz / det;
			// equivalently, we may divide the second-order determinants by square root of det
			// but is it faster ?  is computing one square root faster than performing 36 divisions ?

			RESULT ( psi_P, psi_P, int_psi_x_psi_y ) =
			RESULT ( psi_P, psi_P, int_psi_y_psi_x ) =
					sp1 + sp4 + sp7 + sp2 + sp5 + sp8 + sp3 + sp6 + sp9;
			RESULT ( psi_P, psi_Q, int_psi_x_psi_y ) =
			RESULT ( psi_Q, psi_P, int_psi_y_psi_x ) = - sp1 - sp2 - sp3;
			RESULT ( psi_P, psi_R, int_psi_x_psi_y ) =
			RESULT ( psi_R, psi_P, int_psi_y_psi_x ) = - sp4 - sp5 - sp6;
			RESULT ( psi_P, psi_S, int_psi_x_psi_y ) =
			RESULT ( psi_S, psi_P, int_psi_y_psi_x ) = - sp7 - sp8 - sp9; 
			RESULT ( psi_Q, psi_P, int_psi_x_psi_y ) =
			RESULT ( psi_P, psi_Q, int_psi_y_psi_x ) = - sp1 - sp4 - sp7;
			RESULT ( psi_Q, psi_Q, int_psi_x_psi_y ) =
			RESULT ( psi_Q, psi_Q, int_psi_y_psi_x ) =   sp1;
			RESULT ( psi_Q, psi_R, int_psi_x_psi_y ) =
			RESULT ( psi_R, psi_Q, int_psi_y_psi_x ) =   sp4;
			RESULT ( psi_Q, psi_S, int_psi_x_psi_y ) =
			RESULT ( psi_S, psi_Q, int_psi_y_psi_x ) =   sp7;
			RESULT ( psi_R, psi_P, int_psi_x_psi_y ) =
			RESULT ( psi_P, psi_R, int_psi_y_psi_x ) = - sp2 - sp5 - sp8;
			RESULT ( psi_R, psi_Q, int_psi_x_psi_y ) =
			RESULT ( psi_Q, psi_R, int_psi_y_psi_x ) =   sp2;
			RESULT ( psi_R, psi_R, int_psi_x_psi_y ) =
			RESULT ( psi_R, psi_R, int_psi_y_psi_x ) =   sp5;
			RESULT ( psi_R, psi_S, int_psi_x_psi_y ) =
			RESULT ( psi_S, psi_R, int_psi_y_psi_x ) =   sp8;
			RESULT ( psi_S, psi_P, int_psi_x_psi_y ) =
			RESULT ( psi_P, psi_S, int_psi_y_psi_x ) = - sp3 - sp6 - sp9;
			RESULT ( psi_S, psi_Q, int_psi_x_psi_y ) =
			RESULT ( psi_Q, psi_S, int_psi_y_psi_x ) =   sp3;
			RESULT ( psi_S, psi_R, int_psi_x_psi_y ) =
			RESULT ( psi_R, psi_S, int_psi_y_psi_x ) =   sp6;
			RESULT ( psi_S, psi_S, int_psi_x_psi_y ) =
			RESULT ( psi_S, psi_S, int_psi_y_psi_x ) =   sp9;
			} // just a block of code

			{ // just a block of code for hiding 'spi'
			const double sp1 = det_PRSyz * det_PRSxy / det,
			             sp2 = det_PQSyz * det_PRSxy / det,
			             sp3 = det_PQRyz * det_PRSxy / det,
			             sp4 = det_PRSyz * det_PQSxy / det,
			             sp5 = det_PQSyz * det_PQSxy / det,
			             sp6 = det_PQRyz * det_PQSxy / det,
			             sp7 = det_PRSyz * det_PQRxy / det,
			             sp8 = det_PQSyz * det_PQRxy / det,
			             sp9 = det_PQRyz * det_PQRxy / det;
			// equivalently, we may divide the second-order determinants by square root of det
			// but is it faster ?  is computing one square root faster than performing 36 divisions ?

			RESULT ( psi_P, psi_P, int_psi_x_psi_z ) =
			RESULT ( psi_P, psi_P, int_psi_z_psi_x ) =
					sp1 + sp4 + sp7 + sp2 + sp5 + sp8 + sp3 + sp6 + sp9;
			RESULT ( psi_P, psi_Q, int_psi_x_psi_z ) =
			RESULT ( psi_Q, psi_P, int_psi_z_psi_x ) = - sp1 - sp2 - sp3;
			RESULT ( psi_P, psi_R, int_psi_x_psi_z ) =
			RESULT ( psi_R, psi_P, int_psi_z_psi_x ) = - sp4 - sp5 - sp6;
			RESULT ( psi_P, psi_S, int_psi_x_psi_z ) =
			RESULT ( psi_S, psi_P, int_psi_z_psi_x ) = - sp7 - sp8 - sp9; 
			RESULT ( psi_Q, psi_P, int_psi_x_psi_z ) =
			RESULT ( psi_P, psi_Q, int_psi_z_psi_x ) = - sp1 - sp4 - sp7;
			RESULT ( psi_Q, psi_Q, int_psi_x_psi_z ) =
			RESULT ( psi_Q, psi_Q, int_psi_z_psi_x ) =   sp1;
			RESULT ( psi_Q, psi_R, int_psi_x_psi_z ) =
			RESULT ( psi_R, psi_Q, int_psi_z_psi_x ) =   sp4;
			RESULT ( psi_Q, psi_S, int_psi_x_psi_z ) =
			RESULT ( psi_S, psi_Q, int_psi_z_psi_x ) =   sp7;
			RESULT ( psi_R, psi_P, int_psi_x_psi_z ) =
			RESULT ( psi_P, psi_R, int_psi_z_psi_x ) = - sp2 - sp5 - sp8;
			RESULT ( psi_R, psi_Q, int_psi_x_psi_z ) =
			RESULT ( psi_Q, psi_R, int_psi_z_psi_x ) =   sp2;
			RESULT ( psi_R, psi_R, int_psi_x_psi_z ) =
			RESULT ( psi_R, psi_R, int_psi_z_psi_x ) =   sp5;
			RESULT ( psi_R, psi_S, int_psi_x_psi_z ) =
			RESULT ( psi_S, psi_R, int_psi_z_psi_x ) =   sp8;
			RESULT ( psi_S, psi_P, int_psi_x_psi_z ) =
			RESULT ( psi_P, psi_S, int_psi_z_psi_x ) = - sp3 - sp6 - sp9;
			RESULT ( psi_S, psi_Q, int_psi_x_psi_z ) =
			RESULT ( psi_Q, psi_S, int_psi_z_psi_x ) =   sp3;
			RESULT ( psi_S, psi_R, int_psi_x_psi_z ) =
			RESULT ( psi_R, psi_S, int_psi_z_psi_x ) =   sp6;
			RESULT ( psi_S, psi_S, int_psi_x_psi_z ) =
			RESULT ( psi_S, psi_S, int_psi_z_psi_x ) =   sp9;
			} // just a block of code

			{ // just a block of code for hiding 'spi'
			const double sp1 = det_PRSxz * det_PRSxy / det,
			             sp2 = det_PQSxz * det_PRSxy / det,
			             sp3 = det_PQRxz * det_PRSxy / det,
			             sp4 = det_PRSxz * det_PQSxy / det,
			             sp5 = det_PQSxz * det_PQSxy / det,
			             sp6 = det_PQRxz * det_PQSxy / det,
			             sp7 = det_PRSxz * det_PQRxy / det,
			             sp8 = det_PQSxz * det_PQRxy / det,
			             sp9 = det_PQRxz * det_PQRxy / det;
			// equivalently, we may divide the second-order determinants by square root of det
			// but is it faster ?  is computing one square root faster than performing 36 divisions ?

			RESULT ( psi_P, psi_P, int_psi_y_psi_z ) =
			RESULT ( psi_P, psi_P, int_psi_z_psi_y ) =
					sp1 + sp4 + sp7 + sp2 + sp5 + sp8 + sp3 + sp6 + sp9;
			RESULT ( psi_P, psi_Q, int_psi_y_psi_z ) =
			RESULT ( psi_Q, psi_P, int_psi_z_psi_y ) = - sp1 - sp2 - sp3;
			RESULT ( psi_P, psi_R, int_psi_y_psi_z ) =
			RESULT ( psi_R, psi_P, int_psi_z_psi_y ) = - sp4 - sp5 - sp6;
			RESULT ( psi_P, psi_S, int_psi_y_psi_z ) =
			RESULT ( psi_S, psi_P, int_psi_z_psi_y ) = - sp7 - sp8 - sp9; 
			RESULT ( psi_Q, psi_P, int_psi_y_psi_z ) =
			RESULT ( psi_P, psi_Q, int_psi_z_psi_y ) = - sp1 - sp4 - sp7;
			RESULT ( psi_Q, psi_Q, int_psi_y_psi_z ) =
			RESULT ( psi_Q, psi_Q, int_psi_z_psi_y ) =   sp1;
			RESULT ( psi_Q, psi_R, int_psi_y_psi_z ) =
			RESULT ( psi_R, psi_Q, int_psi_z_psi_y ) =   sp4;
			RESULT ( psi_Q, psi_S, int_psi_y_psi_z ) =
			RESULT ( psi_S, psi_Q, int_psi_z_psi_y ) =   sp7;
			RESULT ( psi_R, psi_P, int_psi_y_psi_z ) =
			RESULT ( psi_P, psi_R, int_psi_z_psi_y ) = - sp2 - sp5 - sp8;
			RESULT ( psi_R, psi_Q, int_psi_y_psi_z ) =
			RESULT ( psi_Q, psi_R, int_psi_z_psi_y ) =   sp2;
			RESULT ( psi_R, psi_R, int_psi_y_psi_z ) =
			RESULT ( psi_R, psi_R, int_psi_z_psi_y ) =   sp5;
			RESULT ( psi_R, psi_S, int_psi_y_psi_z ) =
			RESULT ( psi_S, psi_R, int_psi_z_psi_y ) =   sp8;
			RESULT ( psi_S, psi_P, int_psi_y_psi_z ) =
			RESULT ( psi_P, psi_S, int_psi_z_psi_y ) = - sp3 - sp6 - sp9;
			RESULT ( psi_S, psi_Q, int_psi_y_psi_z ) =
			RESULT ( psi_Q, psi_S, int_psi_z_psi_y ) =   sp3;
			RESULT ( psi_S, psi_R, int_psi_y_psi_z ) =
			RESULT ( psi_R, psi_S, int_psi_z_psi_y ) =   sp6;
			RESULT ( psi_S, psi_S, int_psi_y_psi_z ) =
			RESULT ( psi_S, psi_S, int_psi_z_psi_y ) =   sp9;
			}  // just a block of code

		} // end of case 11
				
		break;  // end of case 11 --  Tetrahedron::dock_on_comput
			
		case 12 :  // { int psi1 * psi2, int psi1 .deriv(x) * psi2 .deriv(y) }  // 2 and 4

		{	constexpr size_t int_psi_psi = 0;
			constexpr size_t int_psi_x_psi_x = 1, int_psi_x_psi_y = 2, int_psi_x_psi_z = 3,
			                 int_psi_y_psi_x = 4, int_psi_y_psi_y = 5, int_psi_y_psi_z = 6,
			                 int_psi_z_psi_x = 7, int_psi_z_psi_y = 8, int_psi_z_psi_z = 9;
				
			assert ( this->implemented_cases .find (12) != this->implemented_cases .end() );
			
			const double det_PRSyz = PR_y * PS_z - PS_y * PR_z,
			             det_PQRyz = PQ_y * PR_z - PR_y * PQ_z,
			             det_PRSxz = PS_x * PR_z - PS_z * PR_x,
			             det_PQSxz = PQ_x * PS_z - PQ_z * PS_x,
			             det_PQRxz = PR_x * PQ_z - PQ_x * PR_z,
			             det_PQSyz = PS_y * PQ_z - PQ_y * PS_z,
			             det_PRSxy = PR_x * PS_y - PS_x * PR_y,
			             det_PQSxy = PS_x * PQ_y - PQ_x * PS_y,
			             det_PQRxy = PQ_x * PR_y - PR_x * PQ_y;

			double det = PQ_x * det_PRSyz + PR_x * det_PQSyz + PS_x * det_PQRyz;
			assert ( det > 0. );

			RESULT ( psi_P, psi_P, int_psi_psi ) =
			RESULT ( psi_Q, psi_Q, int_psi_psi ) =
			RESULT ( psi_R, psi_R, int_psi_psi ) =
			RESULT ( psi_S, psi_S, int_psi_psi ) = det / 60.;
			
			RESULT ( psi_P, psi_Q, int_psi_psi ) =
			RESULT ( psi_P, psi_R, int_psi_psi ) =
			RESULT ( psi_P, psi_S, int_psi_psi ) =
			RESULT ( psi_Q, psi_P, int_psi_psi ) =
			RESULT ( psi_Q, psi_R, int_psi_psi ) =
			RESULT ( psi_Q, psi_S, int_psi_psi ) =
			RESULT ( psi_R, psi_P, int_psi_psi ) =
			RESULT ( psi_R, psi_Q, int_psi_psi ) =
			RESULT ( psi_R, psi_S, int_psi_psi ) =
			RESULT ( psi_S, psi_P, int_psi_psi ) =
			RESULT ( psi_S, psi_Q, int_psi_psi ) =
			RESULT ( psi_S, psi_R, int_psi_psi ) = det / 120.;
			
			det *= 6.;

			{ // just a block of code for hiding 'spi'
			const double sp1 = det_PRSyz * det_PRSyz / det,
			             sp2 = det_PQSyz * det_PRSyz / det,
			             sp3 = det_PQRyz * det_PRSyz / det,
			             sp5 = det_PQSyz * det_PQSyz / det,
			             sp6 = det_PQRyz * det_PQSyz / det,
			             sp9 = det_PQRyz * det_PQRyz / det;
			// equivalently, we may divide the second-order determinants by square root of det
			// but is it faster ?  is computing one square root faster than performing 36 divisions ?

			RESULT ( psi_P, psi_P, int_psi_x_psi_x ) =
					sp1 + sp2 + sp3 + sp2 + sp5 + sp6 + sp3 + sp6 + sp9;
			RESULT ( psi_P, psi_Q, int_psi_x_psi_x ) = 
			RESULT ( psi_Q, psi_P, int_psi_x_psi_x ) = - sp1 - sp2 - sp3;
			RESULT ( psi_P, psi_R, int_psi_x_psi_x ) = 
			RESULT ( psi_R, psi_P, int_psi_x_psi_x ) = - sp2 - sp5 - sp6;
			RESULT ( psi_P, psi_S, int_psi_x_psi_x ) = 
			RESULT ( psi_S, psi_P, int_psi_x_psi_x ) = - sp3 - sp6 - sp9;
			RESULT ( psi_Q, psi_Q, int_psi_x_psi_x ) =   sp1;
			RESULT ( psi_Q, psi_R, int_psi_x_psi_x ) = 
			RESULT ( psi_R, psi_Q, int_psi_x_psi_x ) =   sp2;
			RESULT ( psi_Q, psi_S, int_psi_x_psi_x ) = 
			RESULT ( psi_S, psi_Q, int_psi_x_psi_x ) =   sp3;
			RESULT ( psi_R, psi_R, int_psi_x_psi_x ) =   sp5;
			RESULT ( psi_R, psi_S, int_psi_x_psi_x ) = 
			RESULT ( psi_S, psi_R, int_psi_x_psi_x ) =   sp6;
			RESULT ( psi_S, psi_S, int_psi_x_psi_x ) =   sp9;
			} // just a block of code

			{ // just a block of code for hiding 'spi'
			const double sp1 = det_PRSxz * det_PRSxz / det,
			             sp2 = det_PQSxz * det_PRSxz / det,
			             sp3 = det_PQRxz * det_PRSxz / det,
			             sp5 = det_PQSxz * det_PQSxz / det,
			             sp6 = det_PQRxz * det_PQSxz / det,
			             sp9 = det_PQRxz * det_PQRxz / det;
			// equivalently, we may divide the second-order determinants by square root of det
			// but is it faster ?  is computing one square root faster than performing 36 divisions ?

			RESULT ( psi_P, psi_P, int_psi_y_psi_y ) =
					sp1 + sp2 + sp3 + sp2 + sp5 + sp6 + sp3 + sp6 + sp9;
			RESULT ( psi_P, psi_Q, int_psi_y_psi_y ) = 
			RESULT ( psi_Q, psi_P, int_psi_y_psi_y ) = - sp1 - sp2 - sp3;
			RESULT ( psi_P, psi_R, int_psi_y_psi_y ) = 
			RESULT ( psi_R, psi_P, int_psi_y_psi_y ) = - sp2 - sp5 - sp6;
			RESULT ( psi_P, psi_S, int_psi_y_psi_y ) = 
			RESULT ( psi_S, psi_P, int_psi_y_psi_y ) = - sp3 - sp6 - sp9;
			RESULT ( psi_Q, psi_Q, int_psi_y_psi_y ) =   sp1;
			RESULT ( psi_Q, psi_R, int_psi_y_psi_y ) = 
			RESULT ( psi_R, psi_Q, int_psi_y_psi_y ) =   sp2;
			RESULT ( psi_Q, psi_S, int_psi_y_psi_y ) = 
			RESULT ( psi_S, psi_Q, int_psi_y_psi_y ) =   sp3;
			RESULT ( psi_R, psi_R, int_psi_y_psi_y ) =   sp5;
			RESULT ( psi_R, psi_S, int_psi_y_psi_y ) = 
			RESULT ( psi_S, psi_R, int_psi_y_psi_y ) =   sp6;
			RESULT ( psi_S, psi_S, int_psi_y_psi_y ) =   sp9;
			} // just a block of code

			{ // just a block of code for hiding 'spi'
			const double sp1 = det_PRSxy * det_PRSxy / det,
			             sp2 = det_PQSxy * det_PRSxy / det,
			             sp3 = det_PQRxy * det_PRSxy / det,
			             sp5 = det_PQSxy * det_PQSxy / det,
			             sp6 = det_PQRxy * det_PQSxy / det,
			             sp9 = det_PQRxy * det_PQRxy / det;
			// equivalently, we may divide the second-order determinants by square root of det
			// but is it faster ?  is computing one square root faster than performing 36 divisions ?

			RESULT ( psi_P, psi_P, int_psi_z_psi_z ) =
					sp1 + sp2 + sp3 + sp2 + sp5 + sp6 + sp3 + sp6 + sp9;
			RESULT ( psi_P, psi_Q, int_psi_z_psi_z ) = 
			RESULT ( psi_Q, psi_P, int_psi_z_psi_z ) = - sp1 - sp2 - sp3;
			RESULT ( psi_P, psi_R, int_psi_z_psi_z ) = 
			RESULT ( psi_R, psi_P, int_psi_z_psi_z ) = - sp2 - sp5 - sp6;
			RESULT ( psi_P, psi_S, int_psi_z_psi_z ) = 
			RESULT ( psi_S, psi_P, int_psi_z_psi_z ) = - sp3 - sp6 - sp9;
			RESULT ( psi_Q, psi_Q, int_psi_z_psi_z ) =   sp1;
			RESULT ( psi_Q, psi_R, int_psi_z_psi_z ) = 
			RESULT ( psi_R, psi_Q, int_psi_z_psi_z ) =   sp2;
			RESULT ( psi_Q, psi_S, int_psi_z_psi_z ) = 
			RESULT ( psi_S, psi_Q, int_psi_z_psi_z ) =   sp3;
			RESULT ( psi_R, psi_R, int_psi_z_psi_z ) =   sp5;
			RESULT ( psi_R, psi_S, int_psi_z_psi_z ) = 
			RESULT ( psi_S, psi_R, int_psi_z_psi_z ) =   sp6;
			RESULT ( psi_S, psi_S, int_psi_z_psi_z ) =   sp9;
			} // just a block of code

			{ // just a block of code for hiding 'spi'
			const double sp1 = det_PRSyz * det_PRSxz / det,
			             sp2 = det_PQSyz * det_PRSxz / det,
			             sp3 = det_PQRyz * det_PRSxz / det,
			             sp4 = det_PRSyz * det_PQSxz / det,
			             sp5 = det_PQSyz * det_PQSxz / det,
			             sp6 = det_PQRyz * det_PQSxz / det,
			             sp7 = det_PRSyz * det_PQRxz / det,
			             sp8 = det_PQSyz * det_PQRxz / det,
			             sp9 = det_PQRyz * det_PQRxz / det;
			// equivalently, we may divide the second-order determinants by square root of det
			// but is it faster ?  is computing one square root faster than performing 36 divisions ?

			RESULT ( psi_P, psi_P, int_psi_x_psi_y ) =
			RESULT ( psi_P, psi_P, int_psi_y_psi_x ) =
					sp1 + sp4 + sp7 + sp2 + sp5 + sp8 + sp3 + sp6 + sp9;
			RESULT ( psi_P, psi_Q, int_psi_x_psi_y ) =
			RESULT ( psi_Q, psi_P, int_psi_y_psi_x ) = - sp1 - sp2 - sp3;
			RESULT ( psi_P, psi_R, int_psi_x_psi_y ) =
			RESULT ( psi_R, psi_P, int_psi_y_psi_x ) = - sp4 - sp5 - sp6;
			RESULT ( psi_P, psi_S, int_psi_x_psi_y ) =
			RESULT ( psi_S, psi_P, int_psi_y_psi_x ) = - sp7 - sp8 - sp9; 
			RESULT ( psi_Q, psi_P, int_psi_x_psi_y ) =
			RESULT ( psi_P, psi_Q, int_psi_y_psi_x ) = - sp1 - sp4 - sp7;
			RESULT ( psi_Q, psi_Q, int_psi_x_psi_y ) =
			RESULT ( psi_Q, psi_Q, int_psi_y_psi_x ) =   sp1;
			RESULT ( psi_Q, psi_R, int_psi_x_psi_y ) =
			RESULT ( psi_R, psi_Q, int_psi_y_psi_x ) =   sp4;
			RESULT ( psi_Q, psi_S, int_psi_x_psi_y ) =
			RESULT ( psi_S, psi_Q, int_psi_y_psi_x ) =   sp7;
			RESULT ( psi_R, psi_P, int_psi_x_psi_y ) =
			RESULT ( psi_P, psi_R, int_psi_y_psi_x ) = - sp2 - sp5 - sp8;
			RESULT ( psi_R, psi_Q, int_psi_x_psi_y ) =
			RESULT ( psi_Q, psi_R, int_psi_y_psi_x ) =   sp2;
			RESULT ( psi_R, psi_R, int_psi_x_psi_y ) =
			RESULT ( psi_R, psi_R, int_psi_y_psi_x ) =   sp5;
			RESULT ( psi_R, psi_S, int_psi_x_psi_y ) =
			RESULT ( psi_S, psi_R, int_psi_y_psi_x ) =   sp8;
			RESULT ( psi_S, psi_P, int_psi_x_psi_y ) =
			RESULT ( psi_P, psi_S, int_psi_y_psi_x ) = - sp3 - sp6 - sp9;
			RESULT ( psi_S, psi_Q, int_psi_x_psi_y ) =
			RESULT ( psi_Q, psi_S, int_psi_y_psi_x ) =   sp3;
			RESULT ( psi_S, psi_R, int_psi_x_psi_y ) =
			RESULT ( psi_R, psi_S, int_psi_y_psi_x ) =   sp6;
			RESULT ( psi_S, psi_S, int_psi_x_psi_y ) =
			RESULT ( psi_S, psi_S, int_psi_y_psi_x ) =   sp9;
			} // just a block of code

			{ // just a block of code for hiding 'spi'
			const double sp1 = det_PRSyz * det_PRSxy / det,
			             sp2 = det_PQSyz * det_PRSxy / det,
			             sp3 = det_PQRyz * det_PRSxy / det,
			             sp4 = det_PRSyz * det_PQSxy / det,
			             sp5 = det_PQSyz * det_PQSxy / det,
			             sp6 = det_PQRyz * det_PQSxy / det,
			             sp7 = det_PRSyz * det_PQRxy / det,
			             sp8 = det_PQSyz * det_PQRxy / det,
			             sp9 = det_PQRyz * det_PQRxy / det;
			// equivalently, we may divide the second-order determinants by square root of det
			// but is it faster ?  is computing one square root faster than performing 36 divisions ?

			RESULT ( psi_P, psi_P, int_psi_x_psi_z ) =
			RESULT ( psi_P, psi_P, int_psi_z_psi_x ) =
					sp1 + sp4 + sp7 + sp2 + sp5 + sp8 + sp3 + sp6 + sp9;
			RESULT ( psi_P, psi_Q, int_psi_x_psi_z ) =
			RESULT ( psi_Q, psi_P, int_psi_z_psi_x ) = - sp1 - sp2 - sp3;
			RESULT ( psi_P, psi_R, int_psi_x_psi_z ) =
			RESULT ( psi_R, psi_P, int_psi_z_psi_x ) = - sp4 - sp5 - sp6;
			RESULT ( psi_P, psi_S, int_psi_x_psi_z ) =
			RESULT ( psi_S, psi_P, int_psi_z_psi_x ) = - sp7 - sp8 - sp9; 
			RESULT ( psi_Q, psi_P, int_psi_x_psi_z ) =
			RESULT ( psi_P, psi_Q, int_psi_z_psi_x ) = - sp1 - sp4 - sp7;
			RESULT ( psi_Q, psi_Q, int_psi_x_psi_z ) =
			RESULT ( psi_Q, psi_Q, int_psi_z_psi_x ) =   sp1;
			RESULT ( psi_Q, psi_R, int_psi_x_psi_z ) =
			RESULT ( psi_R, psi_Q, int_psi_z_psi_x ) =   sp4;
			RESULT ( psi_Q, psi_S, int_psi_x_psi_z ) =
			RESULT ( psi_S, psi_Q, int_psi_z_psi_x ) =   sp7;
			RESULT ( psi_R, psi_P, int_psi_x_psi_z ) =
			RESULT ( psi_P, psi_R, int_psi_z_psi_x ) = - sp2 - sp5 - sp8;
			RESULT ( psi_R, psi_Q, int_psi_x_psi_z ) =
			RESULT ( psi_Q, psi_R, int_psi_z_psi_x ) =   sp2;
			RESULT ( psi_R, psi_R, int_psi_x_psi_z ) =
			RESULT ( psi_R, psi_R, int_psi_z_psi_x ) =   sp5;
			RESULT ( psi_R, psi_S, int_psi_x_psi_z ) =
			RESULT ( psi_S, psi_R, int_psi_z_psi_x ) =   sp8;
			RESULT ( psi_S, psi_P, int_psi_x_psi_z ) =
			RESULT ( psi_P, psi_S, int_psi_z_psi_x ) = - sp3 - sp6 - sp9;
			RESULT ( psi_S, psi_Q, int_psi_x_psi_z ) =
			RESULT ( psi_Q, psi_S, int_psi_z_psi_x ) =   sp3;
			RESULT ( psi_S, psi_R, int_psi_x_psi_z ) =
			RESULT ( psi_R, psi_S, int_psi_z_psi_x ) =   sp6;
			RESULT ( psi_S, psi_S, int_psi_x_psi_z ) =
			RESULT ( psi_S, psi_S, int_psi_z_psi_x ) =   sp9;
			} // just a block of code

			{ // just a block of code for hiding 'spi'
			const double sp1 = det_PRSxz * det_PRSxy / det,
			             sp2 = det_PQSxz * det_PRSxy / det,
			             sp3 = det_PQRxz * det_PRSxy / det,
			             sp4 = det_PRSxz * det_PQSxy / det,
			             sp5 = det_PQSxz * det_PQSxy / det,
			             sp6 = det_PQRxz * det_PQSxy / det,
			             sp7 = det_PRSxz * det_PQRxy / det,
			             sp8 = det_PQSxz * det_PQRxy / det,
			             sp9 = det_PQRxz * det_PQRxy / det;
			// equivalently, we may divide the second-order determinants by square root of det
			// but is it faster ?  is computing one square root faster than performing 36 divisions ?

			RESULT ( psi_P, psi_P, int_psi_y_psi_z ) =
			RESULT ( psi_P, psi_P, int_psi_z_psi_y ) =
					sp1 + sp4 + sp7 + sp2 + sp5 + sp8 + sp3 + sp6 + sp9;
			RESULT ( psi_P, psi_Q, int_psi_y_psi_z ) =
			RESULT ( psi_Q, psi_P, int_psi_z_psi_y ) = - sp1 - sp2 - sp3;
			RESULT ( psi_P, psi_R, int_psi_y_psi_z ) =
			RESULT ( psi_R, psi_P, int_psi_z_psi_y ) = - sp4 - sp5 - sp6;
			RESULT ( psi_P, psi_S, int_psi_y_psi_z ) =
			RESULT ( psi_S, psi_P, int_psi_z_psi_y ) = - sp7 - sp8 - sp9; 
			RESULT ( psi_Q, psi_P, int_psi_y_psi_z ) =
			RESULT ( psi_P, psi_Q, int_psi_z_psi_y ) = - sp1 - sp4 - sp7;
			RESULT ( psi_Q, psi_Q, int_psi_y_psi_z ) =
			RESULT ( psi_Q, psi_Q, int_psi_z_psi_y ) =   sp1;
			RESULT ( psi_Q, psi_R, int_psi_y_psi_z ) =
			RESULT ( psi_R, psi_Q, int_psi_z_psi_y ) =   sp4;
			RESULT ( psi_Q, psi_S, int_psi_y_psi_z ) =
			RESULT ( psi_S, psi_Q, int_psi_z_psi_y ) =   sp7;
			RESULT ( psi_R, psi_P, int_psi_y_psi_z ) =
			RESULT ( psi_P, psi_R, int_psi_z_psi_y ) = - sp2 - sp5 - sp8;
			RESULT ( psi_R, psi_Q, int_psi_y_psi_z ) =
			RESULT ( psi_Q, psi_R, int_psi_z_psi_y ) =   sp2;
			RESULT ( psi_R, psi_R, int_psi_y_psi_z ) =
			RESULT ( psi_R, psi_R, int_psi_z_psi_y ) =   sp5;
			RESULT ( psi_R, psi_S, int_psi_y_psi_z ) =
			RESULT ( psi_S, psi_R, int_psi_z_psi_y ) =   sp8;
			RESULT ( psi_S, psi_P, int_psi_y_psi_z ) =
			RESULT ( psi_P, psi_S, int_psi_z_psi_y ) = - sp3 - sp6 - sp9;
			RESULT ( psi_S, psi_Q, int_psi_y_psi_z ) =
			RESULT ( psi_Q, psi_S, int_psi_z_psi_y ) =   sp3;
			RESULT ( psi_S, psi_R, int_psi_y_psi_z ) =
			RESULT ( psi_R, psi_S, int_psi_z_psi_y ) =   sp6;
			RESULT ( psi_S, psi_S, int_psi_y_psi_z ) =
			RESULT ( psi_S, psi_S, int_psi_z_psi_y ) =   sp9;
			}  // just a block of code

		} // end of case 12
				
		break;  // end of case 12 --  Tetrahedron::dock_on_comput
			
		case 13 :  // { int psi1 * psi2, int grad psi1 * grad psi2 }  // 2 and 5

		{	constexpr size_t int_psi_psi = 0;
			constexpr size_t int_grad_psi_grad_psi = 1;

			assert ( this->implemented_cases .find (13) != this->implemented_cases .end() );

			const double det_PRSyz = PR_y * PS_z - PS_y * PR_z,
			             det_PQRyz = PQ_y * PR_z - PR_y * PQ_z,
			             det_PRSxz = PS_x * PR_z - PS_z * PR_x,
			             det_PQSxz = PQ_x * PS_z - PQ_z * PS_x,
			             det_PQRxz = PR_x * PQ_z - PQ_x * PR_z,
			             det_PQSyz = PS_y * PQ_z - PQ_y * PS_z,
			             det_PRSxy = PR_x * PS_y - PS_x * PR_y,
			             det_PQSxy = PS_x * PQ_y - PQ_x * PS_y,
			             det_PQRxy = PQ_x * PR_y - PR_x * PQ_y;

			double det = PQ_x * det_PRSyz + PR_x * det_PQSyz + PS_x * det_PQRyz;
			assert ( det > 0. );
			
			RESULT ( psi_P, psi_P, int_psi_psi ) =
			RESULT ( psi_Q, psi_Q, int_psi_psi ) =
			RESULT ( psi_R, psi_R, int_psi_psi ) =
			RESULT ( psi_S, psi_S, int_psi_psi ) = det / 60.;
			
			RESULT ( psi_P, psi_Q, int_psi_psi ) =
			RESULT ( psi_P, psi_R, int_psi_psi ) =
			RESULT ( psi_P, psi_S, int_psi_psi ) =
			RESULT ( psi_Q, psi_P, int_psi_psi ) =
			RESULT ( psi_Q, psi_R, int_psi_psi ) =
			RESULT ( psi_Q, psi_S, int_psi_psi ) =
			RESULT ( psi_R, psi_P, int_psi_psi ) =
			RESULT ( psi_R, psi_Q, int_psi_psi ) =
			RESULT ( psi_R, psi_S, int_psi_psi ) =
			RESULT ( psi_S, psi_P, int_psi_psi ) =
			RESULT ( psi_S, psi_Q, int_psi_psi ) =
			RESULT ( psi_S, psi_R, int_psi_psi ) = det / 120.;
			
			det *= 6.;

			const double
				sp1 = ( det_PRSyz * det_PRSyz + det_PRSxz * det_PRSxz + det_PRSxy * det_PRSxy ) / det,
				sp2 = ( det_PQSyz * det_PRSyz + det_PQSxz * det_PRSxz + det_PQSxy * det_PRSxy ) / det,
				sp3 = ( det_PQRyz * det_PRSyz + det_PQRxz * det_PRSxz + det_PQRxy * det_PRSxy ) / det,
				sp5 = ( det_PQSyz * det_PQSyz + det_PQSxz * det_PQSxz + det_PQSxy * det_PQSxy ) / det,
				sp6 = ( det_PQRyz * det_PQSyz + det_PQRxz * det_PQSxz + det_PQRxy * det_PQSxy ) / det,
				sp9 = ( det_PQRyz * det_PQRyz + det_PQRxz * det_PQRxz + det_PQRxy * det_PQRxy ) / det;
			
			RESULT ( psi_P, psi_P, int_grad_psi_grad_psi ) =
					sp1 + sp2 + sp3 + sp2 + sp5 + sp6 + sp3 + sp6 + sp9;
			RESULT ( psi_P, psi_Q, int_grad_psi_grad_psi ) = 
			RESULT ( psi_Q, psi_P, int_grad_psi_grad_psi ) = - sp1 - sp2 - sp3;
			RESULT ( psi_P, psi_R, int_grad_psi_grad_psi ) = 
			RESULT ( psi_R, psi_P, int_grad_psi_grad_psi ) = - sp2 - sp5 - sp6;
			RESULT ( psi_P, psi_S, int_grad_psi_grad_psi ) = 
			RESULT ( psi_S, psi_P, int_grad_psi_grad_psi ) = - sp3 - sp6 - sp9;
			RESULT ( psi_Q, psi_Q, int_grad_psi_grad_psi ) =   sp1;
			RESULT ( psi_Q, psi_R, int_grad_psi_grad_psi ) = 
			RESULT ( psi_R, psi_Q, int_grad_psi_grad_psi ) =   sp2;
			RESULT ( psi_Q, psi_S, int_grad_psi_grad_psi ) = 
			RESULT ( psi_S, psi_Q, int_grad_psi_grad_psi ) =   sp3;
			RESULT ( psi_R, psi_R, int_grad_psi_grad_psi ) =   sp5;
			RESULT ( psi_R, psi_S, int_grad_psi_grad_psi ) = 
			RESULT ( psi_S, psi_R, int_grad_psi_grad_psi ) =   sp6;
			RESULT ( psi_S, psi_S, int_grad_psi_grad_psi ) =   sp9;

		} // end of case 13
				
		break;  // end of case 13 --  Tetrahedron::dock_on_comput
			
		case 14 :  // 1, 2, 3 and 4

		{	constexpr size_t int_psi = 0;
			constexpr size_t int_psi_psi = 1;
			constexpr size_t int_psi_x = 2, int_psi_y = 3, int_psi_z = 4;
			constexpr size_t int_psi_x_psi_x = 5, int_psi_x_psi_y = 6, int_psi_x_psi_z = 7,
			                 int_psi_y_psi_x = 8, int_psi_y_psi_y = 9, int_psi_y_psi_z = 10,
			                 int_psi_z_psi_x = 11, int_psi_z_psi_y = 12, int_psi_z_psi_z = 13;
				
			assert ( this->implemented_cases .find (14) != this->implemented_cases .end() );
			
			double det_PRSyz = PR_y * PS_z - PS_y * PR_z,
			       det_PQRyz = PQ_y * PR_z - PR_y * PQ_z,
			       det_PRSxz = PS_x * PR_z - PS_z * PR_x,
			       det_PQSxz = PQ_x * PS_z - PQ_z * PS_x,
			       det_PQRxz = PR_x * PQ_z - PQ_x * PR_z,
			       det_PQSyz = PS_y * PQ_z - PQ_y * PS_z,
			       det_PRSxy = PR_x * PS_y - PS_x * PR_y,
			       det_PQSxy = PS_x * PQ_y - PQ_x * PS_y,
			       det_PQRxy = PQ_x * PR_y - PR_x * PQ_y;

			double det = PQ_x * det_PRSyz + PR_x * det_PQSyz + PS_x * det_PQRyz;
			assert ( det > 0. );
			
			RESULT ( psi_P, psi_P, int_psi ) =
			RESULT ( psi_P, psi_Q, int_psi ) =
			RESULT ( psi_P, psi_R, int_psi ) =
			RESULT ( psi_P, psi_S, int_psi ) =
			RESULT ( psi_Q, psi_P, int_psi ) =
			RESULT ( psi_Q, psi_Q, int_psi ) =
			RESULT ( psi_Q, psi_R, int_psi ) =
			RESULT ( psi_Q, psi_S, int_psi ) =
			RESULT ( psi_R, psi_P, int_psi ) =
			RESULT ( psi_R, psi_Q, int_psi ) =
			RESULT ( psi_R, psi_R, int_psi ) =
			RESULT ( psi_R, psi_S, int_psi ) =
			RESULT ( psi_S, psi_P, int_psi ) =
			RESULT ( psi_S, psi_Q, int_psi ) =
			RESULT ( psi_S, psi_R, int_psi ) =
			RESULT ( psi_S, psi_S, int_psi ) = det / 24.;

			// end of case 1 (within 14)
			
			RESULT ( psi_P, psi_P, int_psi_psi ) =
			RESULT ( psi_Q, psi_Q, int_psi_psi ) =
			RESULT ( psi_R, psi_R, int_psi_psi ) =
			RESULT ( psi_S, psi_S, int_psi_psi ) = det / 60.;
			
			RESULT ( psi_P, psi_Q, int_psi_psi ) =
			RESULT ( psi_P, psi_R, int_psi_psi ) =
			RESULT ( psi_P, psi_S, int_psi_psi ) =
			RESULT ( psi_Q, psi_P, int_psi_psi ) =
			RESULT ( psi_Q, psi_R, int_psi_psi ) =
			RESULT ( psi_Q, psi_S, int_psi_psi ) =
			RESULT ( psi_R, psi_P, int_psi_psi ) =
			RESULT ( psi_R, psi_Q, int_psi_psi ) =
			RESULT ( psi_R, psi_S, int_psi_psi ) =
			RESULT ( psi_S, psi_P, int_psi_psi ) =
			RESULT ( psi_S, psi_Q, int_psi_psi ) =
			RESULT ( psi_S, psi_R, int_psi_psi ) = det / 120.;
						
			// end of case 2 (within 14)

			det_PRSyz /= 6.;  det_PQRyz /= 6.;  det_PRSxz /= 6.;
			det_PQSxz /= 6.;  det_PQRxz /= 6.;  det_PQSyz /= 6.;
			det_PRSxy /= 6.;  det_PQSxy /= 6.;  det_PQRxy /= 6.;

			RESULT ( psi_P, psi_P, int_psi_x ) =
			RESULT ( psi_P, psi_Q, int_psi_x ) =
			RESULT ( psi_P, psi_R, int_psi_x ) =
			RESULT ( psi_P, psi_S, int_psi_x ) = - det_PRSyz - det_PQSyz - det_PQRyz;
			RESULT ( psi_Q, psi_P, int_psi_x ) =
			RESULT ( psi_Q, psi_Q, int_psi_x ) =
			RESULT ( psi_Q, psi_R, int_psi_x ) =
			RESULT ( psi_Q, psi_S, int_psi_x ) =   det_PRSyz;
			RESULT ( psi_R, psi_P, int_psi_x ) =
			RESULT ( psi_R, psi_Q, int_psi_x ) =
			RESULT ( psi_R, psi_R, int_psi_x ) =
			RESULT ( psi_R, psi_S, int_psi_x ) =   det_PQSyz;
			RESULT ( psi_S, psi_P, int_psi_x ) =
			RESULT ( psi_S, psi_Q, int_psi_x ) =
			RESULT ( psi_S, psi_R, int_psi_x ) =
			RESULT ( psi_S, psi_S, int_psi_x ) =   det_PQRyz;

			RESULT ( psi_P, psi_P, int_psi_y ) =
			RESULT ( psi_P, psi_Q, int_psi_y ) =
			RESULT ( psi_P, psi_R, int_psi_y ) =
			RESULT ( psi_P, psi_S, int_psi_y ) = - det_PRSxz - det_PQSxz - det_PQRxz;
			RESULT ( psi_Q, psi_P, int_psi_y ) =
			RESULT ( psi_Q, psi_Q, int_psi_y ) =
			RESULT ( psi_Q, psi_R, int_psi_y ) =
			RESULT ( psi_Q, psi_S, int_psi_y ) =   det_PRSxz;
			RESULT ( psi_R, psi_P, int_psi_y ) =
			RESULT ( psi_R, psi_Q, int_psi_y ) =
			RESULT ( psi_R, psi_R, int_psi_y ) =
			RESULT ( psi_R, psi_S, int_psi_y ) =   det_PQSxz;
			RESULT ( psi_S, psi_P, int_psi_y ) =
			RESULT ( psi_S, psi_Q, int_psi_y ) =
			RESULT ( psi_S, psi_R, int_psi_y ) =
			RESULT ( psi_S, psi_S, int_psi_y ) =   det_PQRxz;

			RESULT ( psi_P, psi_P, int_psi_z ) =
			RESULT ( psi_P, psi_Q, int_psi_z ) =
			RESULT ( psi_P, psi_R, int_psi_z ) =
			RESULT ( psi_P, psi_S, int_psi_z ) = - det_PRSxy - det_PQSxy - det_PQRxy;
			RESULT ( psi_Q, psi_P, int_psi_z ) =
			RESULT ( psi_Q, psi_Q, int_psi_z ) =
			RESULT ( psi_Q, psi_R, int_psi_z ) =
			RESULT ( psi_Q, psi_S, int_psi_z ) =   det_PRSxy;
			RESULT ( psi_R, psi_P, int_psi_z ) =
			RESULT ( psi_R, psi_Q, int_psi_z ) =
			RESULT ( psi_R, psi_R, int_psi_z ) =
			RESULT ( psi_R, psi_S, int_psi_z ) =   det_PQSxy;
			RESULT ( psi_S, psi_P, int_psi_z ) =
			RESULT ( psi_S, psi_Q, int_psi_z ) =
			RESULT ( psi_S, psi_R, int_psi_z ) =
			RESULT ( psi_S, psi_S, int_psi_z ) =   det_PQRxy;
			
			// end of case 3 (within 14)

			det /= 6.;

			// second order determinants have been divided by 6 above

			{ // just a block of code for hiding 'spi'
			const double sp1 = det_PRSyz * det_PRSyz / det,
			             sp2 = det_PQSyz * det_PRSyz / det,
			             sp3 = det_PQRyz * det_PRSyz / det,
			             sp5 = det_PQSyz * det_PQSyz / det,
			             sp6 = det_PQRyz * det_PQSyz / det,
			             sp9 = det_PQRyz * det_PQRyz / det;
			// equivalently, we may divide the second-order determinants by square root of det
			// but is it faster ?  is computing one square root faster than performing 36 divisions ?

			RESULT ( psi_P, psi_P, int_psi_x_psi_x ) =
					sp1 + sp2 + sp3 + sp2 + sp5 + sp6 + sp3 + sp6 + sp9;
			RESULT ( psi_P, psi_Q, int_psi_x_psi_x ) = 
			RESULT ( psi_Q, psi_P, int_psi_x_psi_x ) = - sp1 - sp2 - sp3;
			RESULT ( psi_P, psi_R, int_psi_x_psi_x ) = 
			RESULT ( psi_R, psi_P, int_psi_x_psi_x ) = - sp2 - sp5 - sp6;
			RESULT ( psi_P, psi_S, int_psi_x_psi_x ) = 
			RESULT ( psi_S, psi_P, int_psi_x_psi_x ) = - sp3 - sp6 - sp9;
			RESULT ( psi_Q, psi_Q, int_psi_x_psi_x ) =   sp1;
			RESULT ( psi_Q, psi_R, int_psi_x_psi_x ) = 
			RESULT ( psi_R, psi_Q, int_psi_x_psi_x ) =   sp2;
			RESULT ( psi_Q, psi_S, int_psi_x_psi_x ) = 
			RESULT ( psi_S, psi_Q, int_psi_x_psi_x ) =   sp3;
			RESULT ( psi_R, psi_R, int_psi_x_psi_x ) =   sp5;
			RESULT ( psi_R, psi_S, int_psi_x_psi_x ) = 
			RESULT ( psi_S, psi_R, int_psi_x_psi_x ) =   sp6;
			RESULT ( psi_S, psi_S, int_psi_x_psi_x ) =   sp9;
			} // just a block of code

			{ // just a block of code for hiding 'spi'
			const double sp1 = det_PRSxz * det_PRSxz / det,
			             sp2 = det_PQSxz * det_PRSxz / det,
			             sp3 = det_PQRxz * det_PRSxz / det,
			             sp5 = det_PQSxz * det_PQSxz / det,
			             sp6 = det_PQRxz * det_PQSxz / det,
			             sp9 = det_PQRxz * det_PQRxz / det;
			// equivalently, we may divide the second-order determinants by square root of det
			// but is it faster ?  is computing one square root faster than performing 36 divisions ?

			RESULT ( psi_P, psi_P, int_psi_y_psi_y ) =
					sp1 + sp2 + sp3 + sp2 + sp5 + sp6 + sp3 + sp6 + sp9;
			RESULT ( psi_P, psi_Q, int_psi_y_psi_y ) = 
			RESULT ( psi_Q, psi_P, int_psi_y_psi_y ) = - sp1 - sp2 - sp3;
			RESULT ( psi_P, psi_R, int_psi_y_psi_y ) = 
			RESULT ( psi_R, psi_P, int_psi_y_psi_y ) = - sp2 - sp5 - sp6;
			RESULT ( psi_P, psi_S, int_psi_y_psi_y ) = 
			RESULT ( psi_S, psi_P, int_psi_y_psi_y ) = - sp3 - sp6 - sp9;
			RESULT ( psi_Q, psi_Q, int_psi_y_psi_y ) =   sp1;
			RESULT ( psi_Q, psi_R, int_psi_y_psi_y ) = 
			RESULT ( psi_R, psi_Q, int_psi_y_psi_y ) =   sp2;
			RESULT ( psi_Q, psi_S, int_psi_y_psi_y ) = 
			RESULT ( psi_S, psi_Q, int_psi_y_psi_y ) =   sp3;
			RESULT ( psi_R, psi_R, int_psi_y_psi_y ) =   sp5;
			RESULT ( psi_R, psi_S, int_psi_y_psi_y ) = 
			RESULT ( psi_S, psi_R, int_psi_y_psi_y ) =   sp6;
			RESULT ( psi_S, psi_S, int_psi_y_psi_y ) =   sp9;
			} // just a block of code

			{ // just a block of code for hiding 'spi'
			const double sp1 = det_PRSxy * det_PRSxy / det,
			             sp2 = det_PQSxy * det_PRSxy / det,
			             sp3 = det_PQRxy * det_PRSxy / det,
			             sp5 = det_PQSxy * det_PQSxy / det,
			             sp6 = det_PQRxy * det_PQSxy / det,
			             sp9 = det_PQRxy * det_PQRxy / det;
			// equivalently, we may divide the second-order determinants by square root of det
			// but is it faster ?  is computing one square root faster than performing 36 divisions ?

			RESULT ( psi_P, psi_P, int_psi_z_psi_z ) =
					sp1 + sp2 + sp3 + sp2 + sp5 + sp6 + sp3 + sp6 + sp9;
			RESULT ( psi_P, psi_Q, int_psi_z_psi_z ) = 
			RESULT ( psi_Q, psi_P, int_psi_z_psi_z ) = - sp1 - sp2 - sp3;
			RESULT ( psi_P, psi_R, int_psi_z_psi_z ) = 
			RESULT ( psi_R, psi_P, int_psi_z_psi_z ) = - sp2 - sp5 - sp6;
			RESULT ( psi_P, psi_S, int_psi_z_psi_z ) = 
			RESULT ( psi_S, psi_P, int_psi_z_psi_z ) = - sp3 - sp6 - sp9;
			RESULT ( psi_Q, psi_Q, int_psi_z_psi_z ) =   sp1;
			RESULT ( psi_Q, psi_R, int_psi_z_psi_z ) = 
			RESULT ( psi_R, psi_Q, int_psi_z_psi_z ) =   sp2;
			RESULT ( psi_Q, psi_S, int_psi_z_psi_z ) = 
			RESULT ( psi_S, psi_Q, int_psi_z_psi_z ) =   sp3;
			RESULT ( psi_R, psi_R, int_psi_z_psi_z ) =   sp5;
			RESULT ( psi_R, psi_S, int_psi_z_psi_z ) = 
			RESULT ( psi_S, psi_R, int_psi_z_psi_z ) =   sp6;
			RESULT ( psi_S, psi_S, int_psi_z_psi_z ) =   sp9;
			} // just a block of code

			{ // just a block of code for hiding 'spi'
			const double sp1 = det_PRSyz * det_PRSxz / det,
			             sp2 = det_PQSyz * det_PRSxz / det,
			             sp3 = det_PQRyz * det_PRSxz / det,
			             sp4 = det_PRSyz * det_PQSxz / det,
			             sp5 = det_PQSyz * det_PQSxz / det,
			             sp6 = det_PQRyz * det_PQSxz / det,
			             sp7 = det_PRSyz * det_PQRxz / det,
			             sp8 = det_PQSyz * det_PQRxz / det,
			             sp9 = det_PQRyz * det_PQRxz / det;
			// equivalently, we may divide the second-order determinants by square root of det
			// but is it faster ?  is computing one square root faster than performing 36 divisions ?

			RESULT ( psi_P, psi_P, int_psi_x_psi_y ) =
			RESULT ( psi_P, psi_P, int_psi_y_psi_x ) =
					sp1 + sp4 + sp7 + sp2 + sp5 + sp8 + sp3 + sp6 + sp9;
			RESULT ( psi_P, psi_Q, int_psi_x_psi_y ) =
			RESULT ( psi_Q, psi_P, int_psi_y_psi_x ) = - sp1 - sp2 - sp3;
			RESULT ( psi_P, psi_R, int_psi_x_psi_y ) =
			RESULT ( psi_R, psi_P, int_psi_y_psi_x ) = - sp4 - sp5 - sp6;
			RESULT ( psi_P, psi_S, int_psi_x_psi_y ) =
			RESULT ( psi_S, psi_P, int_psi_y_psi_x ) = - sp7 - sp8 - sp9; 
			RESULT ( psi_Q, psi_P, int_psi_x_psi_y ) =
			RESULT ( psi_P, psi_Q, int_psi_y_psi_x ) = - sp1 - sp4 - sp7;
			RESULT ( psi_Q, psi_Q, int_psi_x_psi_y ) =
			RESULT ( psi_Q, psi_Q, int_psi_y_psi_x ) =   sp1;
			RESULT ( psi_Q, psi_R, int_psi_x_psi_y ) =
			RESULT ( psi_R, psi_Q, int_psi_y_psi_x ) =   sp4;
			RESULT ( psi_Q, psi_S, int_psi_x_psi_y ) =
			RESULT ( psi_S, psi_Q, int_psi_y_psi_x ) =   sp7;
			RESULT ( psi_R, psi_P, int_psi_x_psi_y ) =
			RESULT ( psi_P, psi_R, int_psi_y_psi_x ) = - sp2 - sp5 - sp8;
			RESULT ( psi_R, psi_Q, int_psi_x_psi_y ) =
			RESULT ( psi_Q, psi_R, int_psi_y_psi_x ) =   sp2;
			RESULT ( psi_R, psi_R, int_psi_x_psi_y ) =
			RESULT ( psi_R, psi_R, int_psi_y_psi_x ) =   sp5;
			RESULT ( psi_R, psi_S, int_psi_x_psi_y ) =
			RESULT ( psi_S, psi_R, int_psi_y_psi_x ) =   sp8;
			RESULT ( psi_S, psi_P, int_psi_x_psi_y ) =
			RESULT ( psi_P, psi_S, int_psi_y_psi_x ) = - sp3 - sp6 - sp9;
			RESULT ( psi_S, psi_Q, int_psi_x_psi_y ) =
			RESULT ( psi_Q, psi_S, int_psi_y_psi_x ) =   sp3;
			RESULT ( psi_S, psi_R, int_psi_x_psi_y ) =
			RESULT ( psi_R, psi_S, int_psi_y_psi_x ) =   sp6;
			RESULT ( psi_S, psi_S, int_psi_x_psi_y ) =
			RESULT ( psi_S, psi_S, int_psi_y_psi_x ) =   sp9;
			} // just a block of code

			{ // just a block of code for hiding 'spi'
			const double sp1 = det_PRSyz * det_PRSxy / det,
			             sp2 = det_PQSyz * det_PRSxy / det,
			             sp3 = det_PQRyz * det_PRSxy / det,
			             sp4 = det_PRSyz * det_PQSxy / det,
			             sp5 = det_PQSyz * det_PQSxy / det,
			             sp6 = det_PQRyz * det_PQSxy / det,
			             sp7 = det_PRSyz * det_PQRxy / det,
			             sp8 = det_PQSyz * det_PQRxy / det,
			             sp9 = det_PQRyz * det_PQRxy / det;
			// equivalently, we may divide the second-order determinants by square root of det
			// but is it faster ?  is computing one square root faster than performing 36 divisions ?

			RESULT ( psi_P, psi_P, int_psi_x_psi_z ) =
			RESULT ( psi_P, psi_P, int_psi_z_psi_x ) =
					sp1 + sp4 + sp7 + sp2 + sp5 + sp8 + sp3 + sp6 + sp9;
			RESULT ( psi_P, psi_Q, int_psi_x_psi_z ) =
			RESULT ( psi_Q, psi_P, int_psi_z_psi_x ) = - sp1 - sp2 - sp3;
			RESULT ( psi_P, psi_R, int_psi_x_psi_z ) =
			RESULT ( psi_R, psi_P, int_psi_z_psi_x ) = - sp4 - sp5 - sp6;
			RESULT ( psi_P, psi_S, int_psi_x_psi_z ) =
			RESULT ( psi_S, psi_P, int_psi_z_psi_x ) = - sp7 - sp8 - sp9; 
			RESULT ( psi_Q, psi_P, int_psi_x_psi_z ) =
			RESULT ( psi_P, psi_Q, int_psi_z_psi_x ) = - sp1 - sp4 - sp7;
			RESULT ( psi_Q, psi_Q, int_psi_x_psi_z ) =
			RESULT ( psi_Q, psi_Q, int_psi_z_psi_x ) =   sp1;
			RESULT ( psi_Q, psi_R, int_psi_x_psi_z ) =
			RESULT ( psi_R, psi_Q, int_psi_z_psi_x ) =   sp4;
			RESULT ( psi_Q, psi_S, int_psi_x_psi_z ) =
			RESULT ( psi_S, psi_Q, int_psi_z_psi_x ) =   sp7;
			RESULT ( psi_R, psi_P, int_psi_x_psi_z ) =
			RESULT ( psi_P, psi_R, int_psi_z_psi_x ) = - sp2 - sp5 - sp8;
			RESULT ( psi_R, psi_Q, int_psi_x_psi_z ) =
			RESULT ( psi_Q, psi_R, int_psi_z_psi_x ) =   sp2;
			RESULT ( psi_R, psi_R, int_psi_x_psi_z ) =
			RESULT ( psi_R, psi_R, int_psi_z_psi_x ) =   sp5;
			RESULT ( psi_R, psi_S, int_psi_x_psi_z ) =
			RESULT ( psi_S, psi_R, int_psi_z_psi_x ) =   sp8;
			RESULT ( psi_S, psi_P, int_psi_x_psi_z ) =
			RESULT ( psi_P, psi_S, int_psi_z_psi_x ) = - sp3 - sp6 - sp9;
			RESULT ( psi_S, psi_Q, int_psi_x_psi_z ) =
			RESULT ( psi_Q, psi_S, int_psi_z_psi_x ) =   sp3;
			RESULT ( psi_S, psi_R, int_psi_x_psi_z ) =
			RESULT ( psi_R, psi_S, int_psi_z_psi_x ) =   sp6;
			RESULT ( psi_S, psi_S, int_psi_x_psi_z ) =
			RESULT ( psi_S, psi_S, int_psi_z_psi_x ) =   sp9;
			} // just a block of code

			{ // just a block of code for hiding 'spi'
			const double sp1 = det_PRSxz * det_PRSxy / det,
			             sp2 = det_PQSxz * det_PRSxy / det,
			             sp3 = det_PQRxz * det_PRSxy / det,
			             sp4 = det_PRSxz * det_PQSxy / det,
			             sp5 = det_PQSxz * det_PQSxy / det,
			             sp6 = det_PQRxz * det_PQSxy / det,
			             sp7 = det_PRSxz * det_PQRxy / det,
			             sp8 = det_PQSxz * det_PQRxy / det,
			             sp9 = det_PQRxz * det_PQRxy / det;
			// equivalently, we may divide the second-order determinants by square root of det
			// but is it faster ?  is computing one square root faster than performing 36 divisions ?

			RESULT ( psi_P, psi_P, int_psi_y_psi_z ) =
			RESULT ( psi_P, psi_P, int_psi_z_psi_y ) =
					sp1 + sp4 + sp7 + sp2 + sp5 + sp8 + sp3 + sp6 + sp9;
			RESULT ( psi_P, psi_Q, int_psi_y_psi_z ) =
			RESULT ( psi_Q, psi_P, int_psi_z_psi_y ) = - sp1 - sp2 - sp3;
			RESULT ( psi_P, psi_R, int_psi_y_psi_z ) =
			RESULT ( psi_R, psi_P, int_psi_z_psi_y ) = - sp4 - sp5 - sp6;
			RESULT ( psi_P, psi_S, int_psi_y_psi_z ) =
			RESULT ( psi_S, psi_P, int_psi_z_psi_y ) = - sp7 - sp8 - sp9; 
			RESULT ( psi_Q, psi_P, int_psi_y_psi_z ) =
			RESULT ( psi_P, psi_Q, int_psi_z_psi_y ) = - sp1 - sp4 - sp7;
			RESULT ( psi_Q, psi_Q, int_psi_y_psi_z ) =
			RESULT ( psi_Q, psi_Q, int_psi_z_psi_y ) =   sp1;
			RESULT ( psi_Q, psi_R, int_psi_y_psi_z ) =
			RESULT ( psi_R, psi_Q, int_psi_z_psi_y ) =   sp4;
			RESULT ( psi_Q, psi_S, int_psi_y_psi_z ) =
			RESULT ( psi_S, psi_Q, int_psi_z_psi_y ) =   sp7;
			RESULT ( psi_R, psi_P, int_psi_y_psi_z ) =
			RESULT ( psi_P, psi_R, int_psi_z_psi_y ) = - sp2 - sp5 - sp8;
			RESULT ( psi_R, psi_Q, int_psi_y_psi_z ) =
			RESULT ( psi_Q, psi_R, int_psi_z_psi_y ) =   sp2;
			RESULT ( psi_R, psi_R, int_psi_y_psi_z ) =
			RESULT ( psi_R, psi_R, int_psi_z_psi_y ) =   sp5;
			RESULT ( psi_R, psi_S, int_psi_y_psi_z ) =
			RESULT ( psi_S, psi_R, int_psi_z_psi_y ) =   sp8;
			RESULT ( psi_S, psi_P, int_psi_y_psi_z ) =
			RESULT ( psi_P, psi_S, int_psi_z_psi_y ) = - sp3 - sp6 - sp9;
			RESULT ( psi_S, psi_Q, int_psi_y_psi_z ) =
			RESULT ( psi_Q, psi_S, int_psi_z_psi_y ) =   sp3;
			RESULT ( psi_S, psi_R, int_psi_y_psi_z ) =
			RESULT ( psi_R, psi_S, int_psi_z_psi_y ) =   sp6;
			RESULT ( psi_S, psi_S, int_psi_y_psi_z ) =
			RESULT ( psi_S, psi_S, int_psi_z_psi_y ) =   sp9;
			}  // just a block of code

			// end of case 4 (within 14)

		} // end of case 14
				
		break;  // end of case 14 --  Tetrahedron::dock_on_comput
			
		default : assert ( false );
	}  // end of  switch  statement
	
}  // end of  FiniteElement::StandAlone::TypeOne::Tetrahedron::dock_on_comput

//------------------------------------------------------------------------------------------------------//

#undef RESULT

//------------------------------------------------------------------------------------------------------//


void FiniteElement::StandAlone::TypeOne::Triangle::dock_on ( const Cell & cll )
// virtual from FiniteElement::Core, this function is speed-critical

{	assert ( cll .dim() == 2 );
	this->docked_on = cll;
	Mesh::Iterator it = cll .boundary() .iterator ( tag::over_vertices, tag::require_order );
	it .reset();  assert ( it .in_range() );  Cell P = *it;
	it++;  assert ( it .in_range() );  Cell Q = *it;
	it++;  assert ( it .in_range() );  Cell R = *it;
	#ifndef NDEBUG  // DEBUG mode
	it++;  assert ( not it .in_range() );
	#endif

	Function xyz = Manifold::working .coordinates();
	assert ( xyz .number_of ( tag::components ) >= 2 );

	assert ( xyz .number_of ( tag::components ) == 2 );
	Function x = xyz [0], y = xyz [1];

	this->dock_on_comput ( x(P), y(P), x(Q), y(Q), x(R), y(R) );

	// code below can be viewed as a local numbering of vertices P, Q, R
	this->base_fun_1 .clear();
	this->base_fun_1 .insert ( std::pair < Cell::Core*, Function > ( P .core, this->bf1 ) );
	this->base_fun_1 .insert ( std::pair < Cell::Core*, Function > ( Q .core, this->bf2 ) );
	this->base_fun_1 .insert ( std::pair < Cell::Core*, Function > ( R .core, this->bf3 ) );
	// using Cell::Core::short_int_heap for local numbering of vertices
	// should be slightly faster

}  // end of  FiniteElement::StandAlone::TypeOne::Triangle::dock_on

//------------------------------------------------------------------------------------------------------//


#ifndef MANIFEM_NO_QUOTIENT

void FiniteElement::StandAlone::TypeOne::Triangle::dock_on  // virtual from FiniteElement::Core
( const Cell & cll, const tag::Winding & )                  // this function is speed-critical

{	assert ( cll .dim() == 2 );
	this->docked_on = cll;
	// perhaps implement a special iterator returning points and segments
	Mesh::Iterator it = cll .boundary() .iterator ( tag::over_vertices, tag::require_order );
	it .reset();  assert ( it .in_range() );  Cell P = *it;
	Cell PQ = cll .boundary() .cell_in_front_of ( P );
	it++;  assert ( it .in_range() );  Cell Q = *it;
	Cell QR = cll .boundary() .cell_in_front_of ( Q );
	it++;  assert ( it .in_range() );  Cell R = *it;
	#ifndef NDEBUG  // DEBUG mode
	Cell RP = cll .boundary() .cell_in_front_of ( R );
	it++;  assert ( not it .in_range() );
	#endif

	Function::Action winding_Q = PQ .winding(), winding_R = winding_Q + QR .winding();
	assert ( winding_R + RP .winding() == 0 );

	Function xyz = Manifold::working .coordinates();
	assert ( xyz .number_of ( tag::components ) >= 2 );

	assert ( xyz .number_of ( tag::components ) == 2 );

	const std::vector < double > xyz_P = xyz ( P ),
	                             xyz_Q = xyz ( Q, tag::winding, winding_Q ),
	                             xyz_R = xyz ( R, tag::winding, winding_R );
	
	this->dock_on_comput
	( xyz_P [0], xyz_P [1], xyz_Q [0], xyz_Q [1], xyz_R [0], xyz_R [1] );

	// code below can be viewed as a local numbering of vertices P, Q, R
	this->base_fun_1 .clear();
	this->base_fun_1 .insert ( std::pair < Cell::Core*, Function > ( P .core, this->bf1 ) );
	this->base_fun_1 .insert ( std::pair < Cell::Core*, Function > ( Q .core, this->bf2 ) );
	this->base_fun_1 .insert ( std::pair < Cell::Core*, Function > ( R .core, this->bf3 ) );
	// using Cell::Core::short_int_heap for local numbering of vertices
	// should be slightly faster

}  // end of  FiniteElement::StandAlone::TypeOne::Triangle::dock_on  with tag::winding

#endif  // ifndef MANIFEM_NO_QUOTIENT

//------------------------------------------------------------------------------------------------------//


void FiniteElement::StandAlone::TypeOne::Quadrangle::dock_on ( const Cell & cll )
// virtual from FiniteElement::Core
// overridden by FiniteElement::StandAlone::TypeOne::Rectangle
// this function is speed-critical

{	assert ( cll .dim() == 2 );
	this->docked_on = cll;
	Mesh::Iterator it = cll .boundary() .iterator ( tag::over_vertices, tag::require_order );
	it .reset();  assert ( it .in_range() );  Cell P = *it;
	it++;  assert ( it .in_range() );  Cell Q = *it;
	it++;  assert ( it .in_range() );  Cell R = *it;
	it++;  assert ( it .in_range() );  Cell S = *it;
	#ifndef NDEBUG  // DEBUG mode
	it++;  assert ( not it .in_range() );
	#endif

	Function xyz = Manifold::working .coordinates();
	assert ( xyz .number_of ( tag::components ) >= 2 );

	assert ( xyz .number_of ( tag::components ) == 2 );
	Function x = xyz [0], y = xyz [1];

	this->dock_on_comput ( x(P), y(P), x(Q), y(Q), x(R), y(R), x(S), y(S) );

	// code below can be viewed as a local numbering of vertices P, Q, R, S
	this->base_fun_1 .clear();
	this->base_fun_1 .insert ( std::pair < Cell::Core*, Function > ( P .core, this->bf1 ) );
	this->base_fun_1 .insert ( std::pair < Cell::Core*, Function > ( Q .core, this->bf2 ) );
	this->base_fun_1 .insert ( std::pair < Cell::Core*, Function > ( R .core, this->bf3 ) );
	this->base_fun_1 .insert ( std::pair < Cell::Core*, Function > ( S .core, this->bf4 ) );
	// using Cell::Core::short_int_heap for local numbering of vertices
	// should be slightly faster

}  // end of  FiniteElement::StandAlone::TypeOne::Quadrangle::dock_on

//------------------------------------------------------------------------------------------------------//


#ifndef MANIFEM_NO_QUOTIENT

void FiniteElement::StandAlone::TypeOne::Quadrangle::dock_on  
( const Cell & cll, const tag::Winding & )  // virtual from FiniteElement::Core
// overridden by FiniteElement::StandAlone::TypeOne::Rectangle
// this function is speed-critical

{	assert ( cll .dim() == 2 );
	this->docked_on = cll;
	// perhaps implement a special iterator returning points and segments
	Mesh::Iterator it = cll .boundary() .iterator ( tag::over_vertices, tag::require_order );
	it .reset();  assert ( it .in_range() );  Cell P = *it;
	Cell PQ = cll .boundary() .cell_in_front_of ( P );
	it++;  assert ( it .in_range() );  Cell Q = *it;
	Cell QR = cll .boundary() .cell_in_front_of ( Q );
	it++;  assert ( it .in_range() );  Cell R = *it;
	Cell RS = cll .boundary() .cell_in_front_of ( R );
	it++;  assert ( it .in_range() );  Cell S = *it;
	#ifndef NDEBUG  // DEBUG mode
	Cell SP = cll .boundary() .cell_in_front_of ( S );
	it++;  assert ( not it .in_range() );
	#endif

	Function::Action winding_Q = PQ .winding(),
	                 winding_R = winding_Q + QR .winding(),
	                 winding_S = winding_R + RS .winding();
	assert ( winding_S + SP .winding() == 0 );

	Function xyz = Manifold::working .coordinates();
	assert ( xyz .number_of ( tag::components ) >= 2 );

	assert ( xyz .number_of ( tag::components ) == 2 );

	const std::vector < double > xyz_P = xyz ( P ),
	                             xyz_Q = xyz ( Q, tag::winding, winding_Q ),
	                             xyz_R = xyz ( R, tag::winding, winding_R ),
	                             xyz_S = xyz ( S, tag::winding, winding_S );
	
	this->dock_on_comput
	( xyz_P [0], xyz_P [1], xyz_Q [0], xyz_Q [1], xyz_R [0], xyz_R [1], xyz_S [0], xyz_S [1] );

	// code below can be viewed as a local numbering of vertices P, Q, R, S
	this->base_fun_1 .clear();
	this->base_fun_1 .insert ( std::pair < Cell::Core*, Function > ( P .core, this->bf1 ) );
	this->base_fun_1 .insert ( std::pair < Cell::Core*, Function > ( Q .core, this->bf2 ) );
	this->base_fun_1 .insert ( std::pair < Cell::Core*, Function > ( R .core, this->bf3 ) );
	this->base_fun_1 .insert ( std::pair < Cell::Core*, Function > ( S .core, this->bf4 ) );
	// using Cell::Core::short_int_heap for local numbering of vertices
	// should be slightly faster

}  // end of  FiniteElement::StandAlone::TypeOne::Quadrangle::dock_on  with tag::winding

#endif  // ifndef MANIFEM_NO_QUOTIENT

//------------------------------------------------------------------------------------------------------//


void FiniteElement::StandAlone::TypeOne::Parallelogram::dock_on ( const Cell & cll )
// virtual from FiniteElement::Core, this function is speed-critical
// defined by FiniteElement::StandAlone::TypeOne::Rectangle, here overridden

{	assert ( cll .dim() == 2 );
	this->docked_on = cll;
	Mesh::Iterator it = cll .boundary() .iterator ( tag::over_vertices, tag::require_order );
	it .reset();  assert ( it .in_range() );  Cell P = *it;
	it++;  assert ( it .in_range() );  Cell Q = *it;
	it++;  assert ( it .in_range() );  Cell R = *it;
	it++;  assert ( it .in_range() );  Cell S = *it;
	#ifndef NDEBUG  // DEBUG mode
	it++;  assert ( not it .in_range() );
	#endif

	Function xyz = Manifold::working .coordinates();
	assert ( xyz .number_of ( tag::components ) >= 2 );

	assert ( xyz .number_of ( tag::components ) == 2 );
	Function x = xyz [0], y = xyz [1];

	assert ( false );  // below, replace quadrangle by parallelogram

	// dock_on_hand_quadrangle_Q1 ( x(P), y(P), x(Q), y(Q), x(R), y(R), x(S), y(S),
	//                              this->cas, this->result_of_integr              );

	// code below can be viewed as a local numbering of vertices P, Q, R, S
	this->base_fun_1 .clear();
	this->base_fun_1 .insert ( std::pair < Cell::Core*, Function > ( P .core, this->bf1 ) );
	this->base_fun_1 .insert ( std::pair < Cell::Core*, Function > ( Q .core, this->bf2 ) );
	this->base_fun_1 .insert ( std::pair < Cell::Core*, Function > ( R .core, this->bf3 ) );
	this->base_fun_1 .insert ( std::pair < Cell::Core*, Function > ( S .core, this->bf4 ) );
	// using Cell::Core::short_int_heap for local numbering of vertices
	// should be slightly faster

}  // end of  FiniteElement::StandAlone::TypeOne::Parallelogram::dock_on

//------------------------------------------------------------------------------------------------------//

#ifndef MANIFEM_NO_QUOTIENT

void FiniteElement::StandAlone::TypeOne::Parallelogram::dock_on  
( const Cell & cll, const tag::Winding & )  // virtual from FiniteElement::Core

// virtual from FiniteElement::Core
// defined by FiniteElement::StandAlone::TypeOne::Quadrangle, here overridden
// overridden again by FiniteElement::StandAlone::TypeOne::{Rectangle,Square}
// this function is speed-critical

{	assert ( cll .dim() == 2 );
	this->docked_on = cll;
	// perhaps implement a special iterator returning points and segments
	Mesh::Iterator it = cll .boundary() .iterator ( tag::over_vertices, tag::require_order );
	it .reset();  assert ( it .in_range() );  Cell P = *it;
	Cell PQ = cll .boundary() .cell_in_front_of ( P );
	it++;  assert ( it .in_range() );  Cell Q = *it;
	Cell QR = cll .boundary() .cell_in_front_of ( Q );
	it++;  assert ( it .in_range() );  Cell R = *it;
	Cell RS = cll .boundary() .cell_in_front_of ( R );
	it++;  assert ( it .in_range() );  Cell S = *it;
	#ifndef NDEBUG  // DEBUG mode
	Cell SP = cll .boundary() .cell_in_front_of ( S );
	it++;  assert ( not it .in_range() );
	#endif

	Function::Action winding_Q = PQ .winding(),
	                 winding_R = winding_Q + QR .winding(),
	                 winding_S = winding_R + RS .winding();
	assert ( winding_S + SP .winding() == 0 );

	Function xyz = Manifold::working .coordinates();
	assert ( xyz .number_of ( tag::components ) >= 2 );

	assert ( xyz .number_of ( tag::components ) == 2 );

	const std::vector < double > xyz_P = xyz ( P ),
	                             xyz_Q = xyz ( Q, tag::winding, winding_Q ),
	                             xyz_R = xyz ( R, tag::winding, winding_R ),
	                             xyz_S = xyz ( S, tag::winding, winding_S );
	
	assert ( false );  // below, replace quadrangle by parallelogram

	// dock_on_hand_quadrangle_Q1
	// ( xyz_P [0], xyz_P [1], xyz_Q [0], xyz_Q [1], xyz_R [0], xyz_R [1], xyz_S [0], xyz_S [1],
	//   this->cas, this->result_of_integr                                                      );

	// code below can be viewed as a local numbering of vertices P, Q, R, S
	this->base_fun_1 .clear();
	this->base_fun_1 .insert ( std::pair < Cell::Core*, Function > ( P .core, this->bf1 ) );
	this->base_fun_1 .insert ( std::pair < Cell::Core*, Function > ( Q .core, this->bf2 ) );
	this->base_fun_1 .insert ( std::pair < Cell::Core*, Function > ( R .core, this->bf3 ) );
	this->base_fun_1 .insert ( std::pair < Cell::Core*, Function > ( S .core, this->bf4 ) );
	// using Cell::Core::short_int_heap for local numbering of vertices
	// should be slightly faster

}  // end of  FiniteElement::StandAlone::TypeOne::Parallelogram::dock_on  with tag::winding

#endif  // ifndef MANIFEM_NO_QUOTIENT

//------------------------------------------------------------------------------------------------------//


void FiniteElement::StandAlone::TypeOne::Rectangle::dock_on ( const Cell & cll )

// virtual from FiniteElement::Core
// defined by FiniteElement::StandAlone::TypeOne::Parallelogram, here overridden
// overridden again by FiniteElement::StandAlone::TypeOne::Square
// this function is speed-critical

{	assert ( cll .dim() == 2 );
	this->docked_on = cll;
	Mesh::Iterator it = cll .boundary() .iterator ( tag::over_vertices, tag::require_order );
	it .reset();  assert ( it .in_range() );  Cell P = *it;
	it++;  assert ( it .in_range() );  Cell Q = *it;
	it++;  assert ( it .in_range() );  Cell R = *it;
	it++;  assert ( it .in_range() );  Cell S = *it;
	#ifndef NDEBUG  // DEBUG mode
	it++;  assert ( not it .in_range() );
	#endif

	Function xyz = Manifold::working .coordinates();
	assert ( xyz .number_of ( tag::components ) >= 2 );

	assert ( xyz .number_of ( tag::components ) == 2 );
	Function x = xyz [0], y = xyz [1];

	const double xP = x(P), yP = y(P), xQ = x(Q), yQ = y(Q),
	             xPmxQ = xP - xQ, yPmyQ = yP - yQ;
	this->base_fun_1 .clear();
	if ( std::abs ( xPmxQ  ) < std::abs ( yPmyQ ) )
		// P and Q are on the same vertical -- xP == xQ
		if ( yPmyQ > 0 )  // PQ is the left side of the rectangle
		{	this->dock_on_comput ( xQ, yQ, x(R), y(R), x(S), y(S), xP, yP );
			this->base_fun_1 .insert
				( std::pair < Cell::Core*, Function > ( Q .core, this->bf1 ) );
			this->base_fun_1 .insert ( std::pair < Cell::Core*, Function > ( R .core, this->bf2 ) );
			this->base_fun_1 .insert ( std::pair < Cell::Core*, Function > ( S .core, this->bf3 ) );
			this->base_fun_1 .insert ( std::pair < Cell::Core*, Function > ( P .core, this->bf4 ) );  }
		else  // PQ is the right side of the rectangle
		{	this->dock_on_comput ( x(S), y(S), xP, yP, xQ, yQ, x(R), y(R) );
			this->base_fun_1 .insert ( std::pair < Cell::Core*, Function > ( S .core, this->bf1 ) );
			this->base_fun_1 .insert ( std::pair < Cell::Core*, Function > ( P .core, this->bf2 ) );
			this->base_fun_1 .insert ( std::pair < Cell::Core*, Function > ( Q .core, this->bf3 ) );
			this->base_fun_1 .insert ( std::pair < Cell::Core*, Function > ( R .core, this->bf4 ) );  }
	else  // P and Q are on the same horizontal -- yP == yQ
		if ( xPmxQ > 0 )  // PQ is the upper side of the rectangle
		{	this->dock_on_comput ( x(R), y(R), x(S), y(S), xP, yP, xQ, yQ );
			this->base_fun_1 .insert ( std::pair < Cell::Core*, Function > ( R .core, this->bf1 ) );
			this->base_fun_1 .insert ( std::pair < Cell::Core*, Function > ( S .core, this->bf2 ) );
			this->base_fun_1 .insert ( std::pair < Cell::Core*, Function > ( P .core, this->bf3 ) );
			this->base_fun_1 .insert ( std::pair < Cell::Core*, Function > ( Q .core, this->bf4 ) );  }
		else  // PQ is the lower side of the rectangle
		{	this->dock_on_comput ( xP, yP, xQ, yQ, x(R), y(R), x(S), y(S) );
			this->base_fun_1 .insert ( std::pair < Cell::Core*, Function > ( P .core, this->bf1 ) );
			this->base_fun_1 .insert ( std::pair < Cell::Core*, Function > ( Q .core, this->bf2 ) );
			this->base_fun_1 .insert ( std::pair < Cell::Core*, Function > ( R .core, this->bf3 ) );
			this->base_fun_1 .insert ( std::pair < Cell::Core*, Function > ( S .core, this->bf4 ) );  }
	
}  // end of  FiniteElement::StandAlone::TypeOne::Rectangle::dock_on

//------------------------------------------------------------------------------------------------------//


#ifndef MANIFEM_NO_QUOTIENT

void FiniteElement::StandAlone::TypeOne::Rectangle::dock_on  
( const Cell & cll, const tag::Winding & )  // virtual from FiniteElement::Core
// this function is speed-critical
// defined by FiniteElement::StandAlone::TypeOne::Quadrangle, here overridden
// overridden again by FiniteElement::StandAlone::TypeOne::Square

{	assert ( cll .dim() == 2 );
	this->docked_on = cll;
	// perhaps implement a special iterator returning points and segments
	Mesh::Iterator it = cll .boundary() .iterator ( tag::over_vertices, tag::require_order );
	it .reset();  assert ( it .in_range() );  Cell P = *it;
	Cell PQ = cll .boundary() .cell_in_front_of ( P );
	it++;  assert ( it .in_range() );  Cell Q = *it;
	Cell QR = cll .boundary() .cell_in_front_of ( Q );
	it++;  assert ( it .in_range() );  Cell R = *it;
	Cell RS = cll .boundary() .cell_in_front_of ( R );
	it++;  assert ( it .in_range() );  Cell S = *it;
	#ifndef NDEBUG  // DEBUG mode
	Cell SP = cll .boundary() .cell_in_front_of ( S );
	it++;  assert ( not it .in_range() );
	#endif

	Function::Action winding_Q = PQ .winding(),
	                 winding_R = winding_Q + QR .winding(),
	                 winding_S = winding_R + RS .winding();
	assert ( winding_S + SP .winding() == 0 );

	Function xyz = Manifold::working .coordinates();
	assert ( xyz .number_of ( tag::components ) >= 2 );

	assert ( xyz .number_of ( tag::components ) == 2 );

	const std::vector < double > xyz_P = xyz ( P ),
	                             xyz_Q = xyz ( Q, tag::winding, winding_Q ),
	                             xyz_R = xyz ( R, tag::winding, winding_R ),
	                             xyz_S = xyz ( S, tag::winding, winding_S );

	// this works on quotient manifolds defined by translations
	// does not make sense on quotient with rotations
	
	const double xP = xyz_P [0], yP = xyz_P [1], xQ = xyz_Q [0], yQ = xyz_Q [1],
	             xPmxQ = xP - xQ, yPmyQ = yP - yQ;
	this->base_fun_1 .clear();
	if ( std::abs ( xPmxQ  ) < std::abs ( yPmyQ ) )
		// P and Q are on the same vertical -- xP == xQ
		if ( yPmyQ > 0 )  // PQ is the left side of the rectangle
		{	this->dock_on_comput
			( xQ, yQ, xyz_R [0], xyz_R [1], xyz_S [0], xyz_S [1], xP, yP );
			this->base_fun_1 .insert ( std::pair < Cell::Core*, Function > ( Q .core, this->bf1 ) );
			this->base_fun_1 .insert ( std::pair < Cell::Core*, Function > ( R .core, this->bf2 ) );
			this->base_fun_1 .insert ( std::pair < Cell::Core*, Function > ( S .core, this->bf3 ) );
			this->base_fun_1 .insert ( std::pair < Cell::Core*, Function > ( P .core, this->bf4 ) );  }
		else  // PQ is the right side of the rectangle
		{	this->dock_on_comput
			( xyz_S [0], xyz_S [1], xP, yP, xQ, yQ, xyz_R [0], xyz_R [1] );
			this->base_fun_1 .insert ( std::pair < Cell::Core*, Function > ( S .core, this->bf1 ) );
			this->base_fun_1 .insert ( std::pair < Cell::Core*, Function > ( P .core, this->bf2 ) );
			this->base_fun_1 .insert ( std::pair < Cell::Core*, Function > ( Q .core, this->bf3 ) );
			this->base_fun_1 .insert ( std::pair < Cell::Core*, Function > ( R .core, this->bf4 ) );  }
	else  // P and Q are on the same horizontal -- yP == yQ
		if ( xPmxQ > 0 )  // PQ is the upper side of the rectangle
		{	this->dock_on_comput
			( xyz_R [0], xyz_R [1], xyz_S [0], xyz_S [1], xP, yP, xQ, yQ );
			this->base_fun_1 .insert ( std::pair < Cell::Core*, Function > ( R .core, this->bf1 ) );
			this->base_fun_1 .insert ( std::pair < Cell::Core*, Function > ( S .core, this->bf2 ) );
			this->base_fun_1 .insert ( std::pair < Cell::Core*, Function > ( P .core, this->bf3 ) );
			this->base_fun_1 .insert ( std::pair < Cell::Core*, Function > ( Q .core, this->bf4 ) );  }
		else  // PQ is the lower side of the rectangle
		{	this->dock_on_comput
			( xP, yP, xQ, yQ, xyz_R [0], xyz_R [1], xyz_S [0], xyz_S [1] );
			this->base_fun_1 .insert ( std::pair < Cell::Core*, Function > ( P .core, this->bf1 ) );
			this->base_fun_1 .insert ( std::pair < Cell::Core*, Function > ( Q .core, this->bf2 ) );
			this->base_fun_1 .insert ( std::pair < Cell::Core*, Function > ( R .core, this->bf3 ) );
			this->base_fun_1 .insert ( std::pair < Cell::Core*, Function > ( S .core, this->bf4 ) );  }

}  // end of  FiniteElement::StandAlone::TypeOne::Rectangle::dock_on  with tag::winding

#endif  // ifndef MANIFEM_NO_QUOTIENT

//------------------------------------------------------------------------------------------------------//


void FiniteElement::StandAlone::TypeOne::Rectangle::dock_on
( const Cell & cll, const tag::FirstVertex &, const Cell & P )
// virtual from FiniteElement::Core, defined there (execution forbidden), here overridden
// this function is speed-critical

{	assert ( cll .dim() == 2 );
	this->docked_on = cll;

	Cell PQ = cll. boundary() .cell_in_front_of ( P, tag::surely_exists );
	Cell Q = PQ .tip();
	Cell QR = cll. boundary() .cell_in_front_of ( Q, tag::surely_exists );
	Cell R = QR .tip();
	Cell RS = cll. boundary() .cell_in_front_of ( R, tag::surely_exists );
	Cell S = RS .tip();
	#ifndef NDEBUG  // DEBUG mode
	Cell SP = cll .boundary() .cell_in_front_of ( S, tag::surely_exists );
	assert ( SP .tip() == P );
	#endif

	Function xyz = Manifold::working .coordinates();
	assert ( xyz .number_of ( tag::components ) >= 2 );

	assert ( xyz .number_of ( tag::components ) == 2 );
	Function x = xyz [0], y = xyz [1];

	this->dock_on_comput ( x(P), y(P), x(Q), y(Q), x(R), y(R), x(S), y(S) );

	// code below can be viewed as a local numbering of vertices P, Q, R
	this->base_fun_1 .clear();
	this->base_fun_1 .insert
		( std::pair < Cell::Core*, Function > ( P .core, this->bf1 ) );
	this->base_fun_1 .insert
		( std::pair < Cell::Core*, Function > ( Q .core, this->bf2 ) );
	this->base_fun_1 .insert
		( std::pair < Cell::Core*, Function > ( R .core, this->bf3 ) );
	this->base_fun_1 .insert
		( std::pair < Cell::Core*, Function > ( S .core, this->bf4 ) );
	// using Cell::Core::short_int_heap for local numbering of vertices
	// should be slightly faster

}  // end of  FiniteElement::StandAlone::TypeOne::Rectangle::dock_on with tag::first_vertex

//------------------------------------------------------------------------------------------------------//


#ifndef MANIFEM_NO_QUOTIENT

void FiniteElement::StandAlone::TypeOne::Rectangle::dock_on  
( const Cell & cll, const tag::FirstVertex &, const Cell & P, const tag::Winding & )
// virtual from FiniteElement::Core, defined there (execution forbidden), here overridden
// this function is speed-critical

{	assert ( cll .dim() == 2 );
	this->docked_on = cll;

	Cell PQ = cll. boundary() .cell_in_front_of ( P, tag::surely_exists );
	Cell Q = PQ .tip();
	Cell QR = cll. boundary() .cell_in_front_of ( Q, tag::surely_exists );
	Cell R = QR .tip();
	Cell RS = cll. boundary() .cell_in_front_of ( R, tag::surely_exists );
	Cell S = RS .tip();
	#ifndef NDEBUG  // DEBUG mode
	Cell SP = cll .boundary() .cell_in_front_of ( S, tag::surely_exists );
	assert ( SP .tip() == P );
	#endif
	
	Function::Action winding_Q = PQ .winding(),
	                 winding_R = winding_Q + QR .winding(),
	                 winding_S = winding_R + RS .winding();
	assert ( winding_S + SP .winding() == 0 );

	Function xyz = Manifold::working .coordinates();
	assert ( xyz .number_of ( tag::components ) >= 2 );

	assert ( xyz .number_of ( tag::components ) == 2 );

	const std::vector < double > xyz_P = xyz ( P ),
	                             xyz_Q = xyz ( Q, tag::winding, winding_Q ),
	                             xyz_R = xyz ( R, tag::winding, winding_R ),
	                             xyz_S = xyz ( S, tag::winding, winding_S );
	
	this->dock_on_comput
	( xyz_P [0], xyz_P [1], xyz_Q [0], xyz_Q [1], xyz_R [0], xyz_R [1], xyz_S [0], xyz_S [1] );

	// code below can be viewed as a local numbering of vertices P, Q, R
	this->base_fun_1 .clear();
	this->base_fun_1 .insert ( std::pair < Cell::Core*, Function > ( P .core, this->bf1 ) );
	this->base_fun_1 .insert ( std::pair < Cell::Core*, Function > ( Q .core, this->bf2 ) );
	this->base_fun_1 .insert ( std::pair < Cell::Core*, Function > ( R .core, this->bf3 ) );
	this->base_fun_1 .insert ( std::pair < Cell::Core*, Function > ( S .core, this->bf4 ) );
	// using Cell::Core::short_int_heap for local numbering of vertices
	// should be slightly faster

}  // end of  FiniteElement::StandAlone::TypeOne::Rectangle::dock_on
   //         with tag::winding and tag::first_vertex

#endif  // ifndef MANIFEM_NO_QUOTIENT

//------------------------------------------------------------------------------------------------------//


void FiniteElement::StandAlone::TypeOne::Square::dock_on ( const Cell & cll )
// virtual from FiniteElement::Core, this function is speed-critical
// defined by FiniteElement::StandAlone::TypeOne::Rectangle, here overridden

{	assert ( cll .dim() == 2 );
	this->docked_on = cll;
	Mesh::Iterator it = cll .boundary() .iterator ( tag::over_vertices, tag::require_order );
	it .reset();  assert ( it .in_range() );  Cell P = *it;
	it++;  assert ( it .in_range() );  Cell Q = *it;
	it++;  assert ( it .in_range() );  Cell R = *it;
	it++;  assert ( it .in_range() );  Cell S = *it;
	#ifndef NDEBUG  // DEBUG mode
	it++;  assert ( not it .in_range() );
	#endif

	Function xyz = Manifold::working .coordinates();
	assert ( xyz .number_of ( tag::components ) >= 2 );

	assert ( xyz .number_of ( tag::components ) == 2 );
	Function x = xyz [0], y = xyz [1];

	const double xP = x(P), yP = y(P), xQ = x(Q), yQ = y(Q),
	             xPmxQ = xP - xQ, yPmyQ = yP - yQ;
	this->base_fun_1 .clear();
	if ( std::abs ( xPmxQ  ) < std::abs ( yPmyQ ) )
		// P and Q are on the same vertical -- xP == xQ
		if ( yPmyQ > 0 )  // PQ is the left side of the square
		{	this->dock_on_comput ( xQ, yQ, x(R), y(R), x(S), y(S), xP, yP );
			this->base_fun_1 .insert ( std::pair < Cell::Core*, Function > ( Q .core, this->bf1 ) );
			this->base_fun_1 .insert ( std::pair < Cell::Core*, Function > ( R .core, this->bf2 ) );
			this->base_fun_1 .insert ( std::pair < Cell::Core*, Function > ( S .core, this->bf3 ) );
			this->base_fun_1 .insert ( std::pair < Cell::Core*, Function > ( P .core, this->bf4 ) );  }
		else  // PQ is the right side of the square
		{	this->dock_on_comput ( x(S), y(S), xP, yP, xQ, yQ, x(R), y(R) );
			this->base_fun_1 .insert ( std::pair < Cell::Core*, Function > ( S .core, this->bf1 ) );
			this->base_fun_1 .insert ( std::pair < Cell::Core*, Function > ( P .core, this->bf2 ) );
			this->base_fun_1 .insert ( std::pair < Cell::Core*, Function > ( Q .core, this->bf3 ) );
			this->base_fun_1 .insert ( std::pair < Cell::Core*, Function > ( R .core, this->bf4 ) );  }
	else  // P and Q are on the same horizontal -- yP == yQ
		if ( xPmxQ > 0 )  // PQ is the upper side of the square
		{	this->dock_on_comput ( x(R), y(R), x(S), y(S), xP, yP, xQ, yQ );
			this->base_fun_1 .insert ( std::pair < Cell::Core*, Function > ( R .core, this->bf1 ) );
			this->base_fun_1 .insert ( std::pair < Cell::Core*, Function > ( S .core, this->bf2 ) );
			this->base_fun_1 .insert ( std::pair < Cell::Core*, Function > ( P .core, this->bf3 ) );
			this->base_fun_1 .insert ( std::pair < Cell::Core*, Function > ( Q .core, this->bf4 ) );  }
		else  // PQ is the lower side of the square
		{	this->dock_on_comput ( xP, yP, xQ, yQ, x(R), y(R), x(S), y(S) );
			this->base_fun_1 .insert ( std::pair < Cell::Core*, Function > ( P .core, this->bf1 ) );
			this->base_fun_1 .insert ( std::pair < Cell::Core*, Function > ( Q .core, this->bf2 ) );
			this->base_fun_1 .insert ( std::pair < Cell::Core*, Function > ( R .core, this->bf3 ) );
			this->base_fun_1 .insert ( std::pair < Cell::Core*, Function > ( S .core, this->bf4 ) );  }
	
}  // end of  FiniteElement::StandAlone::TypeOne::Square::dock_on

//------------------------------------------------------------------------------------------------------//


#ifndef MANIFEM_NO_QUOTIENT

void FiniteElement::StandAlone::TypeOne::Square::dock_on  
( const Cell & cll, const tag::Winding & )  // virtual from FiniteElement::Core

// defined by FiniteElement::StandAlone::TypeOne::Rectangle, here overridden
// this function is speed-critical

{	assert ( cll .dim() == 2 );
	this->docked_on = cll;
	// perhaps implement a special iterator returning points and segments
	Mesh::Iterator it = cll .boundary() .iterator ( tag::over_vertices, tag::require_order );
	it .reset();  assert ( it .in_range() );  Cell P = *it;
	Cell PQ = cll .boundary() .cell_in_front_of ( P );
	it++;  assert ( it .in_range() );  Cell Q = *it;
	Cell QR = cll .boundary() .cell_in_front_of ( Q );
	it++;  assert ( it .in_range() );  Cell R = *it;
	Cell RS = cll .boundary() .cell_in_front_of ( R );
	it++;  assert ( it .in_range() );  Cell S = *it;
	#ifndef NDEBUG  // DEBUG mode
	Cell SP = cll .boundary() .cell_in_front_of ( S );
	it++;  assert ( not it .in_range() );
	#endif

	Function::Action winding_Q = PQ .winding(),
	                 winding_R = winding_Q + QR .winding(),
	                 winding_S = winding_R + RS .winding();
	assert ( winding_S + SP .winding() == 0 );

	Function xyz = Manifold::working .coordinates();
	assert ( xyz .number_of ( tag::components ) >= 2 );

	assert ( xyz .number_of ( tag::components ) == 2 );

	const std::vector < double > xyz_P = xyz ( P ),
	                             xyz_Q = xyz ( Q, tag::winding, winding_Q ),
	                             xyz_R = xyz ( R, tag::winding, winding_R ),
	                             xyz_S = xyz ( S, tag::winding, winding_S );
	
	// this works on quotient manifolds defined by translations
	// does not make sense on quotient with rotations
	
	const double xP = xyz_P [0], yP = xyz_P [1], xQ = xyz_Q [0], yQ = xyz_Q [1],
	             xPmxQ = xP - xQ, yPmyQ = yP - yQ;
	this->base_fun_1 .clear();
	if ( std::abs ( xPmxQ  ) < std::abs ( yPmyQ ) )
		// P and Q are on the same vertical -- xP == xQ
		if ( yPmyQ > 0 )  // PQ is the left side of the square
		{	this->dock_on_comput
			( xQ, yQ, xyz_R [0], xyz_R [1], xyz_S [0], xyz_S [1], xP, yP );
			this->base_fun_1 .insert ( std::pair < Cell::Core*, Function > ( Q .core, this->bf1 ) );
			this->base_fun_1 .insert ( std::pair < Cell::Core*, Function > ( R .core, this->bf2 ) );
			this->base_fun_1 .insert ( std::pair < Cell::Core*, Function > ( S .core, this->bf3 ) );
			this->base_fun_1 .insert ( std::pair < Cell::Core*, Function > ( P .core, this->bf4 ) );  }
		else  // PQ is the right side of the square
		{	this->dock_on_comput
			( xyz_S [0], xyz_S [1], xP, yP, xQ, yQ, xyz_R [0], xyz_R [1] );
			this->base_fun_1 .insert ( std::pair < Cell::Core*, Function > ( S .core, this->bf1 ) );
			this->base_fun_1 .insert ( std::pair < Cell::Core*, Function > ( P .core, this->bf2 ) );
			this->base_fun_1 .insert ( std::pair < Cell::Core*, Function > ( Q .core, this->bf3 ) );
			this->base_fun_1 .insert ( std::pair < Cell::Core*, Function > ( R .core, this->bf4 ) );  }
	else  // P and Q are on the same horizontal -- yP == yQ
		if ( xPmxQ > 0 )  // PQ is the upper side of the square
		{	this->dock_on_comput
			( xyz_R [0], xyz_R [1], xyz_S [0], xyz_S [1], xP, yP, xQ, yQ );
			this->base_fun_1 .insert ( std::pair < Cell::Core*, Function > ( R .core, this->bf1 ) );
			this->base_fun_1 .insert ( std::pair < Cell::Core*, Function > ( S .core, this->bf2 ) );
			this->base_fun_1 .insert ( std::pair < Cell::Core*, Function > ( P .core, this->bf3 ) );
			this->base_fun_1 .insert ( std::pair < Cell::Core*, Function > ( Q .core, this->bf4 ) );  }
		else  // PQ is the lower side of the square
		{	this->dock_on_comput
			( xP, yP, xQ, yQ, xyz_R [0], xyz_R [1], xyz_S [0], xyz_S [1] );
			this->base_fun_1 .insert ( std::pair < Cell::Core*, Function > ( P .core, this->bf1 ) );
			this->base_fun_1 .insert ( std::pair < Cell::Core*, Function > ( Q .core, this->bf2 ) );
			this->base_fun_1 .insert ( std::pair < Cell::Core*, Function > ( R .core, this->bf3 ) );
			this->base_fun_1 .insert ( std::pair < Cell::Core*, Function > ( S .core, this->bf4 ) );  }

}  // end of  FiniteElement::StandAlone::TypeOne::Square::dock_on  with tag::winding

#endif  // ifndef MANIFEM_NO_QUOTIENT

//------------------------------------------------------------------------------------------------------//


void FiniteElement::StandAlone::TypeOne::Square::dock_on
( const Cell & cll, const tag::FirstVertex &, const Cell & P )
// virtual from FiniteElement::Core, defined there (execution forbidden), here overridden
// this function is speed-critical

{	assert ( cll .dim() == 2 );
	this->docked_on = cll;

	Cell PQ = cll. boundary() .cell_in_front_of ( P, tag::surely_exists );
	Cell Q = PQ .tip();
	Cell QR = cll. boundary() .cell_in_front_of ( Q, tag::surely_exists );
	Cell R = QR .tip();
	Cell RS = cll. boundary() .cell_in_front_of ( R, tag::surely_exists );
	Cell S = RS .tip();
	#ifndef NDEBUG  // DEBUG mode
	Cell SP = cll .boundary() .cell_in_front_of ( S, tag::surely_exists );
	assert ( SP .tip() == P );
	#endif

	Function xyz = Manifold::working .coordinates();
	assert ( xyz .number_of ( tag::components ) >= 2 );

	assert ( xyz .number_of ( tag::components ) == 2 );

	assert ( false );
	
	Function x = xyz [0], y = xyz [1];

	this->dock_on_comput ( x(P), y(P), x(Q), y(Q), x(R), y(R), x(S), y(S) );

	// code below can be viewed as a local numbering of vertices P, Q, R
	this->base_fun_1 .clear();
	this->base_fun_1 .insert ( std::pair < Cell::Core*, Function > ( P .core, this->bf1 ) );
	this->base_fun_1 .insert ( std::pair < Cell::Core*, Function > ( Q .core, this->bf2 ) );
	this->base_fun_1 .insert ( std::pair < Cell::Core*, Function > ( R .core, this->bf3 ) );
	this->base_fun_1 .insert ( std::pair < Cell::Core*, Function > ( S .core, this->bf4 ) );
	// using Cell::Core::short_int_heap for local numbering of vertices
	// should be slightly faster

}  // end of  FiniteElement::StandAlone::TypeOne::Square::dock_on with tag::first_vertex

//------------------------------------------------------------------------------------------------------//


#ifndef MANIFEM_NO_QUOTIENT

void FiniteElement::StandAlone::TypeOne::Square::dock_on  
( const Cell & cll, const tag::FirstVertex &, const Cell & P, const tag::Winding & )
// virtual from FiniteElement::Core, defined there (execution forbidden), here overridden
// this function is speed-critical

{	assert ( cll .dim() == 2 );
	this->docked_on = cll;

	Cell PQ = cll. boundary() .cell_in_front_of ( P, tag::surely_exists );
	Cell Q = PQ .tip();
	Cell QR = cll. boundary() .cell_in_front_of ( Q, tag::surely_exists );
	Cell R = QR .tip();
	Cell RS = cll. boundary() .cell_in_front_of ( R, tag::surely_exists );
	Cell S = RS .tip();
	#ifndef NDEBUG  // DEBUG mode
	Cell SP = cll .boundary() .cell_in_front_of ( S, tag::surely_exists );
	assert ( SP .tip() == P );
	#endif
	
	Function::Action winding_Q = PQ .winding(),
	                 winding_R = winding_Q + QR .winding(),
	                 winding_S = winding_R + RS .winding();
	assert ( winding_S + SP .winding() == 0 );

	Function xyz = Manifold::working .coordinates();
	assert ( xyz .number_of ( tag::components ) >= 2 );

	assert ( xyz .number_of ( tag::components ) == 2 );

	assert ( false );

	const std::vector < double > xyz_P = xyz ( P ),
	                             xyz_Q = xyz ( Q, tag::winding, winding_Q ),
	                             xyz_R = xyz ( R, tag::winding, winding_R ),
	                             xyz_S = xyz ( S, tag::winding, winding_S );
	
	this->dock_on_comput
	( xyz_P [0], xyz_P [1], xyz_Q [0], xyz_Q [1], xyz_R [0], xyz_R [1], xyz_S [0], xyz_S [1] );

	// code below can be viewed as a local numbering of vertices P, Q, R
	this->base_fun_1 .clear();
	this->base_fun_1 .insert ( std::pair < Cell::Core*, Function > ( P .core, this->bf1 ) );
	this->base_fun_1 .insert ( std::pair < Cell::Core*, Function > ( Q .core, this->bf2 ) );
	this->base_fun_1 .insert ( std::pair < Cell::Core*, Function > ( R .core, this->bf3 ) );
	this->base_fun_1 .insert ( std::pair < Cell::Core*, Function > ( S .core, this->bf4 ) );
	// using Cell::Core::short_int_heap for local numbering of vertices
	// should be slightly faster

}  // end of  FiniteElement::StandAlone::TypeOne::Square::dock_on
   //         with tag::winding and tag::first_vertex

#endif  // ifndef MANIFEM_NO_QUOTIENT

//------------------------------------------------------------------------------------------------------//


void FiniteElement::StandAlone::TypeOne::Tetrahedron::dock_on ( const Cell & cll )
// virtual from FiniteElement::Core, this function is speed-critical

{	assert ( cll .dim() == 3 );
	this->docked_on = cll;

	Mesh::Iterator it_faces = cll .boundary() .iterator
			( tag::over_cells_of_max_dim, tag::orientation_compatible_with_mesh );
	it_faces .reset();  assert ( it_faces .in_range() );  Cell QRS = * it_faces;

	Mesh::Iterator it = QRS .boundary() .iterator ( tag::over_vertices, tag::require_order );
	it .reset();  assert ( it .in_range() );  Cell Q = *it;
	it++;  assert ( it .in_range() );  Cell R = *it;
	it++;  assert ( it .in_range() );  Cell S = *it;
	it++;  assert ( not it .in_range() );

	Cell QR = QRS .boundary() .cell_behind ( R, tag::surely_exists );
	Cell PRQ = cll .boundary() .cell_in_front_of ( QR, tag::surely_exists );
	Cell QP = PRQ .boundary() .cell_in_front_of ( Q, tag::surely_exists );
	Cell P = QP .tip();
	// PQRS are 1234 in Figure 6.2 in the book
	// E.B. Becker, G.F. Carey, J.T. Oden, Finite Elements, an introduction, vol 1
	
	Function xyz = Manifold::working .coordinates();
	assert ( xyz .number_of ( tag::components ) == 3 );

	Function x = xyz [0], y = xyz [1], z = xyz [2];

	this->dock_on_comput
		( x(P), y(P), z(P), x(Q), y(Q), z(Q), x(R), y(R), z(R), x(S), y(S), z(S) );

	// code below can be viewed as a local numbering of vertices P, Q, R, S
	this->base_fun_1 .clear();
	this->base_fun_1 .insert ( std::pair < Cell::Core*, Function > ( P .core, this->bf1 ) );
	this->base_fun_1 .insert ( std::pair < Cell::Core*, Function > ( Q .core, this->bf2 ) );
	this->base_fun_1 .insert ( std::pair < Cell::Core*, Function > ( R .core, this->bf3 ) );
	this->base_fun_1 .insert ( std::pair < Cell::Core*, Function > ( S .core, this->bf4 ) );
	// using Cell::Core::short_int_heap for local numbering of vertices
	// should be slightly faster

}  // end of  FiniteElement::StandAlone::TypeOne::Tetrahedron::dock_on

//------------------------------------------------------------------------------------------------------//


#ifndef MANIFEM_NO_QUOTIENT

void FiniteElement::StandAlone::TypeOne::Tetrahedron::dock_on  // virtual from FiniteElement::Core
( const Cell & cll, const tag::Winding & )                  // this function is speed-critical

{	assert ( cll .dim() == 3 );
	this->docked_on = cll;

	Mesh::Iterator it_faces = cll .boundary() .iterator
			( tag::over_cells_of_max_dim, tag::orientation_compatible_with_mesh );
	it_faces .reset();  assert ( it_faces .in_range() );  Cell QRS = * it_faces;

	Mesh::Iterator it = QRS .boundary() .iterator ( tag::over_vertices, tag::require_order );
	it .reset();  assert ( it .in_range() );  Cell Q = *it;
	it++;  assert ( it .in_range() );  Cell R = *it;
	it++;  assert ( it .in_range() );  Cell S = *it;
	it++;  assert ( not it .in_range() );
	Cell RS = QRS .boundary() .cell_behind ( S, tag::surely_exists );

	Cell QR = QRS .boundary() .cell_behind ( R, tag::surely_exists );
	Cell PRQ = cll .boundary() .cell_in_front_of ( QR, tag::surely_exists );
	Cell QP = PRQ .boundary() .cell_in_front_of ( Q, tag::surely_exists );
	Cell P = QP .tip();
	// PQRS are 1234 in Figure 6.2 in the book
	// E.B. Becker, G.F. Carey, J.T. Oden, Finite Elements, an introduction, vol 1
	
	Function::Action winding_Q = - QP .winding(),
	                 winding_R = winding_Q + QR .winding(),
	                 winding_S = winding_R + RS .winding();
	  
	Function xyz = Manifold::working .coordinates();
	assert ( xyz .number_of ( tag::components ) == 3 );

	const std::vector < double > xyz_P = xyz ( P ),
	                             xyz_Q = xyz ( Q, tag::winding, winding_Q ),
	                             xyz_R = xyz ( R, tag::winding, winding_R ),
	                             xyz_S = xyz ( S, tag::winding, winding_S );
	
	this->dock_on_comput
		( xyz_P [0], xyz_P [1], xyz_P [2], xyz_Q [0], xyz_Q [1], xyz_Q [2],
		  xyz_R [0], xyz_R [1], xyz_R [2], xyz_S [0], xyz_S [1], xyz_S [2] );

	// code below can be viewed as a local numbering of vertices P, Q, R, S
	this->base_fun_1 .clear();
	this->base_fun_1 .insert ( std::pair < Cell::Core*, Function > ( P .core, this->bf1 ) );
	this->base_fun_1 .insert ( std::pair < Cell::Core*, Function > ( Q .core, this->bf2 ) );
	this->base_fun_1 .insert ( std::pair < Cell::Core*, Function > ( R .core, this->bf3 ) );
	this->base_fun_1 .insert ( std::pair < Cell::Core*, Function > ( S .core, this->bf4 ) );
	// using Cell::Core::short_int_heap for local numbering of vertices
	// should be slightly faster

}  // end of  FiniteElement::StandAlone::TypeOne::Tetrahedron::dock_on  with tag::winding

#endif  // ifndef MANIFEM_NO_QUOTIENT

//------------------------------------------------------------------------------------------------------//


void Integrator::Gauss::dock_on ( const Cell & cll )  // virtual from Integrator::Core

{	this->finite_element .dock_on ( cll );  }

	
void Integrator::HandCoded::dock_on ( const Cell & cll )  // virtual from Integrator::Core

{	std::cout << __FILE__ << ":" << __LINE__ << ": "
	          << __extension__ __PRETTY_FUNCTION__ << ": " << std::endl;
	std::cout << "hand-coded integrators do not support this operation yet" << std::endl;
	exit ( 1 );                                                                            }

//------------------------------------------------------------------------------------------------------//


void FiniteElement::WithMaster::pre_compute  // virtual from FiniteElement::Core
( const std::vector < Function > & bf, const std::vector < Function > & result )
{	std::cout << __FILE__ << ":" << __LINE__ << ": "
	          << __extension__ __PRETTY_FUNCTION__ << ": " << std::endl;
	std::cout << "pre-compute does not apply to this type of element" << std::endl;
	exit ( 1 );                                                                      }


void FiniteElement::StandAlone::TypeOne::pre_compute
( const std::vector < Function > & bf, const std::vector < Function > & result )
// virtual from FiniteElement::Core

// 'bf' contains one or two dummy basis functions, of type Function::MereSymbol
// 'result' contains a list of expressions whose integral the user wants
// the numeric values will be only computed later, at 'dock_on'
// and retrieved by 'integrate' which calls 'retrieve_precomputed'

{	this->selector .clear();
	#ifndef NDEBUG  // DEBUG mode
	this->pre_compute_tries = "";
	#endif

	this->dummy_bf = bf;
	
	// we analyse syntactically expressions listed in 'result'
	// and identify cases previously studied and hard-coded
	// if none matches, stop with error message

	// possible situations :
	//  1: integral of psi
	//  2: integral of psi1 * psi2
	//  3: integral of d_psi_dx
	//  4: integral of d_psi1_dx * d_psi2_dy
	//  5: integral of grad psi1 * grad psi2
	//  6: integral of psi1 * d_psi2_dx
	//  7: 1 and 2
	//  8: 1 and 3
	//  9: 1 and 4 (elasticity)
	// 10: 1 and 5 (laplacian)
	// 11: 3 and 4
	// 12: 2 and 4
	// 13: 2 and 5
	// 14: 1, 2, 3 and 4 (7,11)
	// 15: 1, 2, 3, 4 and 5 (14,5)
	// 16: everything (123456) (15,6)

	bool int_psi = false,       int_psi_psi = false,   int_dpsi = false,
	     int_dpsi_dpsi = false, int_grad_grad = false, int_psi_dpsi = false;

	for ( std::vector < Function > ::const_iterator it = result .begin();
	      it != result .end(); it++                                      )
	{	Function expr = *it;
		Function::MereSymbol * ms = dynamic_cast < Function::MereSymbol* > ( expr .core );
		if ( ms ) { int_psi = true;  continue;  }
		Function::DelayedDerivative * dd =
			dynamic_cast < Function::DelayedDerivative* > ( expr .core );
		if ( dd )
		{ if ( dynamic_cast < Function::MereSymbol* > ( dd->base .core ) == nullptr )
				goto early_error;
			int_dpsi = true;  continue;                                                 }
		Function::Product * prod = dynamic_cast < Function::Product* > ( expr .core );
		if ( prod )
		{	std::forward_list < Function > ::const_iterator itt = prod->factors .begin();
			assert ( itt != prod->factors .end() );
			ms = dynamic_cast < Function::MereSymbol* > ( itt->core );
			if ( ms )
			{	itt++;  assert ( itt != prod->factors .end() );
				ms = dynamic_cast < Function::MereSymbol* > ( itt->core );
				if ( ms )
				{	itt++;  if ( itt != prod->factors .end() )  goto early_error;
					int_psi_psi = true;  continue;                         }
				dd = dynamic_cast < Function::DelayedDerivative* > ( itt->core );
				if ( dd == nullptr )  goto early_error;
				if ( dynamic_cast < Function::MereSymbol* > ( dd->base .core ) == nullptr )
					goto early_error;
				itt++;  if ( itt != prod->factors .end() )  goto early_error;
				int_psi_dpsi = true;  continue;                                              }
			dd = dynamic_cast < Function::DelayedDerivative* > ( itt->core );
			if ( dd == nullptr )  goto early_error;
			if ( dynamic_cast < Function::MereSymbol* > ( dd->base .core ) == nullptr )
				goto early_error;
			itt++;  assert ( itt != prod->factors .end() );
			ms = dynamic_cast < Function::MereSymbol* > ( itt->core );
			if ( ms )
			{	itt++;  if ( itt != prod->factors .end() )  goto early_error;
				int_psi_dpsi = true;  continue;                       }
			dd = dynamic_cast < Function::DelayedDerivative* > ( itt->core );
			if ( dd == nullptr )  goto early_error;
			if ( dynamic_cast < Function::MereSymbol* > ( dd->base .core ) == nullptr )
				goto early_error;
			itt++;  if ( itt != prod->factors .end() )  goto early_error;
			int_dpsi_dpsi = true;  continue;                                                   }
		Function::Sum * s = dynamic_cast < Function::Sum* > ( expr .core );
		if ( s == nullptr )  goto early_error;
		for ( std::forward_list < Function > ::const_iterator itt = s->terms .begin();
					itt != s->terms .end(); itt++                                        )
			assert ( dynamic_cast < Function::Product* > ( itt->core ) );
		int_grad_grad = true;  continue;
	}  // end of for over vector 'result'

	if ( int_psi and not int_psi_psi and not int_dpsi and
	     not int_dpsi_dpsi and not int_grad_grad and not int_psi_dpsi )
	if ( this->implemented_cases .find (1) != this->implemented_cases .end() )
	{	this->cas = 1;  // int_psi
		if ( bf .size() != 1 )  goto late_error;
		if ( result .size() != 1 )  goto late_error;
		Function f = result [0];
		if ( f .core != bf [0] .core )  goto late_error;
		this->selector .push_back ( 0 ); // selector = { 0 }
		return;                                                                                        }
	#ifndef NDEBUG  // DEBUG mode
	else
		this->pre_compute_tries += "case 1 not implemented, sorry\n";
	else {}
	#endif

	if ( not int_psi and int_psi_psi and not int_dpsi and
	     not int_dpsi_dpsi and not int_grad_grad and not int_psi_dpsi )
	if ( this->implemented_cases .find (2) != this->implemented_cases .end() )
	{	this->cas = 2;  // int_psi_psi
		if ( bf .size() != 2 )  goto late_error;
		if ( result .size() != 1 )  goto late_error;
		Function::Product * prod = dynamic_cast < Function::Product* > ( result [0] .core );
		if ( prod == nullptr )  goto late_error;
		std::forward_list < Function > ::const_iterator itt = prod->factors .begin();
		assert ( itt != prod->factors .end() );
		if ( itt->core != bf [0] .core )  goto late_error;
		itt++;  assert ( itt != prod->factors .end() );
		if ( itt->core != bf [1] .core )  goto late_error;
		itt++;
		if ( itt != prod->factors .end() )  goto late_error;
		this->selector .push_back ( 0 ); // selector = { 0 }
		return;                                                                        }
	#ifndef NDEBUG  // DEBUG mode
	else
		this->pre_compute_tries += "case 2 not implemented, sorry\n";
	else {}
	#endif
	
	if ( not int_psi and not int_psi_psi and int_dpsi and
	     not int_dpsi_dpsi and not int_grad_grad and not int_psi_dpsi )
	if ( this->implemented_cases .find (3) != this->implemented_cases .end() )
	{	this->cas = 3;  // int_dpsi
		if ( bf .size() != 1 )  goto late_error;
		Function xy = Manifold::working .coordinates();
		const size_t n = xy .number_of ( tag::components );
		if ( ( n < 2 ) or ( n > 3 ) )  goto late_error;
		// we analyse again the requested 'result' to see which expressions are wanted
		if ( result .size() == 0 )  goto late_error;
		if ( result .size() > n )  goto late_error;
		for ( size_t i = 0; i < result .size(); i++ )
		{	Function f = result [i];
			Function::DelayedDerivative * dd = 
				dynamic_cast < Function::DelayedDerivative* > ( f .core );
			if ( dd == nullptr )  goto late_error;
			if ( dd->base .core != bf[0] .core )  goto late_error;
			bool found = false;
			for ( size_t j = 0; j < n; j++ )
				if ( dd->variable .core == xy [j] .core )
				{	this->selector .push_back (j);
					found = true;  break;           }
			if ( not found )  goto late_error;                             }
		return;                                                              }
	#ifndef NDEBUG  // DEBUG mode
	else
		this->pre_compute_tries += "case 3 not implemented, sorry\n";
	else {}
	#endif
	
	if ( not int_psi and not int_psi_psi and not int_dpsi and
	     int_dpsi_dpsi and not int_grad_grad and not int_psi_dpsi )
	if ( this->implemented_cases .find (4) != this->implemented_cases .end() )
	{	this->cas = 4;  // int_dpsi_dpsi
		if ( bf .size() != 2 )  goto late_error;
		Function xy = Manifold::working .coordinates();
		const size_t n = xy .number_of ( tag::components );
		if ( ( n < 2 ) or ( n > 3 ) )  goto late_error;
		// we analyse again the requested 'result' to see which expressions are wanted
		if ( result .size() == 0 )  goto late_error;
		for ( size_t i = 0; i < result .size(); i++ )
		{	Function f = result [i];
			Function::Product * prod = dynamic_cast < Function::Product* > ( f .core );
			if ( prod == nullptr )  goto late_error;
			std::forward_list < Function > ::const_iterator itt = prod->factors .begin();
			assert ( itt != prod->factors .end() );
			Function::DelayedDerivative * d1 = dynamic_cast
				< Function::DelayedDerivative* > ( itt->core );
			if ( d1 == nullptr )  goto late_error;
			if ( d1->base .core == nullptr )  goto late_error;
			itt++;  assert ( itt != prod->factors .end() );
			Function::DelayedDerivative * d2 = dynamic_cast
				< Function::DelayedDerivative* > ( itt->core );
			if ( d2 == nullptr )  goto late_error;
			if ( d2->base .core == nullptr )  goto late_error;
			itt++;
			if ( itt != prod->factors .end() )  goto late_error;
			Function::Core * v1 = d1->variable .core;
			Function::Core * v2 = d2->variable .core;
			bool not_found = true;
			for ( size_t j = 0; not_found and ( j < n ); j++ )
			for ( size_t k = 0; k < n; k++ )
				if ( ( v1 == xy [j] .core ) and ( v2 == xy [k] .core ) )
				{	this->selector .push_back ( j*n + k );
					not_found = false;  break;              }
			if ( not_found )  goto late_error;                                             }
		return;                                                                              }
	#ifndef NDEBUG  // DEBUG mode
	else
		this->pre_compute_tries += "case 4 not implemented, sorry\n";
	else {}
	#endif
	
	if ( not int_psi and not int_psi_psi and not int_dpsi and
	     not int_dpsi_dpsi and int_grad_grad and not int_psi_dpsi )
	if ( this->implemented_cases .find (5) != this->implemented_cases .end() )
	{	this->cas = 5;  // int_grad_grad
		if ( bf .size() != 2 )  goto late_error;
		Function xy = Manifold::working .coordinates();
		const size_t n = xy .number_of ( tag::components );
		if ( ( n < 2 ) or ( n > 3 ) )  goto late_error;
		// we analyse again the requested 'result' to confirm only the gradient is requested
		if ( result .size() != 1 )  goto late_error;
		Function f = result [0];
		Function::Sum * s = dynamic_cast < Function::Sum* > ( f .core );
		if ( s == nullptr )  goto late_error;
		#ifndef NDEBUG  // DEBUG mode
		std::vector < size_t > variables ( n, 0 );
		for ( std::forward_list < Function > ::const_iterator it_s = s->terms .begin();
		      it_s != s->terms .end(); it_s++                                          )
		{	Function::Product * fp = dynamic_cast < Function::Product* > ( it_s->core );
			if ( fp == nullptr )  goto late_error;
			std::forward_list < Function > ::const_iterator it_p = fp->factors .begin();
			assert ( it_p != fp->factors .end() );
			Function::DelayedDerivative * dd1 = dynamic_cast
				< Function::DelayedDerivative* > ( it_p->core );
			if ( dd1 == nullptr )  goto late_error;
			if ( dd1->base .core != bf [0] .core )  goto late_error;
			size_t ii = 3000;
			for ( size_t i = 0; i < n; i++ )
				if ( dd1->variable .core == xy [i] .core )
				{	if ( variables [i] != 0 )  goto late_error;
					ii = i;
					variables [i] = 1;                           }
			if ( ii >= n )  goto late_error;
			it_p ++;  assert ( it_p != fp->factors .end() );
			Function::DelayedDerivative * dd2 = dynamic_cast
				< Function::DelayedDerivative* > ( it_p->core );
			if ( dd2 == nullptr )  goto late_error;
			if ( dd2->base .core != bf [1] .core )  goto late_error;
			if ( dd2->variable .core != xy [ii] .core )  goto late_error;
			it_p ++;
			if ( it_p != fp->factors .end() )  goto late_error;                           }
		for ( size_t i = 0; i < n; i++ )
			if ( variables [i] != 1 )  goto late_error;
		#endif  // DEBUG
		this->selector .push_back ( 0 ); // selector = { 0 }
		return;                                                                             }
	#ifndef NDEBUG  // DEBUG mode
	else
		this->pre_compute_tries += "case 5 not implemented, sorry\n";
	else {}
	#endif
		
	if ( not int_psi and not int_psi_psi and not int_dpsi and
	     not int_dpsi_dpsi and not int_grad_grad and int_psi_dpsi )
	if ( this->implemented_cases .find (6) != this->implemented_cases .end() )
	{	this->cas = 6;  // int_psi_dpsi
		Function xy = Manifold::working .coordinates();
		const size_t n = xy .number_of ( tag::components );
		if ( ( n < 2 ) or ( n > 3 ) )  goto late_error;
		// we analyse again the requested 'result' to see which expressions are wanted
		if ( result .size() == 0 )  goto late_error;
		if ( result .size() >= 3 )  goto late_error;
		for ( size_t i = 0; i < result .size(); i++ )
		{	Function::Product * prod = dynamic_cast
				< Function::Product* > ( result [i] .core );
			if ( prod == nullptr )   goto late_error;
			std::forward_list < Function > ::const_iterator itt = prod->factors .begin();
			assert ( itt != prod->factors .end() );
			if ( itt->core != bf [0] .core )  goto late_error;
			itt++;  assert ( itt != prod->factors .end() );
			Function::DelayedDerivative * dd =
				dynamic_cast < Function::DelayedDerivative* > ( itt->core );
			if ( dd == nullptr )  goto late_error;
			if ( dd->base .core != bf [1] .core )  goto late_error;
			bool found = false;
			for ( size_t j = 0; j < n; j++ )
				if ( dd->variable .core == xy [j] .core )
				{	this->selector .push_back ( j );
					found = true;  break;             }
			if ( not found )  goto late_error;
			itt++;
			if ( itt != prod->factors .end() )  goto late_error;                           }
		return;                                                                              }
	#ifndef NDEBUG  // DEBUG mode
	else
		this->pre_compute_tries += "case 6 not implemented, sorry\n";
	else {}
	#endif
	
	if ( int_psi and int_psi_psi and not int_dpsi and
	     not int_dpsi_dpsi and not int_grad_grad and not int_psi_dpsi )
	if ( this->implemented_cases .find (7) != this->implemented_cases .end() )
	{	this->cas = 7;  // int_psi and int_psi_psi  // 1, 2
		// we analyse again the requested 'result' to see which expressions are wanted
		if ( result .size() != 2 )  goto late_error;
		Function f0 = result [0], f1 = result [1];
		Function::MereSymbol * ff0 = dynamic_cast < Function::MereSymbol* > ( f0 .core );
		if ( ff0 )
		{  Function::Product * prod = dynamic_cast < Function::Product* > ( f1 .core );
			if ( prod == nullptr )  goto late_error;
			std::forward_list < Function > ::const_iterator itt = prod->factors .begin();
			assert ( itt != prod->factors .end() );
			Function::MereSymbol * ms = dynamic_cast < Function::MereSymbol* > ( itt->core );
			if ( ms == nullptr )  goto late_error;
			if ( ms != bf [0] .core )  goto late_error;
			itt++;  assert ( itt != prod->factors .end() );
			ms = dynamic_cast < Function::MereSymbol* > ( itt->core );
			if ( ms == nullptr )  goto late_error;
			if ( ms != bf [1] .core )  goto late_error;
			itt++;
			if ( itt != prod->factors .end() )  goto late_error;
			if ( ff0 != bf [0] .core )  goto late_error;
			this->selector = { 0, 1 };                                                         }
		else
		{	Function::Product * prod = dynamic_cast < Function::Product* > ( f0 .core );
			if ( prod == nullptr )  goto late_error;
			std::forward_list < Function > ::const_iterator itt = prod->factors .begin();
			assert ( itt != prod->factors .end() );
			Function::MereSymbol * ms = dynamic_cast < Function::MereSymbol* > ( itt->core );
			if ( ms == nullptr )  goto late_error;
			if ( ms != bf [0] .core )  goto late_error;
			itt++;  assert ( itt != prod->factors .end() );
			ms = dynamic_cast < Function::MereSymbol* > ( itt->core );
			if ( ms == nullptr )  goto late_error;
			if ( ms != bf [1] .core )  goto late_error;
			itt++;
			if ( itt != prod->factors .end() )  goto late_error;
			Function::MereSymbol * ff1 = dynamic_cast < Function::MereSymbol* > ( f1 .core );
			if ( ff1 != bf [0] .core )  goto late_error;
			this->selector = { 1, 0 };                                                         }
		return;                                                                                  }
	#ifndef NDEBUG  // DEBUG mode
	else
		this->pre_compute_tries +=
		  "case 7 not implemented, try calling 'pre_compute' with subsets of integral expressions\n";
	else {}
	#endif

	if ( int_psi and not int_psi_psi and int_dpsi and
	     not int_dpsi_dpsi and not int_grad_grad and not int_psi_dpsi )
	if ( this->implemented_cases .find (8) != this->implemented_cases .end() )
	{	this->cas = 8;  // int_psi, int_dpsi  // 1, 3
		if ( bf .size() != 1 )  goto late_error;
		Function xy = Manifold::working .coordinates();
		const size_t n = xy .number_of ( tag::components );
		if ( ( n < 2 ) or ( n > 3 ) )  goto late_error;
		// we analyse again the requested 'result' to see which expressions are wanted
		if ( result .size() <= 1 )  goto late_error;
		if ( result .size() > n+1 )  goto late_error;
		for ( size_t i = 0; i < result .size(); i++ )
		{	Function f = result [i];
			if ( f .core == bf[0] .core )
			{	this->selector .push_back ( 0 );  continue;  }
			Function::DelayedDerivative * dd = dynamic_cast
				< Function::DelayedDerivative* > ( f .core );
			if ( dd == nullptr )  goto late_error;
			if ( dd->base .core != bf[0] .core )  goto late_error;
			bool found = false;
			for ( size_t j = 0; j < n; j++ )
				if ( dd->variable .core == xy [j] .core )
				{	this->selector .push_back ( j+1 );
					found = true;  break;         }
			if ( not found )  goto late_error;                      }
		return;                                                       }
	#ifndef NDEBUG  // DEBUG mode
	else
		this->pre_compute_tries +=
		  "case 8 not implemented, try calling 'pre_compute' with subsets of integral expressions\n";
	else {}
	#endif

	if ( int_psi and not int_psi_psi and not int_dpsi and
	     int_dpsi_dpsi and not int_grad_grad and not int_psi_dpsi )
	if ( this->implemented_cases .find (9) != this->implemented_cases .end() )
	{	this->cas = 9;  // int_psi, int_dpsi_dpsi  // 1, 4  // elasticity
		if ( bf .size() != 2 )  goto late_error;
		Function xy = Manifold::working .coordinates();
		const size_t n = xy .number_of ( tag::components );
		if ( ( n < 2 ) or ( n > 3 ) )  goto late_error;
		// we analyse again the requested 'result' to see which expressions are wanted
		if ( result .size() == 0 )  goto late_error;
		for ( size_t i = 0; i < result .size(); i++ )
		{	Function f = result [i];
			Function::MereSymbol * ms =
				dynamic_cast < Function::MereSymbol* > ( f .core );
			if ( ms )
			{	this->selector .push_back ( 0 );  continue;  }
			Function::Product * prod = dynamic_cast < Function::Product* > ( f .core );
			if ( prod == nullptr )  goto late_error;
			std::forward_list < Function > ::const_iterator itt = prod->factors .begin();
			assert ( itt != prod->factors .end() );
			Function::DelayedDerivative * d1 = dynamic_cast
				< Function::DelayedDerivative* > ( itt->core );
			if ( d1 == nullptr )  goto late_error;
			itt++;  assert ( itt != prod->factors .end() );
			Function::DelayedDerivative * d2 = dynamic_cast
				< Function::DelayedDerivative* > ( itt->core );
			itt++;
			if ( itt != prod->factors .end() )  goto late_error;
			if ( d1->base .core != bf [0] .core )  goto late_error;
			if ( d2->base .core != bf [1] .core )  goto late_error;
			Function::Core * v1 = d1->variable .core;
			Function::Core * v2 = d2->variable .core;
			bool not_found = true;
			for ( size_t j = 0; not_found and ( j < n ); j++ )
			for ( size_t k = 0; k < n; k++ )
				if ( ( v1 == xy [j] .core ) and ( v2 == xy [k] .core ) )
				{	this->selector .push_back ( 1 + j*n + k );
					not_found = false;  break;                  }
			if ( not_found )  goto late_error;                                             }
		return;                                                                              }
	#ifndef NDEBUG  // DEBUG mode
	else
		this->pre_compute_tries +=
			"case 9 not implemented, try calling 'pre_compute' with subsets of integral expressions\n";
	else {}
	#endif
		
	if ( int_psi and not int_psi_psi and not int_dpsi and
	     not int_dpsi_dpsi and int_grad_grad and not int_psi_dpsi )
	if ( this->implemented_cases .find (10) != this->implemented_cases .end() )
	{	this->cas = 10;  // int_psi, int_grad_grad  // 1, 5  // Laplacian
		if ( bf .size() != 2 )  goto late_error;
		Function xy = Manifold::working .coordinates();
		const size_t n = xy .number_of ( tag::components );
		if ( ( n < 2 ) or ( n > 3 ) )  goto late_error;
		// we analyse again the requested 'result' to see which expressions are wanted
		if ( result .size() != 2 )  goto late_error;
		for ( size_t j = 0; j < result .size(); j++ )
		{	Function f = result [j];
			if ( f .core == bf [0] .core )
			{	this->selector .push_back ( 0 );
				continue;                         }
			Function::Sum * s = dynamic_cast < Function::Sum* > ( f .core );
			if ( s == nullptr )  goto late_error;
			std::vector < size_t > variables ( n, 0 );
			for ( std::forward_list < Function > ::const_iterator it_s = s->terms .begin();
			      it_s != s->terms .end(); it_s++                                          )
			{	Function::Product * fp = dynamic_cast < Function::Product* > ( it_s->core );
				if ( fp == nullptr )  goto late_error;
				std::forward_list < Function > ::const_iterator it_p = fp->factors .begin();
				assert ( it_p != fp->factors .end() );
				Function::DelayedDerivative * dd1 = dynamic_cast
						< Function::DelayedDerivative* > ( it_p->core );
				if ( dd1 == nullptr )  goto late_error;
				if ( dynamic_cast < Function::MereSymbol* > ( dd1->base .core ) == nullptr )
					goto late_error;
				if ( dd1->base .core != bf [0] .core )  goto late_error;
				size_t ii = 3000;
				for ( size_t i = 0; i < n; i++ )
					if ( dd1->variable .core == xy [i] .core )
					{	if ( variables [i] != 0 )  goto late_error;
						ii = i;
						variables [i] = 1;                           }
				if ( ii >= n )  goto late_error;
				it_p ++;  assert ( it_p != fp->factors .end() );
				Function::DelayedDerivative * dd2 = dynamic_cast
						< Function::DelayedDerivative* > ( it_p->core );
				if ( dd2 == nullptr )  goto late_error;
				if ( dynamic_cast < Function::MereSymbol* > ( dd2->base .core ) == nullptr )
					goto late_error;
				if ( dd2->base .core != bf [1] .core )  goto late_error;
				if ( dd2->variable .core != xy [ii] .core )  goto late_error;
				it_p ++;
				if ( it_p != fp->factors .end() )  goto late_error;                            }
			for ( size_t i = 0; i < n; i++ )  assert ( variables [i] == 1 );
			this->selector .push_back ( 1 );                                                     }
		return;                                                                                    }
	#ifndef NDEBUG  // DEBUG mode
	else
		this->pre_compute_tries +=
			"case 10 not implemented, try calling 'pre_compute' with subsets of integral expressions\n";
	else {}
	#endif
		
	if ( not int_psi and not int_psi_psi and int_dpsi and
	     int_dpsi_dpsi and not int_grad_grad and not int_psi_dpsi )
	if ( this->implemented_cases .find (11) != this->implemented_cases .end() )
	{	this->cas = 11;  // int_dpsi and int_dpsi_dpsi  // 3, 4
		if ( bf .size() != 2 )  goto late_error;
		Function xy = Manifold::working .coordinates();
		const size_t n = xy .number_of ( tag::components );
		if ( ( n < 2 ) or ( n > 3 ) )  goto late_error;
		// we analyse again the requested 'result' to see which expressions are wanted
		if ( result .size() < 2 )  goto late_error;
		for ( size_t i = 0; i < result .size(); i++ )
		{	Function f = result [i];
			Function::DelayedDerivative * dd = 
				dynamic_cast < Function::DelayedDerivative* > ( f .core );
			if ( dd )
			{	if ( dd->base .core != bf[0] .core )  goto late_error;
				bool found = false;
				for ( size_t j = 0; j < n; j++ )
					if ( dd->variable .core == xy [j] .core )
					{	this->selector .push_back (j);
						found = true;  break;           }
				if ( not found )  goto late_error;
				continue;                                               }
			Function::Product * prod = dynamic_cast < Function::Product* > ( f .core );
			if ( prod == nullptr )  goto late_error;
			std::forward_list < Function > ::const_iterator itt = prod->factors .begin();
			assert ( itt != prod->factors .end() );
			Function::DelayedDerivative * d1 = dynamic_cast
				< Function::DelayedDerivative* > ( itt->core );
			if ( d1 == nullptr )  goto late_error;
			if ( d1->base .core != bf[0] .core )  goto late_error;
			itt++;  assert ( itt != prod->factors .end() );
			Function::DelayedDerivative * d2 = dynamic_cast
				< Function::DelayedDerivative* > ( itt->core );
			if ( d2 == nullptr )  goto late_error;
			if ( d2->base .core != bf[1] .core )  goto late_error;
			itt++;
			if ( itt != prod->factors .end() )  goto late_error;
			Function::Core * v1 = d1->variable .core;
			Function::Core * v2 = d2->variable .core;
			bool not_found = true;
			for ( size_t j = 0; not_found and ( j < n ); j++ )
			for ( size_t k = 0; k < n; k++ )
				if ( ( v1 == xy [j] .core ) and ( v2 == xy [k] .core ) )
				{	this->selector .push_back ( n + j*n + k );
					not_found = false;  break;                  }
			if ( not_found )  goto late_error;                                             }
		return;                                                                              }
	#ifndef NDEBUG  // DEBUG mode
	else
		this->pre_compute_tries +=
			"case 11 not implemented, try calling 'pre_compute' with subsets of integral expressions\n";
	else {}
	#endif
	
	if ( not int_psi and int_psi_psi and not int_dpsi and
	     int_dpsi_dpsi and not int_grad_grad and not int_psi_dpsi )
	if ( this->implemented_cases .find (12) != this->implemented_cases .end() )
	{	this->cas = 12;  // int_psi_psi and int_dpsi_dpsi  // 2, 4
		if ( bf .size() != 2 )  goto late_error;
		// we analyse again the requested 'result' to see which expressions are wanted
		if ( result .size() < 2 )  goto late_error;
		Function xy = Manifold::working .coordinates();
		const size_t n = xy .number_of ( tag::components );
		if ( ( n < 2 ) or ( n > 3 ) )  goto late_error;
		for ( size_t i = 0; i < result .size(); i++ )
		{	Function f = result [i];
			Function::Product * prod = dynamic_cast < Function::Product* > ( f .core );
			if ( prod == nullptr )  goto late_error;
			std::forward_list < Function > ::const_iterator itt = prod->factors .begin();
			assert ( itt != prod->factors .end() );
			Function::MereSymbol * ms = dynamic_cast < Function::MereSymbol* > ( itt->core );
			if ( ms )
			{	if ( ms != bf [0] .core )  goto late_error;
				itt++;  assert ( itt != prod->factors .end() );
				ms = dynamic_cast < Function::MereSymbol* > ( itt->core );
				if ( ms == nullptr )  goto late_error;
				if ( ms != bf [1] .core )  goto late_error;
				this->selector .push_back ( 0 );
				continue;                                                   }
			Function::DelayedDerivative * d1 = dynamic_cast
				< Function::DelayedDerivative* > ( itt->core );
			if ( d1 == nullptr )  goto late_error;
			itt++;  assert ( itt != prod->factors .end() );
			Function::DelayedDerivative * d2 = dynamic_cast
				< Function::DelayedDerivative* > ( itt->core );
			if ( d2 == nullptr )  goto late_error;
			itt++;
			if ( itt != prod->factors .end() )  goto late_error;
			if ( d1->base .core != bf [0] .core )  goto late_error;
			if ( d2->base .core != bf [1] .core )  goto late_error;
			Function::Core * v1 = d1->variable .core;
			Function::Core * v2 = d2->variable .core;
			bool not_found = true;
			for ( size_t j = 0; not_found and ( j < n ); j++ )
			for ( size_t k = 0; k < n; k++ )
				if ( ( v1 == xy [j] .core ) and ( v2 == xy [k] .core ) )
				{	this->selector .push_back ( 1 + j*n + k );
					not_found = false;  break;                  }
			if ( not_found )  goto late_error;                                                 }
		return;                                                                                  }
	#ifndef NDEBUG  // DEBUG mode
	else
		this->pre_compute_tries +=
			"case 12 not implemented, try calling 'pre_compute' with subsets of integral expressions\n";
	else {}
	#endif
	
	if ( not int_psi and int_psi_psi and not int_dpsi and
	     not int_dpsi_dpsi and int_grad_grad and not int_psi_dpsi )
	if ( this->implemented_cases .find (13) != this->implemented_cases .end() )
	{	this->cas = 13;  // int_psi_psi and int_grad_grad  // 2, 5
		if ( bf .size() != 2 )  goto late_error;
		// we analyse again the requested 'result' to see which expressions are wanted
		if ( result .size() != 2 )  goto late_error;
		Function xy = Manifold::working .coordinates();
		const size_t n = xy .number_of ( tag::components );
		if ( ( n < 2 ) or ( n > 3 ) )  goto late_error;
		for ( size_t i = 0; i < result .size(); i++ )
		{	Function f = result [i];
			Function::Product * prod = dynamic_cast < Function::Product* > ( f .core );
			if ( prod )
			{	std::forward_list < Function > ::const_iterator itt = prod->factors .begin();
				assert ( itt != prod->factors .end() );
				if ( itt->core != bf [0] .core )  goto late_error;
				itt++;  assert ( itt != prod->factors .end() );
				if ( itt->core != bf [1] .core )  goto late_error;
				itt++;
				if ( itt != prod->factors .end() )  goto late_error;
				this->selector .push_back ( 0 );
				continue;                                                                         }
			Function::Sum * s = dynamic_cast < Function::Sum* > ( f .core );
			if ( s == nullptr )  goto late_error;
			std::vector < size_t > variables ( n, 0 );
			for ( std::forward_list < Function > ::const_iterator it_s = s->terms .begin();
			      it_s != s->terms .end(); it_s++                                          )
			{	Function::Product * fp = dynamic_cast < Function::Product* > ( it_s->core );
				if ( fp == nullptr )  goto late_error;
				std::forward_list < Function > ::const_iterator it_p = fp->factors .begin();
				assert ( it_p != fp->factors .end() );
				Function::DelayedDerivative * dd1 = dynamic_cast
					< Function::DelayedDerivative* > ( it_p->core );
				if ( dd1 == nullptr )  goto late_error;
				if ( dd1->base .core != bf [0] .core )  goto late_error;
				size_t ii = 3000;
				for ( size_t j = 0; j < n; j++ )
					if ( dd1->variable .core == xy [j] .core )
					{	if ( variables [j] != 0 )  goto late_error;
						ii = j;
						variables [j] = 1;                           }
				if ( ii >= n )  goto late_error;
				it_p ++;  assert ( it_p != fp->factors .end() );
				Function::DelayedDerivative * dd2 = dynamic_cast
					< Function::DelayedDerivative* > ( it_p->core );
				if ( dd2 == nullptr )  goto late_error;
				if ( dd2->base .core != bf [1] .core )  goto late_error;
				if ( dd2->variable .core != xy [ii] .core )  goto late_error;
				it_p ++;
				if ( it_p != fp->factors .end() )  goto late_error;                           }
			for ( size_t j = 0; j < n; j++ )
				if ( variables [j] != 1 )  goto late_error;
			this->selector .push_back ( 1 );                                                    }
		return;                                                                                   }
	#ifndef NDEBUG  // DEBUG mode
	else
		this->pre_compute_tries +=
			"case 13 not implemented, try calling 'pre_compute' with subsets of integral expressions\n";
	else {}
	#endif
	
	// triggered by 23, 123, 124, 134, 1234
	if ( not int_grad_grad and not int_psi_dpsi )  // not 5, not 6
	if ( this->implemented_cases .find (14) != this->implemented_cases .end() )
	{	this->cas = 14;  // int_psi, int_psi_psi, int_dpsi and int_dpsi_dpsi  // 1, 2, 3, 4
		if ( bf .size() != 2 )  goto late_error;
		Function xy = Manifold::working .coordinates();
		const size_t n = xy .number_of ( tag::components );
		if ( ( n < 2 ) or ( n > 3 ) )  goto late_error;
		#ifndef NDEBUG  // DEBUG mode
		if ( not int_psi or not int_psi_psi or not int_dpsi or not int_dpsi_dpsi )
			this->pre_compute_tries +=
				"you may want to try computing different integrals separately, " 
				"test which version is faster\n";
		#endif
		// we analyse again the requested 'result' to see which expressions are wanted
		for ( size_t i = 0; i < result .size(); i++ )
		{	Function f = result [i];
			Function::MereSymbol * ms = dynamic_cast < Function::MereSymbol* > ( f .core );
			if ( ms )
			{	if ( ms != bf [0] .core )  goto late_error;
				this->selector .push_back ( 0 );   // case 1
				continue;                                    }
			Function::DelayedDerivative * dd = dynamic_cast
				< Function::DelayedDerivative* > ( f .core );
			if ( dd )
			{	if ( dd->base .core != bf [0] .core )  goto late_error;
				bool found = false;
				for ( size_t j = 0; j < n; j++ )
					if ( dd->variable .core == xy [j] .core )
					{	this->selector .push_back ( 2 + j );   // case 3
						found = true;  break;                 }
				if ( not found )  goto late_error;
				continue;                                     }
			Function::Product * prod = dynamic_cast < Function::Product* > ( f .core );
			if ( prod == nullptr )  goto late_error;
			std::forward_list < Function > ::const_iterator itt = prod->factors .begin();
			assert ( itt != prod->factors .end() );
			ms = dynamic_cast < Function::MereSymbol* > ( itt->core );
			if ( ms )
			{	if ( ms != bf [0] .core )  goto late_error;
				itt++;  assert ( itt != prod->factors .end() );
				ms = dynamic_cast < Function::MereSymbol* > ( itt->core );
				if ( ms == nullptr )  goto late_error;
				if ( ms != bf [1] .core )  goto late_error;
				this->selector .push_back ( 1 );   // case 2
				continue;                                                   }
			Function::DelayedDerivative * d1 = dynamic_cast
				< Function::DelayedDerivative* > ( itt->core );
			if ( d1 == nullptr )  goto late_error;
			itt++;  assert ( itt != prod->factors .end() );
			Function::DelayedDerivative * d2 = dynamic_cast
				< Function::DelayedDerivative* > ( itt->core );
			if ( d2 == nullptr )  goto late_error;
			itt++;
			if ( itt != prod->factors .end() )  goto late_error;
			if ( d1->base .core != bf [0] .core )  goto late_error;
			if ( d2->base .core != bf [1] .core )  goto late_error;
			Function::Core * v1 = d1->variable .core;
			Function::Core * v2 = d2->variable .core;
			bool not_found = true;
			for ( size_t j = 0; not_found and ( j < n ); j++ )
			for ( size_t k = 0; k < n; k++ )
				if ( ( v1 == xy [j] .core ) and ( v2 == xy [k] .core ) )
				{	this->selector .push_back ( 2 + n + j*n + k );   // case 4
					not_found = false;  break;                      }
			if ( not_found )  goto late_error;                                             }
		return;                                                                           }
	#ifndef NDEBUG  // DEBUG mode
	else
		this->pre_compute_tries +=
			"case 14 not implemented, try calling 'pre_compute' with subsets of integral expressions\n";
	else {}
	#endif

	if ( not int_psi_dpsi )  // not 6
	// triggered by 35, 125, 135, 145, 235, 245, 345, 1235, 1245, 1345, 2345, 12345
	if ( this->implemented_cases .find (15) != this->implemented_cases .end() )
	{	this->cas = 15;  // everything but int_psi_dpsi  // everything but 6  // 1, 2, 3, 4, 5
		if ( bf .size() != 2 )  goto late_error;
		Function xy = Manifold::working .coordinates();
		const size_t n = xy .number_of ( tag::components );
		if ( ( n < 2 ) or ( n > 3 ) )  goto late_error;
		#ifndef NDEBUG  // DEBUG mode
		if ( not int_psi or not int_psi_psi or not int_dpsi or not int_dpsi_dpsi or not int_grad_grad )
			this->pre_compute_tries +=
				"you may want to try computing different integrals separately, " 
				"test which version is faster\n";
		#endif
		// we analyse again the requested 'result' to see which expressions are wanted
		for ( size_t i = 0; i < result .size(); i++ )
		{	Function f = result [i];
			Function::MereSymbol * ms = dynamic_cast < Function::MereSymbol* > ( f .core );
			if ( ms )
			{	if ( ms != bf [0] .core )  goto late_error;
				this->selector .push_back ( 0 );   // case 1
				continue;                                    }
			Function::DelayedDerivative * dd = dynamic_cast
				< Function::DelayedDerivative* > ( f .core );
			if ( dd )
			{	if ( dd->base .core != bf [0] .core )  goto late_error;
				bool found = false;
				for ( size_t j = 0; j < n; j++ )
					if ( dd->variable .core == xy [j] .core )
					{	this->selector .push_back ( 2 + j );   // case 3
						found = true;  break;                 }
				if ( not found )  goto late_error;
				continue;                                     }
			Function::Sum * s = dynamic_cast < Function::Sum* > ( f .core );
			if ( s )
			{	size_t j = 0;
				for ( std::forward_list < Function > ::const_iterator it_s
				      = s->terms .begin(); it_s != s->terms .end(); it_s++ )
				{	Function::Product * fp = dynamic_cast < Function::Product* > ( it_s->core );
					if ( fp == nullptr )  goto late_error;
					std::forward_list < Function > ::const_iterator it_p = fp->factors .begin();
					assert ( it_p != fp->factors .end() );
					Function::DelayedDerivative * dd1 = dynamic_cast
						< Function::DelayedDerivative* > ( it_p->core );
					if ( dd1 == nullptr )  goto late_error;
					if ( dynamic_cast < Function::MereSymbol* > ( dd1->base .core ) == nullptr )
						goto late_error;
					if ( dd1->base .core != bf [0] .core )  goto late_error;
					if ( dd1->variable .core != xy [j] .core )  goto late_error;
					it_p ++;  assert ( it_p != fp->factors .end() );
					Function::DelayedDerivative * dd2 = dynamic_cast
						< Function::DelayedDerivative* > ( it_p->core );
					if ( dd2 == nullptr )  goto late_error;
					if ( dynamic_cast < Function::MereSymbol* > ( dd2->base .core ) == nullptr )
						goto late_error;
					if ( dd2->base .core != bf [1] .core )  goto late_error;
					if ( dd2->variable .core != xy [j] .core )  goto late_error;
					it_p ++;
					if ( it_p != fp->factors .end() )  goto late_error;
					j++;                                                                           }
				assert ( j == 2 );
				this->selector .push_back ( 2 + n + n*n );  // case 5
				continue;                                                                            }
			Function::Product * prod = dynamic_cast < Function::Product* > ( f .core );
			if ( prod == nullptr )  goto late_error;
			std::forward_list < Function > ::const_iterator itt = prod->factors .begin();
			assert ( itt != prod->factors .end() );
			ms = dynamic_cast < Function::MereSymbol* > ( itt->core );
			if ( ms )
			{	if ( ms != bf [0] .core )  goto late_error;
				itt++;  assert ( itt != prod->factors .end() );
				ms = dynamic_cast < Function::MereSymbol* > ( itt->core );
				if ( ms == nullptr )  goto late_error;
				if ( ms != bf [1] .core )  goto late_error;
				this->selector .push_back ( 1 );   // case 2
				continue;                                                   }
			Function::DelayedDerivative * d1 = dynamic_cast
				< Function::DelayedDerivative* > ( itt->core );
			if ( d1 == nullptr )  goto late_error;
			itt++;  assert ( itt != prod->factors .end() );
			Function::DelayedDerivative * d2 = dynamic_cast
				< Function::DelayedDerivative* > ( itt->core );
			if ( d2 == nullptr )  goto late_error;
			itt++;
			if ( itt != prod->factors .end() )  goto late_error;
			if ( d1->base .core != bf [0] .core )  goto late_error;
			if ( d2->base .core != bf [1] .core )  goto late_error;
			Function::Core * v1 = d1->variable .core;
			Function::Core * v2 = d2->variable .core;
			bool not_found = true;
			for ( size_t j = 0; not_found and ( j < n ); j++ )
			for ( size_t k = 0; k < n; k++ )
				if ( ( v1 == xy [j] .core ) and ( v2 == xy [k] .core ) )
				{	this->selector .push_back ( 2 + n + j*n + k );   // case 4
					not_found = false;  break;                      }
			if ( not_found )  goto late_error;                                                       }
		return;                                                                                        }
	#ifndef NDEBUG  // DEBUG mode
	else
		this->pre_compute_tries +=
			"case 15 not implemented, try calling 'pre_compute' with subsets of integral expressions\n";
	else {}
	#endif

	// no need to ask anything, this is the last case, swallows everything
	if ( this->implemented_cases .find (16) != this->implemented_cases .end() )
	{	this->cas = 16;  // everything
		if ( bf .size() != 2 )  goto late_error;
		Function xy = Manifold::working .coordinates();
		const size_t n = xy .number_of ( tag::components );
		if ( ( n < 2 ) or ( n > 3 ) )  goto late_error;
		#ifndef NDEBUG  // DEBUG mode
		if ( not int_psi or not int_psi_psi or not int_dpsi or not int_dpsi_dpsi or not int_grad_grad )
			this->pre_compute_tries +=
				"you may want to try computing different integrals separately, " 
				"test which version is faster\n";
		#endif
		// we analyse again the requested 'result' to see which expressions are wanted
		for ( size_t i = 0; i < result .size(); i++ )
		{	Function f = result [i];
			Function::MereSymbol * ms =
				dynamic_cast < Function::MereSymbol* > ( f .core );
			if ( ms )
			{	if ( ms != bf [0] .core )  goto late_error;
				this->selector .push_back ( 0 );   // case 1
				continue;                                    }
			Function::DelayedDerivative * dd = dynamic_cast
				< Function::DelayedDerivative* > ( f .core );
			if ( dd )
			{	if ( dd->base .core != bf [0] .core )  goto late_error;
				bool found = false;
				for ( size_t j = 0; j < n; j++ )
					if ( dd->variable .core == xy [j] .core )
					{	this->selector .push_back ( 2 + j );   // case 3
						found = true;  break;                 }
				if ( not found )  goto late_error;
				continue;                                     }
			Function::Sum * s = dynamic_cast < Function::Sum* > ( f .core );
			if ( s )
			{	size_t j = 0;
				for ( std::forward_list < Function > ::const_iterator it_s
				      = s->terms .begin(); it_s != s->terms .end(); it_s++ )
				{	Function::Product * fp = dynamic_cast < Function::Product* > ( it_s->core );
					if ( fp == nullptr )  goto late_error;
					if ( j >= n )  goto late_error;
					std::forward_list < Function > ::const_iterator it_p = fp->factors .begin();
					assert ( it_p != fp->factors .end() );
					Function::DelayedDerivative * dd1 = dynamic_cast
						< Function::DelayedDerivative* > ( it_p->core );
					if ( dd1 == nullptr )  goto late_error;
					if ( dynamic_cast < Function::MereSymbol* > ( dd1->base .core ) == nullptr )
						goto late_error;
					if ( dd1->base .core != bf [0] .core )  goto late_error;
					if ( dd1->variable .core != xy [j] .core )  goto late_error;
					it_p ++;  assert ( it_p != fp->factors .end() );
					Function::DelayedDerivative * dd2 = dynamic_cast
						< Function::DelayedDerivative* > ( it_p->core );
					if ( dd2 == nullptr )  goto late_error;
					if ( dynamic_cast < Function::MereSymbol* > ( dd2->base .core ) == nullptr )
						goto late_error;
					if ( dd2->base .core != bf [1] .core )  goto late_error;
					if ( dd2->variable .core != xy [j] .core )  goto late_error;
					it_p ++;
					if ( it_p != fp->factors .end() )  goto late_error;
					j++;                                                                           }
				if ( j != n )  goto late_error;
				this->selector .push_back ( 2 + n + n*n );  // case 5
				continue;                                                                            }
			Function::Product * prod = dynamic_cast < Function::Product* > ( f .core );
			if ( prod == nullptr )  goto late_error;
			std::forward_list < Function > ::const_iterator itt = prod->factors .begin();
			assert ( itt != prod->factors .end() );
			ms = dynamic_cast < Function::MereSymbol* > ( itt->core );
			if ( ms )
			{	if ( ms != bf [0] .core )  goto late_error;
				itt++;  assert ( itt != prod->factors .end() );
				ms = dynamic_cast < Function::MereSymbol* > ( itt->core );
				if ( ms )
				{	if ( ms != bf [1] .core )  goto late_error;
					this->selector .push_back ( 1 );   // case 2
					continue;                                                }
				Function::DelayedDerivative * dd2 = dynamic_cast
					< Function::DelayedDerivative* > ( itt->core );
				if ( dd2 == nullptr )  goto late_error;
				itt++;
				if ( itt != prod->factors .end() )  goto late_error;
				// implementar case 6
				if ( dd2->base .core != bf [1] .core )  goto late_error;
				bool found = false;
				for ( size_t j = 0; j < n; j++ )
					if ( dd2->variable .core == xy [j] .core )
					{	this->selector .push_back ( 3 + n + n*n + j );  // case 6
						found = true;  break;                           }
				if ( not found )  goto late_error;
				continue;                                                                      }
			Function::DelayedDerivative * d1 = dynamic_cast
				< Function::DelayedDerivative* > ( itt->core );
			if ( d1 == nullptr )  goto late_error;
			itt++;  assert ( itt != prod->factors .end() );
			Function::DelayedDerivative * d2 = dynamic_cast
				< Function::DelayedDerivative* > ( itt->core );
			if ( d2 == nullptr )  goto late_error;
			itt++;
			if ( itt != prod->factors .end() )  goto late_error;
			if ( d1->base .core != bf [0] .core )  goto late_error;
			if ( d2->base .core != bf [1] .core )  goto late_error;
			Function::Core * v1 = d1->variable .core;
			Function::Core * v2 = d2->variable .core;
			bool not_found = true;
			for ( size_t j = 0; not_found and ( j < n ); j++ )
			for ( size_t k = 0; k < n; k++ )
				if ( ( v1 == xy [j] .core ) and ( v2 == xy [k] .core ) )
				{	this->selector .push_back ( 2 + n + j*n + k );   // case 4
					not_found = false;  break;                      }
			if ( not_found )  goto late_error;                                                       }
		return;                                                                                        }
	#ifndef NDEBUG  // DEBUG mode
	else
		this->pre_compute_tries +=
			"case 16 not implemented, try calling 'pre_compute' with subsets of integral expressions\n";
	#endif

	late_error :
	std::cout << "error in pre_compute" << std::endl;
	#ifndef NDEBUG  // DEBUG mode
	std::cout << this->info();
	#endif
	std::cout << "boolean variables at end of pre_compute : ";
	std::cout << int_psi << int_psi_psi << int_dpsi <<
		int_dpsi_dpsi << int_grad_grad << int_psi_dpsi << std::endl;
	
	early_error :
	std::cout << __FILE__ << ":" << __LINE__ << ": "
	          << __extension__ __PRETTY_FUNCTION__ << ": " << std::endl;
	std::cout << "pre_compute unsuccessful" << std::endl;
	exit ( 1 );

}  // end of  FiniteElement::StandAlone::TypeOne::Triangle::pre_compute

//------------------------------------------------------------------------------------------------------//


Cell::Numbering & FiniteElement::StandAlone::TypeOne::Triangle::build_global_numbering ( )
// virtual from FiniteElement::Core
{	assert ( false );
	return  this->numbers [1];  }

Cell::Numbering & FiniteElement::StandAlone::TypeOne::Quadrangle::build_global_numbering ( )
// virtual from FiniteElement::Core
{	assert ( false );
	return  this->numbers [1];  }

Cell::Numbering & FiniteElement::StandAlone::TypeOne::Parallelogram::build_global_numbering ( )
// virtual from FiniteElement::Core
{	assert ( false );
	return  this->numbers [1];  }

Cell::Numbering & FiniteElement::StandAlone::TypeOne::Rectangle::build_global_numbering ( )
// virtual from FiniteElement::Core
{	assert ( false );
	return  this->numbers [1];  }

Cell::Numbering & FiniteElement::StandAlone::TypeOne::Square::build_global_numbering ( )
// virtual from FiniteElement::Core
{	assert ( false );
	return  this->numbers [1];  }

Cell::Numbering & FiniteElement::StandAlone::TypeOne::Tetrahedron::build_global_numbering ( )
// virtual from FiniteElement::Core
{	assert ( false );
	return  this->numbers [1];  }

Cell::Numbering & FiniteElement::WithMaster::Segment::build_global_numbering ( )
// virtual from FiniteElement::Core
{	assert ( false );
	return  this->numbers [1];  }

Cell::Numbering & FiniteElement::WithMaster::Triangle::P1::build_global_numbering ( )
// virtual from FiniteElement::Core
{	assert ( false );
	return  this->numbers [1];  }

Cell::Numbering & FiniteElement::WithMaster::Triangle::P2::Straight::build_global_numbering ( )
// virtual from FiniteElement::Core
{	assert ( false );
	return  this->numbers [1];  }

Cell::Numbering & FiniteElement::WithMaster::Triangle::P2::Straight::Incremental
::build_global_numbering ( ) // virtual from FiniteElement::Core
{	assert ( false );
	return  this->numbers [1];  }

Cell::Numbering & FiniteElement::WithMaster::Triangle::P2::Curved::build_global_numbering ( )
// virtual from FiniteElement::Core
{	assert ( false );
	return  this->numbers [1];  }

Cell::Numbering & FiniteElement::WithMaster::Quadrangle::Q1::build_global_numbering ( )
// virtual from FiniteElement::Core
{	assert ( false );
	return  this->numbers [1];  }

Cell::Numbering & FiniteElement::WithMaster::Quadrangle::Q2::Straight::build_global_numbering ( )
// virtual from FiniteElement::Core
{	assert ( false );
	return  this->numbers [1];  }

Cell::Numbering & FiniteElement::WithMaster::Quadrangle::Q2::Straight::Incremental
::build_global_numbering ( ) // virtual from FiniteElement::Core
{	assert ( false );
	return  this->numbers [1];  }

Cell::Numbering & FiniteElement::WithMaster::Quadrangle::Q2::Curved::build_global_numbering ( )
// virtual from FiniteElement::Core
{	assert ( false );
	return  this->numbers [1];  }
	
	Cell::Numbering & FiniteElement::WithMaster::Tetrahedron::P1::build_global_numbering ( )
// virtual from FiniteElement::Core
{	assert ( false );
	return  this->numbers [1];  }

Cell::Numbering & FiniteElement::WithMaster::Hexahedron::Q1::build_global_numbering ( )
// virtual from FiniteElement::Core
{	assert ( false );
	return  this->numbers [1];  }


//------------------------------------------------------------------------------------------------------//


#ifndef NDEBUG  // DEBUG mode

std::string FiniteElement::WithMaster::info ( )
{	return this->info_string;  }


std::string FiniteElement::StandAlone::TypeOne::Triangle::info ( )

{	switch ( this->cas )
		
	{	case 0 :  return this->info_string + this->pre_compute_tries;

		case 1 :  // { int psi }
			return this->info_string + this->pre_compute_tries + "case 1, { int psi }\n" +
				"simple hand-computed arithmetic expressions\n";
		case 2 :  // { int psi1 * psi2 }
			return this->info_string + this->pre_compute_tries + "case 2, { int psi1 * psi2 }\n" +
				"simple hand-computed arithmetic expressions\n";
		case 3 :  // { int psi .deriv(x) }
			return this->info_string + this->pre_compute_tries + "case 3, { int psi .deriv(x) }\n" +
				"arithmetic expressions based on output of FFC, further optimized by hand\n";
		case 4 :  // { int psi1 .deriv(x) * psi2 .deriv(y) }
			return this->info_string + this->pre_compute_tries +
				"case 4, { int psi1 .deriv(x) * psi2 .deriv(y) }\n" +
				"arithmetic expressions based on output of FFC, further optimized by hand\n";
		case 5 :  // { int grad psi1 * grad psi2 }
			return this->info_string + this->pre_compute_tries +
				"case 5, { int grad psi1 * grad psi2 }\n" +
				"arithmetic expressions based on output of FFC, further optimized by hand\n";
		case 6 :  // { int psi * psi .deriv(x) }
			return this->info_string + this->pre_compute_tries +
				"case 6, { int psi1 * psi2 .deriv(x) }\n" +
				"arithmetic expressions based on output of FFC, further optimized by hand\n";
		case 7 :  // { int psi1, int psi1 * psi2 }
			return this->info_string + this->pre_compute_tries +
				"case 7, { int psi1, int psi1 * psi2 }\n" +
				"arithmetic expressions based on output of FFC, further optimized by hand\n";
		case 8 :  // { int psi, int psi .deriv(x) }
			return this->info_string + this->pre_compute_tries +
				"case 8, { int psi, int psi .deriv(x) }\n" +
				"arithmetic expressions based on output of FFC, further optimized by hand\n";
		case 9 :  // { int psi1, int psi1 .deriv(x) * psi2 .deriv(y) }
			return this->info_string + this->pre_compute_tries +
				"case 9, { int psi1, int psi1 .deriv(x) * psi2 .deriv(y) }\n" +
				"this case is not implemented yet\n";
		case 10 :  // { int psi1, int grad psi1 * grad psi2 }
			return this->info_string + this->pre_compute_tries +
				"case 10, { int psi1, int grad psi1 * grad psi2 }\n" +
				"this case is not implemented yet\n" +
				"try calling 'pre_compute' with subsets of integral expressions\n";
		case 11 :  // { int psi .deriv(x), int psi1 .deriv(x) * psi2 .deriv(y) }
			return this->info_string + this->pre_compute_tries +
				"case 11, { int psi .deriv(x), int psi1 .deriv(x) * psi2 .deriv(y) }\n" +
				"arithmetic expressions based on output of FFC, further optimized by hand\n";
		case 12 :  // { int psi1 * psi2, int psi1 .deriv(x) * psi2 .deriv(y) }
			return this->info_string + this->pre_compute_tries +
				"case 12, { int psi1 * psi2, int psi1 .deriv(x) * psi2 .deriv(y) }\n" +
				"arithmetic expressions based on output of FFC, further optimized by hand\n";
		case 13 :  // { int psi1 * psi2, int grad psi1 * grad psi2 }
			return this->info_string + this->pre_compute_tries +
				"case 13, { int psi1 * psi2, int grad psi1 * grad psi2 }\n" +
				"arithmetic expressions based on output of FFC, further optimized by hand\n";
		case 14 :  // 1, 2, 3 and 4
			return this->info_string + this->pre_compute_tries + "case 14 (1234)\n" +
				"arithmetic expressions based on output of FFC, further optimized by hand\n";
		case 15 :  // 1, 2, 3, 4 and 5
			return this->info_string + this->pre_compute_tries + "case 15 (12345)\n" +
				"arithmetic expressions based on output of FFC, further optimized by hand\n";
		case 16 :  // everything
			return this->info_string + this->pre_compute_tries + "case 16 (123456)\n" +
				"arithmetic expressions based on output of FFC, further optimized by hand\n";

		default :
			assert ( false );                                                                        }  }


std::string FiniteElement::StandAlone::TypeOne::Quadrangle::info ( )

{	switch ( this->cas )
		
	{	case 0 :  return this->info_string + this->pre_compute_tries;

		case 1 :  // { int psi }
			return this->info_string + this->pre_compute_tries + "case 1, { int psi }\n" +
				"this case is not implemented yet\n";
		case 2 :  // { int psi1 * psi2 }
			return this->info_string + this->pre_compute_tries + "case 2, { int psi1 * psi2 }\n" +
				"this case is not implemented yet\n";
		case 3 :  // { int psi .deriv(x) }
			return this->info_string + this->pre_compute_tries + "case 3, { int psi .deriv(x) }\n" +
				"arithmetic expressions based on output of FFC, further optimized by hand\n";
		case 4 :  // { int psi1 .deriv(x) * psi2 .deriv(y) }
			return this->info_string + this->pre_compute_tries +
				"case 4, { int psi1 .deriv(x) * psi2 .deriv(y) }\n" +
				"arithmetic expressions based on output of FFC, further optimized by hand\n";
		case 5 :  // { int grad psi1 * grad psi2 }
			return this->info_string + this->pre_compute_tries +
				"case 5, { int grad psi1 * grad psi2 }\n" +
				"this case is not implemented yet\n";
		case 6 :  // { int psi * psi .deriv(x) }
			return this->info_string + this->pre_compute_tries +
				"case 6, { int psi1 * psi2 .deriv(x) }\n" +
				"this case is not implemented yet\n";
		case 7 :  // { int psi1, int psi1 * psi2 }
			return this->info_string + this->pre_compute_tries +
				"case 7, { int psi1, int psi1 * psi2 }\n" +
				"this case is not implemented yet\n" +
				"try calling 'pre_compute' with subsets of integral expressions\n";
		case 8 :  // { int psi, int psi .deriv(x) }
			return this->info_string + this->pre_compute_tries +
				"case 8, { int psi, int psi .deriv(x) }\n" +
				"this case is not implemented yet\n" +
				"try calling 'pre_compute' with subsets of integral expressions\n";
		case 9 :  // { int psi1, int psi1 .deriv(x) * psi2 .deriv(y) }
			return this->info_string + this->pre_compute_tries + 
				"case 9, { int psi1, int psi1 .deriv(x) * psi2 .deriv(y) }\n" +
				"this case is not implemented yet\n" +
				"try calling 'pre_compute' with subsets of integral expressions\n";
		case 10 :  // { int psi1, int grad psi1 * grad psi2 }
			return this->info_string + this->pre_compute_tries + 
				"case 10, { int psi1, int grad psi1 * grad psi2 }\n" +
				"this case is not implemented yet\n" +
				"try calling 'pre_compute' with subsets of integral expressions\n";
		case 11 :  // { int psi .deriv(x), int psi1 .deriv(x) * psi2 .deriv(y) }
			return this->info_string + this->pre_compute_tries +
				"case 11, { int psi .deriv(x), int psi1 .deriv(x) * psi2 .deriv(y) }\n" +
				"this case is not implemented yet\n" +
				"try calling 'pre_compute' with subsets of integral expressions\n";
		case 12 :  // { int psi1 * psi2, int psi1 .deriv(x) * psi2 .deriv(y) }
			return this->info_string + this->pre_compute_tries +
				"case 12, { int psi1 * psi2, int psi1 .deriv(x) * psi2 .deriv(y) }\n" +
				"this case is not implemented yet\n" +
				"try calling 'pre_compute' with subsets of integral expressions\n";
		case 13 :  // { int psi1 * psi2, int grad psi1 * grad psi2 }
			return this->info_string + this->pre_compute_tries +
				"case 13, { int psi1 * psi2, int grad psi1 * grad psi2 }\n" +
				"this case is not implemented yet\n" +
				"try calling 'pre_compute' with subsets of integral expressions\n";
		case 14 :  // 1, 2, 3 and 4
			return this->info_string + this->pre_compute_tries + "case 14 (1234)\n" +
				"this case is not implemented yet\n" +
				"try calling 'pre_compute' with subsets of integral expressions\n";
		case 15 :  // 1, 2, 3, 4 and 5
			return this->info_string + this->pre_compute_tries + "case 15 (12345)\n" +
				"this case is not implemented yet\n" +
				"try calling 'pre_compute' with subsets of integral expressions\n";
		case 16 :  // everything
			return this->info_string + this->pre_compute_tries + "case 16 (123456)\n" +
				"this case is not implemented yet\n" +
				"try calling 'pre_compute' with subsets of integral expressions\n";

		default :
			assert ( false );                                                                        }  }


std::string FiniteElement::StandAlone::TypeOne::Parallelogram::info ( )

{	switch ( this->cas )
		
	{	case 0 :  return this->info_string + this->pre_compute_tries;

		default : return this->info_string + "integrator not implemented yet\n";   }  }


std::string FiniteElement::StandAlone::TypeOne::Rectangle::info ( )

{	switch ( this->cas )
		
	{	case 0 :  return this->info_string + this->pre_compute_tries;

		case 1 :  // { int psi }
			return this->info_string + this->pre_compute_tries + "case 1, { int psi }\n" +
				"simple hand-computed arithmetic expressions\n";
		case 2 :  // { int psi1 * psi2 }
			return this->info_string + this->pre_compute_tries + "case 2, { int psi1 * psi2 }\n" +
				"simple hand-computed arithmetic expressions\n";
		case 3 :  // { int psi .deriv(x) }
			return this->info_string + this->pre_compute_tries + "case 3, { int psi .deriv(x) }\n" +
				"simple hand-computed arithmetic expressions\n";
		case 4 :  // { int psi1 .deriv(x) * psi2 .deriv(y) }
			return this->info_string + this->pre_compute_tries +
				"case 4, { int psi1 .deriv(x) * psi2 .deriv(y) }\n" +
				"simple hand-computed arithmetic expressions\n";
		case 5 :  // { int grad psi1 * grad psi2 }
			return this->info_string + this->pre_compute_tries +
				"case 5, { int grad psi1 * grad psi2 }\n" +
				"simple hand-computed arithmetic expressions\n";
		case 6 :  // { int psi * psi .deriv(x) }
			return this->info_string + this->pre_compute_tries +
				"case 6, { int psi1 * psi2 .deriv(x) }\n" +
				"hand-computed arithmetic expressions\n";
		case 7 :  // { int psi1, int psi1 * psi2 }
			return this->info_string + this->pre_compute_tries +
				"case 7, { int psi1, int psi1 * psi2 }\n" +
				"simple hand-computed arithmetic expressions\n";
		case 8 :  // { int psi, int psi .deriv(x) }
			return this->info_string + this->pre_compute_tries +
				"case 8, { int psi, int psi .deriv(x) }\n" +
				"simple hand-computed arithmetic expressions\n";
		case 9 :  // { int psi1, int psi1 .deriv(x) * psi2 .deriv(y) }
			return this->info_string + this->pre_compute_tries +
				"case 9, { int psi1, int psi1 .deriv(x) * psi2 .deriv(y) }\n" +
				"simple hand-computed arithmetic expressions\n";
		case 10 :  // { int psi1, int grad psi1 * grad psi2 }
			return this->info_string + this->pre_compute_tries +
				"case 10, { int psi1, int grad psi1 * grad psi2 }\n" +
				"simple hand-computed arithmetic expressions\n";
		case 11 :  // { int psi .deriv(x), int psi1 .deriv(x) * psi2 .deriv(y) }
			return this->info_string + this->pre_compute_tries +
				"case 11, { int psi .deriv(x), int psi1 .deriv(x) * psi2 .deriv(y) }\n" +
				"simple hand-computed arithmetic expressions\n";
		case 12 :  // { int psi1 * psi2, int psi1 .deriv(x) * psi2 .deriv(y) }
			return this->info_string + this->pre_compute_tries +
				"case 12, { int psi1 * psi2, int psi1 .deriv(x) * psi2 .deriv(y) }\n" +
				"simple hand-computed arithmetic expressions\n";
		case 13 :  // { int psi1 * psi2, int grad psi1 * grad psi2 }
			return this->info_string + this->pre_compute_tries +
				"case 13, { int psi1 * psi2, int grad psi1 * grad psi2 }\n" +
				"simple hand-computed arithmetic expressions\n";
		case 14 :  // 1, 2, 3 and 4
			return this->info_string + this->pre_compute_tries + "case 14 (1234)\n" +
				"simple hand-computed arithmetic expressions\n";
		case 15 :  // 1, 2, 3, 4 and 5
			return this->info_string + this->pre_compute_tries + "case 15 (12345)\n" +
				"this case is not implemented yet\n" +
				"try calling 'pre_compute' with subsets of integral expressions\n";
		case 16 :  // everything
			return this->info_string + this->pre_compute_tries + "case 16 (123456)\n" +
				"this case is not implemented yet\n" +
				"try calling 'pre_compute' with subsets of integral expressions\n";

		default :
			assert ( false );                                                                        }  }


std::string FiniteElement::StandAlone::TypeOne::Square::info ( )

{	switch ( this->cas )
		
	{	case 0 :  return this->info_string + this->pre_compute_tries;

		case 1 :  // { int psi }
			return this->info_string + this->pre_compute_tries + "case 1, { int psi }\n" +
				"simple hand-computed arithmetic expressions\n";
		case 2 :  // { int psi1 * psi2 }
			return this->info_string + this->pre_compute_tries + "case 2, { int psi1 * psi2 }\n" +
				"simple hand-computed arithmetic expressions\n";
		case 3 :  // { int psi .deriv(x) }
			return this->info_string + this->pre_compute_tries + "case 3, { int psi .deriv(x) }\n" +
				"simple hand-computed arithmetic expressions\n";
		case 4 :  // { int psi1 .deriv(x) * psi2 .deriv(y) }
			return this->info_string + this->pre_compute_tries +
				"case 4, { int psi1 .deriv(x) * psi2 .deriv(y) }\n" +
				"simple hand-computed arithmetic expressions\n";
		case 5 :  // { int grad psi1 * grad psi2 }
			return this->info_string + this->pre_compute_tries +
				"case 5, { int grad psi1 * grad psi2 }\n" +
				"simple hand-computed arithmetic expressions\n";
		case 6 :  // { int psi * psi .deriv(x) }
			return this->info_string + this->pre_compute_tries +
				"case 6, { int psi1 * psi2 .deriv(x) }\n" +
				"simple hand-computed arithmetic expressions\n";
		case 7 :  // { int psi1, int psi1 * psi2 }
			return this->info_string + this->pre_compute_tries +
				"case 7, { int psi1, int psi1 * psi2 }\n" +
				"this case is not implemented yet\n" +
				"try calling 'pre_compute' with subsets of integral expressions\n";
		case 8 :  // { int psi, int psi .deriv(x) }
			return this->info_string + this->pre_compute_tries +
				"case 8, { int psi, int psi .deriv(x) }\n" +
				"this case is not implemented yet\n" +
				"try calling 'pre_compute' with subsets of integral expressions\n";
		case 9 :  // { int psi1, int psi1 .deriv(x) * psi2 .deriv(y) }
			return this->info_string + this->pre_compute_tries +
				"case 9, { int psi1, int psi1 .deriv(x) * psi2 .deriv(y) }\n" +
				"this case is not implemented yet\n" +
				"try calling 'pre_compute' with subsets of integral expressions\n";
		case 10 :  // { int psi1, int grad psi1 * grad psi2 }
			return this->info_string + this->pre_compute_tries +
				"case 10, { int psi1, int grad psi1 * grad psi2 }\n" +
				"this case is not implemented yet\n" +
				"try calling 'pre_compute' with subsets of integral expressions\n";
		case 11 :  // { int psi .deriv(x), int psi1 .deriv(x) * psi2 .deriv(y) }
			return this->info_string + this->pre_compute_tries +
				"case 11, { int psi .deriv(x), int psi1 .deriv(x) * psi2 .deriv(y) }\n" +
				"this case is not implemented yet\n" +
				"try calling 'pre_compute' with subsets of integral expressions\n";
		case 12 :  // { int psi1 * psi2, int psi1 .deriv(x) * psi2 .deriv(y) }
			return this->info_string + this->pre_compute_tries +
				"case 12, { int psi1 * psi2, int psi1 .deriv(x) * psi2 .deriv(y) }\n" +
				"this case is not implemented yet\n" +
				"try calling 'pre_compute' with subsets of integral expressions\n";
		case 13 :  // { int psi1 * psi2, int grad psi1 * grad psi2 }
			return this->info_string + this->pre_compute_tries +
				"case 13, { int psi1 * psi2, int grad psi1 * grad psi2 }\n" +
				"this case is not implemented yet\n" +
				"try calling 'pre_compute' with subsets of integral expressions\n";
		case 14 :  // 1, 2, 3 and 4
			return this->info_string + this->pre_compute_tries + "case 14 (1234)\n" +
				"this case is not implemented yet\n" +
				"try calling 'pre_compute' with subsets of integral expressions\n";
		case 15 :  // 1, 2, 3, 4 and 5
			return this->info_string + this->pre_compute_tries + "case 15 (12345)\n" +
				"this case is not implemented yet\n" +
				"try calling 'pre_compute' with subsets of integral expressions\n";
		case 16 :  // everything
			return this->info_string + this->pre_compute_tries + "case 16 (123456)\n" +
				"this case is not implemented yet\n" +
				"try calling 'pre_compute' with subsets of integral expressions\n";

		default :
			assert ( false );                                                                        }  }


std::string FiniteElement::StandAlone::TypeOne::Tetrahedron::info ( )

{	switch ( this->cas )
		
	{	case 0 :  return this->info_string + this->pre_compute_tries;

		case 1 :  // { int psi }
			return this->info_string + this->pre_compute_tries + "case 1, { int psi }\n" +
				"arithmetic expressions based on output of FFC, further optimized by hand\n";
		case 2 :  // { int psi1 * psi2 }
			return this->info_string + this->pre_compute_tries + "case 2, { int psi1 * psi2 }\n" +
				"arithmetic expressions based on output of FFC, further optimized by hand\n";
		case 3 :  // { int psi .deriv(x) }
			return this->info_string + this->pre_compute_tries + "case 3, { int psi .deriv(x) }\n" +
				"arithmetic expressions based on output of FFC, further optimized by hand\n";
		case 4 :  // { int psi1 .deriv(x) * psi2 .deriv(y) }
			return this->info_string + this->pre_compute_tries +
				"case 4, { int psi1 .deriv(x) * psi2 .deriv(y) }\n" +
				"arithmetic expressions based on output of FFC, further optimized by hand\n";
		case 5 :  // { int grad psi1 * grad psi2 }
			return this->info_string + this->pre_compute_tries +
				"case 5, { int grad psi1 * grad psi2 }\n" +
				"arithmetic expressions based on output of FFC, further optimized by hand\n";
		case 6 :  // { int psi * psi .deriv(x) }
			return this->info_string + this->pre_compute_tries +
				"case 6, { int psi1 * psi2 .deriv(x) }\n" +
				"this case is not implemented yet\n";
		case 7 :  // { int psi1, int psi1 * psi2 }
			return this->info_string + this->pre_compute_tries +
				"case 7, { int psi1, int psi1 * psi2 }\n" +
				"arithmetic expressions based on output of FFC, further optimized by hand\n";
		case 8 :  // { int psi, int psi .deriv(x) }
			return this->info_string + this->pre_compute_tries +
				"case 8, { int psi, int psi .deriv(x) }\n" +
				"arithmetic expressions based on output of FFC, further optimized by hand\n";
		case 9 :  // { int psi1, int psi1 .deriv(x) * psi2 .deriv(y) }
			return this->info_string + this->pre_compute_tries +
				"case 9, { int psi1, int psi1 .deriv(x) * psi2 .deriv(y) }\n" +
				"arithmetic expressions based on output of FFC, further optimized by hand\n";
		case 10 :  // { int psi1, int grad psi1 * grad psi2 }
			return this->info_string + this->pre_compute_tries +
				"case 10, { int psi1, int grad psi1 * grad psi2 }\n" +
				"arithmetic expressions based on output of FFC, further optimized by hand\n";
		case 11 :  // { int psi .deriv(x), int psi1 .deriv(x) * psi2 .deriv(y) }
			return this->info_string + this->pre_compute_tries +
				"case 11, { int psi .deriv(x), int psi1 .deriv(x) * psi2 .deriv(y) }\n" +
				"arithmetic expressions based on output of FFC, further optimized by hand\n";
		case 12 :  // { int psi1 * psi2, int psi1 .deriv(x) * psi2 .deriv(y) }
			return this->info_string + this->pre_compute_tries +
				"case 12, { int psi1 * psi2, int psi1 .deriv(x) * psi2 .deriv(y) }\n" +
				"arithmetic expressions based on output of FFC, further optimized by hand\n";
		case 13 :  // { int psi1 * psi2, int grad psi1 * grad psi2 }
			return this->info_string + this->pre_compute_tries +
				"case 13, { int psi1 * psi2, int grad psi1 * grad psi2 }\n" +
				"arithmetic expressions based on output of FFC, further optimized by hand\n";
		case 14 :  // 1, 2, 3 and 4
			return this->info_string + this->pre_compute_tries + "case 14 (1234)\n" +
				"arithmetic expressions based on output of FFC, further optimized by hand\n";
		case 15 :  // 1, 2, 3, 4 and 5
			return this->info_string + this->pre_compute_tries + "case 15 (12345)\n" +
				"this case is not implemented yet\n" +
				"try calling 'pre_compute' with subsets of integral expressions\n";
		case 16 :  // everything
			return this->info_string + this->pre_compute_tries + "case 16 (123456)\n" +
				"this case is not implemented yet\n" +
				"try calling 'pre_compute' with subsets of integral expressions\n";

		default :
			assert ( false );                                                                        }  }

#endif  // DEBUG

//------------------------------------------------------------------------------------------------------//

#endif  // ifndef MANIFEM_NO_FEM
