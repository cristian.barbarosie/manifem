
//   manifold.cpp  2024.10.04

//   This file is part of maniFEM, a C++ library for meshes and finite elements on manifolds.

//   Copyright  2019 - 2024  Cristian Barbarosie  cristian.barbarosie@gmail.com

//   https://maniFEM.rd.ciencias.ulisboa.pt/
//   https://codeberg.org/cristian.barbarosie/maniFEM

//   ManiFEM is free software: you can redistribute it and/or modify it
//   under the terms of the GNU Lesser General Public License as published
//   by the Free Software Foundation, either version 3 of the License
//   or (at your option) any later version.

//   ManiFEM is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//   See the GNU Lesser General Public License for more details.

//   You should have received a copy of the GNU Lesser General Public License
//   along with maniFEM.  If not, see <https://www.gnu.org/licenses/>.


#include "manifold.h"
#include "manifold-xx.h"

using namespace maniFEM;

	
Manifold Manifold::working ( tag::non_existent );
// anything would do, the user must set this variable before anything else
// by simply declaring a Manifold object (constructor assigns to Manifold::working)

//------------------------------------------------------------------------------------------------------//


const std::vector < std::vector < std::vector < short int > > >
	tag::Util::ortho_basis_int { { }, { { 1 } }, { { 1, 0 }, { 0, 1 } },
	                             { { 1, 0, 0 }, { 0, 1, 0 }, { 0, 0, 1 } } },
	tag::Util::pm_ortho_basis_int { { }, { { 1 }, { -1 } },
		{ { 1, 0 }, { 0, 1 }, { -1, 0 }, { 0, -1 } },
		{ { 1, 0, 0 }, { 0, 1, 0 }, { 0, 0, 1 }, { -1, 0, 0 }, { 0, -1, 0 }, { 0, 0, -1 } } },
	tag::Util::directions_int { { }, { { 1 }, { -1 } },
		{ { 1, 0 }, { 0, 1 }, { -1, 0 }, { 0, -1 }, { 1, 1 }, { 1, -1 }, { -1, -1 }, { -1, 1 } },
		{ { 1, 0, 0 }, { 0, 1, 0 }, { 0, 0, 1 }, { -1, 0, 0 }, { 0, -1, 0 }, { 0, 0, -1 },
		  { 1, 1, 0 }, { 0, 1, 1 }, { 1, 0, 1 }, { -1, -1, 0 }, { 0, -1, -1 }, { -1, 0, -1 },
		  { 1, -1, 0 }, { 0, 1, -1 }, { 1, 0, -1 }, { -1, 1, 0 }, { 0, -1, 1 }, { -1, 0, 1 },
		  { 1, 1, 1 }, { -1, 1, 1 }, { 1, -1, 1 }, { 1, 1, -1 },
		  { 1, -1, -1 }, { -1, 1, -1 }, { -1, -1, 1 }, { -1, -1, -1 }                        } };

const double tag::Util::one_third = 1. / 3.,
             tag::Util::minus_one_third = - tag::Util::one_third,
             tag::Util::one_sixth = 1. / 6.,
             tag::Util::minus_one_sixth = - tag::Util::one_sixth,
             tag::Util::two_thirds = 2. / 3.,
             tag::Util::minus_two_thirds = - tag::Util::two_thirds,
             tag::Util::sqrt_2 = std::sqrt (2.),
             tag::Util::sqrt_half = 1./ tag::Util::sqrt_2,
             tag::Util::sqrt_3 = std::sqrt (3.),
             tag::Util::sqrt_one_third = 1./ tag::Util::sqrt_3,
             tag::Util::sqrt_two_thirds = std::sqrt ( tag::Util::two_thirds ),
             tag::Util::sqrt_sixth = tag::Util::sqrt_two_thirds / 2.,
             tag::Util::sqrt_three_quarters = tag::Util::sqrt_3 / 2.,
             tag::Util::sqrt_1_over_24 = tag::Util::sqrt_two_thirds / 4.,
             tag::Util::fourth_root_three_quarters = std::sqrt ( tag::Util::sqrt_three_quarters );

const std::vector < std::vector < std::vector < double > > >
	tag::Util::ortho_basis_double { { }, { { 1. } }, { { 1., 0. }, { 0., 1. } },
	                                { { 1., 0., 0. }, { 0., 1., 0 }, { 0., 0., 1. } } },
	tag::Util::pm_ortho_basis_double { { }, { { 1. }, { -1. } },
		{ { 1., 0. }, { 0., 1. }, { -1., 0. }, { 0., -1. } },
		{ { 1., 0., 0. }, { 0., 1., 0 }, { 0., 0., 1. }, { -1., 0., 0. }, { 0., -1., 0 }, { 0., 0., -1. } } },
	tag::Util::directions_double { { }, { { 1. }, { -1. } },
		{ { 1., 0. },  {  tag::Util::sqrt_half,  tag::Util::sqrt_half },
		  { 0., 1. },  { -tag::Util::sqrt_half,  tag::Util::sqrt_half },
		  { -1., 0. }, { -tag::Util::sqrt_half, -tag::Util::sqrt_half },
		  { 0., -1. }, {  tag::Util::sqrt_half, -tag::Util::sqrt_half } },
		{ { 1., 0., 0. }, { -1., 0., 0. }, { 0., 1., 0. }, 
		  { 0., -1., 0. }, { 0., 0., 1. }, { 0., 0., -1. },
		  { tag::Util::sqrt_half, tag::Util::sqrt_half, 0. },
		  { tag::Util::sqrt_half, -tag::Util::sqrt_half, 0. },
		  { -tag::Util::sqrt_half, tag::Util::sqrt_half, 0. },
		  { -tag::Util::sqrt_half, -tag::Util::sqrt_half, 0. },
		  { tag::Util::sqrt_half, 0., tag::Util::sqrt_half },
		  { tag::Util::sqrt_half, 0., -tag::Util::sqrt_half },
		  { -tag::Util::sqrt_half, 0., tag::Util::sqrt_half },
		  { -tag::Util::sqrt_half, 0., -tag::Util::sqrt_half },
		  { 0., tag::Util::sqrt_half, tag::Util::sqrt_half },
		  { 0., tag::Util::sqrt_half, -tag::Util::sqrt_half },
		  { 0., -tag::Util::sqrt_half, tag::Util::sqrt_half },
		  { 0., -tag::Util::sqrt_half, -tag::Util::sqrt_half },
		  {  tag::Util::sqrt_one_third,  tag::Util::sqrt_one_third,  tag::Util::sqrt_one_third },
		  {  tag::Util::sqrt_one_third,  tag::Util::sqrt_one_third, -tag::Util::sqrt_one_third },
		  {  tag::Util::sqrt_one_third, -tag::Util::sqrt_one_third,  tag::Util::sqrt_one_third },
		  {  tag::Util::sqrt_one_third, -tag::Util::sqrt_one_third, -tag::Util::sqrt_one_third },
		  { -tag::Util::sqrt_one_third,  tag::Util::sqrt_one_third,  tag::Util::sqrt_one_third },
		  { -tag::Util::sqrt_one_third,  tag::Util::sqrt_one_third, -tag::Util::sqrt_one_third },
		  { -tag::Util::sqrt_one_third, -tag::Util::sqrt_one_third,  tag::Util::sqrt_one_third },
		  { -tag::Util::sqrt_one_third, -tag::Util::sqrt_one_third, -tag::Util::sqrt_one_third } } },
	tag::Util::directions_few_double { { }, { { 1. } },
		{ { 1., 0. },  {  tag::Util::sqrt_half,  tag::Util::sqrt_half },
		  { 0., 1. },  { -tag::Util::sqrt_half,  tag::Util::sqrt_half } },
		{ { 1., 0., 0. }, { 0., 1., 0. }, { 0., 0., 1. },
		  { tag::Util::sqrt_half, tag::Util::sqrt_half, 0. },
		  { tag::Util::sqrt_half, -tag::Util::sqrt_half, 0. },
		  { tag::Util::sqrt_half, 0., tag::Util::sqrt_half },
		  { tag::Util::sqrt_half, 0., -tag::Util::sqrt_half },
		  { 0., tag::Util::sqrt_half, tag::Util::sqrt_half },
		  { 0., tag::Util::sqrt_half, -tag::Util::sqrt_half },
		  {  tag::Util::sqrt_one_third,  tag::Util::sqrt_one_third,  tag::Util::sqrt_one_third },
		  {  tag::Util::sqrt_one_third,  tag::Util::sqrt_one_third, -tag::Util::sqrt_one_third },
		  {  tag::Util::sqrt_one_third, -tag::Util::sqrt_one_third,  tag::Util::sqrt_one_third },
		  {  tag::Util::sqrt_one_third, -tag::Util::sqrt_one_third, -tag::Util::sqrt_one_third } } },
	tag::Util::directions_L_infty { { }, { { 1. } },
		{ { 1., 0. },  { 1., 1. }, { 0., 1. },  { -1., 1. } },
		{ { 1., 0., 0. }, { 0., 1., 0. }, { 0., 0., 1. }, { 1., 1., 0. }, { 1., -1., 0. },
		  { 1., 0., 1. }, { 1., 0., -1. }, { 0., 1., 1. }, { 0., 1., -1. },
		  {  1.,  1.,  1. }, {  1.,  1., -1. }, {  1., -1.,  1. }, {  1., -1., -1. }      } };

//------------------------------------------------------------------------------------------------------//


tag::Util::MetricCore::~MetricCore ( ) { }  // virtual

tag::Util::Metric::Trivial::~Trivial ( ) { }  // virtual from tag::Util::Metric::Core

#ifndef MANIFEM_NO_FRONTAL

tag::Util::Metric::Constant::~Constant ( ) { }  // virtual from tag::Util::Metric::Core

tag::Util::Metric::Constant::Isotropic::~Isotropic ( ) { }  // virtual from tag::Util::Metric::Core

tag::Util::Metric::Constant::Matrix::~Matrix ( ) { }  // virtual from tag::Util::Metric::Core

tag::Util::Metric::Constant::Matrix::Simple::~Simple ( ) { }  // virtual from tag::Util::Metric::Core

tag::Util::Metric::Constant::Matrix::SquareRoot::~SquareRoot ( ) { }
// virtual from tag::Util::Metric::Core

tag::Util::Metric::Variable::~Variable ( ) { }  // virtual from tag::Util::Metric::Core

tag::Util::Metric::Variable::Isotropic::~Isotropic ( ) { }  // virtual from tag::Util::Metric::Core

tag::Util::Metric::Variable::Matrix::~Matrix ( ) { }  // virtual from tag::Util::Metric::Core

tag::Util::Metric::Variable::Matrix::Simple::~Simple ( ) { }  // virtual from tag::Util::Metric::Core

tag::Util::Metric::Variable::Matrix::SquareRoot::~SquareRoot ( ) { }
// virtual from tag::Util::Metric::Core

tag::Util::Metric::Variable::Matrix::ISR::~ISR ( ) { }  // virtual from tag::Util::Metric::Core

tag::Util::Metric::Variable::Matrix::SquareRoot::ISR::~ISR ( ) { }
// virtual from tag::Util::Metric::Core

tag::Util::Metric::ScaledSq::~ScaledSq ( ) { }  // virtual from tag::Util::Metric

tag::Util::Metric::ScaledSqInv::~ScaledSqInv ( ) { }  // virtual from tag::Util::Metric

#endif  // ifndef MANIFEM_NO_FRONTAL

//------------------------------------------------------------------------------------------------------//


tag::Util::MetricCore * tag::Util::Metric::Trivial::clone ( )  // virtual from tag::Util::Metric::Core
{	return  new tag::Util::Metric::Trivial;  }

#ifndef MANIFEM_NO_FRONTAL

tag::Util::MetricCore * tag::Util::Metric::Constant::Isotropic::clone ( )
// virtual from tag::Util::Metric::Core
{	return  new tag::Util::Metric::Constant::Isotropic ( this->zoom );  }


tag::Util::MetricCore * tag::Util::Metric::Constant::Matrix::Simple::clone ( )
// virtual from tag::Util::Metric::Core
{	return  new tag::Util::Metric::Constant::Matrix::Simple ( this->matrix, this->isr );  }


tag::Util::MetricCore * tag::Util::Metric::Constant::Matrix::SquareRoot::clone ( )
// virtual from tag::Util::Metric::Core, defined by tag::Util::Metric::Constant::Matrix
// here overridden
{	return  new tag::Util::Metric::Constant::Matrix::SquareRoot ( this->matrix, this->isr );  }


tag::Util::MetricCore * tag::Util::Metric::Variable::Isotropic::clone ( )
// virtual from tag::Util::Metric::Core
{	return  new tag::Util::Metric::Variable::Isotropic ( this->zoom );  }


tag::Util::MetricCore * tag::Util::Metric::Variable::Matrix::Simple::clone ( )
// virtual from tag::Util::Metric::Core
{	return  new tag::Util::Metric::Variable::Matrix::Simple ( this->matrix );  }


tag::Util::MetricCore * tag::Util::Metric::Variable::Matrix::SquareRoot::clone ( )
// virtual from tag::Util::Metric::Core, defined by tag::Util::Metric::Variable::Matrix,
// here overridden
{	return  new tag::Util::Metric::Variable::Matrix::SquareRoot ( this->matrix );  }


tag::Util::MetricCore * tag::Util::Metric::Variable::Matrix::ISR::clone ( )
// virtual from tag::Util::Metric::Core, defined by tag::Util::Metric::Variable::Matrix,
// here overridden
{	return  new tag::Util::Metric::Variable::Matrix::ISR ( this->isr, this->matrix );  }


tag::Util::MetricCore * tag::Util::Metric::Variable::Matrix::SquareRoot::ISR::clone ( )
// virtual from tag::Util::Metric::Core, defined by tag::Util::Metric::Variable::Matrix,
// here overridden
{	return  new tag::Util::Metric::Variable::Matrix::SquareRoot::ISR ( this->isr, this->matrix );  }


tag::Util::MetricCore * tag::Util::Metric::ScaledSq::clone ( )
// virtual from tag::Util::Metric::Core
{	return  new tag::Util::Metric::ScaledSq ( this->base, this->zoom );  }


tag::Util::MetricCore * tag::Util::Metric::ScaledSqInv::clone ( )
// virtual from tag::Util::Metric::Core
{	return  new tag::Util::Metric::ScaledSqInv ( this->base, this->zoom );  }

#endif  // ifndef MANIFEM_NO_FRONTAL

//------------------------------------------------------------------------------------------------------//


// 'regist' is somewhat similar to 'clone' but for ScaledSq(Inv) goes down to the base

// only meaningful for metrics which interact with the container in frontal.cpp :
// classes derived from 'tag::Util::Metric::Variable::Matrix' (but not all of them)

tag::Util::MetricCore * tag::Util::Metric::Core::regist ( )
// overridden for Variable::Matrix, ScaledSq and ScaledSqInv
{	assert ( false );  return nullptr;  }


void tag::Util::Metric::Core::de_activate ( )   // virtual
{	assert ( false );  }


#ifndef MANIFEM_NO_FRONTAL

tag::Util::MetricCore * tag::Util::Metric::Variable::Matrix::regist ( )
// virtual from tag::Util::Metric::Core, here overridden
{	return  this->clone();  }


tag::Util::MetricCore * tag::Util::Metric::Variable::Matrix::SquareRoot::ISR::regist ( )
// virtual from tag::Util::Metric::Core, here overridden a second time
{	return  this->clone();  }


tag::Util::MetricCore * tag::Util::Metric::ScaledSq::regist ( )
// virtual from tag::Util::Metric::Core, here overridden
{	assert ( this->base .core );
	return  this->base .core->regist();  }
// should be equivalent to 'return this->base->clone()'


tag::Util::MetricCore * tag::Util::Metric::ScaledSqInv::regist ( )
// virtual from tag::Util::Metric::Core, here overridden
{	assert ( this->base .core );
	return  this->base .core->regist();  }
// should be equivalent to 'return this->base->clone()'

#endif  // ifndef MANIFEM_NO_FRONTAL

//------------------------------------------------------------------------------------------------------//


#ifndef MANIFEM_NO_FRONTAL

void Manifold::set_metric ( const tag::Isotropic &, const double s )
{	assert ( this->core );
	this->core->metric =
		tag::Util::Metric ( tag::whose_core_is, new tag::Util::Metric::Constant::Isotropic (s) );  }


void Manifold::set_metric ( const tag::Isotropic &, const Function & s )
{	assert ( this->core );
	this->core->metric =
		tag::Util::Metric ( tag::whose_core_is, new tag::Util::Metric::Variable::Isotropic (s) );  }


void Manifold::set_metric ( const tag::M11M12M22 &,
	                                 const double m11, const double m12, const double m22 )
{	assert ( this->core );
	this->core->metric = tag::Util::Metric ( tag::whose_core_is,
		new tag::Util::Metric::Constant::Matrix::Simple ( m11, m12, m22 ) );  }


void Manifold::set_metric ( const tag::M11M12M22 &,
	                                 const Function & m11, const Function & m12, const Function & m22 )
{	assert ( this->core );
	this->core->metric = tag::Util::Metric ( tag::whose_core_is,
		new tag::Util::Metric::Variable::Matrix::Simple ( m11, m12, m22 ) );  }


void Manifold::set_metric ( const tag::SquareRoot &, const tag::M11M12M22 &,
	                                 const double m11, const double m12, const double m22 )
{	assert ( this->core );
	this->core->metric = tag::Util::Metric ( tag::whose_core_is,
		new tag::Util::Metric::Constant::Matrix::SquareRoot ( m11, m12, m22 ) );  }


void Manifold::set_metric ( const tag::SquareRoot &, const tag::M11M12M22 &,
	                                 const Function & m11, const Function & m12, const Function & m22 )
{	assert ( this->core );
	this->core->metric = tag::Util::Metric ( tag::whose_core_is,
		new tag::Util::Metric::Variable::Matrix::SquareRoot ( m11, m12, m22 ) );  }


void Manifold::set_metric ( const tag::InnerSpectralRadius &, const Function & sr,
	const tag::M11M12M22 &, const Function & m11, const Function & m12, const Function & m22 )
{	assert ( this->core );
	this->core->metric = tag::Util::Metric ( tag::whose_core_is,
		new tag::Util::Metric::Variable::Matrix::ISR ( sr, m11, m12, m22 ) );  }


void Manifold::set_metric ( const tag::SquareRoot &,
	const tag::InnerSpectralRadius &, const Function & sr,
	const tag::M11M12M22 &, const Function & m11, const Function & m12, const Function & m22 )
{	assert ( this->core );
	this->core->metric = tag::Util::Metric ( tag::whose_core_is,
		new tag::Util::Metric::Variable::Matrix::SquareRoot::ISR ( sr, m11, m12, m22 ) );  }

#endif  // ifndef MANIFEM_NO_FRONTAL

//------------------------------------------------------------------------------------------------------//


bool tag::Util::Metric::Trivial::isotropic ( )  // virtual from  tag::Util::Metric::Core
{	return true;  }

#ifndef MANIFEM_NO_FRONTAL

bool tag::Util::Metric::Constant::Isotropic::isotropic ( )  // virtual from  tag::Util::Metric::Core
{	return true;  }

bool tag::Util::Metric::Constant::Matrix::isotropic ( )  // virtual from  tag::Util::Metric::Core
{	return false;  }

bool tag::Util::Metric::Variable::Isotropic::isotropic ( )  // virtual from  tag::Util::Metric::Core
{	return true;  }

bool tag::Util::Metric::Variable::Matrix::isotropic ( )  // virtual from  tag::Util::Metric::Core
{	return false;  }

bool tag::Util::Metric::ScaledSq::isotropic ( )  // virtual from  tag::Util::Metric::Core
{	assert ( this->base .core );
	assert ( not this->base .core->isotropic() );
	return  this->base .core->isotropic();         }

bool tag::Util::Metric::ScaledSqInv::isotropic ( )  // virtual from  tag::Util::Metric::Core
{	assert ( this->base .core );
	assert ( not this->base .core->isotropic() );
	return  this->base .core->isotropic();         }

#endif  // ifndef MANIFEM_NO_FRONTAL

//------------------------------------------------------------------------------------------------------//


void tag::Util::Metric::Core::compute_data  // virtual
( const tag::AtPoint &, const Cell & V )
{	assert ( false );  }


void tag::Util::Metric::Core::compute_data  // virtual
( const tag::AtPoint &, const Cell & W, const tag::UseInformationFrom &, const Cell & V )
{	assert ( false );  }


void tag::Util::Metric::Core::codim_0 ( )  // virtual
{	}


void tag::Util::Metric::Core::codim_1 ( )  // virtual
{	}


void tag::Util::Metric::Trivial::correct_ext_prod_2d
( const Cell & P, std::vector < double > & n )  // virtual from tag::Util::MetricCore
{	assert ( false );  }   // never executed


void tag::Util::Metric::Trivial::correct_ext_prod_3d
( const Cell & P, std::vector < double > & n )  // virtual from tag::Util::MetricCore
{	assert ( false );  }   // never executed

//------------------------------------------------------------------------------------------------------//


// when we first declare a Manifold::Euclid, coord_field and coord_func will be nullptr
// immediately after declaring the manifold (in main),
// we must declare the coordinates to be of type Lagrange degree 1 for instance
//    	Manifold RR2 ( tag::Euclid, 2);
//      Function xy = RR2.build_coordinate_system ( tag::Lagrange, tag::of_degree, 1 );

Manifold::Manifold
( const tag::euclid &, const tag::OfDimension &, const size_t d )
:	Manifold ( tag::whose_core_is, std::make_shared < Manifold::Euclid > ( d ) )
{	assert ( d > 0 );  }


Manifold::Manifold
( const tag::euclid &, const tag::OfDimension &, const size_t d,
  const tag::DoNotSetAsWorking &                                )
:	core { std::make_shared < Manifold::Euclid > ( d ) }
{	assert ( this->core );
	assert ( d > 0 );       }

//------------------------------------------------------------------------------------------------------//


Manifold::Manifold ( const tag::Implicit &, const Manifold & m, const Function & f )

:	Manifold ( tag::non_existent )  // temporarily empty manifold

{	// if m is Manifold::Euclid, we want a Manifold::Implicit::OneEquation
	// if m is Manifold::Implicit::OneEquation, we want a Manifold::Implicit::TwoEquations
	// if m is Manifold::Implicit::TwoEquations, we want a Manifold::Implicit::ThreeEquationsOrMore
	// if m is Manifold::Implicit::ThreeEquationsOrMore, we want a Manifold::Implicit::ThreeEquationsOrMore

	assert ( f .number_of ( tag::components ) == 1 );
	
	std::shared_ptr < Manifold::Euclid > m_euclid =
		std::dynamic_pointer_cast < Manifold::Euclid > ( m .core );
	if ( m_euclid )
	{	this->core = std::make_shared < Manifold::Implicit::OneEquation > ( m, f );
		Manifold::working = *this;
		return;                                                                      }

	std::shared_ptr < Manifold::Implicit::OneEquation > m_one_eq =
		std::dynamic_pointer_cast < Manifold::Implicit::OneEquation > ( m .core );
	if ( m_one_eq )
	{	this->core = std::make_shared < Manifold::Implicit::TwoEquations > ( m, f );
		Manifold::working = *this;
		return;                                                                       }
	
	std::shared_ptr < Manifold::Implicit::TwoEquations > m_two_eq =
		std::dynamic_pointer_cast < Manifold::Implicit::TwoEquations > ( m .core );
	if ( m_two_eq )
	{	this->core = std::make_shared < Manifold::Implicit::ThreeEquationsOrMore > ( m, f );
		Manifold::working = *this;
		return;                                                                               }
	
	std::shared_ptr < Manifold::Implicit::ThreeEquationsOrMore > m_three_eq =
		std::dynamic_pointer_cast < Manifold::Implicit::ThreeEquationsOrMore > ( m .core );
	if ( m_three_eq )
	{	this->core = std::make_shared < Manifold::Implicit::ThreeEquationsOrMore > ( m, f );
		Manifold::working = *this;
		return;                                                                               }
	
	assert ( false );
	
}  // end of  Manifold constructor
	

Manifold::Manifold
( const tag::Implicit &, const Manifold & m, const Function & f1, const Function & f2 )

:	Manifold ( tag::non_existent )  // temporarily empty manifold

{	// if m is Manifold::Euclid, we want a Manifold::Implicit::TwoEquations
	// if m is Manifold::Implicit::{OneEquation,TwoEquations,ThreeEquationsOrMore},
	//      we want a Manifold::Implicit::TwoEquations

	assert ( f1 .number_of ( tag::components ) == 1 );
	assert ( f2 .number_of ( tag::components ) == 1 );

	std::shared_ptr < Manifold::Euclid > m_euclid =
		std::dynamic_pointer_cast < Manifold::Euclid > ( m .core );
	if ( m_euclid )
	{	this->core = std::make_shared < Manifold::Implicit::TwoEquations > ( m, f1, f2 );
		Manifold::working = *this;
		return;                                                                            }

	std::shared_ptr < Manifold::Implicit::OneEquation > m_one_eq =
		std::dynamic_pointer_cast < Manifold::Implicit::OneEquation > ( m .core );
	if ( m_one_eq )
	{	this->core = std::make_shared < Manifold::Implicit::ThreeEquationsOrMore > ( m, f1, f2 );
		Manifold::working = *this;
		return;                                                                                    }
	
	std::shared_ptr < Manifold::Implicit::TwoEquations > m_two_eq =
		std::dynamic_pointer_cast < Manifold::Implicit::TwoEquations > ( m .core );
	if ( m_two_eq )
	{	this->core = std::make_shared < Manifold::Implicit::ThreeEquationsOrMore > ( m, f1, f2 );
		Manifold::working = *this;
		return;                                                                                    }
	
	std::shared_ptr < Manifold::Implicit::ThreeEquationsOrMore > m_three_eq =
		std::dynamic_pointer_cast < Manifold::Implicit::ThreeEquationsOrMore > ( m .core );
	if ( m_three_eq )
	{	this->core = std::make_shared < Manifold::Implicit::ThreeEquationsOrMore > ( m, f1, f2 );
		Manifold::working = *this;
		return;                                                                                    }
	
	assert ( false );
	
}  // end of  Manifold constructor
		

Manifold::Manifold ( const tag::Implicit &, const Manifold & m,
                     const Function & f1, const Function & f2, const Function & f3 )

:	Manifold ( tag::non_existent )  // temporarily empty manifold

{	assert ( f1 .number_of ( tag::components ) == 1 );
	assert ( f2 .number_of ( tag::components ) == 1 );
	assert ( f3 .number_of ( tag::components ) == 1 );

	this->core = std::make_shared < Manifold::Implicit::ThreeEquationsOrMore > ( m, f1, f2, f3 );
	Manifold::working = *this;                                                                     }
		
//------------------------------------------------------------------------------------------------------//


void Manifold::build_intersection_of_two_manif ( const Manifold & m1, const Manifold & m2 )

{	std::shared_ptr < Manifold::Euclid > m1_euclid =
		std::dynamic_pointer_cast < Manifold::Euclid > ( m1 .core );
	if ( m1_euclid )
	{
		#ifndef NDEBUG  // DEBUG mode
		std::shared_ptr < Manifold::Implicit > m2_implicit =
			std::dynamic_pointer_cast < Manifold::Implicit > ( m2 .core );
		assert ( m2_implicit );
		assert ( m2_implicit->surrounding_space .core == m1 .core );
		#endif  // DEBUG
		this->core = m2 .core;
		Manifold::working = *this;
		return;                                                    }

	std::shared_ptr < Manifold::Euclid > m2_euclid =
		std::dynamic_pointer_cast < Manifold::Euclid > ( m2 .core );
	if ( m2_euclid )
	{
		#ifndef NDEBUG  // DEBUG mode
		std::shared_ptr < Manifold::Implicit > m1_implicit =
			std::dynamic_pointer_cast < Manifold::Implicit > ( m1 .core );
		assert ( m1_implicit );
		assert ( m1_implicit->surrounding_space .core == m2 .core );
		#endif  // DEBUG
		this->core = m1 .core;
		Manifold::working = *this;
		return;                                                    }

	#ifndef NDEBUG  // DEBUG mode
	std::shared_ptr < Manifold::Implicit > m1_implicit =
		std::dynamic_pointer_cast < Manifold::Implicit > ( m1 .core );
	assert ( m1_implicit );
	std::shared_ptr < Manifold::Implicit > m2_implicit =
		std::dynamic_pointer_cast < Manifold::Implicit > ( m2 .core );
	assert ( m2_implicit );
	assert ( m1_implicit->surrounding_space .core == m2_implicit->surrounding_space .core );
	#endif  // DEBUG

	std::shared_ptr < Manifold::Implicit::OneEquation > m1_implicit_one =
		std::dynamic_pointer_cast < Manifold::Implicit::OneEquation > ( m1 .core );
	if ( m1_implicit_one )
	{	std::shared_ptr < Manifold::Implicit::OneEquation > m2_implicit_one =
			std::dynamic_pointer_cast < Manifold::Implicit::OneEquation > ( m2 .core );
		if ( m2_implicit_one )
		{	this->core = std::make_shared < Manifold::Implicit::TwoEquations >
				( m1, m2_implicit_one->level_function );
			Manifold::working = *this;
			return;                                                                                    }
		#ifndef NDEBUG  // DEBUG mode
		std::shared_ptr < Manifold::Implicit::OneEquation > m2_implicit_two =
			std::dynamic_pointer_cast < Manifold::Implicit::OneEquation > ( m2 .core );
		assert ( m2_implicit_two );
		#endif  // DEBUG
		this->core = std::make_shared < Manifold::Implicit::TwoEquations >
			( m2, m1_implicit_one->level_function );
		Manifold::working = *this;
		return;                                                                                         }

	#ifndef NDEBUG  // DEBUG mode
	std::shared_ptr < Manifold::Implicit::TwoEquations > m1_implicit_two =
		std::dynamic_pointer_cast < Manifold::Implicit::TwoEquations > ( m1 .core );
	assert ( m1_implicit_two );
	#endif  // DEBUG
	std::shared_ptr < Manifold::Implicit::OneEquation > m2_implicit_one =
		std::dynamic_pointer_cast < Manifold::Implicit::OneEquation > ( m2 .core );
	assert ( m2_implicit_one );
	this->core = std::make_shared < Manifold::Implicit::TwoEquations >
		( m1, m2_implicit_one->level_function );
	Manifold::working = *this;
	return;

}  // end of  build_intersection_two_manif
	

Manifold::Manifold ( const tag::Intersect &, const Manifold & m1, const Manifold & m2 )

:	Manifold ( tag::non_existent )  // temporarily empty manifold

{	this->build_intersection_of_two_manif ( m1, m2 );  }


Manifold::Manifold
( const tag::Intersect &, const Manifold & m1, const Manifold & m2, const Manifold & m3 )

:	Manifold ( tag::non_existent )  // temporarily empty manifold

{	std::shared_ptr < Manifold::Euclid > m1_euclid =
		std::dynamic_pointer_cast < Manifold::Euclid > ( m1 .core );
	if ( m1_euclid )
	{	this->build_intersection_of_two_manif ( m2, m3 );
		return;                                           }

	std::shared_ptr < Manifold::Euclid > m2_euclid =
		std::dynamic_pointer_cast < Manifold::Euclid > ( m2 .core );
	if ( m2_euclid )
	{	this->build_intersection_of_two_manif ( m1, m3 );
		return;                                           }
	
	std::shared_ptr < Manifold::Euclid > m3_euclid =
		std::dynamic_pointer_cast < Manifold::Euclid > ( m3 .core );
	if ( m3_euclid )
	{	this->build_intersection_of_two_manif ( m1, m2 );
		return;                                           }

	#ifndef NDEBUG  // DEBUG mode
	std::shared_ptr < Manifold::Implicit::OneEquation > m1_implicit_one =
		std::dynamic_pointer_cast < Manifold::Implicit::OneEquation > ( m1 .core );
	assert ( m1_implicit_one );
	#endif  // DEBUG
	std::shared_ptr < Manifold::Implicit::OneEquation > m2_implicit_one =
		std::dynamic_pointer_cast < Manifold::Implicit::OneEquation > ( m2 .core );
	assert ( m2_implicit_one );
	std::shared_ptr < Manifold::Implicit::OneEquation > m3_implicit_one =
		std::dynamic_pointer_cast < Manifold::Implicit::OneEquation > ( m3 .core );
	assert ( m3_implicit_one );

	this->core = std::make_shared < Manifold::Implicit::ThreeEquationsOrMore >
		( m1, m2_implicit_one->level_function, m3_implicit_one->level_function );
	Manifold::working = *this;
	return;                                                                         }
	
//------------------------------------------------------------------------------------------------------//


Manifold::Manifold
( const tag::Parametric &, const Manifold & m, const Function::Equality & f_eq )

:	Manifold ( tag::non_existent )  // temporary empty manifold

{	tag::Util::assert_shared_cast < Manifold::Core, Manifold::Euclid > ( m.core );
	this->core = std::make_shared < Manifold::Parametric > ( m, f_eq );
	Manifold::working = *this;                                                 }


Manifold::Manifold ( const tag::Parametric &, const Manifold & m,
                            const Function::Equality & f_eq_1, const Function::Equality & f_eq_2 )

:	Manifold ( tag::non_existent )  // temporary empty manifold

{	tag::Util::assert_shared_cast < Manifold::Core, Manifold::Euclid > ( m.core );
	this->core = std::make_shared < Manifold::Parametric > ( m, f_eq_1, f_eq_2 );
	Manifold::working = *this;                                                 }


Manifold::Manifold
( const tag::Parametric &, const Manifold & m, const Function::Equality & f_eq_1,
  const Function::Equality & f_eq_2, const Function::Equality & f_eq_3           )

:	Manifold ( tag::non_existent )  // temporary empty manifold

{	tag::Util::assert_shared_cast < Manifold::Core, Manifold::Euclid > ( m.core );
	this->core = std::make_shared < Manifold::Parametric > ( m, f_eq_1, f_eq_2, f_eq_3 );
	Manifold::working = *this;                                                           }

//------------------------------------------------------------------------------------------------------//


#ifndef MANIFEM_NO_QUOTIENT

Manifold Manifold::quotient ( const Manifold::Action & a1 )
{	return Manifold ( tag::whose_core_is, std::make_shared < Manifold::Quotient > ( *this, a1 ) );  }

Manifold Manifold::quotient ( const Manifold::Action & a1, const Manifold::Action & a2 )
{	return Manifold ( tag::whose_core_is, std::make_shared < Manifold::Quotient > ( *this, a1, a2 ) );  }

Manifold Manifold::quotient
( const Manifold::Action & a1, const Manifold::Action & a2, const Manifold::Action & a3 )
{	return Manifold ( tag::whose_core_is, std::make_shared < Manifold::Quotient >
	                                         ( *this, a1, a2, a3 )                );  }

//------------------------------------------------------------------------------------------------------//
//------------------------------------------------------------------------------------------------------//


Cell::Winding::operator Manifold::Action ( ) const

{	Manifold::Action res ( 0 );
	std::shared_ptr < Manifold::Quotient > manif_q =
		std::dynamic_pointer_cast < Manifold::Quotient > ( Manifold::working.core );
	assert ( manif_q );

	assert ( this->cll->get_dim() == 1 );  // usually is a segment

	size_t n = manif_q->actions .size();
	assert ( n == manif_q->winding_nbs .size() );
	Cell::Core * pos_cll = this->cll->get_positive() .core;
	for ( size_t i = 0; i < n; i++ )
	{	short int exp = manif_q->winding_nbs[i] .on_cell(pos_cll);
		if ( exp == 0 ) continue;
		if ( not this->cll->is_positive() ) exp = -exp;
		Function::ActionGenerator & g = manif_q->actions[i];
		// inspired in item 24 of the book : Scott Meyers, Effective STL
		std::map<Function::ActionGenerator,short int>::iterator itt =
			res.index_map .lower_bound ( g );
		assert ( ( itt == res .index_map.end() ) or
	           ( res .index_map.key_comp()(g,itt->first) ) );
		res.index_map .emplace_hint ( itt, std::piecewise_construct,
			std::forward_as_tuple(g), std::forward_as_tuple(exp) );      }
	return res;                                                           }


Cell::Winding Cell::Winding::operator= ( const Cell::Winding & w )
	
{	assert ( this->cll->get_dim() == 1 );  // usually is a segment

	Manifold::Action g = w;
	*this = g;

	return *this;                          }


Cell::Winding Cell::Winding::operator= ( const Manifold::Action & a )
	
{	std::shared_ptr < Manifold::Quotient > manif_q =
		std::dynamic_pointer_cast < Manifold::Quotient > ( Manifold::working.core );
	assert ( manif_q );

	assert ( this->cll->get_dim() == 1 );  // usually is a segment

	size_t n = manif_q->actions.size();
	assert ( n == manif_q->winding_nbs .size() );
	Cell::Core * pos_cll = this->cll->get_positive() .core;
	for ( size_t i = 0; i < n; i++ )
	{	Function::ActionGenerator & g = manif_q->actions[i];
		std::map<Function::ActionGenerator,short int>::const_iterator itt =
			a.index_map.find ( g );
		if ( itt == a .index_map.end() )
		{	manif_q->winding_nbs[i] .on_cell ( pos_cll ) = 0;
			continue;                                   }
		short int exp = itt->second;
		assert ( exp != 0 );
		if ( this->cll->is_positive() ) manif_q->winding_nbs[i] .on_cell(pos_cll) = exp;
		else manif_q->winding_nbs[i] .on_cell(pos_cll) = -exp;                            }
	return *this;                                                                           }


Cell::Winding Cell::Winding::operator+= ( const Manifold::Action & a )
	
{	std::shared_ptr < Manifold::Quotient > manif_q =
		std::dynamic_pointer_cast < Manifold::Quotient > ( Manifold::working.core );
	assert ( manif_q );

	assert ( this->cll->get_dim() == 1 );  // usually is a segment

	size_t n = manif_q->actions .size();
	assert ( n == manif_q->winding_nbs .size() );
	Cell::Core * pos_cll = this->cll->get_positive() .core;
	for ( size_t i = 0; i < n; i++ )
	{	Function::ActionGenerator & g = manif_q->actions[i];
		std::map<Function::ActionGenerator,short int>::const_iterator itt =
			a.index_map.find ( g );
		if ( itt == a .index_map .end() ) continue;
		short int exp = itt->second;
		assert ( exp != 0 );
		if ( not this->cll->is_positive() ) exp = -exp;
		manif_q->winding_nbs[i] .on_cell(pos_cll) += exp;                    }
	return *this;                                                              }


Cell::Winding Cell::Winding::operator-= ( const Manifold::Action & a )
	
{	std::shared_ptr < Manifold::Quotient > manif_q =
		std::dynamic_pointer_cast < Manifold::Quotient > ( Manifold::working.core );
	assert ( manif_q );

	assert ( this->cll->get_dim() == 1 );  // usually is a segment

	size_t n = manif_q->actions.size();
	assert ( n == manif_q->winding_nbs .size() );
	Cell::Core * pos_cll = this->cll->get_positive() .core;
	for ( size_t i = 0; i < n; i++ )
	{	Function::ActionGenerator & g = manif_q->actions[i];
		std::map<Function::ActionGenerator,short int>::const_iterator itt =
			a.index_map.find ( g );
		if ( itt == a .index_map .end() ) continue;
		short int exp = itt->second;
		assert ( exp != 0 );
		if ( not this->cll->is_positive() ) exp = -exp;
		manif_q->winding_nbs[i] .on_cell (pos_cll) -= exp;                   }
	return *this;                                                              }


Cell::Winding Cell::winding ( ) const
{ return Cell::Winding ( *this );  }

#endif  // ifndef MANIFEM_NO_QUOTIENT

//------------------------------------------------------------------------------------------------------//
//------------------------------------------------------------------------------------------------------//


Function Manifold::Euclid::build_coord_func
( const tag::lagrange &, const tag::OfDegree &, size_t deg )
// virtual from Manifold::Core

// see also Function::Function ( const tag::LivesOn &, const tag::CellsOfDim &, const size_t dim,
//                               const tag::HasSize &, const size_t s                            )
	
{	assert ( deg == 1 );
	tag::Util::Field::Double::Base * field;
	Function::Core * func;
	if ( this->dim == 1 )
	{	tag::Util::Field::Double::Scalar * field_scalar = new tag::Util::Field::Double::Scalar
			( tag::lives_on_positive_cells, tag::of_dim, 0 );
		field = field_scalar;
		func = new Function::CoupledWithField::Scalar ( field_scalar );  }
	else
	{	assert ( this->dim > 1 );
		tag::Util::Field::Double::Block * field_block = new tag::Util::Field::Double::Block
			( tag::lives_on_positive_cells, tag::of_dim, 0, tag::has_size, this->dim );
		field = field_block;
	  func = new Function::CoupledWithField::Vector ( field_block );                }
	this->coord_field = field;
	this->coord_func = Function ( tag::whose_core_is, func );
	return this->coord_func;                                                            }


Function Manifold::Implicit::build_coord_func  // virtual from Manifold::Core
( const tag::lagrange &, const tag::OfDegree &, size_t deg )
{	assert ( false );
	// we return a non-existent Function just to avoid compilation errors
	return Function ( tag::non_existent );  }

Function Manifold::Parametric::build_coord_func  // virtual from Manifold::Core
( const tag::lagrange &, const tag::OfDegree &, size_t deg )
{	assert ( false );
	// we return a non-existent Function just to avoid compilation errors
	return Function ( tag::non_existent );  }

#ifndef MANIFEM_NO_QUOTIENT

Function Manifold::Quotient::build_coord_func  // virtual from Manifold::Core
( const tag::lagrange &, const tag::OfDegree &, size_t deg )
{	assert ( false );
	// we return a non-existent Function just to avoid compilation errors
	return Function ( tag::non_existent );  }

#endif  // ifndef MANIFEM_NO_QUOTIENT

//------------------------------------------------------------------------------------------------------//
	
Function Manifold::Euclid::get_coord_func ( ) const  // virtual from Manifold::Core
{	return this->coord_func;  }
	
Function Manifold::Implicit::get_coord_func ( ) const  // virtual from Manifold::Core
{	return this->surrounding_space.coordinates();  }

Function Manifold::Parametric::get_coord_func ( ) const  // virtual from Manifold::Core
{	return this->coord_func;  }

#ifndef MANIFEM_NO_QUOTIENT
Function Manifold::Quotient::get_coord_func ( ) const  // virtual from Manifold::Core
{	return this->coord_func;  }
#endif  // ifndef MANIFEM_NO_QUOTIENT

//------------------------------------------------------------------------------------------------------//


void Manifold::Euclid::set_coords ( const Function co )  // virtual from Manifold::Core
{	this->coord_func = co;  }


void Manifold::Implicit::set_coords ( const Function co )  // virtual from Manifold::Core
{	Manifold m = this->surrounding_space;
	std::shared_ptr < Manifold::Euclid > m_euclid =
	   tag::Util::assert_shared_cast < Manifold::Core, Manifold::Euclid > ( m .core );
	m_euclid->coord_func = co;                                                          }


void Manifold::Parametric::set_coords ( const Function co )  // virtual from Manifold::Core
{	Manifold m = this->surrounding_space;
	std::shared_ptr < Manifold::Euclid > m_euclid =
		tag::Util::assert_shared_cast < Manifold::Core, Manifold::Euclid > ( m .core );
	assert ( m_euclid );
	m_euclid->coord_func = co;                                                          }


#ifndef MANIFEM_NO_QUOTIENT
void Manifold::Quotient::set_coords ( const Function co )  // virtual from Manifold::Core
{	this->coord_func = co;  }
#endif  // ifndef MANIFEM_NO_QUOTIENT

//------------------------------------------------------------------------------------------------------//


size_t Manifold::Euclid::topological_dim ( ) const  // virtual from Manifold::Core
{	return this->dim;  }


size_t Manifold::Implicit::OneEquation::topological_dim ( ) const  // virtual from Manifold::Core
{	// return this->surrounding_space .topological_dim() - 1;
	return tag::Util::assert_diff ( this->surrounding_space .topological_dim(), 1 );  }


size_t Manifold::Implicit::TwoEquations::topological_dim ( ) const  // virtual from Manifold::Core
{	// return this->surrounding_space .topological_dim() - 2;
	return tag::Util::assert_diff ( this->surrounding_space .topological_dim(), 2 );  }


size_t Manifold::Implicit::ThreeEquationsOrMore::topological_dim ( ) const  // virtual from Manifold::Core
{	// return this->surrounding_space .topological_dim() - this->level_function .size();
	return tag::Util::assert_diff
		( this->surrounding_space .topological_dim(), this->level_function .size() );  }


size_t Manifold::Parametric::topological_dim ( ) const  // virtual from Manifold::Core
{	assert ( false );  return 0;  }  // to implement  // we return 0 just to avoid compilation errors


#ifndef MANIFEM_NO_QUOTIENT
size_t Manifold::Quotient::topological_dim ( ) const  // virtual from Manifold::Core
{	return this->base_space .topological_dim();  }
#endif  // ifndef MANIFEM_NO_QUOTIENT

//------------------------------------------------------------------------------------------------------//


double Manifold::Euclid::measure ( ) const  // virtual from Manifold::Core
{	std::cout << __FILE__ << ":" << __LINE__ << ": "
	          << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "Euclidian manifolds have infinite measure" << std::endl;
	exit ( 1 );                                                            }


double Manifold::Implicit::measure ( ) const  // virtual from Manifold::Core
{	std::cout << __FILE__ << ":" << __LINE__ << ": "
	          << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "computing the measure of implicit manifolds is not implemented " << std::endl;
	exit ( 1 );                                                                                  }


double Manifold::Parametric::measure ( ) const  // virtual from Manifold::Core
{	std::cout << __FILE__ << ":" << __LINE__ << ": "
	          << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "computing the measure of parametric manifolds is not implemented " << std::endl;
	exit ( 1 );                                                                                    }


#ifndef MANIFEM_NO_QUOTIENT

double Manifold::Quotient::measure ( ) const  // virtual from Manifold::Core
// only significant for a circle or a torus (translations as generators)

{	size_t nb_of_generators = this->actions .size();
	Manifold base_manif = this->base_space;

	if ( nb_of_generators == 1 )  // compute length
	{	assert ( base_manif .coordinates() .number_of ( tag::components ) == 1 );
		Function::ActionGenerator g1 = this->actions [0];
		std::vector < double > v1 =
			Function::Scalar::MultiValued::JumpIsSum::analyse_linear_expression
			( g1 .transf, g1 .coords );
		assert ( v1 .size() == 1 );
		assert ( v1 [0] > 0. );
		return v1 [0];                                                         }
		
	if ( nb_of_generators == 2 )  // compute area
	{	assert ( base_manif .coordinates() .number_of ( tag::components ) == 2 );
		Function::ActionGenerator g1 = this->actions [0];
		Function::ActionGenerator g2 = this->actions [1];
		std::vector < double > v1 =
			Function::Vector::MultiValued::JumpIsSum::analyse_linear_expression
			( g1 .transf, g1 .coords );
		assert ( v1 .size() == 2 );
		std::vector < double > v2 =
			Function::Vector::MultiValued::JumpIsSum::analyse_linear_expression
			( g2 .transf, g2 .coords );
		assert ( v2 .size() == 2 );
		const double area = v1[0]*v2[1] - v1[1]*v2[0];
		assert ( area > 0. );
		return area;                                                           }

	assert ( nb_of_generators == 3 );  // compute volume
	std::cout << __FILE__ << ":" << __LINE__ << ": "
	          << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "computing the volume of a torus is not implemented " << std::endl;
	exit ( 1 );

}  // end of  Manifold::Quotient::measure

#endif  // ifndef MANIFEM_NO_QUOTIENT

//------------------------------------------------------------------------------------------------------//


double Manifold::Core::sq_dist ( const Cell & A, const Cell & B ) const  // virtual

{	Manifold & space = Manifold::working;
	assert ( space .exists() );  // we use the current manifold
	const size_t n = space .coordinates() .number_of ( tag::components );
	std::vector < double > w ( n );
	for ( size_t i = 0; i < n; i++ )
	{	const Function & x = space .coordinates() [i];
		w[i] = x(B) - x(A);                            }
	assert ( this );
	assert ( this->metric .core );
	return this->metric .core->sq_dist ( A, B, w );                }


#ifndef MANIFEM_NO_QUOTIENT

double Manifold::Quotient::sq_dist ( const Cell & A, const Cell & B ) const
// virtual from Manifold::Core, here overridden
{	assert ( false );  return 0.;  }  // we return 0. just to avoid compilation errors

#endif  // ifndef MANIFEM_NO_QUOTIENT

//------------------------------------------------------------------------------------------------------//


// P = sA + sB,  s+t == 1

void Manifold::Euclid::pretty_interpolate
( const Cell & P, double s, const Cell & A, double t, const Cell & B ) const

{	Function coord = this->get_coord_func();
	size_t n = coord .number_of ( tag::components );
	for ( size_t i = 0; i < n; i++ )
		coord[i](P) = s * coord[i](A) + t * coord[i](B);  }	

// code below does the same as code above
// above is pretty, slightly slower - below is ugly, slightly faster


void Manifold::Euclid::interpolate ( Cell::Positive::Vertex * P,
  double s, Cell::Positive::Vertex * A, double t, Cell::Positive::Vertex * B ) const
//  virtual from Manifold::Core

// we could inline these, as interpolate_euclid, to gain speed 
	
{	Function coord = this->get_coord_func();
	Function::Scalar * coord_scalar = dynamic_cast < Function::Scalar * > ( coord.core );
	if ( coord_scalar )
	{	assert ( coord .number_of ( tag::components ) == 1 );
		coord_scalar->set_value_on_cell
			( P, s * coord_scalar->get_value_on_cell ( A ) +
			     t * coord_scalar->get_value_on_cell ( B )   );
		return;                                            } 
	Function::Vector * coord_vector = tag::Util::assert_cast
		< Function::Core*, Function::Vector* > ( coord.core );
	size_t n = coord .number_of ( tag::components );
	for ( size_t i = 0; i < n; i++ )
	{	Function::Scalar * coord_i = tag::Util::assert_cast
			< Function::Core*, Function::Scalar* > ( coord_vector->component(i).core );
		coord_i->set_value_on_cell
			( P, s * coord_i->get_value_on_cell ( A ) +
			     t * coord_i->get_value_on_cell ( B ) );                                }  }


void Manifold::Euclid::interpolate ( Cell::Positive::Vertex * P,
	double s, Cell::Positive::Vertex * A, double t, Cell::Positive::Vertex * B,
	double u, Cell::Positive::Vertex * C                                       ) const
//  virtual from Manifold::Core

// we could inline these, as interpolate_euclid, to gain speed	
	
{	Function coord = this->get_coord_func();
	Function::Scalar * coord_scalar = dynamic_cast < Function::Scalar * > ( coord.core );
	if ( coord_scalar )
	{	assert ( coord .number_of ( tag::components ) == 1 );
		coord_scalar->set_value_on_cell
			( P, s * coord_scalar->get_value_on_cell ( A ) +
			     t * coord_scalar->get_value_on_cell ( B ) +
			     u * coord_scalar->get_value_on_cell ( C )  );
		return;                                                        } 
	Function::Vector * coord_vector = tag::Util::assert_cast
		< Function::Core*, Function::Vector* > ( coord.core );
	size_t n = coord .number_of ( tag::components );
	for ( size_t i = 0; i < n; i++ )
	{	Function::Scalar * coord_i = tag::Util::assert_cast
			< Function::Core*, Function::Scalar* > ( coord_vector->component(i).core );
		coord_i->set_value_on_cell
			( P, s * coord_i->get_value_on_cell ( A ) +
			     t * coord_i->get_value_on_cell ( B ) +
			     u * coord_i->get_value_on_cell ( C )  );                               }  }


// P = sA + sB + uC + vD,  s+t+u+v == 1

void Manifold::Euclid::pretty_interpolate
( const Cell & P, double s, const Cell & A, double t, const Cell & B,
                  double u, const Cell & C, double v, const Cell & D ) const

{	Function coord = this->get_coord_func();
	size_t n = coord .number_of ( tag::components );
	for ( size_t i = 0; i < n; i++ )
		coord[i](P) = s * coord[i](A) + t * coord[i](B) +
		              u * coord[i](C) + v * coord[i](D);   }

// code below does the same as code above
// above is pretty, slightly slower - below is ugly, slightly faster


void Manifold::Euclid::interpolate ( Cell::Positive::Vertex * P,
	double s, Cell::Positive::Vertex * A, double t, Cell::Positive::Vertex * B,
	double u, Cell::Positive::Vertex * C, double v, Cell::Positive::Vertex * D ) const
//  virtual from Manifold::Core

// we could inline these, as interpolate_euclid, to gain speed	
	
{	Function coord = this->get_coord_func();
	Function::Scalar * coord_scalar = dynamic_cast < Function::Scalar * > ( coord.core );
	if ( coord_scalar )
	{	assert ( coord .number_of ( tag::components ) == 1 );
		coord_scalar->set_value_on_cell
			( P, s * coord_scalar->get_value_on_cell ( A ) +
			     t * coord_scalar->get_value_on_cell ( B ) +
			     u * coord_scalar->get_value_on_cell ( C ) +
			     v * coord_scalar->get_value_on_cell ( D )   );
		return;                                                        } 
	Function::Vector * coord_vector = tag::Util::assert_cast
		< Function::Core*, Function::Vector* > ( coord.core );
	size_t n = coord .number_of ( tag::components );
	for ( size_t i = 0; i < n; i++ )
	{	Function::Scalar * coord_i = tag::Util::assert_cast
			< Function::Core*, Function::Scalar* > ( coord_vector->component(i).core );
		coord_i->set_value_on_cell
			( P, s * coord_i->get_value_on_cell ( A ) +
			     t * coord_i->get_value_on_cell ( B ) +
			     u * coord_i->get_value_on_cell ( C ) +
			     v * coord_i->get_value_on_cell ( D )   );                               }  }


// P = sA + sB + uC + vD + wE + zF,  s+t+u+v+w+z == 1

void Manifold::Euclid::pretty_interpolate
( const Cell & P, double s, const Cell & A, double t, const Cell & B,
                  double u, const Cell & C, double v, const Cell & D,
                  double w, const Cell & E, double z, const Cell & F ) const

{	Function coord = this->get_coord_func();
	size_t n = coord .number_of ( tag::components );
	for ( size_t i = 0; i < n; i++ )
		coord[i](P) = s * coord[i](A) + t * coord[i](B) +
		              u * coord[i](C) + v * coord[i](D) +
		              w * coord[i](E) + z * coord[i](F);   }

// code below does the same as code above
// above is pretty, slightly slower - below is ugly, slightly faster


void Manifold::Euclid::interpolate ( Cell::Positive::Vertex * P,
	double s, Cell::Positive::Vertex * A, double t, Cell::Positive::Vertex * B,
	double u, Cell::Positive::Vertex * C, double v, Cell::Positive::Vertex * D,
	double w, Cell::Positive::Vertex * E, double z, Cell::Positive::Vertex * F ) const
//  virtual from Manifold::Core

// we could inline these, as interpolate_euclid, to gain speed	
	
{	Function coord = this->get_coord_func();
	Function::Scalar * coord_scalar = dynamic_cast < Function::Scalar * > ( coord.core );
	if ( coord_scalar )
	{	assert ( coord .number_of ( tag::components ) == 1 );
		coord_scalar->set_value_on_cell
			( P, s * coord_scalar->get_value_on_cell ( A ) +
			     t * coord_scalar->get_value_on_cell ( B ) +
			     u * coord_scalar->get_value_on_cell ( C ) +
			     v * coord_scalar->get_value_on_cell ( D ) +
			     w * coord_scalar->get_value_on_cell ( E ) +
			     z * coord_scalar->get_value_on_cell ( F )   );
		return;                                             } 
	Function::Vector * coord_vector = tag::Util::assert_cast
		< Function::Core*, Function::Vector* > ( coord.core );
	size_t n = coord .number_of ( tag::components );
	for ( size_t i = 0; i < n; i++ )
	{	Function::Scalar * coord_i = tag::Util::assert_cast
			< Function::Core*, Function::Scalar* > ( coord_vector->component(i).core );
		coord_i->set_value_on_cell
			( P, s * coord_i->get_value_on_cell ( A ) +
			     t * coord_i->get_value_on_cell ( B ) +
			     u * coord_i->get_value_on_cell ( C ) +
			     v * coord_i->get_value_on_cell ( D ) +
			     w * coord_i->get_value_on_cell ( E ) +
			     z * coord_i->get_value_on_cell ( F )   );                              }       }


// P = sum c_k P_k,  sum c_k == 1

void Manifold::Euclid::pretty_interpolate ( const Cell & P,
	const std::vector < double > & coefs, const std::vector < Cell > & points ) const

{	Function coord = this->get_coord_func();
	size_t n = coord .number_of ( tag::components );
	size_t m = points.size();  // == coefs.size()
	for ( size_t i = 0; i < n; i++ )
	{	double v = 0.;
		for ( size_t j = 0; j < m; j++ )  v += coefs[j] * coord[i]( points[j] );
		coord[i](P) = v;                                                          }	 }

// code below does the same as code above
// above is pretty, slightly slower - below is ugly, slightly faster


void Manifold::Euclid::interpolate ( Cell::Positive::Vertex * P,
const std::vector < double > & coefs, const std::vector < Cell::Positive::Vertex * > & points ) const
//  virtual from Manifold::Core

// we could inline these, as interpolate_euclid, to gain speed	
	
{	Function coord = this->get_coord_func();
	Function::Scalar * coord_scalar = dynamic_cast < Function::Scalar * > ( coord.core );
	if ( coord_scalar )
	{	assert ( coord .number_of ( tag::components ) == 1 );
		double v = 0.;
		size_t m = points.size();
		assert ( m == coefs.size() );
		for ( size_t j = 0; j < m; j++ )
			v += coefs[j] * coord_scalar->get_value_on_cell ( points[j] );
		coord_scalar->set_value_on_cell ( P, v );
		return;                                            } 
	Function::Vector * coord_vector = tag::Util::assert_cast
		< Function::Core*, Function::Vector* > ( coord.core );
	size_t n = coord .number_of ( tag::components ), m = points.size();
	assert ( m == coefs.size() );
	for ( size_t i = 0; i < n; i++ )
	{	Function::Scalar * coord_i = tag::Util::assert_cast
			< Function::Core*, Function::Scalar* > ( coord_vector->component(i).core );
		double v = 0.;
		for ( size_t j = 0; j < m; j++ )
			v += coefs[j] * coord_i->get_value_on_cell ( points[j] );
		coord_i->set_value_on_cell ( P, v );                                             }      }


// P = sA + sB,  s+t == 1     virtual from Manifold::Core
void Manifold::Implicit::interpolate ( Cell::Positive::Vertex * P,
	double s, Cell::Positive::Vertex * A, double t, Cell::Positive::Vertex * B ) const

{	this->surrounding_space .core->interpolate ( P, s, A, t, B );
	// assert surrounding space is Manifold::Euclid  !!
	this->project ( P );                                          }


// P = sA + sB + uC,  s+t+u == 1     virtual from Manifold::Core
void Manifold::Implicit::interpolate ( Cell::Positive::Vertex * P,
  double s, Cell::Positive::Vertex * A, double t, Cell::Positive::Vertex * B,
  double u, Cell::Positive::Vertex * C                                       ) const

{	this->surrounding_space .core->interpolate ( P, s, A, t, B, u, C );
	// assert surrounding space is Manifold::Euclid  !!
	this->project ( P );                                                      }


// P = sA + sB + uC + vD,  s+t+u+v == 1     virtual from Manifold::Core
void Manifold::Implicit::interpolate ( Cell::Positive::Vertex * P,
  double s, Cell::Positive::Vertex * A, double t, Cell::Positive::Vertex * B,
  double u, Cell::Positive::Vertex * C, double v, Cell::Positive::Vertex * D ) const

{	this->surrounding_space .core->interpolate ( P, s, A, t, B, u, C, v, D );
	// assert surrounding space is Manifold::Euclid  !!
	this->project ( P );                                                      }


// P = sA + sB + uC + vD + wE + zF,  s+t+u+v+w+z == 1     virtual from Manifold::Core
void Manifold::Implicit::interpolate ( Cell::Positive::Vertex * P,
  double s, Cell::Positive::Vertex * A, double t, Cell::Positive::Vertex * B,
  double u, Cell::Positive::Vertex * C, double v, Cell::Positive::Vertex * D,
  double w, Cell::Positive::Vertex * E, double z, Cell::Positive::Vertex * F ) const

{	this->surrounding_space .core->interpolate ( P, s, A, t, B, u, C, v, D, w, E, z, F );
	// assert surrounding space is Manifold::Euclid  !!
	this->project ( P );                                                                  }


// P = sum c_k P_k,  sum c_k == 1     virtual from Manifold::Core
void Manifold::Implicit::interpolate ( Cell::Positive::Vertex * P,
	const std::vector < double > & coefs, const std::vector < Cell::Positive::Vertex * > & points ) const
{	this->surrounding_space .core->interpolate ( P, coefs, points );
	// assert surrounding space is Manifold::Euclid  !!
	this->project ( P );                                            }

	
// P = sA + sB,  s+t == 1     virtual from Manifold::Core
void Manifold::Parametric::interpolate ( Cell::Positive::Vertex * P,
  double s, Cell::Positive::Vertex * A, double t, Cell::Positive::Vertex * B ) const
{	this->surrounding_space .core->interpolate ( P, s, A, t, B );
	this->project ( P );                                          }


// P = sA + sB + uC,  s+t+u == 1     virtual from Manifold::Core
void Manifold::Parametric::interpolate ( Cell::Positive::Vertex * P,
  double s, Cell::Positive::Vertex * A, double t, Cell::Positive::Vertex * B,
  double u, Cell::Positive::Vertex * C                                       ) const
{	this->surrounding_space .core->interpolate ( P, s, A, t, B, u, C );
	this->project ( P );                                                      }


// P = sA + sB + uC + vD,  s+t+u+v == 1     virtual from Manifold::Core
void Manifold::Parametric::interpolate ( Cell::Positive::Vertex * P,
  double s, Cell::Positive::Vertex * A, double t, Cell::Positive::Vertex * B,
  double u, Cell::Positive::Vertex * C, double v, Cell::Positive::Vertex * D ) const
{	this->surrounding_space .core->interpolate ( P, s, A, t, B, u, C, v, D );
	this->project ( P );                                                      }


// P = sA + sB + uC + vD + wE + zF,  s+t+u+v+w+z == 1     virtual from Manifold::Core
void Manifold::Parametric::interpolate ( Cell::Positive::Vertex * P,
  double s, Cell::Positive::Vertex * A, double t, Cell::Positive::Vertex * B,
  double u, Cell::Positive::Vertex * C, double v, Cell::Positive::Vertex * D,
  double w, Cell::Positive::Vertex * E, double z, Cell::Positive::Vertex * F ) const
//  virtual from Manifold::Core
{	this->surrounding_space .core->interpolate ( P, s, A, t, B, u, C, v, D, w, E, z, F );
	this->project ( P );                                                                  }


// P = sum c_k P_k,  sum c_k == 1     virtual from Manifold::Core
void Manifold::Parametric::interpolate ( Cell::Positive::Vertex * P,
  const std::vector < double > & coefs, const std::vector < Cell::Positive::Vertex * > & points ) const
{	this->surrounding_space .core->interpolate ( P, coefs, points );
	this->project ( P );                                            }


#ifndef MANIFEM_NO_QUOTIENT

void Manifold::Euclid::interpolate
( Cell::Positive::Vertex * P,
  double s, Cell::Positive::Vertex * A,
  double t, Cell::Positive::Vertex * B,
  const tag::Winding &, const Manifold::Action & exp_AB ) const
//  virtual from Manifold::Core

{	assert ( false );

	Function coord = this->get_coord_func();
	Function::Scalar * coord_scalar = dynamic_cast < Function::Scalar * > ( coord.core );
	if ( coord_scalar )
	{	assert ( coord .number_of ( tag::components ) == 1 );
		coord_scalar->set_value_on_cell
			( P, s * coord_scalar->get_value_on_cell ( A ) +
			     t * coord_scalar->get_value_on_cell ( B, tag::winding, exp_AB ) );
		return;                                                                } 
	Function::Vector * coord_vector = tag::Util::assert_cast
		< Function::Core*, Function::Vector* > ( coord.core );
	size_t n = coord .number_of ( tag::components );
	for ( size_t i = 0; i < n; i++ )
	{	Function::Scalar * coord_i = tag::Util::assert_cast
			< Function::Core*, Function::Scalar* > ( coord_vector->component(i).core );
		coord_i->set_value_on_cell
			( P, s * coord_i->get_value_on_cell ( A ) +
				t * coord_i->get_value_on_cell ( B, tag::winding, exp_AB ) );                }   }


void Manifold::Euclid::interpolate
( Cell::Positive::Vertex * P,
  double s, Cell::Positive::Vertex * A,
  double t, Cell::Positive::Vertex * B,
  const tag::Winding &, const Manifold::Action & exp_AB,
  double u, Cell::Positive::Vertex * C,
  const tag::Winding &, const Manifold::Action & exp_AC ) const
//  virtual from Manifold::Core

{	assert ( false );

	Function coord = this->get_coord_func();
	Function::Scalar * coord_scalar = dynamic_cast < Function::Scalar * > ( coord.core );
	if ( coord_scalar )
	{	assert ( coord .number_of ( tag::components ) == 1 );
		coord_scalar->set_value_on_cell
			( P, s * coord_scalar->get_value_on_cell ( A ) +
			     t * coord_scalar->get_value_on_cell ( B, tag::winding, exp_AB ) +
			     u * coord_scalar->get_value_on_cell ( C, tag::winding, exp_AC )  );
		return;                                                        } 
	Function::Vector * coord_vector = tag::Util::assert_cast
		< Function::Core*, Function::Vector* > ( coord.core );
	size_t n = coord .number_of ( tag::components );
	for ( size_t i = 0; i < n; i++ )
	{	Function::Scalar * coord_i = tag::Util::assert_cast
			< Function::Core*, Function::Scalar* > ( coord_vector->component(i).core );
		coord_i->set_value_on_cell
			( P, s * coord_i->get_value_on_cell ( A ) +
			     t * coord_i->get_value_on_cell ( B, tag::winding, exp_AB ) +
			     u * coord_i->get_value_on_cell ( C, tag::winding, exp_AC )  );           }       }


void Manifold::Euclid::interpolate
( Cell::Positive::Vertex * P,
  double s, Cell::Positive::Vertex * A,
  double t, Cell::Positive::Vertex * B,
  const tag::Winding &, const Manifold::Action & exp_AB,
  double u, Cell::Positive::Vertex * C,
  const tag::Winding &, const Manifold::Action & exp_AC,
  double v, Cell::Positive::Vertex * D,
  const tag::Winding &, const Manifold::Action & exp_AD ) const
//  virtual from Manifold::Core

{	assert ( false );

	Function coord = this->get_coord_func();
	Function::Scalar * coord_scalar = dynamic_cast < Function::Scalar * > ( coord.core );
	if ( coord_scalar )
	{	assert ( coord .number_of ( tag::components ) == 1 );
		coord_scalar->set_value_on_cell
			( P, s * coord_scalar->get_value_on_cell ( A ) +
			     t * coord_scalar->get_value_on_cell ( B, tag::winding, exp_AB ) +
			     u * coord_scalar->get_value_on_cell ( C, tag::winding, exp_AC ) +
			     v * coord_scalar->get_value_on_cell ( D, tag::winding, exp_AD )   );
		return;                                                        } 
	Function::Vector * coord_vector = tag::Util::assert_cast
		< Function::Core*, Function::Vector* > ( coord.core );
	size_t n = coord .number_of ( tag::components );
	for ( size_t i = 0; i < n; i++ )
	{	Function::Scalar * coord_i = tag::Util::assert_cast
			< Function::Core*, Function::Scalar* > ( coord_vector->component(i).core );
		coord_i->set_value_on_cell
			( P, s * coord_i->get_value_on_cell ( A ) +
			     t * coord_i->get_value_on_cell ( B, tag::winding, exp_AB ) +
			     u * coord_i->get_value_on_cell ( C, tag::winding, exp_AC ) +
			     v * coord_i->get_value_on_cell ( D, tag::winding, exp_AD )   );           }       }


void Manifold::Euclid::interpolate
( Cell::Positive::Vertex * P,
  double s, Cell::Positive::Vertex * A,
  double t, Cell::Positive::Vertex * B,
  const tag::Winding &, const Manifold::Action & exp_AB,
  double u, Cell::Positive::Vertex * C,
  const tag::Winding &, const Manifold::Action & exp_AC,
  double v, Cell::Positive::Vertex * D,
  const tag::Winding &, const Manifold::Action & exp_AD,
  double w, Cell::Positive::Vertex * E,
  const tag::Winding &, const Manifold::Action & exp_AE,
  double z, Cell::Positive::Vertex * F,
  const tag::Winding &, const Manifold::Action & exp_AF ) const
//  virtual from Manifold::Core
	
{	assert ( false );

	Function coord = this->get_coord_func();
	Function::Scalar * coord_scalar = dynamic_cast < Function::Scalar * > ( coord.core );
	if ( coord_scalar )
	{	assert ( coord .number_of ( tag::components ) == 1 );
		coord_scalar->set_value_on_cell
			( P, s * coord_scalar->get_value_on_cell ( A ) +
			     t * coord_scalar->get_value_on_cell ( B ) +
			     u * coord_scalar->get_value_on_cell ( C ) +
			     v * coord_scalar->get_value_on_cell ( D ) +
			     w * coord_scalar->get_value_on_cell ( E ) +
			     z * coord_scalar->get_value_on_cell ( F )   );
		return;                                             } 
	Function::Vector * coord_vector = tag::Util::assert_cast
		< Function::Core*, Function::Vector* > ( coord.core );
	size_t n = coord .number_of ( tag::components );
	for ( size_t i = 0; i < n; i++ )
	{	Function::Scalar * coord_i = tag::Util::assert_cast
			< Function::Core*, Function::Scalar* > ( coord_vector->component(i).core );
		coord_i->set_value_on_cell
			( P, s * coord_i->get_value_on_cell ( A ) +
			     t * coord_i->get_value_on_cell ( B ) +
			     u * coord_i->get_value_on_cell ( C ) +
			     v * coord_i->get_value_on_cell ( D ) +
			     w * coord_i->get_value_on_cell ( E ) +
			     z * coord_i->get_value_on_cell ( F )   );                              }     }


// P = sA + sB,  s+t == 1     virtual from Manifold::Core
void Manifold::Implicit::interpolate ( Cell::Positive::Vertex * P,
  double s, Cell::Positive::Vertex * A, double t, Cell::Positive::Vertex * B,
	const tag::Winding &, const Manifold::Action & exp                    ) const 
{	assert ( false );  }


// P = sA + sB + uC,  s+t+u == 1     virtual from Manifold::Core
void Manifold::Implicit::interpolate ( Cell::Positive::Vertex * P,
  double s, Cell::Positive::Vertex * A,
  double t, Cell::Positive::Vertex * B,
  const tag::Winding &, const Manifold::Action &,
  double u, Cell::Positive::Vertex * C,
  const tag::Winding &, const Manifold::Action & ) const
{	assert ( false );  }


// P = sA + sB + uC + vD,  s+t+u+v == 1     virtual from Manifold::Core
void Manifold::Implicit::interpolate ( Cell::Positive::Vertex * P,
  double s, Cell::Positive::Vertex * A,
  double t, Cell::Positive::Vertex * B,
  const tag::Winding &, const Manifold::Action &,
  double u, Cell::Positive::Vertex * C,
  const tag::Winding &, const Manifold::Action &,
  double v, Cell::Positive::Vertex * D,
  const tag::Winding &, const Manifold::Action & ) const
{	assert ( false );  }


// P = sA + sB + uC + vD + wE + zF,  s+t+u+v+w+z == 1     virtual from Manifold::Core
void Manifold::Implicit::interpolate ( Cell::Positive::Vertex * P,
  double s, Cell::Positive::Vertex * A,
  double t, Cell::Positive::Vertex * B,
  const tag::Winding &, const Manifold::Action &,
  double u, Cell::Positive::Vertex * C,
  const tag::Winding &, const Manifold::Action &,
  double v, Cell::Positive::Vertex * D,
  const tag::Winding &, const Manifold::Action &,
  double w, Cell::Positive::Vertex * E,
  const tag::Winding &, const Manifold::Action &,
  double z, Cell::Positive::Vertex * F,
  const tag::Winding &, const Manifold::Action & ) const
{	assert ( false );  }


// P = sA + sB,  s+t == 1     virtual from Manifold::Core
void Manifold::Parametric::interpolate ( Cell::Positive::Vertex * P,
  double s, Cell::Positive::Vertex * A, double t, Cell::Positive::Vertex * B,
  const tag::Winding &, const Manifold::Action & exp                    ) const 
{	assert ( false );  }


// P = sA + sB + uC,  s+t+u == 1     virtual from Manifold::Core
void Manifold::Parametric::interpolate ( Cell::Positive::Vertex * P,
  double s, Cell::Positive::Vertex * A,
  double t, Cell::Positive::Vertex * B,
  const tag::Winding &, const Manifold::Action &,
  double u, Cell::Positive::Vertex * C,
  const tag::Winding &, const Manifold::Action & ) const
{	assert ( false );  }


// P = sA + sB + uC + vD,  s+t+u+v == 1     virtual from Manifold::Core
void Manifold::Parametric::interpolate ( Cell::Positive::Vertex * P,
  double s, Cell::Positive::Vertex * A,
  double t, Cell::Positive::Vertex * B,
  const tag::Winding &, const Manifold::Action &,
  double u, Cell::Positive::Vertex * C,
  const tag::Winding &, const Manifold::Action &,
  double v, Cell::Positive::Vertex * D,
  const tag::Winding &, const Manifold::Action & ) const
{	assert ( false );  }


// P = sA + sB + uC + vD + wE + zF,  s+t+u+v+w+z == 1     virtual from Manifold::Core
void Manifold::Parametric::interpolate ( Cell::Positive::Vertex * P,
  double s, Cell::Positive::Vertex * A,
  double t, Cell::Positive::Vertex * B,
  const tag::Winding &, const Manifold::Action &,
  double u, Cell::Positive::Vertex * C,
  const tag::Winding &, const Manifold::Action &,
  double v, Cell::Positive::Vertex * D,
  const tag::Winding &, const Manifold::Action &,
  double w, Cell::Positive::Vertex * E,
  const tag::Winding &, const Manifold::Action &,
  double z, Cell::Positive::Vertex * F,
  const tag::Winding &, const Manifold::Action & ) const
{	assert ( false );  }


// P = sA + sB,  s+t == 1     virtual from Manifold::Core
void Manifold::Quotient::interpolate ( Cell::Positive::Vertex * P,
  double s, Cell::Positive::Vertex * A, double t, Cell::Positive::Vertex * B ) const
{	this->base_space .core->interpolate ( P, s, A, t, B );  }


// P = sA + sB,  s+t == 1     virtual from Manifold::Core
void Manifold::Quotient::interpolate ( Cell::Positive::Vertex * P,
  double s, Cell::Positive::Vertex * A, double t, Cell::Positive::Vertex * B,
  const tag::Winding &, const Manifold::Action & exp                         ) const 
{	assert ( false );  }


// P = sA + sB + uC,  s+t+u == 1     virtual from Manifold::Core
void Manifold::Quotient::interpolate ( Cell::Positive::Vertex * P,
  double s, Cell::Positive::Vertex * A, double t, Cell::Positive::Vertex * B,
  double u, Cell::Positive::Vertex * C                                       ) const
{	this->base_space .core->interpolate ( P, s, A, t, B, u, C );  }


// P = sA + sB + uC,  s+t+u == 1     virtual from Manifold::Core
void Manifold::Quotient::interpolate ( Cell::Positive::Vertex * P,
  double s, Cell::Positive::Vertex * A,
  double t, Cell::Positive::Vertex * B,
  const tag::Winding &, const Manifold::Action &,
  double u, Cell::Positive::Vertex * C,
  const tag::Winding &, const Manifold::Action & ) const
{	assert ( false );  }


// P = sA + sB + uC + vD,  s+t+u+v == 1     virtual from Manifold::Core
void Manifold::Quotient::interpolate ( Cell::Positive::Vertex * P,
  double s, Cell::Positive::Vertex * A, double t, Cell::Positive::Vertex * B,
  double u, Cell::Positive::Vertex * C, double v, Cell::Positive::Vertex * D ) const
{	this->base_space .core->interpolate ( P, s, A, t, B, u, C, v, D );  }


// P = sA + sB + uC + vD,  s+t+u+v == 1     virtual from Manifold::Core
void Manifold::Quotient::interpolate ( Cell::Positive::Vertex * P,
  double s, Cell::Positive::Vertex * A,
  double t, Cell::Positive::Vertex * B,
  const tag::Winding &, const Manifold::Action &,
  double u, Cell::Positive::Vertex * C,
  const tag::Winding &, const Manifold::Action &,
  double v, Cell::Positive::Vertex * D,
  const tag::Winding &, const Manifold::Action & ) const
{	assert ( false );  }


// P = sA + sB + uC + vD + wE + zF,  s+t+u+v+w+z == 1     virtual from Manifold::Core
void Manifold::Quotient::interpolate ( Cell::Positive::Vertex * P,
  double s, Cell::Positive::Vertex * A, double t, Cell::Positive::Vertex * B,
  double u, Cell::Positive::Vertex * C, double v, Cell::Positive::Vertex * D,
  double w, Cell::Positive::Vertex * E, double z, Cell::Positive::Vertex * F ) const
//  virtual from Manifold::Core
{	this->base_space .core->interpolate ( P, s, A, t, B, u, C, v, D, w, E, z, F );  }


// P = sA + sB + uC + vD + wE + zF,  s+t+u+v+w+z == 1     virtual from Manifold::Core
void Manifold::Quotient::interpolate ( Cell::Positive::Vertex * P,
  double s, Cell::Positive::Vertex * A,
  double t, Cell::Positive::Vertex * B,
  const tag::Winding &, const Manifold::Action &,
  double u, Cell::Positive::Vertex * C,
  const tag::Winding &, const Manifold::Action &,
  double v, Cell::Positive::Vertex * D,
  const tag::Winding &, const Manifold::Action &,
  double w, Cell::Positive::Vertex * E,
  const tag::Winding &, const Manifold::Action &,
  double z, Cell::Positive::Vertex * F,
  const tag::Winding &, const Manifold::Action & ) const
{	assert ( false );  }


// P = sum c_k P_k,  sum c_k == 1     virtual from Manifold::Core
void Manifold::Quotient::interpolate ( Cell::Positive::Vertex * P,
	const std::vector < double > & coefs, const std::vector < Cell::Positive::Vertex * > & points ) const
{	this->base_space .core->interpolate ( P, coefs, points );   }

#endif  // ifndef MANIFEM_NO_QUOTIENT

//-----------------------------------------------------------------------------------------


void Manifold::Euclid::project ( Cell::Positive::Vertex * P_c ) const  { }
	

void Manifold::Implicit::OneEquation::project ( Cell::Positive::Vertex * P_c ) const

// just a few steps of Newton's method for under-determined equations

{	const Function & coord = this->get_coord_func();
	size_t n = coord .number_of ( tag::components );
	const Function & lev_func = this->level_function;
	assert ( lev_func .number_of ( tag::components ) == 1 );
	const Function & grad_lev = this->grad_lev_func;
	assert ( grad_lev .number_of ( tag::components ) == n );
	const Cell P ( tag::whose_core_is, P_c, tag::previously_existing, tag::surely_not_null );
	// do we really need a temporary wrapper for P_c ? !!
	for ( short int k = 0; k < Manifold::Implicit::steps_for_Newton; k++ )
	// we move doubles around a lot
	// how to do it faster ?
	// somehow bind references to coord_at_P to 'coord'
	{	std::vector < double > coord_at_P = coord (P);
		double lev_at_P = lev_func (P);
		std::vector < double > grad_lev_at_P = grad_lev (P);
		double norm2 = 0.;
		for ( size_t i = 0; i < n; i++ )
		{	double & tmp = grad_lev_at_P [i];
			norm2 += tmp * tmp;               }
		double coef = lev_at_P / norm2;
		for ( size_t i = 0; i < n; i++ )
			coord_at_P [i] -= coef * grad_lev_at_P [i];
		coord (P) = coord_at_P;                            }                                   }


void Manifold::Implicit::TwoEquations::project ( Cell::Positive::Vertex * P_c ) const

// just a few steps of Newton's method for under-determined systems of equations

{	const Function & coord = this->get_coord_func();
	size_t n = coord .number_of ( tag::components );
	const Function & lev_func_1 = this->level_function_1;
	assert ( lev_func_1 .number_of ( tag::components ) == 1 );
	const Function & grad_lev_1 = this->grad_lev_func_1;
	assert ( grad_lev_1 .number_of ( tag::components ) == n );
	const Function & lev_func_2 = this->level_function_2;
	assert ( lev_func_2 .number_of ( tag::components ) == 1 );
	const Function & grad_lev_2 = this->grad_lev_func_2;
	assert ( grad_lev_2 .number_of ( tag::components ) == n );
	const Cell P ( tag::whose_core_is, P_c, tag::previously_existing, tag::surely_not_null );
	// do we really need a temporary wrapper for P_c ? !!
	for ( short int k = 0; k < Manifold::Implicit::steps_for_Newton; k++ )
	// we move doubles around a lot
	// how to do it faster ?
	// somehow bind references to coord_at_P to 'coord'
	{	std::vector < double > coord_at_P = coord (P);
		double lev_1_at_P = lev_func_1 (P);
		std::vector < double > grad_lev_1_at_P = grad_lev_1 (P);
		double lev_2_at_P = lev_func_2 (P);
		std::vector < double > grad_lev_2_at_P = grad_lev_2 (P);
		double a11 = 0., a12 = 0., a22 = 0.;
		for ( size_t i = 0; i < n; i++ )
		{	double & tmp1 = grad_lev_1_at_P [i];
			double & tmp2 = grad_lev_2_at_P [i];
			a11 += tmp1 * tmp1;
			a12 += tmp1 * tmp2;
			a22 += tmp2 * tmp2;                  }
		double det = a11*a22 - a12*a12;
		double l1 = ( - a22 * lev_1_at_P + a12 * lev_2_at_P ) / det;
		double l2 = (   a12 * lev_1_at_P - a11 * lev_2_at_P ) / det;
		for ( size_t i = 0; i < n; i++ )
			coord_at_P [i] += l1 * grad_lev_1_at_P[i] + l2 * grad_lev_2_at_P[i];
		coord (P) = coord_at_P;                                                }                }


void Manifold::Implicit::ThreeEquationsOrMore::project ( Cell::Positive::Vertex * P_c ) const

// just a few steps of conjugate gradient method

{	const Function & coord = this->get_coord_func();
	const size_t geom_dim = coord .number_of ( tag::components );
	const size_t nb_constr = this->level_function .size();
	assert ( this->grad_lev_func .size() == nb_constr );
	std::vector < Function > ::const_iterator it;
	#ifndef NDEBUG  // DEBUG mode
	for ( it = this->level_function .begin(); it != this->level_function .end(); it++ )
	{	const Function & lev_func = *it;
		assert ( lev_func .number_of ( tag::components ) == 1 );  }
	for ( it = this->grad_lev_func .begin(); it != this->grad_lev_func .end(); it++ )
	{	const Function & grad = *it;
		assert ( grad .number_of ( tag::components ) == geom_dim );  }
	#endif  //  DEBUG
	
	const Cell P ( tag::whose_core_is, P_c, tag::previously_existing, tag::surely_not_null );
	// do we really need a temporary wrapper for P_c ? !!

	std::vector < double > coord_at_P = coord (P);
	assert ( coord_at_P .size() == geom_dim );
	std::vector < double > constr ( nb_constr ), grad ( geom_dim ), direc ( geom_dim );
	tag::Util::Tensor < double > grad_constr ( nb_constr, geom_dim );

	// first step in conjugate gradient method :
	{  // just a block of code for hiding names
	size_t i = 0;
	for ( it = this->level_function .begin(); it != this->level_function .end(); it++ )
	{	assert ( i < nb_constr );
		constr [i] = (*it) (P);  i++;  }
	assert ( i == nb_constr );
	i = 0;
	for ( it = this->grad_lev_func .begin(); it != this->grad_lev_func .end(); it++ )
	{	assert ( i < nb_constr );
		for ( size_t j = 0; j < geom_dim; j++ )
			grad_constr ( i, j ) = (*it) [j] (P);
		i++;                                    }
	assert ( i == nb_constr );
	}  // just a block of code for hiding names
	tag::Util::conjugate_gradient ( coord_at_P, constr, grad_constr, grad, direc, true );
	// last argument true means "first step"
	coord (P) = coord_at_P;
	
	for ( short int k = 0; k < Manifold::Implicit::steps_for_Newton; k++ )
	{	{  // just a block of code for hiding names
		size_t i = 0;
		for ( it = this->level_function .begin(); it != this->level_function .end(); it++ )
		{	assert ( i < nb_constr );
			constr [i] = (*it) (P);  i++;  }
		assert ( i == nb_constr );
		i = 0;
		for ( it = this->grad_lev_func .begin(); it != this->grad_lev_func .end(); it++ )
		{	assert ( i < nb_constr );
			for ( size_t j = 0; j < geom_dim; j++ )
				grad_constr ( i, j ) = (*it) [j] (P);
			i++;                                    }
		assert ( i == nb_constr );
		}  // just a block of code for hiding names
		tag::Util::conjugate_gradient ( coord_at_P, constr, grad_constr, grad, direc, false );
		// last argument false means "not first step"
		coord (P) = coord_at_P;                                                                 }  }


void Manifold::Parametric::project ( Cell::Positive::Vertex * P_c ) const

{	std::map< Function, Function >::const_iterator it;
	for ( it = this->equations.begin(); it != this->equations.end(); it++ )
	{	Function::Scalar * coord_scalar = tag::Util::assert_cast
			< Function::Core*, Function::Scalar* > ( it->first.core );
		Function::Scalar * expr_scalar = tag::Util::assert_cast
			< Function::Core*, Function::Scalar* > ( it->second.core );
		coord_scalar->set_value_on_cell ( P_c, expr_scalar->get_value_on_cell ( P_c ) );  }  }


#ifndef MANIFEM_NO_QUOTIENT
void Manifold::Quotient::project ( Cell::Positive::Vertex * P_c ) const  { }
#endif  // ifndef MANIFEM_NO_QUOTIENT

//-----------------------------------------------------------------------------------------


void tag::Util::conjugate_gradient   // Polak Ribiere              // static
( std::vector < double > & x, const std::vector < double > constr,
	const tag::Util::Tensor < double > & grad_constr, std::vector < double > & grad,
	std::vector < double > & direc, bool first_step                                 )

// performs one step for minimizing a sum of squares of m constraints, in n variables

// x is the current point, will be changed

// 'constr' has the values of each constraint at point x
// 'grad_constr' has the gradients of each individual constraint at point x

// grad  receives the gradient of the sum of squares at previous step
// and also returns the gradient of the sum of squares at x
	
// uses previous direction 'direc' and returns the new direction in the same vector
// if 'first step', do not use information about previous direction

// norm of 'grad' or of 'direc' can be used as stopping criterion

// is it not possible to avoid divisions by quantities going to zero ?
// probably not, if we do not use second order derivatives
	
{	size_t n = x .size();
	assert ( direc .size() == n );
	assert ( grad .size() == n );
	size_t m = constr .size();
	assert ( grad_constr .dimensions == std::vector < size_t > ( { m, n } ) );

	std::vector < double > new_grad ( n, 0. );
	assert ( new_grad .size() == n );
	tag::Util::Tensor < double > grad_grad ( n, n );
	for ( size_t i = 0; i <	n; i++ )
	for ( size_t k = 0; k <	m; k++ )
	{	new_grad [i] += constr [k] * grad_constr ( k, i );
		for ( size_t j = 0; j <	n; j++ )
			grad_grad ( i, j ) += grad_constr ( k, i ) * grad_constr ( k, j );  }
	
	if ( first_step ) direc = new_grad;
	else
	{	double up = 0., down = 0.;
		for ( size_t i = 0; i < n; i++ )
		{	up += new_grad [i] * ( new_grad [i] - grad [i] );
			down += grad [i] * grad [i];                      }
		double alpha = up / down;
		for ( size_t i = 0; i < n; i++ ) direc [i] *= alpha;
		for ( size_t i = 0; i < n; i++ ) direc [i] += new_grad [i];  }

	double down = 0.;
	for ( size_t i = 0; i < n; i++ )
	for ( size_t j = 0; j < n; j++ )
		down += grad_grad ( i, j ) * direc [i] * direc [j];

	// down is a small quantity (going to zero when the algorithm converges)
	// we should not divide by small quantities
	
	for ( size_t i = 0; i < n; i++ )
	for ( size_t j = 0; j < n; j++ )
		x [i] -= new_grad [j] * direc [j] * direc [i] / down;
	
	grad .swap ( new_grad );

}  // end of  conjugate_gradient

//------------------------------------------------------------------------------------------------------//


// for an istropic metric, return the zoom at that point
// for an anisotropic metric, return the (square root of the) inner spectral radius

double tag::Util::Metric::Trivial::inner_spectral_radius ( const Cell & A )
{	assert ( A .dim() == 0 );        // virtual from tag::Util::Metric::Core
	return 1.;                 }     // here, could be called length_of_unit


#ifndef MANIFEM_NO_FRONTAL

double tag::Util::Metric::Constant::Isotropic::inner_spectral_radius ( const Cell & A )
{ assert ( A .dim() == 0 );        // virtual from tag::Util::Metric::Core
	return this->zoom;         }     // here, could be called length_of_unit


double tag::Util::Metric::Constant::Matrix::Simple::inner_spectral_radius ( const Cell & A )
{ assert ( A .dim() == 0 );        // virtual from tag::Util::Metric::Core
	return  this->isr;          }


double tag::Util::Metric::Constant::Matrix::SquareRoot::inner_spectral_radius ( const Cell & A )
{	assert ( false );        // virtual from tag::Util::Metric::Core
	return  0.;    }


double tag::Util::Metric::Variable::Isotropic::inner_spectral_radius ( const Cell & A )
{	assert ( A .dim() == 0 );        // virtual from tag::Util::Metric::Core
	return  this->zoom (A);    }      // here, could be called length_of_unit


double tag::Util::Metric::ScaledSq::inner_spectral_radius ( const Cell & A )
// virtual from tag::Util::Metric::Core
{	return  this->base .core->inner_spectral_radius (A) * this->zoom (A);  }     


double tag::Util::Metric::ScaledSqInv::inner_spectral_radius ( const Cell & A )
// virtual from tag::Util::Metric::Core
{	return  this->base .core->inner_spectral_radius (A) / this->zoom (A);  } 

#endif  // ifndef MANIFEM_NO_FRONTAL

//------------------------------------------------------------------------------------------------------//


double tag::Util::Metric::Core::avrg_spectral_radius ( const Cell & A )
{	assert ( A .dim() == 0 );   // virtual, later overridden for anisotropic metrics
	return  this->inner_spectral_radius (A);  }


#ifndef MANIFEM_NO_FRONTAL

double tag::Util::Metric::Constant::Matrix::Simple::avrg_spectral_radius ( const Cell & A )
// virtual from tag::Util::Metric::Core, here overridden
{	assert ( A .dim() == 0 );   // A not used
	return  std::sqrt ( tag::Util::Metric::compute_avrg_sr ( this->matrix ) );  }


double tag::Util::Metric::Constant::Matrix::SquareRoot::avrg_spectral_radius ( const Cell & A )
// virtual from tag::Util::Metric::Core, here overridden
{	assert ( A .dim() == 0 );   // A not used
	return  tag::Util::Metric::compute_avrg_sr ( this->matrix );  }


double tag::Util::Metric::Variable::Matrix::Simple::avrg_spectral_radius ( const Cell & A )
// virtual from tag::Util::Metric::Core, here overridden
{	assert ( A .dim() == 0 );
	assert ( this->matrix .dimensions .size() == 2 );
	size_t n = this->matrix .dimensions [0];
	assert ( this->matrix .dimensions [1] == n );
	Tensor < double > m ( n, n );
	for ( size_t i = 0; i < n; i++ )
	for ( size_t j = 0; j < n; j++ )
		m ( i, j ) = this->matrix ( i, j ) (A);
	return  std::sqrt ( tag::Util::Metric::compute_avrg_sr (m) );  }


double tag::Util::Metric::Variable::Matrix::SquareRoot::avrg_spectral_radius ( const Cell & A )
// virtual from tag::Util::Metric::Core, here overridden
{	assert ( A .dim() == 0 );
	assert ( this->matrix .dimensions .size() == 2 );
	size_t n = this->matrix .dimensions [0];
	assert ( this->matrix .dimensions [1] == n );
	Tensor < double > m ( n, n );
	for ( size_t i = 0; i < n; i++ )
	for ( size_t j = 0; j < n; j++ )
		m ( i, j ) = this->matrix ( i, j ) (A);
	return  tag::Util::Metric::compute_avrg_sr (m);  }


double tag::Util::Metric::Variable::Matrix::ISR::avrg_spectral_radius ( const Cell & A )
// virtual from tag::Util::Metric::Core, here overridden
{	assert ( A .dim() == 0 );
	assert ( this->matrix .dimensions .size() == 2 );
	size_t n = this->matrix .dimensions [0];
	assert ( this->matrix .dimensions [1] == n );
	Tensor < double > m ( n, n );
	for ( size_t i = 0; i < n; i++ )
	for ( size_t j = 0; j < n; j++ )
		m ( i, j ) = this->matrix ( i, j ) (A);
	return  std::sqrt ( this->isr (A) + tag::Util::Metric::compute_avrg_sr (m) );  }


double tag::Util::Metric::Variable::Matrix::SquareRoot::ISR::avrg_spectral_radius ( const Cell & A )
// virtual from tag::Util::Metric::Core, here overridden
{	assert ( A .dim() == 0 );
	assert ( this->matrix .dimensions .size() == 2 );
	size_t n = this->matrix .dimensions [0];
	assert ( this->matrix .dimensions [1] == n );
	Tensor < double > m ( n, n );
	for ( size_t i = 0; i < n; i++ )
	for ( size_t j = 0; j < n; j++ )
		m ( i, j ) = this->matrix ( i, j ) (A);
	return  this->isr (A) + tag::Util::Metric::compute_avrg_sr (m);  }


double tag::Util::Metric::ScaledSq::avrg_spectral_radius ( const Cell & A )
// virtual from tag::Util::Metric::Core, here overridden
{	assert ( A .dim() == 0 );
	return  this->zoom (A) * this->base .core->avrg_spectral_radius (A);  }


double tag::Util::Metric::ScaledSqInv::avrg_spectral_radius ( const Cell & A )
// virtual from tag::Util::Metric::Core, here overridden
{	assert ( A .dim() == 0 );
	return  this->base .core->avrg_spectral_radius (A) / this->zoom (A);  }

#endif  // ifndef MANIFEM_NO_FRONTAL

//------------------------------------------------------------------------------------------------------//


namespace {  // anonymous namespace, mimics static linkage

inline double raw_inner_prod ( const std::vector < double > & v, const std::vector < double > & w )

{	size_t n = v .size();
	assert ( w .size() == n );
	assert ( n == Manifold::working. coordinates() .number_of ( tag::components ) );
	double prod = 0.;
	for ( size_t i = 0; i < n; i++ )  prod += v[i] * w[i];
	return prod;                                                          }
	
}  // end of anonymous namespace


double tag::Util::Metric::Core::inner_prod  // virtual
( const Cell & P, const Cell & Q, const std::vector < double > & v, const std::vector < double > & w )

// computes arithmetic mean between value at P and value at Q

// several derived classes override this generic definition,
// either for speedup or for giving a completely different definition
	
{	assert ( P .dim() == 0 );
	assert ( Q .dim() == 0 );
	assert ( false );  // perhaps we don't need this function ?
	const double tmp_P = this->inner_spectral_radius (P);
	const double tmp_Q = this->inner_spectral_radius (Q);
	return ( tmp_P * tmp_P + tmp_Q * tmp_Q ) * raw_inner_prod ( v, w ) / 2.;  }
// return ( this->inner_prod ( P, v, w ) + this->inner_prod ( Q, v, w ) ) / 2.;


double tag::Util::Metric::Trivial::inner_prod  // virtual from tag::Util::Metric::Core
( const std::vector < double > & v, const std::vector < double > & w )

{	return raw_inner_prod ( v, w );  }


double tag::Util::Metric::Trivial::inner_prod  // virtual from tag::Util::Metric::Core
( const Cell & P, const std::vector < double > & v, const std::vector < double > & w )

// P not used

{	assert ( P .dim() == 0 );
	return raw_inner_prod ( v, w );  }


double tag::Util::Metric::Trivial::inner_prod
( const Cell & P, const Cell & Q, const std::vector < double > & v, const std::vector < double > & w )
// virtual from tag::Util::Metric::Core, here overriden just for speedup

// P, Q not used

{	assert ( P .dim() == 0 );
	assert ( Q .dim() == 0 );
	return raw_inner_prod ( v, w );  }


#ifndef MANIFEM_NO_FRONTAL

double tag::Util::Metric::Constant::inner_prod  // virtual from tag::Util::Metric::Core
( const Cell & P, const std::vector < double > & v, const std::vector < double > & w )
// P not used

{	assert ( P .dim() == 0 );
	return this->inner_prod ( v, w );  }

	
double tag::Util::Metric::Constant::inner_prod  // virtual from tag::Util::Metric::Core
( const Cell & P, const Cell & Q, const std::vector < double > & v, const std::vector < double > & w )
// P and Q not used

{	assert ( P .dim() == 0 );
	assert ( Q .dim() == 0 );
	return this->inner_prod ( v, w );  }

	
double tag::Util::Metric::Constant::Isotropic::inner_prod  // virtual from tag::Util::Metric::Core
( const std::vector < double > & v, const std::vector < double > & w )

{	const double tmp = this->zoom;
	return tmp * tmp * raw_inner_prod ( v, w );  }
 
 
double tag::Util::Metric::Constant::Isotropic::inner_prod  // virtual from tag::Util::Metric::Core
( const Cell & P, const std::vector < double > & v, const std::vector < double > & w )
// overrides definition by tag::Util::Metric::Constant, just for a slight speedup

// P not used
	
{	assert ( P .dim() == 0 );
	const double tmp = this->zoom;
	return tmp * tmp * raw_inner_prod ( v, w );  }


double tag::Util::Metric::Constant::Isotropic::inner_prod  // virtual from tag::Util::Metric::Core
( const Cell & P, const Cell & Q, const std::vector < double > & v, const std::vector < double > & w )
// overrides definition by tag::Util::Metric::Constant, just for a slight speedup

// P, Q not used
	
{	assert ( P .dim() == 0 );
	assert ( Q .dim() == 0 );
	const double tmp = this->zoom;
	return tmp * tmp * raw_inner_prod ( v, w );  }


double tag::Util::Metric::Constant::Matrix::Simple::inner_prod  // virtual from tag::Util::Metric::Core
( const std::vector < double > & v, const std::vector < double > & w )

{ size_t n = v .size();
	assert ( n == w .size() );
	assert ( n == Manifold::working. coordinates() .number_of ( tag::components ) );
	assert ( this->matrix .dimensions .size() == 2 );
	assert ( n == this->matrix .dimensions [0] );
	assert ( n == this->matrix .dimensions [1] );
	double res = 0.;
	for ( size_t i = 0; i < n; i++ )
	{	double tmp = 0.;
		for ( size_t j = 0; j < n; j++ )
			tmp += this->matrix (i,j) * v[j];
		res += tmp * w[i];                   }
	return res;                                                              }
 
 
double tag::Util::Metric::Constant::Matrix::SquareRoot::inner_prod
// virtual from tag::Util::Metric::Core
( const std::vector < double > & v, const std::vector < double > & w )

{ size_t n = v .size();
	assert ( n == w .size() );
	assert ( n == Manifold::working. coordinates() .number_of ( tag::components ) );
	assert ( this->matrix .dimensions .size() == 2 );
	assert ( n == this->matrix .dimensions [0] );
	assert ( n == this->matrix .dimensions [1] );
	double res = 0.;
	for ( size_t i = 0; i < n; i++ )
	{	double Mv_i = 0., Mw_i = 0.;
		for ( size_t j = 0; j < n; j++ )
		{	const double tmp = this->matrix (i,j);
			Mv_i += tmp * v[j];
			Mw_i += tmp * w[j];                    }
		res += Mv_i* Mw_i;                          }
	return res;                                                              }
 
 
double tag::Util::Metric::Variable::inner_prod  // virtual from tag::Util::Metric::Core
( const std::vector < double > & v, const std::vector < double > & w )

// since the metric is variable, a point is required
	
{	assert ( false );  return 0.;  }  // we return 0. just to avoid compilation errors

		
double tag::Util::Metric::Variable::Isotropic::inner_prod  // virtual from tag::Util::Metric::Core
( const Cell & P, const std::vector < double > & v, const std::vector < double > & w )

{	assert ( P .dim() == 0 );
	const double tmp = this->zoom (P);
	return tmp * tmp * raw_inner_prod ( v, w );  }


double tag::Util::Metric::Variable::Isotropic::inner_prod  // virtual from tag::Util::Metric::Core
( const Cell & P, const Cell & Q, const std::vector < double > & v, const std::vector < double > & w )
// here overridden just for a slight speedup : uses this->zoom instead of this->length_of_unit
// and calls raw_inner_prod only once

// computes arithmetic mean between value at P and value at Q
	
{	assert ( P .dim() == 0 );
	assert ( Q .dim() == 0 );
	const double tmp_P = this->zoom (P);
	const double tmp_Q = this->zoom (Q);
	return ( tmp_P * tmp_P + tmp_Q * tmp_Q ) * raw_inner_prod ( v, w ) / 2.;  }
// return ( this->inner_prod ( P, v, w ) + this->inner_prod ( Q, v, w ) ) / 2.;
 
 
double tag::Util::Metric::Variable::Matrix::Simple::inner_prod  // virtual from tag::Util::Metric::Core
( const Cell & P, const std::vector < double > & v, const std::vector < double > & w )

{ size_t n = v .size();
	assert ( n == w .size() );
	assert ( n == Manifold::working. coordinates() .number_of ( tag::components ) );
	assert ( this->matrix .dimensions .size() == 2 );
	assert ( n == this->matrix .dimensions [0] );
	assert ( n == this->matrix .dimensions [1] );
	assert ( P .dim() == 0 );
	// matrix_ij * v_j * w_i
	double res = 0.;
	for ( size_t i = 0; i < n; i++ )
	{	double tmp = 0.;
		for ( size_t j = 0; j < n; j++ )
			tmp += this->matrix (i,j) (P) * v[j];
		res += tmp * w[i];                      }
	return res;                                                              }


double tag::Util::Metric::Variable::Matrix::Simple::inner_prod
( const Cell & P, const Cell & Q, const std::vector < double > & v, const std::vector < double > & w )
// virtual from tag::Util::Metric::Core, defined there as if it were isotropic
// so we override it here to give a completely different definition

{ size_t n = v .size();
	assert ( n == w .size() );
	assert ( n == Manifold::working. coordinates() .number_of ( tag::components ) );
	assert ( this->matrix .dimensions .size() == 2 );
	assert ( n == this->matrix .dimensions [0] );
	assert ( n == this->matrix .dimensions [1] );
	assert ( P .dim() == 0 );
	// matrix_ij * v_j * w_i
	double res = 0.;
	for ( size_t i = 0; i < n; i++ )
	{	double tmp = 0.;
		for ( size_t j = 0; j < n; j++ )
		{	const Function & matrix_ij = this->matrix (i,j);
			tmp += ( matrix_ij (P) + matrix_ij (Q) ) * v[j];  }
		res += tmp * w[i];                                     }
	return res / 2.;                                                           }


double tag::Util::Metric::Variable::Matrix::SquareRoot::inner_prod  
( const Cell & P, const std::vector < double > & v, const std::vector < double > & w )
// virtual from tag::Util::Metric::Core, defined by tag::Util::Metric::Variable::Matrix
// we override it here to give a completely different definition
	
{ size_t n = v .size();
	assert ( n == w .size() );
	assert ( n == Manifold::working. coordinates() .number_of ( tag::components ) );
	assert ( this->matrix .dimensions .size() == 2 );
	assert ( n == this->matrix .dimensions [0] );
	assert ( n == this->matrix .dimensions [1] );
	assert ( P .dim() == 0 );
	// matrix_jk * v_j * matrix_ik * w_i
	double res = 0.;
	for ( size_t k = 0; k < n; k++ )
	{	double Mv_k = 0., Mw_k = 0.;
		for ( size_t i = 0; i < n; i++ )  // i can be seen as j too
		{	const double tmp = this->matrix (i,k) (P);
			Mv_k += tmp * v[i];
			Mw_k += tmp * w[i];                        }
		res += Mv_k * Mw_k;                             }
	return res;                                                              }


double tag::Util::Metric::Variable::Matrix::SquareRoot::inner_prod
( const Cell & P, const Cell & Q, const std::vector < double > & v, const std::vector < double > & w )
// virtual from tag::Util::Metric::Core, defined by tag::Util::Metric::Variable::Matrix
// we override it here to give a completely different definition

{ size_t n = v .size();
	assert ( n == w .size() );
	assert ( n == Manifold::working. coordinates() .number_of ( tag::components ) );
	assert ( this->matrix .dimensions .size() == 2 );
	assert ( n == this->matrix .dimensions [0] );
	assert ( n == this->matrix .dimensions [1] );
	assert ( P .dim() == 0 );
	// matrix_jk * v_j * matrix_ik * w_i
	double res = 0.;
	for ( size_t k = 0; k < n; k++ )
	{	double Mv_k = 0., Mw_k = 0.;
		for ( size_t i = 0; i < n; i++ )  // i can be seen as j too
		{	const Function & matrix_ik = this->matrix (i,k);
			const double tmp = matrix_ik (P) + matrix_ik (Q);
			Mv_k += tmp * v[i];
			Mw_k += tmp * w[i];                               }
		res += Mv_k * Mw_k;                                    }
	return res / 4.;                                                               }


double tag::Util::Metric::Variable::Matrix::ISR::inner_prod  // virtual from tag::Util::Metric::Core
( const Cell & P, const std::vector < double > & v, const std::vector < double > & w )
// virtual from tag::Util::Metric::Core, defined by tag::Util::Metric::Variable::Matrix
// we override it here to give a completely different definition

{ size_t n = v .size();
	assert ( n == w .size() );
	assert ( n == Manifold::working. coordinates() .number_of ( tag::components ) );
	assert ( this->matrix .dimensions .size() == 2 );
	assert ( n == this->matrix .dimensions [0] );
	assert ( n == this->matrix .dimensions [1] );
	assert ( P .dim() == 0 );
	// isr * v_i * w_i + matrix_ij * v_j * w_i
	double res = 0.;
	for ( size_t i = 0; i < n; i++ )
	{	res += this->isr (P) * v[i] * w[i];
		double tmp = 0.;
		for ( size_t j = 0; j < n; j++ )
			tmp += this->matrix (i,j) (P) * v[j];
		res += tmp * w[i];                      }
	return res;                                                              }


double tag::Util::Metric::Variable::Matrix::ISR::inner_prod
( const Cell & P, const Cell & Q, const std::vector < double > & v, const std::vector < double > & w )
// virtual from tag::Util::Metric::Core, defined by tag::Util::Metric::Variable::Matrix
// we override it here to give a completely different definition

{ size_t n = v .size();
	assert ( n == w .size() );
	assert ( n == Manifold::working. coordinates() .number_of ( tag::components ) );
	assert ( this->matrix .dimensions .size() == 2 );
	assert ( n == this->matrix .dimensions [0] );
	assert ( n == this->matrix .dimensions [1] );
	assert ( P .dim() == 0 );
	// isr * v_i * w_i + matrix_ij * v_j * w_i
	const double isr_loc = this->isr (P) + this->isr (Q);
	double res = 0.;
	for ( size_t i = 0; i < n; i++ )
	{	res += isr_loc * v[i] * w[i];
		double tmp = 0.;
		for ( size_t j = 0; j < n; j++ )
		{	const Function & matrix_ij = this->matrix (i,j);
			tmp += ( matrix_ij (P) + matrix_ij (Q) ) * v[j];  }
		res += tmp * w[i];                                     }
	return res / 2.;                                                           }


double tag::Util::Metric::Variable::Matrix::SquareRoot::ISR::inner_prod  
( const Cell & P, const std::vector < double > & v, const std::vector < double > & w )
// virtual from tag::Util::Metric::Core, defined by tag::Util::Metric::Variable::Matrix
// we override it here to give a completely different definition
	
{ size_t n = v .size();
	assert ( n == w .size() );
	assert ( n == Manifold::working. coordinates() .number_of ( tag::components ) );
	assert ( this->matrix .dimensions .size() == 2 );
	assert ( n == this->matrix .dimensions [0] );
	assert ( n == this->matrix .dimensions [1] );
	assert ( P .dim() == 0 );
	// ( isr * delta_jk + matrix_jk ) * v_j * ( isr * delta_ik + matrix_ik ) * w_i
	double isr_loc = this->isr (P);
	double res = 0.;
	for ( size_t k = 0; k < n; k++ )
	{	double Mv_k = 0., Mw_k = 0.;
		for ( size_t i = 0; i < n; i++ )  // i can be seen as j too
		{ double tmp = this->matrix (i,k) (P);
			if ( i == k )  tmp += isr_loc;
			Mv_k += tmp * v[i];
			Mw_k += tmp * w[i];                        }
		res += Mv_k * Mw_k;                             }
	return res;                                                              }


double tag::Util::Metric::Variable::Matrix::SquareRoot::ISR::inner_prod
( const Cell & P, const Cell & Q, const std::vector < double > & v, const std::vector < double > & w )
// virtual from tag::Util::Metric::Core, defined by tag::Util::Metric::Variable::Matrix
// we override it here to give a completely different definition

{ size_t n = v .size();
	assert ( n == w .size() );
	assert ( n == Manifold::working. coordinates() .number_of ( tag::components ) );
	assert ( this->matrix .dimensions .size() == 2 );
	assert ( n == this->matrix .dimensions [0] );
	assert ( n == this->matrix .dimensions [1] );
	assert ( P .dim() == 0 );
	// ( isr * delta_jk + matrix_jk ) * v_j * ( isr * delta_ik + matrix_ik ) * w_i
	double isr_loc = this->isr (P) + this->isr (Q);
	double res = 0.;
	for ( size_t k = 0; k < n; k++ )
	{	double Mv_k = 0., Mw_k = 0.;
		for ( size_t i = 0; i < n; i++ )  // i can be seen as j too
		{	const Function & matrix_ik = this->matrix (i,k);
			double tmp = matrix_ik (P) + matrix_ik (Q);
			if ( i == k )  tmp += isr_loc;
			Mv_k += tmp * v[i];
			Mw_k += tmp * w[i];                               }
		res += Mv_k * Mw_k;                                    }
	return res / 4.;                                                               }


double tag::Util::Metric::ScaledSq::inner_prod  // virtual from tag::Util::Metric
( const Cell & P, const std::vector < double > & v, const std::vector < double > & w )

{	assert ( P .dim() == 0 );
	double tmp = this->zoom (P);
	assert ( this->base .core );
	return this->base .core->inner_prod ( P, v, w ) * tmp * tmp;  }


double tag::Util::Metric::ScaledSq::inner_prod  // virtual from tag::Util::Metric
( const Cell & P, const Cell & Q, const std::vector < double > & v, const std::vector < double > & w )

{	return ( this->inner_prod ( P, v, w ) + this->inner_prod ( Q, v, w ) ) / 2.;  }

		 	
double tag::Util::Metric::ScaledSqInv::inner_prod  // virtual from tag::Util::Metric
( const Cell & P, const std::vector < double > & v, const std::vector < double > & w )

{	assert ( P .dim() == 0 );
	double tmp = this->zoom (P);
	assert ( this->base .core );
	return this->base .core->inner_prod ( P, v, w ) / tmp / tmp;  }


double tag::Util::Metric::ScaledSqInv::inner_prod  // virtual from tag::Util::Metric
( const Cell & P, const Cell & Q, const std::vector < double > & v, const std::vector < double > & w )

{	return ( this->inner_prod ( P, v, w ) + this->inner_prod ( Q, v, w ) ) / 2.;  }

#endif  // ifndef MANIFEM_NO_FRONTAL

//------------------------------------------------------------------------------------------------------//


#ifndef MANIFEM_NO_FRONTAL

tag::Util::Metric::Core * tag::Util::Metric::Trivial::scale_sq ( const double f )
// virtual from tag::Util::Metric
{ return new tag::Util::Metric::Constant::Isotropic ( f );  }


tag::Util::Metric::Core * tag::Util::Metric::Trivial::scale_sq ( const Function & f )
// virtual from tag::Util::Metric
{ return new tag::Util::Metric::Variable::Isotropic ( f );  }


tag::Util::Metric::Core * tag::Util::Metric::Constant::Isotropic::scale_sq ( const double f )
// virtual from tag::Util::Metric
{	return new tag::Util::Metric::Constant::Isotropic ( this->zoom * f );  }


tag::Util::Metric::Core * tag::Util::Metric::Constant::Isotropic::scale_sq ( const Function & f )
// virtual from tag::Util::Metric
{ return new tag::Util::Metric::Variable::Isotropic ( this->zoom * f );  }


tag::Util::Metric::Core * tag::Util::Metric::Constant::Matrix::Simple::scale_sq ( const double f )
// virtual from tag::Util::Metric

{ tag::Util::Metric::Constant::Matrix * res = tag::Util::assert_cast
		< tag::Util::Metric::Core*, tag::Util::Metric::Constant::Matrix* > ( this->clone() );
	const double sq_f = f * f;
	assert ( res->matrix .dimensions .size() == 2 );
	size_t   n =  res->matrix .dimensions [0];
	assert ( n == res->matrix .dimensions [1] );
	for ( size_t i = 0; i < n; i++ )
	for ( size_t j = 0; j < n; j++ )  res->matrix (i,j) *= sq_f;
	res->isr *= f;
	return res;                                                                               }


tag::Util::Metric::Core * tag::Util::Metric::Constant::Matrix::Simple::scale_sq ( const Function & f )
// virtual from tag::Util::Metric
{ assert ( false );  return nullptr;  }


tag::Util::Metric::Core * tag::Util::Metric::Constant::Matrix::SquareRoot::scale_sq ( const double f )
// virtual from tag::Util::Metric::Core, defined by tag::Util::Metric::Constant::Matrix
// we override it here to give a completely different definition

{ tag::Util::Metric::Constant::Matrix * res = tag::Util::assert_cast
		< tag::Util::Metric::Core*, tag::Util::Metric::Constant::Matrix* > ( this->clone() );
	assert ( res->matrix .dimensions .size() == 2 );
	size_t   n =  res->matrix .dimensions [0];
	assert ( n == res->matrix .dimensions [1] );
	for ( size_t i = 0; i < n; i++ )
	for ( size_t j = 0; j < n; j++ )  res->matrix (i,j) *= f;
	res->isr *= f;
	return res;                                                                               }


tag::Util::Metric::Core * tag::Util::Metric::Constant::Matrix::SquareRoot::scale_sq ( const Function & f )
// virtual from tag::Util::Metric::Core, defined by tag::Util::Metric::Constant::Matrix
{ assert ( false );  return nullptr;  }


tag::Util::Metric::Core * tag::Util::Metric::Variable::Isotropic::scale_sq ( const double f )
// virtual from tag::Util::Metric
{	return new tag::Util::Metric::Variable::Isotropic ( this->zoom * f  );  }


tag::Util::Metric::Core * tag::Util::Metric::Variable::Isotropic::scale_sq ( const Function & f )
// virtual from tag::Util::Metric
{ return new tag::Util::Metric::Variable::Isotropic ( this->zoom * f );  }


tag::Util::Metric::Core * tag::Util::Metric::Variable::Matrix::scale_sq ( const double f )
// virtual from tag::Util::Metric
{	return new tag::Util::Metric::ScaledSq
		( tag::Util::Metric ( tag::whose_core_is, this->clone() ), f );  }


tag::Util::Metric::Core * tag::Util::Metric::Variable::Matrix::scale_sq ( const Function & f )
// virtual from tag::Util::Metric
{	return new tag::Util::Metric::ScaledSq
		( tag::Util::Metric ( tag::whose_core_is, this->clone() ), f );  }


tag::Util::Metric::Core * tag::Util::Metric::ScaledSq::scale_sq ( const double f )
// virtual from tag::Util::Metric
{	return new tag::Util::Metric::ScaledSq ( this->base, this->zoom * f );  }


tag::Util::Metric::Core * tag::Util::Metric::ScaledSq::scale_sq ( const Function & f )
// virtual from tag::Util::Metric
{	return new tag::Util::Metric::ScaledSq ( this->base, this->zoom * f );  }


tag::Util::Metric::Core * tag::Util::Metric::ScaledSqInv::scale_sq ( const double f )
// virtual from tag::Util::Metric
{	return new tag::Util::Metric::ScaledSq ( this->base, f / this->zoom );  }


tag::Util::Metric::Core * tag::Util::Metric::ScaledSqInv::scale_sq ( const Function & f )
// virtual from tag::Util::Metric
{	return new tag::Util::Metric::ScaledSq ( this->base, f / this->zoom );  }


#else  // MANIFEM_NO_FRONTAL

tag::Util::Metric::Core * tag::Util::Metric::Trivial::scale_sq ( const double f )
// virtual from tag::Util::Metric
{ assert ( false );
  return new tag::Util::Metric::Trivial();  }


tag::Util::Metric::Core * tag::Util::Metric::Trivial::scale_sq ( const Function & f )
// virtual from tag::Util::Metric
{ assert ( false );
  return new tag::Util::Metric::Trivial();  }


#endif  // ifndef MANIFEM_NO_FRONTAL

//------------------------------------------------------------------------------------------------------//


#ifndef MANIFEM_NO_FRONTAL

tag::Util::Metric::Core * tag::Util::Metric::Trivial::scale_sq_inv ( const double f )
// virtual from tag::Util::Metric
{ return new tag::Util::Metric::Constant::Isotropic ( 1. / f );  }


tag::Util::Metric::Core * tag::Util::Metric::Trivial::scale_sq_inv ( const Function & f )
// virtual from tag::Util::Metric
{ return new tag::Util::Metric::Variable::Isotropic ( 1. / f );  }


tag::Util::Metric::Core * tag::Util::Metric::Constant::Isotropic::scale_sq_inv ( const double f )
// virtual from tag::Util::Metric
{	return new tag::Util::Metric::Constant::Isotropic ( this->zoom / f );  }


tag::Util::Metric::Core * tag::Util::Metric::Constant::Isotropic::scale_sq_inv ( const Function & f )
// virtual from tag::Util::Metric
{ return new tag::Util::Metric::Variable::Isotropic ( this->zoom / f );  }


tag::Util::Metric::Core * tag::Util::Metric::Constant::Matrix::Simple::scale_sq_inv ( const double f )
// virtual from tag::Util::Metric

{ tag::Util::Metric::Constant::Matrix * res = tag::Util::assert_cast
		< tag::Util::Metric::Core*, tag::Util::Metric::Constant::Matrix* > ( this->clone() );
	const double sq_f = f * f;
	assert ( res->matrix .dimensions .size() == 2 );
	size_t   n =  res->matrix .dimensions [0];
	assert ( n == res->matrix .dimensions [1] );
	for ( size_t i = 0; i < n; i++ )
	for ( size_t j = 0; j < n; j++ )  res->matrix (i,j) /= sq_f;
	res->isr /= f;
	return res;                                                                               }


tag::Util::Metric::Core * tag::Util::Metric::Constant::Matrix::Simple::scale_sq_inv ( const Function & f )
// virtual from tag::Util::Metric
{ assert ( false );  return nullptr;  }


tag::Util::Metric::Core * tag::Util::Metric::Constant::Matrix::SquareRoot::scale_sq_inv ( const double f )
// virtual from tag::Util::Metric::Core, defined by tag::Util::Metric::Constant::Matrix
// we override it here to give a completely different definition

{ tag::Util::Metric::Constant::Matrix * res = tag::Util::assert_cast
		< tag::Util::Metric::Core*, tag::Util::Metric::Constant::Matrix* > ( this->clone() );
	assert ( res->matrix .dimensions .size() == 2 );
	size_t   n =  res->matrix .dimensions [0];
	assert ( n == res->matrix .dimensions [1] );
	for ( size_t i = 0; i < n; i++ )
	for ( size_t j = 0; j < n; j++ )  res->matrix (i,j) /= f;
	res->isr /= f;
	return res;                                                                               }


tag::Util::Metric::Core * tag::Util::Metric::Constant::Matrix::SquareRoot::scale_sq_inv
( const Function & f )  // virtual from tag::Util::Metric
{ assert ( false );  return nullptr;  }


tag::Util::Metric::Core * tag::Util::Metric::Variable::Isotropic::scale_sq_inv ( const double f )
// virtual from tag::Util::Metric
{ return new tag::Util::Metric::Variable::Isotropic ( this->zoom / f );  }


tag::Util::Metric::Core * tag::Util::Metric::Variable::Isotropic::scale_sq_inv ( const Function & f )
// virtual from tag::Util::Metric
{ return new tag::Util::Metric::Variable::Isotropic ( this->zoom / f );  }


tag::Util::Metric::Core * tag::Util::Metric::Variable::Matrix::scale_sq_inv ( const double f )
// virtual from tag::Util::Metric
{	return new tag::Util::Metric::ScaledSqInv
		( tag::Util::Metric ( tag::whose_core_is, this->clone() ), f );  }


tag::Util::Metric::Core * tag::Util::Metric::Variable::Matrix::scale_sq_inv ( const Function & f )
// virtual from tag::Util::Metric
{	return new tag::Util::Metric::ScaledSqInv
		( tag::Util::Metric ( tag::whose_core_is, this->clone() ), f );  }


tag::Util::Metric::Core * tag::Util::Metric::ScaledSq::scale_sq_inv ( const double f )
// virtual from tag::Util::Metric
{	return new tag::Util::Metric::ScaledSq ( this->base, this->zoom / f );  }


tag::Util::Metric::Core * tag::Util::Metric::ScaledSq::scale_sq_inv ( const Function & f )
// virtual from tag::Util::Metric
{	return new tag::Util::Metric::ScaledSq ( this->base, this->zoom / f );  }


tag::Util::Metric::Core * tag::Util::Metric::ScaledSqInv::scale_sq_inv ( const double f )
// virtual from tag::Util::Metric
{	return new tag::Util::Metric::ScaledSqInv ( this->base, this->zoom * f );  }


tag::Util::Metric::Core * tag::Util::Metric::ScaledSqInv::scale_sq_inv ( const Function & f )
// virtual from tag::Util::Metric
{	return new tag::Util::Metric::ScaledSqInv ( this->base, this->zoom * f );  }


#else  // MANIFEM_NO_FRONTAL

tag::Util::Metric::Core * tag::Util::Metric::Trivial::scale_sq_inv ( const double f )
// virtual from tag::Util::Metric
{ assert ( false );
  return new tag::Util::Metric::Trivial();  }


tag::Util::Metric::Core * tag::Util::Metric::Trivial::scale_sq_inv ( const Function & f )
// virtual from tag::Util::Metric
{ assert ( false );
  return new tag::Util::Metric::Trivial();  }


#endif  // ifndef MANIFEM_NO_FRONTAL

//------------------------------------------------------------------------------------------------------//


		 	
 
 	
