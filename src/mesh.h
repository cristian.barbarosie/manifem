
//   mesh.h  2025.03.08

//   This file is part of maniFEM, a C++ library for meshes and finite elements on manifolds.

//   Copyright  2019 - 2025  Cristian Barbarosie  cristian.barbarosie@gmail.com

//   https://maniFEM.rd.ciencias.ulisboa.pt/
//   https://codeberg.org/cristian.barbarosie/maniFEM

//   ManiFEM is free software: you can redistribute it and/or modify it
//   under the terms of the GNU Lesser General Public License as published
//   by the Free Software Foundation, either version 3 of the License
//   or (at your option) any later version.

//   ManiFEM is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//   See the GNU Lesser General Public License for more details.

//   You should have received a copy of the GNU Lesser General Public License
//   along with maniFEM.  If not, see <https://www.gnu.org/licenses/>.


#ifndef MANIFEM_MESH_H
#define MANIFEM_MESH_H

#include <iostream>
#include <string>

#include <vector>
#include <list>
#include <deque>
#include <map>

#include <memory>
#include "math.h"
#include "assert.h"

//  src/field.h  included later


namespace maniFEM {

// tags are used to distinguish between different versions of the same function or method or constructor
// they create a sort of spoken language ...
	
namespace tag {  // see paragraph 12.3 in the manual

	// tags are listed in no particular order
	
	struct Positive { };  static const Positive positive;
	struct PositiveCell { };  static const PositiveCell positive_cell;
	struct IsPositive { };  static const IsPositive is_positive;
	struct IsNegative { };  static const IsNegative is_negative;
	struct ThisMeshIsPositive { };  static const ThisMeshIsPositive this_mesh_is_positive;
	struct ThisMeshIsNegative { };  static const ThisMeshIsNegative this_mesh_is_negative;
	struct ThisCellIsPositive { };  static const ThisCellIsPositive this_cell_is_positive;
	struct RequireOrder { };  static const RequireOrder require_order;
	// struct ReverseOrder { };  static const ReverseOrder reverse_order;
	// struct ReverseOrderIfAny { };  static const ReverseOrderIfAny reverse_order_if_any;
	struct Backwards { };  static const Backwards backwards;
	                       static const Backwards backward;
	struct ReverseOf { };  static const ReverseOf reverse_of;
	struct Reversed { };  static const Reversed reversed;
	struct Around { };  static const Around around;
	struct ReverseEachCell { };  static const ReverseEachCell reverse_each_cell;
	struct HasNoReverse { };  static const HasNoReverse has_no_reverse;
	struct AsFound { };  static const AsFound as_found;
	struct NonExistent { };  static const NonExistent non_existent;
	struct BuildIfNotExists { };  static const BuildIfNotExists build_if_not_exists;
	struct ReplaceIfPreviouslyExists { };
	static const ReplaceIfPreviouslyExists replace_if_previously_exists;
	struct SeenFrom { };  static const SeenFrom seen_from;
	struct Fuzzy { };  static const Fuzzy fuzzy;
	struct stsi { };  static const stsi STSI;
	struct ConnectedOneDim { };  static const ConnectedOneDim connected_one_dim;
	struct MakeFuzzy { };  static const MakeFuzzy make_fuzzy;
	struct MakeConnectedOneDim { };  static const MakeConnectedOneDim make_connected_one_dim;
	struct MayNotExist { };  static const MayNotExist may_not_exist;
	struct DoesNotExist { };  static const DoesNotExist does_not_exist;
	struct ReturnNewMesh { };  static const ReturnNewMesh return_new_mesh;
	struct FromPositiveMesh { };  static const FromPositiveMesh from_positive_mesh;
	struct BuildNegative { };  static const BuildNegative build_negative;
	struct BuildNow { };  static const BuildNow build_now;
	struct DoNotBuildCells { };  static const DoNotBuildCells do_not_build_cells;
	struct DoNotBother { };  static const DoNotBother do_not_bother;
	struct KeepAsFirstVer { };  static const KeepAsFirstVer keep_as_first_ver;
	struct SurelyExists { };  static const SurelyExists surely_exists;
	struct SurelyFound { };  static const SurelyFound surely_found;
	struct SurelySingular { };  static const SurelySingular surely_singular;
	struct ReverseCellsSurelyExist { };
	static const ReverseCellsSurelyExist reverse_cells_surely_exist;
	struct InnerVertex { };  static const InnerVertex inner_vertex;
	struct OnlyIfVertexIsInner { };  static const OnlyIfVertexIsInner only_if_vertex_is_inner;
	struct ExactlyOne { };  static const ExactlyOne exactly_one;
	struct OfDimension { };  static const OfDimension of_dim;
	                         static const OfDimension of_dimension;
	struct OfMaxDim { };  static const OfMaxDim of_max_dim;
	struct SameDim { };  static const SameDim same_dim;  static const SameDim of_same_dim;
	                     static const SameDim same_dimension;  static const SameDim of_same_dimension;
	struct MeshHasDimOne { };  static const MeshHasDimOne mesh_has_dim_one;
	struct MeshHasDimTwo { };  static const MeshHasDimOne mesh_has_dim_two;
	struct MeshHasHighDim { };  static const MeshHasHighDim mesh_has_high_dim;
	struct NeighbRels { };  static const NeighbRels neighb_rels;
	                        static const NeighbRels neighbourhood_relations;
	struct Force { };  static const Force force;
	struct CellHasLowDim { }; static const CellHasLowDim cell_has_low_dim;
	struct MinusOne { };  static const MinusOne minus_one;
	struct GreaterThanOne { };  static const GreaterThanOne greater_than_one;
	struct MightBeOne { };  static const MightBeOne might_be_one;
	struct Oriented { };  static const Oriented oriented;
	struct NotOriented { };  static const NotOriented not_oriented;
	// struct DeepCopyOf { };  static const DeepCopyOf deep_copy_of;
	struct Depth { };  static const Depth depth;
	struct Infinity { };  static const Infinity infinity;
	struct BuildCellsIfNec { };  static const BuildCellsIfNec build_cells_if_necessary;
	struct BuildNewVertices { };  static const BuildNewVertices build_new_vertices;
	struct UseExistingVertices { };  static const UseExistingVertices use_existing_vertices;
	struct Frontal { };  static const Frontal frontal;
	struct StartAt { };  static const StartAt start_at;
	struct StopAt { };  static const StopAt stop_at;
	struct On { };  static const On on;
	struct Towards { };  static const Towards towards;
	struct To { };  static const To to;
	struct Along { };  static const Along along;
	struct NormalVector { };  static const NormalVector normal_vect;
	struct OfCoordinates { };  static const OfCoordinates of_coords, of_coordinates;
	struct Boundary { };  static const Boundary boundary;
	struct BoundaryOf { };  static const BoundaryOf boundary_of;
	struct Domain { };  static const Domain domain;
	struct OnBoundary { };  static const OnBoundary on_boundary;
	struct SizeMeshes { };  static const SizeMeshes size_meshes;
	struct BehindFace { };  static const BehindFace behind_face;
	struct InFrontOfFace { };  static const InFrontOfFace in_front_of_face;
	struct WithinMesh { };  static const WithinMesh within_mesh;
	struct WithinSTSIMesh { };  static const WithinSTSIMesh within_STSI_mesh;
	struct Vertices { };  static const Vertices vertices;
	struct Segments { };  static const Segments segments;
	struct Over { };  static const Over over;
	struct OverVertices { };  static const OverVertices over_vertices;
	struct OverSegments { };  static const OverSegments over_segments;
	struct CellsOfDim { };  static const CellsOfDim cells_of_dim;
	struct CellsOfMaxDim { };  static const CellsOfMaxDim cells_of_max_dim;
	struct OverCells { };  static const OverCells over_cells;
	struct OverCellsOfDim { };  static const OverCellsOfDim over_cells_of_dim;
	struct OverCellsOfMaxDim { };  static const OverCellsOfMaxDim over_cells_of_max_dim;
	struct OverCellsOfReverseOf { };  static const OverCellsOfReverseOf over_cells_of_reverse_of;
	struct PointingTowards { };  static const PointingTowards pointing_towards;
	struct PointingAwayFrom { };  static const PointingAwayFrom pointing_away_from;
	struct WhoseTipIs { };  static const WhoseTipIs whose_tip_is;
	struct WhoseBaseIs { };  static const WhoseBaseIs whose_base_is;
	struct Connected { };  static const Connected connected;
	struct ConnectedTo { };  static const ConnectedTo connected_to;
	struct OverMeshesAbove { };  static const OverMeshesAbove over_meshes_above;
	struct MeshesOfDim { };  static const MeshesOfDim meshes_of_dim;
	                         static const MeshesOfDim over_meshes_of_dim;
	struct OneDim { };  static const OneDim one_dim;
	struct HighDim { };  static const HighDim high_dim;
	struct Vertex { };  static const Vertex vertex; static const Vertex point;
	struct Segment { };  static const Segment segment;
	struct DividedIn { };  static const DividedIn divided_in;
	struct Triangle { };  static const Triangle triangle;
	struct Quadrangle { };  static const Quadrangle quadrangle;
	                        static const Quadrangle quadrilateral;
	struct Parallelogram { };  static const Parallelogram parallelogram;
	struct Rectangle { };  static const Rectangle rectangle;
	struct Square { };  static const Square square;
	struct Pentagon { };  static const Pentagon pentagon;
	struct Hexagon { };  static const Hexagon hexagon;
	struct Tetrahedron { };  static const Tetrahedron tetrahedron;
	struct Hexahedron { };  static const Hexahedron hexahedron, parallelipiped, cube;
	struct WithMaster { };  static const WithMaster with_master;
	struct WhoseBoundaryIs { };  static const WhoseBoundaryIs whose_bdry_is;
	                             static const WhoseBoundaryIs whose_boundary_is;
	struct WhoseCoreIs { };  static const WhoseCoreIs whose_core_is;
	struct ForcePositive { };  static const ForcePositive force_positive;
	struct HasSize { };  static const HasSize has_size;
	struct ReserveSize { };  static const ReserveSize reserve_size;
	struct Pretty { };  static const Pretty pretty;
	struct Project { };  static const Project project;
	struct Identify { };  static const Identify identify;
	struct From { };  static const From from;
	struct With { };  static const With with;
	struct Within { };  static const Within within;
	struct Replace { };  static const Replace replace;
	struct By { };  static const By by;
	struct OfDegree { };  static const OfDegree of_degree;
	struct MeshIsBdry { };  static const MeshIsBdry mesh_is_bdry;
	struct MeshIsNotBdry { };  static const MeshIsNotBdry mesh_is_not_bdry;
	enum WithTriangles { with_triangles, not_with_triangles };
	struct Join { };  static const Join join;
	struct Grid { };  static const Grid grid, patch;
	struct Extrude { };  static const Extrude extrude;
	struct Gather { };  static const Gather gather;
	enum KeyForHook { tangent_vector, normal_vector, node_in_cloud,
	                  isr_inv_matrix, isr_eigen_dir, isr_radius, sqrt_det };
	struct Onto { };  static const Onto onto;
	struct EntireManifold { };  static const EntireManifold entire_manifold;
	struct DesiredLength { };  static const DesiredLength desired_length;
	struct Orientation { };  static const Orientation orientation;
	struct OrientCompWithCenter { };  static const OrientCompWithCenter orientation_compatible_with_center;
	struct CompatWithCenter { };  static const CompatWithCenter compatible_with_center;
	struct OrientOpposToCenter { };  static const OrientOpposToCenter orientation_opposed_to_center;
	struct OpposToCenter { };  static const OpposToCenter opposed_to_center;
	struct OpposedTo { };  static const OpposedTo opposed_to;
	struct Center { };  static const Center center;
	struct OrientCompWithMesh { };  static const OrientCompWithMesh orientation_compatible_with_mesh;
	struct OrientOpposToMesh { };  static const OrientOpposToMesh orientation_opposed_to_mesh;
	struct OrientCompWithCell { };  static const OrientCompWithCell orientation_compatible_with_cell;
	struct OrientOpposToCell { };  static const OrientOpposToCell orientation_opposed_to_cell;
	struct OrientationIrrelevant { };  static const OrientationIrrelevant orientation_irrelevant;
	struct CompatWithMesh { };  static const CompatWithMesh compatible_with_mesh;
	struct OpposToMesh { };  static const OpposToMesh opposed_to_mesh;
	struct CompatWith { };  static const CompatWith compatible_with;
	struct MEsh { };  static const MEsh mesh;
	enum OrientationChoice { random, intrinsic, inherent, geodesic, not_provided };
	struct ShortestPath { };  static const ShortestPath shortest_path;
	struct ImportFromFile { };  static const ImportFromFile import_from_file;
	struct postscript { };  static const postscript PostScript;  static const postscript eps;
	struct postscript3d { };  static const postscript3d PostScript3d;  static const postscript3d eps3d;
	struct Gmsh { };  static const Gmsh gmsh;
	struct ImportNumbering { };  static const ImportNumbering import_numbering;
	struct Winding { };  static const Winding winding;
	struct Singular { };  static const Singular singular;
	struct Unfold { };  static const Unfold unfold;
	struct ReturnMapBetween { };  static const ReturnMapBetween return_map_between;
	struct OverRegion { };  static const OverRegion over_region;
	struct OneGenerator { };  static const OneGenerator one_generator;
	struct TwoGenerators { };  static const TwoGenerators two_generators;
	struct ThreeGenerators { };  static const ThreeGenerators three_generators;
	struct ShadowVertices { };  static const ShadowVertices shadow_vertices;
	struct lagrange { };  static const lagrange Lagrange;
	struct Components { };  static const Components components;
	struct Map { };  static const Map map;
	struct Field { };  static const Field field;
  
	struct Util
	{ template < class T > class Wrapper;
		class Core;
		class CellCore;  //  aka  class Cell::Core
		class MeshCore;  //  aka  class Mesh::Core
		class CellIterator;  // aka  class Mesh::Iterator
		class MeshIterator;  // aka  class Cell::Iterator
		class Metric;
		class MetricCore;  // aka  class tag::Util::Metric::Core
		class WeakIntegrator;
		struct Field  {  class Core;  class ShortInt;  class SizeT;  class Double;  };
		static const std::vector < std::vector < std::vector < short int > > >
			ortho_basis_int, pm_ortho_basis_int, directions_int;  // defined in manifold.cpp
		static const double one_third, minus_one_third, one_sixth, minus_one_sixth,
		                    two_thirds, minus_two_thirds, sqrt_2, sqrt_half, sqrt_3,
		                    sqrt_one_third, sqrt_two_thirds, sqrt_three_quarters, sqrt_sixth,
		                    sqrt_1_over_24, fourth_root_three_quarters;  // defined in manifold.cpp
		static const std::vector < std::vector < std::vector < double > > >
			ortho_basis_double, pm_ortho_basis_double, directions_double, directions_few_double,
			directions_L_infty;   // defined in manifold.cpp
		class Action;  //  aka class Function::Action, aka class Manfold::Action
		// we define it in function.h because we need it for Function::MultiValued
		// but we prefer the user to see it as an attribute of class Manifold
		class InequalitySet;  //  aka  class Function::Inequality::Set
		class DelayedIntegral;  // aka  class Function::Form::DelayedIntegral
		template <typename T> class Tensor;
		static void conjugate_gradient   // defined in manifold.cpp
		( std::vector < double > & x, const std::vector < double > constr,
		  const tag::Util::Tensor < double > & grad_constr, std::vector < double > & grad,
		  std::vector < double > & direc, bool first_step                                 );
		inline static double norm_infty ( const std::vector < double > & w );
		inline static void improve_inverse_matrix
		( const Tensor < double > & matrix, Tensor < double > & inv_matr );
		inline static size_t power_method
		( const Tensor < double > & matrix,
		  std::vector < std::vector < double > > & starting_vectors,
		  std::vector < double > & eigenval                         );
		inline static double compute_isr ( const Tensor < double > & matrix );
		inline static size_t assert_diff ( const size_t a, const size_t b )
		{	assert ( a >= b );  return  a - b;  }
		template < typename X, typename Y > inline static Y assert_cast ( X x )
		#ifndef NDEBUG  // DEBUG mode
		{	Y y = dynamic_cast < Y > (x);  assert (y);  return y;  }
		#else  // NDEBUG
		{	Y y = static_cast < Y > (x);  return y;  }
		#endif
		template < typename X, typename Y > inline static std::shared_ptr <Y>
		assert_shared_cast ( std::shared_ptr <X> x )
		#ifndef NDEBUG  // DEBUG mode
		{	std::shared_ptr <Y> y = std::dynamic_pointer_cast < Y > (x);  assert (y);  return y;  }
		#else  // NDEBUG
		{	std::shared_ptr <Y> y = std::static_pointer_cast < Y > (x);  return y;  }
		#endif
		template < typename X, typename Y > inline static const Y assert_const_cast ( const X x )
		#ifndef NDEBUG  // DEBUG mode
		{	const Y y = dynamic_cast < Y* > (x);  assert (y);  return y;  }
		#else  // NDEBUG
		{	const Y y = static_cast < Y* > (x);  return y;  }
		#endif
		template < class container >
		inline static const container::mapped_type & get_element_of_const_map
		( container & cont, const typename container::key_type & key, const tag::SurelyFound & )
		{	typename container::const_iterator it = cont .find ( key );
			assert ( it != cont .end() );
			return  it->second;                                          }
		template < class container >
		inline static container::mapped_type & get_element_of_map
		( container & cont, const typename container::key_type & key, const tag::SurelyFound & )
		{	typename container::iterator it = cont .find ( key );
			assert ( it != cont .end() );
			return  it->second;                                    }
		template < class container >
		inline static void erase_element
		( container & cont, const typename container::key_type & key, const tag::SurelyFound & )
		{	typename container::iterator it = cont .find ( key );
			assert ( it != cont .end() );
			cont .erase ( it );                                    }
		template < class container >  // only makes sens for maps, not for sets
		inline static void replace_elem ( container & cont, const typename container::value_type & elem )
		{	std::pair < typename container::iterator, bool > pair_it_bool = cont .insert ( elem );
			assert ( not pair_it_bool .second );
			pair_it_bool .first->second = elem .second;                                             }
		template < class container >  // only makes sens for maps, not for sets
		inline static void insert_elem_in_map
		( container & cont, const typename container::value_type & elem,
		  const tag::ReplaceIfPreviouslyExists &                        )
		// inspired in item 24 of the book : Scott Meyers, Effective STL
		{	typename container::iterator lb = cont .lower_bound ( elem .first );
			if ( ( lb == cont .end() ) or ( cont .key_comp() ( elem .first, lb->first ) ) )
				cont .insert ( lb, elem );
			else  lb->second = elem .second;                                                 }
		template < class container > 
		inline static container::mapped_type & insert_new_elem_in_map
		( container & cont, const typename container::value_type & elem )
		#ifndef NDEBUG  // DEBUG mode
		{	std::pair < typename container::iterator, bool > pair_it_bool = cont .insert ( elem );
			assert ( pair_it_bool .second );
			return  pair_it_bool .first->second;                                                    }
		#else  // NDEBUG
		{	return  cont .insert ( elem ) .first->second;  }
		#endif
		template < class container > 
		inline static void insert_new_elem_in_set
		( container & cont, const typename container::value_type & elem )
		#ifndef NDEBUG  // DEBUG mode
		{	std::pair < typename container::iterator, bool > pair_it_bool = cont .insert ( elem );
			assert ( pair_it_bool .second );                                                        }
		#else  // NDEBUG
		{	cont .insert ( elem );  }
		#endif
		template < class container, typename... _Args >
		inline static void emplace_new_elem ( container & cont, _Args&&... __args )
		#ifndef NDEBUG  // DEBUG mode
		{	std::pair < typename container::iterator, bool > pair_it_bool =
				cont .emplace ( std::forward<_Args> ( __args )... );
			assert ( pair_it_bool .second );                                 }
		#else  // NDEBUG
		{	cont .emplace ( std::forward<_Args> ( __args )... );  }
		#endif
		static bool return_true ( );
		static bool return_false ( );
		static bool return_bool_forbidden ( );
	};  // end of struct Util
	
	struct MayBeNull { };  static const MayBeNull may_be_null;
	struct SurelyNotNull { };  static const SurelyNotNull surely_not_null;
	// struct FreshlyCreated { };  static const FreshlyCreated freshly_created;
	struct Move { };  static const Move move;
	struct PreviouslyExisting { };  static const PreviouslyExisting previously_existing;
	struct ZeroWrappers { };  static const ZeroWrappers zero_wrappers;
	struct OldCore { };  static const OldCore old_core;
	struct NoNeedToDisposeOf { };  static const NoNeedToDisposeOf no_need_to_dispose_of;
	struct Empty { };  static const Empty empty;
	struct OneDummyWrapper { };  static const OneDummyWrapper one_dummy_wrapper;

}  // end of namespace tag

//------------------------------------------------------------------------------------------------------//


template < typename scalar >
class tag::Util::Tensor

// indices begin at 0
// constructors fill space with 0.

{	// data:
	public:
	std::vector < scalar > elements;
	std::vector < size_t > dimensions, cumulative_dims;
	size_t total_dim;
	
	// constructors:
	inline Tensor ( ) { }
	inline Tensor ( const std::vector < size_t > dims )
	{	this->dimensions = dims;
		this->allocate_space (); }
	inline Tensor ( size_t i, size_t j,
	                size_t k, size_t l )
	{	this->dimensions .push_back (i);
		this->dimensions .push_back (j);
		this->dimensions .push_back (k);
		this->dimensions .push_back (l);
		this->allocate_space ();        }
	inline Tensor ( size_t i, size_t j, size_t k )
	{	this->dimensions .push_back (i);
		this->dimensions .push_back (j);
		this->dimensions .push_back (k);
		this->allocate_space ();        }
	inline Tensor ( size_t i, size_t j )
	{	this->dimensions .push_back (i);
		this->dimensions .push_back (j);
		this->allocate_space ();        }
	inline Tensor ( size_t i )
	{	this->dimensions .push_back (i);
		this->allocate_space ();        }
	inline ~Tensor () { };

	// methods:
	inline void allocate_space ()
	{	assert ( this->elements .size() == 0 );
		this->total_dim = 1;
		std::vector < size_t > ::iterator k;
		for ( k = this->dimensions .begin(); k != this->dimensions .end(); k++)
		{	this->cumulative_dims .push_back ( total_dim );
			this->total_dim *= *k;                         }
		// this->elements .reserve ( total_dim );
		// this->elements .insert ( this->elements .end(), total_dim, 0.0 );
		this->elements .resize ( total_dim );
		assert ( this->elements .size() == this->total_dim );                    }
	inline size_t pointer ( std::vector < size_t > index ) const
	{	assert ( index .size() == this->dimensions .size() );
		size_t point = 0;
		std::vector < size_t > ::const_iterator i, d, cd;
		for ( i = index .begin(), d = this->dimensions .begin(),
		      cd = this->cumulative_dims .begin();
		      i != index.end(); i++, d++, cd++                  )
		{	assert ( d != this->dimensions .end() );
			assert ( cd != this->cumulative_dims .end() );
			assert ( *i < *d );
			point += (*i) * (*cd); }
		return point;                                              }
	inline scalar & operator() ( std::vector < size_t > index )
	{ return ( this->elements [ this->pointer ( index ) ] );  }
	inline const scalar & operator() ( std::vector < size_t > index ) const
	{ return ( this->elements [ this->pointer ( index ) ] );  }
	inline scalar & operator() ( size_t i )
	{	assert ( this->dimensions .size() == 1 );
		std::vector < size_t > index ( 1, i );
		return this->operator() ( index );        }
	inline const scalar & operator() ( size_t i ) const
	{	assert ( this->dimensions .size() == 1 );
		std::vector < size_t > index ( 1, i );
		return operator() ( index );              }
	inline scalar & operator() ( size_t i, size_t j )
	{	assert ( this->dimensions .size() == 2 );
		std::vector < size_t > index;
		index .reserve (2);
		index .push_back (i);
		index .push_back (j);
		return this->operator() ( index );        }
	inline const scalar & operator() ( size_t i, size_t j ) const
	{	assert ( this->dimensions .size() == 2 );
		std::vector < size_t > index;
		index .reserve (2);
		index .push_back (i);
		index .push_back (j);
		return this->operator() ( index );        }
	inline scalar & operator() ( size_t i, size_t j, size_t k )
	{	assert ( this->dimensions .size() == 3 );
		std::vector < size_t > index;
		index .reserve (3);
		index .push_back (i);
		index .push_back (j);
		index .push_back (k);
		return this->operator() ( index );        }
	inline const scalar & operator() ( size_t i, size_t j, size_t k ) const
	{	assert ( this->dimensions .size() == 3 );
		std::vector < size_t > index;
		index .reserve (3);
		index .push_back (i);
		index .push_back (j);
		index .push_back (k);
		return this->operator() ( index );        }
	inline scalar & operator() ( size_t i, size_t j,
	                             size_t k, size_t l )
	{	assert ( this->dimensions .size() == 4 );
		std::vector < size_t > index;
		index .reserve (4);
		index .push_back (i);
		index .push_back (j);
		index .push_back (k);
		index .push_back (l);
		return this->operator() ( index );        }
	inline const scalar & operator()( size_t i, size_t j,
	                                  size_t k, size_t l ) const
	{	assert ( this->dimensions .size() == 4 );
		std::vector < size_t > index;
		index .reserve (4);
		index .push_back (i);
		index .push_back (j);
		index .push_back (k);
		index .push_back (l);
		return this->operator() ( index );        }

};  // end of  class tag::Util::Tensor

//------------------------------------------------------------------------------------------------------//


// wrappers act like shared pointers towards cores
// you may want to forbid copying for your cores

// cores should have virtual destructor

template < class core_type >	class tag::Util::Wrapper 

{	public :

	core_type * core;

	inline Wrapper ( const tag::Empty & ) : core { nullptr }  { }

	inline Wrapper ( const tag::WhoseCoreIs &, core_type * c, const tag::Move & )
	: core { c }
	{	}
	
	inline Wrapper ( const tag::WhoseCoreIs &, core_type * c,
	                 const tag::PreviouslyExisting &, const tag::SurelyNotNull & )
	: core { c }
	{	assert ( c );  c->nb_of_wrappers ++;  }
	
	inline Wrapper ( const tag::WhoseCoreIs &, core_type * c,
	                 const tag::PreviouslyExisting &, const tag::MayBeNull )
	: core { c }
	{	if ( c )  c->nb_of_wrappers ++;  }
	
	inline ~Wrapper ( )
	{	this->dispose_core ( tag::may_be_null );  }

	inline Wrapper operator= ( const Wrapper & w )
	{	dispose_core ( tag::may_be_null );
		set_core ( w .core, tag::may_be_null );
		return *this;                           }
	
	inline void dispose_core ( const tag::MayBeNull & )
	{	if ( this->core )
			if ( this->core->dispose_query () )
				delete this->core;                 }
	
	inline void dispose_core ( const tag::SurelyNotNull & )
	{	assert ( this->core );
		if ( this->core->dispose_query () )
			delete this->core;                 }
	
	class Inactive;

};  // end of class tag::Util::Wrapper


// class tag::Util::Core is intended to be used by constructing
// your own Core class which inherits from tag::Util::Core
// you will probably implement polymorphic cores
// we declare the destructors virtual, thus making the class polymorphic
// this is useful if you want to do things like
//   tag::Util::Core * c = pointer_to_your_own_type_of_core;
//   ... some code ...
//   delete c;
// also useful for dynamic_cast
// see e.g. item 7 in the book of Scott Meyers, Effective C++

// more often than not, cores are built with nb_of_wrappers = 1
// although they did not meet any wrapper yet (they are newborn)
// we assume that the 'new' command was issued by a wrapper
// who is waiting for them to see the light of day and is eager to embrace them

class tag::Util::Core

{	public :

	size_t nb_of_wrappers;

	inline Core ( const tag::OneDummyWrapper & ) : nb_of_wrappers { 1 } { }
	// a new core is often built just before (or within a constructor for) a wrapper for it
	// thus, we initialize with one instead of zero

	inline Core ( const tag::ZeroWrappers & ) : nb_of_wrappers { 0 } { }
	// in some cases, a core is built without a wrapper
	// like for negative cells
	
	virtual ~Core ( ) { }

	inline bool dispose_query ( );

	class DelegateDispose;
	class Inactive;
	static bool default_dispose_query ( tag::Util::Core::DelegateDispose * );
	static bool dispose_query_cell_with_reverse ( tag::Util::Core::DelegateDispose * );

};


class tag::Util::Core::DelegateDispose : public tag::Util::Core

// some classes need specialized 'dispose_query' method, e.g. cells having reverse

{	public :

	bool (*dispose_query_p) ( tag::Util::Core::DelegateDispose * )
	{ & tag::Util::Core::default_dispose_query };  // this is not the body of a function
	// it is an initialization of the pointer-to-function

	inline DelegateDispose ( bool (*disp) ( tag::Util::Core::DelegateDispose * ),
	                         const tag::OneDummyWrapper &                        )
	// a new core is built through a wrapper; thus, we initialize with one instead of zero
	:	tag::Util::Core ( tag::one_dummy_wrapper ), dispose_query_p { disp } { }

	inline DelegateDispose ( bool (*disp) ( tag::Util::Core::DelegateDispose * ),
	                         const tag::ZeroWrappers &                           )
	// in rare cases, a core is built without a wrapper, like for negative cells
	:	tag::Util::Core ( tag::zero_wrappers ), dispose_query_p { disp } { }
	
	virtual ~DelegateDispose ( ) { }

	inline bool dispose_query ( )
	{	return dispose_query_p ( this );  }

};  // end of class tag::Util::Core


template < class core_type >	class tag::Util::Wrapper<core_type>::Inactive
// does nothing
{	public :
	core_type * core;
	inline Inactive ( const tag::Empty & ) : core { nullptr } { }
	inline Inactive ( core_type * c ) : core { c } { }
	inline ~Inactive ( ) { }
};

class tag::Util::Core::Inactive
// does nothing
{	public :
	inline Inactive ( const tag::OneDummyWrapper & ) { }
	inline Inactive ( const tag::ZeroWrappers & ) { }
	inline ~Inactive ( ) { }
};

//------------------------------------------------------------------------------------------------------//

class Cell;  class Mesh;
class Manifold;  class Function;

//------------------------------------------------------------------------------------------------------//


class tag::Util::CellIterator  // aka  class Mesh::Iterator

// a thin wrapper around a tag::Util::CellIterator::Core, aka Mesh::Iterator::Core,
// with most methods delegated to 'core'

// iterates over all cells of a given mesh (cells of given dimension)

// for maximum dimension (equal to the dimension of the mesh) returns oriented cells
// which may be positive or negative (tag::force_positive overrides this)
// for lower dimension, returns positive cells

// or, iterates over all cells around a given cell

// see paragraphs 9.5, 9.6 and 9.10 in the manual
	
{	public :

	class Core;
	
	std::unique_ptr < tag::Util::CellIterator::Core > core;
	// std::unique_ptr < Mesh::Iterator::Core > core;

	inline CellIterator ( const tag::Util::CellIterator & it );
	inline CellIterator ( tag::Util::CellIterator && it );

	inline CellIterator ( const tag::WhoseCoreIs &, tag::Util::CellIterator::Core * c )
	:	core { c }
	{	assert ( c );  }
	
	inline ~CellIterator ( ) { }
	
	inline tag::Util::CellIterator & operator= ( const tag::Util::CellIterator & it );
	inline tag::Util::CellIterator & operator= ( tag::Util::CellIterator && it );

	inline void reset ( );	
	inline void reset ( const tag::StartAt &, const Cell & );	
	inline Cell operator* ( );
	inline tag::Util::CellIterator & operator++ ( );
	inline tag::Util::CellIterator & operator++ ( int );
	inline tag::Util::CellIterator & advance ( );
	inline bool in_range ( );

	// unfortunately code below does not work because often 'deref' returns
	// a newly created Cell wrapper (usually with a previously existing core)
	// inline Cell & Mesh::Iterator::operator* ( )
	// inline Cell * Mesh::Iterator::operator-> ( )

	struct Over  {  class TwoVerticesOfSeg;  };
	struct OverCells
	{	class OfFuzzyMesh;  class OfConnectedOneDimMesh;  };
	struct OverVertices  {  class ConnectedToCenter;  class OfConnectedOneDimMesh;  };
	struct OverSegments
	{	class OfConnectedOneDimMesh;
		struct Around  {  struct OneVertex;  };
		class WhoseTipIsC;                       };
	struct Around  {  class OneCell;
	                  struct OneVertex  {  class ReturnSegments;  };  };
	class ReturnOneCell;  class AlwaysOutOfRange;
	struct OverCellsBehindFace  {  class InSTSIMesh;  };
	struct Adaptor  {  class ForcePositive;  };  // to eliminate

};  // end of class tag::Util::CellIterator


//------------------------------------------------------------------------------------------------------//


class tag::Util::MeshIterator  // aka  class Cell::Iterator

// a thin wrapper around a tag::Util::MeshIterator::Core, aka Cell::Iterator::Core,
// with most methods delegated to 'core'

// iterates over all meshes containing a given cell (meshes of given dimension)

// see paragraph 9.11 in the manual
	
{	public :

	class Core;
	
	std::unique_ptr < tag::Util::MeshIterator::Core > core;
	// std::unique_ptr < Cell::Iterator::Core > core;

	inline MeshIterator ( const tag::Util::MeshIterator & it );
	inline MeshIterator ( tag::Util::MeshIterator && it );

	inline MeshIterator ( const tag::WhoseCoreIs &, tag::Util::MeshIterator::Core * c )
	:	core { c }
	{	assert ( c );  }
	
	inline ~MeshIterator ( ) { }
	
	inline tag::Util::MeshIterator & operator= ( const tag::Util::MeshIterator & it );
	inline tag::Util::MeshIterator & operator= ( tag::Util::MeshIterator && it );

	inline void reset ( );	
	inline Mesh operator* ( );
	inline tag::Util::MeshIterator & operator++ ( );
	inline tag::Util::MeshIterator & operator++ ( int );
	inline tag::Util::MeshIterator & advance ( );
	inline bool in_range ( );

	// unfortunately code below does not work because often 'deref' returns
	// a newly created Mesh wrapper (usually with a previously existing core)
	// inline Mesh & Cell::Iterator::operator* ( )
	// inline Mesh * Cell::Iterator::operator-> ( )

	class OverMeshesOfSameDim;
	struct OverMeshesOfGivenDim
	{	struct OrientationIrrelevant  {  class ReturnPositiveMesh;  };  };
	
};  // end of class tag::Util::MeshIterator


//------------------------------------------------------------------------------------------------------//
//---------------------------  wrappers Cell and Mesh  -------------------------------------------------//
//------------------------------------------------------------------------------------------------------//


// a cell of dimension zero is a point, see classes Cell::Positive::Vertex and Negative
// a cell of dimension one is a segment, see classes Cell::Positive::Segment and Negative
// for cells of dimension two or more, see classes Cell::Positive::HighDim and Negative
// a cell of dimension two may be a triangle, a quadrangle or some other polygon
// a cell of dimension three may be a tetrahedron, a cube or some other polyhedron
// cells of dimension four or higher may be constructed

// cells may be positively or negatively oriented
// see class Cell::Positive and class Cell::Negative
// which is which depends only on the construction process
// the first one to be created is positive, the other one will be negative,
// created when we call the 'reverse' method

// a cell is mainly defined by its boundary (which is a mesh of lower dimension)
// the orientation of a cell is nothing more than an orientation of its boundary
// see the comments on orientation in class Mesh below

// see paragraphs 1.2, 9.1 and 12.4 in the manual

// I would very much prefer the name 'Cell::Core' instead of 'tag::Util::CellCore'
// but it was not possible ...

#ifdef MANIFEM_COLLECT_CM	

class Cell : public tag::Util::Wrapper < tag::Util::CellCore >

#else  // no MANIFEM_COLLECT_CM

class Cell : public tag::Util::Wrapper < tag::Util::CellCore > ::Inactive 

#endif  // MANIFEM_COLLECT_CM	

// a thin wrapper around a Cell::Core, with most methods delegated to 'core'

{	public :

	typedef tag::Util::CellCore Core;
	typedef tag::Util::MeshIterator Iterator;

	// static attributes at the end

	// Cell::Core * core  inherited from tag::Util::Wrapper < Cell::Core >

	inline Cell ( const tag::NonExistent & )
	#ifdef MANIFEM_COLLECT_CM	
	:	tag::Util::Wrapper < Cell::Core > ( tag::empty )
	#else  // no MANIFEM_COLLECT_CM	
	:	tag::Util::Wrapper < Cell::Core > ::Inactive ( tag::empty )
	#endif  // MANIFEM_COLLECT_CM	
	{ }

	inline Cell ( const tag::WhoseCoreIs &, Cell::Core * c, const tag::Move & )
	#ifdef MANIFEM_COLLECT_CM	
	:	tag::Util::Wrapper < Cell::Core > ( tag::whose_core_is, c, tag::move )
	#else  // no MANIFEM_COLLECT_CM	
	:	tag::Util::Wrapper < Cell::Core > ::Inactive ( c )
	#endif  // MANIFEM_COLLECT_CM	
	{ }

	inline Cell ( const tag::WhoseCoreIs &, Cell::Core * c,
	              const tag::PreviouslyExisting &, const tag::SurelyNotNull & )
	#ifdef MANIFEM_COLLECT_CM	
	:	tag::Util::Wrapper < Cell::Core > ( tag::whose_core_is, c,
		                                    tag::previously_existing, tag::surely_not_null )
	#else  // no MANIFEM_COLLECT_CM	
	:	tag::Util::Wrapper < Cell::Core > ::Inactive ( c )
	#endif  // MANIFEM_COLLECT_CM	
	{	}

	inline Cell ( const tag::WhoseCoreIs &, Cell::Core * c,
	              const tag::PreviouslyExisting &, const tag::MayBeNull & )
	#ifdef MANIFEM_COLLECT_CM	
	:	tag::Util::Wrapper < Cell::Core > ( tag::whose_core_is, c,
		                                    tag::previously_existing, tag::may_be_null )
	#else  // no MANIFEM_COLLECT_CM	
	:	tag::Util::Wrapper < Cell::Core > ::Inactive ( c )
	#endif  // MANIFEM_COLLECT_CM	
	{	}

	inline Cell ( )
	: Cell ( tag::non_existent )
	{	std::cout << __FILE__ << ":" << __LINE__ << ": "
		          << __extension__ __PRETTY_FUNCTION__ << ": ";
		std::cout << "Cell constructor without arguments not allowed" << std::endl;
		exit (1);                                                                    }
	
	inline Cell ( const Cell & c )
	: Cell ( tag::whose_core_is, c .core, tag::previously_existing, tag::may_be_null )
	{	}

	inline Cell ( Cell && c )
	: Cell ( tag::whose_core_is, c .core, tag::move )
	{	c .core = nullptr;  }

	inline Cell ( const tag::WhoseBoundaryIs &, Mesh & );

	inline Cell ( const tag::Vertex &, const tag::IsPositive & ispos = tag::is_positive );

	// the two constructors below are defined in  manifold.h
	// because we need to set coordinates and to project
	inline Cell ( const tag::Vertex &, const tag::OfCoordinates &, const std::vector < double > &,
	              const tag::IsPositive & ispos = tag::is_positive                                );
	inline Cell ( const tag::Vertex &, const tag::OfCoordinates &, const std::vector < double > &,
	              const tag::Project &, const tag::IsPositive & ispos = tag::is_positive          );

	inline Cell ( const tag::Segment &, const Cell & A, const Cell & B );

	inline Cell ( const tag::Triangle &, const Cell & AB, const Cell & BC, const Cell & CA );

	inline Cell ( const tag::Quadrangle &, const Cell & AB, const Cell & BC,
                                         const Cell & CD, const Cell & DA );

	inline Cell ( const tag::Parallelogram &, const Cell & AB, const Cell & BC,
                                            const Cell & CD, const Cell & DA )
	:	Cell ( tag::quadrangle, AB, BC, CD, DA )
	{	}
	
	inline Cell ( const tag::Rectangle &, const Cell & AB, const Cell & BC,
                                        const Cell & CD, const Cell & DA )
	:	Cell ( tag::quadrangle, AB, BC, CD, DA )
	{	}

	inline Cell ( const tag::Square &, const Cell & AB, const Cell & BC,
                                     const Cell & CD, const Cell & DA )
	:	Cell ( tag::quadrangle, AB, BC, CD, DA )
	{	}

	inline Cell ( const tag::Pentagon &, const Cell & AB, const Cell & BC,
                                       const Cell & CD, const Cell & DE, const Cell & EA );

	inline Cell ( const tag::Hexagon &, const Cell & AB, const Cell & BC,
                       const Cell & CD, const Cell & DE, const Cell & EF, const Cell & FA );

	inline Cell ( const tag::Tetrahedron &, const Cell & face_1, const Cell & face_2,
                                          const Cell & face_3, const Cell & face_4 );

	inline Cell ( const tag::Hexahedron &, const Cell & face_1, const Cell & face_2,
                                         const Cell & face_3, const Cell & face_4,
                                         const Cell & face_5, const Cell & face_6 );

	inline ~Cell ( )  { }  // destructor of tag::Util::Wrapper is invoked
	
	inline Cell & operator= ( const Cell & c );
	inline Cell & operator= ( Cell && c );
	
	// we are still in class Cell
	
	// methods delegated to 'core'

	inline Cell reverse ( const tag::BuildIfNotExists & build = tag::build_if_not_exists ) const;
	inline Cell reverse ( const tag::SurelyExists & ) const;
	inline Cell reverse ( const tag::DoesNotExist &, const tag::BuildNow & ) const;
	inline Cell reverse ( const tag::MayNotExist & ) const;
	inline Mesh boundary () const;
	inline bool exists () const  { return this->core != nullptr;  }
	inline bool is_positive () const;
	inline Cell get_positive () const;
	inline size_t dim () const;
	inline bool has_reverse () const;
	inline Cell tip () const;
	inline Cell base () const;

	inline bool belongs_to
	( const Mesh & msh, const tag::SameDim &,
	  const tag::OrientCompWithMesh & o = tag::orientation_compatible_with_mesh ) const;
	inline bool belongs_to
	( const Mesh & msh, const tag::SameDim &, const tag::NotOriented & ) const;
	inline bool belongs_to
	( const Mesh & msh, const tag::SameDim &, const tag::OrientationIrrelevant & ) const
	{	return  belongs_to ( msh, tag::same_dim, tag::not_oriented );  }
	inline bool belongs_to
	( const Mesh & msh, const tag::CellHasLowDim &,
	  const tag::NotOriented & no = tag::not_oriented ) const;
	inline bool belongs_to ( const Mesh & msh, const tag::OrientCompWithMesh & ) const;
	inline bool belongs_to ( const Mesh & msh, const tag::NotOriented & ) const;
	inline bool belongs_to ( const Mesh & msh ) const;
	inline bool belongs_to ( const Mesh & msh, const tag::OnBoundary & ) const;
	
	bool is_inner_to ( const Mesh & msh ) const;  // defined in iterator.cpp

	// method 'glue_on_bdry_of' is intensively used when building a mesh
	// it glues 'this' cell to the boundary of 'cll'
	inline void glue_on_bdry_of ( Cell & cll );
	inline void glue_on_bdry_of ( Cell & cll, const tag::DoNotBother & );
	
	// method 'cut_from_bdry_of' does the reverse : cuts 'this' cell from
	// the boundary of 'cll' - used mainly in remeshing
	inline void cut_from_bdry_of ( Cell & cll );
	inline void cut_from_bdry_of ( Cell & cll, const tag::DoNotBother & );
	
	// methods 'add_to' and 'remove_from' add/remove 'this' cell to/from the mesh 'msh'
	// if 'msh' is the boundary of some cell, methods 'glue_on_bdry_of'
	//   and 'cut_from_bdry_of' (see above) should be used instead
	inline void add_to ( Mesh & msh );
	inline void add_to ( Mesh & msh, const tag::DoNotBother & );
	// method below keeps 'this' segment as 'first_ver' although this is of course not true
	inline void add_to ( Mesh & msh, const tag::DoNotBother &, const tag::KeepAsFirstVer & );
	inline void add_to ( Mesh & msh, const tag::Force & );  // for STSI meshes
	inline void add_to ( Mesh & msh,  // for STSI meshes
	                     const tag::NeighbRels &, const std::map < Cell, Cell > & rels,
	                     bool force_other_faces                                        );
	inline void remove_from ( Mesh & msh );
	inline void remove_from ( Mesh & msh, const tag::DoNotBother & );

	// we are still in class Cell
	inline void project ( ) const;  // both defined in manifold.h
	inline void project ( const tag::Onto &, const Manifold m ) const;

	inline bool is_regular ( const tag::WithinSTSIMesh &, const Mesh & msh ) const;
	// face is not singular

	inline Cell::Iterator iterator
	( const tag::OverMeshesAbove &, const tag::SameDim &,
	  const tag::OrientCompWithCell &, const tag::DoNotBuildCells & ) const;	
	// beware: may return negative meshes, does not build reverse cells
	inline Cell::Iterator iterator
	( const tag::OverMeshesAbove &, const tag::SameDim &,
	  const tag::OrientOpposToCell &, const tag::DoNotBuildCells & ) const;	
	// beware: may return negative meshes, does not build reverse cells
	inline Cell::Iterator iterator
	( const tag::OverMeshesAbove &, const tag::OfDimension &, const size_t d,
	  const tag::OrientationIrrelevant &                                     ) const;
	// returns positive meshes
  
	class Winding;  // defined in function.h
	Winding winding ( ) const;  // defined in manifold.h
		
	#ifndef NDEBUG  // DEBUG mode
	inline std::string name ( );
	inline void print_everything ( );
	#endif   // DEBUG mode

	// static int counter;

	// a list of functions to be called each time a new cell is created
	// two lists, in fact, one for positive cells the other for negative cells
	static std::vector < std::vector < void(*)(Cell::Core*,void*) > > init_pos_cell, init_neg_cell;
	// more data can be passed to the above functions by using
	static std::vector < std::vector < void* > > data_for_init_pos, data_for_init_neg;
	// perhaps through a 'this' pointer
	
	struct field_to_meshes
	{	short int counter_pos;
		short int counter_neg;
		std::list < Cell > ::iterator where;
		inline field_to_meshes ( short int i, short int j )
		:	counter_pos {i}, counter_neg {j} { }
		inline field_to_meshes ( short int i, short int j,
		                         std::list < Cell > ::iterator w )
		:	counter_pos {i}, counter_neg {j}, where {w} { }              };

	struct field_to_meshes_same_dim
	{	short int sign;
		std::list < Cell > ::iterator where;
		inline field_to_meshes_same_dim ( short int i ) : sign {i} { }
		inline field_to_meshes_same_dim
		( short int i, std::list < Cell > ::iterator w )
		:	sign {i}, where {w} { }                                       };

	class Positive;  class Negative;
	
	// any way to rewrite the names below as Positive::Vertex and such ?
	// I get an error because Cell::Core::add_to_seg expects a PositiveSegment ...
	class PositiveVertex;  class PositiveNotVertex;
	class PositiveSegment;  class PositiveHighDim;
	class NegativeVertex;  class NegativeNotVertex;
	class NegativeSegment;  class NegativeHighDim;
  
	class Numbering;  // a callable object with a Cell as argument, returning a size_t
	
}; // end of  class Cell


inline bool operator== ( const Cell & c1, const Cell & c2 )
{	return c1.core == c2.core;  }

inline bool operator!= ( const Cell & c1, const Cell & c2 )
{	return c1.core != c2.core;  }

inline bool operator< ( const Cell & c1, const Cell & c2 )
{	return c1.core < c2.core;  }

//------------------------------------------------------------------------------------------------------//
//------------------------------------------------------------------------------------------------------//


// roughly speaking, a mesh is a collection of cells of the same dimension

// an orientation of the mesh
// is nothing more than an orientation of each of its cells (of maximum dimension)
// but these orientations cannot be arbitrary, they must be compatible
// in the sense that a face common to two cells must be seen as positive
// from one of the cells and as negative from the other cell

// as a consequence, for connected meshes there are only two possible orientations
// although nothing prevents a mesh to be disconnected, we only allow for two orientations
// which is which depends only on the construction process
// the first one to be created is positive, the other one will be negative,
// created when we call the 'reverse' method

// negative meshes will appear mostly as boundaries of negative cells
// that's why we do not store their core in the computer's memory
// wrappers for negative meshes are built on-the-fly, e.g. in method Cell::boundary
// their core points to the (positive) reverse, the only difference is in 'is_pos'

// see paragraphs 1.2, 9.1 and 12.4 in the manual

// I would very much prefer the name 'Mesh::Core' instead of 'tag::Util::MeshCore'
// but it was not possible ...

#ifdef MANIFEM_COLLECT_CM	

class Mesh : public tag::Util::Wrapper < tag::Util::MeshCore >

#else  // no MANIFEM_COLLECT_CM	

class Mesh : public tag::Util::Wrapper < tag::Util::MeshCore > ::Inactive

#endif  // MANIFEM_COLLECT_CM	

// a thin wrapper around a Mesh::Core, with most methods delegated to 'core'

{	public :
	
	typedef tag::Util::MeshCore Core;
	typedef tag::Util::CellIterator Iterator;

	// Mesh::Core * core  inherited from tag::Util::Wrapper < Mesh::Core >

	// there are no cores for negative meshes
	// instead, we keep here a pointer to the direct (positive) core
	// thus, for meshes, 'core' points always to a positive Mesh::Core

	// this is how we distinguish between a positive mesh and a negative one
	bool (*is_pos) ();

	// low-level constructors :

	inline Mesh ( const tag::NonExistent & );

	inline Mesh ( const tag::WhoseCoreIs &, Mesh::Core * msh, const tag::Move &,
	              const tag::IsPositive & ispos = tag::is_positive              );
	
	inline Mesh ( const tag::WhoseCoreIs &, Mesh::Core * msh, const tag::Move &,
	              const tag::BuildNegative &, const tag::ReverseCellsSurelyExist &     );
	
	inline Mesh ( const tag::WhoseCoreIs &, Mesh::Core * msh, const tag::PreviouslyExisting &,
	              const tag::IsPositive & ispos = tag::is_positive                            );
	// 'msh' is either pre-existent or is freshly created with tag::move
	
	inline Mesh ( const tag::WhoseCoreIs &, Mesh::Core * msh,
	              const tag::PreviouslyExisting &, const tag::MayBeNull & );
	// 'msh' is either pre-existent or is freshly created with tag::move or is null
	// the sign of 'this' mesh ('is_pos') is not defined here,
	// should be defined by some other constructor calling this one
	
	inline Mesh ( )
	: Mesh ( tag::non_existent )
	{	std::cout << __FILE__ << ":" << __LINE__ << ": "
		          << __extension__ __PRETTY_FUNCTION__ << ": ";
		std::cout << "Mesh constructor without arguments not allowed" << std::endl;
		exit (1);                                                                    }
	
	inline Mesh ( const Mesh & msh )
	: Mesh ( tag::whose_core_is, msh .core, tag::previously_existing, tag::may_be_null )
	{	this->is_pos = msh .is_pos;  }

	inline Mesh ( Mesh && msh )
	: Mesh ( tag::whose_core_is, msh .core, tag::move )
	{	this->is_pos = msh .is_pos;
		msh .core = nullptr;         }
		
	// more elaborate (high-level) constructors :
	// they call one of the above, then manipulate the mesh
	
	// build a negative mesh from a positive one
	// without worrying whether reverse cells exist or not
	inline Mesh ( const tag::FromPositiveMesh &, const tag::WhoseCoreIs &, Mesh::Core * msh,
	              const tag::BuildNegative &, const tag::DoNotBuildCells &                  );

	// build a negative mesh from a positive one, assuming all cells have reverse :
	inline Mesh ( const tag::FromPositiveMesh &, const tag::WhoseCoreIs &, Mesh::Core * msh,
	              const tag::BuildNegative &, const tag::ReverseCellsSurelyExist &          );
	
	// build a negative mesh from a positive one, creating reverse cells if necessary :
	inline Mesh ( const tag::FromPositiveMesh &, const tag::WhoseCoreIs &, Mesh::Core * msh,
	              const tag::BuildNegative &, const tag::BuildCellsIfNec &                  );

	inline Mesh ( const tag::Fuzzy &, const tag::OfDimension &, const size_t dim,
	              const tag::IsPositive & ispos = tag::is_positive               );

	inline Mesh ( const tag::ConnectedOneDim &, const tag::IsPositive & ispos = tag::is_positive );

	inline Mesh ( const tag::Fuzzy &, const tag::OfDimension &, const size_t dim,
	              const tag::MinusOne &, const tag::IsPositive & ispos = tag::is_positive );

	inline Mesh ( const tag::stsi &, const tag::OfDimension &, const size_t dim,
	              const tag::IsPositive & ispos = tag::is_positive              );

	// we are still in class Mesh
	
	template < typename container >
	static inline size_t join ( Mesh & result, const container & l );

	template < typename container >
	static inline void join_meshes ( Mesh & result, const container & l );
	
	inline Mesh & operator= ( const Mesh & c );
	inline Mesh & operator= ( Mesh && c );

	// we are still in class Mesh

	bool has_empty_boundary ( ) const;
  
	Mesh boundary ( ) const;

	inline Mesh clone ( const tag::Depth &, size_t dep );
	inline Mesh clone ( const tag::Depth &, const tag::Infinity & );
	inline Mesh clone ( const tag::MakeFuzzy &, const tag::Depth &, size_t dep );
	inline Mesh clone ( const tag::MakeFuzzy &, const tag::Depth &, const tag::Infinity & );
	inline Mesh clone ( const tag::MakeConnectedOneDim &, const tag::SurelyExists &,
	                    const tag::Depth &, size_t dep                              );
	inline Mesh clone ( const tag::MakeConnectedOneDim &, const tag::SurelyExists &,
	                    const tag::Depth &, const tag::Infinity &                   );
	inline Mesh clone ( const tag::ReverseEachCell &, const tag::Depth &, size_t dep );
	// builds non-existing cells

	#ifndef MANIFEM_NO_QUOTIENT
	
	// methods 'fold' defined in global.cpp -- 'wrap' is a synonym
	// see paragraphs 7.19 - 7.22 in the manual
	
	Mesh fold ( const tag::BuildNewVertices & );

	Mesh fold ( const tag::BuildNewVertices &,
	            const tag::ReturnMapBetween &, const tag::CellsOfDim &,
	            size_t dim, std::map < Cell, Cell > & m                 );

	inline Mesh wrap ( const tag::BuildNewVertices & )
	{	return this->fold ( tag::build_new_vertices );  }

	inline Mesh wrap ( const tag::BuildNewVertices &,
	                   const tag::ReturnMapBetween &, const tag::CellsOfDim &,
	                   size_t dim, std::map < Cell, Cell > & m                 )
	{	return this->fold ( tag::build_new_vertices, tag::return_map_between,
												tag::cells_of_dim, dim, m                         );  }

	Mesh fold ( const tag::UseExistingVertices & );

	Mesh fold ( const tag::UseExistingVertices &,
	            const tag::ReturnMapBetween &, const tag::CellsOfDim &,
	            size_t dim, std::map < Cell, Cell > & m                 );
	
	inline Mesh wrap ( const tag::UseExistingVertices & )
	{	return this->fold ( tag::use_existing_vertices );  }

	inline Mesh wrap ( const tag::UseExistingVertices &,
	                   const tag::ReturnMapBetween &, const tag::CellsOfDim &,
	                   size_t dim, std::map < Cell, Cell > & m                 )
	{	return this->fold ( tag::use_existing_vertices, tag::return_map_between,
												tag::cells_of_dim, dim, m                         );  }

	Mesh fold ( const tag::Identify &, const Mesh & msh1, const tag::With &, const Mesh & msh2,
	            const tag::BuildNewVertices &                                                  );

	Mesh fold ( const tag::Identify &, const Mesh & msh1, const tag::With &, const Mesh & msh2,
	            const tag::BuildNewVertices &,
									const tag::ReturnMapBetween &, const tag::CellsOfDim &,
										size_t dim, std::map < Cell, Cell > & m                 );

	inline Mesh wrap
	( const tag::Identify &, const Mesh & msh1, const tag::With &, const Mesh & msh2,
	  const tag::BuildNewVertices &                                                   )
	{	return this->fold ( tag::identify, msh1, tag::with, msh2, tag::build_new_vertices );  }

	inline Mesh wrap
	( const tag::Identify &, const Mesh & msh1, const tag::With &, const Mesh & msh2,
	  const tag::BuildNewVertices &,
	  const tag::ReturnMapBetween &, const tag::CellsOfDim &,
	  size_t dim, std::map < Cell, Cell > & m                                        )
	{	return this->fold ( tag::identify, msh1, tag::with, msh2,
		                    tag::build_new_vertices, tag::return_map_between,
		                    tag::cells_of_dim, dim, m  );  }

	Mesh fold ( const tag::Identify &, const Mesh & msh1, const tag::With &, const Mesh & msh2,
	            const tag::UseExistingVertices &                                               );

	Mesh fold ( const tag::Identify &, const Mesh & msh1, const tag::With &, const Mesh & msh2,
	            const tag::UseExistingVertices &,
	            const tag::ReturnMapBetween &, const tag::CellsOfDim &,
	            size_t dim, std::map < Cell, Cell > & m                                        );

	inline Mesh wrap
	( const tag::Identify &, const Mesh & msh1, const tag::With &, const Mesh & msh2,
	  const tag::UseExistingVertices &                                                )
	{	return this->fold ( tag::identify, msh1, tag::with, msh2, tag::use_existing_vertices );  }

	inline Mesh wrap ( const tag::Identify &, const Mesh & msh1,
	                   const tag::With &, const Mesh & msh2,
	                   const tag::UseExistingVertices &,
	                   const tag::ReturnMapBetween &, const tag::CellsOfDim &,
	                   size_t dim, std::map < Cell, Cell > & m                 )
	{	return this->fold ( tag::identify, msh1, tag::with, msh2,
		                    tag::use_existing_vertices, tag::return_map_between,
	                      tag::cells_of_dim, dim, m                           );  }

	// we are still in class Mesh

	Mesh fold ( const tag::Identify &, const Mesh & msh1, const tag::With &, const Mesh & msh2,
	            const tag::Identify &, const Mesh & msh3, const tag::With &, const Mesh & msh4,
	            const tag::BuildNewVertices &                                                  );

	Mesh fold ( const tag::Identify &, const Mesh & msh1, const tag::With &, const Mesh & msh2,
	            const tag::Identify &, const Mesh & msh3, const tag::With &, const Mesh & msh4,
	            const tag::BuildNewVertices &,
									const tag::ReturnMapBetween &, const tag::CellsOfDim &,
										size_t dim, std::map < Cell, Cell > & m                 );

	inline Mesh wrap
	( const tag::Identify &, const Mesh & msh1, const tag::With &, const Mesh & msh2,
	  const tag::Identify &, const Mesh & msh3, const tag::With &, const Mesh & msh4,
	  const tag::BuildNewVertices &                                                  )
	{	return this->fold ( tag::identify, msh1, tag::with, msh2,
		                    tag::identify, msh3, tag::with, msh4, tag::build_new_vertices );  }

	inline Mesh wrap
	( const tag::Identify &, const Mesh & msh1, const tag::With &, const Mesh & msh2,
	  const tag::Identify &, const Mesh & msh3, const tag::With &, const Mesh & msh4,
	  const tag::BuildNewVertices &, const tag::ReturnMapBetween &,
	  const tag::CellsOfDim &, size_t dim, std::map < Cell, Cell > & m                 )
	{	return this->fold ( tag::identify, msh1, tag::with, msh2,
		                    tag::identify, msh3, tag::with, msh4, tag::build_new_vertices,
		                    tag::return_map_between, tag::cells_of_dim, dim, m             );  }
	
	Mesh fold ( const tag::Identify &, const Mesh & msh1, const tag::With &, const Mesh & msh2,
	            const tag::Identify &, const Mesh & msh3, const tag::With &, const Mesh & msh4,
	            const tag::UseExistingVertices &                                               );
	
	Mesh fold ( const tag::Identify &, const Mesh & msh1, const tag::With &, const Mesh & msh2,
	            const tag::Identify &, const Mesh & msh3, const tag::With &, const Mesh & msh4,
	            const tag::UseExistingVertices &,
									const tag::ReturnMapBetween &, const tag::CellsOfDim &,
										size_t dim, std::map < Cell, Cell > & m                 );
	
	inline Mesh wrap
	( const tag::Identify &, const Mesh & msh1, const tag::With &, const Mesh & msh2,
	  const tag::Identify &, const Mesh & msh3, const tag::With &, const Mesh & msh4,
	  const tag::UseExistingVertices &                                               )
	{	return this->fold ( tag::identify, msh1, tag::with, msh2,
		                    tag::identify, msh3, tag::with, msh4, tag::use_existing_vertices );  }

	Mesh fold ( const tag::Identify &, const Mesh & msh1, const tag::With &, const Mesh & msh2,
	            const tag::Identify &, const Mesh & msh3, const tag::With &, const Mesh & msh4,
	            const tag::Identify &, const Mesh & msh5, const tag::With &, const Mesh & msh6,
	            const tag::BuildNewVertices &                                                  );

	Mesh fold ( const tag::Identify &, const Mesh & msh1, const tag::With &, const Mesh & msh2,
	            const tag::Identify &, const Mesh & msh3, const tag::With &, const Mesh & msh4,
	            const tag::Identify &, const Mesh & msh5, const tag::With &, const Mesh & msh6,
	            const tag::BuildNewVertices &, const tag::ReturnMapBetween &,
	            const tag::CellsOfDim &, size_t dim, std::map < Cell, Cell > & m                 );

	inline Mesh wrap
	( const tag::Identify &, const Mesh & msh1, const tag::With &, const Mesh & msh2,
	  const tag::Identify &, const Mesh & msh3, const tag::With &, const Mesh & msh4,
	  const tag::Identify &, const Mesh & msh5, const tag::With &, const Mesh & msh6,
	  const tag::BuildNewVertices &                                                  )
	{	return this->fold ( tag::identify, msh1, tag::with, msh2,
		                    tag::identify, msh3, tag::with, msh4,
		                    tag::identify, msh5, tag::with, msh6, tag::build_new_vertices );  }

	Mesh fold ( const tag::Identify &, const Mesh & msh1, const tag::With &, const Mesh & msh2,
	            const tag::Identify &, const Mesh & msh3, const tag::With &, const Mesh & msh4,
	            const tag::Identify &, const Mesh & msh5, const tag::With &, const Mesh & msh6,
	            const tag::UseExistingVertices &                                               );
	
	Mesh fold ( const tag::Identify &, const Mesh & msh1, const tag::With &, const Mesh & msh2,
	            const tag::Identify &, const Mesh & msh3, const tag::With &, const Mesh & msh4,
	            const tag::Identify &, const Mesh & msh5, const tag::With &, const Mesh & msh6,
	            const tag::UseExistingVertices &,
									const tag::ReturnMapBetween &, const tag::CellsOfDim &,
										size_t dim, std::map < Cell, Cell > & m                 );
	
	inline Mesh wrap
	( const tag::Identify &, const Mesh & msh1, const tag::With &, const Mesh & msh2,
	  const tag::Identify &, const Mesh & msh3, const tag::With &, const Mesh & msh4,
	  const tag::Identify &, const Mesh & msh5, const tag::With &, const Mesh & msh6,
	  const tag::UseExistingVertices &                                               )
	{	return this->fold ( tag::identify, msh1, tag::with, msh2,
		                    tag::identify, msh3, tag::with, msh4,
		                    tag::identify, msh5, tag::with, msh6, tag::use_existing_vertices );  }

	// we are still in class Mesh

	// 'unfold' does the opposite : takes a mesh and unfolds it
	// defined in global.cpp
	// over a given region of the plane so we can export the resulting mesh in msh format
	
	Mesh unfold ( const tag::OverRegion &, const tag::Util::InequalitySet & constraints,
	              const tag::ReturnMapBetween &, const tag::CellsOfDim &,
	              size_t dim, std::map < Cell, std::pair < Cell, tag::Util::Action > > & mapping )
	const;

	Mesh unfold ( const std::vector < tag::Util::Action > &,
	              const tag::OverRegion &, const tag::Util::InequalitySet & constraints,
	              const tag::ReturnMapBetween &, const tag::CellsOfDim &,
	              size_t dim, std::map < Cell, std::pair < Cell, tag::Util::Action > > & mapping )
	const;
	
	Mesh unfold ( const tag::OverRegion &, const tag::Util::InequalitySet & constraints ) const;

	Mesh unfold ( const std::vector < tag::Util::Action > & ) const;

	Mesh unfold ( const std::vector < tag::Util::Action > &,
	              const tag::OverRegion &, const tag::Util::InequalitySet & constraints ) const;

	// inline versions of unfold defined in function.h
	
	inline Mesh unfold ( const tag::OverRegion &,
	                     const tag::Util::InequalitySet & c1,
	                     const tag::Util::InequalitySet & c2 ) const;
	inline Mesh unfold ( const tag::OverRegion &,
	                     const tag::Util::InequalitySet & c1,
	                     const tag::Util::InequalitySet & c2,
	                     const tag::Util::InequalitySet & c3 ) const;
	inline Mesh unfold ( const tag::OverRegion &,
	                     const tag::Util::InequalitySet & c1,
	                     const tag::Util::InequalitySet & c2,
	                     const tag::Util::InequalitySet & c3,
	                     const tag::Util::InequalitySet & c4 ) const;
	inline Mesh unfold ( const tag::OverRegion &,
	                     const tag::Util::InequalitySet & c1,
	                     const tag::Util::InequalitySet & c2,
	              const tag::ReturnMapBetween &, const tag::CellsOfDim &,
	              size_t dim, std::map < Cell, std::pair < Cell, tag::Util::Action > > & mapping )
	const;
	inline Mesh unfold ( const tag::OverRegion &,
	                     const tag::Util::InequalitySet & c1,
	                     const tag::Util::InequalitySet & c2,
	                     const tag::Util::InequalitySet & c3,
	              const tag::ReturnMapBetween &, const tag::CellsOfDim &,
	              size_t dim, std::map < Cell, std::pair < Cell, tag::Util::Action > > & mapping )
	const;
	inline Mesh unfold ( const tag::OverRegion &,
	                     const tag::Util::InequalitySet & c1,
	                     const tag::Util::InequalitySet & c2,
	                     const tag::Util::InequalitySet & c3,
	                     const tag::Util::InequalitySet & c4,
	              const tag::ReturnMapBetween &, const tag::CellsOfDim &,
	              size_t dim, std::map < Cell, std::pair < Cell, tag::Util::Action > > & mapping )
	const;

	inline Mesh unfold ( const std::vector < tag::Util::Action > &, const tag::OverRegion &,
	                     const tag::Util::InequalitySet & c1,
	                     const tag::Util::InequalitySet & c2           ) const;
	inline Mesh unfold ( const std::vector < tag::Util::Action > &, const tag::OverRegion &,
	                     const tag::Util::InequalitySet & c1,
	                     const tag::Util::InequalitySet & c2,
	                     const tag::Util::InequalitySet & c3           ) const;
	inline Mesh unfold ( const std::vector < tag::Util::Action > &, const tag::OverRegion &,
	                     const tag::Util::InequalitySet & c1,
	                     const tag::Util::InequalitySet & c2,
	                     const tag::Util::InequalitySet & c3,
	                     const tag::Util::InequalitySet & c4           ) const;
	inline Mesh unfold ( const std::vector < tag::Util::Action > &, const tag::OverRegion &,
	                     const tag::Util::InequalitySet & c1,
	                     const tag::Util::InequalitySet & c2,
	              const tag::ReturnMapBetween &, const tag::CellsOfDim &,
	              size_t dim, std::map < Cell, std::pair < Cell, tag::Util::Action > > & mapping )
	const;
	inline Mesh unfold ( const std::vector < tag::Util::Action > &, const tag::OverRegion &,
	                     const tag::Util::InequalitySet & c1,
	                     const tag::Util::InequalitySet & c2,
	                     const tag::Util::InequalitySet & c3,
	              const tag::ReturnMapBetween &, const tag::CellsOfDim &,
	              size_t dim, std::map < Cell, std::pair < Cell, tag::Util::Action > > & mapping )
	const;
	inline Mesh unfold ( const std::vector < tag::Util::Action > &, const tag::OverRegion &,
	                     const tag::Util::InequalitySet & c1,
	                     const tag::Util::InequalitySet & c2,
	                     const tag::Util::InequalitySet & c3,
	                     const tag::Util::InequalitySet & c4,
	              const tag::ReturnMapBetween &, const tag::CellsOfDim &,
	              size_t dim, std::map < Cell, std::pair < Cell, tag::Util::Action > > & mapping )
	const;

	#endif  // ifndef MANIFEM_NO_QUOTIENT
	
	// we are still in class Mesh

	inline void copy_all_cells_to ( Mesh & msh ) const;

	inline bool exists ( ) const  { return this->core != nullptr;  }
	inline bool is_positive ( ) const  {  assert ( this->exists() );  return this->is_pos();  }
	inline size_t dim ( ) const;
	inline Mesh reverse ( const tag::BuildCellsIfNec & bcin = tag::build_cells_if_necessary ) const;
	inline Mesh reverse ( const tag::ReverseCellsSurelyExist & ) const;

	inline size_t number_of ( const tag::Vertices & ) const;
	inline size_t number_of ( const tag::Segments & ) const;
	inline size_t number_of ( const tag::CellsOfDim &, const size_t d ) const;
	inline size_t number_of ( const tag::CellsOfMaxDim & ) const;

	inline Cell first_vertex ( ) const;
	inline Cell last_vertex ( ) const;
	inline Cell first_segment ( ) const;
	inline Cell last_segment ( ) const;

	// three methods below defined in  global.cpp
	Cell common_vertex ( const tag::With &, const Mesh & m2 ) const;
	Cell common_vertex ( const tag::With &, const Mesh & m2, const tag::ExactlyOne & ) const;
	Mesh common_edge ( const tag::With &, const Mesh & m2 ) const;
	
	// we are still in class Mesh
	
	inline Cell cell_in_front_of
	( const Cell & face, const tag::SurelyExists & se = tag::surely_exists ) const;
	inline Cell cell_behind
	( const Cell & face, const tag::SurelyExists & se = tag::surely_exists ) const;
	inline Cell cell_in_front_of ( const Cell & face, const tag::MayNotExist & ) const;
	inline Cell cell_behind ( const Cell & face, const tag::MayNotExist & ) const;

	// the four methods below are only relevant for STSI meshes

	inline Cell cell_in_front_of
	( const Cell & face, const tag::SeenFrom &, const Cell & neighbour,
	  const tag::SurelyExists & se = tag::surely_exists                ) const;
	inline Cell cell_in_front_of
	( const Cell & face, const tag::SeenFrom &, const Cell & neighbour,
	  const tag::MayNotExist &                                         ) const;
	inline Cell cell_behind
	( const Cell & face, const tag::SeenFrom &, const Cell & neighbour,
	  const tag::SurelyExists & se = tag::surely_exists                ) const;
	inline Cell cell_behind
	( const Cell & face, const tag::SeenFrom &, const Cell & neighbour,
	  const tag::MayNotExist &                                         ) const;

	inline void closed_loop ( const Cell & ver );
	// for connected one-dim meshes, set both first_ver and last_ver to 'ver'

	inline void closed_loop ( const Cell & ver, size_t );
	// for connected one-dim meshes, set both first_ver and last_ver to 'ver'
	// and number of segments

	Mesh convert_to
	( const tag::Connected &, const tag::OneDim &, const tag::SurelyExists & ) const;
	Mesh convert_to
	( const tag::Connected &, const tag::OneDim &, const tag::MayNotExist & ) const;

	void barycenter ( const Cell & ver );
	bool barycenter ( const Cell & ver, const tag::OnlyIfVertexIsInner &, const tag::MeshHasDimOne & );
	bool barycenter ( const Cell & ver, const tag::OnlyIfVertexIsInner &, const tag::MeshHasDimTwo & );
	void barycenter ( const Cell & ver, const tag::InnerVertex &, const tag::MeshHasDimOne & );
	void barycenter ( const Cell & ver, const tag::InnerVertex &, const tag::MeshHasHighDim & );
	#ifndef MANIFEM_NO_QUOTIENT
	void barycenter ( const Cell & ver, const tag::Winding & );
	void barycenter ( const Cell & ver, const tag::Winding &,
	                  const tag::ShadowVertices &, const std::vector < Cell > & vec_cll );
	#endif  // ifndef MANIFEM_NO_QUOTIENT

	// hundreds of iterators to choose from :
	#include "mesh-iter.h"

	// we are still in class Mesh
	// methods  export_to_file  defined in global.cpp

	void export_to_file ( const tag::Gmsh &, std::string f, Cell::Numbering & ) const;
	void export_to_file ( const tag::Gmsh &, std::string f, std::map < Cell, size_t > & ) const;
	void export_to_file ( const tag::Gmsh &, std::string f ) const;
	void export_to_file ( const tag::postscript &, std::string file_name ) const;
	void export_to_file ( const tag::postscript3d &, std::string file_name ) const;
	#ifndef MANIFEM_NO_QUOTIENT
	void export_to_file ( const tag::postscript &, std::string file_name,
	               const tag::Unfold &, const std::vector < tag::Util::Action > &,
	               const tag::OverRegion &, const tag::Util::InequalitySet & constraints ) const;
	void export_to_file ( const tag::postscript &, std::string file_name,
	               const tag::Unfold &, const tag::OverRegion &,
	               const tag::Util::InequalitySet & constraints ) const;
	void export_to_file ( const tag::postscript &, std::string file_name,
	               const tag::Unfold &, const tag::OneGenerator &, const tag::OverRegion &,
	               const tag::Util::InequalitySet & constraints                            ) const;
	void export_to_file ( const tag::postscript &, std::string file_name,
	               const tag::Unfold &, const tag::TwoGenerators &, const tag::OverRegion &,
	               const tag::Util::InequalitySet & constraints                             ) const;

	// inline versions of export_to_file with tag::eps defined in function.h

	inline void export_to_file ( const tag::postscript &, std::string file_name,
	           const tag::Unfold &, const std::vector < tag::Util::Action > &,
	           const tag::OverRegion &, const tag::Util::InequalitySet & c1,
	                                    const tag::Util::InequalitySet & c2   ) const;
	inline void export_to_file ( const tag::postscript &, std::string file_name,
	           const tag::Unfold &, const std::vector < tag::Util::Action > &,
	           const tag::OverRegion &, const tag::Util::InequalitySet & c1,
	                                    const tag::Util::InequalitySet & c2,
	                                    const tag::Util::InequalitySet & c3    ) const;
	inline void export_to_file ( const tag::postscript &, std::string file_name,
	           const tag::Unfold &, const std::vector < tag::Util::Action > &,
	           const tag::OverRegion &, const tag::Util::InequalitySet & c1,
	                                    const tag::Util::InequalitySet & c2,
	                                    const tag::Util::InequalitySet & c3,
	                                    const tag::Util::InequalitySet & c4    ) const;
	inline void export_to_file ( const tag::postscript &, std::string file_name,
	                      const tag::Unfold &, const tag::OverRegion &,
	                      const tag::Util::InequalitySet & c1,
	                      const tag::Util::InequalitySet & c2           ) const;
	inline void export_to_file ( const tag::postscript &, std::string file_name,
	                      const tag::Unfold &, const tag::OverRegion &,
	                      const tag::Util::InequalitySet & c1,
	                      const tag::Util::InequalitySet & c2,
	                      const tag::Util::InequalitySet & c3           ) const;
	inline void export_to_file ( const tag::postscript &, std::string file_name,
	                      const tag::Unfold &, const tag::OverRegion &,
	                      const tag::Util::InequalitySet & c1,
	                      const tag::Util::InequalitySet & c2,
	                      const tag::Util::InequalitySet & c3,
	                      const tag::Util::InequalitySet & c4           ) const;
	#endif  // ifndef MANIFEM_NO_QUOTIENT
	
	// we are still in class Mesh	
	// several versions of 'build' below are defined in global.cpp

	void build ( const tag::Segment &,  // builds a chain of n segment cells
	             const Cell & A, const Cell & B, const tag::DividedIn &, const size_t n );
	
	void build ( const tag::Triangle &, const Mesh & AB, const Mesh & BC, const Mesh & CA );

	void build ( const tag::Quadrangle &, const Mesh & south, const Mesh & east,
	             const Mesh & north, const Mesh & west, bool cut_rectangles_in_half );
	
	void build ( const tag::Hexahedron &, const Mesh & south, const Mesh & north,
	             Mesh east, Mesh west, const Mesh & up, const Mesh & down );
	
	#ifndef MANIFEM_NO_QUOTIENT

	void build ( const tag::Segment &,  // builds a chain of n segment cells
	             const Cell & A, const Cell & B, const tag::DividedIn &, const size_t n,
	             const tag::Winding &, const tag::Util::Action &                        );

	void build ( const tag::Triangle &, const Mesh & AB, const Mesh & BC, const Mesh & CA,
	             const tag::Winding &                                                     );

	void build ( const tag::Triangle &, const Mesh & AB, const Mesh & BC, const Mesh & CA,
	             const tag::Winding &, const tag::Singular &, const Cell & S              );
	
	void build ( const tag::Quadrangle &, const Mesh & south, const Mesh & east,
	             const Mesh & north, const Mesh & west, bool cut_rectangles_in_half,
	             const tag::Winding &                                               );
	
	void build ( const tag::Quadrangle &, const Mesh & south, const Mesh & east,
	             const Mesh & north, const Mesh & west, bool cut_rectangles_in_half,
	             const tag::Winding &, const tag::Singular &, const Cell & O        );
	
	void build ( const tag::Quadrangle &, const Cell & SW, const Cell & SE,
	             const Cell & NE, const Cell & NW, const size_t m, const size_t n,
	             bool cut_rectangles_in_half                                      );
	
	void build ( const tag::Hexahedron &, const Mesh & south, const Mesh & north,
	             Mesh east, Mesh west, const Mesh & up, const Mesh & down, const tag::Winding & );

	#endif  // ifndef MANIFEM_NO_QUOTIENT
	
	void remove_all_cells ( );
  
	#ifndef NDEBUG  // DEBUG mode
	inline void print_everything ( );
	#endif
	
	// we keep here the topological dimension of the largest mesh we intend to build
	// see method 'set_max_dim' and paragraph 12.7 in the manual
	static size_t maximum_dimension_plus_one;

	inline static void set_max_dim ( const size_t d );
	// invoked at the beginning of mesh.cpp
	// see paragraph 12.7 in the manual
	
	struct Connected  {  class OneDim;  class HighDim;  };
	struct MultiplyConnected  {  class OneDim;  class HighDim; };
	class ZeroDim;  class NotZeroDim;  class Fuzzy;  class STSI;

	class Composite;
	class Build;
	
}; // end of  class Mesh

//------------------------------------------------------------------------------------------------------//

inline bool operator== ( const Mesh & m1, const Mesh & m2 )
{	if ( m1 .core != m2 .core )  return false;
	return  ( m1 .is_positive() ) == ( m2 .is_positive() );  }

inline bool operator!= ( const Mesh & m1, const Mesh & m2 )
{	return  not ( m1 == m2 );  }

inline bool operator< ( const Mesh & m1, const Mesh & m2 )
{	if ( m1 .core == m2 .core )
		return  m1 .is_positive() and not m2 .is_positive();
	return  m1 .core < m2 .core;                             }

//------------------------------------------------------------------------------------------------------//
//-------------------------------      core Cells     --------------------------------------------------//
//------------------------------------------------------------------------------------------------------//


// I would very much prefer the name 'class Cell::Core' instead of 'tag::Util::CellCore'
// but it was not possible ...


#ifdef MANIFEM_COLLECT_CM	

class tag::Util::CellCore : public tag::Util::Core::DelegateDispose

#else  // no MANIFEM_COLLECT_CM	

class tag::Util::CellCore : public tag::Util::Core::Inactive

#endif  // MANIFEM_COLLECT_CM	

// abstract class
// specialized in Cell::Positive::{Vertex,Segment,HighDim} and
//                Cell::Negative::{Vertex,Segment,HighDim}

{	public :
	
	// size_t nb_of_wrappers  inherited from tag::Util::Core ifdef MANIFEM_COLLECT_CM
	
	// we keep numeric values here :
	std::vector < double > double_heap;
	std::vector < size_t > size_t_heap;
	std::vector < short int > short_int_heap;
	// and heterogeneous information here :
	std::map < tag::KeyForHook, void * > hook;

	// if 'this' is a face of another cell and that cell belongs to some mesh msh,
	// cell_behind_within [msh] keeps that cell
	// see methods Mesh::cell_behind and Mesh::cell_in_front_of
	std::map < Mesh::Core *, Cell > cell_behind_within;
	// we use the Cell wrapper as pointer

	Cell reverse_attr;  // we use a wrapper as pointer

	#ifndef NDEBUG  // DEBUG mode
	std::string name;
	#endif

	inline CellCore ( const tag::OfDimension &, const size_t d,  // for positive cells
	                  const tag::HasNoReverse &, const tag::OneDummyWrapper & );

	inline CellCore ( const tag::OfDimension &, const size_t d,  // for negative cells
	           const tag::ReverseOf &, Cell::Core * direct_cell_p, const tag::OneDummyWrapper & );

	virtual ~CellCore ( )
	{	}
	// { std::cout << "Cell::Core destructor, counter " << Cell::counter;
	// 	Cell::counter--;
	// 	std::cout << "->" << Cell::counter << std::endl;  }

	// bool dispose_query ( )  defined by tag::Util::Core::DelegateDispose ifdef MANIFEM_COLLECT_CM
	
	virtual bool is_positive ( ) const = 0;
	virtual Cell get_positive ( ) = 0;
	virtual size_t get_dim ( ) const = 0;

	virtual Cell tip ();
	virtual Cell base ();
	// execution forbidden
	// overridden by Cell::Positive::Segment and Cell::Negative::Segment

	virtual Mesh boundary ( ) = 0;

	virtual bool belongs_to_ld
	( Mesh::Core *, const tag::CellHasLowDim &,
	  const tag::NotOriented & no = tag::not_oriented ) const = 0;
	virtual bool belongs_to
	( Mesh::Core *, const tag::SameDim &,
	  const tag::OrientCompWithMesh & o = tag::orientation_compatible_with_mesh ) const = 0;
	bool belongs_to ( Mesh::Core *, const tag::SameDim &, const tag::NotOriented & ) const;

	// Method 'glue_on_bdry_of' is intensively used when building a mesh,
	// e.g. within factory functions in Cell class.
	// It glues 'this' cell to the boundary of 'cll'.
	inline void glue_on_bdry_of ( Cell::Core * cll )
	{	assert ( cll );
		cll->glue_on_my_bdry ( this );   }
	inline void glue_on_bdry_of ( Cell::Core * cll, const tag::DoNotBother & )
	{	assert ( cll );
		cll->glue_on_my_bdry ( this, tag::do_not_bother );   }
	// tag::do_not_bother is useful for a Mesh::Connected::OneDim

	// Method 'cut_from_bdry_of' does the reverse : cuts 'this' cell from
	// the boundary of 'cll'. Used mainly in remeshing.
	inline void cut_from_bdry_of ( Cell::Core * cll )
	{	cll->cut_from_my_bdry ( this );   }
	inline void cut_from_bdry_of ( Cell::Core * cll, const tag::DoNotBother & )
	{	cll->cut_from_my_bdry ( this, tag::do_not_bother );   }
	// tag::do_not_bother is useful for a Mesh::Connected::OneDim

	// we are still in  class Cell::Core

	// the four methods below are only relevant for vertices
	virtual void add_to_seg ( Cell::PositiveSegment * seg ) = 0;
	virtual void add_to_seg ( Cell::PositiveSegment * seg, const tag::DoNotBother & ) = 0;
	virtual void remove_from_seg ( Cell::PositiveSegment * seg ) = 0;
	virtual void remove_from_seg ( Cell::PositiveSegment * seg, const tag::DoNotBother & ) = 0;

	// the nine methods below are not relevant for vertices
	virtual void add_to_mesh ( Mesh::Core * msh ) = 0;
	virtual void add_to_mesh ( Mesh::Core * msh, const tag::DoNotBother & ) = 0;
	virtual void add_to_mesh  // here execution forbidden, overridden by segments
	( Mesh::Core * msh, const tag::DoNotBother &, const tag::KeepAsFirstVer & );
	virtual void add_to_mesh ( Mesh::Core * msh, const tag::Force & ) = 0;  // 'msh' must be Mesh::STSI
	virtual void add_to_mesh ( Mesh::Core * msh,  // 'msh' must be Mesh::STSI
	                           const tag::NeighbRels &, const std::map < Cell, Cell > & rels,
	                           bool force_other_faces                                        ) = 0;
	virtual void remove_from_mesh ( Mesh::Core * msh ) = 0;
	virtual void remove_from_mesh ( Mesh::Core * msh, const tag::DoNotBother & ) = 0;
	virtual void add_to_bdry ( Mesh::Core * msh ) = 0;
	virtual void add_to_bdry ( Mesh::Core * msh, const tag::DoNotBother & ) = 0;
	virtual void remove_from_bdry ( Mesh::Core * msh ) = 0;
	virtual void remove_from_bdry ( Mesh::Core * msh, const tag::DoNotBother & ) = 0;
	// tag::do_not_bother is useful for a Mesh::Connected::OneDim

	virtual void glue_on_my_bdry ( Cell::Core * ) = 0;
	virtual void glue_on_my_bdry ( Cell::Core *, const tag::DoNotBother & ) = 0;
	virtual void cut_from_my_bdry ( Cell::Core * ) = 0;
	virtual void cut_from_my_bdry ( Cell::Core *, const tag::DoNotBother & ) = 0;
	// tag::do_not_bother is useful for a Mesh::Connected::OneDim
	// we feel that 'glue_on_bdry_of' and 'cut_from_bdry_of' are more readable
	// so we suggest to use those (see above)

	// methods 'iterator' below are defined in iterator.cpp, execution forbidden
	// and are later overridden
	virtual Cell::Iterator::Core * iterator_1
	( const tag::OverMeshesAbove &, const tag::SameDim &, const tag::OrientCompWithCell &,
	  const tag::ThisCellIsPositive &, const tag::DoNotBuildCells &                       );
	virtual Cell::Iterator::Core * iterator_2
	( const tag::OverMeshesAbove &, const tag::SameDim &, const tag::OrientOpposToCell &,
	  const tag::ThisCellIsPositive &, const tag::DoNotBuildCells &                      );
	virtual Cell::Iterator::Core * iterator_3
	( const tag::OverMeshesAbove &, const tag::OfDimension &, const size_t d,
	  const tag::OrientationIrrelevant &, const tag::ThisCellIsPositive &    );

	virtual void compute_sign ( short int & cp, short int & cn, Mesh::Core * const cell_bdry ) = 0;

	#ifndef NDEBUG  // DEBUG mode
	virtual std::string get_name ( ) = 0;
	virtual void print_everything ( ) = 0;
	#endif

}; // end of class Cell::Core

//------------------------------------------------------------------------------------------------------//


class Cell::Positive : public Cell::Core

// abstract class, introduces attribute  meshes
// and defines virtual methods  is_positive, get_positive

// specialized in Cell::Positive{Vertex,Segment,HighDim}

{	public :

	typedef Cell::PositiveVertex Vertex;
	typedef Cell::PositiveNotVertex NotVertex;
	typedef Cell::PositiveSegment Segment;
	typedef Cell::PositiveHighDim HighDim;
	
	// size_t nb_of_wrappers  inherited from tag::Util::Core ifdef MANIFEM_COLLECT_CM

	// attributes inherited from Cell::Core :
	// std::vector < double > double_heap
	// std::vector < size_t > size_t_heap
	// std::vector < short int > short_int_heap
	// std::map < tag::KeyForHook, void * > hook
	// std::map < Mesh::Core *, Cell > cell_behind_within
	// Cell reverse_attr

	// the 'meshes' attribute keeps information about all meshes
	// "above" 'this' cell, that is, all meshes containing 'this' as a cell
	// it is indexed over the dimension of the mesh minus the dimension of 'this' 
	// for each mesh, it keeps a 'Cell::field_to_meshes' value, containing two counters
	// and an iterator into the 'cells' field of that particular mesh
	// the iterator is only meaningful for fuzzy or STSI meshes
	// of course this implies quite some amount of redundant information
	// but this redundancy makes the classes fast, especially for remeshing
	
	// the keys of 'meshes[i]' will be meshes of dimension 'i + this->dim'
	// however, for meshes of the same dimension as 'this' cell,
	// we keep a different record in Cell::Positive::Vertex::segments
	// and Cell::Positive::NotVertex::meshes_same_dim
	// so meshes[0] will always be an empty map

	std::vector < std::map < Mesh::Core*, Cell::field_to_meshes > > meshes;
	// short int counter_pos, counter_neg, std::list < Cell > ::iterator where

	static std::vector < size_t > double_heap_size, size_t_heap_size, short_int_heap_size;

	inline Positive ( const tag::OfDimension &, const size_t d,
	                  const tag::SizeMeshes &, const size_t sz, const tag::OneDummyWrapper & )
	:	Cell::Core ( tag::of_dim, d, tag::has_no_reverse, tag::one_dummy_wrapper ),
		meshes ( sz )
	{	}

	virtual ~Positive ( ) { }

	// bool dispose_query ( )  defined by tag::Util::Core::DelegateDispose ifdef MANIFEM_COLLECT_CM
	
	bool is_positive ( ) const;  // virtual from Cell::Core
	Cell get_positive ( );  // virtual from Cell::Core

	virtual Cell::Negative * build_reverse ( const tag::OneDummyWrapper & ) = 0;

	bool belongs_to_ld  // cell has dimension lower than the mesh  // virtual from Cell::Core
	( Mesh::Core *, const tag::CellHasLowDim &,
	  const tag::NotOriented & no = tag::not_oriented ) const;
	// belongs_to ( with tag::same_dim, tag::orientation_compatible_with_mesh )
	//    stays pure virtual from Cell::Core
	// belongs_to (with tag::same_dim, tag::not_oriented )  defined by Cell::Core

	// two versions of glue_on_bdry_of and two of cut_from_bdry_of defined inline by Cell::Core

	// add_to_seg  and  remove_from_seg  stay pure virtual from Cell:Core

	// (two versions of each) add_to_mesh, remove_from_mesh, add_to_bdry, remove_from_bdry
	// stay pure virtual from Cell:Core

	// two versions of glue_on_my_bdry and two of cut_from_my_bdry
	// stay pure virtual from Cell:Core

	// two iterators over meshes of same dim  virtual from Cell::Core, execution forbiden
	Cell::Iterator::Core * iterator_3  // virtual from Cell::Core
	( const tag::OverMeshesAbove &, const tag::OfDimension &, const size_t d,
	  const tag::OrientationIrrelevant &, const tag::ThisCellIsPositive &    ) override;
  
	// compute_sign  stays pure virtual from Cell::Core
	
	#ifndef NDEBUG  // DEBUG mode
	std::string get_name ( );  // virtual from Cell::Core
	// void print_everything ( );  stays pure virtual from Cell::Core
	#endif
	
}; // end of class Cell::Positive

//------------------------------------------------------------------------------------------------------//


class Cell::Negative : public Cell::Core

// abstract class, specialized in Cell::Negative::{Vertex,Segment,HighDim}

{	public :

	typedef Cell::NegativeVertex Vertex;
	typedef Cell::NegativeNotVertex NotVertex;
	typedef Cell::NegativeSegment Segment;
	typedef Cell::NegativeHighDim HighDim;
	// I would very much prefer the name 'Cell::Negative::Vertex', 'Cell::Negative::Segment'
	// instead of 'Cell::NegativeVertex', 'Cell::NegativeSegment'
	// but it was not possible ...
	
	// size_t nb_of_wrappers  inherited from tag::Util::Core ifdef MANIFEM_COLLECT_CM

	// attributes inherited from Cell::Core :
	// std::vector < double > double_heap
	// std::vector < size_t > size_t_heap
	// std::vector < short int > short_int_heap
	// std::map < tag::KeyForHook, void * > hook
	// std::map < Mesh::Core *, Cell > cell_behind_within
	// Cell reverse_attr

	static std::vector < size_t > double_heap_size, size_t_heap_size, short_int_heap_size;

	inline Negative ( const tag::OfDimension &, const size_t d,
	                  const tag::ReverseOf &, Cell::Positive * direct, const tag::OneDummyWrapper & )
	: Cell::Core ( tag::of_dim, d, tag::reverse_of, direct, tag::one_dummy_wrapper )
	{	}

	virtual ~Negative ( ) { }
	
	bool is_positive ( ) const;  // virtual from Cell::Core
	Cell get_positive ( );  // virtual from Cell::Core
	// virtual size_t get_dim ( ) const = 0;  // declared in Cell::Core

	bool belongs_to_ld  // cell has dimension lower than the mesh, virtual from Cell::Core
	( Mesh::Core *, const tag::CellHasLowDim &,
	  const tag::NotOriented & no = tag::not_oriented ) const;
	// belongs_to ( with tag::same_dim, tag::orientation_compatible_with_mesh )
	//    stays pure virtual from Cell::Core
	// belongs_to ( with tag::same_dim, tag::not_oriented )  defined by Cell::Core

	// two versions of glue_on_bdry_of and two of cut_from_bdry_of defined inline by Cell::Core

	// add_to_seg  and  remove_from_seg  stay pure virtual from Cell:Core

	// (two versions of each) add_to_mesh, remove_from_mesh, add_to_bdry, remove_from_bdry
	// stay pure virtual from Cell:Core

	// four methods below are virtual from Cell::Core
	void glue_on_my_bdry ( Cell::Core * );
	void glue_on_my_bdry ( Cell::Core *, const tag::DoNotBother & );
	void cut_from_my_bdry ( Cell::Core * );
	void cut_from_my_bdry ( Cell::Core *, const tag::DoNotBother & );
	// tag::do_not_bother is useful for a Mesh::Connected::OneDim
	// we feel that 'glue_on_bdry_of' and 'cut_from_bdry_of' are more readable
	// so we suggest to use those

	// three iterators over meshes  virtual from Cell::Core, execution forbiden
	
	// method below is virtual from Cell::Core, here execution forbidden
	void compute_sign ( short int & cp, short int & cn, Mesh::Core * const cell_bdry );

	#ifndef NDEBUG  // DEBUG mode
	std::string get_name ( );  // virtual from Cell::Core
	// void print_everything ( );  stays pure virtual from Cell::Core
	#endif
	
}; // end of class Cell::Negative

//------------------------------------------------------------------------------------------------------//


// I would very much prefer the name
// 'class Cell::Positive::Vertex' instead of 'Cell::PositiveVertex'
// but it was not possible ...

class Cell::PositiveVertex : public Cell::Positive

{	public :

	// size_t nb_of_wrappers  inherited from tag::Util::Core ifdef MANIFEM_COLLECT_CM

	// attributes inherited from Cell::Core :
	// double_heap, size_t_heap, short_int_heap, hook, cell_behind_within

	// Cell reverse_attr  inherited from Cell::Core

	// inherited from Cell::Positive :
	// std::vector < std::map < Mesh::Core*, Cell::field_to_meshes > > meshes

	// in 'segments' we keep record of meshes of dimension zero "above" 'this' vertex,
	// that is, of segments having 'this' extremity
	// the 'short int' is a sign, 1 or -1

	std::map < Cell::Positive::Segment *, short int > segments;
	
	inline PositiveVertex ( const tag::OneDummyWrapper & );

	PositiveVertex ( const Cell::Positive::Vertex &  ) = delete;
	PositiveVertex ( const Cell::Positive::Vertex && ) = delete;
	Cell::Positive::Vertex & operator= ( const Cell::Positive::Vertex &  ) = delete;
	Cell::Positive::Vertex & operator= ( const Cell::Positive::Vertex && ) = delete;

	virtual ~PositiveVertex ( ) { }

	// bool dispose_query ( )  defined by tag::Util::Core::DelegateDispose ifdef MANIFEM_COLLECT_CM
	
	// is_positive  and  get_positive  defined by Cell::Positive
	size_t get_dim ( ) const; // virtual from Cell::Core

	// tip  and  base  defined by Cell::Core, execution forbidden
	
	Mesh boundary ( );  // virtual from Cell::Core, here execution forbidden

	Cell::Negative * build_reverse ( const tag::OneDummyWrapper & );
	// virtual from Cell::Positive

	// belongs_to ( with tag::cell_has_low_dim )  defined by Cell::Positive
	bool belongs_to  // virtual from Cell::Core
	( Mesh::Core *, const tag::SameDim &,
	  const tag::OrientCompWithMesh & o = tag::orientation_compatible_with_mesh ) const;
	// belongs_to ( with tag::same_dim, tag::not_oriented )  defined by Cell::Core

	// glue_on_bdry_of  and  cut_from_bdry_of  defined by Cell::Core

	// four methods below virtual from Cell::Core
	void add_to_seg ( Cell::Positive::Segment * seg );
	void add_to_seg ( Cell::Positive::Segment * seg, const tag::DoNotBother & );
	void remove_from_seg ( Cell::Positive::Segment * seg );
	void remove_from_seg ( Cell::Positive::Segment * seg, const tag::DoNotBother & );

	// the twelve methods below are virtual from Cell::Core, here execution forbidden
	void add_to_mesh ( Mesh::Core * msh );
	void add_to_mesh ( Mesh::Core * msh, const tag::DoNotBother & );
	void add_to_mesh ( Mesh::Core * msh, const tag::Force & );  // 'msh' must be Mesh::STSI
	void add_to_mesh ( Mesh::Core * msh,  // 'msh' must be Mesh::STSI
	                   const tag::NeighbRels &, const std::map < Cell, Cell > & rels,
	                   bool force_other_faces                                        );
	void remove_from_mesh ( Mesh::Core * msh );
	void remove_from_mesh ( Mesh::Core * msh, const tag::DoNotBother & );
	void add_to_bdry ( Mesh::Core * msh );
	void add_to_bdry ( Mesh::Core * msh, const tag::DoNotBother & );
	void remove_from_bdry ( Mesh::Core * msh );
	void remove_from_bdry ( Mesh::Core * msh, const tag::DoNotBother & );
	void glue_on_my_bdry ( Cell::Core * );
	void glue_on_my_bdry ( Cell::Core *, const tag::DoNotBother & );
	void cut_from_my_bdry ( Cell::Core * );
	void cut_from_my_bdry ( Cell::Core *, const tag::DoNotBother & );

	// two iterators over meshes of same dim  virtual from Cell::Core, execution forbiden
	// an iterator over meshes above  virtual from Cell::Core, defined by Cell::Positive
	
	// method below is virtual from Cell::Core
	void compute_sign ( short int & cp, short int & cn, Mesh::Core * const cell_bdry );
	
	#ifndef NDEBUG  // DEBUG mode
	// std::string get_name ( )  defined by Cell::Positive
	void print_everything ( );  // virtual from Cell::Core
	#endif
	
}; // end of  class Cell::PositiveVertex, aka Cell::Positive::Vertex

//------------------------------------------------------------------------------------------------------//


// I would very much prefer the name
// 'class Cell::Negative::Vertex' instead of 'Cell::NegativeVertex'
// but it was not possible ...

class Cell::NegativeVertex : public Cell::Negative

{	public :

	// size_t nb_of_wrappers  inherited from tag::Util::Core ifdef MANIFEM_COLLECT_CM

	// attributes inherited from Cell::Core :
	// double_heap, size_t_heap, short_int_heap, hook, cell_behind_within

	// Cell reverse_attr  inherited from Cell::Core

	inline NegativeVertex
	( const tag::ReverseOf &, Cell::Positive * direct_ver_p, const tag::OneDummyWrapper & );

	NegativeVertex ( const Cell::Negative::Vertex & ) = delete;
	NegativeVertex ( const Cell::Negative::Vertex && ) = delete;
	Cell::Negative::Vertex & operator= ( const Cell::Negative::Vertex & ) = delete;
	Cell::Negative::Vertex & operator= ( const Cell::Negative::Vertex && ) = delete;

	virtual ~NegativeVertex ( ) { }
	
	// bool dispose_query ( )  defined by tag::Util::Core::DelegateDispose ifdef MANIFEM_COLLECT_CM
	
	// is_positive  and  get_positive  defined by Cell::Negative
	size_t get_dim ( ) const; // virtual from Cell::Core
	
	// tip  and  base  defined by Cell::Core, execution forbidden

	Mesh boundary ( );  // virtual from Cell::Core, here execution forbidden

	// belongs_to ( with tag::cell_has_low_dim )  defined by Cell::Negative
	bool belongs_to   // virtual from Cell::Core
	( Mesh::Core *, const tag::SameDim &,
	  const tag::OrientCompWithMesh & o = tag::orientation_compatible_with_mesh ) const;
	// belongs_to ( with tag::same_dim, tag::not_oriented )  defined by Cell::Core

	// glue_on_bdry_of  and  cut_from_bdry_of  defined by Cell::Core

	// methods 'add_to' and 'remove_from' add/remove 'this' cell to/from the mesh 'msh'
	// see paragraph 12.9 in the manual

	// four methods below virtual from Cell::Core
	void add_to_seg ( Cell::Positive::Segment * seg );
	void add_to_seg ( Cell::Positive::Segment * seg, const tag::DoNotBother & );
	void remove_from_seg ( Cell::Positive::Segment * seg );
	void remove_from_seg ( Cell::Positive::Segment * seg, const tag::DoNotBother & );

	// the nine methods below are virtual from Cell::Core, here execution forbidden
	void add_to_mesh ( Mesh::Core * msh );
	void add_to_mesh ( Mesh::Core * msh, const tag::DoNotBother & );
	void add_to_mesh ( Mesh::Core * msh, const tag::Force & );  // 'msh' must be Mesh::STSI
	void add_to_mesh ( Mesh::Core * msh,  // 'msh' must be Mesh::STSI
	                   const tag::NeighbRels &, const std::map < Cell, Cell > & rels,
	                   bool force_other_faces                                        );
	void remove_from_mesh ( Mesh::Core * msh );
	void remove_from_mesh ( Mesh::Core * msh, const tag::DoNotBother & );
	void add_to_bdry ( Mesh::Core * msh );
	void add_to_bdry ( Mesh::Core * msh, const tag::DoNotBother & );
	void remove_from_bdry ( Mesh::Core * msh );
	void remove_from_bdry ( Mesh::Core * msh, const tag::DoNotBother & );

	// glue_on_my_bdry  and  cut_from_my_bdry  defined by Cell:Negative
	// three iterators over meshes  virtual from Cell::Core, execution forbiden	
	// compute_sign  defined by Cell::Negative, execution forbidden
	
	#ifndef NDEBUG  // DEBUG mode
	// std::string get_name ( )  defined by Cell::Negative
	void print_everything ( );  // virtual from Cell::Core
	#endif
	
}; // end of  class Cell::NegativeVertex, aka Cell::Negative::Vertex

//------------------------------------------------------------------------------------------------------//


// I would very much prefer the name
// 'class Cell::Positive::NotVertex' instead of 'Cell::PositiveNotVertex'
// but it was not possible ...

class Cell::PositiveNotVertex : public Cell::Positive

// abstract class, specialized in Cell::Positive::Segment, Cell::Positive::HighDim

{	public :

	// size_t nb_of_wrappers  inherited from tag::Util::Core ifdef MANIFEM_COLLECT_CM

	// attributes inherited from Cell::Core :
	// double_heap, size_t_heap, short_int_heap, hook, cell_behind_within

	// Cell reverse_attr  inherited from Cell::Core

	// inherited from Cell::Positive :
	// std::vector < std::map < Mesh::Core*, Cell::field_to_meshes > > meshes

	// in 'meshes_same_dim' we keep record of meshes "above" 'this' cell,
	//   of the same dimension
	// in Cell::field_to_meshes_same_dim, the 'short int sign' is a sign, 1 or -1
	// the iterator 'where' is only meaningful for fuzzy or STSI meshes

	std::map < Mesh::Core*, Cell::field_to_meshes_same_dim > meshes_same_dim;
	
	inline PositiveNotVertex ( const tag::OfDimension &, const size_t d,
                             const tag::SizeMeshes &, const size_t sz,
                             const tag::OneDummyWrapper &              )
	: Cell::Positive ( tag::of_dim, d, tag::size_meshes, sz, tag::one_dummy_wrapper )
	{	}

	virtual ~PositiveNotVertex ( ) { }
	
	// bool dispose_query ( )  defined by tag::Util::Core::DelegateDispose ifdef MANIFEM_COLLECT_CM
	
	// Cell::Negative * build_reverse ( const tag::OneDummyWrapper & )
	//   stays pure virtual from Cell::Positive

	// belongs_to ( with tag::cell_has_low_dim )  defined by Cell::Positive
	bool belongs_to  // virtual from Cell::Core
	( Mesh::Core *, const tag::SameDim &,
	  const tag::OrientCompWithMesh & o = tag::orientation_compatible_with_mesh ) const;
	// belongs_to ( with tag::same_dim, tag::not_oriented )  defined by Cell::Core

	// four methods below virtual from Cell::Core, here execution forbidden
	void add_to_seg ( Cell::Positive::Segment * seg );
	void add_to_seg ( Cell::Positive::Segment * seg, const tag::DoNotBother & );
	void remove_from_seg ( Cell::Positive::Segment * seg );
	void remove_from_seg ( Cell::Positive::Segment * seg, const tag::DoNotBother & );

	// (two versions of each) add_to_mesh, remove_from_mesh, add_to_bdry, remove_from_bdry
	//   stay pure virtual from Cell::Core

	Cell::Iterator::Core * iterator_1  // virtual from Cell::Core
	( const tag::OverMeshesAbove &, const tag::SameDim &, const tag::OrientCompWithCell &,
	  const tag::ThisCellIsPositive &, const tag::DoNotBuildCells &                       ) override;
	Cell::Iterator::Core * iterator_2  // virtual from Cell::Core
	( const tag::OverMeshesAbove &, const tag::SameDim &, const tag::OrientOpposToCell &,
	  const tag::ThisCellIsPositive &, const tag::DoNotBuildCells &                      ) override;
	// an iterator over meshes above  virtual from Cell::Core, defined by Cell::Positive

	// method below is virtual from Cell::Core
	void compute_sign ( short int & cp, short int & cn, Mesh::Core * const cell_bdry );
		
	#ifndef NDEBUG  // DEBUG mode
	// std::string get_name ( )  defined by Cell::Positive
	// void print_everything ( )  stays pure virtual from Cell::Core
	#endif

}; // end of  class Cell::PositiveNotVertex, aka Cell::Positive::NotVertex

//------------------------------------------------------------------------------------------------------//


// I would very much prefer the name
// 'class Cell::Negative::NotVertex' instead of 'Cell::NegativeNotVertex'
// but it was not possible ...

class Cell::NegativeNotVertex : public Cell::Negative

// abstract class, specialized in Cell::Negative::Segment, Cell::Negative::HighDim

{	public :

	// size_t nb_of_wrappers  inherited from tag::Util::Core ifdef MANIFEM_COLLECT_CM

	// attributes inherited from Cell::Core :
	// double_heap, size_t_heap, short_int_heap, hook, cell_behind_within

	// Cell reverse_attr  inherited from Cell::Core

	inline NegativeNotVertex
	( const tag::OfDimension &, size_t d,
	  const tag::ReverseOf &, Cell::Positive * direct_cll_p, const tag::OneDummyWrapper & );

	virtual ~NegativeNotVertex ( ) { }
	
	// belongs_to ( with tag::cell_has_low_dim )  defined by Cell::Negative
	bool belongs_to   // virtual from Cell::Core
	( Mesh::Core *, const tag::SameDim &,
	  const tag::OrientCompWithMesh & o = tag::orientation_compatible_with_mesh ) const;
	// belongs_to ( with tag::same_dim, tag::not_oriented )  defined by Cell::Core

	// four methods below virtual from Cell::Core, here execution forbidden
	void add_to_seg ( Cell::Positive::Segment * seg );
	void add_to_seg ( Cell::Positive::Segment * seg, const tag::DoNotBother & );
	void remove_from_seg ( Cell::Positive::Segment * seg );
	void remove_from_seg ( Cell::Positive::Segment * seg, const tag::DoNotBother & );

	// (two versions of each) add_to_mesh, remove_from_mesh, add_to_bdry, remove_from_bdry
	// stay pure virtual from Cell::Core
	
	// three iterators over meshes  virtual from Cell::Core, execution forbiden
	// compute_sign  defined by Cell::Negative, execution forbidden
	
	#ifndef NDEBUG  // DEBUG mode
	// std::string get_name ( )  defined by Cell::Positive
	// void print_everything ( )  stays pure virtual from Cell::Core
	#endif

};  // end of  class Cell::NegativeNotVertex (aka class Cell::Negative::NotVertex)
	
//------------------------------------------------------------------------------------------------------//


	
// I would very much prefer the name
// 'class Cell::Positive::Segment' instead of 'Cell::PositiveSegment'
// but it was not possible ...

class Cell::PositiveSegment : public Cell::Positive::NotVertex

{	public :

	// size_t nb_of_wrappers  inherited from tag::Util::Core ifdef MANIFEM_COLLECT_CM

	// attributes inherited from Cell::Core :
	// double_heap, size_t_heap, short_int_heap, hook, cell_behind_within

	// Cell reverse_attr  inherited from Cell::Core

	// inherited from Cell::Positive :
	// std::vector < std::map < Mesh::Core*, Cell::field_to_meshes > > meshes

	// inherited from Cell::Positive::NotVertex :
	// std::map < Mesh::Core*, Cell::field_to_meshes_same_dim > meshes_same_dim

	// here we use Cell wrappers as smart pointers
	// because a segment should keep its extremities alive
	Cell base_attr, tip_attr;

	inline PositiveSegment ( Cell A, Cell B, const tag::OneDummyWrapper & );

	PositiveSegment ( const Cell::Positive::Segment & ) = delete;
	PositiveSegment ( const Cell::Positive::Segment && ) = delete;
	Cell::Positive::Segment & operator= ( const Cell::Positive::Segment & ) = delete;
	Cell::Positive::Segment & operator= ( const Cell::Positive::Segment && ) = delete;

	virtual ~PositiveSegment ( )
	{ assert ( base_attr .exists() );
		assert ( tip_attr  .exists() );
		assert (     tip_attr  .is_positive() );
		assert ( not base_attr .is_positive() );
		Cell::Positive::Vertex * pos_base_c = tag::Util::assert_cast
			< Cell::Core*, Cell::Positive::Vertex* > ( base_attr .core->reverse_attr .core );
		std::map < Cell::Positive::Segment*, short int > ::iterator
			it = pos_base_c->segments .find ( this );
		assert ( it != pos_base_c->segments .end() );
		assert ( it->second == -1 );
		pos_base_c->segments .erase ( it );
		Cell::Positive::Vertex * tip_c = tag::Util::assert_cast
			< Cell::Core*, Cell::Positive::Vertex* > ( tip_attr .core );
		it = tip_c->segments .find ( this );
		assert ( it != tip_c->segments .end() );
		assert ( it->second == 1 );
		tip_c->segments .erase ( it );                                                       }
	
	// is_positive  and  get_positive  defined by Cell::Positive
	size_t get_dim ( ) const; // virtual from Cell::Core

	Cell tip () override; // virtual, overrides definition by Cell::Core
	Cell base () override; // virtual, overrides definition by Cell::Core

	Mesh boundary ( );  // virtual from Cell::Core

	Cell::Negative * build_reverse ( const tag::OneDummyWrapper & );
	// virtual from Cell::Positive

	// belongs_to ( with tag::cell_has_low_dim )  defined by Cell::Positive
	// belongs_to (with tag::same_dim, tag::orientation_compatible_with_mesh )
  //    defined by Cell::Positive::NotVertex
	// belongs_to (with tag::same_dim, tag::not_oriented )  defined by Cell::Core

	// glue_on_bdry_of  and  cut_from_bdry_of  defined by Cell::Core

	// methods 'add_to' and 'remove_from' add/remove 'this' cell to/from the mesh 'msh'
	// see paragraph 12.9 in the manual

	// add_to_seg and remove_from seg virtual from Cell::Core,
	// defined by Cell::Positive::NotVertex, execution forbidden

	// the fourteen methods below are virtual from Cell::Core
	void add_to_mesh ( Mesh::Core * msh );
	void add_to_mesh ( Mesh::Core * msh, const tag::DoNotBother & );
	void add_to_mesh  // keep 'this' segment as 'first_ver' although this is of course not true
	( Mesh::Core * msh, const tag::DoNotBother &, const tag::KeepAsFirstVer & ) override;
	void add_to_mesh ( Mesh::Core * msh, const tag::Force & );  // 'msh' must be Mesh::STSI
	void add_to_mesh ( Mesh::Core * msh,  // 'msh' must be Mesh::STSI
	                   const tag::NeighbRels &, const std::map < Cell, Cell > & rels,
	                   bool force_other_faces                                        );
	void remove_from_mesh ( Mesh::Core * msh );
	void remove_from_mesh ( Mesh::Core * msh, const tag::DoNotBother & );
	void add_to_bdry ( Mesh::Core * msh );
	void add_to_bdry ( Mesh::Core * msh, const tag::DoNotBother & );
	void remove_from_bdry ( Mesh::Core * msh );
	void remove_from_bdry ( Mesh::Core * msh, const tag::DoNotBother & );
	void glue_on_my_bdry ( Cell::Core * );
	void glue_on_my_bdry ( Cell::Core *, const tag::DoNotBother & );
	void cut_from_my_bdry ( Cell::Core * );
	void cut_from_my_bdry ( Cell::Core *, const tag::DoNotBother & );
	// tag::do_not_bother is useful for a Mesh::Connected::OneDim	
	// we feel that 'glue_on_bdry_of' and 'cut_from_bdry_of' are more readable
	// so we suggest to use those (see class Cell::Core)

	// two iterators over meshes of same dim  virtual from Cell::Core, defined by Cell::Positive::NotVertex
	// an iterator over meshes above  virtual from Cell::Core, defined by Cell::Positive
	// compute_sign  defined by Cell::Positive::NotVertex

	#ifndef NDEBUG  // DEBUG mode
	// std::string get_name ( )  defined by Cell::Positive
	void print_everything ( );  // virtual from Cell::Core
	#endif
	
}; // end of  class Cell::PositiveSegment, aka Cell::Positive::Segment

//------------------------------------------------------------------------------------------------------//


// I would very much prefer the name
// 'class Cell::Negative::Segment' instead of 'Cell::NegativeSegment'
// but it was not possible ...

class Cell::NegativeSegment : public Cell::Negative::NotVertex

{	public :

	// size_t nb_of_wrappers  inherited from tag::Util::Core ifdef MANIFEM_COLLECT_CM

	// attributes inherited from Cell::Core :
	// double_heap, size_t_heap, short_int_heap, hook, cell_behind_within

	// Cell reverse_attr  inherited from Cell::Core

	inline NegativeSegment
	( const tag::ReverseOf &, Cell::Positive * direct_seg_p, const tag::OneDummyWrapper & );

	// bool dispose_query ( )  defined by tag::Util::Core::DelegateDispose ifdef MANIFEM_COLLECT_CM
	
	NegativeSegment ( const Cell::Negative::Segment & ) = delete;
	NegativeSegment ( const Cell::Negative::Segment && ) = delete;
	Cell::Negative::Segment & operator= ( const Cell::Negative::Segment & ) = delete;
	Cell::Negative::Segment & operator= ( const Cell::Negative::Segment && ) = delete;

	virtual ~NegativeSegment ( ) { }

	// is_positive  and  get_positive  defined by Cell::Negative
	size_t get_dim ( ) const; // virtual from Cell::Core
	
	Cell tip () override;  // virtual, overrides definition by Cell::Core
	Cell base () override;  // virtual, overrides definition by Cell::Core

	Mesh boundary ( );  // virtual from Cell::Core

	// belongs_to ( with tag::cell_has_low_dim )  defined by Cell::Negative
	// belongs_to (with tag::same_dim, tag::orientation_compatible_with_mesh)
	//     defined by Cell::Negative::NotVertex
	// belongs_to (with tag::same_dim, tag::not_oriented )  defined by Cell::Core

	// glue_on_bdry_of  and  cut_from_bdry_of  defined by Cell::Core

	// methods 'add_to' and 'remove_from' add/remove 'this' cell to/from the mesh 'msh'
	// see paragraph 12.9 in the manual

	// add_to_seg and remove_from seg virtual from Cell::Core,
	// defined by Cell::Negative::NotVertex, execution forbidden

	// eleven methods below are virtual from Cell::Core
	void add_to_mesh ( Mesh::Core * msh );
	void add_to_mesh ( Mesh::Core * msh, const tag::DoNotBother & );
	void add_to_mesh  // keep 'this' segment as 'first_ver' although this is of course not true
	( Mesh::Core * msh, const tag::DoNotBother &, const tag::KeepAsFirstVer & ) override;
	void add_to_mesh ( Mesh::Core * msh, const tag::Force & );  // 'msh' must be Mesh::STSI
	void add_to_mesh ( Mesh::Core * msh,  // 'msh' must be Mesh::STSI
	                   const tag::NeighbRels &, const std::map < Cell, Cell > & rels,
	                   bool force_other_faces                                        );
	void remove_from_mesh ( Mesh::Core * msh );
	void remove_from_mesh ( Mesh::Core * msh, const tag::DoNotBother & );
	void add_to_bdry ( Mesh::Core * msh );
	void add_to_bdry ( Mesh::Core * msh, const tag::DoNotBother & );
	void remove_from_bdry ( Mesh::Core * msh );
	void remove_from_bdry ( Mesh::Core * msh, const tag::DoNotBother & );
	// tag::do_not_bother is useful for a Mesh::Connected::OneDim	

	// glue_on_my_bdry and cut_from_my_bdry  defined by Cell:Negative
	// three iterators over meshes  virtual from Cell::Core, execution forbiden
	// compute_sign  defined by Cell::Negative, execution forbidden

	#ifndef NDEBUG  // DEBUG mode
	// std::string get_name ( )  defined by Cell::Negative
	void print_everything ( );  // virtual from Cell::Core
	#endif
	
}; // end of  class Cell::NegativeSegment, aka Cell::Negative::Segment

//------------------------------------------------------------------------------------------------------//


// I would very much prefer the name
// 'class Cell::Positive::HighDim' instead of 'Cell::PositiveHighDim'
// but it was not possible ...

class Cell::PositiveHighDim : public Cell::Positive::NotVertex

// a cell of dimension >= 2

{	public :

	// size_t nb_of_wrappers  inherited from tag::Util::Core ifdef MANIFEM_COLLECT_CM

	// attributes inherited from Cell::Core :
	// double_heap, size_t_heap, short_int_heap, hook, cell_behind_within

	// Cell reverse_attr  inherited from Cell::Core

	// inherited from Cell::Positive :
	// std::vector < std::map < Mesh::Core*, Cell::field_to_meshes > > meshes

	// inherited from Cell::Positive::NotVertex :
	// std::map < Mesh::Core*, Cell::field_to_meshes_same_dim > meshes_same_dim

	// we use Mesh wrapper as pointer because a Cell should keep its boundary alive
	Mesh boundary_attr;  // positive mesh

	inline PositiveHighDim ( const tag::OfDimension &, const size_t d,
	                         const tag::WhoseBoundaryIs &, const Mesh & msh,
	                         const tag::OneDummyWrapper &                   );
	inline PositiveHighDim ( const tag::WhoseBoundaryIs &, const Mesh & msh,
	                         const tag::OneDummyWrapper &                   );
	inline PositiveHighDim ( const tag::Triangle &, const Cell & AB, const Cell & BC,
	                         const Cell & CA, const tag::OneDummyWrapper &           );
	inline PositiveHighDim ( const tag::Quadrangle &, const Cell & AB, const Cell & BC,
	                         const Cell & CD, const Cell & DA, const tag::OneDummyWrapper & );
	inline PositiveHighDim ( const tag::Pentagon &, const Cell & AB, const Cell & BC,
	                         const Cell & CD, const Cell & DE, const Cell & EA,
                           const tag::OneDummyWrapper &                             );
	inline PositiveHighDim ( const tag::Hexagon &, const Cell & AB, const Cell & BC,
	                         const Cell & CD, const Cell & DE, const Cell & EF, const Cell & FA,
                           const tag::OneDummyWrapper &                                        );
	inline PositiveHighDim ( const tag::Tetrahedron &, const Cell & face_1, const Cell & face_2,
	                         const Cell & face_3, const Cell & face_4, const tag::OneDummyWrapper & );
	inline PositiveHighDim ( const tag::Hexahedron &, const Cell & face_1, const Cell & face_2,
	                         const Cell & face_3, const Cell & face_4,
	                         const Cell & face_5, const Cell & face_6, const tag::OneDummyWrapper & );

	PositiveHighDim ( const Cell::Positive::HighDim & ) = delete;
	PositiveHighDim ( const Cell::Positive::HighDim && ) = delete;
	Cell::Positive::HighDim & operator= ( const Cell::Positive::HighDim & ) = delete;
	Cell::Positive::HighDim & operator= ( const Cell::Positive::HighDim && ) = delete;

	virtual ~PositiveHighDim ( ) { }
	
	// bool dispose_query ( )  defined by tag::Util::Core::DelegateDispose ifdef MANIFEM_COLLECT_CM
	
	// is_positive  and  get_positive  defined by Cell::Positive
	size_t get_dim ( ) const; // virtual

	// tip  and  base  defined by Cell::Core, execution forbidden

	Mesh boundary ( );  // virtual from Cell::Core

	Cell::Negative * build_reverse ( const tag::OneDummyWrapper & );
	// virtual from Cell::Positive

	// belongs_to ( with tag::cell_has_low_dim )  defined by Cell::Positive
	// belongs_to ( with tag::same_dim, tag::orientation_compatible_with_mesh )
  //     defined by Cell::Positive::NotVertex
	// belongs_to ( with tag::same_dim, tag::not_oriented )  defined by Cell::Core

	// glue_on_bdry_of  and  cut_from_bdry_of  defined by Cell::Core

	// methods 'add_to' and 'remove_from' add/remove 'this' cell to/from the mesh 'msh'
	// see paragraph 12.9 in the manual

	// add_to_seg and remove_from seg virtual from Cell::Core,
	// defined by Cell::Positive::NotVertex, execution forbidden

	// the thirteen methods below are virtual from Cell::Core
	void add_to_mesh ( Mesh::Core * msh );
	void add_to_mesh ( Mesh::Core * msh, const tag::DoNotBother & );
	void add_to_mesh ( Mesh::Core * msh, const tag::Force & );  // 'msh' must be Mesh::STSI
	void add_to_mesh ( Mesh::Core * msh,  // 'msh' must be Mesh::STSI
	                   const tag::NeighbRels &, const std::map < Cell, Cell > & rels,
	                   bool force_other_faces                                        );
	void remove_from_mesh ( Mesh::Core * msh );
	void remove_from_mesh ( Mesh::Core * msh, const tag::DoNotBother & );
	void add_to_bdry ( Mesh::Core * msh );
	void add_to_bdry ( Mesh::Core * msh, const tag::DoNotBother & );
	void remove_from_bdry ( Mesh::Core * msh );
	void remove_from_bdry ( Mesh::Core * msh, const tag::DoNotBother & );
	void glue_on_my_bdry ( Cell::Core * );
	void glue_on_my_bdry ( Cell::Core *, const tag::DoNotBother & );
	void cut_from_my_bdry ( Cell::Core * );
	void cut_from_my_bdry ( Cell::Core *, const tag::DoNotBother & );
	// tag::do_not_bother is useful for a Mesh::Connected::OneDim	
	// we feel that 'glue_on_bdry_of' and 'cut_from_bdry_of' are more readable
	// so we suggest to use those (see class Cell::Core)

	// two iterators over meshes of same dim  virtual from Cell::Core, defined by Cell::Positive::NotVertex
	// an iterator over meshes above  virtual from Cell::Core, defined by Cell::Positive
	// compute_sign  defined by Cell::Positive::NotVertex
	
	#ifndef NDEBUG  // DEBUG mode
	// std::string get_name ( )  defined by Cell::Positive
	void print_everything ( );  // virtual from Cell::Core
	#endif

}; // end of  class Cell::PositiveHighDim, aka Cell::Positive::HighDim

//------------------------------------------------------------------------------------------------------//


// I would very much prefer the name
// 'class Cell::Negative::HighDim' instead of 'Cell::NegativeHighDim'
// but it was not possible ...

class Cell::NegativeHighDim : public Cell::Negative::NotVertex

// a negative cell of dimension >= 2

{	public :

	// size_t nb_of_wrappers  inherited from tag::Util::Core ifdef MANIFEM_COLLECT_CM

	// attributes inherited from Cell::Core :
	// double_heap, size_t_heap, short_int_heap, hook, cell_behind_within

	// Cell reverse_attr  inherited from Cell::Core

	inline NegativeHighDim ( const tag::OfDimension &, const size_t d,
	                         const tag::ReverseOf &, Cell::Positive * direct_cell_p,
	                         const tag::OneDummyWrapper &                            );
	
	inline NegativeHighDim
	( const tag::ReverseOf &, Cell::Positive * direct_cell_p, const tag::OneDummyWrapper & );

	NegativeHighDim ( const Cell::Negative::HighDim & ) = delete;
	NegativeHighDim ( const Cell::Negative::HighDim && ) = delete;
	Cell::Negative::HighDim & operator= ( const Cell::Negative::HighDim & ) = delete;
	Cell::Negative::HighDim & operator= ( const Cell::Negative::HighDim && ) = delete;

	virtual ~NegativeHighDim ( ) { }
	
	// bool dispose_query ( )  defined by tag::Util::Core::DelegateDispose ifdef MANIFEM_COLLECT_CM
	
	// is_positive  and  get_positive  defined by Cell::Negative
	size_t get_dim ( ) const; // virtual from Cell::Core

	// tip  and  base  defined by Cell::Core, execution forbidden
	
	Mesh boundary ( );  // virtual from Cell::Core

	// belongs_to ( with tag::cell_has_low_dim )  defined by Cell::Negative
	// belongs_to ( with tag::same_dim and tag::orientation_compatible_with_mesh )
  //     defined by Cell::Negative::NotVertex
	// belongs_to ( with tag::same_dim, tag::not_oriented )  defined by Cell::Core

	// glue_on_bdry_of  and  cut_from_bdry_of  defined by Cell::Core

	// methods 'add_to' and 'remove_from' add/remove 'this' cell to/from the mesh 'msh'
	// see paragraph 12.9 in the manual

	// add_to_seg and remove_from seg virtual from Cell::Core,
	// defined by Cell::Negative::NotVertex, execution forbidden

	// the nine methods below are virtual from Cell::Core
	void add_to_mesh ( Mesh::Core * msh );
	void add_to_mesh ( Mesh::Core * msh, const tag::DoNotBother & );
	void add_to_mesh ( Mesh::Core * msh, const tag::Force & );  // 'msh' must be Mesh::STSI
	void add_to_mesh ( Mesh::Core * msh,  // 'msh' must be Mesh::STSI
	                   const tag::NeighbRels &, const std::map < Cell, Cell > & rels,
	                   bool force_other_faces                                        );
	void remove_from_mesh ( Mesh::Core * msh );
	void remove_from_mesh ( Mesh::Core * msh, const tag::DoNotBother & );
	void add_to_bdry ( Mesh::Core * msh );
	void add_to_bdry ( Mesh::Core * msh, const tag::DoNotBother & );
	void remove_from_bdry ( Mesh::Core * msh );
	void remove_from_bdry ( Mesh::Core * msh, const tag::DoNotBother & );
	// tag::do_not_bother is useful for a Mesh::Connected::OneDim	

	// glue_on_my_bdry  and  cut_from_my_bdry  defined by Cell:Negative
	// three iterators over meshes  virtual from Cell::Core, execution forbiden
	// compute_sign  defined by Cell::Negative, execution forbidden

	#ifndef NDEBUG  // DEBUG mode
	// std::string get_name ( )  defined by Cell::Negative
	void print_everything ( );  // virtual from Cell::Core
	#endif

}; // end of  class Cell::NegativeHighDim, aka Cell::Negative::HighDim


//------------------------------------------------------------------------------------------------------//
//--------------------------       core Meshes     -----------------------------------------------------//
//------------------------------------------------------------------------------------------------------//


// I would very much prefer the name 'class Mesh::Core' instead of 'tag::Util::MeshCore'
// but it was not possible ...

#ifdef MANIFEM_COLLECT_CM	

class tag::Util::MeshCore : public tag::Util::Core

#else  // no MANIFEM_COLLECT_CM

class tag::Util::MeshCore

#endif  // MANIFEM_COLLECT_CM	

// represents a positive mesh
// negative meshes have no core (wrappers for negative meshes are built on-the-fly)

// abstract class, specialized in Mesh::ZeroDim, Mesh::Connected::{OneDim,HighDim},
// Mesh::MultiplyConnected::{OneDim,HighDim}, Mesh::Fuzzy, Mesh::STSI

{	public :

	// size_t nb_of_wrappers  inherited from tag::Util::Core ifdef MANIFEM_COLLECT_CM

	// we use an ordinary pointer, not a wrapper
	// because we do not want a cell to be kept alive by its boundary
	Cell::Positive * cell_enclosed { nullptr };
	// when 'this' is the boundary of a cell

	inline MeshCore ( const tag::OfDimension &, const size_t d, const tag::OneDummyWrapper & )
	#ifdef MANIFEM_COLLECT_CM	
	:	tag::Util::Core ( tag::one_dummy_wrapper )
	#endif  // MANIFEM_COLLECT_CM	
	{	}

	inline MeshCore ( const tag::OfDimension &, const size_t d , const tag::MinusOne &,
                    const tag::OneDummyWrapper &                                      )
	#ifdef MANIFEM_COLLECT_CM	
	:	tag::Util::Core ( tag::one_dummy_wrapper )
	#endif  // MANIFEM_COLLECT_CM	
	{	}

	inline MeshCore ( const tag::OfDimension &, const size_t d, const tag::MinusOne &,
	                  const tag::BoundaryOf &, const tag::PositiveCell &,
	                  Cell::Positive * cll, const tag::OneDummyWrapper &               )
	#ifdef MANIFEM_COLLECT_CM	
	:	tag::Util::Core ( tag::one_dummy_wrapper ),
		cell_enclosed { cll }
	#else  // no MANIFEM_COLLECT_CM
	:	cell_enclosed { cll }
	#endif  // MANIFEM_COLLECT_CM
	{ }

	virtual ~MeshCore ( ) { }
	
	// bool dispose_query ( )  defined by tag::Util::Core ifdef MANIFEM_COLLECT_CM
	
	virtual size_t get_dim_plus_one ( ) const = 0;

	virtual size_t number_of ( const tag::Vertices & ) const = 0;
	virtual size_t number_of ( const tag::Segments & ) const = 0;
	virtual size_t number_of ( const tag::CellsOfDim &, const size_t d ) const = 0;
	virtual size_t number_of ( const tag::CellsOfMaxDim & ) const = 0;

	// the four methods below are only relevant for connected one-dimensional meshes
	// so we forbid execution for now and then override them in Mesh::Connected::OneDim
	virtual Cell first_vertex ( );
	virtual Cell last_vertex ( );
	virtual Cell first_segment ( );
	virtual Cell last_segment ( );

	// the four methods below are only relevant for STSI meshes
	// so we forbid execution here and then override them in Mesh::STSI
	virtual Cell cell_in_front_of ( const Cell & face, const tag::SeenFrom &, const Cell & neighbour,
	                                const tag::SurelySingular &, const tag::SurelyExists &           );
	virtual Cell cell_in_front_of ( const Cell & face, const tag::SeenFrom &, const Cell & neighbour,
	                                const tag::SurelySingular &, const tag::MayNotExist &            );
	virtual Cell cell_behind ( const Cell & face, const tag::SeenFrom &, const Cell & neighbour,
	                           const tag::SurelySingular &, const tag::SurelyExists &           );
	virtual Cell cell_behind ( const Cell & face, const tag::SeenFrom &, const Cell & neighbour,
	                           const tag::SurelySingular &, const tag::MayNotExist &            );

	virtual void add_pos_seg ( Cell::Positive::Segment *, const tag::MeshIsNotBdry & ) = 0;
	virtual void add_pos_seg
	( Cell::Positive::Segment *, const tag::MeshIsNotBdry &, const tag::DoNotBother & ) = 0;
	virtual void add_pos_seg  // here calls method above, overridden by Mesh::Connected::OneDim
	( Cell::Positive::Segment *, const tag::MeshIsNotBdry &,
	  const tag::DoNotBother &, const tag::KeepAsFirstVer & );
	virtual void add_pos_seg  // here execution forbidden, overridden by Mesh::STSI
	( Cell::Positive::Segment *, const tag::MeshIsNotBdry &, const tag::Force & );
	virtual void add_pos_seg ( Cell::Positive::Segment *, const tag::MeshIsBdry & ) = 0;
	virtual void add_pos_seg
	( Cell::Positive::Segment *, const tag::MeshIsBdry &, const tag::DoNotBother & ) = 0;
	virtual void remove_pos_seg ( Cell::Positive::Segment *, const tag::MeshIsNotBdry & ) = 0;
	virtual void remove_pos_seg
	( Cell::Positive::Segment *, const tag::MeshIsNotBdry &, const tag::DoNotBother & ) = 0;
	virtual void remove_pos_seg ( Cell::Positive::Segment *, const tag::MeshIsBdry & ) = 0;
	virtual void remove_pos_seg
	( Cell::Positive::Segment *, const tag::MeshIsBdry &, const tag::DoNotBother & ) = 0;
	virtual void add_neg_seg ( Cell::Negative::Segment *, const tag::MeshIsNotBdry & ) = 0;
	virtual void add_neg_seg
	( Cell::Negative::Segment *, const tag::MeshIsNotBdry &, const tag::DoNotBother & ) = 0;
	virtual void add_neg_seg  // here calls method above, overridden by Mesh::Connected::OneDim
	( Cell::Negative::Segment *, const tag::MeshIsNotBdry &,
	  const tag::DoNotBother &, const tag::KeepAsFirstVer & );
	virtual void add_neg_seg  // here execution forbidden, overridden by Mesh::STSI
	( Cell::Negative::Segment *, const tag::MeshIsNotBdry &, const tag::Force & );
	virtual void add_neg_seg ( Cell::Negative::Segment *, const tag::MeshIsBdry & ) = 0;
	virtual void add_neg_seg
	( Cell::Negative::Segment *, const tag::MeshIsBdry &, const tag::DoNotBother & ) = 0;
	virtual void remove_neg_seg ( Cell::Negative::Segment *, const tag::MeshIsNotBdry & ) = 0;
	virtual void remove_neg_seg
	( Cell::Negative::Segment *, const tag::MeshIsNotBdry &, const tag::DoNotBother & ) = 0;
	virtual void remove_neg_seg ( Cell::Negative::Segment *, const tag::MeshIsBdry & ) = 0;
	virtual void remove_neg_seg
	( Cell::Negative::Segment *, const tag::MeshIsBdry &, const tag::DoNotBother & ) = 0;
	virtual void add_pos_hd_cell ( Cell::Positive::HighDim *, const tag::MeshIsNotBdry & ) = 0;
	virtual void add_pos_hd_cell
	( Cell::Positive::HighDim *, const tag::MeshIsNotBdry &, const tag::DoNotBother & ) = 0;
	virtual void add_pos_hd_cell  // here execution forbidden, overridden by Mesh::STSI
	( Cell::Positive::HighDim *, const tag::MeshIsNotBdry &, const tag::Force & );
	virtual void add_pos_hd_cell  // here execution forbidden, overridden by Mesh::STSI
	( Cell::Positive::HighDim *, const tag::MeshIsNotBdry &,
	  const tag::NeighbRels &, const std::map < Cell, Cell > & rels, bool force_other_faces );
	virtual void add_pos_hd_cell ( Cell::Positive::HighDim *, const tag::MeshIsBdry & ) = 0;
	virtual void add_pos_hd_cell
	( Cell::Positive::HighDim *, const tag::MeshIsBdry &, const tag::DoNotBother & ) = 0;
	virtual void remove_pos_hd_cell ( Cell::Positive::HighDim *, const tag::MeshIsNotBdry & ) = 0;
	virtual void remove_pos_hd_cell
	( Cell::Positive::HighDim *, const tag::MeshIsNotBdry &, const tag::DoNotBother & ) = 0;
	virtual void remove_pos_hd_cell ( Cell::Positive::HighDim *, const tag::MeshIsBdry & ) = 0;
	virtual void remove_pos_hd_cell
	( Cell::Positive::HighDim *, const tag::MeshIsBdry &, const tag::DoNotBother & ) = 0;
	virtual void add_neg_hd_cell ( Cell::Negative::HighDim *, const tag::MeshIsNotBdry & ) = 0;
	virtual void add_neg_hd_cell
	( Cell::Negative::HighDim *, const tag::MeshIsNotBdry &, const tag::DoNotBother & ) = 0;
	virtual void add_neg_hd_cell  // here execution forbidden, overridden by Mesh::STSI
	( Cell::Negative::HighDim *, const tag::MeshIsNotBdry &, const tag::Force & );
	virtual void add_neg_hd_cell  // here execution forbidden, overridden by Mesh::STSI
	( Cell::Negative::HighDim *, const tag::MeshIsNotBdry &,
	  const tag::NeighbRels &, const std::map < Cell, Cell > & rels, bool force_other_faces );
	virtual void add_neg_hd_cell ( Cell::Negative::HighDim *, const tag::MeshIsBdry & ) = 0;
	virtual void add_neg_hd_cell
	( Cell::Negative::HighDim *, const tag::MeshIsBdry &, const tag::DoNotBother & ) = 0;
	virtual void remove_neg_hd_cell ( Cell::Negative::HighDim *, const tag::MeshIsNotBdry & ) = 0;
	virtual void remove_neg_hd_cell
	( Cell::Negative::HighDim *, const tag::MeshIsNotBdry &, const tag::DoNotBother & ) = 0;
	virtual void remove_neg_hd_cell ( Cell::Negative::HighDim *, const tag::MeshIsBdry & ) = 0;
	virtual void remove_neg_hd_cell
	( Cell::Negative::HighDim *, const tag::MeshIsBdry &, const tag::DoNotBother & ) = 0;

	virtual Mesh::Core * clone
	( const tag::Depth &, size_t dep, const tag::OneDummyWrapper & ) = 0;
	virtual Mesh::Core * clone
	( const tag::Depth &, const tag::Infinity &, const tag::OneDummyWrapper & ) = 0;
	virtual Mesh::Core * clone
	( const tag::MakeFuzzy &, const tag::Depth &, size_t dep, const tag::OneDummyWrapper & ) = 0;
	virtual Mesh::Core * clone
	( const tag::MakeFuzzy &, const tag::Depth &, const tag::Infinity &,
	  const tag::OneDummyWrapper &                                      ) = 0;
	virtual Mesh::Core * clone
	( const tag::MakeConnectedOneDim &, const tag::Depth &, size_t dep,
	  const tag::OneDummyWrapper &                                     ) = 0;
	virtual Mesh::Core * clone
	( const tag::MakeConnectedOneDim &, const tag::Depth &, const tag::Infinity &,
	  const tag::OneDummyWrapper &                                                ) = 0;
	virtual Mesh::Core * clone ( const tag::ReverseEachCell &, const tag::OneDummyWrapper & ) = 0;
	
	virtual std::list<Cell>::iterator add_to_my_cells ( Cell::Core * const, const size_t ) = 0;
	virtual std::list<Cell>::iterator add_to_my_cells
		( Cell::Core * const, const size_t, const tag::DoNotBother & ) = 0;

	virtual void remove_from_my_cells
		( Cell::Core * const, const size_t, std::list<Cell>::iterator ) = 0;
	virtual void remove_from_my_cells
		( Cell::Core * const, const size_t, std::list<Cell>::iterator, const tag::DoNotBother & ) = 0;
		
	virtual void closed_loop ( const Cell & ver ) = 0;
	virtual void closed_loop ( const Cell & ver, size_t ) = 0;
	// for connected one-dim meshes, set both first_ver and last_ver to 'ver'
	// (and number of segments)
		
	// iterators defined in iterator.cpp
	// we are still in class Mesh::Core

	virtual Mesh::Iterator::Core * iterator
	( const tag::OverVertices &, const tag::AsFound &, const tag::ThisMeshIsPositive & ) = 0;
	virtual Mesh::Iterator::Core * iterator
	( const tag::OverVertices &, const tag::ForcePositive &, const tag::ThisMeshIsPositive & ) = 0;
	virtual Mesh::Iterator::Core * iterator
	( const tag::OverVertices &, const tag::ThisMeshIsPositive & ) = 0;
	virtual Mesh::Iterator::Core * iterator
	( const tag::OverVertices &, const tag::OrientCompWithMesh &, const tag::ThisMeshIsPositive & ) = 0;
	virtual Mesh::Iterator::Core * iterator
	( const tag::OverVertices &, const tag::OrientOpposToMesh &, const tag::ThisMeshIsPositive & ) = 0;
	virtual Mesh::Iterator::Core * iterator
	( const tag::OverVertices &, const tag::AsFound &,
	  const tag::RequireOrder &, const tag::ThisMeshIsPositive & ) = 0;
	virtual Mesh::Iterator::Core * iterator
	( const tag::OverVertices &, const tag::ForcePositive &,
	  const tag::RequireOrder &, const tag::ThisMeshIsPositive & ) = 0;
	virtual Mesh::Iterator::Core * iterator
	( const tag::OverVertices &, const tag::RequireOrder &, const tag::ThisMeshIsPositive & ) = 0;
	virtual Mesh::Iterator::Core * iterator
	( const tag::OverVertices &, const tag::OrientCompWithMesh &,
	  const tag::RequireOrder &, const tag::ThisMeshIsPositive & ) = 0;
	virtual Mesh::Iterator::Core * iterator
	( const tag::OverVertices &, const tag::OrientOpposToMesh &,
	  const tag::RequireOrder &, const tag::ThisMeshIsPositive & ) = 0;
	virtual Mesh::Iterator::Core * iterator
	( const tag::OverVertices &, const tag::AsFound &,
	  const tag::Backwards &, const tag::ThisMeshIsPositive & ) = 0;
	virtual Mesh::Iterator::Core * iterator
	( const tag::OverVertices &, const tag::ForcePositive &,
	  const tag::Backwards &, const tag::ThisMeshIsPositive & ) = 0;
	virtual Mesh::Iterator::Core * iterator
	( const tag::OverVertices &, const tag::Backwards &, const tag::ThisMeshIsPositive & ) = 0;
	virtual Mesh::Iterator::Core * iterator
	( const tag::OverVertices &, const tag::OrientCompWithMesh &,
	  const tag::Backwards &, const tag::ThisMeshIsPositive & ) = 0;
	virtual Mesh::Iterator::Core * iterator
	( const tag::OverVertices &, const tag::OrientOpposToMesh &,
	  const tag::Backwards &, const tag::ThisMeshIsPositive & ) = 0;

	// we are still in class Mesh::Core

	virtual Mesh::Iterator::Core * iterator
	( const tag::OverSegments &, const tag::AsFound &, const tag::ThisMeshIsPositive & ) = 0;
	virtual Mesh::Iterator::Core * iterator
	( const tag::OverSegments &, const tag::ForcePositive &, const tag::ThisMeshIsPositive & ) = 0;
	virtual Mesh::Iterator::Core * iterator
	( const tag::OverSegments &, const tag::ThisMeshIsPositive & ) = 0;
	virtual Mesh::Iterator::Core * iterator
	( const tag::OverSegments &, const tag::OrientCompWithMesh &, const tag::ThisMeshIsPositive & ) = 0;
	virtual Mesh::Iterator::Core * iterator
	( const tag::OverSegments &, const tag::OrientOpposToMesh &, const tag::ThisMeshIsPositive & ) = 0;
	virtual Mesh::Iterator::Core * iterator
	( const tag::OverSegments &, const tag::AsFound &,
	  const tag::RequireOrder &, const tag::ThisMeshIsPositive & ) = 0;
	virtual Mesh::Iterator::Core * iterator
	( const tag::OverSegments &, const tag::ForcePositive &,
	  const tag::RequireOrder &, const tag::ThisMeshIsPositive & ) = 0;
	virtual Mesh::Iterator::Core * iterator
	( const tag::OverSegments &, const tag::RequireOrder &, const tag::ThisMeshIsPositive & ) = 0;
	virtual Mesh::Iterator::Core * iterator
	( const tag::OverSegments &, const tag::OrientCompWithMesh &,
	  const tag::RequireOrder &, const tag::ThisMeshIsPositive & ) = 0;
	virtual Mesh::Iterator::Core * iterator
	( const tag::OverSegments &, const tag::OrientOpposToMesh &,
	  const tag::RequireOrder &, const tag::ThisMeshIsPositive & ) = 0;
	virtual Mesh::Iterator::Core * iterator
	( const tag::OverSegments &, const tag::AsFound &,
	  const tag::Backwards &, const tag::ThisMeshIsPositive & ) = 0;
	virtual Mesh::Iterator::Core * iterator
	( const tag::OverSegments &, const tag::ForcePositive &,
	  const tag::Backwards &, const tag::ThisMeshIsPositive & ) = 0;
	virtual Mesh::Iterator::Core * iterator
	( const tag::OverSegments &, const tag::Backwards &, const tag::ThisMeshIsPositive & ) = 0;
	virtual Mesh::Iterator::Core * iterator
	( const tag::OverSegments &, const tag::OrientCompWithMesh &,
	  const tag::Backwards &, const tag::ThisMeshIsPositive & ) = 0;
	virtual Mesh::Iterator::Core * iterator
	( const tag::OverSegments &, const tag::OrientOpposToMesh &,
	  const tag::Backwards &, const tag::ThisMeshIsPositive & ) = 0;

	// we are still in class Mesh::Core

	virtual Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfDim &, const size_t,
	  const tag::AsFound &, const tag::ThisMeshIsPositive & ) = 0;
	virtual Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfDim &, const size_t,
	  const tag::CellHasLowDim &, const tag::ThisMeshIsPositive & ) = 0;
	virtual Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfDim &, const size_t,
	  const tag::ForcePositive &, const tag::ThisMeshIsPositive & ) = 0;
	virtual Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfDim &, const size_t, const tag::ThisMeshIsPositive & ) = 0;
	virtual Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfDim &, const size_t,
	  const tag::OrientCompWithMesh &, const tag::ThisMeshIsPositive & ) = 0;
	virtual Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfDim &, const size_t,
	  const tag::OrientOpposToMesh &, const tag::ThisMeshIsPositive & ) = 0;
	virtual Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfDim &, const size_t, const tag::AsFound &,
	  const tag::RequireOrder &, const tag::ThisMeshIsPositive &        ) = 0;
	virtual Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfDim &, const size_t, const tag::ForcePositive &,
	  const tag::RequireOrder &, const tag::ThisMeshIsPositive &            ) = 0;
	virtual Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfDim &, const size_t,
	  const tag::RequireOrder &, const tag::ThisMeshIsPositive & ) = 0;
	virtual Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfDim &, const size_t, const tag::OrientCompWithMesh &,
	  const tag::RequireOrder &, const tag::ThisMeshIsPositive &                 ) = 0;
	virtual Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfDim &, const size_t, const tag::OrientOpposToMesh &,
	  const tag::RequireOrder &, const tag::ThisMeshIsPositive &                ) = 0;
	virtual Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfDim &, const size_t, const tag::AsFound &,
	  const tag::Backwards &, const tag::ThisMeshIsPositive &        ) = 0;
	virtual Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfDim &, const size_t, const tag::ForcePositive &,
	  const tag::Backwards &, const tag::ThisMeshIsPositive &            ) = 0;
	virtual Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfDim &, const size_t,
	  const tag::Backwards &, const tag::ThisMeshIsPositive & ) = 0;
	virtual Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfDim &, const size_t, const tag::OrientCompWithMesh &,
	  const tag::Backwards &, const tag::ThisMeshIsPositive &                 ) = 0;
	virtual Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfDim &, const size_t, const tag::OrientOpposToMesh &,
	  const tag::Backwards &, const tag::ThisMeshIsPositive &              ) = 0;

	// we are still in class Mesh::Core

	virtual Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfMaxDim &, const tag::AsFound &, const tag::ThisMeshIsPositive & ) = 0;
	virtual Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfMaxDim &, const tag::ForcePositive &, const tag::ThisMeshIsPositive & ) = 0;
	virtual Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfMaxDim &, const tag::OrientCompWithMesh &,
	  const tag::ThisMeshIsPositive &                                 ) = 0;
	virtual Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfMaxDim &, const tag::OrientOpposToMesh &,
	  const tag::ThisMeshIsPositive &                                ) = 0;

	virtual Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfMaxDim &, const tag::AsFound &,
	  const tag::RequireOrder &, const tag::ThisMeshIsPositive & ) = 0;
	virtual Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfMaxDim &, const tag::ForcePositive &,
	  const tag::RequireOrder &, const tag::ThisMeshIsPositive & ) = 0;
	virtual Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfMaxDim &, const tag::OrientCompWithMesh &,
	  const tag::RequireOrder &, const tag::ThisMeshIsPositive &      ) = 0;
	virtual Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfMaxDim &, const tag::OrientOpposToMesh &,
	  const tag::RequireOrder &, const tag::ThisMeshIsPositive &     ) = 0;

	virtual Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfMaxDim &, const tag::AsFound &,
	  const tag::Backwards &, const tag::ThisMeshIsPositive & ) = 0;
	virtual Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfMaxDim &, const tag::ForcePositive &,
	  const tag::Backwards &, const tag::ThisMeshIsPositive & ) = 0;
	virtual Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfMaxDim &, const tag::OrientCompWithMesh &,
	  const tag::Backwards &, const tag::ThisMeshIsPositive &      ) = 0;
	virtual Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfMaxDim &, const tag::OrientOpposToMesh &,
	  const tag::Backwards &, const tag::ThisMeshIsPositive &     ) = 0;

	// we are still in class Mesh::Core

	virtual Mesh::Iterator::Core * iterator
	( const tag::OverVertices &, const tag::ConnectedTo &, Cell::Positive::Vertex *,
	  const tag::ThisMeshIsPositive &                                               ) = 0;
	virtual Mesh::Iterator::Core * iterator
	( const tag::OverVertices &, const tag::ConnectedTo &, Cell::Positive::Vertex *,
	  const tag::RequireOrder &, const tag::ThisMeshIsPositive &                    ) = 0;

	virtual Mesh::Iterator::Core * iterator
	( const tag::OverSegments &, const tag::WhoseTipIs &, Cell::Positive::Vertex * V,
	  const tag::ThisMeshIsPositive &                                                ) = 0;
	virtual Mesh::Iterator::Core * iterator
	( const tag::OverSegments &, const tag::WhoseTipIs &, Cell::Positive::Vertex * V,
	  const tag::RequireOrder &, const tag::ThisMeshIsPositive &                     ) = 0;
	virtual Mesh::Iterator::Core * iterator
	( const tag::OverSegments &, const tag::WhoseTipIs &, Cell::Positive::Vertex * V,
	  const tag::Backwards &, const tag::ThisMeshIsPositive &                        ) = 0;

	virtual Mesh::Iterator::Core * iterator
	( const tag::OverSegments &, const tag::WhoseBaseIs &, Cell::Positive::Vertex * V,
	  const tag::ThisMeshIsPositive &                                                 ) = 0;
	virtual Mesh::Iterator::Core * iterator
	( const tag::OverSegments &, const tag::WhoseBaseIs &, Cell::Positive::Vertex * V,
	  const tag::RequireOrder &, const tag::ThisMeshIsPositive &                      ) = 0;
	virtual Mesh::Iterator::Core * iterator
	( const tag::OverSegments &, const tag::WhoseBaseIs &, Cell::Positive::Vertex * V,
	  const tag::Backwards &, const tag::ThisMeshIsPositive &                         ) = 0;
	
	virtual Mesh::Iterator::Core * iterator
	( const tag::OverSegments &, const tag::AsFound &, const tag::Around &, Cell::Positive::Vertex *,
	  const tag::ThisMeshIsPositive &                                                                ) = 0;
	virtual Mesh::Iterator::Core * iterator
	( const tag::OverSegments &, const tag::AsFound &, const tag::Around &, Cell::Positive::Vertex *,
	  const tag::RequireOrder &, const tag::ThisMeshIsPositive &                                     ) = 0;
	virtual Mesh::Iterator::Core * iterator
	( const tag::OverSegments &, const tag::AsFound &, const tag::Around &, Cell::Positive::Vertex *,
	  const tag::Backwards &, const tag::ThisMeshIsPositive &                                     ) = 0;
	virtual Mesh::Iterator::Core * iterator
	( const tag::OverSegments &, const tag::ForcePositive &,
	  const tag::Around &, Cell::Positive::Vertex *, const tag::ThisMeshIsPositive & ) = 0;
	virtual Mesh::Iterator::Core * iterator
	( const tag::OverSegments &, const tag::ForcePositive &,
	  const tag::Around &, Cell::Positive::Vertex *,
	  const tag::RequireOrder &, const tag::ThisMeshIsPositive & ) = 0;
	virtual Mesh::Iterator::Core * iterator
	( const tag::OverSegments &, const tag::ForcePositive &,
	  const tag::Around &, Cell::Positive::Vertex *,
	  const tag::Backwards &, const tag::ThisMeshIsPositive & ) = 0;

	// we are still in class Mesh::Core

	virtual Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfDim &, const size_t, const tag::AsFound &,
	  const tag::Around &, Cell::Core *, const tag::ThisMeshIsPositive & ) = 0;
	virtual Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfDim &, const size_t, const tag::AsFound &,
	  const tag::Around &, Cell::Core *, const tag::RequireOrder &, const tag::ThisMeshIsPositive & ) = 0;
	virtual Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfDim &, const size_t, const tag::AsFound &,
	  const tag::Around &, Cell::Core *, const tag::Backwards &, const tag::ThisMeshIsPositive & ) = 0;
	virtual Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfDim &, const size_t, const tag::ForcePositive &,
	  const tag::Around &, Cell::Core *, const tag::ThisMeshIsPositive &    ) = 0;
	virtual Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfDim &, const size_t, const tag::ForcePositive &,
	  const tag::Around &, Cell::Core *, const tag::RequireOrder &, const tag::ThisMeshIsPositive & ) = 0;
	virtual Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfDim &, const size_t, const tag::ForcePositive &,
	  const tag::Around &, Cell::Core *, const tag::Backwards &, const tag::ThisMeshIsPositive & ) = 0;
	virtual Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfDim &, const size_t, const tag::OrientCompWithCenter &,
	  const tag::Around &, Cell::Core *, const tag::ThisMeshIsPositive &           ) = 0;
	virtual Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfDim &, const size_t, const tag::OrientOpposToCenter &,
	  const tag::Around &, Cell::Core *, const tag::ThisMeshIsPositive &          ) = 0;
	/*	virtual Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfDim &, const size_t, const tag::OrientOpposToMesh &,
	  const tag::Around &, Cell::Core *, const tag::ThisMeshIsPositive &      ) = 0;
	virtual Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfDim &, const size_t, const tag::OrientOpposToMesh &,
	  const tag::Around &, Cell::Core *, const tag::RequireOrder &, const tag::ThisMeshIsPositive & ) = 0;
	virtual Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfDim &, const size_t, const tag::OrientOpposToMesh &,
	const tag::Around &, Cell::Core *, const tag::Backwards &, const tag::ThisMeshIsPositive & ) = 0; */

	// we are still in class Mesh::Core

	virtual Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfMaxDim &, const tag::OrientCompWithMesh &,
	  const tag::Around &, Cell::Core *, const tag::ThisMeshIsPositive & ) = 0;
	virtual Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfMaxDim &, const tag::OrientCompWithMesh &,
	  const tag::Around &, Cell::Core *, const tag::RequireOrder &, const tag::ThisMeshIsPositive & ) = 0;
	virtual Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfMaxDim &, const tag::OrientCompWithMesh &,
	  const tag::Around &, Cell::Core *, const tag::Backwards &, const tag::ThisMeshIsPositive & ) = 0;
	virtual Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfMaxDim &, const tag::ForcePositive &,
	  const tag::Around &, Cell::Core *, const tag::ThisMeshIsPositive & ) = 0;
	virtual Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfMaxDim &, const tag::ForcePositive &,
	  const tag::Around &, Cell::Core *, const tag::RequireOrder &, const tag::ThisMeshIsPositive & ) = 0;
	virtual Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfMaxDim &, const tag::ForcePositive &,
	  const tag::Around &, Cell::Core *, const tag::Backwards &, const tag::ThisMeshIsPositive & ) = 0;
	virtual Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfMaxDim &, const tag::OrientOpposToMesh &,
	  const tag::Around &, Cell::Core *, const tag::ThisMeshIsPositive & ) = 0;
	virtual Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfMaxDim &, const tag::OrientOpposToMesh &,
	  const tag::Around &, Cell::Core *, const tag::RequireOrder &, const tag::ThisMeshIsPositive & ) = 0;
	virtual Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfMaxDim &, const tag::OrientOpposToMesh &,
	  const tag::Around &, Cell::Core *, const tag::Backwards &, const tag::ThisMeshIsPositive & ) = 0;
  virtual Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfMaxDim &, const tag::OrientCompWithCenter &,
	  const tag::Around &, Cell::Core *, const tag::ThisMeshIsPositive & ) = 0;
	virtual Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfMaxDim &, const tag::OrientOpposToCenter &,
	  const tag::Around &, Cell::Core *, const tag::ThisMeshIsPositive & ) = 0;
	virtual Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfMaxDim &, const tag::AsFound &,
	  const tag::Around &, Cell::Core *, const tag::ThisMeshIsPositive & ) = 0;

	// we are still in class Mesh::Core

	#ifndef NDEBUG  // DEBUG mode
	std::string name;
	std::string get_name();
	virtual void print_everything () = 0;
	#endif  // DEBUG

}; // end of  class Mesh::Core

//------------------------------------------------------------------------------------------------------//


class Mesh::ZeroDim : public Mesh::Core

// represents two points, one negative one positive

// zero-dimensional meshes only exist as boundary of a segment,
// which can be retrieved through the cell_enclosed attribute
// they are built on-the-fly

{	public :

	// size_t nb_of_wrappers  inherited from tag::Util::Core ifdef MANIFEM_COLLECT_CM

	// Cell::Positive * cell_enclosed inherited from Mesh::Core
	
	inline ZeroDim
	( const tag::BoundaryOf &, const tag::Positive &, const tag::Segment &,
	  Cell::Positive * seg_p, const tag::OneDummyWrapper &                  )
	:	Mesh::Core ( tag::of_dimension, 1, tag::minus_one,
	               tag::boundary_of, tag::positive_cell, seg_p, tag::one_dummy_wrapper )
	{ }

	virtual ~ZeroDim ( )  { }
	
	// bool dispose_query ( )  defined by tag::Util::Core ifdef MANIFEM_COLLECT_CM
	
	size_t get_dim_plus_one ( ) const;  // virtual from Mesh::Core

	size_t number_of ( const tag::Vertices & ) const;  // virtual from Mesh::Core
	size_t number_of ( const tag::Segments & ) const;
	// virtual from Mesh::Core, here execution forbidden
	size_t number_of ( const tag::CellsOfDim &, const size_t d ) const;  // virtual from Mesh::Core
	size_t number_of ( const tag::CellsOfMaxDim & ) const;  // virtual from Mesh::Core

	// first_vertex, last_vertex, first_segment and last_segment
	// are defined by Mesh::Core, execution forbidden

	// private:

	// cell_in_front_of  and  cell_behind  defined by Mesh::Core, execution forbidden

	// the thirty-two methods below are virtual from Mesh::Core, here execution forbidden
	void add_pos_seg ( Cell::Positive::Segment *, const tag::MeshIsNotBdry & );
	void add_pos_seg
	( Cell::Positive::Segment *, const tag::MeshIsNotBdry &, const tag::DoNotBother & );
	void add_pos_seg ( Cell::Positive::Segment *, const tag::MeshIsBdry & );
	void add_pos_seg
	( Cell::Positive::Segment *, const tag::MeshIsBdry &, const tag::DoNotBother & );
	void remove_pos_seg ( Cell::Positive::Segment *, const tag::MeshIsNotBdry & );
	void remove_pos_seg
	( Cell::Positive::Segment *, const tag::MeshIsNotBdry &, const tag::DoNotBother & );
	void remove_pos_seg ( Cell::Positive::Segment *, const tag::MeshIsBdry & );
	void remove_pos_seg
	( Cell::Positive::Segment *, const tag::MeshIsBdry &, const tag::DoNotBother & );
	void add_neg_seg ( Cell::Negative::Segment *, const tag::MeshIsNotBdry & );
	void add_neg_seg
	( Cell::Negative::Segment *, const tag::MeshIsNotBdry &, const tag::DoNotBother & );
	void add_neg_seg ( Cell::Negative::Segment *, const tag::MeshIsBdry & );
	void add_neg_seg
	( Cell::Negative::Segment *, const tag::MeshIsBdry &, const tag::DoNotBother & );
	void remove_neg_seg ( Cell::Negative::Segment *, const tag::MeshIsNotBdry & );
	void remove_neg_seg
	( Cell::Negative::Segment *, const tag::MeshIsNotBdry &, const tag::DoNotBother & );
	void remove_neg_seg ( Cell::Negative::Segment *, const tag::MeshIsBdry & );
	void remove_neg_seg
	( Cell::Negative::Segment *, const tag::MeshIsBdry &, const tag::DoNotBother & );
	void add_pos_hd_cell ( Cell::Positive::HighDim *, const tag::MeshIsNotBdry & );
	void add_pos_hd_cell
	( Cell::Positive::HighDim *, const tag::MeshIsNotBdry &, const tag::DoNotBother & );
	void add_pos_hd_cell ( Cell::Positive::HighDim *, const tag::MeshIsBdry & );
	void add_pos_hd_cell
	( Cell::Positive::HighDim *, const tag::MeshIsBdry &, const tag::DoNotBother & );
	void remove_pos_hd_cell ( Cell::Positive::HighDim *, const tag::MeshIsNotBdry & );
	void remove_pos_hd_cell
	( Cell::Positive::HighDim *, const tag::MeshIsNotBdry &, const tag::DoNotBother & );
	void remove_pos_hd_cell ( Cell::Positive::HighDim *, const tag::MeshIsBdry & );
	void remove_pos_hd_cell
	( Cell::Positive::HighDim *, const tag::MeshIsBdry &, const tag::DoNotBother & );
	void add_neg_hd_cell ( Cell::Negative::HighDim *, const tag::MeshIsNotBdry & );
	void add_neg_hd_cell
	( Cell::Negative::HighDim *, const tag::MeshIsNotBdry &, const tag::DoNotBother & );
	void add_neg_hd_cell ( Cell::Negative::HighDim *, const tag::MeshIsBdry & );
	void add_neg_hd_cell
	( Cell::Negative::HighDim *, const tag::MeshIsBdry &, const tag::DoNotBother & );
	void remove_neg_hd_cell ( Cell::Negative::HighDim *, const tag::MeshIsNotBdry & );
	void remove_neg_hd_cell
	( Cell::Negative::HighDim *, const tag::MeshIsNotBdry &, const tag::DoNotBother & );
	void remove_neg_hd_cell ( Cell::Negative::HighDim *, const tag::MeshIsBdry & );
	void remove_neg_hd_cell
	( Cell::Negative::HighDim *, const tag::MeshIsBdry &, const tag::DoNotBother & );

	// four methods below are virtual from Mesh::Core, defined there, execution forbidden
	// void add_pos_seg ( Cell::Positive::Segment *, const tag::MeshIsNotBdry &, const tag::Force & )
	// void add_neg_seg ( Cell::Negative::Segment *, const tag::MeshIsNotBdry & const tag::Force & )
	// void add_pos_hd_cell ( Cell::Positive::HighDim *, const tag::MeshIsNotBdry &, const tag::Force & )
	// virtual void add_pos_hd_cell ( Cell::Positive::HighDim *, const tag::MeshIsNotBdry &,
	//   const tag::NeighbRels &, const std::map < Cell, Cell > & rels &, bool force_other_faces )
	// void add_neg_hd_cell ( Cell::Negative::HighDim *, const tag::MeshIsNotBdry &, const tag::Force & );
	// virtual void add_neg_hd_cell ( Cell::Positive::HighDim *, const tag::MeshIsNotBdry &,
	//   const tag::NeighbRels &, const std::map < Cell, Cell > & rels &, bool force_other_faces )
	
	// seven methods 'clone' virtual from Mesh::Core, here execution forbidden
	Mesh::Core * clone ( const tag::Depth &, size_t dep, const tag::OneDummyWrapper & );
	Mesh::Core * clone ( const tag::Depth &, const tag::Infinity &, const tag::OneDummyWrapper & );
	Mesh::Core * clone
	( const tag::MakeFuzzy &, const tag::Depth &, size_t dep, const tag::OneDummyWrapper & );
	Mesh::Core * clone
	( const tag::MakeFuzzy &, const tag::Depth &, const tag::Infinity &, const tag::OneDummyWrapper & );
	Mesh::Core * clone
	( const tag::MakeConnectedOneDim &, const tag::Depth &, size_t dep, const tag::OneDummyWrapper & );
	Mesh::Core * clone
	( const tag::MakeConnectedOneDim &, const tag::Depth &, const tag::Infinity &,
	  const tag::OneDummyWrapper &                                                );
	Mesh::Core * clone ( const tag::ReverseEachCell &, const tag::OneDummyWrapper & );

	// we are still in class Mesh::ZeroDim
	
	std::list<Cell>::iterator add_to_my_cells ( Cell::Core * const, const size_t );
	std::list<Cell>::iterator add_to_my_cells
	( Cell::Core * const, const size_t, const tag::DoNotBother & );
	// virtual from Cell::Core, here execution forbidden

	void remove_from_my_cells ( Cell::Core * const, const size_t, std::list<Cell>::iterator );
	void remove_from_my_cells
		( Cell::Core * const, const size_t, std::list<Cell>::iterator, const tag::DoNotBother & );
	// virtual from Cell::Core, here execution forbidden
	
	void closed_loop ( const Cell & ver );
	void closed_loop ( const Cell & ver, size_t );
	// virtual from Mesh::Core, here execution forbidden
		
	// iterators are virtual from Mesh::Core and are defined in iterator.cpp

	Mesh::Iterator::Core * iterator
	( const tag::OverVertices &, const tag::AsFound &, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator
	( const tag::OverVertices &, const tag::ForcePositive &, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator
	( const tag::OverVertices &, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator
	( const tag::OverVertices &, const tag::OrientCompWithMesh &, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator
	( const tag::OverVertices &, const tag::OrientOpposToMesh &, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator
	( const tag::OverVertices &, const tag::AsFound &,
	  const tag::RequireOrder &, const tag::ThisMeshIsPositive & );
	// iterate over the two vertices, first base (negative) then tip (positive)
	Mesh::Iterator::Core * iterator
	( const tag::OverVertices &, const tag::ForcePositive &,
	  const tag::RequireOrder &, const tag::ThisMeshIsPositive & );
	// iterate over the two vertices, first base then tip (both positive)
	Mesh::Iterator::Core * iterator
	( const tag::OverVertices &, const tag::RequireOrder &, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator
	( const tag::OverVertices &, const tag::OrientCompWithMesh &,
	  const tag::RequireOrder &, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator
	( const tag::OverVertices &, const tag::OrientOpposToMesh &,
	  const tag::RequireOrder &, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator
	( const tag::OverVertices &, const tag::AsFound &,
	  const tag::Backwards &, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator
	( const tag::OverVertices &, const tag::ForcePositive &,
	  const tag::Backwards &, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator
	( const tag::OverVertices &, const tag::Backwards &, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator
	( const tag::OverVertices &, const tag::OrientCompWithMesh &,
	  const tag::Backwards &, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator
	( const tag::OverVertices &, const tag::OrientOpposToMesh &,
	  const tag::Backwards &, const tag::ThisMeshIsPositive & );

	// we are still in class Mesh::ZeroDim

	Mesh::Iterator::Core * iterator  // execution forbidden
	( const tag::OverSegments &, const tag::AsFound &, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator  // execution forbidden
	( const tag::OverSegments &, const tag::ForcePositive &, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator  // execution forbidden
	( const tag::OverSegments &, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator  // execution forbidden
	( const tag::OverSegments &, const tag::OrientCompWithMesh &, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator
	( const tag::OverSegments &, const tag::OrientOpposToMesh &, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator  // execution forbidden
	( const tag::OverSegments &, const tag::AsFound &,
	  const tag::RequireOrder &, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator  // execution forbidden
	( const tag::OverSegments &, const tag::ForcePositive &,
	  const tag::RequireOrder &, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator  // execution forbidden
	( const tag::OverSegments &, const tag::RequireOrder &, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator  // execution forbidden
	( const tag::OverSegments &, const tag::OrientCompWithMesh &,
	  const tag::RequireOrder &, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator
	( const tag::OverSegments &, const tag::OrientOpposToMesh &,
	  const tag::RequireOrder &, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator  // execution forbidden
	( const tag::OverSegments &, const tag::AsFound &,
	  const tag::Backwards &, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator  // execution forbidden
	( const tag::OverSegments &, const tag::ForcePositive &,
	  const tag::Backwards &, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator  // execution forbidden
	( const tag::OverSegments &, const tag::Backwards &, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator  // execution forbidden
	( const tag::OverSegments &, const tag::OrientCompWithMesh &,
	  const tag::Backwards &, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator
	( const tag::OverSegments &, const tag::OrientOpposToMesh &,
	  const tag::Backwards &, const tag::ThisMeshIsPositive & );

	// we are still in class Mesh::ZeroDim
	// twelve iterators below assert dim == 0 then call iterator over vertices

	Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfDim &, const size_t,
	  const tag::AsFound &, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfDim &, const size_t,
	  const tag::CellHasLowDim &, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfDim &, const size_t,
	  const tag::ForcePositive &, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfDim &, const size_t, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfDim &, const size_t,
	  const tag::OrientCompWithMesh &, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfDim &, const size_t,
	  const tag::OrientOpposToMesh &, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfDim &, const size_t, const tag::AsFound &,
	  const tag::RequireOrder &, const tag::ThisMeshIsPositive &        );
	Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfDim &, const size_t, const tag::ForcePositive &,
	  const tag::RequireOrder &, const tag::ThisMeshIsPositive &            );
	Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfDim &, const size_t,
	  const tag::RequireOrder &, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfDim &, const size_t, const tag::OrientCompWithMesh &,
	  const tag::RequireOrder &, const tag::ThisMeshIsPositive &                 );
	Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfDim &, const size_t, const tag::OrientOpposToMesh &,
	  const tag::RequireOrder &, const tag::ThisMeshIsPositive &                );
	Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfDim &, const size_t, const tag::AsFound &,
	  const tag::Backwards &, const tag::ThisMeshIsPositive &        );
	Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfDim &, const size_t, const tag::ForcePositive &,
	  const tag::Backwards &, const tag::ThisMeshIsPositive &            );
	Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfDim &, const size_t,
	  const tag::Backwards &, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfDim &, const size_t, const tag::OrientCompWithMesh &,
	  const tag::Backwards &, const tag::ThisMeshIsPositive &                 );
	Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfDim &, const size_t, const tag::OrientOpposToMesh &,
	  const tag::Backwards &, const tag::ThisMeshIsPositive &                );

	// we are still in class Mesh::ZeroDim
	// twelve iterators below simply produce iterator over vertices
	
	Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfMaxDim &, const tag::AsFound &, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfMaxDim &, const tag::ForcePositive &, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfMaxDim &, const tag::OrientCompWithMesh &, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfMaxDim &, const tag::OrientOpposToMesh &, const tag::ThisMeshIsPositive & );

	Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfMaxDim &, const tag::AsFound &,
	  const tag::RequireOrder &, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfMaxDim &, const tag::ForcePositive &,
	  const tag::RequireOrder &, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfMaxDim &, const tag::OrientCompWithMesh &,
	  const tag::RequireOrder &, const tag::ThisMeshIsPositive &      );
	Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfMaxDim &, const tag::OrientOpposToMesh &,
	  const tag::RequireOrder &, const tag::ThisMeshIsPositive &     );

	Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfMaxDim &, const tag::AsFound &,
	  const tag::Backwards &, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfMaxDim &, const tag::ForcePositive &,
	  const tag::Backwards &, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfMaxDim &, const tag::OrientCompWithMesh &,
	  const tag::Backwards &, const tag::ThisMeshIsPositive &      );
	Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfMaxDim &, const tag::OrientOpposToMesh &,
	  const tag::Backwards &, const tag::ThisMeshIsPositive &     );

	// we are still in class Mesh::ZeroDim

	Mesh::Iterator::Core * iterator  // execution forbidden
	( const tag::OverVertices &, const tag::ConnectedTo &, Cell::Positive::Vertex *,
	  const tag::ThisMeshIsPositive &                                               );
	Mesh::Iterator::Core * iterator  // execution forbidden
	( const tag::OverVertices &, const tag::ConnectedTo &, Cell::Positive::Vertex *,
	  const tag::RequireOrder &, const tag::ThisMeshIsPositive &                    );

	Mesh::Iterator::Core * iterator  // execution forbidden
	( const tag::OverSegments &, const tag::WhoseTipIs &, Cell::Positive::Vertex * V,
	  const tag::ThisMeshIsPositive &                                                );
	Mesh::Iterator::Core * iterator  // execution forbidden
	( const tag::OverSegments &, const tag::WhoseTipIs &, Cell::Positive::Vertex * V,
	  const tag::RequireOrder &, const tag::ThisMeshIsPositive &                     );
	Mesh::Iterator::Core * iterator  // execution forbidden
	( const tag::OverSegments &, const tag::WhoseTipIs &, Cell::Positive::Vertex * V,
	  const tag::Backwards &, const tag::ThisMeshIsPositive &                        );

	Mesh::Iterator::Core * iterator  // execution forbidden
	( const tag::OverSegments &, const tag::WhoseBaseIs &, Cell::Positive::Vertex * V,
	  const tag::ThisMeshIsPositive &                                                 );
	Mesh::Iterator::Core * iterator  // execution forbidden
	( const tag::OverSegments &, const tag::WhoseBaseIs &, Cell::Positive::Vertex * V,
	  const tag::RequireOrder &, const tag::ThisMeshIsPositive &                      );
	Mesh::Iterator::Core * iterator  // execution forbidden
	( const tag::OverSegments &, const tag::WhoseBaseIs &, Cell::Positive::Vertex * V,
	  const tag::Backwards &, const tag::ThisMeshIsPositive &                         );

	Mesh::Iterator::Core * iterator  // execution forbidden
	( const tag::OverSegments &, const tag::AsFound &, const tag::Around &, Cell::Positive::Vertex *,
	  const tag::ThisMeshIsPositive &                                                                );
	Mesh::Iterator::Core * iterator  // execution forbidden
	( const tag::OverSegments &, const tag::AsFound &, const tag::Around &, Cell::Positive::Vertex *,
	  const tag::RequireOrder &, const tag::ThisMeshIsPositive &                                     );
	Mesh::Iterator::Core * iterator  // execution forbidden
	( const tag::OverSegments &, const tag::AsFound &, const tag::Around &, Cell::Positive::Vertex *,
	  const tag::Backwards &, const tag::ThisMeshIsPositive &                                     );
	Mesh::Iterator::Core * iterator  // execution forbidden
	( const tag::OverSegments &, const tag::ForcePositive &,
	  const tag::Around &, Cell::Positive::Vertex *, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator  // execution forbidden
	( const tag::OverSegments &, const tag::ForcePositive &,
	  const tag::Around &, Cell::Positive::Vertex *,
	  const tag::RequireOrder &, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator  // execution forbidden
	( const tag::OverSegments &, const tag::ForcePositive &,
	  const tag::Around &, Cell::Positive::Vertex *,
	  const tag::Backwards &, const tag::ThisMeshIsPositive & );

	// we are still in class Mesh::ZeroDim

	Mesh::Iterator::Core * iterator  // execution forbidden
	( const tag::OverCellsOfDim &, const size_t, const tag::AsFound &,
	  const tag::Around &, Cell::Core *, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator  // execution forbidden
	( const tag::OverCellsOfDim &, const size_t, const tag::AsFound &,
	  const tag::Around &, Cell::Core *,
	  const tag::RequireOrder &, const tag::ThisMeshIsPositive &      );
	Mesh::Iterator::Core * iterator  // execution forbidden
	( const tag::OverCellsOfDim &, const size_t, const tag::AsFound &,
	  const tag::Around &, Cell::Core *,
	  const tag::Backwards &, const tag::ThisMeshIsPositive &      );
	Mesh::Iterator::Core * iterator  // execution forbidden
	( const tag::OverCellsOfDim &, const size_t, const tag::ForcePositive &,
	  const tag::Around &, Cell::Core *, const tag::ThisMeshIsPositive &    );
	Mesh::Iterator::Core * iterator  // execution forbidden
	( const tag::OverCellsOfDim &, const size_t, const tag::ForcePositive &,
	  const tag::Around &, Cell::Core *,
	  const tag::RequireOrder &, const tag::ThisMeshIsPositive &            );
	Mesh::Iterator::Core * iterator  // execution forbidden
	( const tag::OverCellsOfDim &, const size_t, const tag::ForcePositive &,
	  const tag::Around &, Cell::Core *,
	  const tag::Backwards &, const tag::ThisMeshIsPositive &            );
	Mesh::Iterator::Core * iterator  // execution forbidden
	( const tag::OverCellsOfDim &, const size_t, const tag::OrientCompWithCenter &,
	  const tag::Around &, Cell::Core *, const tag::ThisMeshIsPositive &           );
	Mesh::Iterator::Core * iterator  // execution forbidden
	( const tag::OverCellsOfDim &, const size_t, const tag::OrientOpposToCenter &,
	  const tag::Around &, Cell::Core *, const tag::ThisMeshIsPositive &          );
	/*	Mesh::Iterator::Core * iterator  // execution forbidden
	( const tag::OverCellsOfDim &, const size_t, const tag::OrientOpposToMesh &,
	  const tag::Around &, Cell::Core *,
	  const tag::ThisMeshIsPositive &                                         );
	Mesh::Iterator::Core * iterator  // execution forbidden
	( const tag::OverCellsOfDim &, const size_t, const tag::OrientOpposToMesh &,
	  const tag::Around &, Cell::Core *,
	  const tag::RequireOrder &, const tag::ThisMeshIsPositive &              );
	Mesh::Iterator::Core * iterator  // execution forbidden
	( const tag::OverCellsOfDim &, const size_t, const tag::OrientOpposToMesh &,
	  const tag::Around &, Cell::Core *,
	  const tag::Backwards &, const tag::ThisMeshIsPositive &              );  */

	// we are still in class Mesh:ZeroDim

	Mesh::Iterator::Core * iterator  // execution forbidden
	( const tag::OverCellsOfMaxDim &, const tag::OrientCompWithMesh &,
	  const tag::Around &, Cell::Core *, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator  // execution forbidden
	( const tag::OverCellsOfMaxDim &, const tag::OrientCompWithMesh &,
	  const tag::Around &, Cell::Core *, const tag::RequireOrder &, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator  // execution forbidden
	( const tag::OverCellsOfMaxDim &, const tag::OrientCompWithMesh &,
	  const tag::Around &, Cell::Core *, const tag::Backwards &, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator  // execution forbidden
	( const tag::OverCellsOfMaxDim &, const tag::ForcePositive &,
	  const tag::Around &, Cell::Core *, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator  // execution forbidden
	( const tag::OverCellsOfMaxDim &, const tag::ForcePositive &,
	  const tag::Around &, Cell::Core *,
	  const tag::RequireOrder &, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator  // execution forbidden
	( const tag::OverCellsOfMaxDim &, const tag::ForcePositive &,
	  const tag::Around &, Cell::Core *,
	  const tag::Backwards &, const tag::ThisMeshIsPositive &      );
	Mesh::Iterator::Core * iterator  // execution forbidden
	( const tag::OverCellsOfMaxDim &, const tag::OrientOpposToMesh &,
	  const tag::Around &, Cell::Core *, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator  // execution forbidden
	( const tag::OverCellsOfMaxDim &, const tag::OrientOpposToMesh &,
	  const tag::Around &, Cell::Core *, const tag::RequireOrder &, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator  // execution forbidden
	( const tag::OverCellsOfMaxDim &, const tag::OrientOpposToMesh &,
	  const tag::Around &, Cell::Core *, const tag::Backwards &, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator  // execution forbidden
	( const tag::OverCellsOfMaxDim &, const tag::OrientCompWithCenter &,
	  const tag::Around &, Cell::Core *, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator  // execution forbidden
	( const tag::OverCellsOfMaxDim &, const tag::OrientOpposToCenter &,
	  const tag::Around &, Cell::Core *, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator  // execution forbidden
	( const tag::OverCellsOfMaxDim &, const tag::AsFound &,
	  const tag::Around &, Cell::Core *, const tag::ThisMeshIsPositive & );

	#ifndef NDEBUG  // DEBUG mode
	// attribute  name  inherited from Mesh::Core
	// std::string get_name()  defined by Mesh::Core
	void print_everything ();  // virtual from Mesh::Core
	#endif  // DEBUG

};  // end of class Mesh::ZeroDim

//------------------------------------------------------------------------------------------------------//


class Mesh::NotZeroDim : public Mesh::Core

// represents a positive mesh of dimension >= 1

// abstract class, specialized in Mesh::Connected::***Dim,
//   Mesh::MultiplyConnected::***Dim, Mesh::Fuzzy, Mesh::STSI
// see paragraph 12.4 in the manual

{	public :

	// size_t nb_of_wrappers  inherited from tag::Util::Core ifdef MANIFEM_COLLECT_CM

	// Cell::Positive * cell_enclosed inherited from Mesh::Core

	inline NotZeroDim ( const tag::OfDimension &, const size_t d, const tag::OneDummyWrapper & )
	:	Mesh::Core ( tag::of_dim, d, tag::one_dummy_wrapper )  { }

	inline NotZeroDim ( const tag::OfDimension &, const size_t dim_p1, const tag::MinusOne &,
	                    const tag::OneDummyWrapper &                                         )
	:	Mesh::Core ( tag::of_dim, dim_p1, tag::minus_one, tag::one_dummy_wrapper )  { }

	virtual ~NotZeroDim ( ) { }
	
	// bool dispose_query ( )  defined by tag::Util::Core ifdef MANIFEM_COLLECT_CM
	
	// size_t get_dim_plus_one ( )  stays pure virtual from Mesh::Core

	// four versions of number_of  stay pure virtual from Mesh::Core

	// first_vertex, last_vertex, first_segment and last_segment
	// are defined by Mesh::Core, execution forbidden

	// cell_in_front_of and cell_behind ( tag::seen_from )
	// defined by Mesh::Core, execution forbidden

	// the thirty-three methods below are virtual from Mesh::Core
	// some of them are later overridden by Mesh::STSI
	// called from Cell::****tive::***::add_to_mesh and Cell::****tive::***::remove_from_mesh
	void add_pos_seg ( Cell::Positive::Segment *, const tag::MeshIsNotBdry & );
	void add_pos_seg
	( Cell::Positive::Segment *, const tag::MeshIsNotBdry &, const tag::DoNotBother & );
	void add_pos_seg ( Cell::Positive::Segment *, const tag::MeshIsBdry & );
	void add_pos_seg
	( Cell::Positive::Segment *, const tag::MeshIsBdry &, const tag::DoNotBother & );
	void remove_pos_seg ( Cell::Positive::Segment *, const tag::MeshIsNotBdry & );
	void remove_pos_seg
	( Cell::Positive::Segment *, const tag::MeshIsNotBdry &, const tag::DoNotBother & );
	void remove_pos_seg ( Cell::Positive::Segment *, const tag::MeshIsBdry & );
	void remove_pos_seg
	( Cell::Positive::Segment *, const tag::MeshIsBdry &, const tag::DoNotBother & );
	void add_neg_seg ( Cell::Negative::Segment *, const tag::MeshIsNotBdry & );
	void add_neg_seg
	( Cell::Negative::Segment *, const tag::MeshIsNotBdry &, const tag::DoNotBother & );
	void add_neg_seg ( Cell::Negative::Segment *, const tag::MeshIsBdry & );
	void add_neg_seg
	( Cell::Negative::Segment *, const tag::MeshIsBdry &, const tag::DoNotBother & );
	void remove_neg_seg ( Cell::Negative::Segment *, const tag::MeshIsNotBdry & );
	void remove_neg_seg
	( Cell::Negative::Segment *, const tag::MeshIsNotBdry &, const tag::DoNotBother & );
	void remove_neg_seg ( Cell::Negative::Segment *, const tag::MeshIsBdry & );
	void remove_neg_seg
	( Cell::Negative::Segment *, const tag::MeshIsBdry &, const tag::DoNotBother & );
	void add_pos_hd_cell ( Cell::Positive::HighDim *, const tag::MeshIsNotBdry & );
	void add_pos_hd_cell
	( Cell::Positive::HighDim *, const tag::MeshIsNotBdry &, const tag::DoNotBother & );
	void add_pos_hd_cell ( Cell::Positive::HighDim *, const tag::MeshIsBdry & );
	void add_pos_hd_cell
	( Cell::Positive::HighDim *, const tag::MeshIsBdry &, const tag::DoNotBother & );
	void remove_pos_hd_cell ( Cell::Positive::HighDim *, const tag::MeshIsNotBdry & );
	void remove_pos_hd_cell
	( Cell::Positive::HighDim *, const tag::MeshIsNotBdry &, const tag::DoNotBother & );
	void remove_pos_hd_cell ( Cell::Positive::HighDim *, const tag::MeshIsBdry & );
	void remove_pos_hd_cell
	( Cell::Positive::HighDim *, const tag::MeshIsBdry &, const tag::DoNotBother & );
	void add_neg_hd_cell ( Cell::Negative::HighDim *, const tag::MeshIsNotBdry & );
	void add_neg_hd_cell
	( Cell::Negative::HighDim *, const tag::MeshIsNotBdry &, const tag::DoNotBother & );
	void add_neg_hd_cell ( Cell::Negative::HighDim *, const tag::MeshIsBdry & );
	void add_neg_hd_cell
	( Cell::Negative::HighDim *, const tag::MeshIsBdry &, const tag::DoNotBother & );
	void remove_neg_hd_cell ( Cell::Negative::HighDim *, const tag::MeshIsNotBdry & );
	void remove_neg_hd_cell
	( Cell::Negative::HighDim *, const tag::MeshIsNotBdry &, const tag::DoNotBother & );
	void remove_neg_hd_cell ( Cell::Negative::HighDim *, const tag::MeshIsBdry & );
	void remove_neg_hd_cell
	( Cell::Negative::HighDim *, const tag::MeshIsBdry &, const tag::DoNotBother & );

	// seven versions of 'clone' stay pure virtual from Mesh::Core

	// two versions of 'add_to_my_cells' stay pure virtual from Cell::Core
	// two versions of 'remove_from_my_cells' stay pure virtual from Cell::Core

	// two versions of 'closed_loop' stay pure virtual from Mesh::Core

	// iterators stay pure virtual from Mesh::Core

	#ifndef NDEBUG  // DEBUG mode
	// attribute  name  inherited from Mesh::Core
	// std::string get_name()  defined by Mesh::Core
	// void print_everything ()  stays pure virtual from Mesh::Core
	#endif  // DEBUG

};  // end of class Mesh::NotZeroDim

//------------------------------------------------------------------------------------------------------//


class Mesh::Connected::OneDim : public Mesh::NotZeroDim

// represents a connected positive mesh of dimension 1
// a chain of segments, either open or closed

{	public :

	// size_t nb_of_wrappers  inherited from tag::Util::Core ifdef MANIFEM_COLLECT_CM

	// Cell::Positive * cell_enclosed inherited from Mesh::Core

	size_t nb_of_segs;
	// useful for quickly answering to 'number_of'

	// here we use Cell wrappers as pointers
	Cell first_ver, last_ver;  // both positive
	// if  last_ver == first_ver  the mesh is a closed loop
	
	inline OneDim ( const tag::OneDummyWrapper & )
	:	Mesh::NotZeroDim ( tag::of_dimension, 2, tag::minus_one, tag::one_dummy_wrapper ),
		first_ver ( tag::non_existent ), last_ver ( tag::non_existent )
	{	}

	inline OneDim
	( const tag::With &, size_t n, const tag::Segments &, const tag::OneDummyWrapper & )
	:	Mesh::NotZeroDim ( tag::of_dimension, 2, tag::minus_one, tag::one_dummy_wrapper ),
		nb_of_segs { n }, first_ver ( tag::non_existent ), last_ver ( tag::non_existent )
	{	}

	virtual ~OneDim ();
	
	// bool dispose_query ( )  defined by tag::Util::Core ifdef MANIFEM_COLLECT_CM
	
	size_t get_dim_plus_one ( ) const;  // virtual from Mesh::Core

	size_t number_of ( const tag::Vertices & ) const;  // virtual from Mesh::Core
	size_t number_of ( const tag::Segments & ) const;  // virtual from Mesh::Core
	size_t number_of ( const tag::CellsOfDim &, const size_t d ) const;  // virtual from Mesh::Core
	size_t number_of ( const tag::CellsOfMaxDim & ) const;  // virtual from Mesh::Core

	Cell first_vertex ( );  // virtual from Mesh::Core, here overridden
	Cell last_vertex ( );  // virtual from Mesh::Core, here overridden
	Cell first_segment ( );  // virtual from Mesh::Core, here overridden
	Cell last_segment ( );  // virtual from Mesh::Core, here overridden

	// cell_in_front_of and cell_behind ( tag::seen_from )
	// defined by Mesh::Core, execution forbidden

	// thirty-two methods add_pos_seg, remove_pos_seg, add_neg_seg, remove_neg_seg,
	// add_pos_hd_cell, remove_pos_hd_cell, add_neg_hd_cell, remove_neg_hd_cell
	// (four versions of each) defined by Mesh::NotZeroDim
	// called from Cell::****tive::***::add_to_mesh and Cell::****tive::***::remove_from_mesh
	using Mesh::NotZeroDim::add_pos_seg;
	using Mesh::NotZeroDim::add_neg_seg;

	// two methods below keep 'seg' as 'first_ver' although this is of course not true
	// useful for extruding meshes
	void add_pos_seg  // virtual from Mesh::Core, here overridden
	( Cell::Positive::Segment *, const tag::MeshIsNotBdry &,
	  const tag::DoNotBother &, const tag::KeepAsFirstVer & );
	void add_neg_seg  // virtual from Mesh::Core, here overridden
	( Cell::Negative::Segment *, const tag::MeshIsNotBdry &,
	  const tag::DoNotBother &, const tag::KeepAsFirstVer & );
	
	// seven versions of 'clone' virtual from Mesh::Core
	Mesh::Core * clone ( const tag::Depth &, size_t dep, const tag::OneDummyWrapper & );
	Mesh::Core * clone ( const tag::Depth &, const tag::Infinity &, const tag::OneDummyWrapper & );
	Mesh::Core * clone
	( const tag::MakeFuzzy &, const tag::Depth &, size_t dep, const tag::OneDummyWrapper & );
	Mesh::Core * clone
	( const tag::MakeFuzzy &, const tag::Depth &, const tag::Infinity &, const tag::OneDummyWrapper & );
	Mesh::Core * clone
	( const tag::MakeConnectedOneDim &, const tag::Depth &, size_t dep, const tag::OneDummyWrapper & );
	Mesh::Core * clone
	( const tag::MakeConnectedOneDim &, const tag::Depth &, const tag::Infinity &,
	  const tag::OneDummyWrapper &                                                );
	Mesh::Core * clone ( const tag::ReverseEachCell &, const tag::OneDummyWrapper & );
	
	std::list<Cell>::iterator add_to_my_cells ( Cell::Core * const, const size_t );
	// virtual from Cell::Core, here returns garbage, updates nb_of_segs, first_ver, last_ver

	std::list<Cell>::iterator add_to_my_cells
	( Cell::Core * const, const size_t, const tag::DoNotBother & );
	// virtual from Cell::Core, here returns garbage

	void remove_from_my_cells ( Cell::Core * const, const size_t, std::list<Cell>::iterator );
	// virtual from Cell::Core, here only updates nb_of_segs, first_ver, last_ver
	
	void remove_from_my_cells
	( Cell::Core * const, const size_t, std::list<Cell>::iterator, const tag::DoNotBother & );
	// virtual from Cell::Core, here does nothing
	
	void closed_loop ( const Cell & ver );  // virtual from Mesh::Core
	// sets both first_ver and last_ver to 'ver'

	void closed_loop ( const Cell & ver, size_t );  // virtual from Mesh::Core
	// sets both first_ver and last_ver to 'ver' and number of segments

	// iterators are virtual from Mesh::Core and are defined in iterator.cpp

	using Mesh::NotZeroDim::iterator;
	
	Mesh::Iterator::Core * iterator
	( const tag::OverVertices &, const tag::AsFound &, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator
	( const tag::OverVertices &, const tag::ForcePositive &, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator
	( const tag::OverVertices &, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator
	( const tag::OverVertices &, const tag::OrientCompWithMesh &, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator
	( const tag::OverVertices &, const tag::OrientOpposToMesh &, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator
	( const tag::OverVertices &, const tag::AsFound &,
	  const tag::RequireOrder &, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator
	( const tag::OverVertices &, const tag::ForcePositive &,
	  const tag::RequireOrder &, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator
	( const tag::OverVertices &, const tag::RequireOrder &, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator
	( const tag::OverVertices &, const tag::OrientCompWithMesh &,
	  const tag::RequireOrder &, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator
	( const tag::OverVertices &, const tag::OrientOpposToMesh &,
	  const tag::RequireOrder &, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator
	( const tag::OverVertices &, const tag::AsFound &,
	  const tag::Backwards &, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator
	( const tag::OverVertices &, const tag::ForcePositive &,
	  const tag::Backwards &, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator
	( const tag::OverVertices &, const tag::Backwards &, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator
	( const tag::OverVertices &, const tag::OrientCompWithMesh &,
	  const tag::Backwards &, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator
	( const tag::OverVertices &, const tag::OrientOpposToMesh &,
	  const tag::Backwards &, const tag::ThisMeshIsPositive & );

	// we are still in class Mesh::Connected::OneDim

	Mesh::Iterator::Core * iterator
	( const tag::OverSegments &, const tag::AsFound &, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator
	( const tag::OverSegments &, const tag::ForcePositive &, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator
	( const tag::OverSegments &, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator
	( const tag::OverSegments &, const tag::OrientCompWithMesh &, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator
	( const tag::OverSegments &, const tag::OrientOpposToMesh &, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator
	( const tag::OverSegments &, const tag::AsFound &,
	  const tag::RequireOrder &, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator
	( const tag::OverSegments &, const tag::ForcePositive &,
	  const tag::RequireOrder &, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator
	( const tag::OverSegments &, const tag::RequireOrder &, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator
	( const tag::OverSegments &, const tag::OrientCompWithMesh &,
	  const tag::RequireOrder &, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator
	( const tag::OverSegments &, const tag::OrientOpposToMesh &,
	  const tag::RequireOrder &, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator
	( const tag::OverSegments &, const tag::AsFound &,
	  const tag::Backwards &, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator
	( const tag::OverSegments &, const tag::ForcePositive &,
	  const tag::Backwards &, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator
	( const tag::OverSegments &, const tag::Backwards &, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator
	( const tag::OverSegments &, const tag::OrientCompWithMesh &,
	  const tag::Backwards &, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator
	( const tag::OverSegments &, const tag::OrientOpposToMesh &,
	  const tag::Backwards &, const tag::ThisMeshIsPositive & );

	// we are still in class Mesh::Connected::OneDim

	Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfDim &, const size_t,
	  const tag::AsFound &, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfDim &, const size_t,
	  const tag::CellHasLowDim &, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfDim &, const size_t,
	  const tag::ForcePositive &, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfDim &, const size_t, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfDim &, const size_t,
	  const tag::OrientCompWithMesh &, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfDim &, const size_t,
	  const tag::OrientOpposToMesh &, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfDim &, const size_t, const tag::AsFound &,
	  const tag::RequireOrder &, const tag::ThisMeshIsPositive &        );
	Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfDim &, const size_t, const tag::ForcePositive &,
	  const tag::RequireOrder &, const tag::ThisMeshIsPositive &            );
	Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfDim &, const size_t,
	  const tag::RequireOrder &, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfDim &, const size_t, const tag::OrientCompWithMesh &,
	  const tag::RequireOrder &, const tag::ThisMeshIsPositive &                 );
	Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfDim &, const size_t, const tag::OrientOpposToMesh &,
	  const tag::RequireOrder &, const tag::ThisMeshIsPositive &                );
	Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfDim &, const size_t, const tag::AsFound &,
	  const tag::Backwards &, const tag::ThisMeshIsPositive &        );
	Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfDim &, const size_t, const tag::ForcePositive &,
	  const tag::Backwards &, const tag::ThisMeshIsPositive &            );
	Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfDim &, const size_t,
	  const tag::Backwards &, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfDim &, const size_t, const tag::OrientCompWithMesh &,
	  const tag::Backwards &, const tag::ThisMeshIsPositive &                 );
	Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfDim &, const size_t, const tag::OrientOpposToMesh &,
	  const tag::Backwards &, const tag::ThisMeshIsPositive &              );

	// we are still in class Mesh::Connected::OneDim
	// twelve iterators below simply produce iterator over segments
	
	Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfMaxDim &, const tag::AsFound &, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfMaxDim &, const tag::ForcePositive &, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfMaxDim &, const tag::OrientCompWithMesh &, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfMaxDim &, const tag::OrientOpposToMesh &, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfMaxDim &, const tag::AsFound &,
	  const tag::RequireOrder &, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfMaxDim &, const tag::ForcePositive &,
	  const tag::RequireOrder &, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfMaxDim &, const tag::OrientCompWithMesh &,
	  const tag::RequireOrder &, const tag::ThisMeshIsPositive &      );
	Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfMaxDim &, const tag::OrientOpposToMesh &,
	  const tag::RequireOrder &, const tag::ThisMeshIsPositive &     );
	Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfMaxDim &, const tag::AsFound &,
	  const tag::Backwards &, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfMaxDim &, const tag::ForcePositive &,
	  const tag::Backwards &, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfMaxDim &, const tag::OrientCompWithMesh &,
	  const tag::Backwards &, const tag::ThisMeshIsPositive &      );
	Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfMaxDim &, const tag::OrientOpposToMesh &,
	  const tag::Backwards &, const tag::ThisMeshIsPositive &     );

	// we are still in class Mesh::Connected::OneDim

	Mesh::Iterator::Core * iterator  // execution forbidden
	( const tag::OverVertices &, const tag::ConnectedTo &, Cell::Positive::Vertex *,
	  const tag::ThisMeshIsPositive &                                               );
	Mesh::Iterator::Core * iterator  // execution forbidden
	( const tag::OverVertices &, const tag::ConnectedTo &, Cell::Positive::Vertex *,
	  const tag::RequireOrder &, const tag::ThisMeshIsPositive &                    );

	Mesh::Iterator::Core * iterator  // execution forbidden
	( const tag::OverSegments &, const tag::WhoseTipIs &, Cell::Positive::Vertex * V,
	  const tag::ThisMeshIsPositive &                                                );
	Mesh::Iterator::Core * iterator  // execution forbidden
	( const tag::OverSegments &, const tag::WhoseTipIs &, Cell::Positive::Vertex * V,
	  const tag::RequireOrder &, const tag::ThisMeshIsPositive &                     );
	Mesh::Iterator::Core * iterator  // execution forbidden
	( const tag::OverSegments &, const tag::WhoseTipIs &, Cell::Positive::Vertex * V,
	  const tag::Backwards &, const tag::ThisMeshIsPositive &                        );

	Mesh::Iterator::Core * iterator  // execution forbidden
	( const tag::OverSegments &, const tag::WhoseBaseIs &, Cell::Positive::Vertex * V,
	  const tag::ThisMeshIsPositive &                                                 );
	Mesh::Iterator::Core * iterator  // execution forbidden
	( const tag::OverSegments &, const tag::WhoseBaseIs &, Cell::Positive::Vertex * V,
	  const tag::RequireOrder &, const tag::ThisMeshIsPositive &                      );
	Mesh::Iterator::Core * iterator  // execution forbidden
	( const tag::OverSegments &, const tag::WhoseBaseIs &, Cell::Positive::Vertex * V,
	  const tag::Backwards &, const tag::ThisMeshIsPositive &                         );

	Mesh::Iterator::Core * iterator  // execution forbidden
	( const tag::OverSegments &, const tag::AsFound &, const tag::Around &, Cell::Positive::Vertex *,
	  const tag::ThisMeshIsPositive &                                                                );
	Mesh::Iterator::Core * iterator  // execution forbidden
	( const tag::OverSegments &, const tag::AsFound &, const tag::Around &, Cell::Positive::Vertex *,
	  const tag::RequireOrder &, const tag::ThisMeshIsPositive &                                     );
	Mesh::Iterator::Core * iterator  // execution forbidden
	( const tag::OverSegments &, const tag::AsFound &, const tag::Around &, Cell::Positive::Vertex *,
	  const tag::Backwards &, const tag::ThisMeshIsPositive &                                     );
	Mesh::Iterator::Core * iterator  // execution forbidden
	( const tag::OverSegments &, const tag::ForcePositive &,
	  const tag::Around &, Cell::Positive::Vertex *, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator  // execution forbidden
	( const tag::OverSegments &, const tag::ForcePositive &,
	  const tag::Around &, Cell::Positive::Vertex *,
	  const tag::RequireOrder &, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator  // execution forbidden
	( const tag::OverSegments &, const tag::ForcePositive &,
	  const tag::Around &, Cell::Positive::Vertex *,
	  const tag::Backwards &, const tag::ThisMeshIsPositive & );

	// we are still in class Mesh::Connected::OneDim

	Mesh::Iterator::Core * iterator  // execution forbidden
	( const tag::OverCellsOfDim &, const size_t, const tag::AsFound &,
	  const tag::Around &, Cell::Core *, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator  // execution forbidden
	( const tag::OverCellsOfDim &, const size_t, const tag::AsFound &,
	  const tag::Around &, Cell::Core *,
	  const tag::RequireOrder &, const tag::ThisMeshIsPositive &      );
	Mesh::Iterator::Core * iterator  // execution forbidden
	( const tag::OverCellsOfDim &, const size_t, const tag::AsFound &,
	  const tag::Around &, Cell::Core *, const tag::Backwards &, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator  // execution forbidden
	( const tag::OverCellsOfDim &, const size_t, const tag::ForcePositive &,
	  const tag::Around &, Cell::Core *, const tag::ThisMeshIsPositive &    );
	Mesh::Iterator::Core * iterator  // execution forbidden
	( const tag::OverCellsOfDim &, const size_t, const tag::ForcePositive &,
	  const tag::Around &, Cell::Core *,
	  const tag::RequireOrder &, const tag::ThisMeshIsPositive &            );
	Mesh::Iterator::Core * iterator  // execution forbidden
	( const tag::OverCellsOfDim &, const size_t, const tag::ForcePositive &,
	  const tag::Around &, Cell::Core *,
	  const tag::Backwards &, const tag::ThisMeshIsPositive &            );
	Mesh::Iterator::Core * iterator  // execution forbidden
	( const tag::OverCellsOfDim &, const size_t, const tag::OrientCompWithCenter &,
	  const tag::Around &, Cell::Core *, const tag::ThisMeshIsPositive &           );
	Mesh::Iterator::Core * iterator  // execution forbidden
	( const tag::OverCellsOfDim &, const size_t, const tag::OrientOpposToCenter &,
	  const tag::Around &, Cell::Core *, const tag::ThisMeshIsPositive &          );
	/*Mesh::Iterator::Core * iterator  // execution forbidden
	( const tag::OverCellsOfDim &, const size_t, const tag::OrientOpposToMesh &,
	  const tag::Around &, Cell::Core *, const tag::ThisMeshIsPositive &      );
	Mesh::Iterator::Core * iterator  // execution forbidden
	( const tag::OverCellsOfDim &, const size_t, const tag::OrientOpposToMesh &,
	  const tag::Around &, Cell::Core *,
	  const tag::RequireOrder &, const tag::ThisMeshIsPositive &              );
	Mesh::Iterator::Core * iterator  // execution forbidden
	( const tag::OverCellsOfDim &, const size_t, const tag::OrientOpposToMesh &,
	  const tag::Around &, Cell::Core *,
	  const tag::Backwards &, const tag::ThisMeshIsPositive &              );  */

	// we are still in class Mesh:Connected::OneDim

	Mesh::Iterator::Core * iterator  // execution forbidden
	( const tag::OverCellsOfMaxDim &, const tag::OrientCompWithMesh &,
	  const tag::Around &, Cell::Core *, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator  // execution forbidden
	( const tag::OverCellsOfMaxDim &, const tag::OrientCompWithMesh &,
	  const tag::Around &, Cell::Core *, const tag::RequireOrder &, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator  // execution forbidden
	( const tag::OverCellsOfMaxDim &, const tag::OrientCompWithMesh &,
	  const tag::Around &, Cell::Core *, const tag::Backwards &, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator  // execution forbidden
	( const tag::OverCellsOfMaxDim &, const tag::ForcePositive &,
	  const tag::Around &, Cell::Core *, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator  // execution forbidden
	( const tag::OverCellsOfMaxDim &, const tag::ForcePositive &,
	  const tag::Around &, Cell::Core *,
	  const tag::RequireOrder &, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator  // execution forbidden
	( const tag::OverCellsOfMaxDim &, const tag::ForcePositive &,
	  const tag::Around &, Cell::Core *,
	  const tag::Backwards &, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator  // execution forbidden
	( const tag::OverCellsOfMaxDim &, const tag::OrientOpposToMesh &,
	  const tag::Around &, Cell::Core *, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator  // execution forbidden
	( const tag::OverCellsOfMaxDim &, const tag::OrientOpposToMesh &,
	  const tag::Around &, Cell::Core *, const tag::RequireOrder &, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator  // execution forbidden
	( const tag::OverCellsOfMaxDim &, const tag::OrientOpposToMesh &,
	  const tag::Around &, Cell::Core *, const tag::Backwards &, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfMaxDim &, const tag::OrientCompWithCenter &,
	  const tag::Around &, Cell::Core *, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfMaxDim &, const tag::OrientOpposToCenter &,
	  const tag::Around &, Cell::Core *, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfMaxDim &, const tag::AsFound &,
	  const tag::Around &, Cell::Core *, const tag::ThisMeshIsPositive & );

	// iterator with tag::orientation_compatible_with_both_center_and_mesh
	// defined in Mesh::NotZeroDim
	
	#ifndef NDEBUG  // DEBUG mode
	// attribute  name  inherited from Mesh::Core
	// std::string get_name()  defined by Mesh::Core
	void print_everything ();  // virtual from Mesh::Core
	#endif  // DEBUG

}; // end of  class Mesh::Connected::OneDim

//------------------------------------------------------------------------------------------------------//


class Mesh::Connected::HighDim : public Mesh::NotZeroDim

// represents a connected positive mesh of dimension >= 2

{	public :

	// size_t nb_of_wrappers  inherited from tag::Util::Core ifdef MANIFEM_COLLECT_CM

	std::vector < size_t > nb_of_cells;
	// useful for quickly answering to 'number_of'

	// Cell start;
	
	inline HighDim ( const tag::OfDimension &, const size_t dim, const tag::OneDummyWrapper & )
	:	Mesh::NotZeroDim ( tag::of_dimension, dim, tag::one_dummy_wrapper )
	{	}

	inline HighDim ( const tag::OfDimension &, const size_t dim_p1, const tag::MinusOne &,
	                 const tag::OneDummyWrapper &                                          )
	:	Mesh::NotZeroDim ( tag::of_dimension, dim_p1, tag::minus_one, tag::one_dummy_wrapper )
	{	}

	HighDim ( const tag::Triangle &, const Mesh & AB, const Mesh & BC, const Mesh & CA,
	          const tag::OneDummyWrapper &                                              );
	// defined in global.cpp

	virtual ~HighDim ( ) { }
	
	// bool dispose_query ( )  defined by tag::Util::Core ifdef MANIFEM_COLLECT_CM
	
	size_t get_dim_plus_one ( ) const;  // virtual from Mesh::Core

	size_t number_of ( const tag::Vertices & ) const;  // virtual from Mesh::Core
	size_t number_of ( const tag::Segments & ) const;  // virtual from Mesh::Core
	size_t number_of ( const tag::CellsOfDim &, const size_t d ) const;  // virtual from Mesh::Core
	size_t number_of ( const tag::CellsOfMaxDim & ) const;  // virtual from Mesh::Core

	// first_vertex, last_vertex, first_segment and last_segment
	// are defined by Mesh::Core, execution forbidden

	// private :
	
	// cell_in_front_of  and  cell_behind  defined by Mesh::Core, execution forbidden

	// add_to_my_cells ( Cell::Core *, size_t )  defined by Cell::Core, returns garbage
	// remove_from_my_cells defined by Cell::Core, does nothing

	// iterator with tag::orientation_compatible_with_both_center_and_mesh
	// defined in Mesh::NotZeroDim
	
	#ifndef NDEBUG  // DEBUG mode
	// attribute  name  inherited from Mesh::Core
	// std::string get_name()  defined by Mesh::Core
	void print_everything ();  // virtual from Mesh::Core
	#endif  // DEBUG
	
}; // end of  class Mesh::Connected::HighDim

//------------------------------------------------------------------------------------------------------//


class Mesh::MultiplyConnected::OneDim : public Mesh::NotZeroDim

// represents a positive mesh of dimension 1 with several connected components

{	public :

	// size_t nb_of_wrappers  inherited from tag::Util::Core ifdef MANIFEM_COLLECT_CM

	// Cell::Positive * cell_enclosed inherited from Mesh::Core

	std::vector < size_t > nb_of_cells;
	// useful for quickly answering to 'number_of'

	// keep a list or a vector of starting/ending points

	inline OneDim ( const tag::OneDummyWrapper & )
	:	Mesh::NotZeroDim ( tag::of_dimension, 2, tag::minus_one, tag::one_dummy_wrapper )
	{ }

	OneDim ( const tag::Segment &, Cell::Negative::Vertex * A,
	         Cell::Positive::Vertex * B, const tag::DividedIn &, const size_t n,
	         const tag::OneDummyWrapper &                                       );
	// defined in global.cpp
	
	// bool dispose_query ( )  defined by tag::Util::Core ifdef MANIFEM_COLLECT_CM

	virtual ~OneDim ( ) { }
	
	size_t get_dim_plus_one ( ) const;  // virtual from Mesh::Core

	// size_t number_of ( const tag::Vertices & ) const;  // virtual from Mesh::Core
	// size_t number_of ( const tag::Segments & ) const;  // virtual from Mesh::Core
	// size_t number_of ( const tag::CellsOfDim &, const size_t d ) const;  // virtual from Mesh::Core
	// size_t number_of ( const tag::CellsOfMaxDim & ) const;  // virtual from Mesh::Core

	// first_vertex, last_vertex, first_segment and last_segment
	// are defined by Mesh::Core, execution forbidden

	// private:
	
	// cell_in_front_of  and  cell_behind  defined by Mesh::Core, execution forbidden

	// add_to_my_cells ( Cell::Core *, size_t )  defined by Cell::Core, returns garbage
	// remove_from_my_cells defined by Cell::Core, does nothing

	// iterator with tag::orientation_compatible_with_both_center_and_mesh
	// defined in Mesh::NotZeroDim
	
	#ifndef NDEBUG  // DEBUG mode
	// attribute  name  inherited from Mesh::Core
	// std::string get_name()  defined by Mesh::Core
	// void print_everything ();  // virtual from Mesh::Core
	#endif  // DEBUG

}; // end of  class Mesh::MultiplyConnected::OneDim

//------------------------------------------------------------------------------------------------------//


class Mesh::MultiplyConnected::HighDim : public Mesh::NotZeroDim

// represents a positive mesh of dimension >= 2 with several connected components

{	public :

	// size_t nb_of_wrappers  inherited from tag::Util::Core ifdef MANIFEM_COLLECT_CM

	// Cell::Positive * cell_enclosed inherited from Mesh::Core

	std::vector < size_t > nb_of_cells;
	// useful for quickly answering to 'number_of'

	// keep a list or a vector of starting cells
	
	inline HighDim ( const tag::OfDimension &, const size_t dim, const tag::OneDummyWrapper & )
	:	Mesh::NotZeroDim ( tag::of_dimension, dim, tag::one_dummy_wrapper )
	{	}

	inline HighDim ( const tag::OfDimension &, const size_t dim_p1, const tag::MinusOne &,
	                 const tag::OneDummyWrapper &                                         )
	:	Mesh::NotZeroDim ( tag::of_dimension, dim_p1, tag::minus_one, tag::one_dummy_wrapper )
	{	}

	virtual ~HighDim ( ) { }
	
	// bool dispose_query ( )  defined by tag::Util::Core ifdef MANIFEM_COLLECT_CM
	
	size_t get_dim_plus_one ( ) const;  // virtual from Mesh::Core

	// size_t number_of ( const tag::Vertices & ) const;  // virtual from Mesh::Core
	// size_t number_of ( const tag::Segments & ) const;  // virtual from Mesh::Core
	// size_t number_of ( const tag::CellsOfDim &, const size_t d ) const;  // virtual from Mesh::Core
	// size_t number_of ( const tag::CellsOfMaxDim & ) const;  // virtual from Mesh::Core

	// first_vertex, last_vertex, first_segment and last_segment
	// are defined by Mesh::Core, execution forbidden

	void build_rectangle ( const Mesh & south, const Mesh & east,
		const Mesh & north, const Mesh & west, bool cut_rectangles_in_half );
	// defined in global.cpp

	static bool is_positive ( );
	static Mesh reverse ( Mesh::Core * core );

	// private :
	
	// cell_in_front_of  and  cell_behind  defined by Mesh::Core, execution forbidden

	// add_to_my_cells ( Cell::Core *, size_t )  defined by Cell::Core, returns garbage
	// remove_from_my_cells defined by Cell::Core, does nothing

	// iterator with tag::orientation_compatible_with_both_center_and_mesh
	// defined in Mesh::NotZeroDim
	
	#ifndef NDEBUG  // DEBUG mode
	// attribute  name  inherited from Mesh::Core
	// std::string get_name()  defined by Mesh::Core
	// void print_everything ();  // virtual from Mesh::Core
	#endif  // DEBUG
	
}; // end of  class Mesh::MultiplyConnected::HighDim

//------------------------------------------------------------------------------------------------------//


class Mesh::Fuzzy : public Mesh::NotZeroDim

// represents a positive mesh, unordered (includes 1D meshes)

// roughly speaking, a mesh is a collection of cells of the same dimension
// however, for efficiency purposes, we keep lists of cells of lower
// dimension as well; that's the purpose of the 'cells' vector :
// to keep lists of cells indexed by their dimension

{	public :
	
	// size_t nb_of_wrappers  inherited from tag::Util::Core ifdef MANIFEM_COLLECT_CM

	// Cell::Positive * cell_enclosed inherited from Mesh::Core

	// the 'cells' attribute holds lists of cells of 'this' mesh, indexed by their dimension
	// for maximum dimension, the cells are oriented
	// for lower dimension, the cells are always positive

	// here we use Cell wrappers as pointers because we want a mesh to keep its cells alive
	std::vector < std::list < Cell > > cells;

	inline Fuzzy ( const tag::OfDimension &, const size_t dim, const tag::OneDummyWrapper & )
	:	Mesh::NotZeroDim ( tag::of_dimension, dim, tag::one_dummy_wrapper ), cells ( dim + 1 )
	{	assert ( dim >= 1 );  }

	inline Fuzzy ( const tag::OfDimension &, const size_t dim_p1, const tag::MinusOne &,
	               const tag::OneDummyWrapper &                                         )
	:	Mesh::NotZeroDim ( tag::of_dimension, dim_p1, tag::minus_one, tag::one_dummy_wrapper ),
		cells ( dim_p1 )
	{	assert ( dim_p1 > 1 );  }

	virtual ~Fuzzy ();
	
	// bool dispose_query ( )  defined by tag::Util::Core ifdef MANIFEM_COLLECT_CM
	
	size_t get_dim_plus_one ( ) const;  // virtual from Mesh::Core

	size_t number_of ( const tag::Vertices & ) const;  // virtual from Mesh::Core
	size_t number_of ( const tag::Segments & ) const;  // virtual from Mesh::Core
	size_t number_of ( const tag::CellsOfDim &, const size_t d ) const;  // virtual from Mesh::Core
	size_t number_of ( const tag::CellsOfMaxDim & ) const;  // virtual from Mesh::Core

	// first_vertex, last_vertex, first_segment and last_segment
	// are defined by Mesh::Core, execution forbidden

	// cell_in_front_of and cell_behind ( tag::seen_from )
	// defined by Mesh::Core, execution forbidden, later overridden by Mesh::STSI
	
	void build_rectangle ( const Mesh & south, const Mesh & east,  // defined in global.cpp
		const Mesh & north, const Mesh & west, bool cut_rectangles_in_half );
		
	// thirty-two methods add_*** and remove_***  defined by Mesh::NotZeroDim
	// called from Cell::****tive::***::add_to_mesh and Cell::****tive::***::remove_from_mesh

	// seven versions of 'clone' virtual from Mesh::Core
	Mesh::Core * clone ( const tag::Depth &, size_t dep, const tag::OneDummyWrapper & );
	Mesh::Core * clone ( const tag::Depth &, const tag::Infinity &, const tag::OneDummyWrapper & );
	Mesh::Core * clone
	( const tag::MakeFuzzy &, const tag::Depth &, size_t dep, const tag::OneDummyWrapper & );
	Mesh::Core * clone
	( const tag::MakeFuzzy &, const tag::Depth &, const tag::Infinity &, const tag::OneDummyWrapper & );
	Mesh::Core * clone
	( const tag::MakeConnectedOneDim &, const tag::Depth &, size_t dep, const tag::OneDummyWrapper & );
	Mesh::Core * clone
	( const tag::MakeConnectedOneDim &, const tag::Depth &, const tag::Infinity &,
	  const tag::OneDummyWrapper &                                                );
	Mesh::Core * clone ( const tag::ReverseEachCell &, const tag::OneDummyWrapper & );
	// builds non-existing cells

	// add a cell to 'this->cells [d]' list, return iterator into that list
	std::list < Cell > ::iterator add_to_my_cells ( Cell::Core * const cll, const size_t d );
	std::list < Cell > ::iterator add_to_my_cells
	( Cell::Core * const cll, const size_t d, const tag::DoNotBother & );
	// virtual from Cell::Core
	
	// remove a cell from 'this->cells[d]' list using the provided iterator
	void remove_from_my_cells ( Cell::Core * const, const size_t d, std::list < Cell > ::iterator );
	void remove_from_my_cells
	( Cell::Core * const, const size_t d, std::list < Cell > ::iterator, const tag::DoNotBother & );
	// virtual from Cell::Core
	
	void closed_loop ( const Cell & ver );
	void closed_loop ( const Cell & ver, size_t );
	// virtual from Mesh::Core, here execution forbidden

	// we are still in class Mesh::Fuzzy
	// iterators are virtual from Mesh::Core and are defined in iterator.cpp

	using Mesh::NotZeroDim::iterator;

	Mesh::Iterator::Core * iterator
	( const tag::OverVertices &, const tag::AsFound &, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator
	( const tag::OverVertices &, const tag::ForcePositive &, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator
	( const tag::OverVertices &, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator
	( const tag::OverVertices &, const tag::OrientCompWithMesh &, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator
	( const tag::OverVertices &, const tag::OrientOpposToMesh &, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator
	( const tag::OverVertices &, const tag::AsFound &,
	  const tag::RequireOrder &, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator
	( const tag::OverVertices &, const tag::ForcePositive &,
	  const tag::RequireOrder &, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator
	( const tag::OverVertices &, const tag::RequireOrder &, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator
	( const tag::OverVertices &, const tag::OrientCompWithMesh &,
	  const tag::RequireOrder &, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator
	( const tag::OverVertices &, const tag::OrientOpposToMesh &,
	  const tag::RequireOrder &, const tag::ThisMeshIsPositive & );

	Mesh::Iterator::Core * iterator
	( const tag::OverVertices &, const tag::AsFound &,
	  const tag::Backwards &, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator
	( const tag::OverVertices &, const tag::ForcePositive &,
	  const tag::Backwards &, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator
	( const tag::OverVertices &, const tag::Backwards &, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator
	( const tag::OverVertices &, const tag::OrientCompWithMesh &,
	  const tag::Backwards &, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator
	( const tag::OverVertices &, const tag::OrientOpposToMesh &,
	  const tag::Backwards &, const tag::ThisMeshIsPositive & );

	// we are still in class Mesh::Fuzzy

	Mesh::Iterator::Core * iterator
	( const tag::OverSegments &, const tag::AsFound &, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator
	( const tag::OverSegments &, const tag::ForcePositive &, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator
	( const tag::OverSegments &, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator
	( const tag::OverSegments &, const tag::OrientCompWithMesh &, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator
	( const tag::OverSegments &, const tag::OrientOpposToMesh &, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator
	( const tag::OverSegments &, const tag::AsFound &,
	  const tag::RequireOrder &, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator
	( const tag::OverSegments &, const tag::ForcePositive &,
	  const tag::RequireOrder &, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator
	( const tag::OverSegments &, const tag::RequireOrder &, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator
	( const tag::OverSegments &, const tag::OrientCompWithMesh &,
	  const tag::RequireOrder &, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator
	( const tag::OverSegments &, const tag::OrientOpposToMesh &,
	  const tag::RequireOrder &, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator
	( const tag::OverSegments &, const tag::AsFound &,
	  const tag::Backwards &, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator
	( const tag::OverSegments &, const tag::ForcePositive &,
	  const tag::Backwards &, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator
	( const tag::OverSegments &, const tag::Backwards &, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator
	( const tag::OverSegments &, const tag::OrientCompWithMesh &,
	  const tag::Backwards &, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator
	( const tag::OverSegments &, const tag::OrientOpposToMesh &,
	  const tag::Backwards &, const tag::ThisMeshIsPositive & );

	// we are still in class Mesh::Fuzzy

	Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfDim &, const size_t,
	  const tag::AsFound &, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfDim &, const size_t,
	  const tag::CellHasLowDim &, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfDim &, const size_t,
	  const tag::ForcePositive &, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfDim &, const size_t, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfDim &, const size_t,
	  const tag::OrientCompWithMesh &, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfDim &, const size_t,
	  const tag::OrientOpposToMesh &, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfDim &, const size_t, const tag::AsFound &,
	  const tag::RequireOrder &, const tag::ThisMeshIsPositive &        );
	Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfDim &, const size_t, const tag::ForcePositive &,
	  const tag::RequireOrder &, const tag::ThisMeshIsPositive &            );
	Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfDim &, const size_t,
	  const tag::RequireOrder &, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfDim &, const size_t, const tag::OrientCompWithMesh &,
	  const tag::RequireOrder &, const tag::ThisMeshIsPositive &                 );
	Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfDim &, const size_t, const tag::OrientOpposToMesh &,
	  const tag::RequireOrder &, const tag::ThisMeshIsPositive &                );
	Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfDim &, const size_t, const tag::AsFound &,
	  const tag::Backwards &, const tag::ThisMeshIsPositive &        );
	Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfDim &, const size_t, const tag::ForcePositive &,
	  const tag::Backwards &, const tag::ThisMeshIsPositive &            );
	Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfDim &, const size_t,
	  const tag::Backwards &, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfDim &, const size_t, const tag::OrientCompWithMesh &,
	  const tag::Backwards &, const tag::ThisMeshIsPositive &                 );
	Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfDim &, const size_t, const tag::OrientOpposToMesh &,
	  const tag::Backwards &, const tag::ThisMeshIsPositive &                );

	// we are still in class Mesh::Fuzzy
	
	Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfMaxDim &, const tag::AsFound &, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfMaxDim &, const tag::ForcePositive &, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfMaxDim &, const tag::OrientCompWithMesh &, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfMaxDim &, const tag::OrientOpposToMesh &, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfMaxDim &, const tag::AsFound &,
	  const tag::RequireOrder &, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfMaxDim &, const tag::ForcePositive &,
	  const tag::RequireOrder &, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfMaxDim &, const tag::OrientCompWithMesh &,
	  const tag::RequireOrder &, const tag::ThisMeshIsPositive &      );
	Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfMaxDim &, const tag::OrientOpposToMesh &,
	  const tag::RequireOrder &, const tag::ThisMeshIsPositive &   );
	Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfMaxDim &, const tag::AsFound &,
	  const tag::Backwards &, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfMaxDim &, const tag::ForcePositive &,
	  const tag::Backwards &, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfMaxDim &, const tag::OrientCompWithMesh &,
	  const tag::Backwards &, const tag::ThisMeshIsPositive &      );
	Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfMaxDim &, const tag::OrientOpposToMesh &,
	  const tag::Backwards &, const tag::ThisMeshIsPositive &     );
	
	// we are still in class Mesh::Fuzzy

	Mesh::Iterator::Core * iterator  // execution forbidden
	( const tag::OverVertices &, const tag::ConnectedTo &, Cell::Positive::Vertex *,
	  const tag::ThisMeshIsPositive &                                               );
	Mesh::Iterator::Core * iterator  // execution forbidden
	( const tag::OverVertices &, const tag::ConnectedTo &, Cell::Positive::Vertex *,
	  const tag::RequireOrder &, const tag::ThisMeshIsPositive &                    );

	Mesh::Iterator::Core * iterator
	( const tag::OverSegments &, const tag::WhoseTipIs &, Cell::Positive::Vertex * V,
	  const tag::ThisMeshIsPositive &                                                );
	Mesh::Iterator::Core * iterator
	( const tag::OverSegments &, const tag::WhoseTipIs &, Cell::Positive::Vertex * V,
	  const tag::RequireOrder &, const tag::ThisMeshIsPositive &                     );
	Mesh::Iterator::Core * iterator
	( const tag::OverSegments &, const tag::WhoseTipIs &, Cell::Positive::Vertex * V,
	  const tag::Backwards &, const tag::ThisMeshIsPositive &                        );

	Mesh::Iterator::Core * iterator
	( const tag::OverSegments &, const tag::WhoseBaseIs &, Cell::Positive::Vertex * V,
	  const tag::ThisMeshIsPositive &                                                 );
	Mesh::Iterator::Core * iterator
	( const tag::OverSegments &, const tag::WhoseBaseIs &, Cell::Positive::Vertex * V,
	  const tag::RequireOrder &, const tag::ThisMeshIsPositive &                      );
	Mesh::Iterator::Core * iterator
	( const tag::OverSegments &, const tag::WhoseBaseIs &, Cell::Positive::Vertex * V,
	  const tag::Backwards &, const tag::ThisMeshIsPositive &                         );

	Mesh::Iterator::Core * iterator  // execution forbidden
	( const tag::OverSegments &, const tag::AsFound &, const tag::Around &, Cell::Positive::Vertex *,
	  const tag::ThisMeshIsPositive &                                                                );
	Mesh::Iterator::Core * iterator  // execution forbidden
	( const tag::OverSegments &, const tag::AsFound &, const tag::Around &, Cell::Positive::Vertex *,
	  const tag::RequireOrder &, const tag::ThisMeshIsPositive &                                     );
	Mesh::Iterator::Core * iterator  // execution forbidden
	( const tag::OverSegments &, const tag::AsFound &, const tag::Around &, Cell::Positive::Vertex *,
	  const tag::Backwards &, const tag::ThisMeshIsPositive &                                     );
	Mesh::Iterator::Core * iterator  // execution forbidden
	( const tag::OverSegments &, const tag::ForcePositive &,
	  const tag::Around &, Cell::Positive::Vertex *, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator  // execution forbidden
	( const tag::OverSegments &, const tag::ForcePositive &,
	  const tag::Around &, Cell::Positive::Vertex *,
	  const tag::RequireOrder &, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator  // execution forbidden
	( const tag::OverSegments &, const tag::ForcePositive &,
	  const tag::Around &, Cell::Positive::Vertex *,
	  const tag::Backwards &, const tag::ThisMeshIsPositive & );

	// we are still in class Mesh::Fuzzy

	Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfDim &, const size_t, const tag::AsFound &,
	  const tag::Around &, Cell::Core *, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfDim &, const size_t, const tag::AsFound &,
	  const tag::Around &, Cell::Core *,
	  const tag::RequireOrder &, const tag::ThisMeshIsPositive &      );
	Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfDim &, const size_t, const tag::AsFound &,
	  const tag::Around &, Cell::Core *,
	  const tag::Backwards &, const tag::ThisMeshIsPositive &      );
	Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfDim &, const size_t, const tag::ForcePositive &,
	  const tag::Around &, Cell::Core *, const tag::ThisMeshIsPositive &    );
	Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfDim &, const size_t, const tag::ForcePositive &,
	  const tag::Around &, Cell::Core *,
	  const tag::RequireOrder &, const tag::ThisMeshIsPositive &            );
	Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfDim &, const size_t, const tag::ForcePositive &,
	  const tag::Around &, Cell::Core *,
	  const tag::Backwards &, const tag::ThisMeshIsPositive &            );
	Mesh::Iterator::Core * iterator  // execution forbidden
	( const tag::OverCellsOfDim &, const size_t, const tag::OrientCompWithCenter &,
	  const tag::Around &, Cell::Core *, const tag::ThisMeshIsPositive &           );
	Mesh::Iterator::Core * iterator  // execution forbidden
	( const tag::OverCellsOfDim &, const size_t, const tag::OrientOpposToCenter &,
	  const tag::Around &, Cell::Core *, const tag::ThisMeshIsPositive &          );
	/*	Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfDim &, const size_t, const tag::OrientOpposToMesh &,
	  const tag::Around &, Cell::Core *, const tag::ThisMeshIsPositive &      );
	Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfDim &, const size_t, const tag::OrientOpposToMesh &,
	  const tag::Around &, Cell::Core *,
	  const tag::RequireOrder &, const tag::ThisMeshIsPositive &              );
	Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfDim &, const size_t, const tag::OrientOpposToMesh &,
	  const tag::Around &, Cell::Core *,
	  const tag::Backwards &, const tag::ThisMeshIsPositive &              );  */

	// we are still in class Mesh:Fuzzy

	Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfMaxDim &, const tag::OrientCompWithMesh &,
	  const tag::Around &, Cell::Core *, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfMaxDim &, const tag::OrientCompWithMesh &,
	  const tag::Around &, Cell::Core *, const tag::RequireOrder &, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfMaxDim &, const tag::OrientCompWithMesh &,
	  const tag::Around &, Cell::Core *, const tag::Backwards &, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfMaxDim &, const tag::ForcePositive &,
	  const tag::Around &, Cell::Core *, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfMaxDim &, const tag::ForcePositive &,
	  const tag::Around &, Cell::Core *,
	  const tag::RequireOrder &, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfMaxDim &, const tag::ForcePositive &,
	  const tag::Around &, Cell::Core *,
	  const tag::Backwards &, const tag::ThisMeshIsPositive &    );
	Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfMaxDim &, const tag::OrientOpposToMesh &,
	  const tag::Around &, Cell::Core *, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfMaxDim &, const tag::OrientOpposToMesh &,
	  const tag::Around &, Cell::Core *, const tag::RequireOrder &, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfMaxDim &, const tag::OrientOpposToMesh &,
	  const tag::Around &, Cell::Core *, const tag::Backwards &, const tag::ThisMeshIsPositive & ); 
	Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfMaxDim &, const tag::OrientCompWithCenter &,
	  const tag::Around &, Cell::Core *, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfMaxDim &, const tag::OrientOpposToCenter &,
	  const tag::Around &, Cell::Core *, const tag::ThisMeshIsPositive & );
	Mesh::Iterator::Core * iterator
	( const tag::OverCellsOfMaxDim &, const tag::AsFound &,
	  const tag::Around &, Cell::Core *, const tag::ThisMeshIsPositive & );
	
	// iterator with tag::orientation_compatible_with_both_center_and_mesh
	// defined in Mesh::NotZeroDim
	
	#ifndef NDEBUG  // DEBUG mode
	// attribute  name  inherited from Mesh::Core
	// std::string get_name()  defined by Mesh::Core
	void print_everything ();  // virtual from Mesh::Core
	#endif  // DEBUG
	
}; // end of  class Mesh::Fuzzy

//------------------------------------------------------------------------------------------------------//


class Mesh::STSI : public Mesh::Fuzzy

// represents a positive mesh, maybe self-touching, maybe self-intersecting
// (includes 1D meshes)

// mainly used for iterators over connected high-dim meshes
// also used for frontal mesh generation
// see paragraph 13.16 in the manual

{	public :
	
	// size_t nb_of_wrappers  inherited from tag::Util::Core ifdef MANIFEM_COLLECT_CM

	// Cell::Positive * cell_enclosed  inherited from Mesh::Core
	// should be nullptr since no boundary should be Mesh::STSI

	// std::vector < std::list < Cell > > cells  inherited from Mesh::Fuzzy

	// in 'singular_behind' we keep pairs of adjacent cells
  // singular_behind [ face ] .size() == singular_behind [ face .reverse() ] .size()
  // pairs are formed by viewing the two lists side by side :
  //    [ singular_behind [ face ] [i], singular_behind [ face .reverse() ] [i] ]
 	// 'face' is singular, that is, a face where the mesh touches itself
	// for such a common face,  face->cell_behind_within  should not have key 'this'
	// (in spite of 'face.positive' belonging to this->cells[d-1])
	std::map < Cell, std::list < Cell > > singular_behind;
	
	inline STSI ( const tag::OfDimension &, const size_t dim_p1, const tag::MinusOne &,
                const tag::OneDummyWrapper &                                         )
	:	Mesh::Fuzzy ( tag::of_dimension, dim_p1, tag::minus_one, tag::one_dummy_wrapper )
	{	}

	// this destructor only works correctly if 'this' mesh is in a simple state
	// (similar to a Mesh::Fuzzy, no singular faces)
	// we might experience errors especially with the -DMANIFEM_COLLECT_CM compilation flag
	// see parag-test-error-stsi.cpp
	// in practice everything goes smoothly because, in frontal.cpp, the interface is always left empty
	virtual ~STSI ();

	// bool dispose_query ( )  defined by tag::Util::Core ifdef MANIFEM_COLLECT_CM
	
	// size_t get_dim_plus_one ( )  defined in Mesh::Fuzzy
	// four versions of  size_t number_of  defined in Mesh::Fuzzy

	// first_vertex, last_vertex, first_segment and last_segment
	// are defined by Mesh::Core, execution forbidden

	// private :

	// four methods below are virtual from and defined by Mesh::Core, here overridden
	Cell cell_in_front_of ( const Cell & face, const tag::SeenFrom &, const Cell & neighbour,
	                        const tag::SurelySingular &, const tag::SurelyExists &           ) override;
	Cell cell_in_front_of ( const Cell & face, const tag::SeenFrom &, const Cell & neighbour,
	                        const tag::SurelySingular &, const tag::MayNotExist &            ) override;
	Cell cell_behind ( const Cell & face, const tag::SeenFrom &, const Cell & neighbour,
                     const tag::SurelySingular &, const tag::SurelyExists &           ) override;
	Cell cell_behind ( const Cell & face, const tag::SeenFrom &, const Cell & neighbour,
	                   const tag::SurelySingular &, const tag::MayNotExist &            ) override;
	
	// the twenty-eight methods below are virtual from Mesh::Core
	// many are defined by Mesh::NotZeroDim, here overridden
	// methods with tag::force and with tag::neighbourhood_relations
	//    are defined by Mesh::Core, here overridden
	// methods with tag::mesh_is_bdry are execution forbidden since a boundary should never be STSI
	// called from Cell::****tive::***::add_to_mesh and Cell::****tive::***::remove_from_mesh
	void add_pos_seg ( Cell::Positive::Segment *, const tag::MeshIsNotBdry & ) override;
	void add_pos_seg ( Cell::Positive::Segment *, const tag::MeshIsBdry & ) override;
	void add_pos_seg
	( Cell::Positive::Segment *, const tag::MeshIsBdry &, const tag::DoNotBother & ) override;
	void add_pos_seg
	( Cell::Positive::Segment *, const tag::MeshIsNotBdry &, const tag::Force & ) override;
	void remove_pos_seg ( Cell::Positive::Segment *, const tag::MeshIsNotBdry & ) override;
	void remove_pos_seg ( Cell::Positive::Segment *, const tag::MeshIsBdry & ) override;
	void remove_pos_seg
	( Cell::Positive::Segment *, const tag::MeshIsBdry &, const tag::DoNotBother & ) override;
	void add_neg_seg ( Cell::Negative::Segment *, const tag::MeshIsNotBdry & ) override;
	void add_neg_seg ( Cell::Negative::Segment *, const tag::MeshIsBdry & ) override;
	void add_neg_seg
	( Cell::Negative::Segment *, const tag::MeshIsBdry &, const tag::DoNotBother & ) override;
	void add_neg_seg
	( Cell::Negative::Segment *, const tag::MeshIsNotBdry &, const tag::Force & ) override;
	void remove_neg_seg ( Cell::Negative::Segment *, const tag::MeshIsNotBdry & ) override;
	void remove_neg_seg ( Cell::Negative::Segment *, const tag::MeshIsBdry & ) override;
	void remove_neg_seg
	( Cell::Negative::Segment *, const tag::MeshIsBdry &, const tag::DoNotBother & ) override;
	void add_pos_hd_cell ( Cell::Positive::HighDim *, const tag::MeshIsNotBdry & ) override;
	void add_pos_hd_cell ( Cell::Positive::HighDim *, const tag::MeshIsBdry & ) override;
	void add_pos_hd_cell ( Cell::Positive::HighDim *, const tag::MeshIsNotBdry &,
	                       const tag::Force &                                    ) override;
	void add_pos_hd_cell
	( Cell::Positive::HighDim *, const tag::MeshIsNotBdry &,
	  const tag::NeighbRels &, const std::map < Cell, Cell > & rels, bool force_other_faces ) override;
	void add_pos_hd_cell
	( Cell::Positive::HighDim *, const tag::MeshIsBdry &, const tag::DoNotBother & ) override;
	void remove_pos_hd_cell ( Cell::Positive::HighDim *, const tag::MeshIsNotBdry & ) override;
	void remove_pos_hd_cell ( Cell::Positive::HighDim *, const tag::MeshIsBdry & ) override;
	void remove_pos_hd_cell
	( Cell::Positive::HighDim *, const tag::MeshIsBdry &, const tag::DoNotBother & ) override;
	void add_neg_hd_cell ( Cell::Negative::HighDim *, const tag::MeshIsNotBdry & ) override;
	void add_neg_hd_cell ( Cell::Negative::HighDim *, const tag::MeshIsBdry & ) override;
	void add_neg_hd_cell ( Cell::Negative::HighDim *, const tag::MeshIsNotBdry &,
	                       const tag::Force &                                    ) override;
	void add_neg_hd_cell
	( Cell::Negative::HighDim *, const tag::MeshIsNotBdry &,
	  const tag::NeighbRels &, const std::map < Cell, Cell > & rels, bool force_other_faces ) override;
	void add_neg_hd_cell
	( Cell::Negative::HighDim *, const tag::MeshIsBdry &, const tag::DoNotBother & ) override;
	void remove_neg_hd_cell ( Cell::Negative::HighDim *, const tag::MeshIsNotBdry & ) override;
	void remove_neg_hd_cell ( Cell::Negative::HighDim *, const tag::MeshIsBdry & ) override;
	void remove_neg_hd_cell
	( Cell::Negative::HighDim *, const tag::MeshIsBdry &, const tag::DoNotBother & ) override;

	// the eight methods below are virtual from Mesh::Core, defined by Mesh::NotZeroDim
	// thus, calls with tag::do_not_bother act on a STSI mesh as if it were a Fuzzy mesh
	// called from Cell::****tive::***::add_to_mesh and Cell::****tive::***::remove_from_mesh
	// void add_pos_seg
	// ( Cell::Positive::Segment *, const tag::MeshIsNotBdry &, const tag::DoNotBother & )
	// void remove_pos_seg
	// ( Cell::Positive::Segment *, const tag::MeshIsNotBdry &, const tag::DoNotBother & )
	// void add_neg_seg
	// ( Cell::Negative::Segment *, const tag::MeshIsNotBdry &, const tag::DoNotBother & )
	// void remove_neg_seg
	// ( Cell::Negative::Segment *, const tag::MeshIsNotBdry &, const tag::DoNotBother & )
	// void add_pos_hd_cell
	// ( Cell::Positive::HighDim *, const tag::MeshIsNotBdry &, const tag::DoNotBother & )
	// void remove_pos_hd_cell
	// ( Cell::Positive::HighDim *, const tag::MeshIsNotBdry &, const tag::DoNotBother & )
	// void add_neg_hd_cell
	// ( Cell::Negative::HighDim *, const tag::MeshIsNotBdry &, const tag::DoNotBother & )
	// void remove_neg_hd_cell
	// ( Cell::Negative::HighDim *, const tag::MeshIsNotBdry &, const tag::DoNotBother & )

	// iterators are virtual from Mesh::Core and are defined by Mesh::Fuzzy

}; // end of  class Mesh::STSI

//------------------------------------------------------------------------------------------------------//
//------------------------------------------------------------------------------------------------------//


inline void Mesh::set_max_dim ( const size_t d )  // static
// see paragraph 12.7 in the manual
{	Mesh::maximum_dimension_plus_one = d + 1;
	Cell::Positive::double_heap_size    .resize ( maximum_dimension_plus_one, 0 );
	Cell::Negative::double_heap_size    .resize ( maximum_dimension_plus_one, 0 );
	Cell::Positive::size_t_heap_size    .resize ( maximum_dimension_plus_one, 0 );
	Cell::Negative::size_t_heap_size    .resize ( maximum_dimension_plus_one, 0 );
	Cell::Positive::short_int_heap_size .resize ( maximum_dimension_plus_one, 0 );
	Cell::Negative::short_int_heap_size .resize ( maximum_dimension_plus_one, 0 );  }

//------------------------------------------------------------------------------------------------------//


inline tag::Util::CellCore::CellCore
( const tag::OfDimension &, const size_t d,  // for positive cells
  const tag::HasNoReverse &, const tag::OneDummyWrapper & )
#ifdef MANIFEM_COLLECT_CM	
:	tag::Util::Core::DelegateDispose
	( & tag::Util::Core::default_dispose_query, tag::one_dummy_wrapper ),
#else  // no MANIFEM_COLLECT_CM
:	tag::Util::Core::Inactive ( tag::one_dummy_wrapper ),
#endif  // MANIFEM_COLLECT_CM	
	double_heap ( Cell::Positive::double_heap_size [d] ),
	size_t_heap ( Cell::Positive::size_t_heap_size [d] ),
	short_int_heap ( Cell::Positive::short_int_heap_size [d], 0 ),
	reverse_attr ( tag::non_existent )
{	// std::cout << "positive Cell::Core constructor line 6159, counter " << Cell::counter;
	// Cell::counter++;
	// std::cout << "->" << Cell::counter << std::endl;
	std::vector < void(*)(Cell::Core*,void*) > & init = Cell::init_pos_cell [d];
	std::vector < void* > & data = Cell::data_for_init_pos [d];
	std::vector < void(*)(Cell::Core*,void*) > ::iterator it_f = init .begin();
	std::vector < void* > ::iterator it_d = data .begin();
	for ( ; it_f != init .end(); it_f++, it_d++ )
	{	assert ( it_d != data .end() );
		(*it_f) ( this, *it_d );         }
	assert ( it_d == data .end() );                                               }


inline tag::Util::CellCore::CellCore
( const tag::OfDimension &, const size_t d,  // for negative cells
  const tag::ReverseOf &, Cell::Core * direct_cell_p, const tag::OneDummyWrapper & )
#ifdef MANIFEM_COLLECT_CM	
:	tag::Util::Core::DelegateDispose
	( & tag::Util::Core::dispose_query_cell_with_reverse, tag::one_dummy_wrapper ),
#else  // no MANIFEM_COLLECT_CM
:	tag::Util::Core::Inactive ( tag::one_dummy_wrapper ),
#endif  // MANIFEM_COLLECT_CM	
	double_heap ( Cell::Negative::double_heap_size [d] ),
	size_t_heap ( Cell::Negative::size_t_heap_size [d] ),
	short_int_heap ( Cell::Negative::short_int_heap_size [d], 0 ),
	reverse_attr ( tag::whose_core_is, direct_cell_p, tag::previously_existing, tag::surely_not_null )
{	// std::cout << "negative Cell::Core constructor line 6185, counter " << Cell::counter;
	// Cell::counter++;
	// std::cout << "->" << Cell::counter << std::endl;
	std::vector < void(*)(Cell::Core*,void*) > & init = Cell::init_neg_cell [d];
	std::vector < void* > & data = Cell::data_for_init_neg [d];
	std::vector < void(*)(Cell::Core*,void*) > ::iterator it_f = init .begin();
	std::vector<void*>::iterator it_d = data .begin();
	for ( ; it_f != init .end(); it_f++, it_d++ )
	{	assert ( it_d != data .end() );
		( *it_f ) ( this, *it_d );       }
	assert ( it_d == data .end() );                                               }

//------------------------------------------------------------------------------------------------------//


inline Cell & Cell::operator= ( const Cell & c )
{
	#ifdef MANIFEM_COLLECT_CM	
	this->dispose_core ( tag::may_be_null );  // old core may be null
	#endif
	this->core = c .core;
	#ifdef MANIFEM_COLLECT_CM	
	if ( c .core )  c .core->nb_of_wrappers ++;
	#endif
	return *this;                                }
		

inline Cell & Cell::operator= ( Cell && c )
{
	#ifdef MANIFEM_COLLECT_CM	
	this->dispose_core ( tag::may_be_null );  // old core may be null
	#endif
	this->core = c .core;
	c .core = nullptr;
	return *this;                             }

	
inline Mesh & Mesh::operator= ( const Mesh & c )
{
	#ifdef MANIFEM_COLLECT_CM	
	this->dispose_core ( tag::may_be_null );  // old core may be null
	#endif
	this->core = c .core;
	this->is_pos = c .is_pos;
	#ifdef MANIFEM_COLLECT_CM	
	if ( c .core ) c .core->nb_of_wrappers ++;
	#endif
	return *this;                               }


inline Mesh & Mesh::operator= ( Mesh && c )
{
	#ifdef MANIFEM_COLLECT_CM	
	this->dispose_core ( tag::may_be_null );  // old core may be null
	#endif
	this->core = c .core;
	this->is_pos = c .is_pos;
	c .core = nullptr;
	return *this;                            }
	
//------------------------------------------------------------------------------------------------------//


inline Mesh::Mesh ( const tag::NonExistent & )
#ifdef MANIFEM_COLLECT_CM	
:	tag::Util::Wrapper < Mesh::Core > ( tag::empty ),
#else  // no MANIFEM_COLLECT_CM	
:	tag::Util::Wrapper < Mesh::Core > ::Inactive ( tag::empty ),
#endif  // MANIFEM_COLLECT_CM	
	is_pos { & tag::Util::return_bool_forbidden }
{ }


inline Mesh::Mesh ( const tag::WhoseCoreIs &, Mesh::Core * msh, const tag::Move &,
                    const tag::IsPositive & ispos                                 )
// 'ispos' defaults to tag::is_positive
#ifdef MANIFEM_COLLECT_CM	
:	tag::Util::Wrapper < Mesh::Core > ( tag::whose_core_is, msh, tag::move ),
#else  // no MANIFEM_COLLECT_CM	
:	tag::Util::Wrapper < Mesh::Core > ::Inactive ( msh ),
#endif  // MANIFEM_COLLECT_CM	
	is_pos { & tag::Util::return_true }
{ }
	

inline Mesh::Mesh ( const tag::WhoseCoreIs &, Mesh::Core * msh, const tag::Move &,
                    const tag::BuildNegative &, const tag::ReverseCellsSurelyExist & )
#ifdef MANIFEM_COLLECT_CM	
:	tag::Util::Wrapper < Mesh::Core > ( tag::whose_core_is, msh, tag::move ),
#else  // no MANIFEM_COLLECT_CM	
:	tag::Util::Wrapper < Mesh::Core > ::Inactive ( msh ),
#endif  // MANIFEM_COLLECT_CM	
	is_pos { & tag::Util::return_false }
#ifndef NDEBUG  // DEBUG mode
{	assert ( msh );
	// check that all cells have reverse
	Mesh::Iterator it = this->iterator  // as they are : oriented
		( tag::over_cells_of_max_dim, tag::as_found );
	for ( it .reset() ; it .in_range(); it++ )
		assert ( ( *it ) .reverse ( tag::may_not_exist ) .exists() );  }
#else  // NDEBUG
{	}
#endif
	

inline Mesh::Mesh ( const tag::WhoseCoreIs &, Mesh::Core * msh, const tag::PreviouslyExisting &,
                    const tag::IsPositive & ispos                                               )
// 'ispos' defaults to tag::is_positive
// msh is either pre-existent or is freshly created with tag::zero_wrappers
#ifdef MANIFEM_COLLECT_CM	
: tag::Util::Wrapper < Mesh::Core > ( tag::whose_core_is, msh,
                                      tag::previously_existing, tag::surely_not_null ),
#else  // no MANIFEM_COLLECT_CM	
:	tag::Util::Wrapper < Mesh::Core > ::Inactive ( msh ),
#endif  // MANIFEM_COLLECT_CM	
	is_pos { & tag::Util::return_true }
{	}
	

inline Mesh::Mesh ( const tag::WhoseCoreIs &, Mesh::Core * msh,
                    const tag::PreviouslyExisting &, const tag::MayBeNull & )
// 'ispos' defaults to tag::is_positive
// msh is either pre-existent or is freshly created with tag::zero_wrappers
#ifdef MANIFEM_COLLECT_CM	
:	tag::Util::Wrapper < Mesh::Core > ( tag::whose_core_is, msh,
	                                    tag::previously_existing, tag::may_be_null )
#else  // no MANIFEM_COLLECT_CM	
:	tag::Util::Wrapper < Mesh::Core > ::Inactive ( msh )
#endif  // MANIFEM_COLLECT_CM	
{	}
	

inline Mesh::Mesh ( const tag::FromPositiveMesh &, const tag::WhoseCoreIs &, Mesh::Core * msh,
                    const tag::BuildNegative &, const tag::DoNotBuildCells &                  )
// build a negative mesh from a positive one
// without worrying whether reverse cells exist or not

#ifdef MANIFEM_COLLECT_CM	
:	tag::Util::Wrapper < Mesh::Core > ( tag::whose_core_is, msh,
                                       tag::previously_existing, tag::surely_not_null ),
#else  // no MANIFEM_COLLECT_CM	
:	tag::Util::Wrapper < Mesh::Core > ::Inactive ( msh ),
#endif  // MANIFEM_COLLECT_CM	
	is_pos { & tag::Util::return_false }

{	}


inline Mesh::Mesh ( const tag::FromPositiveMesh &, const tag::WhoseCoreIs &, Mesh::Core * msh,
                    const tag::BuildNegative &, const tag::ReverseCellsSurelyExist &          )
// builds a negative mesh from a positive one, assuming that reverse cells exist
// used in Cell::boundary and in Mesh::Mesh below

:	Mesh ( tag::from_positive_mesh, tag::whose_core_is, msh,
	       tag::build_negative, tag::do_not_build_cells     )

#ifndef NDEBUG  // DEBUG mode
{	assert ( msh );
	// check that all cells have reverse
	Mesh::Iterator it = this->iterator  // as they are : oriented
		( tag::over_cells_of_max_dim, tag::as_found );
	for ( it .reset() ; it .in_range(); it++ )
		assert ( ( *it ) .reverse ( tag::may_not_exist ) .exists() );  }
#else  // NDEBUG
{	}
#endif


inline Mesh::Mesh ( const tag::FromPositiveMesh &, const tag::WhoseCoreIs &, Mesh::Core * msh,
                    const tag::BuildNegative &, const tag::BuildCellsIfNec &                  )
// builds a negative mesh from a positive one, creating reverse cells if necessary
// used in Mesh::Positive::reverse
	
:	Mesh ( tag::from_positive_mesh, tag::whose_core_is, msh,
	       tag::build_negative, tag::do_not_build_cells      )

{	Mesh::Iterator it ( tag::whose_core_is, msh->iterator  // as found : oriented
		( tag::over_cells_of_max_dim, tag::as_found, tag::this_mesh_is_positive ) );
	for ( it .reset() ; it .in_range(); it++ )
		( *it ) .reverse ( tag::build_if_not_exists );                                }


inline void Mesh::copy_all_cells_to ( Mesh & msh ) const
{	assert ( this->exists() );
	assert ( msh.exists() );
	Mesh::Iterator it = this->iterator ( tag::over_cells_of_max_dim );
	for ( it .reset(); it .in_range(); it ++ )
	{	Cell cll = *it;  cll .add_to ( msh );  }                         }


inline Mesh::Mesh ( const tag::Fuzzy &, const tag::OfDimension &, const size_t d,
                    const tag::IsPositive & ispos                                )
// by default, ispos = tag::is_positive, so may be called with only three arguments
:	Mesh ( tag::whose_core_is,
	       new Mesh::Fuzzy ( tag::of_dimension, d+1, tag::minus_one, tag::one_dummy_wrapper ),
	       tag::move, tag::is_positive                                                        )
{	}


inline Mesh::Mesh ( const tag::ConnectedOneDim &, const tag::IsPositive & ispos )
// by default, ispos = tag::is_positive, so may be called with only three arguments
:	Mesh ( tag::whose_core_is,
	       new Mesh::Fuzzy ( tag::of_dimension, 2, tag::minus_one, tag::one_dummy_wrapper ),
	       tag::move, tag::is_positive                                                        )
{	}   // all inner details must be set by calling code : nb_os_segs, fist_ver, last_ver


inline Mesh::Mesh ( const tag::Fuzzy &, const tag::OfDimension &, const size_t d,
                    const tag::MinusOne &, const tag::IsPositive & ispos          )
// by default, ispos = tag::is_positive, so may be called with only four arguments
:	Mesh ( tag::whose_core_is,
	       new Mesh::Fuzzy ( tag::of_dimension, d, tag::minus_one, tag::one_dummy_wrapper ),
	       tag::move, tag::is_positive                                                      )
{	}
	

inline Mesh::Mesh ( const tag::stsi &, const tag::OfDimension &, const size_t d,
                    const tag::IsPositive & ispos                               )
// by default, ispos = tag::is_positive, so may be called with only three arguments
:	Mesh ( tag::whose_core_is,
	       new Mesh::STSI ( tag::of_dimension, d+1, tag::minus_one, tag::one_dummy_wrapper ),
	       tag::move, tag::is_positive                                                       )
{	}


inline Mesh Mesh::clone ( const tag::Depth &, size_t dep )
{	assert ( this->is_positive() );
	return Mesh ( tag::whose_core_is,
	              this->core->clone ( tag::depth, dep, tag::one_dummy_wrapper ),
	              tag::move, tag::is_positive                                   );  }


inline Mesh Mesh::clone ( const tag::Depth &, const tag::Infinity & )
{	assert ( this->is_positive() );
	return Mesh ( tag::whose_core_is,
	              this->core->clone ( tag::depth, tag::infinity, tag::one_dummy_wrapper ),
	              tag::move, tag::is_positive                                             );  }


inline Mesh Mesh::clone ( const tag::MakeFuzzy &, const tag::Depth &, size_t dep )
{	assert ( this->is_positive() );
	return Mesh ( tag::whose_core_is,
	              this->core->clone ( tag::make_fuzzy, tag::depth, dep, tag::one_dummy_wrapper ),
	              tag::move, tag::is_positive                                                    );  }


inline Mesh Mesh::clone ( const tag::MakeFuzzy &, const tag::Depth &, const tag::Infinity & )
{	assert ( this->is_positive() );
	return Mesh ( tag::whose_core_is,
	              this->core->clone ( tag::make_fuzzy, tag::depth, tag::infinity,
	                                  tag::one_dummy_wrapper                     ),
	              tag::move, tag::is_positive                                      );  }


inline Mesh Mesh::clone
( const tag::MakeConnectedOneDim &, const tag::SurelyExists &, const tag::Depth &, size_t dep )
{	assert ( this->is_positive() );
	return Mesh ( tag::whose_core_is,
	              this->core->clone ( tag::make_connected_one_dim, tag::depth, dep,
	                                  tag::one_dummy_wrapper                       ),
	              tag::move, tag::is_positive                                        );  }


inline Mesh Mesh::clone
( const tag::MakeConnectedOneDim &, const tag::SurelyExists &, const tag::Depth &, const tag::Infinity & )
{	assert ( this->is_positive() );
	return Mesh ( tag::whose_core_is,
	              this->core->clone ( tag::make_connected_one_dim, tag::depth, tag::infinity,
	                                  tag::one_dummy_wrapper                                 ),
	              tag::move, tag::is_positive                                                  );  }


inline Mesh Mesh::clone ( const tag::ReverseEachCell &, const tag::Depth &, size_t dep )
{	if ( dep != 0 )
	{	std::cout << __FILE__ << ":" << __LINE__ << ": "
					 << __extension__ __PRETTY_FUNCTION__ << ": ";
		std::cout << "cloning with reverse cells requires zero depth" << std::endl;
		exit ( 1 );                                                                  }
	assert ( this->is_positive() );
	return Mesh ( tag::whose_core_is,
	              this->core->clone ( tag::reverse_each_cell, tag::one_dummy_wrapper ),
	              tag::move, tag::is_positive                                          );  }

//------------------------------------------------------------------------------------------------------//


template < typename container >  // static
inline size_t Mesh::join ( Mesh & result, const container & l )

{	// check the dimensions
	#ifndef NDEBUG  // DEBUG mode
	typename container::const_iterator it0 = l .begin();
	assert ( it0 != l .end() );
	for ( ; it0 != l .end(); it0++ ) assert ( result .dim() == it0->dim() );
	#endif  // DEBUG
	size_t n = 0;
	// sweep over the list of meshes	
	typename container::const_iterator it = l .begin();
	for ( ; it != l .end(); it++ )
	{	Mesh m = *it;  // sweep all cells of m
		n += m .number_of ( tag::cells_of_max_dim );
		Mesh::Iterator itt = m .iterator
			( tag::over_cells_of_max_dim, tag::orientation_compatible_with_mesh );
		for ( itt .reset(); itt .in_range(); itt++ )
		{	Cell cll = *itt;
			cll .add_to ( result, tag::do_not_bother );  }                          }
	return n;                                                                        }


template < typename container >  // static
inline void Mesh::join_meshes ( Mesh & result, const container & l )

// if any of the meshes is not a Mesh::Connected::OneDim, 'this' will be Mesh::Fuzzy

{	container ll = l;
	std::deque < Mesh > d;
	typename container::iterator it = ll .begin();
	assert ( it != ll .end() );
	Mesh m = *it;
	Mesh::Connected::OneDim * mm = dynamic_cast < Mesh::Connected::OneDim* > ( m .core );
	if ( mm == nullptr )  goto fuzzy;
	d .push_back (m);  ll .erase ( it );

 	again :
	for ( it = ll .begin(); it != ll .end(); it++ )
	{	m = *it;
		mm = dynamic_cast < Mesh::Connected::OneDim* > ( m .core );
		if ( mm == nullptr )  goto fuzzy;
		Mesh m_front = d .front();
		if ( m_front .first_vertex() == m .last_vertex() )
		{  d .push_front ( m );  ll .erase ( it );  goto again;  }
		Mesh m_back = d .back();
		if ( m_back .last_vertex() == m .first_vertex() )
		{  d .push_back (m);  ll .erase ( it );  goto again;  }    }
	
	// if some meshes have not moved to d (are still in ll), the mesh will be disconnected
	if ( ll .empty() )  // build connected 1D mesh
	{	mm = new Mesh::Connected::OneDim ( tag::one_dummy_wrapper );
		result .core = mm;
		result .is_pos = & tag::Util::return_true;
		mm->nb_of_segs = Mesh::join ( result, l );
		mm->first_ver = d .front() .first_vertex();
		mm->last_ver = d .back() .last_vertex();
		return;                                                       }

	// ends do not match, or one of the meshes is fuzzy, so build fuzzy mesh
	fuzzy :
	result .core = new Mesh::Fuzzy ( tag::of_dim, l .front() .core->get_dim_plus_one(),
	                                 tag::minus_one, tag::one_dummy_wrapper             );
	result .is_pos = & tag::Util::return_true;
	Mesh::join ( result, l );
	return;                                                                                 }

//------------------------------------------------------------------------------------------------------//


inline Cell::Cell ( const tag::WhoseBoundaryIs &, Mesh & msh )
:	Cell ( tag::whose_core_is,
	       new Cell::Positive::HighDim ( tag::whose_boundary_is, msh, tag::one_dummy_wrapper ),
	       tag::move                                                                           )

#ifndef NDEBUG  // DEBUG mode
{	assert ( msh .is_positive() );
	assert ( msh .dim() >= 1 );
	Mesh::STSI * msh_stsi = dynamic_cast < Mesh::STSI* > ( msh .core );
	assert ( msh_stsi == nullptr );                                     }
#else  // NDEBUG
{	}
#endif


inline Cell::Cell ( const tag::Vertex &, const tag::IsPositive & ispos )
// by default, ispos = tag::is_positive, so may be called with only one argument
:	Cell ( tag::whose_core_is, new Cell::Positive::Vertex ( tag::one_dummy_wrapper ),
	       tag::move                                                                 )
{	}


inline Cell::Cell ( const tag::Segment &, const Cell & A, const Cell & B )
: Cell ( tag::whose_core_is, new Cell::Positive::Segment ( A, B, tag::one_dummy_wrapper ),
	       tag::move                                                                        )
{	}


inline Cell::Cell ( const tag::Triangle &, const Cell & AB, const Cell & BC, const Cell & CA )
:	Cell ( tag::whose_core_is, new Cell::Positive::HighDim
	         ( tag::triangle, AB, BC, CA, tag::one_dummy_wrapper ),
	       tag::move                                               )
{	}


inline Cell::Cell ( const tag::Quadrangle &, const Cell & AB, const Cell & BC,
                                             const Cell & CD, const Cell & DA )
:	Cell ( tag::whose_core_is, new Cell::Positive::HighDim
	         ( tag::quadrangle, AB, BC, CD, DA, tag::one_dummy_wrapper ),
	       tag::move                                                     )
{	}


inline Cell::Cell ( const tag::Pentagon &, const Cell & AB, const Cell & BC,
                         const Cell & CD, const Cell & DE, const Cell & EA )
:	Cell ( tag::whose_core_is, new Cell::Positive::HighDim
	       ( tag::pentagon, AB, BC, CD, DE, EA, tag::one_dummy_wrapper ),
	       tag::move                                                     )
{	}


inline Cell::Cell ( const tag::Hexagon &, const Cell & AB, const Cell & BC,
                    const Cell & CD, const Cell & DE, const Cell & EF, const Cell & FA )
:	Cell ( tag::whose_core_is, new Cell::Positive::HighDim
	       ( tag::hexagon, AB, BC, CD, DE, EF, FA, tag::one_dummy_wrapper ),
	       tag::move                                                        )
{	}

inline Cell::Cell ( const tag::Tetrahedron &, const Cell & face_1, const Cell & face_2,
                                              const Cell & face_3, const Cell & face_4 )
// the order of the faces does not count, they will cling according to their sides
:	Cell ( tag::whose_core_is, new Cell::Positive::HighDim
				 ( tag::tetrahedron, face_1, face_2, face_3, face_4, tag::one_dummy_wrapper ),
	       tag::move                                                                       )
{	}

inline Cell::Cell ( const tag::Hexahedron &, const Cell & face_1, const Cell & face_2,
                                             const Cell & face_3, const Cell & face_4,
                                             const Cell & face_5, const Cell & face_6 )
// the order of the faces does not count, they will cling according to their sides
:	Cell ( tag::whose_core_is, new Cell::Positive::HighDim
           ( tag::hexahedron, face_1, face_2, face_3, face_4, face_5, face_6,
             tag::one_dummy_wrapper                                          ),
	       tag::move                                                            )
{	}


//------------------------------------------------------------------------------------------------------//


inline size_t Mesh::number_of ( const tag::Vertices & ) const
{	return this->number_of ( tag::cells_of_dim, 0 );  }

inline size_t Mesh::number_of ( const tag::Segments & ) const
{	return this->number_of ( tag::cells_of_dim, 1 );  }

inline size_t Mesh::number_of ( const tag::CellsOfDim &, const size_t d ) const
{	return this->core->number_of ( tag::cells_of_dim, d );  }

inline size_t Mesh::number_of ( const tag::CellsOfMaxDim & ) const
{	return this->core->number_of ( tag::cells_of_max_dim );  }


inline Cell Mesh::first_vertex ( ) const
{	assert ( this->dim() == 1 );
	if ( this->is_positive() ) return this->core->first_vertex();
	// else
	return this->core->last_vertex();                              }

inline Cell Mesh::last_vertex ( ) const
{	assert ( this->dim() == 1 );
	if ( this->is_positive() ) return this->core->last_vertex();
	// else
	return this->core->first_vertex();                            }


inline Cell Mesh::first_segment ( ) const
{	assert ( this->dim() == 1 );
	if ( this->is_positive() ) return this->core->first_segment();
	// else
	return this->core->last_segment() .reverse ( tag::surely_exists );  }

inline Cell Mesh::last_segment ( ) const
{	assert ( this->dim() == 1 );
	if ( this->is_positive() ) return this->core->last_segment();
	// else
	return this->core->first_segment() .reverse ( tag::surely_exists );        }


inline bool Cell::belongs_to
( const Mesh & msh, const tag::SameDim &, const tag::OrientCompWithMesh & ) const
// third argument defaults to tag::orientation_compatible_with_mesh,
// so method can be called with two arguments only

{	assert ( msh .exists() );
	if ( msh .is_positive() )
		return  this->core->belongs_to
			( msh .core, tag::same_dim, tag::orientation_compatible_with_mesh );
	// mesh is negative
	Cell rev_this = this->reverse ( tag::may_not_exist );
	if ( rev_this .exists() )
		return  rev_this .core->belongs_to
			( msh .core, tag::same_dim, tag::orientation_compatible_with_mesh );
	return false;                                                             }


inline bool Cell::belongs_to
( const Mesh & msh, const tag::SameDim &, const tag::NotOriented & ) const
{ return this->core->belongs_to ( msh.core, tag::same_dim, tag::not_oriented );  }


inline bool Cell::belongs_to
( const Mesh & msh, const tag::CellHasLowDim &, const tag::NotOriented & ) const
{ return this->core->belongs_to_ld ( msh.core, tag::cell_has_low_dim, tag::not_oriented );  }

	
inline bool Cell::belongs_to ( const Mesh & msh, const tag::OrientCompWithMesh & ) const
{ return this->belongs_to ( msh, tag::same_dim, tag::orientation_compatible_with_mesh );  }


inline bool Cell::belongs_to ( const Mesh & msh, const tag::NotOriented & ) const

{	assert ( msh .exists() );
	if ( this->dim() == msh .dim() )
		return this->core->belongs_to ( msh .core, tag::same_dim, tag::not_oriented );
	// else --
	assert ( this->dim() < msh .dim() );
	return this->core->belongs_to_ld ( msh .core, tag::cell_has_low_dim, tag::not_oriented );  }


inline bool Cell::belongs_to ( const Mesh & msh ) const

{	assert ( msh .exists() );
	if ( this->dim() >= msh .dim() )
		// when the dimensions are equal, we require a more specific call
	{	assert ( this->dim() == msh .dim() );
		std::cout << __FILE__ << ":" << __LINE__ << ": " << __extension__ __PRETTY_FUNCTION__ << ": ";
		std::cout << "Please provide tag::same_dim." << std::endl;
		exit ( 1 );                                                                                     }
	return this->core->belongs_to_ld ( msh .core, tag::cell_has_low_dim, tag::not_oriented );            }


inline bool Cell::belongs_to ( const Mesh & msh, const tag::OnBoundary & ) const

{ if ( not this->belongs_to ( msh, tag::cell_has_low_dim ) )  return false;
	if ( msh .cell_in_front_of ( * this, tag::may_not_exist ) .exists() )  return false;
	assert ( msh .cell_behind ( * this, tag::may_not_exist ) .exists() );
	return true;                                                                          }

//------------------------------------------------------------------------------------------------------//


inline Cell Mesh::cell_in_front_of ( const Cell & face, const tag::MayNotExist & ) const

// return the cell towards which 'face' is looking
// recall that the faces of a cell are looking outwards

{	assert ( face .exists() );
	Cell face_rev = face .reverse ( tag::may_not_exist );
	if ( not face_rev .exists() ) return  Cell ( tag::non_existent );
	return  this->cell_behind ( face_rev, tag::may_not_exist );        }
	

inline Cell Mesh::cell_in_front_of ( const Cell & face, const tag::SurelyExists & se ) const

// 'se' defaults to tag::surely_exists, so method may be called with only one argument

// return the cell towards which 'face' is looking
// recall that the faces of a cell are looking outwards

{	assert ( face .exists() );
	Cell face_rev = face .reverse ( tag::surely_exists );
	return  this->cell_behind ( face_rev, tag::surely_exists );  }
	

inline Cell Mesh::cell_behind ( const Cell & face, const tag::MayNotExist & ) const

// return the cell to which 'face' belongs, non-existent if we are on the boundary

{	assert ( face .exists() );
	assert ( this->dim() == face .dim() + 1 );
	if ( this->is_positive() )
	{	std::map < Mesh::Core*, Cell > ::const_iterator
			it = face .core->cell_behind_within .find ( this->core );
		if ( it == face .core->cell_behind_within .end() )  return  Cell ( tag::non_existent );
			// nothing behind us, we are on the boundary
		assert ( it->second .exists() );
		return  it->second;                                                                     }
		// face .core->cell_behind_within [ this->core ]
	else
	{	Cell face_rev = face .reverse ( tag::surely_exists );
		// we are in a negative mesh, all faces must have reverse
		std::map < Mesh::Core*, Cell >::const_iterator
			it = face_rev .core->cell_behind_within .find ( this->core );
		if ( it == face_rev .core->cell_behind_within .end() )
			return  Cell ( tag::non_existent );  // we are on the boundary
		Cell cll_rev = it->second;
		assert ( cll_rev .exists() );
		return  cll_rev .reverse ( tag::surely_exists );                  }                        }


inline Cell Mesh::cell_behind ( const Cell & face, const tag::SurelyExists & se ) const

// 'se' defaults to tag::surely_exists, so method may be called with only one argument

// return the cell to which 'face' belongs

{	assert ( face .exists() );
	assert ( this->dim() == face .dim() + 1 );
	if ( this->is_positive() )
	{	std::map < Mesh::Core*, Cell > ::const_iterator
			it = face .core->cell_behind_within .find ( this->core );
		assert ( it != face .core->cell_behind_within .end() );
		assert ( it->second .exists() );
		return  it->second;                                         }
		// face .core->cell_behind_within [ this->core ]
	else
	{	Cell face_rev = face .reverse ( tag::surely_exists );
		std::map <Mesh::Core*, Cell > ::const_iterator
			it = face_rev .core->cell_behind_within .find ( this->core );
		assert ( it != face_rev .core->cell_behind_within .end() );
		Cell cll_rev = it->second;
		assert ( cll_rev .exists() );
		return  cll_rev .reverse ( tag::surely_exists );                  }  }


inline Cell Mesh::cell_in_front_of
( const Cell & face, const tag::SeenFrom &, const Cell & neighbour, const tag::MayNotExist & ) const

// return the cell towards which 'face' is looking
// recall that the faces of a cell are looking outwards
// this function is interesting only for STSI meshes

{	assert ( face .exists() );
	assert ( neighbour .exists() );
	assert ( neighbour .belongs_to ( *this, tag::same_dim ) );
	assert ( face .belongs_to ( neighbour .boundary(), tag::same_dim ) );
	assert ( this->dim() == face .dim() + 1 );
	assert ( this->is_positive() );  // no negative STSI meshes
	Cell res = this->cell_in_front_of ( face, tag::may_not_exist );
	if ( res .exists() )  return res;
	// else : either the cell does not exist or we are at a singular face
	return  this->core->cell_in_front_of
		( face, tag::seen_from, neighbour, tag::surely_singular, tag::may_not_exist );  }
	

inline Cell Mesh::cell_in_front_of
( const Cell & face, const tag::SeenFrom &, const Cell & neighbour, const tag::SurelyExists & se ) const

// 'se' defaults to tag::surely_exists, so method may be called with only three arguments

// return the cell towards which 'face' is looking
// recall that the faces of a cell are looking outwards
// this function is interesting only for STSI meshes

{	assert ( face .exists() );
	assert ( neighbour .exists() );
	assert ( neighbour .belongs_to ( *this, tag::same_dim ) );
	assert ( face .belongs_to ( neighbour .boundary(), tag::same_dim ) );
	assert ( this->dim() == face .dim() + 1 );
	assert ( this->is_positive() );  // no negative STSI meshes
	Cell res = this->cell_in_front_of ( face, tag::may_not_exist );
	if ( res .exists() )  return res;  // we are at a singular face
	// else : either the cell does not exist or we are at a singular face
	return  this->core->cell_in_front_of
		( face, tag::seen_from, neighbour, tag::surely_singular, tag::surely_exists );  }
	

inline Cell Mesh::cell_behind
( const Cell & face, const tag::SeenFrom &, const Cell & neighbour, const tag::MayNotExist & ) const

// return the cell to which 'face' belongs, non-existent if we are on the boundary

{	assert ( face .exists() );
	assert ( neighbour .exists() );
	assert ( neighbour .belongs_to ( *this, tag::same_dim ) );
	assert ( face .reverse ( tag::surely_exists )
	         .belongs_to ( neighbour .boundary(), tag::same_dim ) );
	assert ( this->dim() == face .dim() + 1 );
	assert ( this->is_positive() );  // no negative STSI meshes
	Cell res = this->cell_behind ( face, tag::may_not_exist );
	if ( res .exists() )  return res;
	// else : either the cell does not exist or we are at a singular face
	return this->core->cell_behind
		( face, tag::seen_from, neighbour, tag::surely_singular, tag::may_not_exist );  }


inline Cell Mesh::cell_behind
( const Cell & face, const tag::SeenFrom &, const Cell & neighbour, const tag::SurelyExists & se ) const

// 'se' defaults to tag::surely_exists, so method may be called with only three arguments

// return the cell to which 'face' belongs
// this function is interesting only for STSI meshes

{	assert ( face .exists() );
	assert ( neighbour .exists() );
	assert ( neighbour .belongs_to ( *this, tag::same_dim ) );
	assert ( face .reverse ( tag::surely_exists )
	         .belongs_to ( neighbour .boundary(), tag::same_dim ) );
	assert ( this->dim() == face .dim() + 1 );
	assert ( this->is_positive() );  // no negative STSI meshes
	Cell res = this->cell_behind ( face, tag::may_not_exist );
	if ( res .exists() )  return res;
	// else : either the cell does not exist or we are at a singular face
	return this->core->cell_behind
		( face, tag::seen_from, neighbour, tag::surely_singular, tag::surely_exists );  }


#ifndef NDEBUG  // DEBUG mode

inline void Mesh::print_everything ( )
{	if ( not is_positive() ) std::cout << "(negative Mesh) ";
	core->print_everything ();                                 }

#endif  // DEBUG

//------------------------------------------------------------------------------------------------------//


inline bool Cell::is_positive ( ) const
{	assert ( this->exists() );
	return this->core->is_positive();  }

inline Cell Cell::get_positive ( ) const
{	assert ( this->exists() );
	return this->core->get_positive();  }


inline size_t Cell::dim ( ) const
{	assert ( this->exists() );
	return this->core->get_dim ( );  }

inline size_t Mesh::dim ( ) const
{	return tag::Util::assert_diff ( this->core->get_dim_plus_one(), 1 );  }
// tag::Util::assert_diff  provides a safe way to substract two size_t numbers


inline Cell Cell::reverse ( const tag::BuildIfNotExists & build ) const
// 'build' defaults to tag::build_if_not_exists, so method may be called with no arguments
{	assert ( this->exists() );
	if ( not this->core->reverse_attr .exists() )
	{	Cell::Positive * pos_this_core = tag::Util::assert_cast
			< Cell::Core*, Cell::Positive* > ( this->core );
		this->core->reverse_attr = Cell ( tag::whose_core_is,  // check, optimize !
			pos_this_core->build_reverse ( tag::one_dummy_wrapper ), tag::move );  }
	return this->core->reverse_attr;                                                }


inline Cell Cell::reverse ( const tag::DoesNotExist &, const tag::BuildNow & ) const

{	assert ( this->exists() );
	assert ( not this->core->reverse_attr .exists() );
	Cell::Positive * pos_this_core = tag::Util::assert_cast
		< Cell::Core*, Cell::Positive* > ( this->core );
	return  this->core->reverse_attr = Cell ( tag::whose_core_is,  // check, optimize !
		pos_this_core->build_reverse ( tag::one_dummy_wrapper ), tag::move );  }


inline Cell Cell::reverse ( const tag::MayNotExist & ) const
{	assert ( this->exists() );
	return this->core->reverse_attr;  }


inline Cell Cell::reverse ( const tag::SurelyExists & ) const
{	assert ( this->exists() );
	assert ( this->core->reverse_attr .exists() );
	return this->core->reverse_attr;                }


inline Mesh Mesh::reverse ( const tag::BuildCellsIfNec & bcin ) const
// 'bcin' defaults to tag::build_cells_if_necessary, so method can be called with no arguments

{	if ( this->is_positive() )
		return Mesh ( tag::from_positive_mesh, tag::whose_core_is, this->core,
	                 tag::build_negative, tag::build_cells_if_necessary      );
	// else
	return Mesh ( tag::whose_core_is, core, tag::previously_existing, tag::is_positive );  }
	

inline Mesh Mesh::reverse ( const tag::ReverseCellsSurelyExist & ) const

{	if ( this->is_positive() )
		return Mesh ( tag::from_positive_mesh, tag::whose_core_is, this->core,
	                 tag::build_negative, tag::reverse_cells_surely_exist    );
	// else
	return Mesh ( tag::whose_core_is, core, tag::previously_existing, tag::is_positive );  }
	

inline bool Cell::has_reverse ( ) const
{	assert ( this->exists() );
	return this->core->reverse_attr .exists();  }


inline Mesh Cell::boundary ( ) const
{	assert ( this->exists() );
	return this->core->boundary();  }


inline Cell Cell::tip () const
{	assert ( this->exists() );
	assert ( this->core->tip() .exists() );
	return this->core->tip();                }


inline Cell Cell::base () const
{	assert ( this->exists() );
	assert ( this->core->base() .exists() );
	return this->core->base();                }


#ifndef NDEBUG  // DEBUG mode
inline void Cell::print_everything ( )
{	core->print_everything ( );  }
#endif


inline void Cell::glue_on_bdry_of ( Cell & cll )

// glue 'this' face on the boundary of cell 'cll'
// any of them may be negative

{	assert ( this->exists() );
	this->core->glue_on_bdry_of ( cll .core );  }


inline void Cell::glue_on_bdry_of ( Cell & cll, const tag::DoNotBother & )

// glue 'this' face on the boundary of cell 'cll'
// any of them may be negative

{	assert ( this->exists() );
	this->core->glue_on_bdry_of ( cll .core, tag::do_not_bother );  }


inline void Cell::cut_from_bdry_of ( Cell & cll )

// cut 'this' face from the boundary of cell 'cll'
// any of them may be negative

{	assert ( this->exists() );
	this->core->cut_from_bdry_of ( cll .core );  }


inline void Cell::cut_from_bdry_of ( Cell & cll, const tag::DoNotBother & )

// cut 'this' face from the boundary of cell 'cll'
// any of them may be negative

{	assert ( this->exists() );
	this->core->cut_from_bdry_of ( cll .core, tag::do_not_bother );  }


inline void Cell::add_to ( Mesh & msh )

// add 'this' cell to the mesh 'msh' by calling the virtual method add_to_mesh
// if 'msh' is the boundary of a cell, use instead 'glue_on_bdry_of'

{	assert ( this->exists() );
	assert ( msh .exists() );
	assert ( this->dim() == msh .dim() );
	assert ( this->dim() > 0 );
	if ( msh .is_positive() )  this->core->add_to_mesh ( msh .core );
	else
	{	assert( this->core->reverse_attr .exists() );
		this->core->reverse_attr .core->add_to_mesh ( msh .core );  }   }
// for negative Meshes, the core points towards the reverse, positive, Mesh::Core


inline void Cell::add_to ( Mesh & msh, const tag::DoNotBother & )

// add 'this' cell to the mesh 'msh' by calling the virtual method add_to_mesh
// if 'msh' is the boundary of a cell, use instead 'glue_on_bdry_of'

// tag::do_not_bother means : for Mesh::Connected::***,
// do not check for connectivity/openness/closedeness
// do not try to update first_vertex, last_vertex

{	assert ( this->exists() );
	assert ( msh .exists() );
	assert ( this->dim() == msh .dim() );
	assert ( this->dim() > 0 );
	if ( msh .is_positive() )  this->core->add_to_mesh ( msh .core, tag::do_not_bother );
	else
	{	assert( this->core->reverse_attr .exists() );
		this->core->reverse_attr .core->add_to_mesh ( msh .core, tag::do_not_bother );  }   }
// for negative Meshes, the core points towards the reverse, positive, Mesh::Core


inline void Cell::add_to ( Mesh & msh, const tag::DoNotBother &, const tag::KeepAsFirstVer & )

// add 'this' cell to the mesh 'msh' by calling the virtual method add_to_mesh
// if 'msh' is the boundary of a cell, use instead 'glue_on_bdry_of'

// tag::do_not_bother means : for Mesh::Connected::***,
// do not check for connectivity/openness/closedeness
// do not try to update first_vertex, last_vertex

// tag::keep_as_first_ver means : for Mesh::Connected::***,
// keep (a wrapper of) 'this' as first_ver, although this is of course not true
// used in extruding meshes

{	assert ( this->exists() );
	assert ( msh .exists() );
	assert ( this->dim() == msh .dim() );
	assert ( this->dim() > 0 );
	if ( msh .is_positive() )
		this->core->add_to_mesh ( msh .core, tag::do_not_bother, tag::keep_as_first_ver );
	else
	{	assert( this->core->reverse_attr .exists() );
		this->core->reverse_attr .core->add_to_mesh
			( msh .core, tag::do_not_bother, tag::keep_as_first_ver );  }                    }
// for negative Meshes, the core points towards the reverse, positive, Mesh::Core


inline void Cell::add_to ( Mesh & msh, const tag::Force & )

// 'msh' must have a Mesh::STSI core
// add 'this' cell to the mesh 'msh' by calling the virtual method add_to_mesh
// breaks previously existing neighbourhood relationships in order to insert 'this' cell

{	assert ( this->exists() );
	assert ( msh .exists() );
	assert ( this->dim() == msh .dim() );
	assert ( this->dim() > 0 );
	if ( msh .is_positive() )  this->core->add_to_mesh ( msh .core, tag::force );
	else
	{	assert( this->core->reverse_attr .exists() );
		this->core->reverse_attr .core->add_to_mesh ( msh .core, tag::force );  }   }
// for negative Meshes, the core points towards the reverse, positive, Mesh::Core


inline void Cell::add_to
( Mesh & msh, const tag::NeighbRels &, const std::map < Cell, Cell > & rels, bool force_other_faces )

// 'msh' must have a Mesh::STSI core
// add 'this' cell to the mesh 'msh' by calling the virtual method add_to_mesh
// uses neighbourhood relationships described by 'rels' in order to insert 'this' cell
// if last argument is true (tag::force_other_faces),
// a face not appearing in 'rels' will break previously existing neighbourhood relationships

{	assert ( this->exists() );
	assert ( msh .exists() );
	assert ( this->dim() == msh .dim() );
	assert ( this->dim() > 0 );
	assert ( msh .is_positive() );
  this->core->add_to_mesh ( msh .core, tag::neighbourhood_relations, rels, force_other_faces );  }


inline void Cell::remove_from ( Mesh & msh )

// remove 'this' cell from the mesh 'msh' by calling the virtual method remove_from_mesh
// if 'msh' is the boundary of a cell, 'cut_from_bdry_of' should be used instead

{	assert ( this->exists() );
	assert ( this->dim() == msh .dim() );
	assert ( this->dim() > 0 );
	if ( msh .is_positive() )  this->core->remove_from_mesh ( msh .core );
	else
	{	assert( this->core->reverse_attr .exists() );
		this->core->reverse_attr .core->remove_from_mesh ( msh .core );  }   }
// for negative Meshes, the core points towards the reverse, positive, Mesh::Core


inline void Cell::remove_from ( Mesh & msh, const tag::DoNotBother & )

// remove 'this' cell from the mesh 'msh' by calling the virtual method remove_from_mesh
// if 'msh' is the boundary of a cell, 'cut_from_bdry_of' should be used instead

// tag::do_not_bother means : for Mesh::Connected::***,
// do not check for connectivity/openness/closedeness
// do not try to update first_vertex, last_vertex

{	assert ( this->exists() );
	assert ( this->dim() == msh .dim() );
	assert ( this->dim() > 0 );
	if ( msh .is_positive() )  this->core->remove_from_mesh ( msh .core, tag::do_not_bother );
	else
	{	assert ( this->core->reverse_attr .exists() );
		this->core->reverse_attr .core->remove_from_mesh ( msh .core, tag::do_not_bother );  }   }
// for negative Meshes, the core points towards the reverse, positive, Mesh::Core

//------------------------------------------------------------------------------------------------------//


inline void Mesh::closed_loop ( const Cell & ver )
// for connected one-dim meshes, set both first_ver and last_ver to 'ver'
{	assert ( this->exists() );
	this->core->closed_loop ( ver );  }


inline void Mesh::closed_loop ( const Cell & ver, size_t n )
// for connected one-dim meshes, set both first_ver and last_ver to 'ver'
// and number of segments
{	assert ( this->exists() );
	this->core->closed_loop ( ver, n );  }

//------------------------------------------------------------------------------------------------------//


inline bool Cell::is_regular ( const tag::WithinSTSIMesh &, const Mesh & msh ) const
// 'this' face is not singular

{	assert ( msh .is_positive() );  // no negative STSI meshes
	assert ( this->dim() + 1 == msh .dim() );
	assert ( this->belongs_to ( msh ) );
	
	Mesh::STSI * msh_core = tag::Util::assert_cast < Mesh::Core *, Mesh::STSI * > ( msh .core );
	std::map < Cell, std::list < Cell > > ::const_iterator it =
		msh_core->singular_behind .find ( * this );
	#ifndef NDEBUG  // DEBUG mode
	Cell face_rev = this->reverse ( tag::may_not_exist );
	if ( face_rev .exists() )
	{	std::map < Cell, std::list < Cell > > ::const_iterator it_rev =
			msh_core->singular_behind .find ( face_rev );
		assert ( ( it == msh_core->singular_behind .end() ) ==
		         ( it_rev == msh_core->singular_behind .end() ) );  }
	#endif  // DEBUG
	return  it == msh_core->singular_behind .end();                                               }
	
//------------------------------------------------------------------------------------------------------//


inline Cell::PositiveVertex::PositiveVertex ( const tag::OneDummyWrapper & )
: Cell::Positive ( tag::of_dim, 0, tag::size_meshes, Mesh::maximum_dimension_plus_one,
                   tag::one_dummy_wrapper                                              )
{	}


inline Cell::NegativeVertex::NegativeVertex
( const tag::ReverseOf &, Cell::Positive * direct_ver_p, const tag::OneDummyWrapper & )
: Cell::Negative ( tag::of_dim, 0, tag::reverse_of, direct_ver_p, tag::one_dummy_wrapper )
{	assert ( direct_ver_p );
	assert ( direct_ver_p->get_dim() == 0 );    }


inline Cell::PositiveSegment::PositiveSegment ( Cell A, Cell B, const tag::OneDummyWrapper & )

:	Cell::Positive::NotVertex ( tag::of_dim, 1, tag::size_meshes,
	                            tag::Util::assert_diff ( Mesh::maximum_dimension_plus_one, 1 ),
	                            tag::one_dummy_wrapper                                         ),
	// tag::Util::assert_diff provides a safe way to substract two 'size_t' numbers
	base_attr { A }, tip_attr { B }

{	// below is a much simplified version of Cell::Negative::Vertex::add_to_seg
	// that's because 'this' segment has just been created, so it has no meshes above
	// also, the base and tip have already been correctly initialized
	assert ( not A .is_positive() );
	assert ( B .is_positive() );
	Cell pos_A =  A .reverse ( tag::surely_exists );
	assert ( pos_A .is_positive() );
	Cell::Positive::Vertex * pos_Aa = tag::Util::assert_cast
		< Cell::Core*, Cell::Positive::Vertex* > ( pos_A .core );
	assert ( pos_Aa->segments .find (this) == pos_Aa->segments.end() );
	// pos_Aa->meshes [0] [msh] = Cell::field_to_meshes { 0, 1 };
	// the third component 'where' is irrelevant here
	pos_Aa->segments.emplace ( std::piecewise_construct,
	      std::forward_as_tuple (this), std::forward_as_tuple (-1) );
	// below is a much simplified version of Cell::Positive::Vertex::add_to_seg
	// that's because 'this' segment has just been created, so it has no meshes above
	// also, the tip has already been correctly initialized
	Cell::Positive::Vertex * Bb = tag::Util::assert_cast
		< Cell::Core*, Cell::Positive::Vertex* > ( B .core );
	assert ( Bb->segments .find (this) == Bb->segments .end() );
	// Bb->meshes [0] [msh] = Cell::field_to_meshes { 1, 0 };
	// the third component 'where' is irrelevant here
	Bb->segments.emplace ( std::piecewise_construct,
	   std::forward_as_tuple (this), std::forward_as_tuple (1) );           }


inline Cell::NegativeNotVertex::NegativeNotVertex
( const tag::OfDimension &, size_t d,
  const tag::ReverseOf &, Cell::Positive * direct_cll_p, const tag::OneDummyWrapper & )

:	Cell::Negative ( tag::of_dim, d, tag::reverse_of, direct_cll_p, tag::one_dummy_wrapper )

{	}


inline Cell::NegativeSegment::NegativeSegment
( const tag::ReverseOf &, Cell::Positive * direct_seg_p, const tag::OneDummyWrapper & )

:	Cell::NegativeNotVertex ( tag::of_dim, 1, tag::reverse_of, direct_seg_p,
	                          tag::one_dummy_wrapper                        )

// we must make sure that both extremities of 'direct_seg_p' have a reverse
// well, the base surely has one since it's a NegativeVertex

{	assert ( direct_seg_p );
	Cell Ar = direct_seg_p->base(), B = direct_seg_p->tip();
	assert ( direct_seg_p->base() .exists() );
	assert ( direct_seg_p->tip() .exists() );
	assert ( direct_seg_p->base() .reverse ( tag::may_not_exist ) .exists() );
	Cell rev_tip = direct_seg_p->tip() .reverse ( tag::build_if_not_exists );  }	


inline Cell::PositiveHighDim::PositiveHighDim
( const tag::OfDimension &, const size_t d,
  const tag::WhoseBoundaryIs &, const Mesh & msh, const tag::OneDummyWrapper &    )

:	Cell::Positive::NotVertex ( tag::of_dim, d, tag::size_meshes,
	         tag::Util::assert_diff ( Mesh::maximum_dimension_plus_one, d ),
	                           tag::one_dummy_wrapper                       ),
	// tag::Util::assert_diff provides a safe way to substract two 'size_t' numbers
	boundary_attr ( msh )
	
{	assert ( msh .core );
	assert ( msh .core->get_dim_plus_one() == d );
	msh.core->cell_enclosed = this;                }


inline Cell::PositiveHighDim::PositiveHighDim
( const tag::WhoseBoundaryIs &, const Mesh & msh, const tag::OneDummyWrapper & )
:	Cell::Positive::HighDim ( tag::of_dim, msh.core->get_dim_plus_one(),
	                          tag::whose_bdry_is, msh, tag::one_dummy_wrapper )
{	}


inline Cell::NegativeHighDim::NegativeHighDim
( const tag::ReverseOf &, Cell::Positive * direct_cell_p, const tag::OneDummyWrapper & )
:	Cell::Negative::HighDim
	( tag::of_dim, direct_cell_p->get_dim(), tag::reverse_of, direct_cell_p,
	  tag::one_dummy_wrapper                                                )
{	}


inline Cell::NegativeHighDim::NegativeHighDim
(	const tag::OfDimension &, const size_t d, const tag::ReverseOf &,
	Cell::Positive * direct_cell_p, const tag::OneDummyWrapper &     )
	
:	Cell::NegativeNotVertex
	( tag::of_dim, d, tag::reverse_of, direct_cell_p, tag::one_dummy_wrapper )

// we must make sure that all faces of 'direct_cell_p' have a reverse
// we build the reverse faces if necessary

{	assert ( direct_cell_p );
	assert ( direct_cell_p->get_dim() == d );
	assert ( direct_cell_p->boundary() .is_positive() );
	Cell direct_cell ( tag::whose_core_is, direct_cell_p,
                     tag::previously_existing, tag::surely_not_null );
	// Mesh::Iterator it ( tag::whose_core_is, direct_cell_p->iterator ( ...  !!
	Mesh::Iterator it = direct_cell_p->boundary() .iterator
		( tag::over_cells_of_max_dim, tag::as_found );
	for ( it .reset(); it .in_range(); it++ )
	{	Cell face = *it;
		assert ( face .exists() );
		Cell face_rev = face .reverse ( tag::build_if_not_exists );  }        }


inline Cell::PositiveHighDim::PositiveHighDim
( const tag::Triangle &, const Cell & AB, const Cell & BC, const Cell & CA,
  const tag::OneDummyWrapper &                                             )
	
:	Cell::Positive::HighDim
	( tag::whose_boundary_is,
	  Mesh ( tag::whose_core_is,
	         new Mesh::Connected::OneDim ( tag::with, 3, tag::segments,
//	         new Mesh::Fuzzy ( tag::of_dimension, 2, tag::minus_one,
	                                       tag::one_dummy_wrapper       ),
	         tag::move, tag::is_positive                                  ),
	  tag::one_dummy_wrapper                                                )
	
{	assert ( AB .exists() );
	assert ( BC .exists() );
	assert ( CA .exists() );
	assert ( AB .dim() == 1 );
	assert ( BC .dim() == 1 );
	assert ( CA .dim() == 1 );
	assert ( AB .tip() == BC .base() .reverse() );
	assert ( BC .tip() == CA .base() .reverse() );
	assert ( CA .tip() == AB .base() .reverse() );
	// no need for 'glue_on_bdry_of': 'this' cell has just been created, it has no meshes above
	// we call 'add_to_mesh' instead (as if the mesh were not a boundary)
	assert  ( this->boundary() .is_positive() );
	AB .core->add_to_mesh ( this->boundary() .core, tag::do_not_bother );
	BC .core->add_to_mesh ( this->boundary() .core, tag::do_not_bother );
	CA .core->add_to_mesh ( this->boundary() .core, tag::do_not_bother );
	Mesh::Connected::OneDim * bdry = tag::Util::assert_cast
		< Mesh::Core*, Mesh::Connected::OneDim* > ( this->boundary() .core );
	bdry->first_ver = AB .base() .reverse ( tag::surely_exists );
	bdry->last_ver = CA .tip();                                               }
	

inline Cell::PositiveHighDim::PositiveHighDim
( const tag::Quadrangle &, const Cell & AB, const Cell & BC, const Cell & CD,
  const Cell & DA, const tag::OneDummyWrapper &                              )
	
:	Cell::Positive::HighDim
	( tag::whose_boundary_is,
	  Mesh ( tag::whose_core_is,
	         new Mesh::Connected::OneDim ( tag::with, 4, tag::segments,
//	         new Mesh::Fuzzy ( tag::of_dimension, 2, tag::minus_one,
	                                       tag::one_dummy_wrapper       ),
	         tag::move, tag::is_positive                                  ),
	  tag::one_dummy_wrapper                                                )

{	assert ( AB .exists() );
	assert ( BC .exists() );
	assert ( CD .exists() );
	assert ( DA .exists() );
	assert ( AB .dim() == 1 );
	assert ( BC .dim() == 1 );
	assert ( CD .dim() == 1 );
	assert ( DA .dim() == 1 );
	assert ( AB .tip() == BC .base() .reverse() );
	assert ( BC .tip() == CD .base() .reverse() );
	assert ( CD .tip() == DA .base() .reverse() );
	assert ( DA .tip() == AB .base() .reverse() );
	// no need for glue_on_bdry_of : 'this' cell has just been created, it has no meshes above
	// we call add_to_mesh instead (as if the mesh were not a boundary)
	assert  ( this->boundary() .is_positive() );
	AB .core->add_to_mesh ( this->boundary() .core, tag::do_not_bother );
	BC .core->add_to_mesh ( this->boundary() .core, tag::do_not_bother );
	CD .core->add_to_mesh ( this->boundary() .core, tag::do_not_bother );
	DA .core->add_to_mesh ( this->boundary() .core, tag::do_not_bother );
	Mesh::Connected::OneDim * bdry = tag::Util::assert_cast
		< Mesh::Core*, Mesh::Connected::OneDim* > ( this->boundary().core );
	bdry->first_ver = AB .base() .reverse ( tag::surely_exists );
	bdry->last_ver = DA .tip();                                              }


inline Cell::PositiveHighDim::PositiveHighDim
( const tag::Pentagon &, const Cell & AB, const Cell & BC, const Cell & CD,
  const Cell & DE, const Cell & EA, const tag::OneDummyWrapper &           )
	
:	Cell::Positive::HighDim
	( tag::whose_boundary_is,
	  Mesh ( tag::whose_core_is,
	         new Mesh::Connected::OneDim ( tag::with, 5, tag::segments,
	                                       tag::one_dummy_wrapper       ),
	         tag::move, tag::is_positive                                  ),
	  tag::one_dummy_wrapper                                                )

{	assert ( AB .exists() );
	assert ( BC .exists() );
	assert ( CD .exists() );
	assert ( DE .exists() );
	assert ( EA .exists() );
	assert ( AB .dim() == 1 );
	assert ( BC .dim() == 1 );
	assert ( CD .dim() == 1 );
	assert ( DE .dim() == 1 );
	assert ( EA .dim() == 1 );
	assert ( AB .tip() == BC .base() .reverse() );
	assert ( BC .tip() == CD .base() .reverse() );
	assert ( CD .tip() == DE .base() .reverse() );
	assert ( DE .tip() == EA .base() .reverse() );
	assert ( EA .tip() == AB .base() .reverse() );
	// no need for glue_on_bdry_of : 'this' cell has just been created, it has no meshes above
	// we call add_to_mesh instead (as if the mesh were not a boundary)
	assert  ( this->boundary() .is_positive() );
	AB .core->add_to_mesh ( this->boundary() .core, tag::do_not_bother );
	BC .core->add_to_mesh ( this->boundary() .core, tag::do_not_bother );
	CD .core->add_to_mesh ( this->boundary() .core, tag::do_not_bother );
	DE .core->add_to_mesh ( this->boundary() .core, tag::do_not_bother );
	EA .core->add_to_mesh ( this->boundary() .core, tag::do_not_bother );
	Mesh::Connected::OneDim * bdry = tag::Util::assert_cast
		< Mesh::Core*, Mesh::Connected::OneDim* > ( this->boundary() .core );
	bdry->first_ver = AB .base() .reverse ( tag::surely_exists );
	bdry->last_ver = EA .tip();                                               }


inline Cell::PositiveHighDim::PositiveHighDim
( const tag::Hexagon &, const Cell & AB, const Cell & BC, const Cell & CD,
  const Cell & DE, const Cell & EF, const Cell & FA, const tag::OneDummyWrapper & )
	
:	Cell::Positive::HighDim
	( tag::whose_boundary_is,
	  Mesh ( tag::whose_core_is,
	         new Mesh::Connected::OneDim ( tag::with, 6, tag::segments,
	                                       tag::one_dummy_wrapper       ),
	         tag::move, tag::is_positive                                  ),
	  tag::one_dummy_wrapper                                                )

{	assert ( AB .exists() );
	assert ( BC .exists() );
	assert ( CD .exists() );
	assert ( DE .exists() );
	assert ( EF .exists() );
	assert ( FA .exists() );
	assert ( AB .dim() == 1 );
	assert ( BC .dim() == 1 );
	assert ( CD .dim() == 1 );
	assert ( DE .dim() == 1 );
	assert ( EF .dim() == 1 );
	assert ( FA .dim() == 1 );
	assert ( AB .tip() == BC .base() .reverse() );
	assert ( BC .tip() == CD .base() .reverse() );
	assert ( CD .tip() == DE .base() .reverse() );
	assert ( DE .tip() == EF .base() .reverse() );
	assert ( EF .tip() == FA .base() .reverse() );
	assert ( FA .tip() == AB .base() .reverse() );
	// no need for glue_on_bdry_of : 'this' cell has just been created, it has no meshes above
	// we call add_to_mesh instead (as if the mesh were not a boundary)
	assert  ( this->boundary() .is_positive() );
	AB .core->add_to_mesh ( this->boundary() .core, tag::do_not_bother );
	BC .core->add_to_mesh ( this->boundary() .core, tag::do_not_bother );
	CD .core->add_to_mesh ( this->boundary() .core, tag::do_not_bother );
	DE .core->add_to_mesh ( this->boundary() .core, tag::do_not_bother );
	EF .core->add_to_mesh ( this->boundary() .core, tag::do_not_bother );
	FA .core->add_to_mesh ( this->boundary() .core, tag::do_not_bother );
	Mesh::Connected::OneDim * bdry = tag::Util::assert_cast
		< Mesh::Core*, Mesh::Connected::OneDim* > ( this->boundary() .core );
	bdry->first_ver = AB .base() .reverse ( tag::surely_exists );
	bdry->last_ver = FA .tip();                                               }


inline Cell::PositiveHighDim::PositiveHighDim
( const tag::Tetrahedron &, const Cell & face_1, const Cell & face_2,
  const Cell & face_3, const Cell & face_4, const tag::OneDummyWrapper & )
	
:	Cell::Positive::HighDim
	( tag::whose_boundary_is,
	  Mesh ( tag::whose_core_is,
	         new Mesh::Fuzzy ( tag::of_dimension, 3, tag::minus_one,
	                                       tag::one_dummy_wrapper       ),
	         tag::move, tag::is_positive                                  ),
	  tag::one_dummy_wrapper                                                )

{	assert ( face_1 .exists() );
	assert ( face_2 .exists() );
	assert ( face_3 .exists() );
	assert ( face_4 .exists() );
	assert ( face_1 .dim() == 2 );
	assert ( face_2 .dim() == 2 );
	assert ( face_3 .dim() == 2 );
	assert ( face_4 .dim() == 2 );
	// no need for glue_on_bdry_of : 'this' cell has just been created, it has no meshes above
	// we call add_to_mesh instead (as if the mesh were not a boundary)
	assert  ( this->boundary() .is_positive() );
	face_1 .core->add_to_mesh ( this->boundary() .core );
	face_2 .core->add_to_mesh ( this->boundary() .core );
	face_3 .core->add_to_mesh ( this->boundary() .core );
	face_4 .core->add_to_mesh ( this->boundary() .core );  }



inline Cell::PositiveHighDim::PositiveHighDim
( const tag::Hexahedron &, const Cell & face_1, const Cell & face_2,
  const Cell & face_3, const Cell & face_4,
  const Cell & face_5, const Cell & face_6, const tag::OneDummyWrapper & )

// we do not need to worry about which face is up, which is down and so on
// faces' sides are like magnets, they will clink right
	
:	Cell::Positive::HighDim
	( tag::whose_boundary_is,
	  Mesh ( tag::whose_core_is,
	         new Mesh::Fuzzy ( tag::of_dimension, 3, tag::minus_one,
	                                       tag::one_dummy_wrapper       ),
	         tag::move, tag::is_positive                                  ),
	  tag::one_dummy_wrapper                                                )

{	assert ( face_1 .exists() );
	assert ( face_2 .exists() );
	assert ( face_3 .exists() );
	assert ( face_4 .exists() );
	assert ( face_5 .exists() );
	assert ( face_6 .exists() );
	assert ( face_1 .dim() == 2 );
	assert ( face_2 .dim() == 2 );
	assert ( face_3 .dim() == 2 );
	assert ( face_4 .dim() == 2 );
	assert ( face_5 .dim() == 2 );
	assert ( face_6 .dim() == 2 );
	// no need for glue_on_bdry_of : 'this' cell has just been created, it has no meshes above
	// we call add_to_mesh instead (as if the mesh were not a boundary)
	assert  ( this->boundary() .is_positive() );
	face_1 .core->add_to_mesh ( this->boundary() .core );
	face_2 .core->add_to_mesh ( this->boundary() .core );
	face_3 .core->add_to_mesh ( this->boundary() .core );
	face_4 .core->add_to_mesh ( this->boundary() .core );
	face_5 .core->add_to_mesh ( this->boundary() .core );
	face_6 .core->add_to_mesh ( this->boundary() .core );  }

//------------------------------------------------------------------------------------------------------//


inline bool tag::Util::Core::dispose_query ( )
{	assert ( this->nb_of_wrappers > 0 );
	this->nb_of_wrappers--;
	return this->nb_of_wrappers == 0;     }

//------------------------------------------------------------------------------------------------------//
//------------------------------------------------------------------------------------------------------//


class Cell::Numbering

{	public :

	class Core;

	std::shared_ptr < Numbering::Core > core;

	inline Numbering ( const tag::WhoseCoreIs &, std::shared_ptr < Numbering::Core > c )
	:	core ( c )
	{	assert ( c );  }

	inline Numbering ( const tag::NonExistent & ) : core ( nullptr )
	{	}

	inline Numbering ( const tag::Map & );

	inline Numbering ( const tag::Field & );

	inline bool exists ( ) const
	{	return  core != nullptr;  }

	inline size_t size ( ) const;

	inline size_t & operator() ( const Cell ) const;

	inline bool has_cell ( const Cell ) const;

	class Map;  class Field;  // latter defined in field.h

};	 // end of  class Cell::Numbering

//------------------------------------------------------------------------------------------------------//

  
class Cell::Numbering::Core

// abstract class, specialized in Cell::Numbering::Field and Cell::Numbering::Map

{	public :

	virtual ~Core ( ) { }

	virtual bool exists ( ) = 0;

	virtual size_t size ( ) = 0;

	virtual size_t & index_of ( const Cell ) = 0;

	virtual bool has_cell ( const Cell ) = 0;

};	 // end of  class Cell::Numbering::Core

//------------------------------------------------------------------------------------------------------//


inline size_t Cell::Numbering::size ( ) const
{	assert ( this->exists() );
	return  this->core->size();  }

inline size_t & Cell::Numbering::operator() ( const Cell c ) const
{	assert ( this->exists() );
	return  this->core->index_of (c);  }

inline bool Cell::Numbering::has_cell ( const Cell c ) const
{	assert ( this->exists() );
	return  this->core->has_cell (c);  }

//------------------------------------------------------------------------------------------------------//


class Cell::Numbering::Map : public Cell::Numbering::Core

{	public :

	std::map < Cell, size_t > * map;
	bool delete_map_upon_destr;

	inline Map ( )
	: map { new std::map < Cell, size_t > }, delete_map_upon_destr { true }
	{	}

	inline Map ( std::map < Cell, size_t > * numb_map )
	:	map { numb_map }, delete_map_upon_destr { false }
	{	}

	virtual ~Map ( );

	bool exists ( );  // virtual from Cell::Numbering::Core

	size_t size ( );  // virtual from Cell::Numbering::Core
	
	bool has_cell ( const Cell cll );  // virtual from Cell::Numbering::Core
	
	size_t & index_of ( const Cell );  // virtual from Cell::Numbering::Core

};	 // end of  class Cell::Numbering::Map

//------------------------------------------------------------------------------------------------------//


inline Cell::Numbering::Numbering ( const tag::Map & )
:	core ( std::make_shared < Cell::Numbering::Map > () )
{	}

//------------------------------------------------------------------------------------------------------//
//------------------------------------------------------------------------------------------------------//

}  // namespace maniFEM

#include <src/field.h>  // needed for Cell::Numbering::Field

namespace maniFEM {

class Mesh::Composite

// a conglomerate of several cells and meshes related to each other
// usually, lower dimensional entities are included in the boundary of higher dimensional ones

// also, can have attached other heterogeneous information like
// a numeric field (or several) storing the solution of some PDE
// a numbering of the vertices

// if geometric dimension > 3, exported file will not be readable by gmsh
// if max topological dimension > 3, exported file will not be readable by gmsh
// it still can be used by maniFEM programs to read Mesh and Cell objects

{	public :

	// maps below correspond to $PhysicalNames in gmsh
	// elementary entities will be built when needed, e.g. in 'export_to_file'
	std::map < std::string, std::vector < Cell > > points { };
	std::map < std::string, std::vector < Mesh > > meshes { };

	// maps below correspond to $NodeData in gmsh
	std::map < std::string, std::vector < Function > > functions { };

	Cell::Numbering numbering { tag::non_existent };

	inline Composite ( )
	{	}

	void add_cell ( const std::string & name, const Cell & P );
	Cell retrieve_cell ( const std::string & name ) const;
	
	void add_mesh ( const std::string & name, const Mesh & msh );
	inline void add_mesh ( const Mesh & msh )
	{	add_mesh ( "", msh );  }
	Mesh retrieve_mesh ( const std::string & name ) const;

	void add_function ( const std::string & name, const Function & f );
	inline void add_function ( const Function & f )
	{	add_function ( "", f );  }
	Function retrieve_function ( const std::string & name ) const;

	void add_numbering ( const Cell::Numbering & n );
	inline Cell::Numbering & retrieve_numbering ( )
	{	return  this->numbering;  }

	Mesh get_global_mesh ( bool existing_mesh_accepted ) const;
	inline Mesh get_global_mesh ( ) const
	{	return  get_global_mesh ( true );  }
	inline Mesh get_global_mesh ( const tag::ReturnNewMesh & ) const
	{	return  get_global_mesh ( false );  }

	void export_to_file ( const tag::Gmsh &, const std::string & ) const;  // defined in global.cpp

	struct Build  {  class Gather;  class ImportFromFile;  };
	
};  // end of class Mesh::Composite

//------------------------------------------------------------------------------------------------------//
//------------------------------------------------------------------------------------------------------//


class Mesh::Build

// a germ of a future mesh, awaiting for more options before finally building the mesh

{	public :

	class Core;

	std::shared_ptr < Mesh::Build::Core > core;

	inline Build ( const tag::WhoseCoreIs &, std::shared_ptr < Mesh::Build::Core > c )
	:	core { c }
	{	assert ( c );  }
	
	inline Build ( const tag::Join & );
	inline Build ( const tag::Grid & );
	#ifndef MANIFEM_NO_FRONTAL
	inline Build ( const tag::Frontal & );
	#endif  // ifndef MANIFEM_NO_FRONTAL
	inline Build ( const tag::ImportFromFile & );
	inline Build ( const tag::Extrude & );
	inline Build ( const tag::Gather & );
	
	inline ~Build ( ) { }
	
	inline operator Mesh () const;
	inline operator Mesh::Composite () const;

	// three methods below inteded for Mesh::Build::Join
	inline Mesh::Build mesh ( const Mesh & msh );
	inline Mesh::Build meshes ( const std::vector < Mesh > & c );
	template < typename container >
	inline Mesh::Build meshes ( const container & c );

	// two methods below inteded for Mesh::Composite::Build::Gather
	inline Mesh::Build cell ( const std::string & name, const Cell & cll );
	inline Mesh::Build mesh ( const std::string & name, const Mesh & msh );
	inline Mesh::Build function ( const std::string & name, const Function & f );
	inline Mesh::Build numbering ( const Cell::Numbering & n );

	// two methods below inteded for Mesh::Build::Grid, Mesh::Build::Frontal
	inline Mesh::Build start_at ( const Cell & cll );
	inline Mesh::Build stop_at  ( const Cell & cll );

	// eighteen methods below inteded for Mesh::Build::Grid
	inline Mesh::Build shape ( const tag::Segment & );
	inline Mesh::Build divided_in ( size_t div );
	inline Mesh::Build divided_in ( size_t div_1, size_t div_2 );
	inline Mesh::Build divided_in ( size_t div_1, size_t div_2, size_t div_3 );
	inline Mesh::Build shape ( const tag::Triangle & );
	inline Mesh::Build faces ( const Mesh & AB, const Mesh & BC, const Mesh & CA );
	inline Mesh::Build vertices ( const Cell & P1, const Cell & P2, const Cell & P3 );
	inline Mesh::Build shape ( const tag::Quadrangle & );
	inline Mesh::Build shape ( const tag::Parallelogram & )
	{	return  shape ( tag::quadrangle );  }
	inline Mesh::Build shape ( const tag::Rectangle & )
	{	return  shape ( tag::quadrangle );  }
	inline Mesh::Build shape ( const tag::Square & )
	{	return  shape ( tag::quadrangle );  }
	inline Mesh::Build with_triangles ( );
	inline Mesh::Build faces ( const Mesh & AB, const Mesh & BC, const Mesh & CD, const Mesh & DA );
	inline Mesh::Build vertices ( const Cell & P1, const Cell & P2, const Cell & P3, const Cell & P4 );
	inline Mesh::Build shape ( const tag::Hexahedron & );
	inline Mesh::Build faces ( const Mesh & f1, const Mesh & f2, const Mesh & f3,
	                           const Mesh & f4, const Mesh & f5, const Mesh & f6 );
	inline Mesh::Build vertices ( const Cell & P1, const Cell & P2, const Cell & P3, const Cell & P4,
	                              const Cell & P5, const Cell & P6, const Cell & P7, const Cell & P8 );
	inline Mesh::Build singular ( const Cell & cll );
	
	#ifndef MANIFEM_NO_QUOTIENT
	// two methods below inteded for Mesh::Build::Grid
	inline Mesh::Build winding ( );
	inline Mesh::Build winding ( const tag::Util::Action & g );
	#endif  // ifndef MANIFEM_NO_QUOTIENT

	#ifndef MANIFEM_NO_FRONTAL
	// seven methods below inteded for Mesh::Build::Frontal
	inline Mesh::Build boundary ( const Mesh & msh );
	inline Mesh::Build entire_manifold ( );
	inline Mesh::Build shortest_path ( );
	inline Mesh::Build towards ( const std::vector < double > & dir );
	inline Mesh::Build orientation ( const tag::OrientationChoice & );
	inline Mesh::Build desired_length ( double l );
	inline Mesh::Build desired_length ( const Function & l );
	#endif  // ifndef MANIFEM_NO_FRONTAL
	
	// three methods below inteded for Mesh::Build::ImportFromFile, Mesh::Composite::Build::ImportFromFile
	inline Mesh::Build format ( const tag::Gmsh & );
	inline Mesh::Build file_name ( const std::string & );
	inline Mesh::Build import_numbering ( );

	// eight methods below inteded for Mesh::Build::Extrude
	inline Mesh::Build swatch ( const Mesh & msh );
	inline Mesh::Build slide_along ( const Mesh & msh );   // track
	inline Mesh::Build track ( const Mesh & msh )
	{	return  slide_along ( msh );  }
	inline Mesh::Build swatch ( const Mesh::Composite & msh );
	inline Mesh::Build slide_along ( const Mesh::Composite & msh );   // track
	inline Mesh::Build track ( const Mesh::Composite & msh )
	{	return  slide_along ( msh );  }
	inline Mesh::Build follow_curvature ( );
	inline Mesh::Build follow_torsion ( );

	class Join;  class Grid;  class Frontal;  class ImportFromFile;  class Extrude;
	
};  // end of class Mesh::Build

//------------------------------------------------------------------------------------------------------//


class Mesh::Build::Core

// abstract class, instantiated in Mesh::Build::Join, Mesh::Build::Grid::***, Mesh::Build::ImportFromFile,
//                                 Mesh::Build::Extrude, Mesh::Build::Frontal::***,
//                                 Mesh::Composite::Build::Gather, Mesh::Composite::Build::Extrude
//                                 Mesh::Composite::Build::ImportFromFile

{	public :

	inline Core ( ) { };
	
	virtual ~Core ( ) { };

	virtual void add_mesh ( const Mesh & msh );
	// here execution forbidden, overridden by Mesh::Build::Join

	virtual void add_cell ( const std::string & name, const Cell & cll );
	virtual void add_mesh ( const std::string & name, const Mesh & msh );
	virtual void add_function ( const std::string & name, const Function & f );
	virtual void add_numbering ( const Cell::Numbering & n );
	// here execution forbidden, overridden by Mesh::Composite::Build::Gather

	virtual void divided_in ( size_t div );
	// here execution forbidden, overridden by Mesh::Build::Grid::Segment
	//                                  and by Mesh::Build::Grid::Triangle::WithVertices
	virtual void divided_in ( size_t div_1, size_t div_2 );
	// here execution forbidden, overridden by Mesh::Build::Grid::Quadrangle::WithVertices
	virtual void divided_in ( size_t div_1, size_t div_2, size_t div_3 );
	// here execution forbidden, overridden by Mesh::Build::Grid::Hexahedron::WithVertices
	#ifndef MANIFEM_NO_QUOTIENT
	virtual void set_winding ( const tag::Util::Action & g );
	// here execution forbidden, overridden by Mesh::Build::Grid::Segment
	virtual void set_winding_true ( );
	// here execution forbidden, overridden by Mesh::Build::Grid
	#endif  // ifndef MANIFEM_NO_QUOTIENT

	virtual Mesh::Build shape_segment ( );
	virtual Mesh::Build shape_triangle ( );
	virtual void define_faces ( const Mesh & AB, const Mesh & BC, const Mesh & CA );
	virtual Mesh::Build define_vertices ( const Cell & P1, const Cell & P2, const Cell & P3 );
	virtual Mesh::Build shape_quadrangle ( );
	virtual void define_faces ( const Mesh & AB, const Mesh & BC, const Mesh & CD, const Mesh & DA );
	virtual Mesh::Build define_vertices
	( const Cell & P1, const Cell & P2, const Cell & P3, const Cell & P4 );
	virtual void set_with_triangles ( );
	virtual Mesh::Build shape_cube ( );
	virtual void define_faces ( const Mesh & f1, const Mesh & f2, const Mesh & f3,
	                            const Mesh & f4, const Mesh & f5, const Mesh & f6 );
	virtual Mesh::Build define_vertices ( const Cell & P1, const Cell & P2, const Cell & P3,
	    const Cell & P4, const Cell & P5, const Cell & P6, const Cell & P7, const Cell & P8 );
	// here execution forbidden, overridden by Mesh::Build::Grid::***

	virtual void start_at ( const Cell & cll );
	virtual Mesh::Build stop_at ( const Cell & cll );
	// changes core for frontal mesh generation, not for grid

	#ifndef MANIFEM_NO_FRONTAL
	// methods below defined in frontal.cpp
	virtual Mesh::Build set_boundary ( const Mesh & msh );
	virtual void set_orientation ( const tag::OrientationChoice & oc );
	virtual void set_entire_manifold ( );
	virtual void set_towards ( const std::vector < double > & dir );
	virtual Mesh::Build set_constant_length ( double l );
	virtual Mesh::Build set_variable_length ( const Function & l );
	// here execution forbidden, overridden by Mesh::Build::Frontal
	#endif  // ifndef MANIFEM_NO_FRONTAL
	
	virtual void set_singular ( const Cell & cll );

	virtual void set_format ( const tag::Gmsh & );
	virtual void set_file_name ( const std::string & s );
	virtual Mesh::Build set_import_numbering ( );
	// here execution forbidden, overridden by Mesh::Build::ImportFromFile
	
	virtual void set_swatch ( const Mesh & msh );
	virtual Mesh::Build set_swatch ( const Mesh::Composite & msh );
	virtual void set_track ( const Mesh & msh );
	virtual Mesh::Build set_track ( const Mesh::Composite & msh );
	virtual void set_follow_curvature ( );
	virtual void set_follow_torsion ( );
	// here execution forbidden, overridden by Mesh::Build::Extrude

	virtual Mesh build_mesh ( );  // not const, sets common_vertex when extruding
	virtual Mesh::Composite build_composite_mesh ( );  // not const, idem
	// here execution forbidden, overridden if there is enough information to build the mesh
	
	// define_faces, define_vertices, set_winding, set_winding_true, set_singular,
	// set_with_triangles, set_boundary, add_mesh, add_cell, add_numbering,
	// set_entire_manifold, set_orientation, start_at, stop_at,
	// shape_segment, shape_triangle, shape_quadrangle, shape_cube,
	// set_constant_length, set_variable_length, set_towards,
	// set_format, set_file_name, set_import_numbering,
	// set_swatch, set_track, seg_follow_curvature, set_follow_torsion,
	// build_mesh, build_composite_mesh, add_mesh, divided_in

};  // end of class Mesh::Build::Core


inline Mesh::Build::operator Mesh () const
{	return  this->core->build_mesh();  }

inline Mesh::Build::operator Mesh::Composite () const
{	return  this->core->build_composite_mesh();  }

inline Mesh::Build Mesh::Build::mesh ( const Mesh & msh )
{	this->core->add_mesh ( msh );
	return * this;                 }

inline Mesh::Build Mesh::Build::meshes ( const std::vector < Mesh > & c )
{	for ( std::vector < Mesh > ::const_iterator it = c .begin(); it != c .end(); it ++ )
		this->core->add_mesh ( *it );
	return * this;                                                                        }

template < typename container >
inline Mesh::Build Mesh::Build::meshes ( const container & c )
{	for ( typename container::const_iterator it = c .begin(); it != c .end(); it ++ )
		this->core->add_mesh ( *it );
	return * this;                                                                     }

inline Mesh::Build Mesh::Build::cell ( const std::string & name, const Cell & cll )
{	this->core->add_cell ( name, cll );
	return * this;                       }

inline Mesh::Build Mesh::Build::mesh ( const std::string & name, const Mesh & msh )
{	this->core->add_mesh ( name, msh );
	return * this;                       }

inline Mesh::Build Mesh::Build::function ( const std::string & name, const Function & f )
{	this->core->add_function ( name, f );
	return * this;                         }

inline Mesh::Build Mesh::Build::numbering ( const Cell::Numbering & n )
{	this->core->add_numbering ( n );
	return * this;                    }

inline Mesh::Build Mesh::Build::divided_in ( size_t div )
{	this->core->divided_in ( div );
	return  * this;                  }

inline Mesh::Build Mesh::Build::divided_in ( size_t div_1, size_t div_2 )
{	this->core->divided_in ( div_1, div_2 );
	return  * this;                           }

inline Mesh::Build Mesh::Build::divided_in ( size_t div_1, size_t div_2, size_t div_3 )
{	this->core->divided_in ( div_1, div_2, div_3 );
	return  * this;                                  }

inline Mesh::Build Mesh::Build::faces ( const Mesh & AB, const Mesh & BC, const Mesh & CA )
{	this->core->define_faces ( AB, BC, CA );
	return * this;                            }

inline Mesh::Build Mesh::Build::faces
( const Mesh & AB, const Mesh & BC, const Mesh & CD, const Mesh & DA )
{	this->core->define_faces ( AB, BC, CD, DA );
	return * this;                                }

inline Mesh::Build Mesh::Build::faces
( const Mesh & f1, const Mesh & f2, const Mesh & f3,
  const Mesh & f4, const Mesh & f5, const Mesh & f6 )
{	this->core->define_faces ( f1, f2, f3, f4, f5, f6 );
	return * this;                                        }

inline Mesh::Build Mesh::Build::vertices ( const Cell & P1, const Cell & P2, const Cell & P3 )
{	return this->core->define_vertices ( P1, P2, P3 );  }

inline Mesh::Build Mesh::Build::vertices
( const Cell & P1, const Cell & P2, const Cell & P3, const Cell & P4 )
{	return this->core->define_vertices ( P1, P2, P3, P4 );  }

inline Mesh::Build Mesh::Build::vertices
( const Cell & P1, const Cell & P2, const Cell & P3, const Cell & P4,
  const Cell & P5, const Cell & P6, const Cell & P7, const Cell & P8 )
{	return this->core->define_vertices ( P1, P2, P3, P4, P5, P6, P7, P8 );  }

inline Mesh::Build Mesh::Build::with_triangles ( )
{	this->core->set_with_triangles();
	return  * this;                    }

inline Mesh::Build Mesh::Build::singular ( const Cell & cll )
{	this->core->set_singular ( cll );
	return  * this;                    }

inline Mesh::Build Mesh::Build::start_at ( const Cell & cll )
{	this->core->start_at ( cll );
	return  * this;                }

inline Mesh::Build Mesh::Build::stop_at ( const Cell & cll )
{	return  this->core->stop_at ( cll );                               }

#ifndef MANIFEM_NO_FRONTAL

inline Mesh::Build Mesh::Build::desired_length ( double l )
{	return  this->core->set_constant_length ( l );  }

inline Mesh::Build Mesh::Build::desired_length ( const Function & l )
{	return  this->core->set_variable_length ( l );  }

inline Mesh::Build Mesh::Build::orientation ( const tag::OrientationChoice & oc )
{	this->core->set_orientation ( oc );
	return  * this;                      }

inline Mesh::Build Mesh::Build::boundary ( const Mesh & msh )
{  return  this->core->set_boundary ( msh );  }

inline Mesh::Build Mesh::Build::shortest_path ( )
{	this->core->set_orientation ( tag::geodesic );
	return  * this;                                 }

inline Mesh::Build Mesh::Build::towards ( const std::vector < double > & dir )
{	this->core->set_towards ( dir );
	return  * this;                   }

inline Mesh::Build Mesh::Build::entire_manifold ( )
{	this->core->set_entire_manifold();
	return  * this;                     }

#endif  // ifndef MANIFEM_NO_FRONTAL

inline Mesh::Build Mesh::Build::shape ( const tag::Segment & )
{	return  this->core->shape_segment();  }

inline Mesh::Build Mesh::Build::shape ( const tag::Triangle & )
{	return  this->core->shape_triangle();  }

inline Mesh::Build Mesh::Build::shape ( const tag::Quadrangle & )
{	return  this->core->shape_quadrangle();  }

inline Mesh::Build Mesh::Build::shape ( const tag::Hexahedron & )
{	return  this->core->shape_cube();  }

inline Mesh::Build Mesh::Build::format ( const tag::Gmsh & )
{	this->core->set_format ( tag::gmsh );
	return  * this;                        }

inline Mesh::Build Mesh::Build::file_name ( const std::string & s )
{	this->core->set_file_name ( s );
	return  * this;                   }

inline Mesh::Build Mesh::Build::import_numbering ( )
{	return  this->core->set_import_numbering();  }

#ifndef MANIFEM_NO_QUOTIENT

inline Mesh::Build Mesh::Build::winding ( )
{	this->core->set_winding_true();
	return  * this;                  }

inline Mesh::Build Mesh::Build::winding ( const tag::Util::Action & g )
{	this->core->set_winding ( g );
	return  * this;                 }

#endif  // ifndef MANIFEM_NO_QUOTIENT

inline Mesh::Build Mesh::Build::swatch ( const Mesh & msh )
{	this->core->set_swatch ( msh);
	return  * this;                 }

inline Mesh::Build Mesh::Build::swatch ( const Mesh::Composite & msh )
{	return  this->core->set_swatch ( msh );  }

inline Mesh::Build Mesh::Build::slide_along ( const Mesh & msh )  // track
{	this->core->set_track ( msh );
	return  * this;                 }

inline Mesh::Build Mesh::Build::slide_along ( const Mesh::Composite & msh )  // track
{	return  this->core->set_track ( msh );  }

inline Mesh::Build Mesh::Build::follow_curvature ( )
{	this->core->set_follow_curvature();
	return  * this;                      }

inline Mesh::Build Mesh::Build::follow_torsion ( )
{	this->core->set_follow_torsion();
	return  * this;                    }

//------------------------------------------------------------------------------------------------------//


class Mesh::Build::Join : public Mesh::Build::Core

// builder for joining several meshes

{	public :

	std::vector < Mesh > components;

	inline Join ( )
	:	Mesh::Build::Core ()
	{	}

	// define_faces, set_winding, set_winding_true, set_singular, set_boundary,
	// set_entire_manifold, set_orientation, start_at, stop_at, build_composite_mesh,
	// shape_segment, shape_triangle, shape_quadrangle, shape_cube, add_numbering, add_function,
	// set_constant_length, set_variable_length, set_towards, set_with_triangles,
	// set_format, set_file_name, set_import_numbering, define_vertices,
	// set_swatch, set_track, seg_follow_curvature, set_follow_torsion, add_cell
	//   virtual from Mesh::Build::Core, defined there, execution forbidden
	
	void add_mesh ( const Mesh & msh ) override;  // virtual from Mesh::Build::Core

	Mesh build_mesh ( ) override;  // virtual from Mesh::Build::Core
	
};  // end of class Mesh::Build::Join


inline Mesh::Build::Build ( const tag::Join & )
:	Mesh::Build::Build ( tag::whose_core_is, std::make_shared < Mesh::Build::Join > () )
{	}

//------------------------------------------------------------------------------------------------------//


class Mesh::Composite::Build::Gather : public Mesh::Build::Core

// builder for gathering several heterogeneous objects into a Mesh::Composite

{	public :

	std::map < std::string, std::vector < Cell > > points { };
	std::map < std::string, std::vector < Mesh > > meshes { };
	std::map < std::string, std::vector < Function > > functions { };
	Cell::Numbering numbering { tag::non_existent };

	inline Gather ( )
	:	Mesh::Build::Core ()
	{	}

	// define_faces, set_winding, set_winding_true, set_singular, set_boundary,
	// set_entire_manifold, set_orientation, start_at, stop_at, build_mesh,
	// shape_segment, shape_triangle, shape_quadrangle, shape_cube,
	// set_constant_length, set_variable_length, set_towards, set_with_triangles,
	// set_format, set_file_name, set_import_numbering, define_vertices,
	// set_swatch, set_track, seg_follow_curvature, set_follow_torsion,
	//   virtual from Mesh::Build::Core, defined there, execution forbidden

	// four methods below virtual from Mesh::Build::Core
	void add_cell ( const std::string & name, const Cell & cll ) override;  
	void add_mesh ( const std::string & name, const Mesh & msh ) override;  
	void add_function ( const std::string & name, const Function & f ) override;  
	void add_numbering ( const Cell::Numbering & n ) override;  

	Mesh::Composite build_composite_mesh ( ) override;
	// virtual from Mesh::Build::Core, defined in global.cpp
	
};  // end of class Mesh::Composite::Build::Gather


inline Mesh::Build::Build ( const tag::Gather & )
  :	Mesh::Build::Build ( tag::whose_core_is, std::make_shared < Mesh::Composite::Build::Gather > () )
{	}

//------------------------------------------------------------------------------------------------------//


// classes derived from  Mesh::Build::Grid  are defined in mesh.cpp

class Mesh::Build::Grid : public Mesh::Build::Core

// builder for grids of segments, of triangles, of quadrilaterals, of hexahedra

{	public :

	#ifndef MANIFEM_NO_QUOTIENT
	bool winding { false };
	#endif  // ifndef MANIFEM_NO_QUOTIENT
	// 'winding' provides no specific information,
	// it just warns maniFEM that we are on a quotient manifold
	// and that it must take winding segments into account
	// specific information about winding numbers is included in sides

	Cell singular { tag::non_existent };
	// a vertex where special attention must be given, like the vertex of a cone
	
	inline Grid ( )
	:	Mesh::Build::Core ()
	{	}

	// define_faces, set_winding, set_boundary,
	// set_entire_manifold, set_orientation, start_at, stop_at,
	// set_constant_length, set_variable_length, set_towards, set_with_triangles,
	// set_format, set_file_name, set_import_numbering, define_vertices,
	// set_swatch, set_track, seg_follow_curvature, set_follow_torsion,
	// build_composite_mesh, add_mesh, add_cell, add_numbering, add_function
	//   virtual from Mesh::Build::Core, defined there, execution forbidden

	#ifndef MANIFEM_NO_QUOTIENT
	void set_winding_true  ( ) override; // virtual from Mesh::Build::Core
	#endif  // ifndef MANIFEM_NO_QUOTIENT
	void set_singular ( const Cell & cll ) override;  // virtual from Mesh::Build::Core

	Mesh::Build shape_segment ( )  override;    // virtual from Mesh::Build::Core
	Mesh::Build shape_triangle ( ) override;    // virtual from Mesh::Build::Core
	Mesh::Build shape_quadrangle ( ) override;  // virtual from Mesh::Build::Core
	Mesh::Build shape_cube ( ) override;        // virtual from Mesh::Build::Core

	class Segment;  class Triangle;  class Quadrangle;  class Tetrahedron;  class Hexahedron;
	// defined in mesh.cpp
	
};  // end of class Mesh::Build::Grid


inline Mesh::Build::Build ( const tag::Grid & )
:	Mesh::Build::Build ( tag::whose_core_is, std::make_shared < Mesh::Build::Grid > () )
{	}

//------------------------------------------------------------------------------------------------------//


#ifndef MANIFEM_NO_FRONTAL

// classes derived from  Mesh::Build::Frontal  are defined in frontal.cpp

class Mesh::Build::Frontal : public Mesh::Build::Core

// builder for frontal mesh generation
// abstract class, instantiated in Mesh::Build::Frontal::***

{	public :

	tag::OrientationChoice orientation { tag::not_provided };
	bool entire_manifold { false };
	// if entire_manifold, we do not accept declaration of boundary or start or stop points
	Cell start { tag::non_existent };
	bool ready_for_towards { false }, ready_for_stop_at { false };
	std::vector<double> towards;
		
	inline Frontal ( )
	:	Mesh::Build::Core ()
	{	}

	inline Frontal ( const tag::StartAt &, const Cell & cll )
	:	Mesh::Build::Core (), start ( cll )
	{	}

	// define_faces, set_winding, set_winding_true, set_singular, add_mesh, add_cell,
	// shape_segment, shape_triangle, shape_quadrangle, shape_cube,
	// set_with_triangles, build_composite_mesh, add_numbering, add_function,
	// set_format, set_file_name, set_import_numbering, define_vertices,
	// set_swatch, set_track, seg_follow_curvature, set_follow_torsion
	//   virtual from Mesh::Build::Core, defined there, execution forbidden

	// virtual from Mesh::Build::Core, defined in frontal.cpp
	Mesh::Build set_constant_length ( double l ) override;
	Mesh::Build set_variable_length ( const Function & l ) override;
	Mesh::Build set_boundary ( const Mesh & msh ) override;
	Mesh::Build stop_at ( const Cell & cll ) override;
	void start_at ( const Cell & cll ) override;
	void set_towards ( const std::vector < double > & dir ) override;
	void set_orientation ( const tag::OrientationChoice & oc ) override;
	void set_entire_manifold ( ) override;

	class ConstantSegLength;  class VariableSegLength;
	class FromBoundary;  class StartStopPoints;
	// defined in frontal.cpp
	
};  // end of class Mesh::Build::Frontal


inline Mesh::Build::Build ( const tag::Frontal & )
:	Mesh::Build::Build ( tag::whose_core_is, std::make_shared < Mesh::Build::Frontal > () )
{	}

#endif  // ifndef MANIFEM_NO_FRONTAL

//------------------------------------------------------------------------------------------------------//


class Mesh::Build::ImportFromFile : public Mesh::Build::Core

// builder for importing a mesh from a file

{	public :

	std::string file_name;

	inline ImportFromFile ( )
	:	Mesh::Build::Core ()
	{	}

	// define_faces, set_winding, set_winding_true, set_singular, set_boundary, 
	// set_entire_manifold, set_orientation, start_at, stop_at, add_numbering, add_function,
	// shape_segment, shape_triangle, shape_quadrangle, shape_cube, define_vertices,
	// set_constant_length, set_variable_length, set_towards, set_with_triangles,
	// set_swatch, set_track, seg_follow_curvature, set_follow_torsion, add_mesh, add_cell
	//   virtual from Mesh::Build::Core, defined there, execution forbidden

	// three methods below defined in global.cpp
	void set_format ( const tag::Gmsh & ) override;         // virtual from Mesh::Build::Core
	void set_file_name ( const std::string & s ) override;  // virtual from Mesh::Build::Core	
	Mesh::Build set_import_numbering ( ) override;          // virtual from Mesh::Build::Core

	Mesh build_mesh ( ) override;                         // virtual from Mesh::Build::Core
	Mesh::Composite build_composite_mesh ( ) override;    // virtual from Mesh::Build::Core
	
};  // end of class Mesh::Build::ImportFromFile

//------------------------------------------------------------------------------------------------------//


class Mesh::Composite::Build::ImportFromFile : public Mesh::Build::Core

// builder for importing a composite mesh from a file

{	public :

	std::string file_name;
	bool import_numbering { false };

	inline ImportFromFile ( )
	:	Mesh::Build::Core ()
	{	}

	// define_faces, set_winding, set_winding_true, set_singular, set_boundary, 
	// set_entire_manifold, set_orientation, start_at, stop_at, add_numbering, add_function,
	// shape_segment, shape_triangle, shape_quadrangle, shape_cube, define_vertices,
	// set_constant_length, set_variable_length, set_towards, set_with_triangles,
	// set_swatch, set_track, seg_follow_curvature, set_follow_torsion, add_mesh, add_cell
	//   virtual from Mesh::Build::Core, defined there, execution forbidden

	// three methods below defined in global.cpp
	void set_format ( const tag::Gmsh & ) override;         // virtual from Mesh::Build::Core
	void set_file_name ( const std::string & s ) override;  // virtual from Mesh::Build::Core	
	Mesh::Build set_import_numbering ( ) override;          // virtual from Mesh::Build::Core

	using Mesh::Build::Core::build_mesh;                  // virtual, execution forbidden
	Mesh::Composite build_composite_mesh ( ) override;    // virtual from Mesh::Build::Core
	
};  // end of class Mesh::Composite::Build::ImportFromFile


inline Mesh::Build::Build ( const tag::ImportFromFile & )
:	Mesh::Build::Build ( tag::whose_core_is, std::make_shared < Mesh::Build::ImportFromFile > () )
{	}

//------------------------------------------------------------------------------------------------------//


class Mesh::Build::Extrude : public Mesh::Build::Core

// builder for extruded meshes

{	public :

	Mesh swatch { tag::non_existent }, track { tag::non_existent };
	Cell common_vertex { tag::non_existent };
	bool follow_curvature { false }, follow_torsion { false };
	
	inline Extrude ( )
	:	Mesh::Build::Core ()
	{	}

	// define_faces, set_winding, set_winding_true, set_singular, set_boundary, 
	// set_entire_manifold, set_orientation, start_at, stop_at, add_numbering, add_function,
	// shape_segment, shape_triangle, shape_quadrangle, shape_cube,
	// set_constant_length, set_variable_length, set_towards, set_with_triangles,
	// set_format, set_file_name, set_import_numbering, define_vertices, add_mesh, add_cell
	//   virtual from Mesh::Build::Core, defined there, execution forbidden

	void set_swatch ( const Mesh & msh ) override;  // virtual from Mesh::Build::Core
	Mesh::Build set_swatch ( const Mesh::Composite & msh ) override;  // virtual from Mesh::Build::Core
	void set_track ( const Mesh & msh ) override;   // virtual from Mesh::Build::Core
	Mesh::Build set_track ( const Mesh::Composite & msh ) override;   // virtual from Mesh::Build::Core
	void set_follow_curvature ( ) override;         // virtual from Mesh::Build::Core
	void set_follow_torsion ( ) override;           // virtual from Mesh::Build::Core

	// two methods below virtual from Mesh::Build::Core, defined in extrude.cpp
	Mesh build_mesh ( ) override;
	Mesh::Composite build_composite_mesh ( ) override;

	class Composite;

};  // end of class Mesh::Build::Extrude


inline Mesh::Build::Build ( const tag::Extrude & )
:	Mesh::Build::Build ( tag::whose_core_is, std::make_shared < Mesh::Build::Extrude > () )
{	}

//------------------------------------------------------------------------------------------------------//


class Mesh::Build::Extrude::Composite : public Mesh::Build::Core

// builder for extruded meshes

{	public :

	Mesh::Composite swatch { }, track { };
	bool follow_curvature { false }, follow_torsion { false };
	
	inline Composite ( )
	:	Mesh::Build::Core ()
	{	}

	// define_faces, set_winding, set_winding_true, set_singular, set_boundary, 
	// set_entire_manifold, set_orientation, start_at, stop_at, add_numbering, add_function,
	// shape_segment, shape_triangle, shape_quadrangle, shape_cube,
	// set_constant_length, set_variable_length, set_towards, set_with_triangles,
	// set_format, set_file_name, set_import_numbering, define_vertices, add_mesh, add_cell
	//   virtual from Mesh::Build::Core, defined there, execution forbidden

	void set_swatch ( const Mesh & msh ) override;  // virtual from Mesh::Build::Core
	Mesh::Build set_swatch ( const Mesh::Composite & msh ) override;  // virtual from Mesh::Build::Core
	void set_track ( const Mesh & msh ) override;   // virtual from Mesh::Build::Core
	Mesh::Build set_track ( const Mesh::Composite & msh ) override;   // virtual from Mesh::Build::Core
	void set_follow_curvature ( ) override;         // virtual from Mesh::Build::Core
	void set_follow_torsion ( ) override;           // virtual from Mesh::Build::Core

	// two methods below virtual from Mesh::Build::Core, defined in extrude.cpp
	Mesh build_mesh ( ) override;
	Mesh::Composite build_composite_mesh ( ) override;

};  // end of class Mesh::Build::Extrude::Composite

//------------------------------------------------------------------------------------------------------//

}  // namespace maniFEM

//------------------------------------------------------------------------------------------------------//

#ifndef OMIT_OBSOLETE_CODE

using namespace maniFEM;

namespace std {

template < >
struct less < Mesh >
{	inline bool operator() ( const Mesh & m1, const Mesh & m2 ) const
	{	if ( m1 .core == m2 .core )
			return  m1 .is_positive() and not m2 .is_positive();
		return  m1 .core < m2 .core;                             }
	typedef Mesh first_argument_type;
	typedef Mesh second_argument_type;
	typedef bool result_type;                                          };

}  // namespace std

#endif  // ifndef OMIT_OBSOLETE_CODE

#endif  // ifndef MANIFEM_MESH_H

