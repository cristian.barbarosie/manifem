
//   manifold.h  2024.10.31

//   This file is part of maniFEM, a C++ library for meshes and finite elements on manifolds.

//   Copyright  2019 - 2024  Cristian Barbarosie  cristian.barbarosie@gmail.com

//   https://maniFEM.rd.ciencias.ulisboa.pt/
//   https://codeberg.org/cristian.barbarosie/maniFEM

//   ManiFEM is free software: you can redistribute it and/or modify it
//   under the terms of the GNU Lesser General Public License as published
//   by the Free Software Foundation, either version 3 of the License
//   or (at your option) any later version.

//   ManiFEM is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//   See the GNU Lesser General Public License for more details.

//   You should have received a copy of the GNU Lesser General Public License
//   along with maniFEM.  If not, see <https://www.gnu.org/licenses/>.


#ifndef MANIFEM_MANIFOLD_H
#define MANIFEM_MANIFOLD_H

#include <iostream>
#include <vector>
#include <forward_list>
#include "assert.h"

#include "mesh.h"
#include "field.h"
#include "function.h"


namespace maniFEM {

namespace tag
{	struct euclid { };  static const euclid Euclid;
	struct Implicit { };  static const Implicit implicit;
	struct Intersect { };  static const Intersect intersect;
	struct Parametric { };  static const Parametric parametric;
	struct Isotropic { };  static const Isotropic isotropic;
	struct M11M12M22 { };  static const M11M12M22 m11_m12_m22;
	static const M11M12M22 a11_a12_a22;  static const M11M12M22 d11_d12_d22;
	struct M11M12M13M22M23M33 { };  static const M11M12M13M22M23M33 m11_m12_m13_m22_m23_m33;
	static const M11M12M13M22M23M33 a11_a12_a13_a22_a23_a33;
	static const M11M12M13M22M23M33 d11_d12_d13_d22_d23_d33;
	struct SquareRoot { };  static const SquareRoot square_root;
	struct InnerSpectralRadius { };  static const InnerSpectralRadius inner_spectral_radius;
	struct DoNotSetAsWorking { };  static const DoNotSetAsWorking do_not_set_as_working;
	struct StartWithInconsistentMesh { };
	static const StartWithInconsistentMesh start_with_inconsistent_mesh;
	struct StartWithNonExistentMesh { };
	static const StartWithNonExistentMesh start_with_non_existent_mesh;
	struct AtPoint { };  static const AtPoint at_point ;
	struct UseInformationFrom { };  static const UseInformationFrom use_information_from;   }

//------------------------------------------------------------------------------------------------------//
//------------------------------------------------------------------------------------------------------//

// class below will allow us to easily manipulate the metric of a manifold

// Metric cores are cheap to copy/clone, so we use a unique_ptr
	
class tag::Util::Metric

{	public :

	typedef tag::Util::MetricCore Core;

	std::unique_ptr < tag::Util::Metric::Core > core;

	inline Metric ( );  // execution forbidden

	inline Metric ( const tag::WhoseCoreIs &, tag::Util::Metric::Core * mc );
	
	inline Metric ( const Metric & m );   // clone core of 'm'

	inline Metric ( Metric && m );

	inline Metric & operator= ( const Metric & m );   // clone core of 'm'

	inline Metric & operator= ( Metric && m );

	class Trivial;  class Constant;  class Variable;  class ScaledSq;  class ScaledSqInv;

	// twelve static methods below are used in frontal.cpp, defined there
	// a Metric::Core will keep six pointers to six of these functions
	
	#ifndef MANIFEM_NO_FRONTAL

	static void frontal_method_nw_inact_oc
	( Mesh & msh, const tag::Boundary &, const Mesh & interface,
	  const tag::StartAt &, const Cell & start,
	  const tag::Orientation &, const tag::OrientationChoice & oc );

	static void frontal_method_nw_active_oc
	( Mesh & msh, const tag::Boundary &, const Mesh & interface,
	  const tag::StartAt &, const Cell & start,
	  const tag::Orientation &, const tag::OrientationChoice & oc );

	static void frontal_method_nw_inact_tow
	( Mesh & msh, const tag::Boundary &, const Mesh & bdry,
	  const tag::StartAt &, const Cell & start,
	  const tag::Towards &, const std::vector < double > & normal );

	static void frontal_method_nw_active_tow
	( Mesh & msh, const tag::Boundary &, const Mesh & bdry,
	  const tag::StartAt &, const Cell & start,
	  const tag::Towards &, const std::vector < double > & normal );

	static void frontal_method_nw_inact_nem
	( Mesh & msh, const tag::StartWithNonExistentMesh &,
	  const tag::StartAt &, const Cell & start,
	  const tag::Orientation &, const tag::OrientationChoice & oc );
	
	static void frontal_method_nw_active_nem
	( Mesh & msh, const tag::StartWithNonExistentMesh &,
	  const tag::StartAt &, const Cell & start,
	  const tag::Orientation &, const tag::OrientationChoice & oc );
	
	#ifndef MANIFEM_NO_QUOTIENT
  
	static void frontal_method_q_inact_oc
	( Mesh & msh, const tag::Boundary &, const Mesh & interface,
	  const tag::StartAt &, const Cell & start,
	  const tag::Orientation &, const tag::OrientationChoice & oc );

	static void frontal_method_q_active_oc
	( Mesh & msh, const tag::Boundary &, const Mesh & interface,
	  const tag::StartAt &, const Cell & start,
	  const tag::Orientation &, const tag::OrientationChoice & oc );

	static void frontal_method_q_inact_tow
	( Mesh & msh, const tag::Boundary &, const Mesh & bdry,
	  const tag::StartAt &, const Cell & start,
	  const tag::Towards &, const std::vector < double > & normal );

	static void frontal_method_q_active_tow
	( Mesh & msh, const tag::Boundary &, const Mesh & bdry,
	  const tag::StartAt &, const Cell & start,
	  const tag::Towards &, const std::vector < double > & normal );

	static void frontal_method_q_inact_nem
	( Mesh & msh, const tag::StartWithNonExistentMesh &,
	  const tag::StartAt &, const Cell & start,
	  const tag::Orientation &, const tag::OrientationChoice & oc );
	
	static void frontal_method_q_active_nem
	( Mesh & msh, const tag::StartWithNonExistentMesh &,
	  const tag::StartAt &, const Cell & start,
	  const tag::Orientation &, const tag::OrientationChoice & oc );

	#endif // ifndef MANIFEM_NO_QUOTIENT
  
	inline static double compute_avrg_sr ( const Tensor < double > & m );

	#endif  // ifndef MANIFEM_NO_FRONTAL

};  // end of  class tag::Util::Metric


#ifndef MANIFEM_NO_FRONTAL

inline double tag::Util::Metric::compute_avrg_sr ( const Tensor < double > & matrix )  // static

{	assert ( matrix .dimensions .size() == 2 );
	size_t n = matrix .dimensions [0];
	assert ( n == matrix .dimensions [1] );
	double sum = 0.;
	assert ( n < directions_few_double .size() );
	const std::vector < std::vector < double > > & direc = tag::Util::directions_few_double [n];
	for ( std::vector < std::vector < double > > ::const_iterator
	        it = direc .begin(); it != direc .end(); it++         )
	{	const std::vector < double > & v = *it;
		assert ( v .size() == n );
		for ( size_t i = 0; i < n; i++ )
		{	double tmp = 0.;
			for ( size_t j = 0; j < n; j++ )
				tmp += matrix(i,j) * v[j];
			sum += tmp * v[i];                }    }
	return sum / direc .size();                                                                    }

#endif  // ifndef MANIFEM_NO_FRONTAL

//------------------------------------------------------------------------------------------------------//


// in frontal.cpp, we keep (in Cell::Core::hook) various informations :
// for codim 1 (surface in 3D) we build normals using information from neighbour cells
// for codim 0 (pure 2D or 3D) we use  det A  A^-2  times the exterior product
//                             i.e.    sqrt det M  M^-1  times the exterior product

// tag::Util::Metric::Variable::Matrix::Simple
// codim 1 :  inverse of M, inner spectral radius (square root), eigen-direction
// codim 0 :  the above plus  square root of det M

// tag::Util::Metric::Variable::Matrix::SquareRoot
// codim 1 :  inverse of A, inner spectral radius, eigen-direction
// codim 0 :  same as above

// tag::Util::Metric::Variable::Matrix::ISR
// codim 1 :  inner spectral radius (square root)
// codim 0 :  the above plus  inverse of M, square root of det M

// tag::Util::Metric::Variable::Matrix::SquareRoot::ISR
// codim 1 :  
// codim 0 :  inverse of A

//------------------------------------------------------------------------------------------------------//


class tag::Util::MetricCore	 // aka  class tag::Util::Metric::Core

// abstract class, specialized in tag::Util::Metric::Trivial, Constant::***, Variable::***

{	public :

	// seven pointers to functions, more precisely, pointers to static methods
	// used in frontal.cpp

	#ifndef MANIFEM_NO_FRONTAL

	void ( * frontal_method_nw_oc )
	( Mesh & msh, const tag::Boundary &, const Mesh & interface,
	  const tag::StartAt &, const Cell & start,
	  const tag::Orientation &, const tag::OrientationChoice & oc );

	void ( * frontal_method_nw_tow )
	( Mesh & msh, const tag::Boundary &, const Mesh & bdry,
	  const tag::StartAt &, const Cell & start,
	  const tag::Towards &, const std::vector < double > & normal );

	void ( * frontal_method_nw_nem )
	( Mesh & msh, const tag::StartWithNonExistentMesh &,
	  const tag::StartAt &, const Cell & start,
	  const tag::Orientation &, const tag::OrientationChoice & oc );
	
	#ifndef MANIFEM_NO_QUOTIENT
  
	void ( * frontal_method_q_oc )
	( Mesh & msh, const tag::Boundary &, const Mesh & interface,
	  const tag::StartAt &, const Cell & start,
	  const tag::Orientation &, const tag::OrientationChoice & oc );
	
	void ( * frontal_method_q_tow )
	( Mesh & msh, const tag::Boundary &, const Mesh & bdry,
	  const tag::StartAt &, const Cell & start,
	  const tag::Towards &, const std::vector < double > & normal );

	void ( * frontal_method_q_nem )
	( Mesh & msh, const tag::StartWithNonExistentMesh &,
	  const tag::StartAt &, const Cell & start,
	  const tag::Orientation &, const tag::OrientationChoice & oc );
  
	#endif // ifndef MANIFEM_NO_QUOTIENT
	
	void ( * remove_node_isr_p ) ( const Cell & V );

	static void remove_node_isr_forbid ( const Cell & V );   // defined in frontal.cpp	
	
	#ifndef MANIFEM_NO_QUOTIENT
  
	inline MetricCore ( )
	:	frontal_method_nw_oc  { & tag::Util::Metric::frontal_method_nw_inact_oc   },
		frontal_method_nw_tow { & tag::Util::Metric::frontal_method_nw_inact_tow  },
		frontal_method_nw_nem { & tag::Util::Metric::frontal_method_nw_inact_nem  },
		frontal_method_q_oc   { & tag::Util::Metric::frontal_method_q_inact_oc    },
		frontal_method_q_tow  { & tag::Util::Metric::frontal_method_q_inact_tow   },
		frontal_method_q_nem  { & tag::Util::Metric::frontal_method_q_inact_nem   },
		remove_node_isr_p     { & tag::Util::Metric::Core::remove_node_isr_forbid }
	{	}

	#else  // MANIFEM_NO_QUOTIENT
	
	inline MetricCore ( )
	:	frontal_method_nw_oc  { & tag::Util::Metric::frontal_method_nw_inact_oc   },
		frontal_method_nw_tow { & tag::Util::Metric::frontal_method_nw_inact_tow  },
		frontal_method_nw_nem { & tag::Util::Metric::frontal_method_nw_inact_nem  },
		remove_node_isr_p     { & tag::Util::Metric::Core::remove_node_isr_forbid }
	{	}

	#endif  // ifndef MANIFEM_NO_QUOTIENT
  
	#else  // MANIFEM_NO_FRONTAL
	
	inline MetricCore ( )
	{	}
	
	#endif  // ifndef MANIFEM_NO_FRONTAL

	virtual ~MetricCore ( );

	virtual tag::Util::MetricCore & operator= ( tag::Util::MetricCore & ) = delete;
	virtual tag::Util::MetricCore & operator= ( tag::Util::MetricCore && ) = delete;

	virtual tag::Util::MetricCore * clone ( ) = 0;

	virtual tag::Util::MetricCore * regist ( );
	// here execution forbidden, later overridden

	virtual bool isotropic ( ) = 0;

	virtual double inner_prod  // only for tag::Util::Metric::Constant
	( const std::vector < double > & v, const std::vector < double > & w ) = 0;	

	virtual double inner_prod
	( const Cell & P, const std::vector < double > & v, const std::vector < double > & w ) = 0;
	
	virtual double inner_prod         // later overridden for several different reasons
	( const Cell & P, const Cell & Q,
	  const std::vector < double > & v, const std::vector < double > & w );	

	virtual double inner_spectral_radius ( const Cell & A ) = 0;
	// for isotropic metric, return the zoom at A, aka the length of unit
	// for anisotropic metric, return the minimum zoom = square root of inner spectral radius
	// that is, the inverse of the spectral radius of the inverse matrix (square root)

	virtual double avrg_spectral_radius ( const Cell & A );
	// just a rough estimate (if the metric is anisotropic)
	// return inner_spectral_radius, overridden for anisotropic metrics
	// used in search_start_ver, file frontal.cpp
	
	// metamorphosis : produce a modified copy of self (a new, scaled, metric)
	virtual tag::Util::MetricCore * scale_sq ( const Function & f ) = 0;
	virtual tag::Util::MetricCore * scale_sq ( const double f ) = 0;
	virtual tag::Util::MetricCore * scale_sq_inv ( const Function & f ) = 0;
	virtual tag::Util::MetricCore * scale_sq_inv ( const double f ) = 0;

	// compute (square of) distance between two points, trusting that  w == AB
	inline double sq_dist ( const Cell & A, const Cell & B, const std::vector < double > & w );

	// compute (square of) distance between A and A+e
	inline double sq_dist ( const Cell & A, const std::vector < double > & e );

	virtual void compute_data ( const tag::AtPoint &, const Cell & V );
	virtual void compute_data
	( const tag::AtPoint &, const Cell & W, const tag::UseInformationFrom &, const Cell & V );
	// here execution forbidden, later overridden by metrics which interact with the container

	virtual void codim_0 ( );
	virtual void codim_1 ( );
	virtual void de_activate ( );
	// here execution forbidden, later overridden by metrics which interact with the container

	virtual void correct_ext_prod_2d ( const Cell & P, std::vector < double > & n ) = 0;
	virtual void correct_ext_prod_3d ( const Cell & P, std::vector < double > & n ) = 0;

};  // end of  class tag::Util::MetricCore

//------------------------------------------------------------------------------------------------------//


inline tag::Util::Metric::Metric ( )  { assert ( false );  }

inline tag::Util::Metric::Metric ( const tag::WhoseCoreIs &, tag::Util::Metric::Core * mc )
:	core { mc }
{	}

inline tag::Util::Metric::Metric ( const Metric & m )
:	core { m .core->clone() }
{	}

inline tag::Util::Metric::Metric ( Metric && m )
:	core { std::move ( m .core ) }
{	}

inline tag::Util::Metric & tag::Util::Metric::operator= ( const Metric & m )
{	this->core .reset ( m .core->clone() );
	return *this;                           }

inline tag::Util::Metric & tag::Util::Metric::operator= ( Metric && m )
{	this->core = std::move ( m .core );
	return *this;                       }

//------------------------------------------------------------------------------------------------------//

	
inline double tag::Util::Metric::Core::sq_dist  // here we trust that w == AB
( const Cell & A, const Cell & B, const std::vector < double > & w )
{	return this->inner_prod ( A, B, w, w );  }
		
	
inline double tag::Util::Metric::Core::sq_dist
( const Cell & A, const std::vector < double > & e )
{	return this->inner_prod ( A, e, e );  }
		
//------------------------------------------------------------------------------------------------------//


class tag::Util::Metric::Trivial : public tag::Util::Metric::Core

{	public :

	// seven pointers to functions, more precisely, pointers to static methods
	// inherited from tag::Util::Metric::Core :
	// frontal_method_nw_***, frontal_method_q_***, remove_node_isr_p

	inline Trivial ( ) : tag::Util::Metric::Core() { }
	virtual ~Trivial ( );

	using tag::Util::Metric::Core::operator=;
	// operator=, copy and move, deleted

	tag::Util::MetricCore * clone ( );  // virtual from tag::Util::Metric::Core

	// tag::Util::MetricCore * regist ( )   virtual from tag::Util::Metric::Core
	// defined there, execution forbidden

	bool isotropic ( );  // virtual from tag::Util::Metric::Core
	
	double inner_prod ( const std::vector < double > & v, const std::vector < double > & w );
	// virtual from tag::Util::Metric::Core

	double inner_prod  // virtual from tag::Util::Metric::Core
	( const Cell & P, const std::vector < double > & v, const std::vector < double > & w );	

	double inner_prod ( const Cell & P, const Cell & Q,  // virtual from tag::Util::Metric::Core
	  const std::vector < double > & v, const std::vector < double > & w ) override;	

	double inner_spectral_radius ( const Cell & A );  // here, could be called length_of_unit
	// virtual from tag::Util::Metric::Core, here return 1.

	// double avrg_spectral_radius ( const Cell & A )
	// virtual from tag::Util::Metric::Core, defined there, return inner_spectral_radius
	
	// two 'scale_sq' and two 'scale_sq_inv' methods are virtual from tag::Util::Metric::Core
	tag::Util::MetricCore * scale_sq ( const Function & f );
	tag::Util::MetricCore * scale_sq ( const double f );
	tag::Util::MetricCore * scale_sq_inv ( const Function & f );
	tag::Util::MetricCore * scale_sq_inv ( const double f );

	// inlined 'sq_dist' defined by tag::Util::Metric::Core

	// two versions of  compute_data
	// virtual from tag::Util::Metric::Trivial, defined there, execution forbidden

	// methods codim_0, codim_1, de_activate virtual from tag::Util::MetricCore,
	// defined there, execution forbidden

	void correct_ext_prod_2d ( const Cell & P, std::vector < double > & n );
	void correct_ext_prod_3d ( const Cell & P, std::vector < double > & n );
	// virtual from tag::Util::MetricCore, defined in frontal.cpp, here does nothing
	
};  // end of  class tag::Util::Metric::Trivial

//------------------------------------------------------------------------------------------------------//

// many other types of Metric::Core in manifold-xx.h

//------------------------------------------------------------------------------------------------------//


// Manifold core will be shared
// for instance, implicit manifods keep a base (Euclidian) manifold
// we could use unique_ptr like for Metrics
// we prefer a shared_ptr
	
//------------------------------------------------------------------------------------------------------//
//------------------------------------------------------------------------------------------------------//


class Manifold

{	public :

	class Core;

	std::shared_ptr < Manifold::Core > core;

	inline Manifold ( const tag::WhoseCoreIs &, std::shared_ptr < Manifold::Core > c )
	:	core ( c )
	{	assert ( c );
		Manifold::working = *this;  }

	inline Manifold ( const tag::NonExistent & ) : core ( nullptr ) { }

	Manifold ( const tag::euclid &, const tag::OfDimension &, const size_t dim );

	Manifold ( const tag::euclid &, const tag::OfDimension &, const size_t dim,
	                  const tag::DoNotSetAsWorking &                                  );

	Manifold ( const tag::Implicit &, const Manifold &, const Function & );
	Manifold ( const tag::Implicit &, const Manifold &, const Function &, const Function & );
	Manifold ( const tag::Implicit &,
	           const Manifold &, const Function &, const Function &, const Function & );
	
	Manifold ( const tag::Intersect &, const Manifold &, const Manifold & );
	Manifold ( const tag::Intersect &, const Manifold &, const Manifold &, const Manifold & );
	
	Manifold ( const tag::Parametric &, const Manifold &, const Function::Equality & );
	Manifold ( const tag::Parametric &,
	           const Manifold &, const Function::Equality &, const Function::Equality & );
	Manifold ( const tag::Parametric &, const Manifold &,
	           const Function::Equality &, const Function::Equality &, const Function::Equality & );

	inline ~Manifold ( )
	{	} //	std::cout << "destructor Manifold" << std::endl;
	
	inline Function coordinates ( ) const;
	inline void set_coordinates ( const Function co );
	// inline void set_coordinates ( Function & ) const;

	inline Function build_coordinate_system
	( const tag::lagrange &, const tag::OfDegree &, size_t d );

	// a non-existent manifold has null core
	inline bool exists ( ) const { return core != nullptr; }

	inline size_t topological_dim () const;
	
	// compute (square of) distance between two points
	// the first version computes the (coordinates of the) vector AB
	// in the second version, B = A+e
	// the third version trusts that  w == AB
	inline double sq_dist ( const Cell & A, const Cell & B );
	inline double sq_dist ( const Cell & A, const std::vector < double > & e );
	inline double sq_dist ( const Cell & A, const Cell & B, const std::vector < double > & w );

	void build_intersection_of_two_manif ( const Manifold & m1, const Manifold & m2 );
		
	// measure of the entire manifold (length for 1D, area for 2D, volume for 3D)
	// produces run-time error for Euclidian manifolds
	// and for other manifolds whose measure is infinite (e.g. cylinder)
	// and for manifolds whose measure is too difficult to compute (e.g. implicit manifolds)
	// significant for torus
 	inline double measure ( ) const;
	
	// metric in the manifold (an inner product on the tangent space)
	inline double inner_prod ( const Cell & P, const std::vector<double> & v,
                                             const std::vector<double> & w ) const;
	inline double inner_prod ( const Cell & P, const Cell & Q,
		const std::vector<double> & v, const std::vector<double> & w ) const;

	void set_metric ( const tag::Isotropic &, const double s );
	void set_metric ( const tag::Isotropic &, const Function & s );
	void set_metric ( const tag::M11M12M22 &,
	                         const double m11, const double m12, const double m22 );
	void set_metric ( const tag::M11M12M22 &,
	                         const Function & m11, const Function & m12, const Function & m22 );
	void set_metric ( const tag::SquareRoot &, const tag::M11M12M22 &,
	                         const double m11, const double m12, const double m22 );
	void set_metric ( const tag::SquareRoot &, const tag::M11M12M22 &,
	                         const Function & m11, const Function & m12, const Function & m22 );
	void set_metric ( const tag::InnerSpectralRadius &, const Function & sr,
	    const tag::M11M12M22 &, const Function & m11, const Function & m12, const Function & m22 );
	void set_metric ( const tag::SquareRoot &,
	    const tag::InnerSpectralRadius &, const Function & sr,
	    const tag::M11M12M22 &, const Function & m11, const Function & m12, const Function & m22 );
	
	typedef tag::Util::Action Action;  // aka  class Function::Action
	// we define it in function.h because we need it for Function::MultiValued
	// but we prefer the user to see it as an attribute of class Manifold

	// P = sA + sB,  s+t == 1
	inline void interpolate
	( const Cell & P, double s, const Cell & A, double t, const Cell & B ) const;

	// P = sA + sB + uC + vD,  s+t+u+v == 1
	inline void interpolate ( const Cell & P, double s, const Cell & A,
	  double t, const Cell & B, double u, const Cell & C, double v, const Cell & D ) const;

	// P = sA + sB + uC + vD + wE + zF,  s+t+u+v+w+z == 1
	inline void interpolate ( const Cell & P, double s, const Cell & A,
	  double t, const Cell & B, double u, const Cell & C, double v, const Cell & D,
	  double w, const Cell & E, double z, const Cell & F                           ) const;

	// P = sA + sB + uC,  s+t+u == 1
	inline void interpolate ( const Cell & P, double s, const Cell & A,
	  double t, const Cell & B, double u, const Cell & C               ) const;

	// P = sum c_k P_k,  sum c_k == 1
	inline void interpolate
	( const Cell & P, const std::vector < double > & coefs,
	  const std::vector < Cell > & points                  ) const;

	#ifndef MANIFEM_NO_QUOTIENT
	
	// P = sA + sB,  s+t == 1
	inline void interpolate
	( const Cell & P, double s, const Cell & A, double t, const Cell & B,
	  const tag::Winding &, const Manifold::Action & exp                 ) const;

	// P = sA + sB + uC,  s+t+u == 1
	inline void interpolate ( const Cell & P, double s, const Cell & A,
	  double t, const Cell & B, const tag::Winding &, const Manifold::Action &,
	  double u, const Cell & C, const tag::Winding &, const Manifold::Action & ) const;

	// P = sA + sB + uC + vD,  s+t+u+v == 1
	inline void interpolate ( const Cell & P, double s, const Cell & A,
	  double t, const Cell & B, const tag::Winding &, const Manifold::Action &,
	  double u, const Cell & C, const tag::Winding &, const Manifold::Action &,
	  double v, const Cell & D, const tag::Winding &, const Manifold::Action & ) const;

	// P = sA + sB + uC + vD + wE + zF,  s+t+u+v+w+z == 1
	inline void interpolate ( const Cell & P, double s, const Cell & A,
	  double t, const Cell & B, const tag::Winding &, const Manifold::Action &,
	  double u, const Cell & C, const tag::Winding &, const Manifold::Action &,
	  double v, const Cell & D, const tag::Winding &, const Manifold::Action &,
	  double w, const Cell & E, const tag::Winding &, const Manifold::Action &,
	  double z, const Cell & F, const tag::Winding &, const Manifold::Action & ) const;

	// P = sum c_k P_k,  sum c_k == 1
	inline void interpolate
	( const Cell & P, const std::vector < double > & coefs,
	  const std::vector < Cell > & points, const tag::Winding &,
	  const std::vector < Manifold::Action >  ) const;

	#endif  // ifndef MANIFEM_NO_QUOTIENT

	inline void project ( const Cell & ) const;
	
	inline Manifold implicit ( const Function::Equality eq ) const;
	inline Manifold implicit ( const Function::Equality eq1,
	                           const Function::Equality eq2 ) const;
	inline Manifold parametric ( const Function::Equality eq ) const;
	inline Manifold parametric ( const Function::Equality eq1,
	                             const Function::Equality eq2 ) const;
	inline Manifold parametric ( const Function::Equality eq1,
        	const Function::Equality eq2, const Function::Equality eq3 ) const;

	#ifndef MANIFEM_NO_QUOTIENT
	Manifold quotient ( const Manifold::Action & a1 );
	Manifold quotient ( const Manifold::Action & a1, const Manifold::Action & a2 );
	Manifold quotient
	( const Manifold::Action & a1, const Manifold::Action & a2, const Manifold::Action & a3 );
	#endif  // ifndef MANIFEM_NO_QUOTIENT

	static Manifold working;

	inline void set_as_working_manifold ( )
	{	Manifold::working = * this;  }

	class Euclid;  class Implicit;  class Parametric;  class Quotient;

};  // end of  class Manifold

//------------------------------------------------------------------------------------------------------//


inline Cell::Cell ( const tag::Vertex &, const tag::OfCoordinates &, const std::vector < double > & v,
                    const tag::IsPositive & ispos                                                     )
// by default, ispos = tag::is_positive, so may be called with only three arguments
:	Cell ( tag::whose_core_is, new Cell::Positive::Vertex ( tag::one_dummy_wrapper ),
         tag::move                                                                 )
{	Manifold & space = Manifold::working;
	assert ( space .exists() );  // we use the current manifold
	Function coords = space .coordinates();
	assert ( v .size() == coords .number_of ( tag::components ) );
	coords ( *this ) = v;                              }


inline Cell::Cell ( const tag::Vertex &, const tag::OfCoordinates &, const std::vector < double > & v,
                    const tag::Project &, const tag::IsPositive & ispos                               )
// by default, ispos = tag::is_positive, so may be called with only three arguments
:	Cell ( tag::whose_core_is, new Cell::Positive::Vertex ( tag::one_dummy_wrapper ),
         tag::move                                                                 )
{	Manifold & space = Manifold::working;
	assert ( space .exists() );  // we use the current manifold
	Function coords = space .coordinates();
	assert ( v .size() == coords .number_of ( tag::components ) );
	coords ( *this ) = v;
	space .project ( *this );                           }

//------------------------------------------------------------------------------------------------------//


class Manifold::Core

// abstract class

{	public :

	tag::Util::Metric metric;

	inline Core ( )
	:	metric ( tag::whose_core_is, new tag::Util::Metric::Trivial )
	{ }

	virtual ~Core ( ) { };

	virtual Function build_coord_func
	( const tag::lagrange &, const tag::OfDegree &, size_t d ) = 0;
	
	virtual Function get_coord_func ( ) const = 0;
	
	virtual void set_coords ( const Function co ) = 0;

	virtual size_t topological_dim () const = 0;
	
	// measure of the entire manifold (length for 1D, area for 2D, volume for 3D)
	// produces run-time error for Euclidian manifolds
	// and for other manifolds whose measure is infinite (e.g. cylinder)
	// and for manifolds whose measure is too difficult to compute (e.g. implicit manifolds)
	// significant for quotient manifolds (torus)

	virtual double measure () const = 0;
	
	virtual double sq_dist ( const Cell & A, const Cell & B ) const;
  // overridden by Manifold::Core::Quotient
	
	virtual void project ( Cell::Positive::Vertex * ) const = 0;

	// P = sA + sB,  s+t == 1
	virtual void interpolate ( Cell::Positive::Vertex * P,
	 double s, Cell::Positive::Vertex * A, double t, Cell::Positive::Vertex * B ) const = 0;

	// P = sA + sB + uC,  s+t+u == 1
	virtual void interpolate ( Cell::Positive::Vertex * P,
	  double s, Cell::Positive::Vertex * A, double t, Cell::Positive::Vertex * B,
	  double u, Cell::Positive::Vertex * C                                       ) const = 0;

	// P = sA + sB + uC + vD,  s+t+u+v == 1
	virtual void interpolate ( Cell::Positive::Vertex * P,
		double s, Cell::Positive::Vertex * A, double t, Cell::Positive::Vertex * B,
		double u, Cell::Positive::Vertex * C, double v, Cell::Positive::Vertex * D ) const = 0;

	virtual void interpolate ( Cell::Positive::Vertex * P,
		double s, Cell::Positive::Vertex * A, double t, Cell::Positive::Vertex * B,
		double u, Cell::Positive::Vertex * C, double v, Cell::Positive::Vertex * D,
		double w, Cell::Positive::Vertex * E, double z, Cell::Positive::Vertex * F ) const = 0;
	
	// P = sum c_k P_k,  sum c_k == 1
	virtual void interpolate ( Cell::Positive::Vertex * P, const std::vector < double > & coefs,
		const std::vector < Cell::Positive::Vertex * > & points ) const = 0;

	#ifndef MANIFEM_NO_QUOTIENT

	// P = sA + sB,  s+t == 1
	virtual void interpolate ( Cell::Positive::Vertex * P,
	  double s, Cell::Positive::Vertex * A, double t, Cell::Positive::Vertex * B,
	  const tag::Winding &, const Manifold::Action & exp                         ) const = 0;

	// P = sA + sB + uC,  s+t+u == 1
	virtual void interpolate ( Cell::Positive::Vertex * P,
	  double s, Cell::Positive::Vertex * A,
	  double t, Cell::Positive::Vertex * B,
	  const tag::Winding &, const Manifold::Action &,
	  double u, Cell::Positive::Vertex * C,
	  const tag::Winding &, const Manifold::Action & ) const = 0;

	// P = sA + sB + uC + vD,  s+t+u+v == 1
	virtual void interpolate ( Cell::Positive::Vertex * P,
	  double s, Cell::Positive::Vertex * A,
	  double t, Cell::Positive::Vertex * B,
	  const tag::Winding &, const Manifold::Action &,
	  double u, Cell::Positive::Vertex * C,
	  const tag::Winding &, const Manifold::Action &,
	  double v, Cell::Positive::Vertex * D,
	  const tag::Winding &, const Manifold::Action & ) const = 0;

	// P = sA + sB + uC + vD + wE + zF,  s+t+u+v+w+z == 1
	// P = sA + sB + uC + vD + wE + zF,  s+t+u+v+w+z == 1
	virtual void interpolate ( Cell::Positive::Vertex * P,
	  double s, Cell::Positive::Vertex * A,
	  double t, Cell::Positive::Vertex * B,
	  const tag::Winding &, const Manifold::Action &,
	  double u, Cell::Positive::Vertex * C,
	  const tag::Winding &, const Manifold::Action &,
	  double v, Cell::Positive::Vertex * D,
	  const tag::Winding &, const Manifold::Action &,
	  double w, Cell::Positive::Vertex * E,
	  const tag::Winding &, const Manifold::Action &,
	  double z, Cell::Positive::Vertex * F,
	  const tag::Winding &, const Manifold::Action & ) const = 0;

	#endif  // ifndef MANIFEM_NO_QUOTIENT

	#ifndef MANIFEM_NO_FRONTAL

	virtual void move_vertex  // moves V by delta, projects if necessary, updates norm_2
	( const Cell & V, std::vector < double > & delta, double & norm_2 ) const = 0;
	
	virtual void frontal_method_1d  // defined in frontal.cpp, overridden by Manifold::Quotient
	( Mesh & msh, const tag::StartWithInconsistentMesh &,
	  const tag::StartAt &, const Cell & start,
	  const tag::Towards &, std::vector<double> tangent,
	  const tag::StopAt &,  const Cell & stop            );
	
	virtual void frontal_method_1d  // defined in frontal.cpp, overridden by Manifold::Quotient
	( Mesh & msh, const tag::StartWithInconsistentMesh &,
	  const tag::StartAt &, const Cell & start,
	  const tag::StopAt &,  const Cell & stop,
	  const tag::Orientation &, const tag::OrientationChoice & oc );
	
	virtual void frontal_method_1d  // defined in frontal.cpp, overridden by Manifold::Quotient
	( Mesh & msh, const tag::StartWithInconsistentMesh &,
	  const tag::StartAt &, const Cell & start,
	  const tag::Orientation &, const tag::OrientationChoice & oc );
	
	virtual void frontal_method_1d  // defined in frontal.cpp, overridden by Manifold::Quotient
	( Mesh & msh, const tag::StartWithInconsistentMesh &,
	  const tag::StartAt &, const Cell & start,
	  const tag::StopAt &,  const Cell & stop,
	  const tag::Winding &, const Manifold::Action & g   );

	virtual void frontal_method  // defined in frontal.cpp, overridden by Manifold::Quotient
	( Mesh & msh, const tag::StartWithNonExistentMesh &,
	  const tag::StartAt &, const Cell & start,
	  const tag::Orientation &, const tag::OrientationChoice & oc );

	virtual void frontal_method  // defined in frontal.cpp, overridden by Manifold::Quotient
	( Mesh & msh, const tag::Boundary &, const Mesh & interface,
	  const tag::StartAt &, const Cell & start,
	  const tag::Towards &, const std::vector < double > & tangent );
	
	virtual void frontal_method  // defined in frontal.cpp, overridden by Manifold::Quotient
	( Mesh & msh, const tag::Boundary &, const Mesh & interface,
	  const tag::StartAt &, const Cell & seg,
	  const tag::Orientation &, const tag::OrientationChoice & oc );
	
	virtual Cell search_start_ver ( ) = 0;
	
	#endif  // ifndef MANIFEM_NO_FRONTAL
  
}; // end of class Manifold::Core

//------------------------------------------------------------------------------------------------------//


inline size_t Manifold::topological_dim () const
{	assert ( this->core );
	return this->core->topological_dim();  }


inline double Manifold::sq_dist ( const Cell & A, const Cell & B )
// this 'sq_dist' is special because, for quotient manifolds, we prefer to forbid execution
{	return this->core->sq_dist ( A, B );  }


// below B = A+e
inline double Manifold::sq_dist ( const Cell & A, const std::vector < double > & e )
{	assert ( this->core );
	assert ( this->core->metric .core );
	return this->core->metric .core->sq_dist ( A, e );  }


// below we trust w = B-A
inline double Manifold::sq_dist ( const Cell & A, const Cell & B, const std::vector < double > & w )
{	assert ( this->core );
	assert ( this->core->metric .core );
	return this->core->metric .core->sq_dist ( A, B, w );  }


inline double Manifold::inner_prod
( const Cell & P, const std::vector<double> & v, const std::vector<double> & w ) const
{	assert ( this->core );
	assert ( this->core->metric .core );
	return this->core->metric .core->inner_prod ( P, v, w );  }


inline double Manifold::inner_prod
( const Cell & P, const Cell & Q, const std::vector<double> & v, const std::vector<double> & w ) const
{	assert ( this->core );
	assert ( this->core->metric .core );
	return this->core->metric .core->inner_prod ( P, Q, v, w );  }

//------------------------------------------------------------------------------------------------------//


inline Function Manifold::coordinates ( ) const
{	assert ( this->core );
	return this->core->get_coord_func();  }


inline Function Manifold::build_coordinate_system
( const tag::lagrange &, const tag::OfDegree &, size_t d )
{	return this->core->build_coord_func ( tag::Lagrange, tag::of_degree, d );  }


inline void Manifold::set_coordinates ( const Function co )
{	assert ( this->core );
	this->core->set_coords ( co );  }


inline double Manifold::measure ( ) const
{	return this->core->measure();  }


// P = sA + sB,  s+t == 1
inline void Manifold::interpolate
( const Cell & P, double s, const Cell & A, double t, const Cell & B ) const

{	assert ( P .dim() == 0 );  assert ( A .dim() == 0 );  assert ( B .dim() == 0 );
	assert ( P .is_positive() );  assert ( A .is_positive() );  assert ( B .is_positive() );
	assert ( s >= 0. );  assert ( s <= 1. );
	assert ( t >= 0. );  assert ( t <= 1. );

	#ifndef NDEBUG  // DEBUG mode
	double sum = s + t;
	// cannot assert the sum is 1. due to round-off errors
	assert ( sum > 0.99 );  assert ( sum < 1.01 );
	#endif  // DEBUG

	Cell::Positive::Vertex * P_c = tag::Util::assert_cast
		< Cell::Core*, Cell::Positive::Vertex* > ( P.core );
	Cell::Positive::Vertex * A_c = tag::Util::assert_cast
		< Cell::Core*, Cell::Positive::Vertex* > ( A.core );
	Cell::Positive::Vertex * B_c = tag::Util::assert_cast
		< Cell::Core*, Cell::Positive::Vertex* > ( B.core );
	this->core->interpolate ( P_c, s, A_c, t, B_c );                                       }


// P = sA + sB + uC,  s+t+u == 1
inline void Manifold::interpolate
( const Cell & P, double s, const Cell & A, double t, const Cell & B,
                  double u, const Cell & C                           ) const

{	assert ( P .dim() == 0 );
	assert ( A .dim() == 0 );  assert ( B .dim() == 0 );
	assert ( C .dim() == 0 );
	assert ( P .is_positive() );
	assert ( A .is_positive() );  assert ( B .is_positive() );
	assert ( C .is_positive() );
	// in some situations, we may want to provide negative weights
	assert ( s >= 0. );  // assert ( s <= 1. );
	// assert ( t >= 0. );  assert ( t <= 1. );
	assert ( u >= 0. );  // assert ( u <= 1. );

	#ifndef NDEBUG  // DEBUG mode
	double sum = s + t + u;
	// cannot assert the sum is 1. due to round-off errors
	assert ( sum > 0.99 );  assert ( sum < 1.01 );
	#endif  // DEBUG

	Cell::Positive::Vertex * P_c = tag::Util::assert_cast
		< Cell::Core*, Cell::Positive::Vertex* > ( P.core );
	Cell::Positive::Vertex * A_c = tag::Util::assert_cast
		< Cell::Core*, Cell::Positive::Vertex* > ( A.core );
	Cell::Positive::Vertex * B_c = tag::Util::assert_cast
		< Cell::Core*, Cell::Positive::Vertex* > ( B.core );
	Cell::Positive::Vertex * C_c = tag::Util::assert_cast
		< Cell::Core*, Cell::Positive::Vertex* > ( C.core );
	this->core->interpolate ( P_c, s, A_c, t, B_c, u, C_c );  }


// P = sA + sB + uC + vD,  s+t+u+v == 1
inline void Manifold::interpolate
( const Cell & P, double s, const Cell & A, double t, const Cell & B,
                  double u, const Cell & C, double v, const Cell & D ) const

{	assert ( P .dim() == 0 );
	assert ( A .dim() == 0 );  assert ( B .dim() == 0 );
	assert ( C .dim() == 0 );  assert ( D .dim() == 0 );
	assert ( P .is_positive() );
	assert ( A .is_positive() );  assert ( B .is_positive() );
	assert ( C .is_positive() );  assert ( D .is_positive() );
	assert ( s >= 0. );  assert ( s <= 1. );
	assert ( t >= 0. );  assert ( t <= 1. );
	assert ( u >= 0. );  assert ( u <= 1. );
	assert ( v >= 0. );  assert ( v <= 1. );

	#ifndef NDEBUG  // DEBUG mode
	double sum = s + t + u + v;
	// cannot assert the sum is 1. due to round-off errors
	assert ( sum > 0.99 );  assert ( sum < 1.01 );
	#endif  // DEBUG
	
	Cell::Positive::Vertex * P_c = tag::Util::assert_cast
		< Cell::Core*, Cell::Positive::Vertex* > ( P.core );
	Cell::Positive::Vertex * A_c = tag::Util::assert_cast
		< Cell::Core*, Cell::Positive::Vertex* > ( A.core );
	Cell::Positive::Vertex * B_c = tag::Util::assert_cast
		< Cell::Core*, Cell::Positive::Vertex* > ( B.core );
	Cell::Positive::Vertex * C_c = tag::Util::assert_cast
		< Cell::Core*, Cell::Positive::Vertex* > ( C.core );
	Cell::Positive::Vertex * D_c = tag::Util::assert_cast
		< Cell::Core*, Cell::Positive::Vertex* > ( D.core );
	this->core->interpolate ( P_c, s, A_c, t, B_c, u, C_c, v, D_c );  }


// P = sA + sB + uC + vD + wE + zF,  s+t+u+v+w+z == 1
inline void Manifold::interpolate
( const Cell & P, double s, const Cell & A, double t, const Cell & B,
                  double u, const Cell & C, double v, const Cell & D,
                  double w, const Cell & E, double z, const Cell & F ) const

{	assert ( P .dim() == 0 );
	assert ( A .dim() == 0 );  assert ( B .dim() == 0 );
	assert ( C .dim() == 0 );  assert ( D .dim() == 0 );
	assert ( E .dim() == 0 );  assert ( F .dim() == 0 );
	assert ( P .is_positive() );
	assert ( A .is_positive() );  assert ( B .is_positive() );
	assert ( C .is_positive() );  assert ( D .is_positive() );
	assert ( E .is_positive() );  assert ( F .is_positive() );
	assert ( s >= 0. );  assert ( s <= 1. );
	assert ( t >= 0. );  assert ( t <= 1. );
	assert ( u >= 0. );  assert ( u <= 1. );
	assert ( v >= 0. );  assert ( v <= 1. );
	assert ( w >= 0. );  assert ( w <= 1. );
	assert ( z >= 0. );  assert ( z <= 1. );

	#ifndef NDEBUG  // DEBUG mode
	double sum = s + t + u + v + w + z;
	// cannot assert the sum is 1. due to round-off errors
	assert ( sum > 0.99 );  assert ( sum < 1.01 );
	#endif  // DEBUG
	
	Cell::Positive::Vertex * P_c = tag::Util::assert_cast
		< Cell::Core*, Cell::Positive::Vertex* > ( P.core );
	Cell::Positive::Vertex * A_c = tag::Util::assert_cast
		< Cell::Core*, Cell::Positive::Vertex* > ( A.core );
	Cell::Positive::Vertex * B_c = tag::Util::assert_cast
		< Cell::Core*, Cell::Positive::Vertex* > ( B.core );
	Cell::Positive::Vertex * C_c = tag::Util::assert_cast
		< Cell::Core*, Cell::Positive::Vertex* > ( C.core );
	Cell::Positive::Vertex * D_c = tag::Util::assert_cast
		< Cell::Core*, Cell::Positive::Vertex* > ( D.core );
	Cell::Positive::Vertex * E_c = tag::Util::assert_cast
		< Cell::Core*, Cell::Positive::Vertex* > ( E.core );
	Cell::Positive::Vertex * F_c = tag::Util::assert_cast
		< Cell::Core*, Cell::Positive::Vertex* > ( F.core );
	this->core->interpolate ( P_c, s, A_c, t, B_c, u, C_c, v, D_c, w, E_c, z, F_c );  }


// P = sum c_k P_k,  sum c_k == 1
inline void Manifold::interpolate
( const Cell & P, const std::vector < double > & coefs, const std::vector < Cell > & points ) const
	
{	assert ( points.size() == coefs .size() );
	for ( size_t i = 0; i < points .size(); i++ )  // if debug !!
	{	assert ( points [i] .is_positive() );
		assert ( points [i] .dim() == 0 );
		assert ( coefs [i] >= 0. );   assert ( coefs [i] <= 1. );  }
		// cannot assert the sum is 1. due to round-off errors
	std::vector < Cell::Positive::Vertex * > cores ( coefs .size() );
	for ( size_t i = 0; i < coefs.size(); i++ )
		cores [i] = ( Cell::Positive::Vertex * ) points [i] .core;
	this->core->interpolate ( ( Cell::Positive::Vertex * ) P .core, coefs, cores );   }

					 
#ifndef MANIFEM_NO_QUOTIENT

// P = sA + sB,  s+t == 1
inline void Manifold::interpolate
( const Cell & P, double s, const Cell & A, double t, const Cell & B,
  const tag::Winding &, const Manifold::Action & exp_AB              ) const

{	assert ( P .dim() == 0 );  assert ( A .dim() == 0 );  assert ( B .dim() == 0 );
	assert ( P .is_positive() );  assert ( A .is_positive() );  assert ( B .is_positive() );
	assert ( s >= 0. );  assert ( s <= 1. );
	assert ( t >= 0. );  assert ( t <= 1. );

	#ifndef NDEBUG  // DEBUG mode
	double sum = s + t;
	// cannot assert the sum is 1. due to round-off errors
	assert ( sum > 0.99 );  assert ( sum < 1.01 );
	#endif  // DEBUG

	Cell::Positive::Vertex * P_c = tag::Util::assert_cast
		< Cell::Core*, Cell::Positive::Vertex* > ( P.core );
	Cell::Positive::Vertex * A_c = tag::Util::assert_cast
		< Cell::Core*, Cell::Positive::Vertex* > ( A.core );
	Cell::Positive::Vertex * B_c = tag::Util::assert_cast
		< Cell::Core*, Cell::Positive::Vertex* > ( B.core );
	this->core->interpolate ( P_c, s, A_c, t, B_c, tag::winding, exp_AB );                    }


// P = sA + sB + uC,  s+t+u == 1
inline void Manifold::interpolate
( const Cell & P, double s, const Cell & A,
  double t, const Cell & B, const tag::Winding &, const Manifold::Action & exp_AB,
  double u, const Cell & C, const tag::Winding &, const Manifold::Action & exp_AC ) const

{	assert ( P .dim() == 0 );
	assert ( A .dim() == 0 );  assert ( B .dim() == 0 );
	assert ( C .dim() == 0 );
	assert ( P .is_positive() );
	assert ( A .is_positive() );  assert ( B .is_positive() );
	assert ( C .is_positive() );
	assert ( s >= 0. );  assert ( s <= 1. );
	assert ( t >= 0. );  assert ( t <= 1. );
	assert ( u >= 0. );  assert ( u <= 1. );

	#ifndef NDEBUG  // DEBUG mode
	double sum = s + t + u;
	// cannot assert the sum is 1. due to round-off errors
	assert ( sum > 0.99 );  assert ( sum < 1.01 );
	#endif  // DEBUG

	Cell::Positive::Vertex * P_c = tag::Util::assert_cast
		< Cell::Core*, Cell::Positive::Vertex* > ( P.core );
	Cell::Positive::Vertex * A_c = tag::Util::assert_cast
		< Cell::Core*, Cell::Positive::Vertex* > ( A.core );
	Cell::Positive::Vertex * B_c = tag::Util::assert_cast
		< Cell::Core*, Cell::Positive::Vertex* > ( B.core );
	Cell::Positive::Vertex * C_c = tag::Util::assert_cast
		< Cell::Core*, Cell::Positive::Vertex* > ( C.core );
	this->core->interpolate ( P_c, s, A_c, t, B_c, tag::winding, exp_AB,
              u, C_c, tag::winding, exp_AC                            );  }


// P = sA + sB + uC + vD,  s+t+u+v == 1
inline void Manifold::interpolate
( const Cell & P, double s, const Cell & A,
  double t, const Cell & B, const tag::Winding &, const Manifold::Action & exp_AB,
  double u, const Cell & C, const tag::Winding &, const Manifold::Action & exp_AC,
  double v, const Cell & D, const tag::Winding &, const Manifold::Action & exp_AD ) const

{	assert ( P .dim() == 0 );
	assert ( A .dim() == 0 );  assert ( B .dim() == 0 );
	assert ( C .dim() == 0 );  assert ( D .dim() == 0 );
	assert ( P .is_positive() );
	assert ( A .is_positive() );  assert ( B .is_positive() );
	assert ( C .is_positive() );  assert ( D .is_positive() );
	assert ( s >= 0. );  assert ( s <= 1. );
	assert ( t >= 0. );  assert ( t <= 1. );
	assert ( u >= 0. );  assert ( u <= 1. );
	assert ( v >= 0. );  assert ( v <= 1. );

	#ifndef NDEBUG  // DEBUG mode
	double sum = s + t + u + v;
	// cannot assert the sum is 1. due to round-off errors
	assert ( sum > 0.99 );  assert ( sum < 1.01 );
	#endif  // DEBUG
	
	Cell::Positive::Vertex * P_c = tag::Util::assert_cast
		< Cell::Core*, Cell::Positive::Vertex* > ( P.core );
	Cell::Positive::Vertex * A_c = tag::Util::assert_cast
		< Cell::Core*, Cell::Positive::Vertex* > ( A.core );
	Cell::Positive::Vertex * B_c = tag::Util::assert_cast
		< Cell::Core*, Cell::Positive::Vertex* > ( B.core );
	Cell::Positive::Vertex * C_c = tag::Util::assert_cast
		< Cell::Core*, Cell::Positive::Vertex* > ( C.core );
	Cell::Positive::Vertex * D_c = tag::Util::assert_cast
		< Cell::Core*, Cell::Positive::Vertex* > ( D.core );
	this->core->interpolate ( P_c, s, A_c, t, B_c, tag::winding, exp_AB,
              u, C_c, tag::winding, exp_AC, v, D_c, tag::winding, exp_AD );  }


// P = sA + sB + uC + vD + wE + zF,  s+t+u+v+w+z == 1
inline void Manifold::interpolate
( const Cell & P, double s, const Cell & A,
  double t, const Cell & B, const tag::Winding &, const Manifold::Action & exp_AB,
  double u, const Cell & C, const tag::Winding &, const Manifold::Action & exp_AC,
  double v, const Cell & D, const tag::Winding &, const Manifold::Action & exp_AD,
  double w, const Cell & E, const tag::Winding &, const Manifold::Action & exp_AE,
  double z, const Cell & F, const tag::Winding &, const Manifold::Action & exp_AF ) const

{	assert ( P .dim() == 0 );
	assert ( A .dim() == 0 );  assert ( B .dim() == 0 );
	assert ( C .dim() == 0 );  assert ( D .dim() == 0 );
	assert ( E .dim() == 0 );  assert ( F .dim() == 0 );
	assert ( P .is_positive() );
	assert ( A .is_positive() );  assert ( B .is_positive() );
	assert ( C .is_positive() );  assert ( D .is_positive() );
	assert ( E .is_positive() );  assert ( F .is_positive() );
	assert ( s >= 0. );  assert ( s <= 1. );
	assert ( t >= 0. );  assert ( t <= 1. );
	assert ( u >= 0. );  assert ( u <= 1. );
	assert ( v >= 0. );  assert ( v <= 1. );
	assert ( w >= 0. );  assert ( w <= 1. );
	assert ( z >= 0. );  assert ( z <= 1. );

	#ifndef NDEBUG  // DEBUG mode
	double sum = s + t + u + v + w + z;
	// cannot assert the sum is 1. due to round-off errors
	assert ( sum > 0.99 );  assert ( sum < 1.01 );
	#endif  // DEBUG
	
	Cell::Positive::Vertex * P_c = tag::Util::assert_cast
		< Cell::Core*, Cell::Positive::Vertex* > ( P.core );
	Cell::Positive::Vertex * A_c = tag::Util::assert_cast
		< Cell::Core*, Cell::Positive::Vertex* > ( A.core );
	Cell::Positive::Vertex * B_c = tag::Util::assert_cast
		< Cell::Core*, Cell::Positive::Vertex* > ( B.core );
	Cell::Positive::Vertex * C_c = tag::Util::assert_cast
		< Cell::Core*, Cell::Positive::Vertex* > ( C.core );
	Cell::Positive::Vertex * D_c = tag::Util::assert_cast
		< Cell::Core*, Cell::Positive::Vertex* > ( D.core );
	Cell::Positive::Vertex * E_c = tag::Util::assert_cast
		< Cell::Core*, Cell::Positive::Vertex* > ( E.core );
	Cell::Positive::Vertex * F_c = tag::Util::assert_cast
		< Cell::Core*, Cell::Positive::Vertex* > ( F.core );
	this->core->interpolate ( P_c, s, A_c, t, B_c, tag::winding, exp_AB,
              u, C_c, tag::winding, exp_AC, v, D_c, tag::winding, exp_AD,
              w, E_c, tag::winding, exp_AE, z, F_c, tag::winding, exp_AF );  }


#endif  // ifndef MANIFEM_NO_QUOTIENT

inline void Manifold::project ( const Cell & cll ) const
{	assert ( cll .is_positive () );
	assert ( cll .dim() == 0 );
	this->core->project ( tag::Util::assert_cast
                        < Cell::Core*, Cell::Positive::Vertex* > ( cll .core ) );  }


inline Manifold Manifold::implicit ( const Function::Equality eq ) const
{	return Manifold ( tag::implicit, *this, eq.lhs - eq.rhs );  }

inline Manifold Manifold::implicit
( const Function::Equality eq1, const Function::Equality eq2 ) const
{	return Manifold ( tag::implicit, *this, eq1.lhs - eq1.rhs, eq2.lhs - eq2.rhs );  }


inline Manifold Manifold::parametric ( const Function::Equality eq ) const
{	return Manifold ( tag::parametric, *this, eq );  }

inline Manifold Manifold::parametric
( const Function::Equality eq1, const Function::Equality eq2 ) const
{	return Manifold ( tag::parametric, *this, eq1, eq2 );  }

inline Manifold Manifold::parametric
( const Function::Equality eq1, const Function::Equality eq2, const Function::Equality eq3 ) const
{	return Manifold ( tag::parametric, *this, eq1, eq2, eq3 );  }

//------------------------------------------------------------------------------------------------------//


inline void Cell::project () const
{	this->project ( tag::onto, Manifold::working );  }
	
inline void Cell::project ( const tag::Onto &, const Manifold m ) const
{	assert ( m.core );
	assert ( this->core->get_dim() == 0 );
	assert ( this->core->is_positive() );
	Cell::Positive::Vertex * cll = ( Cell::Positive::Vertex * ) this->core;
	m.core->project ( cll );                                                }
		
//------------------------------------------------------------------------------------------------------//


}  // end of  namespace maniFEM

#endif  // ifndef MANIFEM_MANIFOLD_H

