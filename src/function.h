
//   function.h 2025.01.26

//   This file is part of maniFEM, a C++ library for meshes and finite elements on manifolds.

//   Copyright  2019 - 2025  Cristian Barbarosie  cristian.barbarosie@gmail.com

//   https://maniFEM.rd.ciencias.ulisboa.pt/
//   https://codeberg.org/cristian.barbarosie/maniFEM

//   ManiFEM is free software: you can redistribute it and/or modify it
//   under the terms of the GNU Lesser General Public License as published
//   by the Free Software Foundation, either version 3 of the License
//   or (at your option) any later version.

//   ManiFEM is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//   See the GNU Lesser General Public License for more details.

//   You should have received a copy of the GNU Lesser General Public License
//   along with maniFEM.  If not, see <https://www.gnu.org/licenses/>.


#ifndef MANIFEM_FUNCTION_H
#define MANIFEM_FUNCTION_H

#include <iostream>
#include <vector>
#include <forward_list>
#include <memory>
#include "math.h"
#include "assert.h"

#include "mesh.h"
#include "iterator.h"
#include "field.h"

#ifndef MANIFEM_NO_FEM
#include <Eigen/Dense>
#endif  // ifndef MANIFEM_NO_FEM



namespace maniFEM {


namespace tag
{	struct Diffeomorphism { };  static const Diffeomorphism diffeomorphism;
	struct Immersion { };  static const Immersion immersion;
	struct BuildJacobian { };  static const BuildJacobian build_jacobian;
	struct ComposedWith { };  static const ComposedWith composed_with;
	struct Piecewise { };  static const Piecewise piecewise;
	struct Iff { };  static const Iff iff;
	struct PreviouslyNonExistent { };
		static const PreviouslyNonExistent previously_non_existent;
	struct Unknown { };  static const Unknown unknown;
	struct Test { };  static const Test test;
	struct dirichlet { };  static const dirichlet Dirichlet;
	struct BasisFunction { };  static const BasisFunction basis_function;
	struct LivesOn { };  static const LivesOn lives_on;
	struct LessThan { };  static const LessThan less_than;
	struct IfLessThan { };  static const IfLessThan if_less_than;
	struct Otherwise { };  static const Otherwise otherwise;
	struct MereSymbol { };  static const MereSymbol mere_symbol;
	struct AssociatedWith { };  static const AssociatedWith associated_with;
	struct Through { };  static const Through through;
	struct Becomes { };  static const Becomes becomes;
	struct Transforms { };  static const Transforms transforms;
	struct Into { };  static const Into into;
	struct CoupledWithArray { };  static const CoupledWithArray coupled_with_array;
	struct UseNumbering { };  static const UseNumbering use_numbering;
	struct UseVector { };  static const UseVector use_vector;
}
	

class Manifold;  class FiniteElement;  class VariationalFormulation;
	

class Function

// just a a thin wrapper around Function::Core
// I guess this is what they call "delegation" ...
// useful for arithmetic operators and for vector functions

{	public :

	class Core;

	static unsigned int total_cores;

	#ifndef NDEBUG  // DEBUG mode
	static std::map < const Function::Core*, std::string > name;
	#endif

	Function::Core * core;
	// change the above to shared_ptr, do the same in TakenOnCell

	inline Function ( ) { assert ( false );  }

	inline Function ( const tag::NonExistent & ) : core ( nullptr )  {  }

	inline Function ( const tag::WhoseCoreIs &, Function::Core * c );

	inline Function ( const Function & f );

	Function ( const tag::LivesOn &, const tag::CellsOfDim &, const size_t dim,
             const tag::HasSize &, const size_t s                            );
	inline Function ( const tag::LivesOn &, const tag::Vertices &,
	                  const tag::HasSize &, const size_t s        );

	inline Function ( double c );

	inline Function ( const tag::Piecewise &,
	                  const Function & v1, const tag::Iff, const Function & x,
	                                       const tag::LessThan &, double c,
	                  const Function & v2, const tag::Otherwise &             );

	inline Function ( const tag::Piecewise &,
	                  const Function & v1, const tag::Iff, const Function & x,
	                                       const tag::LessThan &, double c1,
	                  const Function & v2, const tag::IfLessThan &, double c2,
	                  const Function & v3, const tag::Otherwise &             );

	inline Function ( const tag::Piecewise &,
	                  const Function & v1, const tag::Iff, const Function & x,
	                                       const tag::LessThan &, double c1,
	                  const Function & v2, const tag::IfLessThan &, double c2,
	                  const Function & v3, const tag::IfLessThan &, double c3,
	                  const Function & v4, const tag::Otherwise &             );

	inline Function ( const tag::Piecewise &,
	                  const Function & v1, const tag::Iff, const Function & x,
	                                       const tag::LessThan &, double c1,
	                  const Function & v2, const tag::IfLessThan &, double c2,
	                  const Function & v3, const tag::IfLessThan &, double c3,
	                  const Function & v4, const tag::IfLessThan &, double c4,
	                  const Function & v5, const tag::Otherwise &             );

	inline Function ( const tag::Diffeomorphism &, const tag::OneDim &,
	                  const Function & geom_coords, const Function & master_coords,
	                  const Function & geom_back_coords                             );

	inline Function ( const tag::Diffeomorphism &, const tag::HighDim &,
	                  const Function & geom_coords, const Function & master_coords,
	                  const Function & geom_back_coords                             );

	inline Function ( const tag::Diffeomorphism &, const Function & geom_coords,
	                  const Function & master_coords, const Function & geom_back_coords );

	inline Function ( const tag::Immersion &, const Function & geom_coords,
	                  const Function & master_coords, const Function & geom_back_coords );

	inline Function ( const Function & expr,
	                  const tag::ComposedWith &, const Function & map );
	// an 'expr'ession involving master coordinates ( e.g.  1. - xi - eta )
	// composed with a map (diffeomorphism or immersion) sending it in the physical space

	inline Function ( const tag::BasisFunction &, const tag::Within &, FiniteElement & );

	inline Function ( const tag::MereSymbol & );

	inline Function ( const tag::Unknown &,
	                  const tag::lagrange &, const tag::OfDegree &, size_t,
	                  const tag::MereSymbol &                              );

	inline Function ( const tag::Test &,
	                  const tag::lagrange &, const tag::OfDegree &, size_t,
	                  const tag::MereSymbol &                              );

	#ifndef MANIFEM_NO_FEM
	inline Function ( const tag::CoupledWithArray &,
	                  const tag::UseNumbering &, Cell::Numbering,
	                  const tag::UseVector &, std::shared_ptr < Eigen::VectorXd > );
	#endif  // ifndef MANIFEM_NO_FEM

	#ifndef MANIFEM_NO_QUOTIENT
	class ActionGenerator;  // a generator of a discrete group
	// an action will act on functions, particularly on coordinates of a quotient manifold
	typedef tag::Util::Action Action;  // aka  class Manifold::Action
	// we define it here because we need it for Function::MultiValued
	// but we prefer the user to see it as an attribute of class Manifold
	#endif  // ifndef MANIFEM_NO_QUOTIENT
	
	inline ~Function();

	inline Function & operator= ( const Function & );
	
	inline size_t number_of ( const tag::Components & ) const;

	inline bool exists ( ) const
	{	return this->core;  }

	inline Function operator[] ( size_t ) const;

	inline void conditionally_dispose_core ( );
	inline void set_core ( Function::Core *, const tag::PreviouslyNonExistent & );
	inline void set_core_to_null ( );
	inline void change_core_to ( Function::Core * );

	#ifndef MANIFEM_NO_QUOTIENT
	inline Function make_multivalued ( const tag::Through &, const Function::Action &,
	                                   const tag::Becomes &, const Function &         );
 	inline Function make_multivalued ( const tag::Through &, const Function::Action &,
	                                   const tag::Becomes &, const Function &,
	                                   const tag::Through &, const Function::Action &,
	                                   const tag::Becomes &, const Function &         );
	inline Function make_multivalued ( const tag::Through &, const Function::Action &,
	                                   const tag::Becomes &, const Function &,
	                                   const tag::Through &, const Function::Action &,
	                                   const tag::Becomes &, const Function &,
	                                   const tag::Through &, const Function::Action &,
	                                   const tag::Becomes &, const Function &         );
	#endif  // ifndef MANIFEM_NO_QUOTIENT

	class TakenOnCell;  class TakenOnWindingCell;
	inline Function::TakenOnCell operator() ( const Cell & cll ) const;
  
	#ifndef MANIFEM_NO_QUOTIENT
	class Jump;
	inline Jump jump();
	inline Function::TakenOnWindingCell operator()
	( const Cell & cll, const tag::Winding &, const Function::Action & exp ) const;
	#endif  // ifndef MANIFEM_NO_QUOTIENT

	inline Function deriv ( const Function & x ) const;  // derivative with respect to x

	inline Function replace ( const Function & x, const Function & y ) const;
	// in an expression, replace x by y

	#ifndef MANIFEM_NO_FEM	
	class Form;			
	inline Function::Form integral_on ( const Mesh & msh );  // defined in var-form.h	
	inline Function::Form value_at ( const Cell & cll );  // defined in var-form.h	
	inline void analyse_integrand ( VariationalFormulation & ) const;  // defined in var-form.h	
	// analyse an integrand and detects Function::Test and Function::Unknown or derivatives thereof
	#endif

	#ifndef NDEBUG  // DEBUG mode
	inline std::string repr ( ) const;
	enum From { from_void, from_sum, from_product, from_power, from_function };
	#endif

	static bool less_for_map ( const Function & f, const Function & g );
	// needed for map 'jacobian' in class Function::Map
	// and for map 'equations' in class Manifold::Parametric
	// other solution :
	//namespace std
	//{	template <>
	//	struct less<Function>
	//	{  inline bool operator() ( const Function &, const Function & );  };  }

	class Scalar;  class ArithmeticExpression;  class Constant;
	class Vector;  class Aggregate;  class CoupledWithField;  class CoupledWithArray;
	class Sum;  class Product;  class Power;  class Expo;  class Sqrt;
	class Sin;  class Cos;  class Step;
	class Map;  class Diffeomorphism;  class Immersion;  class Composition;
	class MultiValued;  class MereSymbol;  class Unknown;  class Test;
	class DelayedDerivative;
	class Equality;
	struct Inequality
	{	class LessThanZero;  class LessThan;  class GreaterThan;
		typedef tag::Util::InequalitySet Set;                     };

};  // end of  class Function

//------------------------------------------------------------------------------------------------------//


class Function::Core

// abstract class, specialized in derived classes like Constant, Sum, Product and many others
// we use dynamic_cast

{	public :

	// Manifold * manifold;

	size_t nb_of_wrappers { 0 };

	inline Core ( ) { Function::total_cores++; };
	
	virtual ~Core ( );

	Core ( const Function::Core & ) = delete;
	Core ( Function::Core && ) = delete;
	
	inline bool dispose ( )
	{	assert ( nb_of_wrappers > 0 );
		nb_of_wrappers--;
		return ( nb_of_wrappers == 0 );  }

	virtual size_t number_of ( const tag::Components & ) const = 0;

	virtual Function component ( size_t i ) = 0;

	virtual Function deriv ( Function ) = 0;

	virtual Function replace ( const Function & x, const Function & y ) = 0;
	// in an expression, replace x by y

	#ifndef MANIFEM_NO_QUOTIENT
	virtual Function::Jump jump ( );
	// here execution forbidden
	// later overridden by Function::***::MultiValued::JumpIsSum
	#endif  // ifndef MANIFEM_NO_QUOTIENT

	#ifndef MANIFEM_NO_FEM
	virtual void analyse_integrand ( VariationalFormulation & vf );  // defined in var-form.cpp
	// here execution forbidden, later overriden by Sum and Product
	#endif

	#ifndef NDEBUG  // DEBUG mode	
	virtual std::string repr ( const Function::From & from = Function::from_void ) const = 0;
	#endif

};  // end of  class Function::Core

//------------------------------------------------------------------------------------------------------//


inline Function::Function ( const tag::WhoseCoreIs &, Function::Core * c )
:	core ( c )
{	assert ( c );  c->nb_of_wrappers++;  }

inline Function::Function ( const Function & f )
:	Function ( tag::whose_core_is, f.core )
{	}
	
inline void Function::set_core ( Function::Core * c, const tag::PreviouslyNonExistent & )
{	assert ( this->core == nullptr );
	assert ( c );
	this->core = c;
	c->nb_of_wrappers++;               }
	
inline void Function::conditionally_dispose_core ( )
{	if ( this->core )
		if ( this->core->dispose() )
			delete this->core;          }
	
inline void Function::set_core_to_null ( )
{	this->conditionally_dispose_core ();
	this->core = nullptr;                 }
	
inline void Function::change_core_to ( Function::Core * c )
{	this->conditionally_dispose_core ();
	assert ( c );
	this->core = c;
	c->nb_of_wrappers++;                  }
	
inline Function & Function::operator= ( const Function & m )
{	this->change_core_to ( m.core );
	return  * this;                   }
	
inline Function::~Function()
{	this->conditionally_dispose_core ();  }

inline Function Function::replace ( const Function & x, const Function & y ) const
{	return  this->core->replace ( x, y );  }

//------------------------------------------------------------------------------------------------------//


class Function::Scalar : public Function::Core
	
{	public :

	inline Scalar ( ) { };
	
	Scalar ( const Function::Scalar & ) = delete;
	Scalar ( Function::Scalar && ) = delete;

	size_t number_of ( const tag::Components & ) const;  // virtual from Function::Core, here returns 1

	Function component ( size_t i ); // virtual from Function::Core
	// never actually used for objects in this class because Function::operator[] returns self

	virtual void set_value ( double );  // only for constants

	virtual double get_value_on_cell ( Cell::Core * ) const = 0;
	#ifndef MANIFEM_NO_QUOTIENT
	virtual double get_value_on_cell
	( Cell::Core *, const tag::Winding &, const Function::Action & exp ) const = 0;
	#endif

	virtual double set_value_on_cell ( Cell::Core *, const double & ) = 0;
	// assign a numeric value to the function on the cell and return that value
	
	// Function deriv ( Function )
	// Function replace ( const Function & x, const Function & y )
	//    stay pure virtual from Function::Core

	// Function::Jump jump  virtual, defined by Function::Core,
	// execution forbidden  ifndef MANIFEM_NO_QUOTIENT

	// void analyse_integrand   virtual, defined by Function::Core,
	// execution forbidden      ifndef MANIFEM_NO_FEM

	// string repr ( const Function::From & from = Function::from_void )  ifndef NDEBUG
	//   stays pure virtual from Function::Core

	class MultiValued;  

};  // end of class Function::Scalar

//------------------------------------------------------------------------------------------------------//


class Function::ArithmeticExpression : public Function::Scalar
	
// abstract class, specialized in derived classes like Constant, Sum, Product
// functions for which 'set_value_on_cell' does not make sense

{	public :

	inline ArithmeticExpression ( ) { };

	ArithmeticExpression ( const Function::ArithmeticExpression & ) = delete;
	ArithmeticExpression ( Function::ArithmeticExpression && ) = delete;
	
	// size_t number_of ( const tag::Components & )
	//   virtual from Function::Core, defined by Function::Scalar, returns 1

	// Function component ( size_t i )  virtual from Function::Core,
	//   defined by Function::Scalar, returns self, never actually used

	// void set_value  defined by Function::Scalar, execution forbidden
	
	// two versions of
	// double get_value_on_cell ( Cell::Core * )  stay pure virtual from Function::Scalar

	double set_value_on_cell ( Cell::Core *, const double & );  // virtual from Function::Scalar
	// here execution forbidden

	// Function deriv ( Function )
	// Function replace ( const Function & x, const Function & y )
	//   stay pure virtual from Function::Core
	
	// Function::Jump jump  virtual, defined by Function::Core,
	// execution forbidden  ifndef MANIFEM_NO_QUOTIENT

	// void analyse_integrand   virtual, defined by Function::Core,
	// execution forbidden      ifndef MANIFEM_NO_FEM

	// string repr ( const Function::From & from = Function::from_void )  ifndef NDEBUG
	//   stays pure virtual from Function::Core

};  // end of  class Function::ArithmeticExpression

//------------------------------------------------------------------------------------------------------//


class Function::Constant : public Function::ArithmeticExpression
	
{	public :

	double value;

	inline Constant ( double c )
	:	value { c } { }

	Constant ( const Function::Constant & ) = delete;
	Constant ( Function::Constant && ) = delete;
	
	Function::Constant operator= ( const Function::Constant & ) = delete;
	Function::Constant operator= ( Function::Constant && ) = delete;

	// size_t number_of ( const tag::Components & )
	//   virtual from Function::Core, defined by Function::Scalar, returns 1

	// Function component ( size_t i )  virtual from Function::Core,
	//   defined by Function::Scalar, returns self, never actually used

	virtual void set_value ( double );  // virtual from Function::Scalar, here overridden

	double get_value_on_cell ( Cell::Core * ) const;  // virtual from Function::Scalar
	#ifndef MANIFEM_NO_QUOTIENT
	double get_value_on_cell  // virtual from Function::Scalar
	( Cell::Core *, const tag::Winding &, const Function::Action & exp ) const;
	#endif

	// double set_value_on_cell ( Cell::Core *, const double & )  // virtual from Function::Scalar
	//   defined by Function::ArithmeticExpression (execution forbidden)

	Function deriv ( Function );  // virtual from Function::Core

	Function replace ( const Function & x, const Function & y );
	//  virtual from Function::Core
	
	// Function::Jump jump  virtual, defined by Function::Core,
	// execution forbidden  ifndef MANIFEM_NO_QUOTIENT

	// void analyse_integrand   virtual, defined by Function::Core,
	// execution forbidden      ifndef MANIFEM_NO_FEM

	#ifndef NDEBUG  // DEBUG mode	
	std::string repr ( const Function::From & from = Function::from_void ) const;
	// virtual from Function::Core
	#endif

};  // end of  class Function::Constant

//------------------------------------------------------------------------------------------------------//

inline Function::Function ( double c )
:	Function ( tag::whose_core_is, new Function::Constant ( c ) )
{	}

//------------------------------------------------------------------------------------------------------//


class Function::Sum : public Function::ArithmeticExpression
	
{	public :

	std::forward_list < Function > terms;  // we use wrappers as pointers

	inline Sum ( ) { };
	
	Sum ( const Function::Sum & ) = delete;
	Sum ( Function::Sum && ) = delete;
	
	Function::Sum operator= ( const Function::Sum & ) = delete;
	Function::Sum operator= ( Function::Sum && ) = delete;

	// size_t number_of ( const tag::Components & )
	//   virtual from Function::Core, defined by Function::Scalar, returns 1

	// Function component ( size_t i )  virtual from Function::Core,
	//   defined by Function::Scalar, returns self, never actually used

	// void set_value  defined by Function::Scalar, execution forbidden

	double get_value_on_cell ( Cell::Core * ) const;  // virtual from Function::Scalar
	#ifndef MANIFEM_NO_QUOTIENT
	double get_value_on_cell  // virtual from Function::Scalar
	( Cell::Core *, const tag::Winding &, const Function::Action & exp ) const;
	#endif

 	// double set_value_on_cell ( Cell::Core *, const double & )  // virtual from Function::Scalar
	//   defined by Function::ArithmeticExpression (execution forbidden)

	Function deriv ( Function );  // virtual from Function::Core

	Function replace ( const Function & x, const Function & y );
	//  virtual from Function::Core
	
	// Function::Jump jump  virtual, defined by Function::Core,
	// execution forbidden  ifndef MANIFEM_NO_QUOTIENT

	#ifndef MANIFEM_NO_FEM
	void analyse_integrand ( VariationalFormulation & vf ) override;
	// virtual from Function::Core  // defined in var-form.cpp
	#endif
	
	#ifndef NDEBUG  // DEBUG mode	
	std::string repr ( const Function::From & from = Function::from_void ) const;
	//  virtual from Function::Core
	#endif

};  // end of  class Function::Sum

//------------------------------------------------------------------------------------------------------//


class Function::Product : public Function::ArithmeticExpression
	
{	public :

	std::forward_list < Function > factors;  // we use wrappers as pointers

	inline Product ( ) { };

	Product ( const Function::Product & ) = delete;
	Product ( Function::Product && ) = delete;
	
	Function::Product operator= ( const Function::Product & ) = delete;
	Function::Product operator= ( Function::Product && ) = delete;

	// size_t number_of ( const tag::Components & )
	//   virtual from Function::Core, defined by Function::Scalar, returns 1

	// Function component ( size_t i )  virtual from Function::Core,
	//   defined by Function::Scalar, returns self, never actually used

	// void set_value  defined by Function::Scalar, execution forbidden

	double get_value_on_cell ( Cell::Core * ) const;  // virtual from Function::Scalar
	#ifndef MANIFEM_NO_QUOTIENT
	double get_value_on_cell  // virtual from Function::Scalar
	( Cell::Core *, const tag::Winding &, const Function::Action & exp ) const;
	#endif

	// double set_value_on_cell ( Cell::Core *, const double & )  // virtual from Function::Scalar
	//   defined by Function::ArithmeticExpression (execution forbidden)

	Function deriv ( Function );  // virtual from Function::Core

	Function replace ( const Function & x, const Function & y );
	//  virtual from Function::Core
	
	// Function::Jump jump  virtual, defined by Function::Core,
	// execution forbidden  ifndef MANIFEM_NO_QUOTIENT

	#ifndef MANIFEM_NO_FEM
	void analyse_integrand ( VariationalFormulation & vf ) override;
	// virtual from Function::Core  // defined in var-form.cpp
	#endif
	
	#ifndef NDEBUG  // DEBUG mode	
	std::string repr ( const Function::From & from = Function::from_void ) const;
	//  virtual from Function::Core
	#endif

};  // end of  class Function::Product

//------------------------------------------------------------------------------------------------------//


class Function::Power : public Function::ArithmeticExpression
	
{	public :

	Function base;  // we use wrapper as a pointer
	double exponent;

	inline Power ( Function b, double e )
	:	base { b }, exponent { e }
	{	assert ( dynamic_cast < Function::Scalar* > ( b.core ) );  }

	Power ( const Function::Power & ) = delete;
	Power ( Function::Power && ) = delete;
	
	Function::Power operator= ( const Function::Power & ) = delete;
	Function::Power operator= ( Function::Power && ) = delete;

	// size_t number_of ( const tag::Components & )
	//   virtual from Function::Core, defined by Function::Scalar, returns 1

	// Function component ( size_t i )  virtual from Function::Core,
	//   defined by Function::Scalar, returns self, never actually used

	// void set_value  defined by Function::Scalar, execution forbidden

	double get_value_on_cell ( Cell::Core * ) const;  // virtual from Function::Scalar
	#ifndef MANIFEM_NO_QUOTIENT
	double get_value_on_cell  // virtual from Function::Scalar
	( Cell::Core *, const tag::Winding &, const Function::Action & exp ) const;
	#endif

	// double set_value_on_cell ( Cell::Core *, const double & )  // virtual from Function::Scalar
	//   defined by Function::ArithmeticExpression (execution forbidden)

	Function deriv ( Function );  // virtual from Function::Core

	Function replace ( const Function & x, const Function & y );
	//  virtual from Function::Core
	
	// Function::Jump jump  virtual, defined by Function::Core,
	// execution forbidden  ifndef MANIFEM_NO_QUOTIENT

	// void analyse_integrand   virtual, defined by Function::Core,
	// execution forbidden      ifndef MANIFEM_NO_FEM

	#ifndef NDEBUG  // DEBUG mode	
	std::string repr ( const Function::From & from = Function::from_void ) const;
	//  virtual from Function::Core
	#endif

};  // end of  class Function::Power

//------------------------------------------------------------------------------------------------------//


class Function::Expo : public Function::ArithmeticExpression
	
{	public :

	Function exponent;  // we use wrapper as a pointer

	inline Expo ( Function e )
	:	exponent { e }
	{	assert ( dynamic_cast < Function::Scalar* > ( e .core ) );  }

	Expo ( const Function::Expo & ) = delete;
	Expo ( Function::Expo && ) = delete;
	
	Function::Expo operator= ( const Function::Expo & ) = delete;
	Function::Expo operator= ( Function::Expo && ) = delete;

	// size_t number_of ( const tag::Components & )
	//   virtual from Function::Core, defined by Function::Scalar, returns 1

	// Function component ( size_t i )  virtual from Function::Core,
	//   defined by Function::Scalar, returns self, never actually used

	// void set_value  defined by Function::Scalar, execution forbidden

	double get_value_on_cell ( Cell::Core * ) const;  // virtual from Function::Scalar
	#ifndef MANIFEM_NO_QUOTIENT
	double get_value_on_cell  // virtual from Function::Scalar
	( Cell::Core *, const tag::Winding &, const Function::Action & exp ) const;
	#endif

	// double set_value_on_cell ( Cell::Core *, const double & )  // virtual from Function::Scalar
	//   defined by Function::ArithmeticExpression (execution forbidden)

	Function deriv ( Function );  // virtual from Function::Core

	Function replace ( const Function & x, const Function & y );
	//  virtual from Function::Core
	
	// Function::Jump jump  virtual, defined by Function::Core,
	// execution forbidden  ifndef MANIFEM_NO_QUOTIENT

	// void analyse_integrand   virtual, defined by Function::Core,
	// execution forbidden      ifndef MANIFEM_NO_FEM

	#ifndef NDEBUG  // DEBUG mode	
	std::string repr ( const Function::From & from = Function::from_void ) const;
	//  virtual from Function::Core
	#endif

};  // end of  class Function::Expo

//------------------------------------------------------------------------------------------------------//


class Function::Sqrt : public Function::ArithmeticExpression
	
{	public :

	Function base;  // we use wrapper as a pointer

	inline Sqrt ( const Function & b ) : base { b }
	{	assert ( dynamic_cast < Function::Scalar* > ( b.core ) );  }

	Sqrt ( const Function::Sqrt & ) = delete;
	Sqrt ( Function::Sqrt && ) = delete;
	
	Function::Sqrt operator= ( const Function::Sqrt & ) = delete;
	Function::Sqrt operator= ( Function::Sqrt && ) = delete;
	
	// size_t number_of ( const tag::Components & )
	//   virtual from Function::Core, defined by Function::Scalar, returns 1

	// Function component ( size_t i )  virtual from Function::Core,
	//   defined by Function::Scalar, returns self, never actually used

	// void set_value  defined by Function::Scalar, execution forbidden

	double get_value_on_cell ( Cell::Core * ) const;  // virtual from Function::Scalar
	#ifndef MANIFEM_NO_QUOTIENT
	double get_value_on_cell  // virtual from Function::Scalar
	( Cell::Core *, const tag::Winding &, const Function::Action & exp ) const;
	#endif

	// double set_value_on_cell ( Cell::Core *, const double & )  // virtual from Function::Scalar
	//   defined by Function::ArithmeticExpression (execution forbidden)

	Function deriv ( Function );  // virtual from Function::Core

	Function replace ( const Function & x, const Function & y );
	//  virtual from Function::Core
	
	// Function::Jump jump  virtual, defined by Function::Core,
	// execution forbidden  ifndef MANIFEM_NO_QUOTIENT

	// void analyse_integrand   virtual, defined by Function::Core,
	// execution forbidden      ifndef MANIFEM_NO_FEM

	#ifndef NDEBUG  // DEBUG mode	
	std::string repr ( const Function::From & from = Function::from_void ) const;
	//  virtual from Function::Core
	#endif

};  // end of  class Function::Sqrt

//------------------------------------------------------------------------------------------------------//

inline Function sqrt ( const Function & f )
{	return Function ( tag::whose_core_is, new Function::Sqrt ( f ) );  }

//------------------------------------------------------------------------------------------------------//


class Function::Sin : public Function::ArithmeticExpression
	
{	public :

	Function base;  // we use wrapper as a pointer

	inline Sin ( const Function & b ) : base { b }
	{	assert ( dynamic_cast < Function::Scalar* > ( b.core ) );  }

	Sin ( const Function::Sin & ) = delete;
	Sin ( Function::Sin && ) = delete;
	
	Function::Sin operator= ( const Function::Sin & ) = delete;
	Function::Sin operator= ( Function::Sin && ) = delete;
	
	// size_t number_of ( const tag::Components & )
	//   virtual from Function::Core, defined by Function::Scalar, returns 1

	// Function component ( size_t i )  virtual from Function::Core,
	//   defined by Function::Scalar, returns self, never actually used

	// void set_value  defined by Function::Scalar, execution forbidden

	double get_value_on_cell ( Cell::Core * ) const;  // virtual from Function::Scalar
	#ifndef MANIFEM_NO_QUOTIENT
	double get_value_on_cell  // virtual from Function::Scalar
	( Cell::Core *, const tag::Winding &, const Function::Action & exp ) const;
	#endif

	// double set_value_on_cell ( Cell::Core *, const double & )  // virtual from Function::Scalar
	//   defined by Function::ArithmeticExpression (execution forbidden)

	Function deriv ( Function );  // virtual from Function::Core

	Function replace ( const Function & x, const Function & y );
	//  virtual from Function::Core
	
	// Function::Jump jump  virtual, defined by Function::Core,
	// execution forbidden  ifndef MANIFEM_NO_QUOTIENT

	// void analyse_integrand   virtual, defined by Function::Core,
	// execution forbidden      ifndef MANIFEM_NO_FEM

	#ifndef NDEBUG  // DEBUG mode	
	std::string repr ( const Function::From & from = Function::from_void ) const;
	//  virtual from Function::Core
	#endif

};  // end of  class Function::Sin

//------------------------------------------------------------------------------------------------------//

inline Function sin ( const Function & f )
{ return Function ( tag::whose_core_is, new Function::Sin ( f ) );  }

//------------------------------------------------------------------------------------------------------//


class Function::Cos : public Function::ArithmeticExpression
	
{	public :

	Function base;  // we use wrapper as a pointer

	inline Cos ( const Function & b ) : base { b }
	{	assert ( dynamic_cast < Function::Scalar* > ( b.core ) );  }

	Cos ( const Function::Cos & ) = delete;
	Cos ( Function::Cos && ) = delete;
	
	Function::Cos operator= ( const Function::Cos & ) = delete;
	Function::Cos operator= ( Function::Cos && ) = delete;

	// size_t number_of ( const tag::Components & )
	//   virtual from Function::Core, defined by Function::Scalar, returns 1

	// Function component ( size_t i )  virtual from Function::Core,
	//   defined by Function::Scalar, returns self, never actually used

	// void set_value  defined by Function::Scalar, execution forbidden

	double get_value_on_cell ( Cell::Core * ) const;  // virtual from Function::Scalar
	#ifndef MANIFEM_NO_QUOTIENT
	double get_value_on_cell  // virtual from Function::Scalar
	( Cell::Core *, const tag::Winding &, const Function::Action & exp ) const;
	#endif

	// double set_value_on_cell ( Cell::Core *, const double & )  // virtual from Function::Scalar
	//   defined by Function::ArithmeticExpression (execution forbidden)

	Function deriv ( Function );  // virtual from Function::Core

	Function replace ( const Function & x, const Function & y );
	//  virtual from Function::Core
	
	// Function::Jump jump  virtual, defined by Function::Core,
	// execution forbidden  ifndef MANIFEM_NO_QUOTIENT

	// void analyse_integrand   virtual, defined by Function::Core,
	// execution forbidden      ifndef MANIFEM_NO_FEM

	#ifndef NDEBUG  // DEBUG mode	
	std::string repr ( const Function::From & from = Function::from_void ) const;
	//  virtual from Function::Core
	#endif

};  // end of  class Function::Cos

//------------------------------------------------------------------------------------------------------//

inline Function cos ( const Function & f )
{	return Function ( tag::whose_core_is, new Function::Cos ( f ) );  }

//------------------------------------------------------------------------------------------------------//


class Function::Step : public Function::ArithmeticExpression

// piecewise constant functions 

{	public :

	Function arg;  // we use wrappers as pointers
	// 'arg' is used for testing inequalities

	std::vector < Function > values;
	std::vector < double > cuts;  //  values.size() == cuts.size() + 1
	// 'cuts' contains values ordered increasingly
	// for instance, suppose arg==x, values=={x,1/x,3} and cuts=={-1,1}
	// this means this(x)=x for x<-1, this(x)=1/x for -1<x<1, this(x)=3 for x>1

	inline Step ( const Function & v1, const tag::Iff, const Function & x,
	              const tag::LessThan &, double c,
	              const Function & v2, const tag::Otherwise &             )
	:	arg { x }, values { v1, v2 }, cuts { c }
	{	assert ( dynamic_cast < Function::Scalar* > ( x.core ) );
		assert ( dynamic_cast < Function::Scalar* > ( v1.core ) );
		assert ( dynamic_cast < Function::Scalar* > ( v2.core ) );  }
		
	inline Step ( const Function & v1, const tag::Iff, const Function & x,
	              const tag::LessThan &, double c1,
	              const Function & v2, const tag::IfLessThan, double c2,
	              const Function & v3, const tag::Otherwise &             )
	:	arg { x }, values { v1, v2, v3 }, cuts { c1, c2 }
	{	assert ( c1 < c2 );
		assert ( dynamic_cast < Function::Scalar* > ( x.core ) );
		assert ( dynamic_cast < Function::Scalar* > ( v1.core ) );
		assert ( dynamic_cast < Function::Scalar* > ( v2.core ) );
		assert ( dynamic_cast < Function::Scalar* > ( v3.core ) );  }
	
	inline Step ( const Function & v1, const tag::Iff, const Function & x,
	              const tag::LessThan &, double c1,
	              const Function & v2, const tag::IfLessThan, double c2,
	              const Function & v3, const tag::IfLessThan, double c3,
	              const Function & v4, const tag::Otherwise &             )
	:	arg { x }, values { v1, v2, v3, v4 }, cuts { c1, c2, c3 }
	{	assert ( c1 < c2 );  assert ( c2 < c3 );
		assert ( dynamic_cast < Function::Scalar* > ( x.core ) );
		assert ( dynamic_cast < Function::Scalar* > ( v1.core ) );
		assert ( dynamic_cast < Function::Scalar* > ( v2.core ) );
		assert ( dynamic_cast < Function::Scalar* > ( v3.core ) );
		assert ( dynamic_cast < Function::Scalar* > ( v4.core ) );  }
	
	inline Step ( const Function & v1, const tag::Iff, const Function & x,
	              const tag::LessThan &, double c1,
	              const Function & v2, const tag::IfLessThan, double c2,
	              const Function & v3, const tag::IfLessThan, double c3,
	              const Function & v4, const tag::IfLessThan, double c4,
	              const Function & v5, const tag::Otherwise &             )
	:	arg { x }, values { v1, v2, v3, v4, v5 }, cuts { c1, c2, c3, c4 }
	{	assert ( c1 < c2 );  assert ( c1 < c2 );  assert ( c3 < c4 );
		assert ( dynamic_cast < Function::Scalar* > ( x.core ) );
		assert ( dynamic_cast < Function::Scalar* > ( v1.core ) );
		assert ( dynamic_cast < Function::Scalar* > ( v2.core ) );
		assert ( dynamic_cast < Function::Scalar* > ( v3.core ) );
		assert ( dynamic_cast < Function::Scalar* > ( v4.core ) );
		assert ( dynamic_cast < Function::Scalar* > ( v5.core ) );     }

	inline Step ( const Function & aarg, std::vector < Function > & vals,
                std::vector < double > cts )
	: arg { aarg }, values { vals }, cuts { cts }
	{	assert ( vals .size() == cts .size() + 1 );  }  // add asserts !!

	Step ( const Function::Step & ) = delete;
	Step ( Function::Step && ) = delete;
	
	Function::Step operator= ( const Function::Step & ) = delete;
	Function::Step operator= ( Function::Step && ) = delete;

	// size_t number_of ( const tag::Components & )
	//   virtual from Function::Core, defined by Function::Scalar, returns 1

	// Function component ( size_t i )  virtual from Function::Core,
	//   defined by Function::Scalar, returns self, never actually used

	// void set_value  defined by Function::Scalar, execution forbidden

	double get_value_on_cell ( Cell::Core * ) const;  // virtual from Function::Scalar
	#ifndef MANIFEM_NO_QUOTIENT
	double get_value_on_cell  // virtual from Function::Scalar
	( Cell::Core *, const tag::Winding &, const Function::Action & exp ) const;
	#endif

	// double set_value_on_cell ( Cell::Core *, const double & )  // virtual from Function::Scalar
	//   defined by Function::ArithmeticExpression (execution forbidden)

	Function deriv ( Function );  // virtual from Function::Core

	Function replace ( const Function & x, const Function & y );
	//  virtual from Function::Core
	
	// Function::Jump jump  virtual, defined by Function::Core,
	// execution forbidden  ifndef MANIFEM_NO_QUOTIENT

	#ifndef NDEBUG  // DEBUG mode	
	std::string repr ( const Function::From & from = Function::from_void ) const;
	//  virtual from Function::Core
	#endif

}; // end of  class Function::Step

//------------------------------------------------------------------------------------------------------//


#ifndef MANIFEM_NO_FEM

class Function::MereSymbol : public Function::Scalar

// used by some finite elements as (slack) basis function
	
{	public :

	// no data

	inline MereSymbol ( ) { };

	MereSymbol ( const Function::MereSymbol & ) = delete;
	MereSymbol ( Function::MereSymbol && ) = delete;
	
	Function::MereSymbol operator= ( const Function::MereSymbol & ) = delete;
	Function::MereSymbol operator= ( Function::MereSymbol && ) = delete;

	// size_t number_of ( const tag::Components & )
	//   virtual from Function::Core, defined by Function::Scalar, returns 1

	// Function component ( size_t i )  virtual from Function::Core,
	//   defined by Function::Scalar, returns self, never actually used

	// void set_value  defined by Function::Scalar, execution forbidden

	double get_value_on_cell ( Cell::Core * ) const;  // virtual from Function::Scalar
	#ifndef MANIFEM_NO_QUOTIENT
	double get_value_on_cell  // virtual from Function::Scalar
	( Cell::Core *, const tag::Winding &, const Function::Action & exp ) const;
	#endif

	double set_value_on_cell ( Cell::Core *, const double & );
	//   virtual from Function::Scalar, here execution forbidden

	Function deriv ( Function );  // virtual from Function::Core

	Function replace ( const Function & x, const Function & y );
	//  virtual from Function::Core
	
	// Function::Jump jump  virtual, defined by Function::Core,
	// execution forbidden  ifndef MANIFEM_NO_QUOTIENT

	// void analyse_integrand   virtual, defined by Function::Core,
	// execution forbidden      ifndef MANIFEM_NO_FEM

	#ifndef NDEBUG  // DEBUG mode	
	std::string repr ( const Function::From & from = Function::from_void ) const;
	// virtual from Function::Core
	#endif

};  // end of  class Function::MereSymbol

//------------------------------------------------------------------------------------------------------//

inline Function::Function
( const tag::BasisFunction &, const tag::Within &, FiniteElement & )
:	Function ( tag::whose_core_is, new Function::MereSymbol )
{	}

inline Function::Function ( const tag::MereSymbol & )
:	Function ( tag::whose_core_is, new Function::MereSymbol )
{	}

//------------------------------------------------------------------------------------------------------//


class Function::Unknown : public Function::Scalar

// used by variational formulations as unknown function
	
{	public :

	// no data

	inline Unknown ( ) { };

	Unknown ( const Function::Unknown & ) = delete;
	Unknown ( Function::Unknown && ) = delete;
	
	Function::Unknown operator= ( const Function::Unknown & ) = delete;
	Function::Unknown operator= ( Function::Unknown && ) = delete;

	// size_t number_of ( const tag::Components & )
	//   virtual from Function::Core, defined by Function::Scalar, returns 1

	// Function component ( size_t i )  virtual from Function::Core,
	//   defined by Function::Scalar, returns self, never actually used

	// void set_value  defined by Function::Scalar, execution forbidden

	double get_value_on_cell ( Cell::Core * ) const;
	#ifndef MANIFEM_NO_QUOTIENT
	double get_value_on_cell
	( Cell::Core *, const tag::Winding &, const Function::Action & exp ) const;
	#endif
	// virtual from Function::Scalar, here execution forbidden

	double set_value_on_cell ( Cell::Core *, const double & );
	//   virtual from Function::Scalar, here execution forbidden

	Function deriv ( Function );  // virtual from Function::Core

	Function replace ( const Function & x, const Function & y );
	//  virtual from Function::Core
	
	// Function::Jump jump  virtual, defined by Function::Core,
	// execution forbidden  ifndef MANIFEM_NO_QUOTIENT

	// void analyse_integrand   virtual, defined by Function::Core,
	// execution forbidden      ifndef MANIFEM_NO_FEM

	#ifndef NDEBUG  // DEBUG mode	
	std::string repr ( const Function::From & from = Function::from_void ) const;
	// virtual from Function::Core
	#endif

};  // end of  class Function::Unknown

//------------------------------------------------------------------------------------------------------//

inline Function::Function ( const tag::Unknown &,
                            const tag::lagrange &, const tag::OfDegree &, size_t deg,
                            const tag::MereSymbol &                                  )
:	Function ( tag::whose_core_is, new Function::Unknown )
{	}

//------------------------------------------------------------------------------------------------------//


class Function::Test : public Function::Scalar

// used by variational formulations as test function
	
{	public :

	// no data

	inline Test ( ) { };

	Test ( const Function::Test & ) = delete;
	Test ( Function::Test && ) = delete;
	
	Function::Test operator= ( const Function::Test & ) = delete;
	Function::Test operator= ( Function::Test && ) = delete;

	// size_t number_of ( const tag::Components & )
	//   virtual from Function::Core, defined by Function::Scalar, returns 1

	// Function component ( size_t i )  virtual from Function::Core,
	//   defined by Function::Scalar, returns self, never actually used

	// void set_value  defined by Function::Scalar, execution forbidden

	double get_value_on_cell ( Cell::Core * ) const;
	#ifndef MANIFEM_NO_QUOTIENT
	double get_value_on_cell
	( Cell::Core *, const tag::Winding &, const Function::Action & exp ) const;
	#endif
	// virtual from Function::Scalar, here execution forbidden

	double set_value_on_cell ( Cell::Core *, const double & );
	//   virtual from Function::Scalar, here execution forbidden

	Function deriv ( Function );  // virtual from Function::Core

	Function replace ( const Function & x, const Function & y );
	//  virtual from Function::Core
	
	// Function::Jump jump  virtual, defined by Function::Core,
	// execution forbidden  ifndef MANIFEM_NO_QUOTIENT

	void analyse_integrand ( VariationalFormulation & vf ) override;
	// virtual from Function::Core  // defined in var-form.cpp
	
	#ifndef NDEBUG  // DEBUG mode	
	std::string repr ( const Function::From & from = Function::from_void ) const;
	// virtual from Function::Core
	#endif

};  // end of  class Function::Test

//------------------------------------------------------------------------------------------------------//

inline Function::Function ( const tag::Test &,
                            const tag::lagrange &, const tag::OfDegree &, size_t deg,
                            const tag::MereSymbol &                                  )
:	Function ( tag::whose_core_is, new Function::Test )
{	}

#endif  // ifndef MANIFEM_NO_FEM

//------------------------------------------------------------------------------------------------------//


class Function::Vector : public Function::Core
	
{	public :

	inline Vector ( ) { };

	Vector ( const Function::Vector & ) = delete;
	Vector ( Function::Vector && ) = delete;
	
	// size_t number_of ( const tag::Components & )  stays pure virtual from Function::Core

	// Function component ( )  stays pure virtual from Function::Core

	virtual std::vector<double> get_value_on_cell ( Cell::Core * ) const = 0;
	#ifndef MANIFEM_NO_QUOTIENT
	virtual std::vector<double> get_value_on_cell
	( Cell::Core *, const tag::Winding &, const Function::Action & exp ) const = 0;
	#endif

	virtual std::vector<double> set_value_on_cell
	( Cell::Core *, const std::vector<double> & ) = 0;
	// assign a numeric vector to the function on the cell and return that vector
	
	Function deriv ( Function );
	//  virtual from Function::Core, here execution forbidden, to change

	Function replace ( const Function & x, const Function & y );
	//  virtual from Function::Core, here execution forbidden
	
	// Function::Jump jump  virtual, defined by Function::Core,
	// execution forbidden  ifndef MANIFEM_NO_QUOTIENT

	// void analyse_integrand   virtual, defined by Function::Core,
	// execution forbidden      ifndef MANIFEM_NO_FEM

	#ifndef NDEBUG  // DEBUG mode	
	std::string repr ( const Function::From & from = Function::from_void ) const;
	//  virtual from Function::Core, here forbids execution
	#endif

	class MultiValued;
	
};  // end of class Function::Vector

//------------------------------------------------------------------------------------------------------//


class Function::Aggregate : public Function::Vector
	
// inheriting from this class means simply that there is a 'components' member

{	public :

	std::vector < Function > components;
	
	inline Aggregate ( const tag::ReserveSize &, size_t s )
	:	Function::Vector()
	{	components.reserve(s);  }

	Aggregate ( const Function::Aggregate & ) = delete;
	Aggregate ( Function::Aggregate && ) = delete;
	
	Function::Aggregate operator= ( const Function::Aggregate & ) = delete;
	Function::Aggregate operator= ( Function::Aggregate && ) = delete;

	// 'number_of ( tag::components )' and 'component' are virtual from Function::Core
	// later overridden by Function::CoupledWithField::Vector
	size_t number_of ( const tag::Components & ) const;
	Function component ( size_t i );
	
	std::vector<double> get_value_on_cell ( Cell::Core * ) const ;  // virtual from Function::Vector
	#ifndef MANIFEM_NO_QUOTIENT
	std::vector<double> get_value_on_cell  // virtual from Function::Vector
	( Cell::Core *, const tag::Winding &, const Function::Action & exp ) const;
	#endif

	std::vector<double> set_value_on_cell ( Cell::Core *, const std::vector<double> & );
	// virtual from Function::Vector

	// Function deriv ( Function )
	//    defined by Function::Vector (execution forbidden), to change

	// Function replace ( const Function & x, const Function & y );
	//    defined by Function::Vector (execution forbidden), to change
	
	// Function::Jump jump  virtual, defined by Function::Core,
	// execution forbidden  ifndef MANIFEM_NO_QUOTIENT

	// void analyse_integrand   virtual, defined by Function::Core,
	// execution forbidden      ifndef MANIFEM_NO_FEM

	// std::string repr ( const Function::From & from = Function::from_void )  ifndef NDEBUG
	//    defined by Function::Vector (execution forbidden)

};  // end of  class Function::Aggregate

//------------------------------------------------------------------------------------------------------//

inline Function operator&& ( Function f, Function g )

{	size_t nf = f .number_of ( tag::components ), ng = g .number_of ( tag::components );
	Function::Aggregate * res = new Function::Aggregate ( tag::reserve_size, nf + ng );
	for ( size_t i = 0; i < nf; i++ ) res->components .emplace_back ( f [i] );
	for ( size_t i = 0; i < ng; i++ ) res->components .emplace_back ( g [i] );
	return Function ( tag::whose_core_is,	res );                                         }

//------------------------------------------------------------------------------------------------------//


class Function::Map

// used for Function::Diffeomorphism::OneDim and Function::Immersion
// Functions inheriting from this class may be used for Function::Composition

{	public :

	Function geom_coords, master_coords, back_geom_coords;
	// back_geom_coords are expressions involving the master coordinates
	// mathematically, they correspond to the same function geom_coords

	// for immersions, 'jacobian' contains the derivatives of
	// back_geom_coords with respect to master_coords

	// for Function::Diffeomorphism::***Dim, 'jacobian' contains these derivatives inverted
	// that is, we keep the inverse matrix, which can be seen as the
	// derivatives of the master_coords with respect to geom_coords
	// they should be composed with 'this' map in the calling code

	std::map < Function, Function, bool (*) ( const Function &, const Function & ) > jacobian;
	//  decltype(Function::less_for_map)*  equals  bool (*) ( const Function &, const Function & )
	
	Function det;  // positive scalar, dilation coefficient

	inline Map ( const Function & gc, const Function & mc, const Function & bgc )
	:	geom_coords ( gc ), master_coords ( mc ), back_geom_coords ( bgc ),
		jacobian ( & Function::less_for_map ), det( tag::non_existent )
	{	}
	
};  // end of Function::Map


class Function::Diffeomorphism

// useful only for asserting dynamic_cast

{	public :
	class OneDim; class HighDim; };

//------------------------------------------------------------------------------------------------------//


class Function::Diffeomorphism::OneDim
: public Function::Scalar, public Function::Map, public Function::Diffeomorphism

// there are one-dimensional diffeomorphisms and we must treat them separately
// because they inherit from Function::Scalar, not from Function::Vector

{	public :

	// Function geom_coords, master_coords, back_geom_coords
	// inherited from Function::Map
	// back_geom_coords are expressions involving the master coordinates
	// mathematically, they correspond to the same function geom_coords

	// std::map < Function, Function > jacobian  inherited from Function::Map
	// contains one over the derivative of back_geom_coords
	//          with respect to master_coords  ( 1./det )
	
	// 'jacobian' is an expression involving master_cords
	// if we want to  look at them as functions of 'geom_coords',
	// in particular, if we want derivatives with respect to 'geom_coords',
	// they should be composed with 'this' map in the calling code

	// Function det  inherited from Function::Map
	// positive scalar, dilation coefficient
	// can be used for integration
	// here, 'det' is equal to the inverse of the only component of 'jacobian'
	// thus, 'det' is	the derivative of back_geom_coords with respect to master_coords
	
	OneDim ( const Function & gc, const Function & mc, const Function & bgc,
	         const tag::BuildJacobian &                                      );
	
	OneDim ( const Function::Diffeomorphism::OneDim & ) = delete;
	OneDim ( Function::Diffeomorphism::OneDim && ) = delete;
	
	Function::Diffeomorphism::OneDim operator=
		( const Function::Diffeomorphism::OneDim & ) = delete;
	Function::Diffeomorphism::OneDim operator=
		( Function::Diffeomorphism::OneDim && ) = delete;
	
	// size_t number_of ( const tag::Components & )
	//   virtual from Function::Core, defined by Function::Scalar, returns 1

	// Function component ( size_t i )  virtual from Function::Core,
	//   defined by Function::Scalar, returns self, never actually used

	// void set_value  defined by Function::Scalar, execution forbidden

	double get_value_on_cell ( Cell::Core * ) const;  // virtual from Function::Scalar
	#ifndef MANIFEM_NO_QUOTIENT
	double get_value_on_cell  // virtual from Function::Scalar
	( Cell::Core *, const tag::Winding &, const Function::Action & exp ) const;
	#endif

	double set_value_on_cell ( Cell::Core *, const double & );
	// virtual from Function::Vector

	Function deriv ( Function );  // virtual from Function::Core

	Function replace ( const Function & x, const Function & y );
	//  virtual from Function::Core
	
	// Function::Jump jump  virtual, defined by Function::Core,
	// execution forbidden  ifndef MANIFEM_NO_QUOTIENT

	// void analyse_integrand   virtual, defined by Function::Core,
	// execution forbidden      ifndef MANIFEM_NO_FEM

	#ifndef NDEBUG  // DEBUG mode	
	std::string repr ( const Function::From & from = Function::from_void ) const;
	//  virtual from Function::Core
	#endif

};  // end of class Function::Diffeomorphism::OneDim
	
//------------------------------------------------------------------------------------------------------//


inline Function::Function ( const tag::Diffeomorphism &, const tag::OneDim &,
                            const Function & geom_coords, const Function & master_coords,
                            const Function & back_geom_coords                             )
: Function ( tag::non_existent )
{	assert ( geom_coords .number_of ( tag::components ) ==
	         back_geom_coords .number_of ( tag::components ) );
	assert ( master_coords .number_of ( tag::components ) ==
	         geom_coords .number_of ( tag::components )      );
	assert ( geom_coords .number_of ( tag::components ) == 1 );
	this->set_core ( new Function::Diffeomorphism::OneDim
	   ( geom_coords, master_coords, back_geom_coords, tag::build_jacobian ),
	   tag::previously_non_existent                                           );         }
	

//------------------------------------------------------------------------------------------------------//


class Function::Immersion : public Function::Vector, public Function::Map

// a map from a master manifold to a geometric manifold
// geometric dimension striclty higher than master dimension

{	public :

	// Function geom_coords, master_coords, back_geom_coords
	// inherited from Function::Map
	// back_geom_coords are expressions involving the master coordinates
	// mathematically, they correspond to the same function geom_coords

	// std::map < Function, Function > jacobian  inherited from Function::Map
	// contains the derivatives of back_geom_coords with respect to master_coords
	// quite different from Function::Diffeomorphism::***Dim !

	// Function det  inherited from Function::Map
	// positive scalar, dilation coefficient, can be used for integration

	// if master_coords is 1D then
	// det = |D Phi| (Phi being back_geom_coords)
	// D Phi is a vector in [back_]geom_coords manifold
	// (more precisely, it is tangent to the manifold)

	// if master_coords is 2D
	// let (e,f) be an orthonormal basis in master_coords
	// let v = D Phi e, w = D Phi f  (Phi being back_geom_coords)
	// v and w belong to [back_]geom_coords manifold
	// (more precisely, they are tangent to the manifold)
	// then this dilation coef 'det' is square root of  v.v w.w - v.w v.w
	// that is, square root of  |v|^2 |w|^2 - (v.w)^2
	// that is, square root of  vi vi wj wj - vi vj wi wj
	// is there any way to avoid computing a square root ? probably no

	// components of 'jacobian', as well as 'det', are expressions involving master_cords

	inline Immersion ( const Function & gc, const Function & mc, const Function & bgc )
	:	Function::Vector ( ), Function::Map ( gc, mc, bgc )
	{	}  // 'jacobian' initialized as empty map, 'det' as non_existent

	Immersion ( const Function & gc, const Function & mc, const Function & bgc,
              const tag::BuildJacobian &                                      );

	Immersion ( const Function::Immersion & ) = delete;
	Immersion ( Function::Immersion && ) = delete;
	
	Function::Immersion operator= ( const Function::Immersion & ) = delete;
	Function::Immersion operator= ( Function::Immersion && ) = delete;

	size_t number_of ( const tag::Components &  ) const;  // virtual from Function::Core

	Function component ( size_t i );  // virtual from Function::Core
	
	std::vector<double> get_value_on_cell ( Cell::Core * ) const ;  // virtual from Function::Vector
	#ifndef MANIFEM_NO_QUOTIENT
	std::vector<double> get_value_on_cell  // virtual from Function::Vector
	( Cell::Core *, const tag::Winding &, const Function::Action & exp ) const;
	#endif

	std::vector<double> set_value_on_cell ( Cell::Core *, const std::vector<double> & );
	// virtual from Function::Vector

	// Function deriv ( Function )  defined by Function::Vector (execution forbidden), to change

	// Function replace ( const Function & x, const Function & y );
	//    defined by Function::Vector (execution forbidden), to change
	
	// Function::Jump jump  virtual, defined by Function::Core,
	// execution forbidden  ifndef MANIFEM_NO_QUOTIENT

	// void analyse_integrand   virtual, defined by Function::Core,
	// execution forbidden      ifndef MANIFEM_NO_FEM

	// std::string repr ( const Function::From & from = Function::from_void )  ifndef NDEBUG
	//    defined by Function::Vector (execution forbidden)

};  // end of class Function::Immersion

//------------------------------------------------------------------------------------------------------//

inline Function::Function ( const tag::Immersion &, const Function & geom_coords,
                            const Function & master_coords, const Function & back_geom_coords )
:	Function ( tag::whose_core_is, new Function::Immersion
						 ( geom_coords, master_coords, back_geom_coords, tag::build_jacobian ) )
{	}

//------------------------------------------------------------------------------------------------------//

// implement  Function::Diffeomorphism::DimTwo  Function::Diffeomorphism::DimThree
// eliminate  Function::Diffeomorphism::HighDim

class Function::Diffeomorphism::HighDim
: public Function::Immersion, public Function::Diffeomorphism

// the inheritance from Function::Immersion is quite misleading !
// here, geometric dimension is equal master dimension
// the interpretation of attribute 'jacobian' is different from Function::Immersion
	
{	public :

	// Function geom_coords, master_coords, back_geom_coords
	// inherited from Function::Map
	// back_geom_coords are expressions involving the master coordinates
	// mathematically, they correspond to the same function geom_coords

	// std::map < Function, Function > jacobian  inherited from Function::Map

	// here, the meaning of the 'jacobian' attribute is different from Function::Immersion
	// we keep here the matrix already inverted
	// conceptually, this 'jacobian' contains the derivatives of master_coords
	// with respect to geom_coords
	// this is implemented backwards, by computing the derivatives of
	// back_geom_coords with respect to master_coords
	// then taking the inverse matrix

	// Function det  inherited from Function::Map
	// determinant of the jacobian matrix - not inverted !
	// should be positive, so may be used directly for integration

	// components of 'jacobian', as well as 'det', are expressions involving master_cords
	// if we want to  look at them as functions of 'geom_coords',
	// in particular if we want derivatives with respect to 'geom_coords',
	// they should be composed with 'this' map in the calling code

	HighDim ( const Function & gc, const Function & mc, const Function & bgc,
                   const tag::BuildJacobian &                               );

	HighDim ( const Function::Diffeomorphism::HighDim & ) = delete;
	HighDim ( Function::Diffeomorphism::HighDim && ) = delete;
	
	Function::Diffeomorphism::HighDim operator=
		( const Function::Diffeomorphism::HighDim & ) = delete;
	Function::Diffeomorphism::HighDim operator=
		( Function::Diffeomorphism::HighDim && ) = delete;

	// size_t number_of ( const tag::Components & )   and
	// Function component ( size_t i )   defined by Function::Immersion, execution forbidden
	
	// two methods below defined by Function::Immersion (execution forbidden)
	// std::vector<double> set_value_on_cell ( Cell::Core *, const std::vector < double > & )
	// std::vector<double> get_value_on_cell ( Cell::Core * ) const

	// Function deriv ( Function )
	//    defined by Function::Vector (execution forbidden)

	// Function replace ( const Function & x, const Function & y );
	//    defined by Function::Vector (execution forbidden)
	
	// Function::Jump jump  virtual, defined by Function::Core,
	// execution forbidden  ifndef MANIFEM_NO_QUOTIENT

	// void analyse_integrand   virtual, defined by Function::Core,
	// execution forbidden      ifndef MANIFEM_NO_FEM

	// std::string repr ( const Function::From & from = Function::from_void )  ifndef NDEBUG
	//    defined by Function::Vector (execution forbidden)

};  // end of class Function::Diffeomorphism::HighDim

//------------------------------------------------------------------------------------------------------//


inline Function::Function ( const tag::Diffeomorphism &, const tag::HighDim &,
                            const Function & geom_coords, const Function & master_coords,
                            const Function & back_geom_coords                             )
: Function ( tag::non_existent )
{	assert ( geom_coords .number_of ( tag::components ) ==
	         back_geom_coords .number_of ( tag::components ) );
	assert ( master_coords .number_of ( tag::components ) ==
	         geom_coords .number_of ( tag::components )      );
	assert ( geom_coords .number_of ( tag::components ) >= 2 );
	this->set_core ( new Function::Diffeomorphism::HighDim
	   ( geom_coords, master_coords, back_geom_coords, tag::build_jacobian ),
	   tag::previously_non_existent                                           );       }

	
inline Function::Function ( const tag::Diffeomorphism &, const Function & geom_coords,
                            const Function & master_coords, const Function & back_geom_coords )
: Function ( tag::non_existent )
{	assert ( geom_coords .number_of ( tag::components ) ==
	         back_geom_coords .number_of ( tag::components ) );
	assert ( master_coords .number_of ( tag::components ) ==
	         geom_coords .number_of ( tag::components )      );
	if ( geom_coords .number_of ( tag::components ) == 1 )
	{	this->set_core ( new Function::Diffeomorphism::OneDim
		   ( geom_coords, master_coords, back_geom_coords, tag::build_jacobian ),
		   tag::previously_non_existent                                           );
		return;                                                                       }
	this->set_core ( new Function::Diffeomorphism::HighDim
	   ( geom_coords, master_coords, back_geom_coords, tag::build_jacobian ),
	   tag::previously_non_existent                                           );       }
	
//------------------------------------------------------------------------------------------------------//


class Function::Composition : public Function::Scalar
	
// an expression 'base' involving master coordinates ( e.g.  1. - xi - eta )
// composed with an 'transf'ormation sending it in the physical space
// note that 'transf' could be an Immersion or a Diffeomorphism::OneDim or ::HighDim

{	public :

	Function base, transf;
	
	inline Composition ( const Function & b, const Function & tr )
	:	Function::Scalar(), base (b), transf (tr)
	{ }

	Composition ( const Function::Composition & ) = delete;
	Composition ( Function::Composition && ) = delete;
	
	Function::Composition operator= ( const Function::Composition & ) = delete;
	Function::Composition operator= ( Function::Composition && ) = delete;

	// size_t number_of ( const tag::Components & )
	//   virtual from Function::Core, defined by Function::Scalar, returns 1

	// Function component ( size_t i )  virtual from Function::Core,
	//   defined by Function::Scalar, returns self, never actually used

	// void set_value  defined by Function::Scalar, execution forbidden

	double get_value_on_cell ( Cell::Core * ) const;  // virtual from Function::Scalar
	#ifndef MANIFEM_NO_QUOTIENT
	double get_value_on_cell  // virtual from Function::Scalar
	( Cell::Core *, const tag::Winding &, const Function::Action & exp ) const;
	#endif

	double set_value_on_cell ( Cell::Core *, const double & );  // virtual from Function::Scalar

	Function deriv ( Function );  // virtual from Function::Core

	Function replace ( const Function & x, const Function & y );
	//  virtual from Function::Core
	
	// Function::Jump jump  virtual, defined by Function::Core,
	// execution forbidden  ifndef MANIFEM_NO_QUOTIENT

	// void analyse_integrand   virtual, defined by Function::Core,
	// execution forbidden      ifndef MANIFEM_NO_FEM

	#ifndef NDEBUG  // DEBUG mode	
	std::string repr ( const Function::From & from = Function::from_void ) const;
	//  virtual from Function::Core
	#endif

};  // end of  class Function::Composition

//------------------------------------------------------------------------------------------------------//


inline Function::Function
( const Function & expr, const tag::ComposedWith &, const Function & f_map )

// an 'expr'ession involving master coordinates ( e.g.  1.- xi - eta )
// composed with a map 'f_map' sending it in the physical space

: Function ( tag::non_existent )

{	assert ( dynamic_cast < Function::Map* > ( f_map .core ) );
	Function::Constant * expr_c = dynamic_cast < Function::Constant* > ( expr .core );
	if ( expr_c ) this->set_core ( expr .core, tag::previously_non_existent );
	else this->set_core ( new Function::Composition ( expr, f_map ),
                        tag::previously_non_existent               );                }

//------------------------------------------------------------------------------------------------------//


class Function::CoupledWithField
	
{	public :

	tag::Util::Field::Double::Base * field;

	inline CoupledWithField ( tag::Util::Field::Double::Base * f ) : field { f }
	{	assert ( f );  }
		
	class Scalar;  class Vector;
};

//------------------------------------------------------------------------------------------------------//


class Function::CoupledWithField::Scalar
:	public Function::Scalar,
	public Function::CoupledWithField
	
{	public :

	inline Scalar ( tag::Util::Field::Double::Scalar * f )
	:	Function::Scalar(),
		Function::CoupledWithField ( f )
	{	assert ( f->number_of ( tag::components ) == 1 );  }
		
	Scalar ( const Function::CoupledWithField::Scalar & ) = delete;
	Scalar ( Function::CoupledWithField::Scalar && ) = delete;
	
	Function::CoupledWithField::Scalar operator=
		( const Function::CoupledWithField::Scalar & ) = delete;
	Function::CoupledWithField::Scalar operator=
		( Function::CoupledWithField::Scalar && ) = delete;

	// size_t number_of ( const tag::Components & )
	//   virtual from Function::Core, defined by Function::Scalar, returns 1

	// Function component ( size_t i )  virtual from Function::Core,
	//   defined by Function::Scalar, returns self, never actually used

	// void set_value  defined by Function::Scalar, execution forbidden

	double get_value_on_cell ( Cell::Core * ) const;  // virtual from Function::Scalar
	#ifndef MANIFEM_NO_QUOTIENT
	double get_value_on_cell  // virtual from Function::Scalar
	( Cell::Core *, const tag::Winding &, const Function::Action & exp ) const;
	#endif

	double set_value_on_cell ( Cell::Core *, const double & );  // virtual from Function::Scalar

	Function deriv ( Function );  // virtual from Function::Core

	Function replace ( const Function & x, const Function & y );
	//  virtual from Function::Core
	
	// Function::Jump jump  virtual, defined by Function::Core,
	// execution forbidden  ifndef MANIFEM_NO_QUOTIENT

	// void analyse_integrand   virtual, defined by Function::Core,
	// execution forbidden      ifndef MANIFEM_NO_FEM

	#ifndef NDEBUG  // DEBUG mode	
	std::string repr ( const Function::From & from = Function::from_void ) const;
	//  virtual from Function::Core
	#endif

};  // end of  class Function::CoupledWithField::Scalar

//------------------------------------------------------------------------------------------------------//


class Function::CoupledWithField::Vector
:	public Function::Aggregate,
	public Function::CoupledWithField

// inheriting from Function::Aggregate means simply that there is a 'components' member

// quando calculamos as componentes duma funcao associada a um campo vectorial,
// nao precisamos de criar as componentes do campo vectorial
// podemos ter uma funcao escalar associada a um indice dentro dum campo vectorial Block
	
{	public :

	// inherited from Function::Aggregate :
	// std::vector < Function > components

	inline Vector ( tag::Util::Field::Double::Block * f )
	:	Function::Aggregate ( tag::reserve_size, f->number_of ( tag::components ) ),
		Function::CoupledWithField ( f )
	{	size_t n = f->number_of ( tag::components );
		for ( size_t j = 0; j < n; j++ )
			// this->components [j] = Function ( tag::whose_core_is,
			this->components .emplace_back ( tag::whose_core_is,
				new Function::CoupledWithField::Scalar ( field->component (j) ) );  }
	
	Vector ( const Function::CoupledWithField::Vector & ) = delete;
	Vector ( Function::CoupledWithField::Vector && ) = delete;
	
	Function::CoupledWithField::Vector operator=
		( const Function::CoupledWithField::Vector & ) = delete;
	Function::CoupledWithField::Vector operator=
		( Function::CoupledWithField::Vector && ) = delete;

	virtual size_t number_of ( const tag::Components & ) const override;
	// virtual from Function::Core
	
	virtual Function component ( size_t i ) override;
	// virtual from Function::Core
	
	std::vector<double> get_value_on_cell ( Cell::Core * ) const;  // virtual from Function::Vector
	#ifndef MANIFEM_NO_QUOTIENT
	std::vector<double> get_value_on_cell  // virtual from Function::Vector
	( Cell::Core *, const tag::Winding &, const Function::Action & exp ) const;
	#endif

	std::vector<double> set_value_on_cell ( Cell::Core *, const std::vector<double> & );
	// virtual from Function::Vector

	// Function deriv ( Function )
	//    defined by Function::Vector (execution forbidden), may change

	Function replace ( const Function & x, const Function & y );
	//  virtual from Function::Core, through Function::Vector
	
	// Function::Jump jump  virtual, defined by Function::Core,
	// execution forbidden  ifndef MANIFEM_NO_QUOTIENT

	// void analyse_integrand   virtual, defined by Function::Core,
	// execution forbidden      ifndef MANIFEM_NO_FEM

	// std::string repr ( const Function::From & from = Function::from_void );  ifndef NDEBUG
	//    defined by Function::Vector (execution forbidden)

};  // end of class Function::CoupledWithField::Vector

//------------------------------------------------------------------------------------------------------//


#ifndef MANIFEM_NO_FEM

class Function::CoupledWithArray

// function defined through an 'Eigen::VectorXd' and a numbering

// abstract class, specialized in Function::CoupledWithArray::{Scalar,Vector}
	
{	public :

	Cell::Numbering numbering;
	// shared with a VariationalFormulation

	std::shared_ptr < Eigen::VectorXd > vector;
	// shared with a VariationalFormulation

	inline CoupledWithArray ( )
	:	numbering { tag::non_existent }, vector { nullptr }
	{  }
		
	inline CoupledWithArray
	( Cell::Numbering numb, std::shared_ptr < Eigen::VectorXd > vec )
	:	numbering { numb }, vector { vec }
	{  }
		
	CoupledWithArray ( const Function::CoupledWithArray & ) = delete;
	CoupledWithArray ( Function::CoupledWithArray && ) = delete;
	
	Function::CoupledWithArray operator= ( const Function::CoupledWithArray & ) = delete;
	Function::CoupledWithArray operator= ( Function::CoupledWithArray && ) = delete;

	class Scalar;  class Vector;
	
};  // end of  class Function::CoupledWithArray

//------------------------------------------------------------------------------------------------------//


class Function::CoupledWithArray::Scalar
:	public Function::Scalar,
	public Function::CoupledWithArray
	
// function defined through an 'Eigen::VectorXd' and a numbering

{	public :

	// inherited from Function::CoupledWithArray :
	// Eigen::VectorXd vector
	// Cell::Numbering numbering
  
	inline Scalar (  )
	:	Function::Scalar(),
		Function::CoupledWithArray()
	{	}
		
	inline Scalar ( Cell::Numbering numb, std::shared_ptr < Eigen::VectorXd > vec )
	:	Function::Scalar(),
		Function::CoupledWithArray ( numb, vec )
	{	}
		
	Scalar ( const Function::CoupledWithArray::Scalar & ) = delete;
	Scalar ( Function::CoupledWithArray::Scalar && ) = delete;
	
	Function::CoupledWithArray::Scalar operator=
		( const Function::CoupledWithArray::Scalar & ) = delete;
	Function::CoupledWithArray::Scalar operator=
		( Function::CoupledWithArray::Scalar && ) = delete;

	// size_t number_of ( const tag::Components & )
	//   virtual from Function::Core, defined by Function::Scalar, returns 1

	// Function component ( size_t i )  virtual from Function::Core,
	//   defined by Function::Scalar, returns self, never actually used

	// void set_value  defined by Function::Scalar, execution forbidden

	double get_value_on_cell ( Cell::Core * ) const;  // virtual from Function::Scalar
	#ifndef MANIFEM_NO_QUOTIENT
	double get_value_on_cell  // virtual from Function::Scalar
	( Cell::Core *, const tag::Winding &, const Function::Action & exp ) const;
	#endif

	double set_value_on_cell ( Cell::Core *, const double & );  // virtual from Function::Scalar

	Function deriv ( Function );  // virtual from Function::Core

	Function replace ( const Function & x, const Function & y );
	//  virtual from Function::Core
	
	// Function::Jump jump  virtual, defined by Function::Core,
	// execution forbidden  ifndef MANIFEM_NO_QUOTIENT

	#ifndef NDEBUG  // DEBUG mode	
	std::string repr ( const Function::From & from = Function::from_void ) const;
	//  virtual from Function::Core
	#endif

};  // end of  class Function::CoupledWithArray::Scalar

//------------------------------------------------------------------------------------------------------//

inline Function::Function
( const tag::CoupledWithArray &, const tag::UseNumbering &, Cell::Numbering num,
                                 const tag::UseVector &, std::shared_ptr < Eigen::VectorXd > vec )
:	Function ( tag::whose_core_is, new Function::CoupledWithArray::Scalar ( num, vec ) )
{	}

//------------------------------------------------------------------------------------------------------//


class Function::CoupledWithArray::Vector
:	public Function::Aggregate,
	public Function::CoupledWithArray

// function defined through an 'Eigen::VectorXd', a numbering and an interlacing scheme

// inheriting from Function::Aggregate means simply that there is a 'components' member

// quando calculamos as componentes duma funcao associada a um campo vectorial,
// nao precisamos de criar as componentes do campo vectorial
// podemos ter uma funcao escalar associada a um indice dentro dum campo vectorial Block
	
{	public :

	// inherited from Function::Aggregate :
	// std::vector < Function > components

	// inherited from Function::CoupledWithArray :
	// Eigen::VectorXd vector
	// Cell::Numbering numbering
  
	inline Vector ( tag::Util::Field::Double::Block * f )
	:	Function::Aggregate ( tag::reserve_size, f->number_of ( tag::components ) ),
		Function::CoupledWithArray()
	{	size_t n = f->number_of ( tag::components );
		for ( size_t j = 0; j < n; j++ )
			// this->components [j] = Function ( tag::whose_core_is,
			this->components .emplace_back ( tag::whose_core_is,
				new Function::CoupledWithArray::Scalar() );  }
	
	Vector ( const Function::CoupledWithArray::Vector & ) = delete;
	Vector ( Function::CoupledWithArray::Vector && ) = delete;
	
	Function::CoupledWithArray::Vector operator=
		( const Function::CoupledWithArray::Vector & ) = delete;
	Function::CoupledWithArray::Vector operator=
		( Function::CoupledWithArray::Vector && ) = delete;

	virtual size_t number_of ( const tag::Components & ) const override;
	// virtual from Function::Core
	
	virtual Function component ( size_t i ) override;
	// virtual from Function::Core
	
	std::vector<double> get_value_on_cell ( Cell::Core * ) const;  // virtual from Function::Vector
	#ifndef MANIFEM_NO_QUOTIENT
	std::vector<double> get_value_on_cell  // virtual from Function::Vector 
	( Cell::Core *, const tag::Winding &, const Function::Action & exp ) const;
	#endif

	std::vector<double> set_value_on_cell ( Cell::Core *, const std::vector<double> & );
	// virtual from Function::Vector

	// Function deriv ( Function )
	//    defined by Function::Vector (execution forbidden), may change

	Function replace ( const Function & x, const Function & y );
	//  virtual from Function::Core, through Function::Vector
	
	// Function::Jump jump  virtual, defined by Function::Core,
	// execution forbidden  ifndef MANIFEM_NO_QUOTIENT

	// void analyse_integrand   virtual, defined by Function::Core,
	// execution forbidden      ifndef MANIFEM_NO_FEM

	// std::string repr ( const Function::From & from = Function::from_void );  ifndef NDEBUG
	//    defined by Function::Vector (execution forbidden)

};  // end of class Function::CoupledWithArray::Vector

#endif  // ifndef MANIFEM_NO_FEM

//------------------------------------------------------------------------------------------------------//


#ifndef NDEBUG  // DEBUG mode
inline std::string Function::repr ( ) const  { return core->repr();  }
#endif

//------------------------------------------------------------------------------------------------------//


inline Function::Function
( const tag::LivesOn &, const tag::Vertices &, const tag::HasSize &, const size_t s )
:	Function ( tag::lives_on, tag::cells_of_dim, 0, tag::has_size, s )
{	}	


Function operator+ ( const Function & f, const Function & g );
Function operator* ( const Function & f, const Function & g );
Function power ( const Function & b, double e );
Function power ( double b, const Function & e );

inline Function exp ( const Function & e )
{	return  power ( std::numbers::e, e );  }


inline Function operator- ( const Function & f )
{	assert ( f .exists() );
	Function minus_one ( -1. );
	return minus_one * f;       }

inline Function operator- ( const Function & f, const Function & g )
{	return f + (-g);  }

inline Function operator+= ( Function & f, const Function & g )
{	return f = f + g;  }

inline Function operator*= ( Function & f, const Function & g )
{	return f = f * g;  }

inline Function operator/ ( const Function & f, const Function & g )
{	return f * power ( g, -1. );  }

inline Function operator/= ( Function & f, const Function & g )
{	return f = f / g;  }

inline Function Function::deriv ( const Function & x ) const  // derivative with respect to x
{	return this->core->deriv ( x );  }

//------------------------------------------------------------------------------------------------------//


inline Function::Function ( const tag::Piecewise &,
                            const Function & v1, const tag::Iff, const Function & x,
                                                 const tag::LessThan &, double c,
                            const Function & v2, const tag::Otherwise &             )
:	Function ( tag::whose_core_is, new Function::Step
     ( v1, tag::iff, x, tag::less_than, c, v2, tag::otherwise ) )
{	}


inline Function::Function ( const tag::Piecewise &,
                            const Function & v1, const tag::Iff, const Function & x,
                                                 const tag::LessThan &, double c1,
                            const Function & v2, const tag::IfLessThan &, double c2,
                            const Function & v3, const tag::Otherwise &             )
:	Function ( tag::whose_core_is, new Function::Step
	           ( v1, tag::iff, x, tag::less_than, c1, v2, tag::if_less_than, c2, v3, tag::otherwise ) )
{	}


inline Function::Function ( const tag::Piecewise &,
                            const Function & v1, const tag::Iff, const Function & x,
                                                 const tag::LessThan &, double c1,
                            const Function & v2, const tag::IfLessThan &, double c2,
                            const Function & v3, const tag::IfLessThan &, double c3,
                            const Function & v4, const tag::Otherwise &             )
:	Function ( tag::whose_core_is, new Function::Step
	           ( v1, tag::iff, x, tag::less_than, c1, v2, tag::if_less_than, c2,
	             v3, tag::if_less_than, c3, v4, tag::otherwise ) )
{	}


inline Function::Function ( const tag::Piecewise &,
                            const Function & v1, const tag::Iff, const Function & x,
                                                 const tag::LessThan &, double c1,
                            const Function & v2, const tag::IfLessThan &, double c2,
                            const Function & v3, const tag::IfLessThan &, double c3,
                            const Function & v4, const tag::IfLessThan &, double c4,
                            const Function & v5, const tag::Otherwise &             )
:	Function ( tag::whose_core_is, new Function::Step
	           ( v1, tag::iff, x, tag::less_than, c1, v2, tag::if_less_than, c2,
	             v3, tag::if_less_than, c3, v4, tag::if_less_than, c4, v5, tag::otherwise ) )
{	}

//------------------------------------------------------------------------------------------------------//


inline Function max ( const Function & f, const Function & g )
{	return Function ( tag::piecewise, g, tag::iff, f-g, tag::less_than, 0., f, tag::otherwise );  }

inline Function min ( const Function & f, const Function & g )
{	return Function ( tag::piecewise, f, tag::iff, f-g, tag::less_than, 0., g, tag::otherwise );  }

inline Function max ( const Function & f, const Function & g, const Function & h )
{	return max ( f, max ( g, h ) );  }

inline Function min ( const Function & f, const Function & g, const Function & h )
{	return min ( f, min ( g, h ) );  }

inline Function max ( const Function & f, const Function & g, const Function & h, const Function & i )
{	return max ( max ( f, g ), max ( h, i ) );  }

inline Function min ( const Function & f, const Function & g, const Function & h, const Function & i )
{	return min ( min ( f, g ), min ( h, i ) );  }

inline Function max
( const Function & f, const Function & g, const Function & h, const Function & i, const Function & j )
{	return max ( max ( f, g ), max ( h, i ), j );  }

inline Function min
( const Function & f, const Function & g, const Function & h, const Function & i, const Function & j )
{	return min ( min ( f, g ), min ( h, i ), j );  }

inline Function max
( const Function & f, const Function & g, const Function & h,
  const Function & i, const Function & j, const Function & k )
{	return max ( max ( f, g ), max ( h, i ), max ( j, k ) );  }

inline Function min
( const Function & f, const Function & g, const Function & h,
  const Function & i, const Function & j, const Function & k )
{	return min ( min ( f, g ), min ( h, i ), min ( j, k ) );  }

inline Function abs ( const Function & f )
{	return max ( f, -f );  }

inline Function sign ( const Function & f )
{	return Function ( tag::piecewise,
	        Function (-1.), tag::iff, f, tag::less_than, 0., Function(1.), tag::otherwise );  }

//------------------------------------------------------------------------------------------------------//


namespace tag  {  struct Threshold { };  static const Threshold threshold;  }

inline Function smooth_abs ( const Function & f, const tag::Threshold &, double d )
// a smooth (C1) approximation of the absolute value
// 'd' is a threshold; if |f| >= d then smooth_abs(f) == |f|
// below 'd', the function is smooth but far from the absolute value (never less than d/2)
{	assert ( d > 0. );
	return Function ( tag::piecewise,
	        -f,                     tag::iff, f, tag::less_than, -d,
	        ( f*f + d*d ) / (2.*d), tag::if_less_than, d, f, tag::otherwise );  }
//	Function abs_f = sign ( f ) * f;
//	return abs_f * abs_f / ( d + abs_f );  }

inline Function smooth_min
( const Function & f, const Function & g, const tag::Threshold &, double d )
// a smooth (C1) approximation of the minimum between two functions
{	return 0.5 * ( f + g - smooth_abs ( f-g, tag::threshold, d ) );  }

inline Function smooth_min
( const Function & f, const Function & g, const Function & h, const tag::Threshold &, double d )
// a smooth (C1) approximation of the minimum between three functions
{	return smooth_min ( f, smooth_min ( g, h, tag::threshold, d ), tag::threshold, d );  }

inline Function smooth_min
( const Function & f, const Function & g, const Function & h, const Function & i,
  const tag::Threshold &, double d                                                )
// a smooth (C1) approximation of the minimum between four functions
{	return smooth_min ( smooth_min ( f, g, tag::threshold, d ),
	                    smooth_min ( h, i, tag::threshold, d ), tag::threshold, d );  }

inline Function smooth_min
( const Function & f, const Function & g, const Function & h, const Function & i,
  const Function & j ,const tag::Threshold &, double d                            )
// a smooth (C1) approximation of the minimum between five functions
{	return smooth_min ( smooth_min ( f, g, h, tag::threshold, d ),
	                    smooth_min ( i, j, tag::threshold, d ), tag::threshold, d );  }

inline Function smooth_min
( const Function & f, const Function & g, const Function & h, const Function & i,
  const Function & j, const Function & k, const tag::Threshold &, double d        )
// a smooth (C1) approximation of the minimum between six functions
{	return smooth_min ( smooth_min ( f, g, tag::threshold, d ),
	                    smooth_min ( h, i, tag::threshold, d ),
	                    smooth_min ( j, k, tag::threshold, d ), tag::threshold, d );  }

inline Function smooth_max
( const Function & f, const Function & g, const tag::Threshold &, double d )
// a smooth (C1) approximation of the maximum between two functions
{	return 0.5 * ( f + g + smooth_abs ( f-g, tag::threshold, d ) );  }

inline Function smooth_max
( const Function & f, const Function & g, const Function & h, const tag::Threshold &, double d )
// a smooth (C1) approximation of the maximum between three functions
{	return smooth_max ( f, smooth_max ( g, h, tag::threshold, d ), tag::threshold, d );  }

inline Function smooth_max
( const Function & f, const Function & g, const Function & h, const Function & i,
  const tag::Threshold &, double d                                                )
// a smooth (C1) approximation of the maximum between four functions
{	return smooth_max ( smooth_max ( f, g, tag::threshold, d ),
	                    smooth_max ( h, i, tag::threshold, d ), tag::threshold, d );  }

inline Function smooth_max
( const Function & f, const Function & g, const Function & h, const Function & i,
  const Function & j ,const tag::Threshold &, double d                            )
// a smooth (C1) approximation of the maximum between five functions
{	return smooth_max ( smooth_max ( f, g, h, tag::threshold, d ),
	                    smooth_max ( i, j, tag::threshold, d ), tag::threshold, d );  }

inline Function smooth_max
( const Function & f, const Function & g, const Function & h, const Function & i,
  const Function & j, const Function & k, const tag::Threshold &, double d        )
// a smooth (C1) approximation of the maximum between six functions
{	return smooth_max ( smooth_max ( f, g, tag::threshold, d ),
	                    smooth_max ( h, i, tag::threshold, d ),
	                    smooth_max ( j, k, tag::threshold, d ), tag::threshold, d );  }
										
//------------------------------------------------------------------------------------------------------//
//------------------------------------------------------------------------------------------------------//


#ifndef MANIFEM_NO_QUOTIENT

class Function::ActionGenerator

// a generator of a discrete group

{	public :

	static size_t counter;
	size_t id;
	
	Function coords, transf;
	// here we momentarily keep the coordinates function together with the composed ones
	// just prior to the declaration of the respective quotient manifold
	// the quotient manifold will then keep these coordinates
	// after that, we can forget about them

	inline ActionGenerator ( )
	:	id { Function::ActionGenerator::counter },
		coords ( tag::non_existent ), transf ( tag::non_existent )
	{	Function::ActionGenerator::counter++;  }

	inline ActionGenerator
	( const tag::Transforms &, const Function f, const tag::Into &, const Function g )
	:	id { Function::ActionGenerator::counter }, coords ( f ), transf ( g )
	{	Function::ActionGenerator::counter++;  }

	inline ActionGenerator ( const Function::ActionGenerator & a )
	:	id { a .id }, coords ( a .coords ), transf ( a .transf )
	{	}

	inline ActionGenerator operator= ( const Function::ActionGenerator & a )
	{	this->id = a .id;  return *this;  }
	
	inline operator Function::Action() const;

	struct Applied { class ToFunction;  };	

};  // end of class Function::ActionGenerator


inline bool operator== ( const Function::ActionGenerator & a, const Function::ActionGenerator & b )
{	return a .id == b .id;  }


inline bool operator< ( const Function::ActionGenerator & f, const Function::ActionGenerator & g )
{	return f .id < g .id;  }
// needed for Function::Action::index_map

//------------------------------------------------------------------------------------------------------//


class tag::Util::Action  //  aka  class Function::Action, aka class Manifold::Action
		// we define it here because we need it for Function::MultiValued
		// but we prefer the user to see it as an attribute of class Manifold

// a composition of actions, thus an element of the group
// essentially, a multi-index, more precisely a map < Function::ActionGenerator, short int >

// the final user will compose actions in an additive notation,
// e.g.  2*g - h  rather than  g^2 h^-1

{	public :

	std::map < Function::ActionGenerator, short int > index_map;

	inline Action ( ) { };
		
	inline Action ( short int i )
	{	assert ( i == 0 );  }
	
	inline Action ( const Function::ActionGenerator & g, short int i )
	:	index_map { std::pair < Function::ActionGenerator, short int > ( g, i ) }
	{	}

	inline Action ( const Function::ActionGenerator & g, short int i,
	                const Function::ActionGenerator & h, short int j )
	:	index_map { std::pair < Function::ActionGenerator, short int > ( g, i ),
		            std::pair < Function::ActionGenerator, short int > ( h, j )  }
	{	}

	inline Action ( const Function::ActionGenerator & g1, short int i,
	                const Function::ActionGenerator & g2, short int j,
	                const Function::ActionGenerator & g3, short int k )
	:	index_map { std::pair < Function::ActionGenerator, short int > ( g1, i ),
		            std::pair < Function::ActionGenerator, short int > ( g2, j ),
		            std::pair < Function::ActionGenerator, short int > ( g3, k )  }
	{	}

	inline Action ( const tag::Transforms &, const Function f, const tag::Into &, const Function g )
	{	this->index_map.emplace ( std::piecewise_construct,
		  std::forward_as_tuple( tag::transforms, f, tag::into, g ), std::forward_as_tuple(1) );  }
		//	{	Function::ActionGenerator a ( tag::transforms, f, tag::into, g );
		//		this->index_map [a] = 1;                                           }

	inline Function::Action operator= ( const short int zero );

};  // end of class Function::Action


inline bool operator== ( const Function::Action & a, const Function::Action & b )

{	{ // just a block for hiding names
	std::map <Function::ActionGenerator, short int > ::const_iterator it_a =
		a .index_map .begin();
	for ( ; it_a != a .index_map .end(); it_a++ )
	{	Function::ActionGenerator aa = it_a->first;
		std::map < Function::ActionGenerator, short int > ::const_iterator it_b =
			b .index_map .find ( aa );
		if ( it_b == b .index_map .end() ) return false;
		if ( it_a->second != it_b->second ) return false;                              }
	} { // just a block for hiding names
	std::map < Function::ActionGenerator, short int > ::const_iterator it_b =
		b .index_map .begin();
	for ( ; it_b != b .index_map .end(); it_b++ )
	{	Function::ActionGenerator bb = it_b->first;
		std::map < Function::ActionGenerator, short int > ::const_iterator it_a =
			a .index_map .find ( bb );
		if ( it_a == a .index_map .end() ) return false;
		assert ( it_a->second == it_b->second );                                       }
	} // just a block for hiding names
	return true;                                                                       }


inline bool operator!= ( const Function::Action & a, const Function::Action & b )
{	return not ( a == b );  }


inline bool operator== ( const Function::Action & a, const short int zero )
// shorthand for quering the identity action :  id == 0
{	assert ( zero == 0 );
	return a .index_map .size() == 0;  }


inline bool operator!= ( const Function::Action & a, const short int zero )
{	return not ( a == zero );  }
	

inline Function::Action Function::Action::operator= ( const short int zero )
// shorthand for quering the identity action :  id = 0
{	assert ( zero == 0 );
	this->index_map .clear();
	return *this;             }

	
inline Function::ActionGenerator::operator Function::Action() const
{	return Function::Action ( *this, 1 );  }


inline Function::Action operator+
( const Function::Action & a, const Function::Action & b )
{	Function::Action res = a;
	std::map < Function::ActionGenerator, short int > ::const_iterator it =
		b .index_map .begin();
	for ( ; it != b .index_map .end(); it++ )
	{	const Function::ActionGenerator & g = it->first;
		// inspired in item 24 of the book : Scott Meyers, Effective STL
		std::map < Function::ActionGenerator, short int > ::iterator itt =
			res .index_map .lower_bound (g);
		if ( ( itt == res .index_map .end() ) or
		     ( res .index_map .key_comp() ( g, itt->first ) ) )
			// new action
			res .index_map .emplace_hint ( itt, std::piecewise_construct,
	      std::forward_as_tuple (g), std::forward_as_tuple ( it->second ) ); 
		else  // action already there
		{	itt->second += it->second;  // could be zero
			if ( itt->second == 0 ) res .index_map .erase ( itt );  }            }
	return res;                                                                 }


inline Function::Action operator+=
( Function::Action & a, const Function::Action & b )
{	std::map < Function::ActionGenerator, short int > ::const_iterator it =
		b .index_map .begin();
	for ( ; it != b .index_map .end(); it++ )
	{	const Function::ActionGenerator & g = it->first;
		// inspired in item 24 of the book : Scott Meyers, Effective STL
		std::map < Function::ActionGenerator, short int > ::iterator itt =
			a .index_map .lower_bound (g);
		if ( ( itt == a .index_map .end() ) or
	       ( a .index_map .key_comp() ( g, itt->first ) ) )
			// new action
			a .index_map .emplace_hint ( itt, std::piecewise_construct,
	      std::forward_as_tuple (g), std::forward_as_tuple ( it->second ) ); 
		else  // action already there
		{	itt->second += it->second;  // could be zero
			if ( itt->second == 0 ) a .index_map .erase ( itt );  }              }
	return a;                                                                   }


inline Function::Action operator-
( const Function::Action & a, const Function::Action & b )
{	Function::Action res = a;
	std::map < Function::ActionGenerator, short int > ::const_iterator it =
		b .index_map .begin();
	for ( ; it != b .index_map .end(); it++ )
	{	const Function::ActionGenerator & g = it->first;
		// inspired in item 24 of the book : Scott Meyers, Effective STL
		std::map < Function::ActionGenerator, short int > ::iterator itt =
			res .index_map .lower_bound (g);
		if ( ( itt == res .index_map .end() ) or
	       ( res .index_map .key_comp() ( g, itt->first ) ) )
			// new action
			res .index_map .emplace_hint ( itt, std::piecewise_construct,
	      std::forward_as_tuple (g), std::forward_as_tuple ( -it->second ) ); 
		else  // action already there
		{	itt->second -= it->second;  // could be zero
			if ( itt->second == 0 ) res .index_map .erase ( itt );  }              }
	return res;                                                                  }


inline Function::Action operator-=
( Function::Action & a, const Function::Action & b )
{	std::map < Function::ActionGenerator, short int > ::const_iterator it =
		b .index_map .begin();
	for ( ; it != b .index_map .end(); it++ )
	{	const Function::ActionGenerator & g = it->first;
		// inspired in item 24 of the book : Scott Meyers, Effective STL
		std::map < Function::ActionGenerator, short int > ::iterator itt =
			a .index_map .lower_bound (g);
		if ( ( itt == a .index_map .end() ) or
		     ( a .index_map .key_comp() ( g, itt->first ) ) )
			// new action
			a .index_map .emplace_hint ( itt, std::piecewise_construct,
			     std::forward_as_tuple (g), std::forward_as_tuple ( -it->second ) ); 
		else  // action already there
		{	itt->second -= it->second;  // could be zero
			if ( itt->second == 0 ) a .index_map .erase ( itt );  }               }
	return a;                                                                   }


inline Function::Action operator*
( const short int k, const Function::Action & a )
{	if ( k == 0 ) return Function::Action ( 0 );
	Function::Action res = a;
	std::map < Function::ActionGenerator, short int > ::iterator it =
		res .index_map .begin();
	for ( ; it != res .index_map .end(); it++ ) it->second *= k;
	return res;                                                        }


inline Function::Action operator-
( const Function::Action & a )
{	return (-1) * a;  }

//------------------------------------------------------------------------------------------------------//


class Cell::Winding

// temporary object returned by Cell::winding() and used for assingment of winding numbers

// only positive segments have space reserved for holding a winding number
// however, the arithmetic operators of this class are crafted in a manner
// which allows getting and setting the winding number of a negative segment, too
// reversed arithmetic operations are performed on the reverse, positive, segment

{	public :

	Cell::Core * cll;  // usually a segment
	// Cell::Winding should only be used as temporary objects
	// they should be immediately converted to a (reference to a) double or vector<double>
	// so an ordinary pointer is OK here

	inline Winding ( const Cell & c )
	:	cll { c.core }
	{	assert ( this->cll );  }

	// mudar o resultado (o return value) das operacoes abaixo para Cell::Winding
	// assim havera' conversoes so' quando necessario
	// porque muitas vezes o resultado nao e' usado e as conversoes ocupam recursos desnecessariamente
	// paragrafo em attic/manifem-comnent.txt
	
	Cell::Winding operator= ( const Cell::Winding & w );      // defined in manifold.cpp
	Cell::Winding operator=  ( const Function::Action & a );  // defined in manifold.cpp
	Cell::Winding operator+= ( const Function::Action & a );  // defined in manifold.cpp
	Cell::Winding operator-= ( const Function::Action & a );  // defined in manifold.cpp
	
	operator Function::Action ( ) const;                      // defined in manifold.cpp

};  // end of  class Cell::Winding

//------------------------------------------------------------------------------------------------------//
//------------------------------------------------------------------------------------------------------//


class Function::MultiValued

// abstract class
// classes Function::Scalar::MultiValued and Function::Vector::MultiValued inherit from here

{	public :

	Function base;

	std::vector < Function::ActionGenerator > actions;

	inline MultiValued ( const Function & b, std::vector < Function::ActionGenerator > a )
	:	base { b }, actions { a }
	{	// assert ( Manifold::working.actions == a );
	}

};  // end of class Function::Scalar::Multivalued

//------------------------------------------------------------------------------------------------------//


class Function::Scalar::MultiValued : public Function::MultiValued, public Function::Scalar

// here (finally) the method get_value_on_cell with tag::winding is meaningful
// suppose 'exp' is a pair of short integers (i,j)
// then the above refered method checks that the 'actions' match
// those of the current working manifold
// then takes the value of 'base' on the cell, applies the first action 'i' times
// then applies the second action 'j' times (recall the group should be commutative)
// the vector of 'actions' is only used for the above refered checking operation

// abstract class, specialized in Function::Scalar::MultiValued::JumpIsSum and JumpIsLinear

{	public :

	// members inherited from Function::MultiValued :
	// Function base  -- here must be Function::Scalar
	// std::vector < Function::ActionGenerator > actions

	inline MultiValued ( const tag::AssociatedWith &, const Function & b,
	                     std::vector < Function::ActionGenerator > a               )
	:	Function::MultiValued ( b, a )
	{	}

	MultiValued ( const Function::Scalar::MultiValued & ) = delete;
	MultiValued ( Function::Scalar::MultiValued && ) = delete;
	
	// size_t number_of ( const tag::Components & )
	//   virtual from Function::Core, defined by Function::Scalar, returns 1

	// Function component ( size_t i )  virtual from Function::Core,
	//   defined by Function::Scalar, returns self, never actually used

	void set_value ( double );  // virtual from Function::Scalar, delegates to 'base'

	double get_value_on_cell ( Cell::Core * ) const;
	//  virtual from Function::Scalar, delegates to 'base'

	// double get_value_on_cell  with tag::winding  stays pure virtual from Function::Scalar

	double set_value_on_cell ( Cell::Core *, const double & );
	//  virtual from Function::Scalar, delegates to 'base'

	Function deriv ( Function );  // virtual from Function::Core

	Function replace ( const Function & x, const Function & y );
	//  virtual from Function::Core
	
	// Function::Jump jump  virtual, defined by Function::Core, execution forbidden

	// void analyse_integrand   virtual, defined by Function::Core,
	// execution forbidden      ifndef MANIFEM_NO_FEM

	#ifndef NDEBUG  // DEBUG mode	
	std::string repr ( const Function::From & from = Function::from_void ) const;
	//  virtual from Function::Core
	#endif

	class JumpIsSum;  class JumpIsLinear;

};  // end of class Function::Scalar::MultiValued

//------------------------------------------------------------------------------------------------------//


class Function::Scalar::MultiValued::JumpIsSum : public Function::Scalar::MultiValued

// here the actions are mere translations on RR (sums)

{	public :

	// members inherited from Function::MultiValued :
	// Function base  -- here must be Function::Scalar
	// std::vector < Function::ActionGenerator > actions
	
	std::vector < double > beta;
	// upon each action [i], the value v of the function becomes  v + beta [i]
	// use a Function::Jump::Sum::Scalar instead !
	
	inline JumpIsSum ( const tag::AssociatedWith &, const Function & f,
	                   std::vector < Function::ActionGenerator > ac, std::vector < double > be )
	:	Function::Scalar::MultiValued ( tag::associated_with, f, ac ), beta { be }
	{	assert ( ac .size() == be .size() );  }

	JumpIsSum ( const Function::Scalar::MultiValued::JumpIsSum & ) = delete;
	JumpIsSum ( Function::Scalar::MultiValued::JumpIsSum && ) = delete;
	
	Function::Scalar::MultiValued::JumpIsSum operator=
		( const Function::Scalar::MultiValued::JumpIsSum & ) = delete;
	Function::Scalar::MultiValued::JumpIsSum operator=
		( Function::Scalar::MultiValued::JumpIsSum && ) = delete;

	// size_t number_of ( const tag::Components & )
	//   virtual from Function::Core, defined by Function::Scalar, returns 1

	// Function component ( size_t i )  virtual from Function::Core,
	//   defined by Function::Scalar, returns self, never actually used

	// void set_value ( double )
	//   defined by Function::Scalar::Multivalued, delegates to 'base'

	using Function::Scalar::MultiValued::get_value_on_cell;
	
	// double get_value_on_cell ( Cell::Core * ) const;
	//   defined by Function::Scalar::MultiValued, delegates to 'base'

	double get_value_on_cell
	( Cell::Core *, const tag::Winding &, const Function::Action & exp ) const;
	//  virtual from Function::Scalar
	
	// double set_value_on_cell ( Cell::Core *, const double & )
	//   defined by Function::Scalar::MultiValued, execution forbidden

	Function::Jump jump ( ) override;  // virtual from Function::Core
	
	// the return value of 'analyse_linear_expression' should be a double
	// however, we use std::vector < double > instead
	// a zero-length vector means not succeeded
	// success is represented by a vector of length one
	inline static std::vector < double > analyse_linear_expression
		( Function expression, Function base );
	
	// void analyse_integrand   virtual, defined by Function::Core,
	// execution forbidden      ifndef MANIFEM_NO_FEM

};  // end of class Function::Scalar::MultiValued::JumpIsSum

//------------------------------------------------------------------------------------------------------//


class Function::Scalar::MultiValued::JumpIsLinear : public Function::Scalar::MultiValued

// here the actions are linear (affine) maps on RR

{	public :

	// members inherited from Function::MultiValued :
	// Function base  -- here must be Function::Scalar
	// std::vector < Function::ActionGenerator > actions
	
	std::vector < double > alpha;
	double gamma;
	// upon each action [i], the value v of the function becomes  alpha[i] * v + beta[i]
	// group is commutative only if  beta[i] / (alpha[i]-1.)  does not depend on i
	// we call  gamma  the common value, so  beta[i] = gamma * (alpha[i]-1.)
	// upon action[i], the value v of the function becomes  alpha[i] * ( v - gamma ) + gamma
	// it is the user's responsibility to ensure that each action is invertible
	//   that is,  alpha [i] != 0.
	// and that they commute, that is,  beta[i] / (alpha[i]-1.)  does not depend on i
	//   often, beta [i] are all zero so this is not a problem
	
	inline JumpIsLinear ( const tag::AssociatedWith &, const Function & f,
	                      std::vector < Function::ActionGenerator > ac,
	                      std::vector < double > al, double ga            )
	:	Function::Scalar::MultiValued ( tag::associated_with, f, ac ), alpha { al }, gamma { ga }
	{	assert ( ac .size() == al .size() );  }

	JumpIsLinear ( const Function::Scalar::MultiValued::JumpIsLinear & ) = delete;
	JumpIsLinear ( Function::Scalar::MultiValued::JumpIsLinear && ) = delete;
	
	Function::Scalar::MultiValued::JumpIsLinear operator=
		( const Function::Scalar::MultiValued::JumpIsLinear & ) = delete;
	Function::Scalar::MultiValued::JumpIsLinear operator=
		( Function::Scalar::MultiValued::JumpIsLinear && ) = delete;

	// size_t number_of ( const tag::Components & )
	//   virtual from Function::Core, defined by Function::Scalar, returns 1

	// Function component ( size_t i )  virtual from Function::Core,
	//   defined by Function::Scalar, returns self, never actually used

	// void set_value ( double )
	//   defined by Function::Scalar::MultiValued, delegates to 'base'

	using Function::Scalar::MultiValued::get_value_on_cell;
	
	// double get_value_on_cell ( Cell::Core * ) const;
	//   defined by Function::Scalar::MultiValued, delegates to 'base'

	double get_value_on_cell
	( Cell::Core *, const tag::Winding &, const Function::Action & exp ) const;
	//  virtual from Function::Scalar
	
	// double set_value_on_cell ( Cell::Core *, const double & )
	//   defined by Function::Scalar::MultiValued, execution forbidden

	// Function::Jump jump  virtual, defined by Function::Core, execution forbidden
	
	static std::pair < std::vector < double >, double > analyse_linear_expression
	( Function expression, Function base );

	// void analyse_integrand   virtual, defined by Function::Core,
	// execution forbidden      ifndef MANIFEM_NO_FEM
	
};  // end of class Function::Scalar::MultiValued::JumpIsLinear

//------------------------------------------------------------------------------------------------------//


class Function::Vector::MultiValued : public Function::MultiValued, public Function::Aggregate

// same as Function::Scalar::MultiValued, here with vector values

// abstract class, specialized in Function::Vector::MultiValued::JumpIsSum and JumpIsLinear

// inheriting from Function::Aggregate means simply that there is a 'components' member
							 
{	public :

	// members inherited from Function::MultiValued :
	// Function base  -- here must be Function::Vector
	// std::vector < Function::ActionGenerator > actions

	// inherited from Function::Aggregate :
	// std::vector < Function > components

	inline MultiValued ( const tag::AssociatedWith &, const Function & b,
	                     std::vector < Function::ActionGenerator > a     )
	:	Function::MultiValued ( b, a ),
		Function::Aggregate ( tag::reserve_size, b .number_of ( tag::components ) )
	{	}
	
	MultiValued ( const Function::Vector::MultiValued & ) = delete;
	MultiValued ( Function::Vector::MultiValued && ) = delete;
	
	size_t number_of ( const tag::Components & ) const;
	// virtual from Function::Core, delegates to 'base'

	// Function component ( size_t i )
	//   virtual from Function::Core, defined by Function::Aggregate

	void set_value ( std::vector < double > );
	// virtual from Function::Vector, here delegates to 'base'

	std::vector < double > get_value_on_cell ( Cell::Core * ) const;

	// std::vector < double > get_value_on_cell  with tag::winding
	//   stays pure virtual from Function::Vector

	std::vector < double > set_value_on_cell ( Cell::Core *, const std::vector < double > & );
	// virtual from Function::Vector

	Function deriv ( Function );  // virtual from Function::Core

	Function replace ( const Function & x, const Function & y );
	//  virtual from Function::Core
	
	// Function::Jump jump  virtual, defined by Function::Core, execution forbidden
	
	// void analyse_integrand   virtual, defined by Function::Core,
	// execution forbidden      ifndef MANIFEM_NO_FEM

	#ifndef NDEBUG  // DEBUG mode	
	std::string repr ( const Function::From & from = Function::from_void ) const;
	//  virtual from Function::Core
	#endif

	class JumpIsSum;  class JumpIsLinear;

};  // end of class Function::Vector::MultiValued

//------------------------------------------------------------------------------------------------------//


class Function::Vector::MultiValued::JumpIsSum : public Function::Vector::MultiValued

// here the actions are mere translations on RR^n (sums)

{	public :

	// members inherited from Function::MultiValued :
	// Function base  -- here must be Function::Vector
	// std::vector < Function::ActionGenerator > actions
	
	std::vector < std::vector < double > > beta;
	// upon each action[i], the value v of the function becomes  v + beta[i]
	// use a Function::Jump::Sum::Vector instead !

	inline JumpIsSum ( const tag::AssociatedWith &, const Function & f,
	                   std::vector < Function::ActionGenerator > ac,
	                   std::vector < std::vector < double > > be        )
	:	Function::Vector::MultiValued ( tag::associated_with, f, ac ), beta { be }
	{	size_t n_act = ac .size();
		assert ( n_act == be .size() );
		size_t n_comp = f .number_of ( tag::components );
		std::vector < double > beta_scalar ( n_act, 0. );
		for ( size_t j = 0; j < n_comp; j++ )
		{	for ( size_t i = 0; i < n_act; i++ ) beta_scalar [i] = be [i][j];
			// this->components [j] = Function ( tag::whose_core_is,
			this->components.emplace_back ( tag::whose_core_is,
			  new Function::Scalar::MultiValued::JumpIsSum
	                    ( tag::associated_with, f[j], ac, beta_scalar ) );  }  }

	JumpIsSum ( const Function::Vector::MultiValued::JumpIsSum & ) = delete;
	JumpIsSum ( Function::Vector::MultiValued::JumpIsSum && ) = delete;
	
	Function::Vector::MultiValued::JumpIsSum operator=
		( const Function::Vector::MultiValued::JumpIsSum & ) = delete;
	Function::Vector::MultiValued::JumpIsSum operator=
		( Function::Vector::MultiValued::JumpIsSum && ) = delete;

	// size_t number_of ( const tag::Components & )  virtual from Function::Core
	//   defined by Function::Vector::MultiValued, delegates to 'base'

	// Function component ( size_t i )
	//   virtual from Function::Core, defined by Function::Aggregate

	// void set_value ( std::vector < double > )
	//   defined by Function::Vector::MultiValued, delegates to 'base'

	using Function::Vector::MultiValued::get_value_on_cell;

	// std::vector < double > get_value_on_cell ( Cell::Core * ) const;
	//   defined by Function::Vector::MultiValued, delegates to 'base'

	std::vector < double > get_value_on_cell
	( Cell::Core *, const tag::Winding &, const Function::Action & exp ) const;
	//  virtual from Function::Vector
	
	// std::vector < double > set_value_on_cell ( Cell::Core *, const std::vector < double > & )
	//   defined by Function::Vector::MultiValued

	Function::Jump jump ( ) override;  // virtual from Function::Core
	
	// void analyse_integrand   virtual, defined by Function::Core,
	// execution forbidden      ifndef MANIFEM_NO_FEM

	inline static std::vector < double > analyse_linear_expression
	( Function expression, Function base );
	
};  // end of class Function::Vector::MultiValued::JumpIsSum

//------------------------------------------------------------------------------------------------------//


class Function::Vector::MultiValued::JumpIsLinear : public Function::Vector::MultiValued

// here the actions are linear (affine) maps on RR^n

{	public :

	// members inherited from Function::MultiValued :
	// Function base  -- here must be Function::Vector
	// std::vector < Function::ActionGenerator > actions
	
	std::vector < std::vector < std::vector < double > > > A;
	std::vector < std::vector < double > > b;
	// upon each action [i], the value v of the function becomes  A[i]*v + b[i]
	// it is the user's responsibility to ensure that each action is invertible
	//   (i.e. the matrix A[i] is invertible) and that they commute
	// often, b[i] are all zero so it suffices that the matrices A[i] commute
	//   A[i1] A[i2] == A[i2] A[i1]

	// we keep also the inverse matrices
	std::vector < std::vector < std::vector < double > > > Ainv;

	inline JumpIsLinear ( const tag::AssociatedWith &, const Function & f,
	                      std::vector < Function::ActionGenerator > ac,
	                      std::vector < std::vector < std::vector < double > > > AA,
	                      std::vector < std::vector < double > > bb                 )
	:	Function::Vector::MultiValued ( tag::associated_with, f, ac ), A { AA },  b { bb }
	{	assert ( ac .size() == AA .size() );
		assert ( ac .size() == bb .size() );
		// we need to compute the inverse matrices, we do this only for 2x2 matrices
		std::vector < std::vector < std::vector < double > > > ::const_iterator it;
		for ( it = this->A .begin(); it != this->A .end(); it++ )
		{	const std::vector < std::vector < double > > & mat = *it;
			assert ( mat.size() == 2 );
			double det = mat[0][0] * mat[1][1] - mat[0][1] * mat[1][0];
			this->Ainv .push_back ( { {   mat[1][1] / det, - mat[0][1] / det },
			                          { - mat[1][0] / det,   mat[0][0] / det } } );  }  }
	
	JumpIsLinear ( const Function::Vector::MultiValued::JumpIsLinear & ) = delete;
	JumpIsLinear ( Function::Vector::MultiValued::JumpIsLinear && ) = delete;
	
	Function::Vector::MultiValued::JumpIsLinear operator=
		( const Function::Vector::MultiValued::JumpIsLinear & ) = delete;
	Function::Vector::MultiValued::JumpIsLinear operator=
		( Function::Vector::MultiValued::JumpIsLinear && ) = delete;

	// size_t number_of ( const tag::Components & )  virtual from Function::Core
	//   defined by Function::Vector::MultiValued, delegates to 'base'

	Function component ( size_t i ) override;   // virtual from Function::Core,
	//  virtual from Function::Core, defined by Function::Aggregate, here overridden
	//  execution forbidden for now, this is a difficult case

	// void set_value ( std::vector < double > )
	//   defined by Function::Vector::MultiValued, delegates to 'base'

	using Function::Vector::MultiValued::get_value_on_cell;

	// std::vector < double > get_value_on_cell ( Cell::Core * ) const;
	//   defined by Function::Vector::MultiValued, delegates to 'base'

	std::vector < double > get_value_on_cell
	( Cell::Core *, const tag::Winding &, const Function::Action & exp ) const;
	//  virtual from Function::Vector
	
	// std::vector < double > set_value_on_cell ( Cell::Core *, const std::vector < double > & )
	//   defined by Function::Vector::MultiValued

	// Function::Jump jump  virtual, defined by Function::Core, execution forbidden

	// void analyse_integrand   virtual, defined by Function::Core,
	// execution forbidden      ifndef MANIFEM_NO_FEM

	inline static std::pair < std::vector < std::vector < double > >, std::vector < double > >
	analyse_linear_expression ( Function expression, Function base );

};  // end of class Function::Vector::MultiValued::JumpIsLinear

//------------------------------------------------------------------------------------------------------//


class Function::Jump

// a Function::Jump object describes the behaviour of a multi-function
// when the point goes "around" the manifold a given number of times
// a Cell::Winding describes this number of times

// so a Function::Jump can be applied to a Cell::Winding, producing as result
// a double for a Function::Scalar::MultiValued
// or a vector of doubles for a Function::Vector::MultiValued


{	public :

	std::vector < Function::ActionGenerator > actions;
	
	std::vector < double > ju;
	
	double operator() ( const Function::Action & a ) const;

};  // end of class Function::Jump::Sum::Scalar


//------------------------------------------------------------------------------------------------------//


Function::Jump operator+ ( const Function::Jump & j1, const Function::Jump & j2 );

Function::Jump operator+= ( Function::Jump & j1, const Function::Jump & j2 );

Function::Jump operator- ( const Function::Jump & j1, const Function::Jump & j2 );

Function::Jump operator-= ( Function::Jump & j1, const Function::Jump & j2 );

Function::Jump operator* ( double a, const Function::Jump & j );

inline Function::Jump operator* ( const Function::Jump & j, double a )
{	return a*j;  }

Function::Jump operator*= ( Function::Jump & j, double a );

inline Function::Jump operator/ ( const Function::Jump & j, double a )
{	return (1./a)*j;  }

inline Function::Jump operator/= ( Function::Jump & j, double a )
{	return operator*= ( j, 1./a );  }

//------------------------------------------------------------------------------------------------------//


inline std::vector < double >  // static
Function::Scalar::MultiValued::JumpIsSum::analyse_linear_expression
( Function expression, Function base )

// 'base' is a variable, say x
// 'expression' is an expression in x (a sum x+c)
// we want to identify c

// the return value should be a double
// however, we use std::vector < double > instead
// a zero-length vector means not succeeded
// success is represented by a vector of length one

{	assert ( base .number_of ( tag::components ) == 1 );
	assert ( expression .number_of ( tag::components ) == 1 );
	if ( expression .core == base .core )  return { 0. };
	Function::Sum * sum = dynamic_cast < Function::Sum * > ( expression .core );
	if ( sum == nullptr )  return { };
	std::forward_list < Function > terms = sum->terms;
	std::forward_list<Function>::iterator it = terms .begin();
	assert ( it != terms .end() );
	Function x = *it;
	if ( x.core != base .core )
	{	Function c = x;
		Function::Constant * cc = dynamic_cast < Function::Constant * > ( c .core );
		if ( cc == nullptr )  return { };
		it++;
		assert ( it != terms .end() );
		Function xx = *it;
		if ( xx .core != base .core )  return { };
		return { cc->value };                                                           }
	it++;
	assert ( it != terms .end() );
	Function c = *it;
	it++;
	if ( it != terms .end() )  return { };
	Function::Constant * cc = dynamic_cast < Function::Constant * > ( c .core );
	if ( cc == nullptr )  return { };
	return  { cc->value };                                                                }

//------------------------------------------------------------------------------------------------------//


inline std::vector < double >  // static
Function::Vector::MultiValued::JumpIsSum::analyse_linear_expression
( Function expression, Function base )

// 'base' is a set of variables, say xyz
// 'expression' is a vector expression in xyz (a sum : x+a, y+b, z+c)
// we want to identify a, b, c
// a zero-length return vector means not succeeded

{	size_t n_coord = base .number_of ( tag::components );
	assert ( n_coord == expression .number_of ( tag::components ) );
	std::vector < double > res;
	res.reserve ( n_coord );
	for ( size_t i = 0; i < n_coord; i++ )
	{	std::vector < double > res_i =
			Function::Scalar::MultiValued::JumpIsSum::analyse_linear_expression
			( expression [i], base [i] );
		if ( res_i .size() == 0 ) return {};
		assert ( res_i .size() == 1 );
		res .push_back ( res_i [0] );                                          }
	return res;                                                                }

//------------------------------------------------------------------------------------------------------//


inline std::pair < std::vector < std::vector < double > >, std::vector < double > >
Function::Vector::MultiValued::JumpIsLinear::analyse_linear_expression  // static
( Function expression, Function base )

// 'base' is a set of variables, say X
// 'expression' is a vector expression in X :  A X + b
// we want to identify A and b

{	size_t n_coord = base .number_of ( tag::components );
	assert ( n_coord == expression .number_of ( tag::components ) );
	std::pair < std::vector < std::vector < double > >, std::vector < double > > res;
	for ( size_t i = 0; i < n_coord; i++ )
	{	std::pair < std::vector < double >, double > res_i =
			Function::Scalar::MultiValued::JumpIsLinear::analyse_linear_expression
			( expression [i], base );
		assert ( res_i .first .size() == n_coord );
		res .first .push_back ( std::move ( res_i .first ) );
		res .second .push_back ( res_i.second );                                   }
	return res;                                                                        }

//------------------------------------------------------------------------------------------------------//


inline Function Function::make_multivalued
( const tag::Through &, const Function::Action & g,
  const tag::Becomes &, const Function & new_f     )

{	std::vector < Function::ActionGenerator > vec_g;
	assert ( g .index_map .size() == 1 );
	assert ( g .index_map .begin()->second == 1 );
	const Function::ActionGenerator & gg = g .index_map .begin()->first;
	vec_g .push_back (gg);

	size_t n = this->number_of ( tag::components );
	assert ( n == new_f .number_of ( tag::components ) );

	if ( n == 1 )
	{	std::vector < double > res_sum =
			Function::Scalar::MultiValued::JumpIsSum::analyse_linear_expression
			( new_f, *this );
		if ( res_sum .size() > 0 )
			return Function ( tag::whose_core_is, new Function::Scalar::MultiValued::JumpIsSum
			                                         ( tag::associated_with, *this, vec_g, res_sum ) );
		std::pair < std::vector < double >, double > res_lin =
			Function::Scalar::MultiValued::JumpIsLinear::analyse_linear_expression
			( new_f, *this );
		return Function ( tag::whose_core_is, new Function::Scalar::MultiValued::JumpIsLinear
		                  ( tag::associated_with, *this, vec_g, res_lin .first, res_lin .second ) ); }

	std::vector < double > res_sum =
		Function::Vector::MultiValued::JumpIsSum::analyse_linear_expression
		( new_f, *this );
	if ( res_sum .size() > 0 )
		return Function ( tag::whose_core_is, new Function::Vector::MultiValued::JumpIsSum
		                  ( tag::associated_with, *this, vec_g, { res_sum } ) );
	std::pair < std::vector < std::vector < double > >, std::vector < double > > res_lin =
		Function::Vector::MultiValued::JumpIsLinear::analyse_linear_expression
		( new_f, *this );
	return Function ( tag::whose_core_is, new Function::Vector::MultiValued::JumpIsLinear
	                  ( tag::associated_with, *this, vec_g, { res_lin .first }, { res_lin .second } ) );

}  // end of  Function Function::make_multivalued  with one action


//------------------------------------------------------------------------------------------------------//

inline Function::Jump Function::jump ( )
{	return this->core->jump();  }

#endif  // ifndef MANIFEM_NO_QUOTIENT

//------------------------------------------------------------------------------------------------------//
//------------------------------------------------------------------------------------------------------//


class Function::TakenOnCell	

{	public :

	Function::Core * f;
	// Function::TakenOnCell should only be used as temporary objects
	// they should be immediately converted to a (reference to a) double or vector<double>
	// so an ordinary pointer is OK here
	
	Cell::Core * cll;

	inline TakenOnCell ( Function::Core * ff, Cell::Core * cc )
	:	f { ff }, cll { cc }
	{	}

	TakenOnCell ( const Function::TakenOnCell &  other ) = delete;
	TakenOnCell ( const Function::TakenOnCell && other ) = delete;
	
	inline Function::TakenOnCell & operator= ( const Function::TakenOnCell & other )
	{	if ( this->f->number_of ( tag::components ) == 1 )
		{	(*this) = static_cast < double > (other);
			return *this;                              }
		else
		{	(*this) = static_cast < std::vector < double > > (other);
			return *this;                                              }  }

	inline Function::TakenOnCell & operator= ( const Function::TakenOnCell && other )
	{	if ( this->f->number_of ( tag::components ) == 1 )
		{	(*this) = static_cast < double > (other);
			return *this;                              }
		else
		{	(*this) = static_cast < std::vector < double > > (other);
			return *this;                                              }  }
	
	inline operator double() const
	// can be used like in  double x = f ( cll )  or  cout << f ( cll )
	{	Function::Scalar * f_scalar =
			tag::Util::assert_cast < Function::Core*, Function::Scalar* > ( this->f );
		return f_scalar->get_value_on_cell ( this->cll );                              }
		
	inline double operator= ( const double & x )
	// can be used like in  f ( cll ) = 2.0
	{	Function::Scalar * f_scalar =
			tag::Util::assert_cast < Function::Core*, Function::Scalar* > ( this->f );
		return f_scalar->set_value_on_cell ( this->cll, x );                           }

	inline double operator+= ( const double & x )
	// can be used like in  f ( cll ) += 2.0
	{	Function::Scalar * f_scalar =
			tag::Util::assert_cast < Function::Core*, Function::Scalar* > ( this->f );
		return f_scalar->set_value_on_cell
			( this->cll, f_scalar->get_value_on_cell ( this->cll ) + x );               }

	inline double operator-= ( const double & x )
	// can be used like in  f ( cll ) -= 2.0
	{	Function::Scalar * f_scalar =
			tag::Util::assert_cast < Function::Core*, Function::Scalar* > ( this->f );
		return f_scalar->set_value_on_cell
			( this->cll, f_scalar->get_value_on_cell ( this->cll ) - x );               }

	inline double operator*= ( const double & x )
	// can be used like in  f ( cll ) *= 2.0
	{	Function::Scalar * f_scalar =
			tag::Util::assert_cast < Function::Core*, Function::Scalar* > ( this->f );
		return f_scalar->set_value_on_cell
			( this->cll, f_scalar->get_value_on_cell ( this->cll ) * x );               }

	inline double operator/= ( const double & x )
	// can be used like in  f ( cll ) /= 2.0
	{	Function::Scalar * f_scalar =
			tag::Util::assert_cast < Function::Core*, Function::Scalar* > ( this->f );
		return f_scalar->set_value_on_cell
			( this->cll, f_scalar->get_value_on_cell ( this->cll ) / x );               }

	inline operator std::vector<double>() const
	// can be used like in  vector<double> vec = f(cll)
	{	Function::Vector * f_vect = dynamic_cast < Function::Vector* > ( this->f );
		if ( f_vect )  return f_vect->get_value_on_cell ( this->cll );
		Function::Scalar * f_scalar =
			tag::Util::assert_cast < Function::Core*, Function::Scalar* > ( this->f );
		std::vector < double > res ( 1, f_scalar->get_value_on_cell ( this->cll ) );
		return res;                                                                    }
		
	inline std::vector<double> operator= ( const std::vector < double > & x )
	// can be used like in  f ( cll ) = vec
	{	Function::Vector * f_vect = dynamic_cast < Function::Vector* > ( this->f );
		if ( f_vect )  return f_vect->set_value_on_cell ( this->cll, x );
		Function::Scalar * f_scalar =
			tag::Util::assert_cast < Function::Core*, Function::Scalar* > ( this->f );
		assert ( x .size() == 1 );
		std::vector < double > res ( 1, f_scalar->set_value_on_cell ( this->cll, x[0] ) );
		return res;                                                                         }

};  // end of class Function::TakenOnCell	

//------------------------------------------------------------------------------------------------------//


#ifndef MANIFEM_NO_QUOTIENT

class Function::TakenOnWindingCell

{	public :

	Function::Core * f;
	const Function::Action & winding;
	
	// Function::TakenOnWindingCell should only be used as temporary objects
	// they should be immediately converted to a (reference to a) double or vector<double>
	// so an ordinary pointer is OK here
	
	Cell::Core * const cll;

	inline TakenOnWindingCell ( const Function & ff, const Cell & c,
                               const Function::Action & exp )
	:	f { ff .core }, winding { exp }, cll { c .core }
	{	}

	TakenOnWindingCell ( const Function::TakenOnWindingCell &  other ) = delete;
	TakenOnWindingCell ( const Function::TakenOnWindingCell && other ) = delete;
	
	inline Function::TakenOnWindingCell & operator= ( const Function::TakenOnWindingCell & other )
	{	if ( this->f->number_of ( tag::components ) == 1 )
		{	(*this) = static_cast < double > ( other );
			return *this;                                }
		else
		{	(*this) = static_cast < std::vector < double > > ( other );
			return *this;                                                }  }

	inline Function::TakenOnWindingCell & operator= ( const Function::TakenOnWindingCell && other )
	{	if ( this->f->number_of ( tag::components ) == 1 )
		{	(*this) = static_cast < double > ( other );
			return *this;                                }
		else
		{	(*this) = static_cast < std::vector < double > > ( other );
			return *this;                                                }  }
	
	inline operator double() const
	// can be used like in  double x = f ( cll )  or  cout << f ( cll )
	{	Function::Scalar * f_scalar =
			tag::Util::assert_cast < Function::Core*, Function::Scalar* > ( this->f );
		return f_scalar->get_value_on_cell ( this->cll, tag::winding, this->winding );  }
		
	inline double operator= ( const double & x )
	// can be used like in  f ( cll ) = 2.0
	{	Function::Scalar * f_scalar =
			tag::Util::assert_cast < Function::Core*, Function::Scalar* > ( this->f );
		return f_scalar->set_value_on_cell ( this->cll, x );                           }

	inline double operator+= ( const double & x )
	// can be used like in  f ( cll ) += 2.0
	{	Function::Scalar * f_scalar =
			tag::Util::assert_cast < Function::Core*, Function::Scalar* > ( this->f );
		return f_scalar->set_value_on_cell
			( this->cll, f_scalar->get_value_on_cell ( this->cll ) + x );               }

	inline double operator-= ( const double & x )
	// can be used like in  f ( cll ) -= 2.0
	{	Function::Scalar * f_scalar =
			tag::Util::assert_cast < Function::Core*, Function::Scalar* > ( this->f );
		return f_scalar->set_value_on_cell
			( this->cll, f_scalar->get_value_on_cell ( this->cll ) - x );               }

	inline double operator*= ( const double & x )
	// can be used like in  f ( cll ) *= 2.0
	{	Function::Scalar * f_scalar =
			tag::Util::assert_cast < Function::Core*, Function::Scalar* > ( this->f );
		return f_scalar->set_value_on_cell
			( this->cll, f_scalar->get_value_on_cell ( this->cll ) * x );               }

	inline double operator/= ( const double & x )
	// can be used like in  f ( cll ) /= 2.0
	{	Function::Scalar * f_scalar =
			tag::Util::assert_cast < Function::Core*, Function::Scalar* > ( this->f );
		return f_scalar->set_value_on_cell
			( this->cll, f_scalar->get_value_on_cell ( this->cll ) / x );               }

	inline operator std::vector<double>() const
	// can be used like in  vector<double> vec = f ( cll )
	{	Function::Vector * f_vect = dynamic_cast < Function::Vector* > ( this->f );
		if ( f_vect )
			return f_vect->get_value_on_cell ( this->cll, tag::winding, this->winding );
		Function::Scalar * f_scalar =
			tag::Util::assert_cast < Function::Core*, Function::Scalar* > ( this->f );
		std::vector < double > res
			( 1, f_scalar->get_value_on_cell ( this->cll, tag::winding, this->winding ) );
		return res;                                                                        }
		
	inline std::vector<double> operator= ( const std::vector<double> & x )
	// can be used like in  f ( cll ) = vec
	{	Function::Vector * f_vect = dynamic_cast < Function::Vector* > ( this->f );
		if ( f_vect )  return f_vect->set_value_on_cell ( this->cll, x );
		Function::Scalar * f_scalar =
			tag::Util::assert_cast < Function::Core*, Function::Scalar* > ( this->f );
		assert ( x .size() == 1 );
		std::vector < double > res ( 1, f_scalar->set_value_on_cell ( this->cll, x[0] ) );
		return res;                                                                         }
	//  {	Function::Vector * f_vect =
	//  		tag::Util::assert_cast < Function::Core*, Function::Vector* > ( this->f );
	//  	return f_vect->set_value_on_cell ( this->cll, x );                           }

};  // end of class Function::TakenOnWindingCell

#endif  // ifndef MANIFEM_NO_QUOTIENT

//------------------------------------------------------------------------------------------------------//


class Function::Equality

// the result of an equality comparison between Functions

{	public :

	Function lhs { tag::non_existent }, rhs { tag::non_existent };
	
};

//-----------------------------------------------------------------------------------------//
//  classes below allow for boolean epressions like
//  f < g < h  --  equivalent to  ( f < g ) < h
//  and   f > g > h  --  equivalent to  ( f > g ) > h
//------------------------------------------------------------------------------------------------------//

class Function::Inequality::LessThanZero

// the result of an inequality comparison between a Function and zero

{	public :

	Function expr { tag::non_existent };

	inline operator Function::Inequality::Set() const;
	
};

//------------------------------------------------------------------------------------------------------//

class Function::Inequality::LessThan

// the result of an inequality comparison between Functions

{	public :

	Function low { tag::non_existent }, high { tag::non_existent };
	
	inline operator Function::Inequality::LessThanZero() const;
	inline operator Function::Inequality::Set() const;

};

//------------------------------------------------------------------------------------------------------//

class Function::Inequality::GreaterThan

// the result of an inequality comparison between Functions

{	public :

	Function low { tag::non_existent }, high { tag::non_existent };
	
	inline operator Function::Inequality::LessThanZero() const;
	inline operator Function::Inequality::Set() const;

};

//------------------------------------------------------------------------------------------------------//

class tag::Util::InequalitySet

// a vector of inequalities

{	public :

	std::vector < Function::Inequality::LessThanZero > vec;

	inline bool on_cell ( const Cell & ) const;
	#ifndef MANIFEM_NO_QUOTIENT
	inline bool on_cell
	( const Cell &, const tag::Winding &, const Function::Action & ) const;
	#endif
	
};

//------------------------------------------------------------------------------------------------------//

inline Function::Inequality::LessThan::operator Function::Inequality::LessThanZero() const
{	return Function::Inequality::LessThanZero { this->low - this->high };  }

inline Function::Inequality::LessThan::operator Function::Inequality::Set() const
{	return Function::Inequality::Set
		{ { Function::Inequality::LessThanZero { this->low - this->high } } };  }

inline Function::Inequality::GreaterThan::operator Function::Inequality::LessThanZero() const
{	return Function::Inequality::LessThanZero { this->low - this->high };  }

inline Function::Inequality::GreaterThan::operator Function::Inequality::Set() const
{	return Function::Inequality::Set
		{ { Function::Inequality::LessThanZero { this->low - this->high } } };  }

inline Function::Inequality::LessThanZero::operator Function::Inequality::Set() const
{	return Function::Inequality::Set { { *this } };  }


inline Function::Inequality::Set operator&&
( const Function::Inequality::Set & f, const Function::Inequality::Set & g )

{	Function::Inequality::Set res;
	for ( std::vector < Function::Inequality::LessThanZero > ::const_iterator
	      it = f .vec .begin(); it != f .vec .end(); it++                     )
		res .vec .push_back ( *it );
	for ( std::vector < Function::Inequality::LessThanZero > ::const_iterator
	      it = g .vec .begin(); it != g .vec .end(); it++                     )
		res .vec .push_back ( *it );
	return res;                                                                   }
// optimize !!


inline bool Function::Inequality::Set::on_cell ( const Cell & cll ) const

{	for ( std::vector < Function::Inequality::LessThanZero > ::const_iterator
	      it = this->vec .begin(); it != this->vec .end(); it++               )
		if ( ( it->expr ) ( cll ) > 0. ) return false;
	return true;                                                                 }


#ifndef MANIFEM_NO_QUOTIENT

inline bool Function::Inequality::Set::on_cell
( const Cell & cll, const tag::Winding &, const Function::Action & a ) const

{	for ( std::vector < Function::Inequality::LessThanZero > ::const_iterator
	      it = this->vec .begin(); it != this->vec .end(); it++               )
		if ( ( it->expr ) ( cll, tag::winding, a ) > 0. ) return false;
	return true;                                                                 }

#endif  // ifndef MANIFEM_NO_QUOTIENT


inline Function::Equality operator== ( const Function & f, const Function & g )
{	return Function::Equality { f, g };  }


inline Function::Inequality::LessThan operator<= ( const Function & f, const Function & g )
{	return Function::Inequality::LessThan { f, g };  }

inline Function::Inequality::LessThan operator< ( const Function & f, const Function & g )
{	return Function::Inequality::LessThan { f, g };  }

inline Function::Inequality::GreaterThan operator>= ( const Function & f, const Function & g )
{	return Function::Inequality::GreaterThan { g, f };  }

inline Function::Inequality::GreaterThan operator> ( const Function & f, const Function & g )
{	return Function::Inequality::GreaterThan { g, f };  }

inline Function::Inequality::Set operator<=   //  f <= g <= h
( const Function::Inequality::LessThan & fg, const Function & h )
{	Function::Inequality::Set res;
	res.vec.push_back ( fg );
	res.vec.push_back ( fg .high < h );
	return res;                          }

inline Function::Inequality::Set operator<   //  f < g < h
( const Function::Inequality::LessThan & fg, const Function & h )
{	Function::Inequality::Set res;
	res.vec.push_back ( fg );
	res.vec.push_back ( fg .high < h );
	return res;                          }

inline Function::Inequality::Set operator>=   //  f >= g >= h
( const Function::Inequality::GreaterThan & fg, const Function & h )
{	Function::Inequality::Set res;
	res.vec.push_back ( fg );
	res.vec.push_back ( fg .low > h );
	return res;                         }

inline Function::Inequality::Set operator>   //  f > g > h
( const Function::Inequality::GreaterThan & fg, const Function & h )
{	Function::Inequality::Set res;
	res.vec.push_back ( fg );
	res.vec.push_back ( fg .low > h );
	return res;                         }


inline size_t Function::number_of ( const tag::Components & ) const
{	return this->core->number_of ( tag::components );  }


inline Function Function::operator[] ( size_t i ) const
{	assert ( i < this->number_of ( tag::components ) );
	Function::Scalar * f_scalar = dynamic_cast < Function::Scalar * > ( this->core );
	if ( f_scalar )
	{	assert ( this->number_of ( tag::components ) == 1 );
		assert ( i == 0 );
		return *this;                              }
  Function::Vector * f_vector = dynamic_cast < Function::Vector * > ( this->core );
	assert ( f_vector );
	return Function ( tag::whose_core_is, f_vector->component(i) .core );               }


inline Function::TakenOnCell Function::operator() ( const Cell & cll ) const
{	assert ( this->exists() );
	return Function::TakenOnCell ( this->core, cll .core );   }

#ifndef MANIFEM_NO_QUOTIENT

inline Function::TakenOnWindingCell Function::operator()
( const Cell & cll, const tag::Winding &, const Function::Action & exp ) const
{	assert ( this->exists() );
	return Function::TakenOnWindingCell ( *this, cll, exp );  }

#endif  // ifndef MANIFEM_NO_QUOTIENT


//------------------------------------------------------------------------------------------------------//
//------------------------------------------------------------------------------------------------------//

#ifndef MANIFEM_NO_QUOTIENT

inline void Mesh::export_to_file ( const tag::postscript &, std::string file_name,
                     const tag::Unfold &, const std::vector < Function::Action > & v,
                     const tag::OverRegion &, const tag::Util::InequalitySet & c1,
                                              const tag::Util::InequalitySet & c2    ) const
{	this->export_to_file ( tag::eps, file_name, tag::unfold, v, tag::over_region, c1 && c2 );  }

//------------------------------------------------------------------------------------------------------//
														
inline void Mesh::export_to_file ( const tag::postscript &, std::string file_name,
                     const tag::Unfold &, const std::vector < Function::Action > & v,
                     const tag::OverRegion &, const tag::Util::InequalitySet & c1,
                                              const tag::Util::InequalitySet & c2,
                                              const tag::Util::InequalitySet & c3     ) const
{	this->export_to_file ( tag::eps, file_name, tag::unfold, v, tag::over_region, c1 && c2 && c3 );  }

//------------------------------------------------------------------------------------------------------//
														
inline void Mesh::export_to_file ( const tag::postscript &, std::string file_name,
                  const tag::Unfold &, const std::vector < Function::Action > & v,
                  const tag::OverRegion &, const tag::Util::InequalitySet & c1,
                                           const tag::Util::InequalitySet & c2,
                                           const tag::Util::InequalitySet & c3,
                                           const tag::Util::InequalitySet & c4     ) const
{	this->export_to_file
		( tag::eps, file_name, tag::unfold, v, tag::over_region, c1 && c2 && c3 && c4 );  }

														
inline void Mesh::export_to_file ( const tag::postscript &, std::string file_name,
                                   const tag::Unfold &, const tag::OverRegion &,
                                   const tag::Util::InequalitySet & c1,
                                   const tag::Util::InequalitySet & c2            ) const
{	this->export_to_file ( tag::eps, file_name, tag::unfold, tag::over_region, c1 && c2 );  }


inline void Mesh::export_to_file ( const tag::postscript &, std::string file_name,
                                   const tag::Unfold &, const tag::OverRegion &,
                                   const tag::Util::InequalitySet & c1,
                                   const tag::Util::InequalitySet & c2,
                                   const tag::Util::InequalitySet & c3            ) const
{	this->export_to_file ( tag::eps, file_name, tag::unfold, tag::over_region, c1 && c2 && c3 );  }


inline void Mesh::export_to_file ( const tag::postscript &, std::string file_name,
                                   const tag::Unfold &, const tag::OverRegion &,
                                   const tag::Util::InequalitySet & c1,
                                   const tag::Util::InequalitySet & c2,
                                   const tag::Util::InequalitySet & c3,
                                   const tag::Util::InequalitySet & c4            ) const
{	this->export_to_file ( tag::eps, file_name, tag::unfold, tag::over_region, c1 && c2 && c3 && c4 );  }

//------------------------------------------------------------------------------------------------------//


inline Mesh Mesh::unfold ( const tag::OverRegion &,
                           const tag::Util::InequalitySet & c1,
                           const tag::Util::InequalitySet & c2 ) const
{	return this->unfold ( tag::over_region, c1 && c2 );  }

inline Mesh Mesh::unfold ( const tag::OverRegion &,
                           const tag::Util::InequalitySet & c1,
                           const tag::Util::InequalitySet & c2,
                           const tag::Util::InequalitySet & c3 ) const
{	return this->unfold ( tag::over_region, c1 && c2 && c3 );  }

inline Mesh Mesh::unfold ( const tag::OverRegion &,
                           const tag::Util::InequalitySet & c1,
                           const tag::Util::InequalitySet & c2,
                           const tag::Util::InequalitySet & c3,
                           const tag::Util::InequalitySet & c4 ) const
{	return this->unfold ( tag::over_region, c1 && c2 && c3 && c4 );  }


inline Mesh Mesh::unfold ( const tag::OverRegion &,
                           const tag::Util::InequalitySet & c1,
                           const tag::Util::InequalitySet & c2,
         const tag::ReturnMapBetween &, const tag::CellsOfDim &,
         size_t dim, std::map < Cell, std::pair < Cell, tag::Util::Action > > & mapping ) const
{	return this->unfold ( tag::over_region, c1 && c2,
	                      tag::return_map_between, tag::cells_of_dim, dim, mapping );  }

inline Mesh Mesh::unfold ( const tag::OverRegion &,
                           const tag::Util::InequalitySet & c1,
                           const tag::Util::InequalitySet & c2,
                           const tag::Util::InequalitySet & c3,
         const tag::ReturnMapBetween &, const tag::CellsOfDim &,
         size_t dim, std::map < Cell, std::pair < Cell, tag::Util::Action > > & mapping ) const
{	return this->unfold ( tag::over_region, c1 && c2 && c3,
	                      tag::return_map_between, tag::cells_of_dim, dim, mapping );  }

inline Mesh Mesh::unfold ( const tag::OverRegion &,
                           const tag::Util::InequalitySet & c1,
                           const tag::Util::InequalitySet & c2,
                           const tag::Util::InequalitySet & c3,
                           const tag::Util::InequalitySet & c4,
         const tag::ReturnMapBetween &, const tag::CellsOfDim &,
         size_t dim, std::map < Cell, std::pair < Cell, tag::Util::Action > > & mapping ) const
{	return this->unfold ( tag::over_region, c1 && c2 && c3 && c4,
	                      tag::return_map_between, tag::cells_of_dim, dim, mapping );  }

inline Mesh Mesh::unfold ( const std::vector < tag::Util::Action > & aa, const tag::OverRegion &,
                           const tag::Util::InequalitySet & c1,
                           const tag::Util::InequalitySet & c2           ) const
{	return this->unfold ( aa, tag::over_region, c1 && c2 );  }

inline Mesh Mesh::unfold ( const std::vector < tag::Util::Action > & aa, const tag::OverRegion &,
                           const tag::Util::InequalitySet & c1,
                           const tag::Util::InequalitySet & c2,
                           const tag::Util::InequalitySet & c3           ) const
{	return this->unfold ( aa, tag::over_region, c1 && c2 && c3 );  }

inline Mesh Mesh::unfold ( const std::vector < tag::Util::Action > & aa, const tag::OverRegion &,
                           const tag::Util::InequalitySet & c1,
                           const tag::Util::InequalitySet & c2,
                           const tag::Util::InequalitySet & c3,
                           const tag::Util::InequalitySet & c4           ) const
{	return this->unfold ( aa, tag::over_region, c1 && c2 && c3 && c4 );  }


inline Mesh Mesh::unfold ( const std::vector < tag::Util::Action > & aa, const tag::OverRegion &,
                           const tag::Util::InequalitySet & c1,
                           const tag::Util::InequalitySet & c2,
         const tag::ReturnMapBetween &, const tag::CellsOfDim &,
         size_t dim, std::map < Cell, std::pair < Cell, tag::Util::Action > > & mapping ) const
{	return this->unfold ( aa, tag::over_region, c1 && c2,
	                      tag::return_map_between, tag::cells_of_dim, dim, mapping );  }

inline Mesh Mesh::unfold ( const std::vector < tag::Util::Action > & aa, const tag::OverRegion &,
                           const tag::Util::InequalitySet & c1,
                           const tag::Util::InequalitySet & c2,
                           const tag::Util::InequalitySet & c3,
         const tag::ReturnMapBetween &, const tag::CellsOfDim &,
         size_t dim, std::map < Cell, std::pair < Cell, tag::Util::Action > > & mapping ) const
{	return this->unfold ( aa, tag::over_region, c1 && c2 && c3,
	                      tag::return_map_between, tag::cells_of_dim, dim, mapping );  }

inline Mesh Mesh::unfold ( const std::vector < tag::Util::Action > & aa, const tag::OverRegion &,
                           const tag::Util::InequalitySet & c1,
                           const tag::Util::InequalitySet & c2,
                           const tag::Util::InequalitySet & c3,
                           const tag::Util::InequalitySet & c4,
         const tag::ReturnMapBetween &, const tag::CellsOfDim &,
         size_t dim, std::map < Cell, std::pair < Cell, tag::Util::Action > > & mapping ) const
{	return this->unfold ( aa, tag::over_region, c1 && c2 && c3 && c4,
	                      tag::return_map_between, tag::cells_of_dim, dim, mapping );  }

#endif  // ifndef MANIFEM_NO_QUOTIENT

//------------------------------------------------------------------------------------------------------//
//------------------------------------------------------------------------------------------------------//


#ifndef MANIFEM_NO_FEM

class Function::DelayedDerivative : public Function::Scalar

// a function 'base' differentiated with respect to 'variable'

{	public :

	Function base, variable;  // we use wrappers as pointers

	inline DelayedDerivative ( const Function & b, const Function & v )
	:	base { b }, variable { v }
	{	}

	DelayedDerivative ( const Function::DelayedDerivative & ) = delete;
	DelayedDerivative ( Function::DelayedDerivative && ) = delete;
	
	Function::DelayedDerivative operator= ( const Function::DelayedDerivative & ) = delete;
	Function::DelayedDerivative operator= ( Function::DelayedDerivative && ) = delete;

	// size_t number_of ( const tag::Components & )
	//   virtual from Function::Core, defined by Function::Scalar, returns 1

	// Function component ( size_t i )  virtual from Function::Core,
	//   defined by Function::Scalar, returns self, never actually used

	// void set_value  defined by Function::Scalar, execution forbidden

	double get_value_on_cell ( Cell::Core * ) const;
	#ifndef MANIFEM_NO_QUOTIENT
	double get_value_on_cell
	( Cell::Core *, const tag::Winding &, const Function::Action & exp ) const;
	#endif
	// virtual from Function::Scalar, here execution forbidden

	double set_value_on_cell ( Cell::Core *, const double & );
	//   virtual from Function::Scalar, here execution forbidden

	Function deriv ( Function );  // virtual from Function::Core

	Function replace ( const Function & x, const Function & y );
	//  virtual from Function::Core
	
	// Function::Jump jump  virtual, defined by Function::Core,
	// execution forbidden  ifndef MANIFEM_NO_QUOTIENT

	void analyse_integrand ( VariationalFormulation & vf ) override;
	// virtual from Function::Core  // defined in var-form.cpp
	
	#ifndef NDEBUG  // DEBUG mode	
	std::string repr ( const Function::From & from = Function::from_void ) const;
	// virtual from Function::Core
	#endif

};  // end of  class Function::DelayedDerivative

//------------------------------------------------------------------------------------------------------//


#endif  // ifndef MANIFEM_NO_FEM


}  // namespace maniFEM


#endif  // ifndef MANIFEM_FUNCTION_H
