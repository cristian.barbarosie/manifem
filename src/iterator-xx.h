
//   iterator-xx.h  2025.01.18

//   This file is part of maniFEM, a C++ library for meshes and finite elements on manifolds.

//   Copyright  2019 - 2025  Cristian Barbarosie  cristian.barbarosie@gmail.com

//   https://maniFEM.rd.ciencias.ulisboa.pt/
//   https://codeberg.org/cristian.barbarosie/maniFEM

//   ManiFEM is free software: you can redistribute it and/or modify it
//   under the terms of the GNU Lesser General Public License as published
//   by the Free Software Foundation, either version 3 of the License
//   or (at your option) any later version.

//   ManiFEM is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//   See the GNU Lesser General Public License for more details.

//   You should have received a copy of the GNU Lesser General Public License
//   along with maniFEM.  If not, see <https://www.gnu.org/licenses/>.


#ifndef MANIFEM_ITERATOR_XX_H
#define MANIFEM_ITERATOR_XX_H


namespace maniFEM {

class Mesh::Iterator::Over::TwoVerticesOfSeg : public Mesh::Iterator::Core

// abstract class, specialized in eight derived classes, see below

{	public :

	Cell::PositiveSegment * seg_p;  // an iterator does not keep the segment alive
	short unsigned int passage;

	inline TwoVerticesOfSeg ( Cell::PositiveSegment * seg )
	:	Mesh::Iterator::Core (), seg_p { seg }
	{	}
	
	inline TwoVerticesOfSeg ( const Mesh::Iterator::Over::TwoVerticesOfSeg & it )
	:	Mesh::Iterator::Core (), seg_p { it .seg_p }, passage { it .passage }
	{	}

	void reset ( );  // virtual from Mesh::Iterator::Core

	void reset ( const tag::StartAt &, Cell::Core * cll );
	// virtual from Mesh::Iterator::Core, here execution forbidden

	void advance ( );  // virtual from Mesh::Iterator::Core
	bool in_range ( );  // virtual from Mesh::Iterator::Core
	// Cell deref ( )  stays pure virtual from Mesh::Iterator::Core

	// Mesh::Iterator::Core * clone ( )  stays pure virtual from Mesh::Iterator::Core
	
	struct NormalOrder  {  class AsFound;  class ForcePositive;  class ReverseEachCell;  };
	struct ReverseOrder  {  class AsFound;  class ForcePositive;  class ReverseEachCell;  };
	
};  // end of class Mesh::Iterator::Over::TwoVerticesOfSeg

//------------------------------------------------------------------------------------------------------//


class Mesh::Iterator::Over::TwoVerticesOfSeg::NormalOrder::AsFound
: public Mesh::Iterator::Over::TwoVerticesOfSeg
// iterate over the two vertices, first base (negative) then tip (positive)

{	public :

	// inherited from Mesh::Iterator::Over::TwoVerticesOfSeg :
	// Cell::PositiveSegment * seg_p
	// short unsigned int passage

	inline AsFound ( Cell::PositiveSegment * seg )
	:	Mesh::Iterator::Over::TwoVerticesOfSeg ( seg )
	{	}
	
	inline AsFound ( const Mesh::Iterator::Over::TwoVerticesOfSeg::NormalOrder::AsFound & it )
	:	Mesh::Iterator::Over::TwoVerticesOfSeg ( it )  // seg_p { it .seg_p }, passage { it .passage }
	{	}
	
	// void reset ( )  virtual, defined by Mesh::Iterator::Over::TwoVerticesOfSeg

	// void reset ( const tag::StartAt &, Cell::Core * cll )
	//   virtual, defined by Mesh::Iterator::Over::TwoVerticesOfSeg, execution forbidden

	// void advance ( )  virtual, defined by Mesh::Iterator::Over::TwoVerticesOfSeg
	// bool in_range ( )  virtual, defined by Mesh::Iterator::Over::TwoVerticesOfSeg
	Cell deref ( );  // virtual from Mesh::Iterator::Core

	Mesh::Iterator::Core * clone ( );  // virtual from Mesh::Iterator::Core

};  // end of class Mesh::Iterator::Over::TwoVerticesOfSeg::NormalOrder::AsFound

//------------------------------------------------------------------------------------------------------//


class Mesh::Iterator::Over::TwoVerticesOfSeg::NormalOrder::ForcePositive
: public Mesh::Iterator::Over::TwoVerticesOfSeg
// iterate over the two vertices, first base then tip (both positive)

{	public :

	// inherited from Mesh::Iterator::Over::TwoVerticesOfSeg :
	// Cell::PositiveSegment * seg_p
	// short unsigned int passage

	inline ForcePositive ( Cell::PositiveSegment * seg )
	:	Mesh::Iterator::Over::TwoVerticesOfSeg ( seg )
	{	}
	
	inline ForcePositive
	( const Mesh::Iterator::Over::TwoVerticesOfSeg::NormalOrder::ForcePositive & it )
	:	Mesh::Iterator::Over::TwoVerticesOfSeg ( it )  //  seg_p { it .seg_p }, passage { it .passage }
	{ }
	
	// void reset ( const tag::StartAt &, Cell::Core * cll )
	//   virtual, defined by Mesh::Iterator::Over::TwoVerticesOfSeg, execution forbidden

	// void advance ( )  virtual, defined by Mesh::Iterator::Over::TwoVerticesOfSeg
	// bool in_range ( )  virtual, defined by Mesh::Iterator::Over::TwoVerticesOfSeg
	Cell deref ( );  // virtual from Mesh::Iterator::Core

	Mesh::Iterator::Core * clone ( );  // virtual from Mesh::Iterator::Core

};  // end of class Mesh::Iterator::Over::TwoVerticesOfSeg::NormalOrder::ForcePositive

//------------------------------------------------------------------------------------------------------//


class Mesh::Iterator::Over::TwoVerticesOfSeg::ReverseOrder::AsFound
: public Mesh::Iterator::Over::TwoVerticesOfSeg
// iterate over the two vertices, first tip (positive) then base (negative)

{	public :

	// inherited from Mesh::Iterator::Over::TwoVerticesOfSeg :
	// Cell::PositiveSegment * seg_p
	// short unsigned int passage
	
	inline AsFound ( Cell::PositiveSegment * seg )
	:	Mesh::Iterator::Over::TwoVerticesOfSeg ( seg )
	{	}
	
	inline AsFound ( const Mesh::Iterator::Over::TwoVerticesOfSeg::ReverseOrder::AsFound & it )
	:	Mesh::Iterator::Over::TwoVerticesOfSeg ( it )  // seg_p { it .seg_p }, passage { it .passage }
	{	}
	
	// void reset ( )  virtual, defined by Mesh::Iterator::Over::TwoVerticesOfSeg

	// void reset ( const tag::StartAt &, Cell::Core * cll )
	//   virtual, defined by Mesh::Iterator::Over::TwoVerticesOfSeg, execution forbidden

	// void advance ( )  virtual, defined by Mesh::Iterator::Over::TwoVerticesOfSeg
	// bool in_range ( )  virtual, defined by Mesh::Iterator::Over::TwoVerticesOfSeg
	Cell deref ( );  // virtual from Mesh::Iterator::Core

	Mesh::Iterator::Core * clone ( );  // virtual from Mesh::Iterator::Core

};  // end of class Mesh::Iterator::Over::TwoVerticesOfSeg::ReverseOrder::AsFound

//------------------------------------------------------------------------------------------------------//


class Mesh::Iterator::Over::TwoVerticesOfSeg::ReverseOrder::ForcePositive
: public Mesh::Iterator::Over::TwoVerticesOfSeg
// iterate over the two vertices, first tip then base (both positive)

{	public :

	// inherited from Mesh::Iterator::Over::TwoVerticesOfSeg :
	// Cell::PositiveSegment * seg_p
	// short unsigned int passage

	inline ForcePositive ( Cell::PositiveSegment * seg )
	:	Mesh::Iterator::Over::TwoVerticesOfSeg ( seg )
	{	};
	
	inline ForcePositive
	( const Mesh::Iterator::Over::TwoVerticesOfSeg::ReverseOrder::ForcePositive & it )
	:	Mesh::Iterator::Over::TwoVerticesOfSeg ( it )  // seg_p { it .seg_p }, passage { it .passage }
	{	}
	
	// void reset ( )  virtual, defined by Mesh::Iterator::Over::TwoVerticesOfSeg

	// void reset ( const tag::StartAt &, Cell::Core * cll )
	//   virtual, defined by Mesh::Iterator::Over::TwoVerticesOfSeg, execution forbidden

	// void advance ( )  virtual, defined by Mesh::Iterator::Over::TwoVerticesOfSeg
	// bool in_range ( )  virtual, defined by Mesh::Iterator::Over::TwoVerticesOfSeg
	Cell deref ( );  // virtual from Mesh::Iterator::Core

	Mesh::Iterator::Core * clone ( );  // virtual from Mesh::Iterator::Core

};  // end of class Mesh::Iterator::Over::TwoVerticesOfSeg::ReverseOrder::ForcePositive

//------------------------------------------------------------------------------------------------------//


class Mesh::Iterator::Over::TwoVerticesOfSeg::NormalOrder::ReverseEachCell
: public Mesh::Iterator::Over::TwoVerticesOfSeg
// iterate over the two vertices, first base (positive) then tip (negative)

{	public :

	// inherited from Mesh::Iterator::Over::TwoVerticesOfSeg :
	// Cell::PositiveSegment * seg_p
	// short unsigned int passage

	inline ReverseEachCell ( Cell::PositiveSegment * seg )
	:	Mesh::Iterator::Over::TwoVerticesOfSeg ( seg )
	{	}
	
	inline ReverseEachCell ( const Mesh::Iterator::Over::TwoVerticesOfSeg
	                              ::NormalOrder::ReverseEachCell & it     )
	:	Mesh::Iterator::Over::TwoVerticesOfSeg ( it )  // seg_p { it .seg_p }, passage { it .passage }
	{	}
	
	// void reset ( )  virtual, defined by Mesh::Iterator::Over::TwoVerticesOfSeg

	// void reset ( const tag::StartAt &, Cell::Core * cll )
	//   virtual, defined by Mesh::Iterator::Over::TwoVerticesOfSeg, execution forbidden

	// void advance ( )  virtual, defined by Mesh::Iterator::Over::TwoVerticesOfSeg
	// bool in_range ( )  virtual, defined by Mesh::Iterator::Over::TwoVerticesOfSeg
	Cell deref ( );  // virtual from Mesh::Iterator::Core

	Mesh::Iterator::Core * clone ( );  // virtual from Mesh::Iterator::Core

};  // end of class Mesh::Iterator::Over::TwoVerticesOfSeg::NormalOrder::ReverseEachCell

//------------------------------------------------------------------------------------------------------//


class Mesh::Iterator::Over::TwoVerticesOfSeg::ReverseOrder::ReverseEachCell
: public Mesh::Iterator::Over::TwoVerticesOfSeg
// iterate over the two vertices, first tip (negative) then base (positive)

{	public :

	// inherited from Mesh::Iterator::Over::TwoVerticesOfSeg :
	// Cell::PositiveSegment * seg_p
	// short unsigned int passage

	inline ReverseEachCell ( Cell::PositiveSegment * seg )
	:	Mesh::Iterator::Over::TwoVerticesOfSeg ( seg )
	{	}
	
	inline ReverseEachCell ( const Mesh::Iterator::Over::TwoVerticesOfSeg
	                                   ::ReverseOrder::ReverseEachCell & it )
	:	Mesh::Iterator::Over::TwoVerticesOfSeg ( it )  // seg_p { it .seg_p }, passage { it .passage }
	{	}
	
	// void reset ( )  virtual, defined by Mesh::Iterator::Over::TwoVerticesOfSeg

	// void reset ( const tag::StartAt &, Cell::Core * cll )
	//   virtual, defined by Mesh::Iterator::Over::TwoVerticesOfSeg, execution forbidden

	// void advance ( )  virtual, defined by Mesh::Iterator::Over::TwoVerticesOfSeg
	// bool in_range ( )  virtual, defined by Mesh::Iterator::Over::TwoVerticesOfSeg
	Cell deref ( );  // virtual from Mesh::Iterator::Core

	Mesh::Iterator::Core * clone ( );  // virtual from Mesh::Iterator::Core

};  // end of class Mesh::Iterator::Over::TwoVerticesOfSeg::ReverseOrder::ReverseEachCell

//------------------------------------------------------------------------------------------------------//
//------------------------------------------------------------------------------------------------------//


class Mesh::Iterator::OverCells::OfConnectedOneDimMesh : public Mesh::Iterator::Core

// abstract class

{	public :

	Mesh::Connected::OneDim * msh;  // an iterator does not keep the mesh alive
	Cell::Core * last_vertex;
	// positive vertex for iterator over vertices
	// positive vertex for iterator over segments, forward
	// negative vertex for iterator over segments, backward

	inline OfConnectedOneDimMesh ( Mesh::Connected::OneDim * m )
	:	Mesh::Iterator::Core (), msh { m }
	{	}
	
	inline OfConnectedOneDimMesh ( const Mesh::Iterator::OverCells::OfConnectedOneDimMesh & it )
	:	Mesh::Iterator::Core (), msh { it .msh }, last_vertex { it .last_vertex }
	{	}
	
	// void reset ( )  stays pure virtual from Mesh::Iterator::Core

	// void reset ( tag::StartAt , Cell::Core * cll )  stays pure virtual from Mesh::Iterator::Core

	// void advance ( )  stays pure virtual from Mesh::Iterator::Core
	// bool in_range ( )  stays pure virtual from Mesh::Iterator::Core
	// Cell deref ( )  stays pure virtual from Mesh::Iterator::Core

	// Mesh::Iterator::Core * clone ( )  stays pure virtual from Mesh::Iterator::Core

};  // end of class Mesh::Iterator::OverCells::OfConnectedOneDimMesh

//------------------------------------------------------------------------------------------------------//
//------------------------------------------------------------------------------------------------------//
	

class Mesh::Iterator::OverVertices::OfConnectedOneDimMesh
:	public Mesh::Iterator::OverCells::OfConnectedOneDimMesh

// abstract class, specialized in two derived classes, see below

{	public :

	// attributes inherited from Mesh::Iterator::OverCells::OfConnectedOneDimMesh :
	// Mesh::Connected::OneDim * msh
	// Cell::Core * last_vertex (here positive vertex)

	Cell::Positive::Vertex * current_vertex;

	inline OfConnectedOneDimMesh ( Mesh::Connected::OneDim * m )
	:	Mesh::Iterator::OverCells::OfConnectedOneDimMesh ( m )
	{	}
	
	inline OfConnectedOneDimMesh
	( const Mesh::Iterator::OverVertices::OfConnectedOneDimMesh & it )
	:	Mesh::Iterator::OverCells::OfConnectedOneDimMesh ( it ), current_vertex { it .current_vertex }
		// msh { it .msh }, last_vertex { it .last_vertex }
	{	}
	
	// void reset ( )  stays pure virtual from Mesh::Iterator::Core

	// void reset ( tag::StartAt, Cell::Core * cll )  stays pure virtual from Mesh::Iterator::Core

	// void advance ( )  stays pure virtual from Mesh::Iterator::Core
	bool in_range ( );  // virtual from Mesh::Iterator::Core
	Cell deref ( );  // virtual from Mesh::Iterator::Core

	// Mesh::Iterator::Core * clone ( )  stays pure virtual from Mesh::Iterator::Core

	class NormalOrder;  class ReverseOrder;
	
};  // end of class Mesh::Iterator::OverVertices::OfConnectedOneDimMesh

//------------------------------------------------------------------------------------------------------//


class Mesh::Iterator::OverVertices::OfConnectedOneDimMesh::NormalOrder
:	public Mesh::Iterator::OverVertices::OfConnectedOneDimMesh

{	public :

	// attributes inherited from Mesh::Iterator::OverCells::OfConnectedOneDimMesh :
	// Mesh::Connected::OneDim * msh
	// Cell::Core * last_vertex (here positive vertex)

	// attribute inherited from Mesh::Iterator::OverVertices::OfConnectedOneDimMesh :
	// Cell::Positive::Vertex * current_vertex
	
	inline NormalOrder ( Mesh::Connected::OneDim * m )
	:	Mesh::Iterator::OverVertices::OfConnectedOneDimMesh ( m )
	{	}
	
	inline NormalOrder ( const Mesh::Iterator::OverVertices::OfConnectedOneDimMesh::NormalOrder & it )
	:	Mesh::Iterator::OverVertices::OfConnectedOneDimMesh ( it )
		// msh { it .msh }, last_vertex { it .last_vertex }, current_vertex { it .current_vertex }
	{	}
	
	void reset ( );  // virtual from Mesh::Iterator::Core

	void reset ( const tag::StartAt &, Cell::Core * cll );  // virtual from Mesh::Iterator::Core

	void advance ( );  // virtual from Mesh::Iterator::Core
	// bool in_range ( )  virtual, defined by Mesh::Iterator::OverVertices::OfConnectedOneDimMesh
	// Cell deref ( )  defined by Mesh::Iterator::OverVertices::OfConnectedOneDimMesh

	Mesh::Iterator::Core * clone ( );  // virtual from Mesh::Iterator::Core

};  // end of class Mesh::Iterator::OverVertices::OfConnectedOneDimMesh::NormalOrder

//------------------------------------------------------------------------------------------------------//


class Mesh::Iterator::OverVertices::OfConnectedOneDimMesh::ReverseOrder
:	public Mesh::Iterator::OverVertices::OfConnectedOneDimMesh

{	public :

	// attributes inherited from Mesh::Iterator::OverCells::OfConnectedOneDimMesh :
	// Mesh::Connected::OneDim * msh
	// Cell::Core * last_vertex (here positive vertex)

	// attribute inherited from Mesh::Iterator::OverVertices::OfConnectedOneDimMesh :
	// Cell::Positive::Vertex * current_vertex
	
	inline ReverseOrder ( Mesh::Connected::OneDim * m )
	:	Mesh::Iterator::OverVertices::OfConnectedOneDimMesh ( m )
	{	}
	
	inline ReverseOrder ( const Mesh::Iterator::OverVertices::OfConnectedOneDimMesh::ReverseOrder & it )
	:	Mesh::Iterator::OverVertices::OfConnectedOneDimMesh ( it )
		// msh { it .msh }, last_vertex { it .last_vertex }, current_vertex { it .current_vertex }
	{	}
	
	void reset ( );  // virtual from Mesh::Iterator::Core

	void reset ( const tag::StartAt &, Cell::Core * cll );  // virtual from Mesh::Iterator::Core

	void advance ( );  // virtual from Mesh::Iterator::Core
	// bool in_range ( )  virtual, defined by Mesh::Iterator::OverVertices::OfConnectedOneDimMesh
	// Cell deref ( )  defined by Mesh::Iterator::OverVertices::OfConnectedOneDimMesh

	Mesh::Iterator::Core * clone ( );  // virtual from Mesh::Iterator::Core

};  // end of class Mesh::Iterator::OverVertices::OfConnectedOneDimMesh::ReverseOrder

//------------------------------------------------------------------------------------------------------//
//------------------------------------------------------------------------------------------------------//


class Mesh::Iterator::OverSegments::OfConnectedOneDimMesh
:	public Mesh::Iterator::OverCells::OfConnectedOneDimMesh

// abstract class

{	public :

	// attributes inherited from Mesh::Iterator::OverCells::OfConnectedOneDimMesh :
	// Mesh::Connected::OneDim * msh
	// Cell::Core * last_vertex (here positive vertex)

	Cell::Core * current_segment;
	
	inline OfConnectedOneDimMesh ( Mesh::Connected::OneDim * m )
	:	Mesh::Iterator::OverCells::OfConnectedOneDimMesh ( m )
	{	}
	
	inline OfConnectedOneDimMesh
	( const Mesh::Iterator::OverSegments::OfConnectedOneDimMesh & it )
	:	Mesh::Iterator::OverCells::OfConnectedOneDimMesh ( it ), current_segment { it .current_segment }
		// msh { it .msh }, last_vertex { it .last_vertex }
	{	}
	
	// void reset ( )  stays pure virtual from Mesh::Iterator::Core

	// void reset ( tag::StartAt, Cell::Core * cll )  stays pure virtual from Mesh::Iterator::Core

	// void advance ( )  stays pure virtual from Mesh::Iterator::Core
	bool in_range ( );  // virtual from Mesh::Iterator::Core
	// Cell deref ( )  stays pure virtual from Mesh::Iterator::Core

	// Mesh::Iterator::Core * clone ( )  stays pure virtual from Mesh::Iterator::Core

	class NormalOrder;  class ReverseOrder;
	
};  // end of class Mesh::Iterator::OverSegments::OfConnectedOneDimMesh

//------------------------------------------------------------------------------------------------------//


class Mesh::Iterator::OverSegments::OfConnectedOneDimMesh::NormalOrder
:	public Mesh::Iterator::OverSegments::OfConnectedOneDimMesh

// abstract class, specialized in four derived classes, see below

{	public :

	// attributes inherited from Mesh::Iterator::OverCells::OfConnectedOneDimMesh :
	// Mesh::Connected::OneDim * msh
	// Cell::Core * last_vertex (here positive vertex)

	// attribute inherited from Mesh::Iterator::OverSegments::OfConnectedOneDimMesh :
	// Cell::Core * current_segment

	inline NormalOrder ( Mesh::Connected::OneDim * m )
	:	Mesh::Iterator::OverSegments::OfConnectedOneDimMesh ( m )
	{	}
	
	inline NormalOrder ( const Mesh::Iterator::OverSegments::OfConnectedOneDimMesh::NormalOrder & it )
	:	Mesh::Iterator::OverSegments::OfConnectedOneDimMesh ( it )
		// msh { it .msh }, last_vertex { it .last_vertex }, current_segment { it .current_segment }
	{	}
	
	void reset ( );  // virtual from Mesh::Iterator::Core

	void reset ( const tag::StartAt &, Cell::Core * cll );  // virtual from Mesh::Iterator::Core

	void advance ( );  // virtual from Mesh::Iterator::Core
	// bool in_range ( )  virtual, defined by Mesh::Iterator::OverVertices::OfConnectedOneDimMesh
	// Cell deref ( )  stays pure virtual from Mesh::Iterator::Core

	// Mesh::Iterator::Core * clone ( )  stays pure virtual from Mesh::Iterator::Core

	class AsFound;  class ForcePositive;
	struct ReverseEachCell;

};  // end of class Mesh::Iterator::OverSegments::OfConnectedOneDimMesh::NormalOrder

//------------------------------------------------------------------------------------------------------//


class Mesh::Iterator::OverSegments::OfConnectedOneDimMesh::NormalOrder::AsFound
:	public Mesh::Iterator::OverSegments::OfConnectedOneDimMesh::NormalOrder

{	public :

	// attributes inherited from Mesh::Iterator::OverCells::OfConnectedOneDimMesh :
	// Mesh::Connected::OneDim * msh
	// Cell::Core * last_vertex (here positive vertex)

	// attribute inherited from Mesh::Iterator::OverSegments::OfConnectedOneDimMesh :
	// Cell::Core * current_segment

	inline AsFound ( Mesh::Connected::OneDim * m )
	:	Mesh::Iterator::OverSegments::OfConnectedOneDimMesh::NormalOrder ( m )
	{	}
	
	inline AsFound
	( const Mesh::Iterator::OverSegments::OfConnectedOneDimMesh::NormalOrder::AsFound & it )
	:	Mesh::Iterator::OverSegments::OfConnectedOneDimMesh::NormalOrder ( it )
		// msh { it .msh }, last_vertex { it .last_vertex }, current_segment { it .current_segment }
	{	}
	
	// void reset ( )  defined by Mesh::Iterator::OverSegments::OfConnectedOneDimMesh::NormalOrder

	// void reset ( const tag::StartAt &, Cell::Core * cll )
	//   virtual, defined by Mesh::Iterator::OverSegments::OfConnectedOneDimMesh::NormalOrder

	// void advance ( )  defined by Mesh::Iterator::OverSegments::OfConnectedOneDimMesh::NormalOrder
	// bool in_range ( )  defined by Mesh::Iterator::OverSegments::OfConnectedOneDimMesh
	Cell deref ( );  // virtual from Mesh::Iterator::Core

	Mesh::Iterator::Core * clone ( );  // virtual from Mesh::Iterator::Core

};  // end of class Mesh::Iterator::OverSegments::OfConnectedOneDimMesh::NormalOrder::AsFound

//------------------------------------------------------------------------------------------------------//


class Mesh::Iterator::OverSegments::OfConnectedOneDimMesh::NormalOrder::ForcePositive
:	public Mesh::Iterator::OverSegments::OfConnectedOneDimMesh::NormalOrder

{	public :

	// attributes inherited from Mesh::Iterator::OverCells::OfConnectedOneDimMesh :
	// Mesh::Connected::OneDim * msh
	// Cell::Core * last_vertex (here positive vertex)

	// attribute inherited from Mesh::Iterator::OverSegments::OfConnectedOneDimMesh :
	// Cell::Core * current_segment

	inline ForcePositive ( Mesh::Connected::OneDim * m )
	:	Mesh::Iterator::OverSegments::OfConnectedOneDimMesh::NormalOrder ( m )
	{	}
	
	inline ForcePositive
	( const Mesh::Iterator::OverSegments::OfConnectedOneDimMesh::NormalOrder::ForcePositive & it )
	:	Mesh::Iterator::OverSegments::OfConnectedOneDimMesh::NormalOrder ( it )
		// msh { it .msh }, last_vertex { it .last_vertex }, current_segment { it .current_segment }
	{	}
	
	// void reset ( )  defined by Mesh::Iterator::OverSegments::OfConnectedOneDimMesh::NormalOrder

	// void reset ( const tag::StartAt &, Cell::Core * cll )
	//   virtual, defined by Mesh::Iterator::OverSegments::OfConnectedOneDimMesh::NormalOrder

	// void advance ( )  defined by Mesh::Iterator::OverSegments::OfConnectedOneDimMesh::NormalOrder
	// bool in_range ( )  defined by Mesh::Iterator::OverSegments::OfConnectedOneDimMesh
	Cell deref ( );  // virtual from Mesh::Iterator::Core

	Mesh::Iterator::Core * clone ( );  // virtual from Mesh::Iterator::Core

};  // end of class Mesh::Iterator::OverSegments::OfConnectedOneDimMesh::NormalOrder::ForcePositive

//------------------------------------------------------------------------------------------------------//


class Mesh::Iterator::OverSegments::OfConnectedOneDimMesh::NormalOrder::ReverseEachCell
: public Mesh::Iterator::OverSegments::OfConnectedOneDimMesh::NormalOrder

{	public :

	// attribute Mesh::Connected::OneDim * msh
	// attribute Cell::Core * last_vertex (here positive vertex)
	// inherited from Mesh::Iterator::OverCells::OfConnectedOneDimMesh

	// attribute Cell::Core * current_segment
	// inherited from Mesh::Iterator::OverSegments::OfConnectedOneDimMesh

	inline ReverseEachCell ( Mesh::Connected::OneDim * m )
	:	Mesh::Iterator::OverSegments::OfConnectedOneDimMesh::NormalOrder ( m )
	{	}
	
	inline ReverseEachCell ( const Mesh::Iterator::OverSegments::OfConnectedOneDimMesh
	                                   ::NormalOrder::ReverseEachCell & it             )
	:	Mesh::Iterator::OverSegments::OfConnectedOneDimMesh::NormalOrder ( it )
		// msh { it .msh }, last_vertex { it .last_vertex }, current_segment { it .current_segment }
	{	}

	// void reset ( )  defined by Mesh::Iterator::OverSegments::OfConnectedOneDimMesh::NormalOrder

	// void reset ( const tag::StartAt &, Cell::Core * cll )
	//   virtual, defined by Mesh::Iterator::OverSegments::OfConnectedOneDimMesh::NormalOrder

	// void advance ( )  defined by Mesh::Iterator::OverSegments::OfConnectedOneDimMesh::NormalOrder
	// bool in_range ( )  defined by Mesh::Iterator::OverSegments::OfConnectedOneDimMesh
	Cell deref ( );  // virtual from Mesh::Iterator::Core

	Mesh::Iterator::Core * clone ( );  // virtual from Mesh::Iterator::Core

};  // end of class Mesh::Iterator::OverSegments::OfConnectedOneDimMesh::NormalOrder::ReverseEachCell

//------------------------------------------------------------------------------------------------------//


class Mesh::Iterator::OverSegments::OfConnectedOneDimMesh::ReverseOrder
:	public Mesh::Iterator::OverSegments::OfConnectedOneDimMesh

// abstract class, specialized in four derived classes, see below

{	public :

	// attributes inherited from Mesh::Iterator::OverCells::OfConnectedOneDimMesh :
	// Mesh::Connected::OneDim * msh
	// Cell::Core * last_vertex (here negative vertex)

	// attribute inherited from Mesh::Iterator::OverSegments::OfConnectedOneDimMesh :
	// Cell::Core * current_segment

	inline ReverseOrder ( Mesh::Connected::OneDim * m )
	:	Mesh::Iterator::OverSegments::OfConnectedOneDimMesh ( m )
	{	}
	
	inline ReverseOrder ( const Mesh::Iterator::OverSegments::OfConnectedOneDimMesh::ReverseOrder & it )
	:	Mesh::Iterator::OverSegments::OfConnectedOneDimMesh ( it )
		// msh { it .msh }, last_vertex { it .last_vertex }, current_segment { it .current_segment }
	{	}
	
	void reset ( );  // virtual from Mesh::Iterator::Core

	void reset ( const tag::StartAt &, Cell::Core * cll );  // virtual from Mesh::Iterator::Core

	void advance ( );  // virtual from Mesh::Iterator::Core
	// bool in_range ( )  virtual, defined by Mesh::Iterator::OverVertices::OfConnectedOneDimMesh
	// Cell deref ( )  stays pure virtual from Mesh::Iterator::Core

	// Mesh::Iterator::Core * clone ( )  stays pure virtual from Mesh::Iterator::Core

	class AsFound;  class ForcePositive;
	struct ReverseEachCell;

};  // end of class Mesh::Iterator::OverSegments::OfConnectedOneDimMesh::ReverseOrder

//------------------------------------------------------------------------------------------------------//


class Mesh::Iterator::OverSegments::OfConnectedOneDimMesh::ReverseOrder::AsFound
:	public Mesh::Iterator::OverSegments::OfConnectedOneDimMesh::ReverseOrder

{	public :

	// attributes inherited from Mesh::Iterator::OverCells::OfConnectedOneDimMesh :
	// Mesh::Connected::OneDim * msh
	// Cell::Core * last_vertex (here negative vertex)

	// attribute inherited from Mesh::Iterator::OverSegments::OfConnectedOneDimMesh :
	// Cell::Core * current_segment
	
	inline AsFound ( Mesh::Connected::OneDim * m )
	:	Mesh::Iterator::OverSegments::OfConnectedOneDimMesh::ReverseOrder ( m )
	{	}
	
	inline AsFound
	( const Mesh::Iterator::OverSegments::OfConnectedOneDimMesh::ReverseOrder::AsFound & it )
	:	Mesh::Iterator::OverSegments::OfConnectedOneDimMesh::ReverseOrder ( it )
		// msh { it .msh }, last_vertex { it .last_vertex }, current_segment { it .current_segment }
	{	}
	
	// void reset ( )  defined by Mesh::Iterator::OverSegments::OfConnectedOneDimMesh::ReverseOrder

	// void reset ( const tag::StartAt &, Cell::Core * cll )
	//   virtual, defined by Mesh::Iterator::OverSegments::OfConnectedOneDimMesh::ReverseOrder

	// void advance ( )  defined by Mesh::Iterator::OverSegments::OfConnectedOneDimMesh::ReverseOrder
	// bool in_range ( )  virtual, defined by Mesh::Iterator::OverSegments::OfConnectedOneDimMesh
	Cell deref ( );  // virtual from Mesh::Iterator::Core

	Mesh::Iterator::Core * clone ( );  // virtual from Mesh::Iterator::Core

};  // end of class Mesh::Iterator::OverSegments::OfConnectedOneDimMesh::ReverseOrder::AsFound

//------------------------------------------------------------------------------------------------------//


class Mesh::Iterator::OverSegments::OfConnectedOneDimMesh::ReverseOrder::ForcePositive
:	public Mesh::Iterator::OverSegments::OfConnectedOneDimMesh::ReverseOrder

{	public :

	// attributes inherited from Mesh::Iterator::OverCells::OfConnectedOneDimMesh :
	// Mesh::Connected::OneDim * msh
	// Cell::Core * last_vertex (here negative vertex)

	// attribute inherited from Mesh::Iterator::OverSegments::OfConnectedOneDimMesh :
	// Cell::Core * current_segment

	inline ForcePositive ( Mesh::Connected::OneDim * m )
	:	Mesh::Iterator::OverSegments::OfConnectedOneDimMesh::ReverseOrder ( m )
	{	}
	
	inline ForcePositive
	( const Mesh::Iterator::OverSegments::OfConnectedOneDimMesh::ReverseOrder::ForcePositive & it )
	:	Mesh::Iterator::OverSegments::OfConnectedOneDimMesh::ReverseOrder ( it )
		// msh { it .msh }, last_vertex { it .last_vertex }, current_segment { it .current_segment }
	{	}

	// void reset ( )  defined by Mesh::Iterator::OverSegments::OfConnectedOneDimMesh::ReverseOrder

	// void reset ( const tag::StartAt &, Cell::Core * cll )
	//   virtual, defined by Mesh::Iterator::OverSegments::OfConnectedOneDimMesh::ReverseOrder

	// void advance ( )  defined by Mesh::Iterator::OverSegments::OfConnectedOneDimMesh::ReverseOrder
	// bool in_range ( )  virtual, defined by Mesh::Iterator::OverSegments::OfConnectedOneDimMesh
	Cell deref ( );  // virtual from Mesh::Iterator::Core

	Mesh::Iterator::Core * clone ( );  // virtual from Mesh::Iterator::Core

};  // end of class Mesh::Iterator::OverSegments::OfConnectedOneDimMesh::ReverseOrder::ForcePositive

//------------------------------------------------------------------------------------------------------//


class Mesh::Iterator::OverSegments::OfConnectedOneDimMesh::ReverseOrder::ReverseEachCell
:	public Mesh::Iterator::OverSegments::OfConnectedOneDimMesh::ReverseOrder

{	public :

	// attributes inherited from Mesh::Iterator::OverCells::OfConnectedOneDimMesh :
	// Mesh::Connected::OneDim * msh
	// Cell::Core * last_vertex (here negative vertex)

	// attribute inherited from Mesh::Iterator::OverSegments::OfConnectedOneDimMesh :
	// Cell::Core * current_segment

	inline ReverseEachCell ( Mesh::Connected::OneDim * m )
	:	Mesh::Iterator::OverSegments::OfConnectedOneDimMesh::ReverseOrder ( m )
	{	}
	
	inline ReverseEachCell ( const Mesh::Iterator::OverSegments::OfConnectedOneDimMesh
	                                   ::ReverseOrder::ReverseEachCell & it            )
	:	Mesh::Iterator::OverSegments::OfConnectedOneDimMesh::ReverseOrder ( it )
		// msh { it .msh }, last_vertex { it .last_vertex }, current_segment { it .current_segment }
	{	}

	// void reset ( )  defined by Mesh::Iterator::OverSegments::OfConnectedOneDimMesh::ReverseOrder

	// void reset ( const tag::StartAt &, Cell::Core * cll )
	//   virtual, defined by Mesh::Iterator::OverSegments::OfConnectedOneDimMesh::ReverseOrder

	// void advance ( )  defined by Mesh::Iterator::OverSegments::OfConnectedOneDimMesh::ReverseOrder
	// bool in_range ( )  virtual, defined by Mesh::Iterator::OverSegments::OfConnectedOneDimMesh
	Cell deref ( );  // virtual from Mesh::Iterator::Core

	Mesh::Iterator::Core * clone ( );  // virtual from Mesh::Iterator::Core

};  // end of class Mesh::Iterator::OverSegments::OfConnectedOneDimMesh::ReverseOrder::ReverseEachCell

//------------------------------------------------------------------------------------------------------//
//------------------------------------------------------------------------------------------------------//


class Mesh::Iterator::OverCells::OfFuzzyMesh : public Mesh::Iterator::Core

// abstract class, specialized in three derived classes, see below

{	public :

	std::list < Cell > & list;
	std::list < Cell > ::iterator iter;

	inline OfFuzzyMesh ( Mesh::Fuzzy * msh, const tag::CellsOfDim &, const size_t d )
	:	Mesh::Iterator::Core (), list { msh->cells[d] }
	{	}  // no need to initialize 'iter', 'reset' will do that
	
	inline OfFuzzyMesh ( const Mesh::Iterator::OverCells::OfFuzzyMesh & it )
	:	Mesh::Iterator::Core (), list { it .list }, iter { it .iter }
	{	}
	
	void reset ( );  // virtual from Mesh::Iterator::Core

	void reset ( const tag::StartAt &, Cell::Core * cll );
	// virtual from Mesh::Iterator::Core, here execution forbidden

	void advance ( );  // virtual from Mesh::Iterator::Core
	bool in_range ( );  // virtual from Mesh::Iterator::Core
	// Cell deref ( )  stays pure virtual from Mesh::Iterator::Core

	// Mesh::Iterator::Core * clone ( )  stays pure virtual from Mesh::Iterator::Core

	class OfLowDim;  class ForcePositive;  class AsFound;
	struct ReverseEachCell;
	
};  // end of class Mesh::Iterator::OverCells::OfFuzzyMesh

//------------------------------------------------------------------------------------------------------//


class Mesh::Iterator::OverCells::OfFuzzyMesh::AsFound
:	public Mesh::Iterator::OverCells::OfFuzzyMesh

// for cells of maximum dimension (equal to the dimension of the mesh), return oriented cells
// for cells of lower dimension, return positive cells

{	public :

	// attributes inherited from Mesh::Iterator::OverCells::OfFuzzyMesh :
	// std::list < Cell > & list
  // std::list < Cell > ::iterator iter

	inline AsFound ( Mesh::Fuzzy * msh, const tag::CellsOfDim &, const size_t d )
	:	Mesh::Iterator::OverCells::OfFuzzyMesh ( msh, tag::cells_of_dim, d )
	{	}  // no need to initialize 'iter', 'reset' will do that
	
	inline AsFound ( const Mesh::Iterator::OverCells::OfFuzzyMesh::AsFound & it )
	:	Mesh::Iterator::OverCells::OfFuzzyMesh ( it )  // list { it .list }, iter { it .iter }
	{	}
	
	// void reset ( )  virtual, defined by Mesh::Iterator::OverCells::OfFuzzyMesh

	// void reset ( const tag::StartAt &, Cell::Core * cll )
	//   virtual, defined by Mesh::Iterator::OverCells::OfFuzzyMesh, execution forbidden

	// void advance ( )  virtual, defined by Mesh::Iterator::OverCells::OfFuzzyMesh
	// bool in_range ( )  virtual, defined by Mesh::Iterator::OverCells::OfFuzzyMesh
	Cell deref ( );  // virtual from Mesh::Iterator::Core

	Mesh::Iterator::Core * clone ( );  // virtual from Mesh::Iterator::Core

};  // end of class Mesh::Iterator::OverCells::OfFuzzyMesh::AsFound

//------------------------------------------------------------------------------------------------------//


class Mesh::Iterator::OverCells::OfFuzzyMesh::ForcePositive
:	public Mesh::Iterator::OverCells::OfFuzzyMesh

{	public :

	// attributes inherited from Mesh::Iterator::OverCells::OfFuzzyMesh :
	// std::list<Cell> & list
  // std::list<Cell>::iterator iter

	inline ForcePositive ( Mesh::Fuzzy * msh, const tag::CellsOfDim &, const size_t d )
	:	Mesh::Iterator::OverCells::OfFuzzyMesh ( msh, tag::cells_of_dim, d )
	{	}  // no need to initialize 'iter', 'reset' will do that
	
	inline ForcePositive ( const Mesh::Iterator::OverCells::OfFuzzyMesh::ForcePositive & it )
	:	Mesh::Iterator::OverCells::OfFuzzyMesh ( it )  // list { it .list }, iter { it .iter }
	{	}
	
	// void reset ( )  virtual, defined by Mesh::Iterator::OverCells::OfFuzzyMesh

	// void reset ( const tag::StartAt &, Cell::Core * cll )
	//   virtual, defined by Mesh::Iterator::OverCells::OfFuzzyMesh, execution forbidden

	// void advance ( )  virtual, defined by Mesh::Iterator::OverCells::OfFuzzyMesh
	// bool in_range ( )  virtual, defined by Mesh::Iterator::OverCells::OfFuzzyMesh
	Cell deref ( );  // virtual from Mesh::Iterator::Core

	Mesh::Iterator::Core * clone ( );  // virtual from Mesh::Iterator::Core

};  // end of class Mesh::Iterator::OverCells::OfFuzzyMesh::ForcePositive

//------------------------------------------------------------------------------------------------------//


class Mesh::Iterator::OverCells::OfFuzzyMesh::ReverseEachCell
:	public Mesh::Iterator::OverCells::OfFuzzyMesh

{	public :

	// attributes inherited from Mesh::Iterator::OverCells::OfFuzzyMesh :
	// std::list<Cell> & list
  // std::list<Cell>::iterator iter

	inline ReverseEachCell ( Mesh::Fuzzy * msh, const tag::CellsOfDim &, const size_t d )
	:	Mesh::Iterator::OverCells::OfFuzzyMesh ( msh, tag::cells_of_dim, d )
	{	}  // no need to initialize 'iter', 'reset' will do that
	
	inline ReverseEachCell
	( const Mesh::Iterator::OverCells::OfFuzzyMesh::ReverseEachCell & it )
	:	Mesh::Iterator::OverCells::OfFuzzyMesh ( it )  // list { it .list }, iter { it .iter }
	{	}
	
	// void reset ( )  virtual, defined by Mesh::Iterator::OverCells::OfFuzzyMesh

	// void reset ( const tag::StartAt &, Cell::Core * cll )
	//   virtual, defined by Mesh::Iterator::OverCells::OfFuzzyMesh, execution forbidden

	// void advance ( )  virtual, defined by Mesh::Iterator::OverCells::OfFuzzyMesh
	// bool in_range ( )  virtual, defined by Mesh::Iterator::OverCells::OfFuzzyMesh
	Cell deref ( );  // virtual from Mesh::Iterator::Core
	// build the reverse cell if it does not exist

	Mesh::Iterator::Core * clone ( );  // virtual from Mesh::Iterator::Core

};  // end of class Mesh::Iterator::OverCells::OfFuzzyMesh::ReverseEachCell


//------------------------------------------------------------------------------------------------------//
//------------------------------------------------------------------------------------------------------//

//    ITERATORS OVER CELLS AROUND A GIVEN CELL (CENTERED AT CELL)

//------------------------------------------------------------------------------------------------------//
//------------------------------------------------------------------------------------------------------//


// see paragraph 9.10 in the manual

class Mesh::Iterator::Around::OneCell : public Mesh::Iterator::Core

// abstract class

// the dimension of the cells to be returned depends on the specialization
// e.g. we can return segments or squares (or cubes) around a vertex
// or squares (or cubes) around a segment

{	public :

	typedef std::map < Mesh::Core*, Cell::field_to_meshes > maptype;
	// center->meshes [ dim ] : short int counter_pos, counter_neg, std::list<Cell>::iterator where
	typedef std::map < Mesh::Core*, Cell::field_to_meshes_same_dim > maptype_same_dim;
	// center->meshes_same_dim : short int sign, std::list<Cell>::iterator where

	Mesh::Core * msh_p;  // an iterator does not keep the mesh alive
	Cell::Positive * center;  // center->dim <= msh->dim - 2
	// exception : iterators of co-dimension 1 are needed for STSI meshes

	inline OneCell ( Mesh::Core * msh, Cell::Positive * const c )
	:	Mesh::Iterator::Core (), msh_p { msh }, center { c }
	{	}

	inline OneCell ( const Mesh::Iterator::Around::OneCell & it )
	:	Mesh::Iterator::Core (), msh_p { it .msh_p }, center { it .center }
	{	}

	// void reset ( )  stays pure virtual from Mesh::Iterator::Core
	// void reset ( tag::StartAt, Cell::Core * cll )  stays pure virtual from Mesh::Iterator::Core
	
	// void advance ( )  stays pure virtual from Mesh::Iterator::Core
	// bool in_range ( )  stays pure virtual from Mesh::Iterator::Core
	// Cell deref ( )  stays pure virtual from Mesh::Iterator::Core

	// Mesh::Iterator::Core * clone ( )  stays pure virtual from Mesh::Iterator::Core

	struct OfCodimTwo
	{ class Ordered; class OverCoVertices; class OverCoSegments;
		struct WorkAround2D { struct NormalOrder { class BuildReverseCells; };
		                      struct ReverseOrder { class BuildReverseCells; }; };	};
	class OfCodimAtLeastTwo;
	struct OfCodimOne  {  class ReturnCellsOfMaxDim;  };
	
};  // end of class Mesh::Iterator::Around::OneCell

//------------------------------------------------------------------------------------------------------//


class Mesh::Iterator::Around::OneCell::OfCodimTwo::Ordered
:	public Mesh::Iterator::Around::OneCell

// abstract class

// when cll->dim == msh->dim - 2, there is a linear order
// e.g. we rotate around a vertex in a 2D mesh
// or we rotate around a segment in a 3D mesh

// the dimension of the cells to be returned is a different matter
// e.g. we can return segments or squares around a vertex in a 2D mesh
// or squares or cubes around a segment in 3D mesh

// from an analogy to iterators over chains of segments
// "covertex" means a cell of dimension center->dim + 1 == msh->dim - 1
// "cosegment" means a cell of dimension center->dim + 2 == msh->dim
// when we rotate around a vertex, "covertex" means segment, "cosegment" means square
// when we rotate around a segment, "covertex" means square, "cosegment" means cube

{	public :

	// attributes inherited from Mesh::Iterator::Around::OneCell :
	// Mesh::Core * msh_p
	// Cell::Positive * center
	
	Cell::Core * current_cosegment { nullptr };

	inline Ordered ( Mesh::Core * msh, Cell::Positive * const c )
	:	Mesh::Iterator::Around::OneCell ( msh, c )  // msh_p { msh }, center { c }
	{	}
	
	inline Ordered ( const Mesh::Iterator::Around::OneCell::OfCodimTwo::Ordered & it )
	:	Mesh::Iterator::Around::OneCell ( it ), current_cosegment { it .current_cosegment }
		// msh_p { it .msh_p }, center { it .center }
	{	}
	
	// void reset ( )  stays pure virtual from Mesh::Iterator::Core
	// void reset ( tag::StartAt, Cell::Core * cll )  stays pure virtual from Mesh::Iterator::Core
	// void reset ( tag::StartAt, Cell::Core * cll )  stays pure virtual from Mesh::Iterator::Core

	// void advance ( )  stays pure virtual from Mesh::Iterator::Core
	// bool in_range ( )  stays virtual from Mesh::Iterator::Core
	// Cell deref ( )  stays pure virtual from Mesh::Iterator::Core
	
	// Mesh::Iterator::Core * clone ( )  stays pure virtual from Mesh::Iterator::Core

};  // end of class Mesh::Iterator::Around::OneCell::OfCodimTwo::Ordered

//------------------------------------------------------------------------------------------------------//


class Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoVertices
:	public Mesh::Iterator::Around::OneCell::OfCodimTwo::Ordered

// abstract class

// when cll->dim == msh->dim - 2, there is a linear order
// e.g. we rotate around a vertex in a 2D mesh
// or we rotate around a segment in a 3D mesh

// the dimension of the cells to be returned is a different matter
// here we return segments around a vertex or squares around a segment

// "CoVertices" means we want to get cells of dimension center->dim + 1 == msh->dim - 1
// the procedure is analogous to Mesh::Iterator::OverVertices::OfConnectedOneDimMesh

// from an analogy to iterators over chains of segments
// "covertex" means a cell of dimension center->dim + 1 == msh->dim - 1
// "cosegment" means a cell of dimension center->dim + 2 == msh->dim
// when we rotate around a vertex, "covertex" means segment, "cosegment" means square
// when we rotate around a segment, "covertex" means square, "cosegment" means cube

{	public :

	// attributes inherited from Mesh::Iterator::Around::OneCell :
	// Mesh::Core * msh_p
	// Cell::Positive * center
	
	// attribute inherited from Mesh::Iterator::Around::OneCell::OfCodimTwo::Ordered :
	// Cell::Core * current_cosegment { nullptr } 

	Cell::Core * current_covertex { nullptr };
	Cell::Core * first_covertex { nullptr };
	Cell::Core * last_cosegment { nullptr };

	inline OverCoVertices ( Mesh::Core * msh, Cell::Positive * const c )
	:	Mesh::Iterator::Around::OneCell::OfCodimTwo::Ordered ( msh, c )  // msh_p { msh }, center { c }
	{	}
	
	inline OverCoVertices ( const Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoVertices & it )
	:	Mesh::Iterator::Around::OneCell::OfCodimTwo::Ordered ( it ),
		// msh_p { it .msh_p }, center { it .center }, current_cosegment { it .current_cosegment }
		current_covertex { it .current_covertex }, first_covertex { it .first_covertex },
		last_cosegment { it .last_cosegment }
	{	}
	
	// void reset ( )  both stay pure virtual from Mesh::Iterator::Core
	// void reset ( const tag::StartAt &, Cell::Core * cll ) 
	
	// void advance ( )  stays pure virtual from Mesh::Iterator::Core

	bool in_range ( );  // virtual from Mesh::Iterator::Core

	// Cell deref ( )  stays pure virtual from Mesh::Iterator::Core
	
	// Mesh::Iterator::Core * clone ( )  stays pure virtual from Mesh::Iterator::Core

	class NormalOrder;  class ReverseOrder;

};  // end of class Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoVertices

//------------------------------------------------------------------------------------------------------//


class Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoVertices::NormalOrder
:	public Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoVertices

// abstract class

// when cll->dim == msh->dim - 2, there is a linear order
// e.g. we rotate around a vertex in a 2D mesh
// or we rotate around a segment in a 3D mesh
// here we follow this order

// the dimension of the cells to be returned is a different matter
// here we return segments around a vertex or squares around a segment
// current_covertex has orientation compatible with center

{	public :

	// attributes inherited from Mesh::Iterator::Around::OneCell :
	// Mesh::Core * msh_p
	// Cell::Positive * center
	
	// attribute inherited from Mesh::Iterator::Around::OneCell::OfCodimTwo::Ordered :
	// Cell::Core * current_cosegment { nullptr } 
	
	// attributes inherited from Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoVertices :
	// Cell::Core * current_covertex { nullptr }
	// Cell::Core * first_covertex { nullptr }
	// Cell::Core * last_cosegment { nullptr }

	// we recognize a closed loop if  last_cosegment == msh .cell_behind ( first_covertex )
	// another possible criterion :  last_cosegment == nullptr  ==>  open chain

	inline NormalOrder ( Mesh::Core * msh, Cell::Positive * const c )
	:	Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoVertices ( msh, c )
	// msh_p { msh }, center { c }
	{	}
	
	inline NormalOrder
	( const Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoVertices::NormalOrder & it )
	:	Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoVertices ( it )
		// msh_p { it .msh_p }, center { it .center }, current_cosegment { it .current_cosegment },
		// current_covertex { it .current_covertex }, first_covertex { it .first_covertex },
		// last_cosegment { it .last_cosegment }
	{	}
	
	void reset ( );  // virtual from Mesh::Iterator::Core
	// void reset ( const tag::StartAt &, Cell::Core * cll )
	//   stays pure virtual from Mesh::Iterator::Core

	void advance ( );  // virtual from Mesh::Iterator::Core
	
	// bool in_range ( )  virtual, defined by Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoVertices
	// Cell deref ( )  stays pure virtual from Mesh::Iterator::Core
	
	// Mesh::Iterator::Core * clone ( )  stays pure virtual from Mesh::Iterator::Core

	class AssumeCellsExist;  class BuildReverseCells;

};  // end of class Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoVertices::NormalOrder

//------------------------------------------------------------------------------------------------------//


class Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoVertices::NormalOrder::BuildReverseCells
:	public Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoVertices::NormalOrder

// abstract class, specialized in three derived classes, see below

{	public :

	// attributes inherited from Mesh::Iterator::Around::OneCell :
	// Mesh::Core * msh_p
	// Cell::Positive * center
	
	// attribute inherited from Mesh::Iterator::Around::OneCell::OfCodimTwo::Ordered :
	// Cell::Core * current_cosegment { nullptr } 
	
	// attributes inherited from Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoVertices :
	// Cell::Core * current_covertex { nullptr }
	// Cell::Core * first_covertex { nullptr }
	// Cell::Core * last_cosegment { nullptr }

	inline BuildReverseCells ( Mesh::Core * msh, Cell::Positive * const c );

	inline BuildReverseCells ( const Mesh::Iterator::Around::OneCell::OfCodimTwo
	                                     ::OverCoVertices::NormalOrder::BuildReverseCells & it )
	:	Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoVertices::NormalOrder ( it )
		// msh_p { it .msh_p }, center { it .center }, current_cosegment { it .current_cosegment },
		// current_covertex { it .current_covertex }, first_covertex { it .first_covertex },
		// last_cosegment { it .last_cosegment }
	{	}
	
	// void reset ( )  virtual,
	//   defined by Mesh::Iterator::Around::OneCell::OfCodimTwo::OverVertices::NormalOrder
	// void reset ( const tag::StartAt &, Cell::Core * cll )
	//   stays pure virtual from Mesh::Iterator::Core
	// void advance ( )  virtual from Mesh::Iterator::Core, defined by
	//   Mesh::Iterator::Around::OneCell::OfCodimTwo::OverVertices::NormalOrder
	// bool in_range ( )  virtual, defined by Mesh::Iterator::Around::OneCell::OfCodimTwo::OverVertices
	// Cell deref ( )  stays pure virtual from Mesh::Iterator::Core

	// Mesh::Iterator::Core * clone ( )  stays pure virtual from Mesh::Iterator::Core

	class AsFound;  class ForcePositive;  class ReverseEachCell;

};  // end of class Mesh::Iterator::Around::OneCell::OfCodimTwo
    //                   ::OverCoVertices::NormalOrder::BuildReverseCells

//------------------------------------------------------------------------------------------------------//


class Mesh::Iterator::Around::OneCell::OfCodimTwo
         ::OverCoVertices::NormalOrder::BuildReverseCells::AsFound
:	public Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoVertices::NormalOrder::BuildReverseCells

// name 'AsFound' is slightly misleading
// returns cells with orientation compatible with center (e.g. segments pointing towards center)

// parent of
// class Mesh::Iterator::Around::OneCell::OfCodimTwo::WorkAround2D::NormalOrder::BuildReverseCells

{	public :

	// attributes inherited from Mesh::Iterator::Around::OneCell :
	// Mesh::Core * msh_p
	// Cell::Positive * center
	
	// attribute inherited from Mesh::Iterator::Around::OneCell::OfCodimTwo::Ordered :
	// Cell::Core * current_cosegment { nullptr } 
	
	// attributes inherited from Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoVertices :
	// Cell::Core * current_covertex { nullptr }
	// Cell::Core * first_covertex { nullptr }
	// Cell::Core * last_cosegment { nullptr }

	inline AsFound ( Mesh::Core * msh, Cell::Positive * const c )
	:	Mesh::Iterator::Around::OneCell::OfCodimTwo
		    ::OverCoVertices::NormalOrder::BuildReverseCells ( msh, c )  // msh_p { msh }, center { c }
	{	}
	
	inline AsFound ( const Mesh::Iterator::Around::OneCell::OfCodimTwo
	                             ::OverCoVertices::NormalOrder::BuildReverseCells::AsFound & it )
	:	Mesh::Iterator::Around::OneCell::OfCodimTwo
		    ::OverCoVertices::NormalOrder::BuildReverseCells ( it )
		// msh_p { it .msh_p }, center { it .center }, current_cosegment { it .current_cosegment },
		// current_covertex { it .current_covertex }, first_covertex { it .first_covertex },
		// last_cosegment { it .last_cosegment }
	{	}

	using Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoVertices::NormalOrder::reset;
  
	// void reset ( )  virtual,
	//   defined by Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoVertices::NormalOrder
	void reset ( const tag::StartAt &, Cell::Core * cll );  // virtual from Mesh::Iterator::Core
	
	// void advance ( )  virtual from Mesh::Iterator::Core, defined by
	//   Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoVertices::NormalOrder

	// bool in_range ( )  virtual, defined by Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoVertices

	Cell deref ( );  // virtual from Mesh::Iterator::Core

	Mesh::Iterator::Core * clone ( );  // virtual from Mesh::Iterator::Core

};  // end of class Mesh::Iterator::Around::OneCell::OfCodimTwo
    //                   ::OverCoVertices::NormalOrder::BuildReverseCells::AsFound

//------------------------------------------------------------------------------------------------------//


class Mesh::Iterator::Around::OneCell::OfCodimTwo
         ::OverCoVertices::NormalOrder::BuildReverseCells::ForcePositive
:	public Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoVertices::NormalOrder::BuildReverseCells

{	public :

	// attributes inherited from Mesh::Iterator::Around::OneCell :
	// Mesh::Core * msh_p
	// Cell::Positive * center
	
	// attribute inherited from Mesh::Iterator::Around::OneCell::OfCodimTwo::Ordered :
	// Cell::Core * current_cosegment { nullptr } 
	
	// attributes inherited from Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoVertices :
	// Cell::Core * current_covertex { nullptr }
	// Cell::Core * first_covertex { nullptr }
	// Cell::Core * last_cosegment { nullptr }

	inline ForcePositive ( Mesh::Core * msh, Cell::Positive * const c )
	:	Mesh::Iterator::Around::OneCell::OfCodimTwo
		    ::OverCoVertices::NormalOrder::BuildReverseCells ( msh, c )  // msh_p { msh }, center { c }
	{	}
	
	inline ForcePositive ( const Mesh::Iterator::Around::OneCell::OfCodimTwo
	                                 ::OverCoVertices::NormalOrder::BuildReverseCells::ForcePositive & it )
	:	Mesh::Iterator::Around::OneCell::OfCodimTwo
		    ::OverCoVertices::NormalOrder::BuildReverseCells ( it )
		// msh_p { it .msh_p }, center { it .center }, current_cosegment { it .current_cosegment },
		// current_covertex { it .current_covertex }, first_covertex { it .first_covertex },
		// last_cosegment { it .last_cosegment }
	{	}

	using Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoVertices::NormalOrder::reset;
  
	// void reset ( )  virtual,
	//   defined by Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoVertices::NormalOrder
	void reset ( const tag::StartAt &, Cell::Core * cll );  // virtual from Mesh::Iterator::Core
	
	// void advance ( )  virtual from Mesh::Iterator::Core, defined by
	//   Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoVertices::NormalOrder

	// bool in_range ( )  virtual, defined by Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoVertices
	
	Cell deref ( );  // virtual from Mesh::Iterator::Core

	Mesh::Iterator::Core * clone ( );  // virtual from Mesh::Iterator::Core

};  // end of class Mesh::Iterator::Around::OneCell::OfCodimTwo
    //                  ::OverCoVertices::NormalOrder::BuildReverseCells::ForcePositive

//------------------------------------------------------------------------------------------------------//


class Mesh::Iterator::Around::OneCell::OfCodimTwo
         ::OverCoVertices::NormalOrder::BuildReverseCells::ReverseEachCell
:	public Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoVertices::NormalOrder::BuildReverseCells

// returns cells with orientation opposite to center (e.g. segments pointing away from center)

{	public :

	// attributes inherited from Mesh::Iterator::Around::OneCell :
	// Mesh::Core * msh_p
	// Cell::Positive * center
	
	// attribute inherited from Mesh::Iterator::Around::OneCell::OfCodimTwo::Ordered :
	// Cell::Core * current_cosegment { nullptr } 
	
	// attributes inherited from Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoVertices :
	// Cell::Core * current_covertex { nullptr }
	// Cell::Core * first_covertex { nullptr }
	// Cell::Core * last_cosegment { nullptr }

	inline ReverseEachCell ( Mesh::Core * msh, Cell::Positive * const c )
	:	Mesh::Iterator::Around::OneCell::OfCodimTwo
		    ::OverCoVertices::NormalOrder::BuildReverseCells ( msh, c )  // msh_p { msh }, center { c }
	{	}
	
	inline ReverseEachCell ( const Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoVertices
	                                   ::NormalOrder::BuildReverseCells::ReverseEachCell & it  )
	:	Mesh::Iterator::Around::OneCell::OfCodimTwo
		    ::OverCoVertices::NormalOrder::BuildReverseCells ( it )
		// msh_p { it .msh_p }, center { it .center }, current_cosegment { it .current_cosegment },
		// current_covertex { it .current_covertex }, first_covertex { it .first_covertex },
		// last_cosegment { it .last_cosegment }
	{	}

	using Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoVertices::NormalOrder::reset;
  
	// void reset ( )  virtual,
	//   defined by Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoVertices::NormalOrder
	void reset ( const tag::StartAt &, Cell::Core * cll );  // virtual from Mesh::Iterator::Core
	
	// void advance ( )  virtual from Mesh::Iterator::Core, defined by
	//   Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoVertices::NormalOrder

	// bool in_range ( )  virtual, defined by Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoVertices
	
	Cell deref ( );  // virtual from Mesh::Iterator::Core

	Mesh::Iterator::Core * clone ( );  // virtual from Mesh::Iterator::Core

};  // end of class Mesh::Iterator::Around::OneCell::OfCodimTwo
    //                  ::OverCoVertices::NormalOrder::BuildReverseCells::ReverseEachCell

//------------------------------------------------------------------------------------------------------//


class Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoVertices::ReverseOrder
:	public Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoVertices

// abstract class

// when cll->dim == msh->dim - 2, there is a linear order
// e.g. we rotate around a vertex in a 2D mesh
// or we rotate around a segment in a 3D mesh
// here we follow the reversed order

// the dimension of the cells to be returned is a different matter
// here we return segments around a vertex or squares around a segment

{	public :

	// attributes inherited from Mesh::Iterator::Around::OneCell :
	// Mesh::Core * msh_p
	// Cell::Positive * center
	
	// attribute inherited from Mesh::Iterator::Around::OneCell::OfCodimTwo::Ordered :
	// Cell::Core * current_cosegment { nullptr }
	
	// attributes inherited from Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoVertices :
	// Cell::Core * current_covertex { nullptr }
	// Cell::Core * first_covertex { nullptr }
	// Cell::Core * last_cosegment { nullptr }

	// we recognize a closed loop if  last_cosegment == msh .cell_in_front_of ( first_covertex )
	// another possible criterion :  last_cosegment == nullptr  ==>  open chain

	inline ReverseOrder ( Mesh::Core * msh, Cell::Positive * const c );
	
	inline ReverseOrder
	( const Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoVertices::ReverseOrder & it )
	:	Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoVertices ( it )
		// msh_p { it .msh_p }, center { it .center }, current_cosegment { it .current_cosegment },
		// current_covertex { it .current_covertex }, first_covertex { it .first_covertex },
		// last_cosegment { it .last_cosegment }
	{	}

	void reset ( );  // virtual from Mesh::Iterator::Core
	// void reset ( const tag::StartAt &, Cell::Core * cll )
	//   stays pure virtual from Mesh::Iterator::Core
	// void advance ( )  stays pure virtual from Mesh::Iterator::Core
	// bool in_range ( )  virtual, defined by Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoVertices
	// Cell deref ( )  stays pure virtual from Mesh::Iterator::Core

	// Mesh::Iterator::Core * clone ( )  stays pure virtual from Mesh::Iterator::Core

	class AssumeCellsExist;  class BuildReverseCells;

};  // end of class Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoVertices::ReverseOrder

//------------------------------------------------------------------------------------------------------//


class Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoVertices::ReverseOrder::BuildReverseCells
:	public Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoVertices::ReverseOrder

// abstract class, specialized in three derived classes, see below

{	public :

	// attributes inherited from Mesh::Iterator::Around::OneCell :
	// Mesh::Core * msh_p
	// Cell::Positive * center
	
	// attribute inherited from Mesh::Iterator::Around::OneCell::OfCodimTwo::Ordered :
	// Cell::Core * current_cosegment { nullptr } 
	
	// attributes inherited from Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoVertices :
	// Cell::Core * current_covertex { nullptr }
	// Cell::Core * first_covertex { nullptr }
	// Cell::Core * last_cosegment { nullptr }

	inline BuildReverseCells ( Mesh::Core * msh, Cell::Positive * const c )
	:	Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoVertices::ReverseOrder ( msh, c )
	// msh_p { msh }, center { c }
	{	}
	
	inline BuildReverseCells ( const Mesh::Iterator::Around::OneCell::OfCodimTwo
	                                     ::OverCoVertices::ReverseOrder::BuildReverseCells & it )
	:	Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoVertices::ReverseOrder ( it )
		// msh_p { it .msh_p }, center { it .center }, current_cosegment { it .current_cosegment },
		// current_covertex { it .current_covertex }, first_covertex { it .first_covertex },
		// last_cosegment { it .last_cosegment }
	{	}

	// void reset ( )  virtual,
	//   defined by Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoVertices::ReverselOrder
	// void reset ( const tag::StartAt &, Cell::Core * cll )
	//   stays pure virtual from Mesh::Iterator::Core
	
	void advance ( );  // virtual from Mesh::Iterator::Core

	// bool in_range ( )  virtual, defined by Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoVertices
	// Cell deref ( )  stays pure virtual from Mesh::Iterator::Core

	// Mesh::Iterator::Core * clone ( )  stays pure virtual from Mesh::Iterator::Core

	class AsFound;  class ForcePositive;  class ReverseEachCell;

};  // end of class Mesh::Iterator::Around::OneCell::OfCodimTwo
    //                   ::OverCoVertices::ReverseOrder::BuildReverseCells

//------------------------------------------------------------------------------------------------------//


class Mesh::Iterator::Around::OneCell::OfCodimTwo
          ::OverCoVertices::ReverseOrder::BuildReverseCells::AsFound
:	public Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoVertices::ReverseOrder::BuildReverseCells

{	public :

	// attributes inherited from Mesh::Iterator::Around::OneCell :
	// Mesh::Core * msh_p
	// Cell::Positive * center
	
	// attribute inherited from Mesh::Iterator::Around::OneCell::OfCodimTwo::Ordered :
	// Cell::Core * current_cosegment { nullptr } 
	
	// attributes inherited from Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoVertices :
	// Cell::Core * current_covertex { nullptr }
	// Cell::Core * first_covertex { nullptr }
	// Cell::Core * last_cosegment { nullptr }

	inline AsFound ( Mesh::Core * msh, Cell::Positive * const c )
	:	Mesh::Iterator::Around::OneCell::OfCodimTwo
		    ::OverCoVertices::ReverseOrder::BuildReverseCells ( msh, c )  // msh_p { msh }, center { c }
	{	}
	
	inline AsFound ( const Mesh::Iterator::Around::OneCell::OfCodimTwo
	                             ::OverCoVertices::ReverseOrder::BuildReverseCells::AsFound & it )
	:	Mesh::Iterator::Around::OneCell::OfCodimTwo
		    ::OverCoVertices::ReverseOrder::BuildReverseCells ( it )
		// msh_p { it .msh_p }, center { it .center }, current_cosegment { it .current_cosegment },
		// current_covertex { it .current_covertex }, first_covertex { it .first_covertex },
		// last_cosegment { it .last_cosegment }
	{	}

  	using Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoVertices::ReverseOrder::reset;

	// void reset ( )  virtual,
	//   defined by Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoVertices::ReverseOrder
	void reset ( const tag::StartAt &, Cell::Core * cll );  // virtual from Mesh::Iterator::Core
	
	// void advance ( )  virtual from Mesh::Iterator::Core, defined by
	//   Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoVertices::ReverseOrder::BuildReverseCells

	Cell deref ( );  // virtual from Mesh::Iterator::Core

	// bool in_range ( )  virtual, defined by Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoVertices

	Mesh::Iterator::Core * clone ( );  // virtual from Mesh::Iterator::Core

};  // end of class Mesh::Iterator::Around::OneCell::OfCodimTwo
    //                   ::OverCoVertices::ReverseOrder::BuildReverseCells::AsFound

//------------------------------------------------------------------------------------------------------//


class Mesh::Iterator::Around::OneCell::OfCodimTwo
          ::OverCoVertices::ReverseOrder::BuildReverseCells::ForcePositive
:	public Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoVertices::ReverseOrder::BuildReverseCells

{	public :

	// attributes inherited from Mesh::Iterator::Around::OneCell :
	// Mesh::Core * msh_p
	// Cell::Positive * center
	
	// attribute inherited from Mesh::Iterator::Around::OneCell::OfCodimTwo::Ordered :
	// Cell::Core * current_cosegment { nullptr } 
	
	// attributes inherited from Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoVertices :
	// Cell::Core * current_covertex { nullptr }
	// Cell::Core * first_covertex { nullptr }
	// Cell::Core * last_cosegment { nullptr }

	inline ForcePositive ( Mesh::Core * msh, Cell::Positive * const c )
	:	Mesh::Iterator::Around::OneCell::OfCodimTwo
		    ::OverCoVertices::ReverseOrder::BuildReverseCells ( msh,c )  // msh_p { msh }, center { c }
	{	}
	
	inline ForcePositive ( const Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoVertices
	                                 ::ReverseOrder::BuildReverseCells::ForcePositive & it   )
	:	Mesh::Iterator::Around::OneCell::OfCodimTwo
		    ::OverCoVertices::ReverseOrder::BuildReverseCells ( it )
		// msh_p { it .msh_p }, center { it .center }, current_cosegment { it .current_cosegment },
		// current_covertex { it .current_covertex }, first_covertex { it .first_covertex },
		// last_cosegment { it .last_cosegment }
	{	}

  	using Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoVertices::ReverseOrder::reset;

	// void reset ( )  virtual,
	//   defined by Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoVertices::ReverseOrder

	void reset ( const tag::StartAt &, Cell::Core * cll );  // virtual from Mesh::Iterator::Core
	
	// void advance ( )  virtual from Mesh::Iterator::Core, defined by
	//   Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoVertices::ReverseOrder::BuildReverseCells
	// bool in_range ( )  virtual, defined by Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoVertices
	
	Cell deref ( );  // virtual from Mesh::Iterator::Core

	Mesh::Iterator::Core * clone ( );  // virtual from Mesh::Iterator::Core

};  // end of class Mesh::Iterator::Around::OneCell::OfCodimTwo
    //                  ::OverCoVertices::ReverseOrder::BuildReverseCells::ForcePositive

//------------------------------------------------------------------------------------------------------//


class Mesh::Iterator::Around::OneCell::OfCodimTwo
          ::OverCoVertices::ReverseOrder::BuildReverseCells::ReverseEachCell
:	public Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoVertices::ReverseOrder::BuildReverseCells

{	public :

	// attributes inherited from Mesh::Iterator::Around::OneCell :
	// Mesh::Core * msh_p
	// Cell::Positive * center
	
	// attribute inherited from Mesh::Iterator::Around::OneCell::OfCodimTwo::Ordered :
	// Cell::Core * current_cosegment { nullptr } 
	
	// attributes inherited from Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoVertices :
	// Cell::Core * current_covertex { nullptr }
	// Cell::Core * first_covertex { nullptr }
	// Cell::Core * last_cosegment { nullptr }

	inline ReverseEachCell ( Mesh::Core * msh, Cell::Positive * const c )
	:	Mesh::Iterator::Around::OneCell::OfCodimTwo
		    ::OverCoVertices::ReverseOrder::BuildReverseCells ( msh, c )  // msh_p { msh }, center { c }
	{	}
	
	inline ReverseEachCell ( const Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoVertices
	                                   ::ReverseOrder::BuildReverseCells::ReverseEachCell & it )
	:	Mesh::Iterator::Around::OneCell::OfCodimTwo
		    ::OverCoVertices::ReverseOrder::BuildReverseCells ( it )
		// msh_p { it .msh_p }, center { it .center }, current_cosegment { it .current_cosegment },
		// current_covertex { it .current_covertex }, first_covertex { it .first_covertex },
		// last_cosegment { it .last_cosegment }
	{	}

  	using Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoVertices::ReverseOrder::reset;

	// void reset ( )  virtual,
	//   defined by Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoVertices::ReverseOrder

	void reset ( const tag::StartAt &, Cell::Core * cll );  // virtual from Mesh::Iterator::Core
	
	// void advance ( )  virtual from Mesh::Iterator::Core, defined by
	//   Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoVertices::ReverseOrder::BuildReverseCells

	// bool in_range ( )  virtual from Mesh::Iterator::Core,
  //   defined by Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoVertices
	
	Cell deref ( );  // virtual from Mesh::Iterator::Core

	Mesh::Iterator::Core * clone ( );  // virtual from Mesh::Iterator::Core

};  // end of class Mesh::Iterator::Around::OneCell::OfCodimTwo
    //                  ::OverCoVertices::ReverseOrder::BuildReverseCells::ReverseEachCell

//------------------------------------------------------------------------------------------------------//
//------------------------------------------------------------------------------------------------------//


namespace tag::local_functions::iterator  {

inline Cell::Core * find_first_coseg ( Cell::Positive * const pos_cen, Mesh::Core * const msh )

{	typedef std::map < Mesh::Core*, Cell::field_to_meshes > maptype;
	typedef std::map < Mesh::Core*, Cell::field_to_meshes_same_dim > maptype_sd;
	maptype & cm2 = pos_cen->meshes [1];
	for ( typename maptype::iterator it = cm2 .begin(); it != cm2 .end(); it++ )
	{	Mesh::Core * m = it->first;
		// is 'm' the boundary of some cell ?
		Cell::Positive * mce = m->cell_enclosed;
		if ( mce == nullptr ) continue;
		#ifndef NDEBUG  // DEBUG mode
		// a boundary has no boundary, so it will appear exactly twice, with opposite orientations
		Cell::field_to_meshes f = it->second;
		assert ( f .counter_pos == 1 );
		assert ( f .counter_neg == 1 );
		#endif  // DEBUG
		// does mce belong to msh ? note that mce->dim == msh->dim and mce->is_positive
		Cell::Positive::NotVertex * mce_nv = tag::Util::assert_cast
			< Cell::Core*, Cell::Positive::NotVertex* > ( mce );
		maptype_sd & mce_msd = mce_nv->meshes_same_dim;
		typename maptype_sd::iterator itt = mce_msd .find ( msh );
		if ( itt == mce_msd .end() ) continue;
		assert ( itt->first == msh );
		// yes ! mce may be current_seg, if correctly oriented
		Cell::field_to_meshes_same_dim ff = itt->second;
		if ( ff .sign == 1 ) return mce;
		// else
		assert ( ff .sign == -1 );
		assert ( mce->reverse_attr .exists() );
		return  mce->reverse_attr .core;                             }
	return nullptr;                                                             }


inline void rotate_backwards_for_ver
( Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoVertices * that )

// that->first_covertex should be reversed by calling function
	
{	Cell this_center ( tag::whose_core_is, that->center,
	                   tag::previously_existing, tag::surely_not_null );
	Mesh m ( tag::whose_core_is, that->msh_p, tag::previously_existing, tag::is_positive );
	assert ( that->current_cosegment );
	Cell::Core * vertex_stop = that->current_cosegment->boundary() .cell_behind
		( this_center, tag::surely_exists ) .core;
	while ( true )
	{	Cell::Core * vertex_p = that->current_cosegment->boundary() .cell_in_front_of
			( this_center, tag::surely_exists ) .core;
		Cell vertex ( tag::whose_core_is, vertex_p,
		              tag::previously_existing, tag::surely_not_null );
		Cell new_seg = m .cell_in_front_of ( vertex, tag::may_not_exist );
		if ( not new_seg .exists() )  // we have hit the boundary
		{	that->first_covertex = vertex_p;
			that->last_cosegment = nullptr;
			return;                          }
		if ( vertex_p->reverse_attr.core == vertex_stop )  // completed loop
		{	that->last_cosegment = m .cell_in_front_of ( vertex ) .core;
			that->first_covertex = vertex_p;
			return;                                                      }
		that->current_cosegment = new_seg .core;                                                  }  }

// the above could get simpler and faster by using a method like
// inline Cell::Core * Mesh::Core::cell_behind_ptr
// ( const Cell::Core * face, const tag::MayNotExist & ) const
// which is not difficult to implement


inline void rotate_forward_for_ver
( Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoVertices * that )
	
{	Cell this_center ( tag::whose_core_is, that->center,
	                   tag::previously_existing, tag::surely_not_null );
	Mesh m ( tag::whose_core_is, that->msh_p, tag::previously_existing, tag::is_positive );
	assert ( that->current_cosegment );
	Cell::Core * vertex_stop = that->current_cosegment->boundary() .cell_in_front_of
		( this_center, tag::surely_exists ) .core;
	while ( true )
	{	Cell::Core * vertex_p = that->current_cosegment->boundary() .cell_behind
			( this_center, tag::surely_exists ) .core;
		Cell vertex ( tag::whose_core_is, vertex_p,
		              tag::previously_existing, tag::surely_not_null );
		Cell new_seg = m .cell_in_front_of ( vertex, tag::may_not_exist );
		if ( not new_seg.exists() )  // we have hit the boundary
		{	that->first_covertex = vertex_p;
			that->last_cosegment = nullptr;
			return;                          }
		if ( vertex_p->reverse_attr.core == vertex_stop )  // completed loop
		{	that->last_cosegment = m .cell_in_front_of ( vertex ) .core;
			that->first_covertex = vertex_p;
			return;                                                      }
		that->current_cosegment = new_seg .core;                                                 }  }

// the above could get simpler and faster by using a method like
// inline Cell::Core * Mesh::Core::cell_behind_ptr
// ( const Cell::Core * face, const tag::MayNotExist & ) const
// which is not difficult to implement
	
}  // end of  namespace tag::local_functions::iterator

//------------------------------------------------------------------------------------------------------//


inline Mesh::Iterator::Around::OneCell::OfCodimTwo
           ::OverCoVertices::NormalOrder::BuildReverseCells::BuildReverseCells
( Mesh::Core * msh, Cell::Positive * cen )
:	Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoVertices::NormalOrder ( msh, cen )
// msh_p { msh }, center { cen }

// we don't know whether we are in the interior or on the boundary of the mesh
// so we must search for this->first_covertex, rotating backwards
	
{	assert ( this->msh_p );
	assert ( cen );
	assert ( cen->get_dim() + 3 == this->msh_p->get_dim_plus_one() );
	this->center = cen;
	this->current_cosegment = tag::local_functions::iterator::find_first_coseg ( cen, this->msh_p );
	assert ( this->current_cosegment );

	// now we rotate backwards until we meet the boundary or close the loop
	tag::local_functions::iterator::rotate_backwards_for_ver ( this );
	// we do not reverse this->first_covertex
	// instead, this->reset() will reverse it while assigning to this->current_covertex
	// however, we build the reverse if it does not exist
	
	if ( not this->first_covertex->reverse_attr .exists() )
	//	this->first_covertex->reverse_attr =
	//		Cell ( tag::whose_core_is, this->first_covertex->build_reverse
	//	         ( tag::one_dummy_wrapper ), tag::freshly_created         );
	{	Cell::Positive * tfv = tag::Util::assert_cast
			< Cell::Core*, Cell::Positive* > ( this->first_covertex );
		tfv->reverse_attr.core = tfv->build_reverse ( tag::one_dummy_wrapper );  }
	
	assert ( this->first_covertex->reverse_attr .core );
	this->first_covertex = this->first_covertex->reverse_attr .core;                                  }

// the above could get simpler and faster by using a method like
// inline Cell::Core * Mesh::Core::cell_behind_ptr
// ( const Cell::Core * face, const tag::MayNotExist & ) const
// which is not difficult to implement
	
//------------------------------------------------------------------------------------------------------//


inline Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoVertices::ReverseOrder::ReverseOrder
( Mesh::Core * msh, Cell::Positive * cen )
:	Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoVertices ( msh, cen )
// msh_p { msh }, center { cen }

// we don't know whether we are in the interior or on the boundary of the mesh
// so we must search for this->first_covertex, rotating backwards
	
{	assert ( this->msh_p );
	assert ( cen );
	assert ( cen->get_dim() + 3 == this->msh_p->get_dim_plus_one() );
	this->center = cen;
	this->current_cosegment = tag::local_functions::iterator::find_first_coseg ( cen, this->msh_p );
	assert ( this->current_cosegment );

	// now we rotate forward until we meet the boundary or close the loop
	tag::local_functions::iterator::rotate_forward_for_ver ( this );                                 }

// the above could get simpler and faster by using a method like
// inline Cell::Core * Mesh::Core::cell_behind_ptr
// ( const Cell::Core * face, const tag::MayNotExist & ) const
// which is not difficult to implement
	
//------------------------------------------------------------------------------------------------------//
//------------------------------------------------------------------------------------------------------//


class Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoSegments
:	public Mesh::Iterator::Around::OneCell::OfCodimTwo::Ordered

// abstract class

// when cll->dim == msh->dim - 2, there is a linear order
// e.g. we rotate around a vertex in a 2D mesh
// or we rotate around a segment in a 3D mesh

// the dimension of the cells to be returned is a different matter
// here we return squares around a vertex or cubes around a segment

// "cosegments" means we want to get cells of dimension center->dim + 2 == msh->dim
// the procedure is analogous to Mesh::Iterator::OverSegments::OfConnectedOneDimMesh

// from an analogy to iterators over chains of segments
// "covertex" means a cell of dimension center->dim + 1 == msh->dim - 1
// "cosegment" means a cell of dimension center->dim + 2 == msh->dim
// when we rotate around a vertex, "covertex" means segment, "cosegment" means square
// when we rotate around a segment, "covertex" means square, "cosegment" means cube

{	public :

	// attributes inherited from Mesh::Iterator::Around::OneCell :
	// Mesh::Core * msh_p
	// Cell::Positive * center
	
	// attribute inherited from Mesh::Iterator::Around::OneCell::OfCodimTwo::Ordered :
	// Cell::Core * current_cosegment { nullptr }

	Cell::Core * first_cosegment { nullptr };
	Cell::Core * last_covertex { nullptr }; 

	inline OverCoSegments ( Mesh::Core * msh, Cell::Positive * const c )
	:	Mesh::Iterator::Around::OneCell::OfCodimTwo::Ordered ( msh, c )  // msh_p { msh }, center { c }
	{	}

	inline OverCoSegments ( const Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoSegments & it )
	:	Mesh::Iterator::Around::OneCell::OfCodimTwo::Ordered ( it ),
		// msh_p { it .msh_p }, center { it .center }, current_cosegment { it .current_cosegment }
		first_cosegment { it .first_cosegment }, last_covertex { it .last_covertex }
	{	}

	void reset ( );  // virtual from Mesh::Iterator::Core
	
	// void reset ( const tag::StartAt &, Cell::Core * cll )
	//      stays pure virtual from Mesh::Iterator::Core
	
	// void advance ( )  stays pure virtual from Mesh::Iterator::Core

	bool in_range ( );  // virtual from Mesh::Iterator::Core
	
	// Cell deref ( )  stays pure virtual from Mesh::Iterator::Core

	// Mesh::Iterator::Core * clone ( )  stays pure virtual from Mesh::Iterator::Core

	class NormalOrder;  class ReverseOrder;

};  // end of class Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoSegments

//------------------------------------------------------------------------------------------------------//


class Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoSegments::NormalOrder
:	public Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoSegments

// abstract class, specialized in one derived class, see below

// when cll->dim == msh->dim - 2, there is a linear order
// e.g. we rotate around a vertex in a 2D mesh
// or we rotate around a segment in a 3D mesh
// here we follow this order

// the dimension of the cells to be returned is a different matter
// here we return squares around a vertex or cubes around a segment

{	public :

	// attributes inherited from Mesh::Iterator::Around::OneCell :
	// Mesh::Core * msh_p
	// Cell::Positive * center
	
	// attribute inherited from Mesh::Iterator::Around::OneCell::OfCodimTwo::Ordered :
	// Cell::Core * current_cosegment { nullptr }

	// attributes inherited from Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoSegments :
	// Cell::Core * first_cosegment { nullptr }
	// Cell::Core * last_covertex { nullptr }

	inline NormalOrder ( Mesh::Core * msh, Cell::Positive * const c );
	
	inline NormalOrder
	( const Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoSegments::NormalOrder & it )
	:	Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoSegments ( it )
		// msh_p { it .msh_p }, center { it .center }, current_cosegment { it .current_cosegment },
		// first_cosegment { it .first_cosegment }, last_covertex { it .last_covertex }
	{	}

	using Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoSegments::reset;

	// void reset ( )  defined by Mesh::Iterator::Around::OneCell::OfCodimTwo::OverSegments

	void reset ( const tag::StartAt &, Cell::Core * cll );  // virtual from Mesh::Iterator::Core

	void advance ( );  // virtual from Mesh::Iterator::Core
	
	// bool in_range ( )  virtual, defined by Mesh::Iterator::Around::OneCell::OfCodimTwo::OverSegments
	// Cell deref ( )  stays pure virtual from Mesh::Iterator::Core
	
	// Mesh::Iterator::Core * clone ( )  stays pure virtual from Mesh::Iterator::Core

	class AsFound;  class ForcePositive;  class ReverseEachCell;

};  // end of class Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoSegments::NormalOrder

//------------------------------------------------------------------------------------------------------//


class Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoSegments::NormalOrder::AsFound
:	public Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoSegments::NormalOrder

// here "as found" is equivalent with "orientation compatible with mesh"

{	public :

	// attributes inherited from Mesh::Iterator::Around::OneCell :
	// Mesh::Core * msh_p
	// Cell::Positive * center
	
	// attribute inherited from Mesh::Iterator::Around::OneCell::OfCodimTwo::Ordered :
	// Cell::Core * current_cosegment { nullptr }

	// attributes inherited from Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoSegments :
	// Cell::Core * first_cosegment { nullptr }
	// Cell::Core * last_covertex { nullptr }

	inline AsFound ( Mesh::Core * msh, Cell::Positive * const c )
	:	Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoSegments::NormalOrder ( msh, c )
	// msh_p { msh }, center { c }
	{	}
	
	inline AsFound
	( const Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoSegments::NormalOrder::AsFound & it )
	:	Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoSegments::NormalOrder ( it )
		// msh_p { it .msh_p }, center { it .center }, current_cosegment { it .current_cosegment },
		// first_cosegment { it .first_cosegment }, last_covertex { it .last_covertex }
	{	}

	// void reset ( )  virtual,
	//    defined by Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoSegments

	// void reset ( const tag::StartAt &, Cell::Core * cll )  -- virtual,
	//    defined by Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoSegments::NormalOrder

	// void advance ( )  virtual,
	//    defined by Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoSegments::NormalOrder
	
	// bool in_range ( )  virtual, defined by Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoSegments
	
	Cell deref ( );  // virtual from Mesh::Iterator::Core

	Mesh::Iterator::Core * clone ( );  // virtual from Mesh::Iterator::Core

};  // end of class Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoSegments::NormalOrder::AsFound

//------------------------------------------------------------------------------------------------------//


class Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoSegments::ReverseOrder
:	public Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoSegments

// abstract class, specialized in one derived class, see below

// when cll->dim == msh->dim - 2, there is a linear order
// e.g. we rotate around a vertex in a 2D mesh
// or we rotate around a segment in a 3D mesh
// here we follow the reversed order

// the dimension of the cells to be returned is a different matter
// here we return squares around a vertex or cubes around a segment

{	public :

	// attributes inherited from Mesh::Iterator::Around::OneCell :
	// Mesh::Core * msh_p
	// Cell::Positive * center
	
	// attribute inherited from Mesh::Iterator::Around::OneCell::OfCodimTwo::Ordered :
	// Cell::Core * current_cosegment { nullptr }

	// attributes inherited from Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoSegments :
	// Cell::Core * first_cosegment { nullptr }
	// Cell::Core * last_covertex { nullptr }

	inline ReverseOrder ( Mesh::Core * msh, Cell::Positive * const c );
	
	inline ReverseOrder
	( const Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoSegments::ReverseOrder & it )
	:	Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoSegments ( it )
		// msh_p { it .msh_p }, center { it .center }, current_cosegment { it .current_cosegment },
		// first_cosegment { it .first_cosegment }, last_covertex { it .last_covertex }
	{	}

	using Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoSegments::reset;
	
	// void reset ( )  defined by Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoSegments

	void reset ( const tag::StartAt &, Cell::Core * cll );  // virtual from Mesh::Iterator::Core
	
	void advance ( );  // virtual from Mesh::Iterator::Core
	
	// bool in_range ( )  virtual, defined by Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoSegments
	// Cell deref ( )  stays pure virtual from Mesh::Iterator::Core

	// Mesh::Iterator::Core * clone ( )  stays pure virtual from Mesh::Iterator::Core

	class AsFound;  class ForcePositive;  class ReverseEachCell;

};  // end of class Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoSegments::ReverseOrder

//------------------------------------------------------------------------------------------------------//


class Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoSegments::ReverseOrder::AsFound
:	public Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoSegments::ReverseOrder

// here "as found" is equivalent with "orientation compatible with mesh"

{	public :

	// attributes inherited from Mesh::Iterator::Around::OneCell :
	// Mesh::Core * msh_p
	// Cell::Positive * center
	
	// attribute inherited from Mesh::Iterator::Around::OneCell::OfCodimTwo::Ordered :
	// Cell::Core * current_cosegment { nullptr }

	// attributes inherited from Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoSegments :
	// Cell::Core * first_cosegment { nullptr }
	// Cell::Core * last_covertex { nullptr }

	inline AsFound ( Mesh::Core * msh, Cell::Positive * const c )
	:	Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoSegments::ReverseOrder ( msh, c )
	// msh_p { msh }, center { c }
	{	}
	
	inline AsFound
	( const Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoSegments::ReverseOrder::AsFound & it )
	:	Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoSegments::ReverseOrder ( it )
		// msh_p { it .msh_p }, center { it .center }, current_cosegment { it .current_cosegment },
		// first_cosegment { it .first_cosegment }, last_covertex { it .last_covertex }
	{	}

	// void reset ( )  -- virtual,
	//    defined by Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoSegments

	// void reset ( const tag::StartAt &, Cell::Core * cll )  -- virtual,
	//    defined by Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoSegments::ReverseOrder

	// void advance ( )  -- virtual,
	//    defined by Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoSegments::ReverseOrder
	
	// bool in_range ( )  virtual, defined by Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoSegments
	
	Cell deref ( );  // virtual from Mesh::Iterator::Core

	Mesh::Iterator::Core * clone ( );  // virtual from Mesh::Iterator::Core

};  // end of class Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoSegments::ReverseOrder::AsFound

//------------------------------------------------------------------------------------------------------//
//------------------------------------------------------------------------------------------------------//


namespace tag::local_functions::iterator  {
		
inline void rotate_backwards_for_seg
( Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoSegments * that )

{	Cell this_center ( tag::whose_core_is, that->center,
	                   tag::previously_existing, tag::surely_not_null );
	Mesh m ( tag::whose_core_is, that->msh_p, tag::previously_existing, tag::is_positive );
	assert ( that->first_cosegment );
	Cell::Core * vertex_stop = that->first_cosegment->boundary() .cell_behind
		( this_center, tag::surely_exists ) .core;
	while ( true )
	{	Cell::Core * vertex_p = that->first_cosegment->boundary() .cell_in_front_of
			( this_center, tag::surely_exists ) .core;
		Cell vertex ( tag::whose_core_is, vertex_p,
	                tag::previously_existing, tag::surely_not_null );
		Cell new_seg = m .cell_in_front_of ( vertex, tag::may_not_exist );
		if ( not new_seg .exists() ) return;  // we have hit the boundary
		        // that->last_covertex remains nullptr
		if ( vertex_p->reverse_attr .core == vertex_stop )  // completed loop
		{	that->last_covertex = vertex_stop;  return; }
		that->first_cosegment = new_seg .core;                                         }           }

// the above could get simpler and faster by using a method like
// inline Cell::Core * Mesh::Core::cell_behind_ptr
// ( const Cell::Core * face, const tag::MayNotExist & ) const
// which is not difficult to implement


inline void rotate_forward_for_seg
( Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoSegments * that )

{	Cell this_center ( tag::whose_core_is, that->center,
	                   tag::previously_existing, tag::surely_not_null );
	Mesh m ( tag::whose_core_is, that->msh_p, tag::previously_existing, tag::is_positive );
	assert ( that->first_cosegment );
	Cell::Core * vertex_stop = that->first_cosegment->boundary() .cell_in_front_of
		( this_center, tag::surely_exists ) .core;
	while ( true )
	{	Cell::Core * vertex_p = that->first_cosegment->boundary() .cell_behind
			( this_center, tag::surely_exists ) .core;
		Cell vertex ( tag::whose_core_is, vertex_p,
	                tag::previously_existing, tag::surely_not_null );
		Cell new_seg = m .cell_in_front_of ( vertex, tag::may_not_exist );
		if ( not new_seg .exists() ) return;  // we have hit the boundary
		        // that->last_covertex remains nullptr
		if ( vertex_p->reverse_attr .core == vertex_stop )  // completed loop
		{	that->last_covertex = vertex_stop;  return; }
		that->first_cosegment = new_seg .core;                                      }           }

// the above could get simpler and faster by using a method like
// inline Cell::Core * Mesh::Core::cell_behind_ptr
// ( const Cell::Core * face, const tag::MayNotExist & ) const
// which is not difficult to implement

}  // end of  namespace tag::local_functions::iterator
	
//------------------------------------------------------------------------------------------------------//


inline Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoSegments::NormalOrder::NormalOrder
( Mesh::Core * msh, Cell::Positive * cen )
:	Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoSegments ( msh, cen )
// msh_p { msh }, center { cen }

// we don't know whether we are in the interior or on the boundary of the mesh
// so we must search for this->first_covertex, rotating backwards
	
{	assert ( this->msh_p );
	assert ( cen );
	assert ( cen->get_dim() + 3 == this->msh_p->get_dim_plus_one() );
	this->center = cen;
	this->first_cosegment = tag::local_functions::iterator::find_first_coseg ( cen, this->msh_p );
	assert ( this->first_cosegment );

	// now we rotate backwards until we meet the boundary or close the loop
	tag::local_functions::iterator::rotate_backwards_for_seg ( this );
	assert ( this->first_cosegment );                                                              }
	

inline Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoSegments::ReverseOrder::ReverseOrder
( Mesh::Core * msh, Cell::Positive * cen )
:	Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoSegments ( msh, cen )
// msh_p { msh }, center { cen }

// we don't know whether we are in the interior or on the boundary of the mesh
// so we must search for this->first_covertex, rotating forward
	
{	assert ( this->msh_p );
	assert ( cen );
	assert ( cen->get_dim() + 3 == this->msh_p->get_dim_plus_one() );
	this->center = cen;
	this->first_cosegment = tag::local_functions::iterator::find_first_coseg ( cen, this->msh_p );
	assert ( this->first_cosegment );

	// now we rotate backwards until we meet the boundary or close the loop
	tag::local_functions::iterator::rotate_forward_for_seg ( this );
	assert ( this->first_cosegment );                                                              }
 	
//------------------------------------------------------------------------------------------------------//
//------------------------------------------------------------------------------------------------------//


class Mesh::Iterator::Around::OneCell::OfCodimTwo::WorkAround2D::NormalOrder::BuildReverseCells
:	public Mesh::Iterator::Around::OneCell::OfCodimTwo
              ::OverCoVertices::NormalOrder::BuildReverseCells::AsFound

// this is a quick and ugly workaround for a specific need
// we are in a 2D mesh and we want all vertices around a given vertex
// so we inherit from ::OverCoVertices:: - which produces segments !
// and just override 'deref' to return the reversed base of the segment

{	public :

	// attributes inherited from Mesh::Iterator::Around::OneCell :
	// Mesh::Core * msh_p
	// Cell::Positive * center
	
	// attribute inherited from Mesh::Iterator::Around::OneCell::OfCodimTwo::Ordered :
	// Cell::Core * current_cosegment { nullptr } 
	
	// attributes inherited from Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoVertices :
	// Cell::Core * current_covertex { nullptr }
	// Cell::Core * first_covertex { nullptr }
	// Cell::Core * last_cosegment { nullptr }

	inline BuildReverseCells ( Mesh::Core * msh, Cell::Positive * const c )
	:	Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoVertices
		    ::NormalOrder::BuildReverseCells::AsFound ( msh, c )  // msh_p { msh }, center { c }
	{	}
	
	inline BuildReverseCells ( const Mesh::Iterator::Around::OneCell::OfCodimTwo
	                                     ::WorkAround2D::NormalOrder::BuildReverseCells & it )
	:	Mesh::Iterator::Around::OneCell::OfCodimTwo
		    ::OverCoVertices::NormalOrder::BuildReverseCells::AsFound ( it )
		// msh_p { it .msh_p }, center { it .center }, current_cosegment { it .current_cosegment },
		// current_covertex { it .current_covertex }, first_covertex { it .first_covertex },
		// last_cosegment { it .last_cosegment }
	{	}

	// void reset ( )  virtual,
	//   defined by Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoVertices::NormalOrder
	// void reset ( const tag::StartAt &, Cell::Core * cll )
	// virtual, defined by Mesh::Iterator::Around::OneCell::OfCodimTwo
	//            ::OverCoVertices::NormalOrder::BuildReverseCells::AsFound

	// void advance ( )  virtual from Mesh::Iterator::Core, defined by
	//   Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoVertices::NormalOrder

	// bool in_range ( )  virtual, defined by Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoVertices

	Cell deref ( ) override;  // virtual from Mesh::Iterator::Core,
	// defined by Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoVertices::NormalOrder::
	// ::BuildReverseCells::AsFound, here overriden to return the reversed base of the segment

	Mesh::Iterator::Core * clone ( );  // virtual from Mesh::Iterator::Core

};  // end of class Mesh::Iterator::Around::OneCell::OfCodimTwo
    //               ::WorkAround2D::NormalOrder::BuildReverseCells


class Mesh::Iterator::Around::OneCell::OfCodimTwo::WorkAround2D::ReverseOrder::BuildReverseCells
:	public Mesh::Iterator::Around::OneCell::OfCodimTwo
              ::OverCoVertices::ReverseOrder::BuildReverseCells::AsFound

// this is a quick and ugly workaround for a specific need
// we are in a 2D mesh and we want all vertices around a given vertex
// so we inherit from ::OverCoVertices:: - which produces segments !
// and just override 'deref' to return the reversed base of the segment

{	public :

	// attributes inherited from Mesh::Iterator::Around::OneCell :
	// Mesh::Core * msh_p
	// Cell::Positive * center
	
	// attribute inherited from Mesh::Iterator::Around::OneCell::OfCodimTwo::Ordered :
	// Cell::Core * current_cosegment { nullptr } 
	
	// attributes inherited from Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoVertices :
	// Cell::Core * current_covertex { nullptr }
	// Cell::Core * first_covertex { nullptr }
	// Cell::Core * last_cosegment { nullptr }

	inline BuildReverseCells ( Mesh::Core * msh, Cell::Positive * const c )
	:	Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoVertices
		    ::ReverseOrder::BuildReverseCells::AsFound ( msh, c )  // msh_p { msh }, center { c }
	{	}
	
	inline BuildReverseCells ( const Mesh::Iterator::Around::OneCell::OfCodimTwo
	                                     ::WorkAround2D::ReverseOrder::BuildReverseCells & it )
	:	Mesh::Iterator::Around::OneCell::OfCodimTwo
		    ::OverCoVertices::ReverseOrder::BuildReverseCells::AsFound ( it )
		// msh_p { it .msh_p }, center { it .center }, current_cosegment { it .current_cosegment },
		// current_covertex { it .current_covertex }, first_covertex { it .first_covertex },
		// last_cosegment { it .last_cosegment }
	{	}

	// void reset ( )  virtual,
	//   defined by Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoVertices::ReverseOrder
	// void reset ( const tag::StartAt &, Cell::Core * cll )
	// virtual, defined by Mesh::Iterator::Around::OneCell::OfCodimTwo
	//            ::OverCoVertices::ReverseOrder::BuildReverseCells::AsFound

	// void advance ( )  virtual from Mesh::Iterator::Core, defined by
	//   Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoVertices::ReverseOrder::BuildReverseCells

	// bool in_range ( )  virtual, defined by Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoVertices

	Cell deref ( ) override;  // virtual from Mesh::Iterator::Core,
	// defined by Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoVertices::ReverseOrder::
	// ::BuildReverseCells::AsFound, here overriden to return the reversed base of the segment

	Mesh::Iterator::Core * clone ( );  // virtual from Mesh::Iterator::Core

};  // end of class Mesh::Iterator::Around::OneCell::OfCodimTwo
    //               ::WorkAround2D::ReverseOrder::BuildReverseCells

//------------------------------------------------------------------------------------------------------//
//------------------------------------------------------------------------------------------------------//

class Mesh::Iterator::Around::OneCell::OfCodimAtLeastTwo
:	public Mesh::Iterator::Around::OneCell

// abstract class, classes below inherit from it
// Mesh::Iterator::Around::OneCell::OfCodimAtLeastTwo::ReturnCellsOfDimPlusOne
// Mesh::Iterator::Around::OneCell::OfCodimAtLeastTwo::ReturnCellsOfHighDim::AsFound
// Mesh::Iterator::Around::OneCell::OfCodimAtLeastTwo::ReturnCellsOfMaxDim

// center->dim <= msh->dim - 2, no order assumed
// more often than not, center->dim < msh->dim - 2 (otherwise, an ordered iterator could be used)
// more often than not, center->dim == 0, msh->dim == 3

// exception : used in frontal.cpp for running over all tetrahedra around a given segment
// because, during frontal mesh generation, tetrahedra may touch at a segment
// thus compromising the order

{	public :

	// from Mesh::Iterator::Around::OneCell :
	// typedef std::map < Mesh::Core*, Cell::field_to_meshes > maptype
	// center->meshes [ dim ] : short int counter_pos, counter_neg, std::list<Cell>::iterator where
	// typedef std::map < Mesh::Core*, Cell::field_to_meshes_same_dim > maptype_same_dim
	// center->meshes_same_dim : short int sign, std::list<Cell>::iterator where

	// attributes inherited from Mesh::Iterator::Around::OneCell :
	// Mesh::Core * msh_p
	// Cell::Positive * center
	
	inline OfCodimAtLeastTwo ( Mesh::Core * msh, Cell::Positive * const c )
	:	Mesh::Iterator::Around::OneCell ( msh, c )  // msh_p { msh }, center { c }
	{	}
	
	inline OfCodimAtLeastTwo ( const Mesh::Iterator::Around::OneCell::OfCodimAtLeastTwo & it )
	:	Mesh::Iterator::Around::OneCell ( it )
		// msh_p { it .msh_p }, center { it .center }
	{	}
	
	// void reset ( )  stays pure  virtual from Mesh::Iterator::Core
	
	void reset ( const tag::StartAt &, Cell::Core * cll );  //  virtual from Mesh::Iterator::Core
	// here execution forbidden
	
	// void advance ( )  stays pure virtual from Mesh::Iterator::Core
	// bool in_range ( )  stays pure virtual from Mesh::Iterator::Core
	// Cell deref ( )  stays pure virtual from Mesh::Iterator::Core

	// Mesh::Iterator::Core * clone ( )  stays pure virtual from Mesh::Iterator::Core

	class ReturnCellsOfDimPlusOne;
	struct ReturnCellsOfHighDim  {  class AsFound;  };
	class ReturnCellsOfMaxDim;
	
};  // end of class Mesh::Iterator::Around::OneCell::OfCodimAtLeastTwo

//------------------------------------------------------------------------------------------------------//


class Mesh::Iterator::Around::OneVertex::ReturnSegments
:	public Mesh::Iterator::Around::OneCell::OfCodimAtLeastTwo

// abstract class, specialized in
// class Mesh::Iterator::Around::OneVertex::ReturnSegments::AsFound
// class Mesh::Iterator::OverSegments::WhoseTipIsC
// class Mesh::Iterator::OverSegments::WhoseTipIsC::ReverseEachCell

// center->dim == 0, no order assumed

// more often than not, msh->dim >= 3 (otherwise, an ordered iterator could be used)
// more often than not, center->dim == 0, msh->dim == 3
// exception : a 2D STSI mesh (interface in frontal.cpp)

// for returning higher-dimensional cells, see
// class Mesh::Iterator::Around::OneCell::ReturnCellsOfHighDim::AsFound

{	public :

	// from Mesh::Iterator::Around::OneCell :
	// typedef std::map < Mesh::Core*, Cell::field_to_meshes > maptype
	// center->meshes [ dim ] : short int counter_pos, counter_neg, std::list<Cell>::iterator where
	// typedef std::map < Mesh::Core*, Cell::field_to_meshes_same_dim > maptype_same_dim
	// center->meshes_same_dim : short int sign, std::list<Cell>::iterator where

	typedef std::map < Cell::Positive::Segment*, short int > maptype_segs;

	// attributes inherited from Mesh::Iterator::Around::OneCell :
	// Mesh::Core * msh_p
	// Cell::Positive * center
	
	maptype_segs::iterator iter;  // initialized by 'reset' as center->segments .begin()
	
	inline ReturnSegments ( Mesh::Core * msh, Cell::Positive * const c )
	:	Mesh::Iterator::Around::OneCell::OfCodimAtLeastTwo ( msh, c )
		// msh_p { msh }, center { c }  // 'iter' will be initialized by 'reset'
	{	assert ( c->get_dim() == 0 );  }
	
	inline ReturnSegments ( const Mesh::Iterator::Around::OneVertex::ReturnSegments & it )
	:	Mesh::Iterator::Around::OneCell::OfCodimAtLeastTwo ( it ), iter { it .iter }
		// msh_p { it .msh_p }, center { it .center }
	{	assert ( it .center->get_dim() == 0 );  }

	using Mesh::Iterator::Around::OneCell::OfCodimAtLeastTwo::reset;
	
	void reset ( );  //  virtual from Mesh::Iterator::Core
	
	// void reset ( const tag::StartAt &, Cell::Core * cll )   virtual from Mesh::Iterator::Core
  // defined by Mesh::Iterator::Around::OneCell::OfCodimAtLeastTwo, execution forbidden
	
	inline void further_advance ( );  //  defined in iterator.cpp
	// 'iter' points towards a segment but we need to search further - the segment must belong to msh_p
	
	void advance ( );  // virtual from Mesh::Iterator::Core

	bool in_range ( );  // virtual from Mesh::Iterator::Core

	// Cell deref ( )  stays pure virtual from Mesh::Iterator::Core

	// Mesh::Iterator::Core * clone ( )  stays pure virtual from Mesh::Iterator::Core

	class AsFound;

};  // end of  class Mesh::Iterator::Around::OneVertex::ReturnSegments

//------------------------------------------------------------------------------------------------------//


class Mesh::Iterator::Around::OneVertex::ReturnSegments::AsFound
:	public Mesh::Iterator::Around::OneVertex::ReturnSegments

// center->dim  == 0, no order assumed, orientation indifferent
// returns positive segments (as found in center->segments)
// more often than not, msh->dim >= 3 (otherwise, an ordered iterator could be used)
// however, there are exceptions, e.g. a 2D STSI mesh (interf in frontal.cpp)

{	public :

	// from Mesh::Iterator::Around::OneCell :
	// typedef std::map < Mesh::Core*, Cell::field_to_meshes > maptype
	// center->meshes [ dim ] : short int counter_pos, counter_neg, std::list<Cell>::iterator where
	// typedef std::map < Mesh::Core*, Cell::field_to_meshes_same_dim > maptype_same_dim
	// center->meshes_same_dim : short int sign, std::list<Cell>::iterator where

	// attributes inherited from Mesh::Iterator::Around::OneCell :
	// Mesh::Core * msh_p
	// Cell::Positive * center
	
	// attributes inherited from Mesh::Iterator::Around::OneVertex::ReturnSegments :
	// typedef std::map < Cell::Positive::Segment *, short int > maptype_segs
	// maptype_segs::iterator iter  ( initialized by 'reset' as center->segments .begin() )
	
	inline AsFound ( Mesh::Core * msh, Cell::Positive * const c )
	:	Mesh::Iterator::Around::OneVertex::ReturnSegments ( msh, c )
	// msh_p { msh }, center { c }  // 'iter' will be initialized by 'reset'
	{	}
	
	inline AsFound ( const Mesh::Iterator::Around::OneVertex::ReturnSegments::AsFound & it )
	:	Mesh::Iterator::Around::OneVertex::ReturnSegments ( it )
		// msh_p { it .msh_p }, center { it .center }, iter { it .iter }
	{	}
	
	// void reset ( )  virtual from Mesh::Iterator::Core
	//   defined by Mesh::Iterator::Around::OneVertex::ReturnSegments
	// void reset ( const tag::StartAt &, Cell::Core * cll )   virtual from Mesh::Iterator::Core
  //   defined by Mesh::Iterator::Around::OneCell::OfCodimAtLeastTwo, execution forbidden
	// void advance ( )   virtual from Mesh::Iterator::Core
	//   defined by Mesh::Iterator::Around::OneVertex::ReturnSegments
	// bool in_range ( )  virtual from Mesh::Iterator::Core
	//   defined by Mesh::Iterator::Around::OneVertex::ReturnSegments

	Cell deref ( );  // virtual from Mesh::Iterator::Core

	Mesh::Iterator::Core * clone ( );  // virtual from Mesh::Iterator::Core

};  // end of  class Mesh::Iterator::Around::OneVertex::ReturnSegments::AsFound

//------------------------------------------------------------------------------------------------------//


class Mesh::Iterator::OverSegments::WhoseTipIsC
:	public Mesh::Iterator::Around::OneVertex::ReturnSegments

// center->dim  == 0, no order assumed
// more often than not, msh->dim >= 3 (otherwise, an ordered iterator could be used)
// however, there are exceptions, e.g. a 2D STSI mesh (interf in frontal.cpp)

{	public :

	// from Mesh::Iterator::Around::OneCell :
	// typedef std::map < Mesh::Core*, Cell::field_to_meshes > maptype
	// center->meshes [ dim ] : short int counter_pos, counter_neg, std::list<Cell>::iterator where
	// typedef std::map < Mesh::Core*, Cell::field_to_meshes_same_dim > maptype_same_dim
	// center->meshes_same_dim : short int sign, std::list<Cell>::iterator where

	// attributes inherited from Mesh::Iterator::Around::OneCell :
	// Mesh::Core * msh_p
	// Cell::Positive * center
	
	// attributes inherited from Mesh::Iterator::Around::OneVertex::ReturnSegments :
	// typedef std::map < Cell::Positive::Segment *, short int > maptype_segs
	// maptype_segs::iterator iter  (initialized by 'reset' as center->segments .begin())
	
	inline WhoseTipIsC ( Mesh::Core * msh, Cell::Positive * const c )
	:	Mesh::Iterator::Around::OneVertex::ReturnSegments ( msh, c )
	// msh_p { msh }, center { c }  // 'iter' will be initialized by 'reset'
	{	}
	
	inline WhoseTipIsC ( const Mesh::Iterator::OverSegments::WhoseTipIsC & it )
	:	Mesh::Iterator::Around::OneVertex::ReturnSegments ( it )
		// msh_p { it .msh_p }, center { it .center }, iter { it .iter }
	{	}
	
	// void reset ( )  virtual from Mesh::Iterator::Core
	//   defined by Mesh::Iterator::Around::OneVertex::ReturnSegments
	// void reset ( const tag::StartAt &, Cell::Core * cll )   virtual from Mesh::Iterator::Core
  //   defined by Mesh::Iterator::Around::OneCell::OfCodimAtLeastTwo, execution forbidden
	// void advance ( )   virtual from Mesh::Iterator::Core
	//   defined by Mesh::Iterator::Around::OneVertex::ReturnSegments
	// bool in_range ( )  virtual from Mesh::Iterator::Core
	//   defined by Mesh::Iterator::Around::OneVertex::ReturnSegments

	Cell deref ( );  // virtual from Mesh::Iterator::Core

	Mesh::Iterator::Core * clone ( );  // virtual from Mesh::Iterator::Core

	class ReverseEachCell;
	
};  // end of  class Mesh::Iterator::OverSegments::WhoseTipIsC

//------------------------------------------------------------------------------------------------------//


class Mesh::Iterator::OverSegments::WhoseTipIsC::ReverseEachCell
:	public Mesh::Iterator::Around::OneVertex::ReturnSegments

// center->dim  == 0, no order assumed
// more often than not, msh->dim >= 3 (otherwise, an ordered iterator could be used)
// however, there are exceptions, e.g. a 2D STSI mesh (interf in frontal.cpp)

{	public :

	// from Mesh::Iterator::Around::OneCell :
	// typedef std::map < Mesh::Core*, Cell::field_to_meshes > maptype
	// center->meshes [ dim ] : short int counter_pos, counter_neg, std::list<Cell>::iterator where
	// typedef std::map < Mesh::Core*, Cell::field_to_meshes_same_dim > maptype_same_dim
	// center->meshes_same_dim : short int sign, std::list<Cell>::iterator where

	// attributes inherited from Mesh::Iterator::Around::OneCell :
	// Mesh::Core * msh_p
	// Cell::Positive * center
	
	// attributes inherited from Mesh::Iterator::Around::OneVertex::ReturnSegments :
	// typedef std::map < Cell::Positive::Segment*, short int > maptype_segs
	// maptype_segs::iterator iter  (initialized by 'reset' as center->segments .begin())
	
	inline ReverseEachCell ( Mesh::Core * msh, Cell::Positive * const c )
	:	Mesh::Iterator::Around::OneVertex::ReturnSegments ( msh, c )
	// msh_p { msh }, center { c }  // 'iter' will be initialized by 'reset'
	{	}
	
	inline ReverseEachCell
	( const Mesh::Iterator::OverSegments::WhoseTipIsC::ReverseEachCell & it )
	:	Mesh::Iterator::Around::OneVertex::ReturnSegments ( it )
		// msh_p { it .msh_p }, center { it .center }, iter { it .iter }
	{	}
	
	// void reset ( )  virtual from Mesh::Iterator::Core
	//   defined by Mesh::Iterator::Around::OneVertex::ReturnSegments
	// void reset ( const tag::StartAt &, Cell::Core * cll )   virtual from Mesh::Iterator::Core
  //   defined by Mesh::Iterator::Around::OneCell::OfCodimAtLeastTwo, execution forbidden
	// void advance ( )   virtual from Mesh::Iterator::Core
	//   defined by Mesh::Iterator::Around::OneVertex::ReturnSegments
	// bool in_range ( )  virtual from Mesh::Iterator::Core
	//   defined by Mesh::Iterator::Around::OneVertex::ReturnSegments

	Cell deref ( );  // virtual from Mesh::Iterator::Core

	Mesh::Iterator::Core * clone ( );  // virtual from Mesh::Iterator::Core

};  // end of  class Mesh::Iterator::OverSegments::WhoseTipIsC::ReverseEachCell

//------------------------------------------------------------------------------------------------------//


class Mesh::Iterator::OverVertices::ConnectedToCenter
:	public Mesh::Iterator::Around::OneVertex::ReturnSegments

// center->dim == 0, no order assumed

// more often than not, msh->dim >= 3 (otherwise, an ordered iterator could be used)
// however, there are exceptions, e.g. a 2D STSI mesh (interf in frontal.cpp)

{	public :

	// from Mesh::Iterator::Around::OneCell :
	// typedef std::map < Mesh::Core*, Cell::field_to_meshes > maptype
	// center->meshes [ dim ] : short int counter_pos, counter_neg, std::list<Cell>::iterator where
	// typedef std::map < Mesh::Core*, Cell::field_to_meshes_same_dim > maptype_same_dim
	// center->meshes_same_dim : short int sign, std::list<Cell>::iterator where

	// attributes inherited from Mesh::Iterator::Around::OneCell :
	// Mesh::Core * msh_p
	// Cell::Positive * center
	
	// attributes inherited from Mesh::Iterator::Around::OneVertex::ReturnSegments :
	// typedef std::map < Cell::Positive::Segment*, short int > maptype_segs
	// maptype_segs::iterator iter  (initialized by 'reset' as center->segments .begin())
	
	inline ConnectedToCenter ( Mesh::Core * msh, Cell::Positive * const c )
	:	Mesh::Iterator::Around::OneVertex::ReturnSegments ( msh, c )
	// msh_p { msh }, center { c }  // 'iter' will be initialized by 'reset'
	{	}
	
	inline ConnectedToCenter ( const Mesh::Iterator::OverVertices::ConnectedToCenter & it )
	:	Mesh::Iterator::Around::OneVertex::ReturnSegments ( it )
		// msh_p { it .msh_p }, center { it .center }, iter { it .iter }
	{	}
	
	// void reset ( )  virtual from Mesh::Iterator::Core
	//   defined by Mesh::Iterator::Around::OneVertex::ReturnSegments
	// void reset ( const tag::StartAt &, Cell::Core * cll )   virtual from Mesh::Iterator::Core
  //   defined by Mesh::Iterator::Around::OneCell::OfCodimAtLeastTwo, execution forbidden
	// void advance ( )   virtual from Mesh::Iterator::Core
	//   defined by Mesh::Iterator::Around::OneVertex::ReturnSegments
	// bool in_range ( )  virtual from Mesh::Iterator::Core
	//   defined by Mesh::Iterator::Around::OneVertex::ReturnSegments

	Cell deref ( );  // virtual from Mesh::Iterator::Core

	Mesh::Iterator::Core * clone ( );  // virtual from Mesh::Iterator::Core

	class Ordered;

};  // end of  class Mesh::Iterator::OverVertices::ConnectedToCenter

//------------------------------------------------------------------------------------------------------//


class Mesh::Iterator::OverVertices::ConnectedToCenter::Ordered
:	public Mesh::Iterator::Around::OneCell::OfCodimTwo
            ::OverCoVertices::NormalOrder::BuildReverseCells::AsFound

// this->dim == 2

// here we use a "codim-two" class which (for 2D meshes) returns segments around a central vertex
// and override 'deref' to get the base

{	public :

	// from Mesh::Iterator::Around::OneCell :
	// typedef std::map < Mesh::Core*, Cell::field_to_meshes > maptype
	// center->meshes [ dim ] : short int counter_pos, counter_neg, std::list<Cell>::iterator where
	// typedef std::map < Mesh::Core*, Cell::field_to_meshes_same_dim > maptype_same_dim
	// center->meshes_same_dim : short int sign, std::list<Cell>::iterator where

	// attributes inherited from Mesh::Iterator::Around::OneCell :
	// Mesh::Core * msh_p
	// Cell::Positive * center
	
	// attribute inherited from Mesh::Iterator::Around::OneCell::OfCodimTwo::Ordered :
	// Cell::Core * current_cosegment { nullptr } 
	
	// attributes inherited from Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoVertices :
	// Cell::Core * current_covertex { nullptr }
	// Cell::Core * first_covertex { nullptr }
	// Cell::Core * last_cosegment { nullptr }

	inline Ordered ( Mesh::Core * msh, Cell::Positive * const c )
	:	Mesh::Iterator::Around::OneCell::OfCodimTwo
	      ::OverCoVertices::NormalOrder::BuildReverseCells::AsFound ( msh, c )
	// msh_p { msh }, center { c }
	{	assert ( msh->get_dim_plus_one() == 3 );  }  // 2D mesh
	
	inline Ordered ( const Mesh::Iterator::OverVertices::ConnectedToCenter::Ordered & it )
	:	Mesh::Iterator::Around::OneCell::OfCodimTwo
	      ::OverCoVertices::NormalOrder::BuildReverseCells::AsFound ( it )
		// msh_p { it .msh_p }, center { it .center }, current_cosegment { it .current_cosegment },
		// current_covertex { it .current_covertex }, first_covertex { it .first_covertex },
		// last_cosegment { it .last_cosegment }
	{	assert ( it .msh_p->get_dim_plus_one() == 3 );  }  // 2D mesh
	
	// void reset ( )  virtual from Mesh::Iterator::Core
	//   defined by Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoVertices::NormalOrder
	// void reset ( const tag::StartAt &, Cell::Core * cll )  virtual from Mesh::Iterator::Core
	//   defined by Mesh::Iterator::Around::OneCell::OfCodimTwo
	//                 ::OverCoVertices::NormalOrder::BuildReverseCells::AsFound
	// void advance ( )  virtual from Mesh::Iterator::Core, defined by
	//   Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoVertices::NormalOrder
	// bool in_range ( )  virtual, defined by Mesh::Iterator::Around::OneCell::OfCodimTwo::OverCoVertices

	Cell deref ( ) override;  // virtual from Mesh::Iterator::Core

	Mesh::Iterator::Core * clone ( );  // virtual from Mesh::Iterator::Core

};  // end of  class Mesh::Iterator::OverVertices::ConnectedToCenter::Ordered

//------------------------------------------------------------------------------------------------------//


class Mesh::Iterator::Around::OneCell::OfCodimAtLeastTwo::ReturnCellsOfDimPlusOne
:	public Mesh::Iterator::Around::OneCell::OfCodimAtLeastTwo

// abstract class, classes below inherit from it
// Mesh::Iterator::Around::OneCell::ReturnCellsOfDimPlusOne::
//   ::{AsFound,OrientCompatWithCenter,OrientOpposToCenter}

// cell_to_return->dim == center->dim + 1, no order
// center->dim >= 1  (i.e. center not a vertex)
// center->dim <= msh->dim - 2  (i.e. center has co-dimension at least two)

// even if center has co-dimension two, do not use neighbourhood relations
// this is important in a STSI mesh (does not matter in a fuzzy mesh)

// for center of co-dimension one,
// use  class Mesh::Iterator::Around::OneCell::OfCodimOne::ReturnCellsOfMaxDim

{	public :

	// from Mesh::Iterator::Around::OneCell :
	// typedef std::map < Mesh::Core*, Cell::field_to_meshes > maptype
	// center->meshes [ dim ] : short int counter_pos, counter_neg, std::list<Cell>::iterator where
	// typedef std::map < Mesh::Core*, Cell::field_to_meshes_same_dim > maptype_same_dim
	// center->meshes_same_dim : short int sign, std::list<Cell>::iterator where

	// attributes inherited from Mesh::Iterator::Around::OneCell :
	// Mesh::Core * msh_p
	// Cell::Positive * center

	maptype_same_dim::iterator iter;  // initialized by 'reset' as center->meshes_same_dim .begin()

	size_t index_diff_msh;  // msh_p->get_dim_plus_one() - 1 - cell_to_return->dim
	
	inline ReturnCellsOfDimPlusOne ( Mesh::Core * msh, Cell::Positive * const c )
	:	Mesh::Iterator::Around::OneCell::OfCodimAtLeastTwo ( msh, c ),
		index_diff_msh { tag::Util::assert_diff ( msh_p->get_dim_plus_one(), center->get_dim() + 2 ) }
		// msh_p { msh }, center { c }  // 'iter' will be initialized by 'reset'
	{ }
	
	inline ReturnCellsOfDimPlusOne
	( const Mesh::Iterator::Around::OneCell::OfCodimAtLeastTwo::ReturnCellsOfDimPlusOne & it )
	:	Mesh::Iterator::Around::OneCell::OfCodimAtLeastTwo ( it ),
		iter { it .iter }, index_diff_msh { it .index_diff_msh }
		// msh_p { it .msh_p }, center { it .center }
	{	}

	using Mesh::Iterator::Around::OneCell::OfCodimAtLeastTwo::reset;
	
	void reset ( );  //  virtual from Mesh::Iterator::Core
	
	// void reset ( const tag::StartAt &, Cell::Core * cll )   virtual from Mesh::Iterator::Core
  // defined by Mesh::Iterator::Around::OneCell::OfCodimAtLeastTwo, execution forbidden
	
	inline void further_advance ( );  //  defined in iterator.cpp
	// 'iter' points towards a mesh, possibly boundary of a cell, but we need to search further
	// the mesh must be the boundary of some cell which in turn must belong to msh_p
	
	void advance ( );  // virtual from Mesh::Iterator::Core

	bool in_range ( );  // virtual from Mesh::Iterator::Core

	// Cell deref ( )  stays pure virtual from Mesh::Iterator::Core

	// Mesh::Iterator::Core * clone ( )  stays pure virtual from Mesh::Iterator::Core

	class AsFound;
	class OrientCompatWithCenter;
	class OrientOpposToCenter;

};  // end of  class Mesh::Iterator::Around::OneCell::OfCodimAtLeastTwo::ReturnCellsOfDimPlusOne

//------------------------------------------------------------------------------------------------------//


class Mesh::Iterator::Around::OneCell::OfCodimAtLeastTwo::ReturnCellsOfDimPlusOne::AsFound
:	public Mesh::Iterator::Around::OneCell::OfCodimAtLeastTwo::ReturnCellsOfDimPlusOne

// cell_to_return->dim == center->dim + 1, no order
// center->dim >= 1  (i.e. center not a vertex)
// center->dim <= msh->dim - 2  (i.e. center has co-dimension at least two)

{	public :

	// from Mesh::Iterator::Around::OneCell :
	// typedef std::map < Mesh::Core*, Cell::field_to_meshes > maptype
	// center->meshes [ dim ] : short int counter_pos, counter_neg, std::list<Cell>::iterator where
	// typedef std::map < Mesh::Core*, Cell::field_to_meshes_same_dim > maptype_same_dim
	// center->meshes_same_dim : short int sign, std::list<Cell>::iterator where

	// attributes inherited from Mesh::Iterator::Around::OneCell :
	// Mesh::Core * msh_p
	// Cell::Positive * center

	// attributes inh. from Mesh::Iterator::Around::OneCell::OfCodimAtLeastTwo::ReturnCellsOfDimPlusOne :
	// maptype::iterator iter  // initialized by 'reset' as center->meshes_same_dim .begin()
	// size_t index_diff  // msh_p->get_dim_plus_one() - 1 - cell_to_return->dim
	
	inline AsFound ( Mesh::Core * msh, Cell::Positive * const c )
	: Mesh::Iterator::Around::OneCell::OfCodimAtLeastTwo::ReturnCellsOfDimPlusOne ( msh, c )
		// msh_p { msh }, center { c }, index_diff { compute_index_diff () }
		// 'iter' will be initialized by 'reset'
	{ }
	
	inline AsFound
	( const Mesh::Iterator::Around::OneCell::OfCodimAtLeastTwo::ReturnCellsOfDimPlusOne::AsFound & it )
	: Mesh::Iterator::Around::OneCell::OfCodimAtLeastTwo::ReturnCellsOfDimPlusOne ( it )
		// msh_p { it .msh_p }, center { it .center }, iter { it .iter }, index_diff { it .index_diff }
	{	}
	
	// void reset ( )  virtual from Mesh::Iterator::Core
	//   defined by  class Mesh::Iterator::Around::OneCell::OfCodimAtLeastTwo::ReturnCellsOfDimPlusOne
	// void reset ( const tag::StartAt &, Cell::Core * cll )   virtual from Mesh::Iterator::Core
  //   defined by Mesh::Iterator::Around::OneCell::OfCodimAtLeastTwo, execution forbidden
	// void advance ( )  virtual from Mesh::Iterator::Core
	//   defined by  class Mesh::Iterator::Around::OneCell::OfCodimAtLeastTwo::ReturnCellsOfDimPlusOne
	// bool in_range ( )  virtual from Mesh::Iterator::Core
	//   defined by  class Mesh::Iterator::Around::OneCell::OfCodimAtLeastTwo::ReturnCellsOfDimPlusOne

	Cell deref ( );  // virtual from Mesh::Iterator::Core

	Mesh::Iterator::Core * clone ( );  // virtual from Mesh::Iterator::Core

};  // end of  class Mesh::Iterator::Around::OneCell::OfCodimAtLeastTwo::ReturnCellsOfDimPlusOne::AsFound

//------------------------------------------------------------------------------------------------------//


class Mesh::Iterator::Around::OneCell::OfCodimAtLeastTwo::ReturnCellsOfDimPlusOne::OrientCompatWithCenter
:	public Mesh::Iterator::Around::OneCell::OfCodimAtLeastTwo::ReturnCellsOfDimPlusOne

// cell_to_return->dim == center->dim + 1, no order
// center->dim >= 1  (i.e. center not a vertex)
// center->dim <= msh->dim - 2  (i.e. center has co-dimension at least two)

{	public :

	// from Mesh::Iterator::Around::OneCell :
	// typedef std::map < Mesh::Core*, Cell::field_to_meshes > maptype
	// center->meshes [ dim ] : short int counter_pos, counter_neg, std::list<Cell>::iterator where
	// typedef std::map < Mesh::Core*, Cell::field_to_meshes_same_dim > maptype_same_dim
	// center->meshes_same_dim : short int sign, std::list<Cell>::iterator where

	// attributes inherited from Mesh::Iterator::Around::OneCell :
	// Mesh::Core * msh_p
	// Cell::Positive * center

	// attributes inh. from Mesh::Iterator::Around::OneCell::OfCodimAtLeastTwo::ReturnCellsOfDimPlusOne :
	// maptype::iterator iter  // initialized by 'reset' as center->meshes_same_dim .begin()
	// size_t index_diff  // msh_p->get_dim_plus_one() - 1 - cell_to_return->dim
	
	inline OrientCompatWithCenter ( Mesh::Core * msh, Cell::Positive * const c )
	: Mesh::Iterator::Around::OneCell::OfCodimAtLeastTwo::ReturnCellsOfDimPlusOne ( msh, c )
		// msh_p { msh }, center { c }, index_diff { compute_index_diff () }
		// 'iter' will be initialized by 'reset'
	{ }
	
	inline OrientCompatWithCenter ( const Mesh::Iterator::Around::OneCell::
	                              OfCodimAtLeastTwo::ReturnCellsOfDimPlusOne::OrientCompatWithCenter & it )
	: Mesh::Iterator::Around::OneCell::OfCodimAtLeastTwo::ReturnCellsOfDimPlusOne ( it )
		// msh_p { it .msh_p }, center { it .center }, iter { it .iter }, index_diff { it .index_diff }
	{	}
	
	// void reset ( )  virtual from Mesh::Iterator::Core
	//   defined by  class Mesh::Iterator::Around::OneCell::OfCodimAtLeastTwo::ReturnCellsOfDimPlusOne
	// void reset ( const tag::StartAt &, Cell::Core * cll )   virtual from Mesh::Iterator::Core
  //   defined by Mesh::Iterator::Around::OneCell::OfCodimAtLeastTwo, execution forbidden
	// void advance ( )  virtual from Mesh::Iterator::Core
	//   defined by  class Mesh::Iterator::Around::OneCell::OfCodimAtLeastTwo::ReturnCellsOfDimPlusOne
	// bool in_range ( )  virtual from Mesh::Iterator::Core
	//   defined by  class Mesh::Iterator::Around::OneCell::OfCodimAtLeastTwo::ReturnCellsOfDimPlusOne

	Cell deref ( );  // virtual from Mesh::Iterator::Core

	Mesh::Iterator::Core * clone ( );  // virtual from Mesh::Iterator::Core

};  // end of  class Mesh::Iterator::Around::OneCell::
    //               OfCodimAtLeastTwo::ReturnCellsOfDimPlusOne::OrientCompatWithCenter

//------------------------------------------------------------------------------------------------------//


class Mesh::Iterator::Around::OneCell::OfCodimAtLeastTwo::ReturnCellsOfDimPlusOne::OrientOpposToCenter
:	public Mesh::Iterator::Around::OneCell::OfCodimAtLeastTwo::ReturnCellsOfDimPlusOne

// cell_to_return->dim == center->dim + 1, no order
// center->dim >= 1  (i.e. center not a vertex)
// center->dim <= msh->dim - 2  (i.e. center has co-dimension at least two)

{	public :

	// from Mesh::Iterator::Around::OneCell :
	// typedef std::map < Mesh::Core*, Cell::field_to_meshes > maptype
	// center->meshes [ dim ] : short int counter_pos, counter_neg, std::list<Cell>::iterator where
	// typedef std::map < Mesh::Core*, Cell::field_to_meshes_same_dim > maptype_same_dim
	// center->meshes_same_dim : short int sign, std::list<Cell>::iterator where

	// attributes inherited from Mesh::Iterator::Around::OneCell :
	// Mesh::Core * msh_p
	// Cell::Positive * center

	// attributes inh. from Mesh::Iterator::Around::OneCell::OfCodimAtLeastTwo::ReturnCellsOfDimPlusOne :
	// maptype::iterator iter  // initialized by 'reset' as center->meshes_same_dim .begin()
	// size_t index_diff  // msh_p->get_dim_plus_one() - 1 - cell_to_return->dim
	
	inline OrientOpposToCenter ( Mesh::Core * msh, Cell::Positive * const c )
	: Mesh::Iterator::Around::OneCell::OfCodimAtLeastTwo::ReturnCellsOfDimPlusOne ( msh, c )
		// msh_p { msh }, center { c }, index_diff { compute_index_diff () }
		// 'iter' will be initialized by 'reset'
	{ }
	
	inline OrientOpposToCenter ( const Mesh::Iterator::Around::OneCell::
	                             OfCodimAtLeastTwo::ReturnCellsOfDimPlusOne::OrientOpposToCenter & it )
	: Mesh::Iterator::Around::OneCell::OfCodimAtLeastTwo::ReturnCellsOfDimPlusOne ( it )
		// msh_p { it .msh_p }, center { it .center }, iter { it .iter }, index_diff { it .index_diff }
	{	}
	
	// void reset ( )  virtual from Mesh::Iterator::Core
	//   defined by  class Mesh::Iterator::Around::OneCell::OfCodimAtLeastTwo::ReturnCellsOfDimPlusOne
	// void reset ( const tag::StartAt &, Cell::Core * cll )   virtual from Mesh::Iterator::Core
  //   defined by Mesh::Iterator::Around::OneCell::OfCodimAtLeastTwo, execution forbidden
	// void advance ( )  virtual from Mesh::Iterator::Core
	//   defined by  class Mesh::Iterator::Around::OneCell::OfCodimAtLeastTwo::ReturnCellsOfDimPlusOne
	// bool in_range ( )  virtual from Mesh::Iterator::Core
	//   defined by  class Mesh::Iterator::Around::OneCell::OfCodimAtLeastTwo::ReturnCellsOfDimPlusOne

	Cell deref ( );  // virtual from Mesh::Iterator::Core

	Mesh::Iterator::Core * clone ( );  // virtual from Mesh::Iterator::Core

};  // end of  class Mesh::Iterator::Around::OneCell::
    //               OfCodimAtLeastTwo::ReturnCellsOfDimPlusOne::OrientOpposToCenter

//------------------------------------------------------------------------------------------------------//


class Mesh::Iterator::Around::OneCell::OfCodimAtLeastTwo::ReturnCellsOfHighDim::AsFound
:	public Mesh::Iterator::Around::OneCell::OfCodimAtLeastTwo

// to be tested

// center->dim + 2 <= cell_to_return->dim <= msh->dim - 1, no order

// even if center has co-dimension two, do not use neighbourhood relations
// this is important in a STSI mesh (does not matter in a fuzzy mesh)

// for returning cells of dimension center->dim + 1, use a class derived from
// Mesh::Iterator::Around::OneCell::OfCodimAtLeastTwo::ReturnCellsOfDimPlusOne
// or Mesh::Iterator::Around::OneVertex::ReturnSegments

// for cells of maximum dimension,
// use a class derived from Mesh::Iterator::Around::OneCell::OfCodimOne::ReturnCellsOfMaxDim

{	public :

	// from Mesh::Iterator::Around::OneCell :
	// typedef std::map < Mesh::Core*, Cell::field_to_meshes > maptype
	// center->meshes [ dim ] : short int counter_pos, counter_neg, std::list<Cell>::iterator where
	// typedef std::map < Mesh::Core*, Cell::field_to_meshes_same_dim > maptype_same_dim
	// center->meshes_same_dim : short int sign, std::list<Cell>::iterator where

	// attributes inherited from Mesh::Iterator::Around::OneCell :
	// Mesh::Core * msh_p
	// Cell::Positive * center

	const size_t index_diff_center;  // cell_to_return->dim - 1 -center->dim
	// center->meshes is indexed over the dimension of the __mesh__ minus the dimension of 'center'
	// __mesh__ refers to the boundary of the cell to return

	const size_t index_diff_msh;  // msh_p->dim - cell_to_return->dim
	
	maptype::iterator iter;  // initialized by 'reset' as center->meshes [ index_diff_center ] .begin()
	
	inline AsFound ( Mesh::Core * msh, Cell::Positive * const c, size_t dim )
		// call to constructor provides dim == dimension of cells to return !!!!!
	:	Mesh::Iterator::Around::OneCell::OfCodimAtLeastTwo ( msh, c ),
		// msh_p { msh }, center { c }  // 'iter' will be initialized by 'reset'
		index_diff_center { tag::Util::assert_diff ( dim, c->get_dim() + 1 ) },
		index_diff_msh { tag::Util::assert_diff ( msh_p->get_dim_plus_one(), dim + 1 ) }
	{	assert ( c->get_dim() == 0 );  }
	
	inline AsFound
	( const Mesh::Iterator::Around::OneCell::OfCodimAtLeastTwo::ReturnCellsOfHighDim::AsFound & it )
	:	Mesh::Iterator::Around::OneCell::OfCodimAtLeastTwo ( it ),
		// msh_p { it .msh_p }, center { it .center }
		index_diff_center { it .index_diff_center }, index_diff_msh { it .index_diff_msh },
		iter { it .iter }
	{	assert ( it .center->get_dim() == 0 );  }

	using Mesh::Iterator::Around::OneCell::OfCodimAtLeastTwo::reset;
  
	void reset ( );  //  virtual from Mesh::Iterator::Core
	
	// void reset ( const tag::StartAt &, Cell::Core * cll )   virtual from Mesh::Iterator::Core
  // defined by Mesh::Iterator::Around::OneCell::OfCodimAtLeastTwo, execution forbidden
	
	inline void further_advance ( );  //  defined in iterator.cpp
	// 'iter' points towards a mesh, possibly boundary of a cell, but we need to search further
	// the mesh must be the boundary of some cell which in turn must belong to msh_p
	
	void advance ( );  // virtual from Mesh::Iterator::Core

	bool in_range ( );  // virtual from Mesh::Iterator::Core

	Cell deref ( );  // virtual from Mesh::Iterator::Core

	Mesh::Iterator::Core * clone ( );  // virtual from Mesh::Iterator::Core

};  // end of  class Mesh::Iterator::Around::OneCell::OfCodimAtLeastTwo::ReturnCellsOfHighDim::AsFound

//------------------------------------------------------------------------------------------------------//


class Mesh::Iterator::Around::OneCell::OfCodimAtLeastTwo::ReturnCellsOfMaxDim
:	public Mesh::Iterator::Around::OneCell::OfCodimAtLeastTwo

// abstract class, specialized in
// Mesh::Iterator::Around::OneCell::OfCodimAtLeastTwo::ReturnCellsOfMaxDim
//   ::{AsFound,OrientCompatWithMesh,OrientOpposToMesh}

// center->dim <= msh->dim - 2, no order
// for center of co-dim one, see  class Mesh::Iterator::OverCells::OfMaxDim::Around::OneCell::OfCodimOne

// more often than not, msh->dim >= center->dim + 3 (otherwise, an ordered iterator could be used)
// however, there are exceptions, e.g. in a 2D STSI mesh (interface in frontal.cpp)

{	public :

	// from Mesh::Iterator::Around::OneCell :
	// typedef std::map < Mesh::Core*, Cell::field_to_meshes > maptype
	// center->meshes [ dim ] : short int counter_pos, counter_neg, std::list<Cell>::iterator where
	// typedef std::map < Mesh::Core*, Cell::field_to_meshes_same_dim > maptype_same_dim
	// center->meshes_same_dim : short int sign, std::list<Cell>::iterator where

	// attributes inherited from Mesh::Iterator::Around::OneCell :
	// Mesh::Core * msh_p
	// Cell::Positive * center

	const size_t index_diff_center;
	// cell_to_return->dim - 1 -center->dim == msh->dim - 1 -center->dim
	// center->meshes is indexed over the dimension of the __mesh__ minus the dimension of 'center'
	// __mesh__ refers to the boundary of the cell to return

	maptype::iterator iter;  // initialized by 'reset' as center->meshes [ index_diff_center ] .begin()
	
	inline ReturnCellsOfMaxDim ( Mesh::Core * msh, Cell::Positive * const c )
	:	Mesh::Iterator::Around::OneCell::OfCodimAtLeastTwo ( msh, c ),
		// msh_p { msh }, center { c }  // 'iter' will be initialized by 'reset'
		index_diff_center { tag::Util::assert_diff ( msh_p->get_dim_plus_one(), center->get_dim() + 2 ) }
	{ }
	
	inline ReturnCellsOfMaxDim
	( const Mesh::Iterator::Around::OneCell::OfCodimAtLeastTwo::ReturnCellsOfMaxDim & it )
	:	Mesh::Iterator::Around::OneCell::OfCodimAtLeastTwo ( it ),
		// msh_p { it .msh_p }, center { it .center }
		index_diff_center { it .index_diff_center }, iter { it .iter }
	{	}
	
	using Mesh::Iterator::Around::OneCell::OfCodimAtLeastTwo::reset;
	
	void reset ( );  //  virtual from Mesh::Iterator::Core
	
	// void reset ( const tag::StartAt &, Cell::Core * cll )   virtual from Mesh::Iterator::Core
  // defined by Mesh::Iterator::Around::OneCell::OfCodimAtLeastTwo, execution forbidden
	
	inline void further_advance ( );  //  defined in iterator.cpp
	// 'iter' points towards a mesh, possibly boundary of a cell, but we need to search further
	// the mesh must be the boundary of some cell which in turn must belong to msh_p
	
	void advance ( );  // virtual from Mesh::Iterator::Core

	bool in_range ( );  // virtual from Mesh::Iterator::Core

	// Cell deref ( )  stays pure virtual from Mesh::Iterator::Core

	// Mesh::Iterator::Core * clone ( )  stays pure virtual from Mesh::Iterator::Core

	class OrientCompatWithMesh;
	class OrientOpposToMesh;
	class AsFound;

};  // end of  class Mesh::Iterator::Around::OneCell::OfCodimAtLeastTwo::ReturnCellsOfMaxDim

//------------------------------------------------------------------------------------------------------//


class Mesh::Iterator::Around::OneCell::OfCodimAtLeastTwo::ReturnCellsOfMaxDim::AsFound
:	public Mesh::Iterator::Around::OneCell::OfCodimAtLeastTwo::ReturnCellsOfMaxDim

// center->dim <= msh->dim - 2, no order

{	public :

	// from Mesh::Iterator::Around::OneCell :
	// typedef std::map < Mesh::Core*, Cell::field_to_meshes > maptype
	// center->meshes [ dim ] : short int counter_pos, counter_neg, std::list<Cell>::iterator where
	// typedef std::map < Mesh::Core*, Cell::field_to_meshes_same_dim > maptype_same_dim
	// center->meshes_same_dim : short int sign, std::list<Cell>::iterator where

	// attributes inherited from Mesh::Iterator::Around::OneCell :
	// Mesh::Core * msh_p
	// Cell::Positive * center

	// attributes inherited from Mesh::Iterator::Around::OneCell::OfCodimAtLeastTwo::ReturnCellsOfMaxDim :
	// size_t dim { tag::Util::assert_diff ( msh_p->get_dim_plus_one(), 2 ) }
	// maptype::iterator iter   initialized by 'reset' as center->meshes [ dim ] .begin()
	
	inline AsFound ( Mesh::Core * msh, Cell::Positive * const c )
	:	Mesh::Iterator::Around::OneCell::OfCodimAtLeastTwo::ReturnCellsOfMaxDim ( msh, c )
		// msh_p { msh }, center { c }  // 'iter' will be initialized by 'reset'
	{ }
	
	inline AsFound ( const Mesh::Iterator::Around::OneCell::
	                       OfCodimAtLeastTwo::ReturnCellsOfMaxDim::AsFound & it )
	: Mesh::Iterator::Around::OneCell::OfCodimAtLeastTwo::ReturnCellsOfMaxDim ( it )
		// msh_p { it .msh_p }, center { it .center }, iter { it .iter }
	{	}
	
	// void reset ( )   virtual from Mesh::Iterator::Core
	//   defined by Mesh::Iterator::OverCells::OfMaxDim::Around::OneCell::OfCodimAtLeastTwo  
	// void reset ( const tag::StartAt &, Cell::Core * cll )   virtual from Mesh::Iterator::Core
  //   defined by Mesh::Iterator::Around::OneCell::OfCodimAtLeastTwo, execution forbidden
	// inline void further_advance ( )
	//   defined by Mesh::Iterator::OverCells::OfMaxDim::Around::OneCell::OfCodimAtLeastTwo  
	// void advance ( )   virtual from Mesh::Iterator::Core
	//   defined by Mesh::Iterator::OverCells::OfMaxDim::Around::OneCell::OfCodimAtLeastTwo  
	// bool in_range ( )  virtual from Mesh::Iterator::Core
	//   defined by Mesh::Iterator::OverCells::OfMaxDim::Around::OneCell::OfCodimAtLeastTwo  

	Cell deref ( );  // virtual from Mesh::Iterator::Core

	Mesh::Iterator::Core * clone ( );  // virtual from Mesh::Iterator::Core

};  // end of  class Mesh::Iterator::Around::OneCell::
    //               OfCodimAtLeastTwo::ReturnCellsOfMaxDim::AsFound

//------------------------------------------------------------------------------------------------------//


class Mesh::Iterator::Around::OneCell::OfCodimAtLeastTwo::ReturnCellsOfMaxDim::OrientCompatWithMesh
:	public Mesh::Iterator::Around::OneCell::OfCodimAtLeastTwo::ReturnCellsOfMaxDim

// no order assumed
// center->dim <= msh->dim - 2

{	public :

	// from Mesh::Iterator::Around::OneCell :
	// typedef std::map < Mesh::Core*, Cell::field_to_meshes > maptype
	// center->meshes [ dim ] : short int counter_pos, counter_neg, std::list<Cell>::iterator where
	// typedef std::map < Mesh::Core*, Cell::field_to_meshes_same_dim > maptype_same_dim
	// center->meshes_same_dim : short int sign, std::list<Cell>::iterator where

	// attributes inherited from Mesh::Iterator::Around::OneCell :
	// Mesh::Core * msh_p
	// Cell::Positive * center

	// attributes inherited from Mesh::Iterator::Around::OneCell::OfCodimAtLeastTwo::ReturnCellsOfMaxDim :
	// size_t dim { tag::Util::assert_diff ( msh_p->get_dim_plus_one(), 2 ) }
	// maptype::iterator iter   initialized by 'reset' as center->meshes [ dim ] .begin()
	
	inline OrientCompatWithMesh ( Mesh::Core * msh, Cell::Positive * const c )
	:	Mesh::Iterator::Around::OneCell::OfCodimAtLeastTwo::ReturnCellsOfMaxDim ( msh, c )
		// msh_p { msh }, center { c }  // 'iter' will be initialized by 'reset'
	{ }
	
	inline OrientCompatWithMesh ( const Mesh::Iterator::Around::OneCell::
	                            OfCodimAtLeastTwo::ReturnCellsOfMaxDim::OrientCompatWithMesh & it )
	: Mesh::Iterator::Around::OneCell::OfCodimAtLeastTwo::ReturnCellsOfMaxDim ( it )
		// msh_p { it .msh_p }, center { it .center }, iter { it .iter }
	{	}
	
	// void reset ( )   virtual from Mesh::Iterator::Core
	//   defined by Mesh::Iterator::OverCells::OfMaxDim::Around::OneCell::OfCodimAtLeastTwo  
	// void reset ( const tag::StartAt &, Cell::Core * cll )   virtual from Mesh::Iterator::Core
  //   defined by Mesh::Iterator::Around::OneCell::OfCodimAtLeastTwo, execution forbidden
	// inline void further_advance ( )
	//   defined by Mesh::Iterator::OverCells::OfMaxDim::Around::OneCell::OfCodimAtLeastTwo  
	// void advance ( )   virtual from Mesh::Iterator::Core
	//   defined by Mesh::Iterator::OverCells::OfMaxDim::Around::OneCell::OfCodimAtLeastTwo  
	// bool in_range ( )  virtual from Mesh::Iterator::Core
	//   defined by Mesh::Iterator::OverCells::OfMaxDim::Around::OneCell::OfCodimAtLeastTwo  

	Cell deref ( );  // virtual from Mesh::Iterator::Core

	Mesh::Iterator::Core * clone ( );  // virtual from Mesh::Iterator::Core

};  // end of  class Mesh::Iterator::Around::OneCell::
    //               OfCodimAtLeastTwo::ReturnCellsOfMaxDim::OrientCompatWithMesh

//------------------------------------------------------------------------------------------------------//


class Mesh::Iterator::Around::OneCell::OfCodimAtLeastTwo::ReturnCellsOfMaxDim::OrientOpposToMesh
:	public Mesh::Iterator::Around::OneCell::OfCodimAtLeastTwo::ReturnCellsOfMaxDim

// no order assumed
// center->dim <= msh->dim - 2

{	public :

	// from Mesh::Iterator::Around::OneCell :
	// typedef std::map < Mesh::Core*, Cell::field_to_meshes > maptype
	// center->meshes [ dim ] : short int counter_pos, counter_neg, std::list<Cell>::iterator where
	// typedef std::map < Mesh::Core*, Cell::field_to_meshes_same_dim > maptype_same_dim
	// center->meshes_same_dim : short int sign, std::list<Cell>::iterator where

	// attributes inherited from Mesh::Iterator::Around::OneCell :
	// Mesh::Core * msh_p
	// Cell::Positive * center

	// attributes inherited from Mesh::Iterator::Around::OneCell::OfCodimAtLeastTwo::ReturnCellsOfMaxDim :
	// size_t dim { tag::Util::assert_diff ( msh_p->get_dim_plus_one(), 2 ) }
	// maptype::iterator iter   initialized by 'reset' as center->meshes [ dim ] .begin()
	
	inline OrientOpposToMesh ( Mesh::Core * msh, Cell::Positive * const c )
	:	Mesh::Iterator::Around::OneCell::OfCodimAtLeastTwo::ReturnCellsOfMaxDim ( msh, c )
		// msh_p { msh }, center { c }  // 'iter' will be initialized by 'reset'
	{ }
	
	inline OrientOpposToMesh ( const Mesh::Iterator::Around::OneCell::
	                           OfCodimAtLeastTwo::ReturnCellsOfMaxDim::OrientOpposToMesh & it )
	: Mesh::Iterator::Around::OneCell::OfCodimAtLeastTwo::ReturnCellsOfMaxDim ( it )
		// msh_p { it .msh_p }, center { it .center }, iter { it .iter }
	{	}
	
	// void reset ( )   virtual from Mesh::Iterator::Core
	//   defined by Mesh::Iterator::OverCells::OfMaxDim::Around::OneCell::OfCodimAtLeastTwo  
	// void reset ( const tag::StartAt &, Cell::Core * cll )   virtual from Mesh::Iterator::Core
  //   defined by Mesh::Iterator::Around::OneCell::OfCodimAtLeastTwo, execution forbidden
	// inline void further_advance ( )
	//   defined by Mesh::Iterator::OverCells::OfMaxDim::Around::OneCell::OfCodimAtLeastTwo  
	// void advance ( )   virtual from Mesh::Iterator::Core
	//   defined by Mesh::Iterator::OverCells::OfMaxDim::Around::OneCell::OfCodimAtLeastTwo  
	// bool in_range ( )  virtual from Mesh::Iterator::Core
	//   defined by Mesh::Iterator::OverCells::OfMaxDim::Around::OneCell::OfCodimAtLeastTwo  

	Cell deref ( );  // virtual from Mesh::Iterator::Core

	Mesh::Iterator::Core * clone ( );  // virtual from Mesh::Iterator::Core

};  // end of  class Mesh::Iterator::Around::OneCell::
    //               OfCodimAtLeastTwo::ReturnCellsOfMaxDim::OrientOpposToMesh

//------------------------------------------------------------------------------------------------------//


class Mesh::Iterator::Around::OneCell::OfCodimOne::ReturnCellsOfMaxDim
:	public Mesh::Iterator::Around::OneCell

// abstract class, specialized in
// Mesh::Iterator::Around::OneCell::OfCodimOne::ReturnCellsOfMaxDim
//   ::{AsFound,OrientCompatWithMesh,OrientOpposToMesh}

// center->dim == msh->dim - 1 (co-dimension one), no order
// for center of lower dim (co-dimension two or more),
// use  class Mesh::Iterator::Around::OneCell::ReturnCellsOfDimPlusOne

// usually, it is preferable to use methods 'cell_behind' and 'cell_in_front_of'
// however, there are exceptions, e.g. in a 2D STSI mesh (interface in frontal.cpp)

{	public :

	// from Mesh::Iterator::Around::OneCell :
	// typedef std::map < Mesh::Core*, Cell::field_to_meshes > maptype
	// center->meshes [ dim ] : short int counter_pos, counter_neg, std::list<Cell>::iterator where
	// typedef std::map < Mesh::Core*, Cell::field_to_meshes_same_dim > maptype_same_dim
	// center->meshes_same_dim : short int sign, std::list<Cell>::iterator where

	// attributes inherited from Mesh::Iterator::Around::OneCell :
	// Mesh::Core * msh_p
	// Cell::Positive * center

	maptype_same_dim::iterator iter;  // initialized by 'reset' as center->meshes_same_dim .begin()
	
	inline ReturnCellsOfMaxDim ( Mesh::Core * msh, Cell::Positive * const c )
	: Mesh::Iterator::Around::OneCell ( msh, c )
		// msh_p { msh }, center { c }  // 'iter' will be initialized by 'reset'
	{ }
	
	inline ReturnCellsOfMaxDim
	( const Mesh::Iterator::Around::OneCell::OfCodimOne::ReturnCellsOfMaxDim & it )
	: Mesh::Iterator::Around::OneCell ( it ), iter { it .iter }
		// msh_p { it .msh_p }, center { it .center }
	{	}
	
	void reset ( );  //  virtual from Mesh::Iterator::Core
	
	void reset ( const tag::StartAt &, Cell::Core * cll );  //  virtual from Mesh::Iterator::Core
	// here execution forbidden
	
	inline void further_advance ( );  //  defined in iterator.cpp
	// 'iter' points towards a mesh, possibly boundary of a cell, but we need to search further
	// the mesh must be the boundary of some cell which in turn must belong to msh_p
	
	void advance ( );  // virtual from Mesh::Iterator::Core

	bool in_range ( );  // virtual from Mesh::Iterator::Core

	// Cell deref ( )  stays pure virtual from Mesh::Iterator::Core

	// Mesh::Iterator::Core * clone ( )  stays pure virtual from Mesh::Iterator::Core

	class AsFound;
	class OrientCompatWithMesh;
	class OrientOpposToMesh;
	class OrientCompatWithCenter;
	class OrientOpposToCenter;

};  // end of  class Mesh::Iterator::Around::OneCell::OfCodimOne::ReturnCellsOfMaxDim

//------------------------------------------------------------------------------------------------------//


class Mesh::Iterator::Around::OneCell::OfCodimOne::ReturnCellsOfMaxDim::AsFound
:	public Mesh::Iterator::Around::OneCell::OfCodimOne::ReturnCellsOfMaxDim

// center->dim == msh->dim - 1, no order
// cell_to_return->dim == center->dim + 1 == msh->dim

{	public :

	// from Mesh::Iterator::Around::OneCell :
	// typedef std::map < Mesh::Core*, Cell::field_to_meshes > maptype
	// center->meshes [ dim ] : short int counter_pos, counter_neg, std::list<Cell>::iterator where
	// typedef std::map < Mesh::Core*, Cell::field_to_meshes_same_dim > maptype_same_dim
	// center->meshes_same_dim : short int sign, std::list<Cell>::iterator where

	// attributes inherited from Mesh::Iterator::Around::OneCell :
	// Mesh::Core * msh_p
	// Cell::Positive * center

	// attributes inherited from Mesh::Iterator::Around::OneCell::ReturnCellsOfDimPlusOne :
	// maptype::iterator iter  // initialized by 'reset' as center->meshes_same_dim .begin()
	
	inline AsFound ( Mesh::Core * msh, Cell::Positive * const c )
	: Mesh::Iterator::Around::OneCell::OfCodimOne::ReturnCellsOfMaxDim ( msh, c )
		// msh_p { msh }, center { c }  // 'iter' will be initialized by 'reset'
	{ }
	
	inline AsFound
	( const Mesh::Iterator::Around::OneCell::OfCodimOne::ReturnCellsOfMaxDim::AsFound & it )
	:	Mesh::Iterator::Around::OneCell::OfCodimOne::ReturnCellsOfMaxDim ( it )
		// msh_p { it .msh_p }, center { it .center }, iter { it .iter }
	{	}
	
	// void reset ( )  virtual from Mesh::Iterator::Core
	//   defined by   class Mesh::Iterator::Around::OneCell::ReturnCellsOfDimPlusOne
	// void reset ( const tag::StartAt &, Cell::Core * cll )   virtual from Mesh::Iterator::Core
  //   defined by Mesh::Iterator::Around::OneCell::OfCodimAtLeastTwo, execution forbidden
	// void advance ( )  virtual from Mesh::Iterator::Core
	//   defined by   class Mesh::Iterator::Around::OneCell::ReturnCellsOfDimPlusOne
	// bool in_range ( )  virtual from Mesh::Iterator::Core
	//   defined by   class Mesh::Iterator::Around::OneCell::ReturnCellsOfDimPlusOne

	Cell deref ( );  // virtual from Mesh::Iterator::Core

	Mesh::Iterator::Core * clone ( );  // virtual from Mesh::Iterator::Core

};  // end of  class Mesh::Iterator::Around::OneCell::OfCodimOne::ReturnCellsOfMaxDim::AsFound

//------------------------------------------------------------------------------------------------------//


class Mesh::Iterator::Around::OneCell::OfCodimOne::ReturnCellsOfMaxDim::OrientCompatWithMesh
:	public Mesh::Iterator::Around::OneCell::OfCodimOne::ReturnCellsOfMaxDim

// center->dim == msh->dim - 1, no order
// cell_to_return->dim == center->dim + 1 == msh->dim

{	public :

	// from Mesh::Iterator::Around::OneCell :
	// typedef std::map < Mesh::Core*, Cell::field_to_meshes > maptype
	// center->meshes [ dim ] : short int counter_pos, counter_neg, std::list<Cell>::iterator where
	// typedef std::map < Mesh::Core*, Cell::field_to_meshes_same_dim > maptype_same_dim
	// center->meshes_same_dim : short int sign, std::list<Cell>::iterator where

	// attributes inherited from Mesh::Iterator::Around::OneCell :
	// Mesh::Core * msh_p
	// Cell::Positive * center

	// attributes inherited from Mesh::Iterator::Around::OneCell::ReturnCellsOfMaxDim :
	// maptype::iterator iter  // initialized by 'reset' as center->meshes_same_dim .begin()
	
	inline OrientCompatWithMesh ( Mesh::Core * msh, Cell::Positive * const c )
	: Mesh::Iterator::Around::OneCell::OfCodimOne::ReturnCellsOfMaxDim ( msh, c )
		// msh_p { msh }, center { c }  // 'iter' will be initialized by 'reset'
	{ }
	
	inline OrientCompatWithMesh
	( const Mesh::Iterator::Around::OneCell::OfCodimOne::ReturnCellsOfMaxDim::OrientCompatWithMesh & it )
	: Mesh::Iterator::Around::OneCell::OfCodimOne::ReturnCellsOfMaxDim ( it )
		// msh_p { it .msh_p }, center { it .center }, iter { it .iter }
	{	}
	
	// void reset ( )  virtual from Mesh::Iterator::Core
	//   defined by   class Mesh::Iterator::Around::OneCell::ReturnCellsOfDimPlusOne
	// void reset ( const tag::StartAt &, Cell::Core * cll )   virtual from Mesh::Iterator::Core
  //   defined by Mesh::Iterator::Around::OneCell::OfCodimAtLeastTwo, execution forbidden
	// void advance ( )  virtual from Mesh::Iterator::Core
	//   defined by   class Mesh::Iterator::Around::OneCell::ReturnCellsOfDimPlusOne
	// bool in_range ( )  virtual from Mesh::Iterator::Core
	//   defined by   class Mesh::Iterator::Around::OneCell::ReturnCellsOfDimPlusOne

	Cell deref ( );  // virtual from Mesh::Iterator::Core

	Mesh::Iterator::Core * clone ( );  // virtual from Mesh::Iterator::Core

};  // end of  class Mesh::Iterator::Around::OneCell::OfCodimOne::ReturnCellsOfMaxDim::OrientCompatWithMesh

//------------------------------------------------------------------------------------------------------//


class Mesh::Iterator::Around::OneCell::OfCodimOne::ReturnCellsOfMaxDim::OrientOpposToMesh
:	public Mesh::Iterator::Around::OneCell::OfCodimOne::ReturnCellsOfMaxDim

// center->dim == msh->dim - 1, no order
// cell_to_return->dim == center->dim + 1 == msh->dim

{	public :

	// from Mesh::Iterator::Around::OneCell :
	// typedef std::map < Mesh::Core*, Cell::field_to_meshes > maptype
	// center->meshes [ dim ] : short int counter_pos, counter_neg, std::list<Cell>::iterator where
	// typedef std::map < Mesh::Core*, Cell::field_to_meshes_same_dim > maptype_same_dim
	// center->meshes_same_dim : short int sign, std::list<Cell>::iterator where

	// attributes inherited from Mesh::Iterator::Around::OneCell :
	// Mesh::Core * msh_p
	// Cell::Positive * center

	// attributes inherited from Mesh::Iterator::Around::OneCell::ReturnCellsOfDimPlusOne :
	// maptype::iterator iter  // initialized by 'reset' as center->meshes_same_dim .begin()
	
	inline OrientOpposToMesh ( Mesh::Core * msh, Cell::Positive * const c )
	: Mesh::Iterator::Around::OneCell::OfCodimOne::ReturnCellsOfMaxDim ( msh, c )
		// msh_p { msh }, center { c }  // 'iter' will be initialized by 'reset'
	{ }
	
	inline OrientOpposToMesh
	( const Mesh::Iterator::Around::OneCell::OfCodimOne::ReturnCellsOfMaxDim::OrientOpposToMesh & it )
	: Mesh::Iterator::Around::OneCell::OfCodimOne::ReturnCellsOfMaxDim ( it )
		// msh_p { it .msh_p }, center { it .center }, iter { it .iter }
	{	}
	
	// void reset ( )  virtual from Mesh::Iterator::Core
	//   defined by   class Mesh::Iterator::Around::OneCell::ReturnCellsOfDimPlusOne
	// void reset ( const tag::StartAt &, Cell::Core * cll )   virtual from Mesh::Iterator::Core
  //   defined by Mesh::Iterator::Around::OneCell::OfCodimAtLeastTwo, execution forbidden
	// void advance ( )  virtual from Mesh::Iterator::Core
	//   defined by   class Mesh::Iterator::Around::OneCell::ReturnCellsOfDimPlusOne
	// bool in_range ( )  virtual from Mesh::Iterator::Core
	//   defined by   class Mesh::Iterator::Around::OneCell::ReturnCellsOfDimPlusOne

	Cell deref ( );  // virtual from Mesh::Iterator::Core

	Mesh::Iterator::Core * clone ( );  // virtual from Mesh::Iterator::Core

};  // end of  class Mesh::Iterator::Around::OneCell::OfCodimOne::ReturnCellsOfMaxDim::OrientOpposToMesh

//------------------------------------------------------------------------------------------------------//


class Mesh::Iterator::Around::OneCell::OfCodimOne::ReturnCellsOfMaxDim::OrientCompatWithCenter
:	public Mesh::Iterator::Around::OneCell::OfCodimOne::ReturnCellsOfMaxDim

// center->dim == msh->dim - 1, no order
// cell_to_return->dim == center->dim + 1 == msh->dim

{	public :

	// from Mesh::Iterator::Around::OneCell :
	// typedef std::map < Mesh::Core*, Cell::field_to_meshes > maptype
	// center->meshes [ dim ] : short int counter_pos, counter_neg, std::list<Cell>::iterator where
	// typedef std::map < Mesh::Core*, Cell::field_to_meshes_same_dim > maptype_same_dim
	// center->meshes_same_dim : short int sign, std::list<Cell>::iterator where

	// attributes inherited from Mesh::Iterator::Around::OneCell :
	// Mesh::Core * msh_p
	// Cell::Positive * center

	// attributes inherited from Mesh::Iterator::Around::OneCell::ReturnCellsOfMaxDim :
	// maptype::iterator iter  // initialized by 'reset' as center->meshes_same_dim .begin()
	
	inline OrientCompatWithCenter ( Mesh::Core * msh, Cell::Positive * const c )
	: Mesh::Iterator::Around::OneCell::OfCodimOne::ReturnCellsOfMaxDim ( msh, c )
		// msh_p { msh }, center { c }  // 'iter' will be initialized by 'reset'
	{ }
	
	inline OrientCompatWithCenter
	( const Mesh::Iterator::Around::OneCell::OfCodimOne::ReturnCellsOfMaxDim::OrientCompatWithCenter & it )
	: Mesh::Iterator::Around::OneCell::OfCodimOne::ReturnCellsOfMaxDim ( it )
		// msh_p { it .msh_p }, center { it .center }, iter { it .iter }
	{	}
	
	// void reset ( )  virtual from Mesh::Iterator::Core
	//   defined by   class Mesh::Iterator::Around::OneCell::ReturnCellsOfDimPlusOne
	// void reset ( const tag::StartAt &, Cell::Core * cll )   virtual from Mesh::Iterator::Core
  //   defined by Mesh::Iterator::Around::OneCell::OfCodimAtLeastTwo, execution forbidden
	// void advance ( )  virtual from Mesh::Iterator::Core
	//   defined by   class Mesh::Iterator::Around::OneCell::ReturnCellsOfDimPlusOne
	// bool in_range ( )  virtual from Mesh::Iterator::Core
	//   defined by   class Mesh::Iterator::Around::OneCell::ReturnCellsOfDimPlusOne

	Cell deref ( );  // virtual from Mesh::Iterator::Core

	Mesh::Iterator::Core * clone ( );  // virtual from Mesh::Iterator::Core

}; // end of  class Mesh::Iterator::Around::OneCell::OfCodimOne::
   //               ReturnCellsOfMaxDim::OrientCompatWithCenter

//------------------------------------------------------------------------------------------------------//


class Mesh::Iterator::Around::OneCell::OfCodimOne::ReturnCellsOfMaxDim::OrientOpposToCenter
:	public Mesh::Iterator::Around::OneCell::OfCodimOne::ReturnCellsOfMaxDim

// center->dim == msh->dim - 1, no order
// cell_to_return->dim == center->dim + 1 == msh->dim

{	public :

	// from Mesh::Iterator::Around::OneCell :
	// typedef std::map < Mesh::Core*, Cell::field_to_meshes > maptype
	// center->meshes [ dim ] : short int counter_pos, counter_neg, std::list<Cell>::iterator where
	// typedef std::map < Mesh::Core*, Cell::field_to_meshes_same_dim > maptype_same_dim
	// center->meshes_same_dim : short int sign, std::list<Cell>::iterator where

	// attributes inherited from Mesh::Iterator::Around::OneCell :
	// Mesh::Core * msh_p
	// Cell::Positive * center

	// attributes inherited from Mesh::Iterator::Around::OneCell::ReturnCellsOfDimPlusOne :
	// maptype::iterator iter  // initialized by 'reset' as center->meshes_same_dim .begin()
	
	inline OrientOpposToCenter ( Mesh::Core * msh, Cell::Positive * const c )
	: Mesh::Iterator::Around::OneCell::OfCodimOne::ReturnCellsOfMaxDim ( msh, c )
		// msh_p { msh }, center { c }  // 'iter' will be initialized by 'reset'
	{ }
	
	inline OrientOpposToCenter
	( const Mesh::Iterator::Around::OneCell::OfCodimOne::ReturnCellsOfMaxDim::OrientOpposToCenter & it )
	: Mesh::Iterator::Around::OneCell::OfCodimOne::ReturnCellsOfMaxDim ( it )
		// msh_p { it .msh_p }, center { it .center }, iter { it .iter }
	{	}
	
	// void reset ( )  virtual from Mesh::Iterator::Core
	//   defined by   class Mesh::Iterator::Around::OneCell::ReturnCellsOfDimPlusOne
	// void reset ( const tag::StartAt &, Cell::Core * cll )   virtual from Mesh::Iterator::Core
  //   defined by Mesh::Iterator::Around::OneCell::OfCodimAtLeastTwo, execution forbidden
	// void advance ( )  virtual from Mesh::Iterator::Core
	//   defined by   class Mesh::Iterator::Around::OneCell::ReturnCellsOfDimPlusOne
	// bool in_range ( )  virtual from Mesh::Iterator::Core
	//   defined by   class Mesh::Iterator::Around::OneCell::ReturnCellsOfDimPlusOne

	Cell deref ( );  // virtual from Mesh::Iterator::Core

	Mesh::Iterator::Core * clone ( );  // virtual from Mesh::Iterator::Core

};  // end of  class Mesh::Iterator::Around::OneCell::OfCodimOne::ReturnCellsOfMaxDim::OrientOpposToCenter

//------------------------------------------------------------------------------------------------------//


class Mesh::Iterator::ReturnOneCell : public Mesh::Iterator::Core

// note : this iterator was used in a previous version of frontal.cpp, commit 23.09.e
//        in the current version it is never used

{	public :

	Cell::Core * the_only_cell;  // an iterator does not keep the cell alive
	bool in_range_attr;

	inline ReturnOneCell ( Cell::Core * cll )
	:	Mesh::Iterator::Core (), the_only_cell { cll }
	{	assert ( cll );  }

	inline ReturnOneCell ( const Mesh::Iterator::ReturnOneCell & it )
	:	Mesh::Iterator::Core (), the_only_cell { it .the_only_cell }
	{	assert ( it .the_only_cell );  }

	void reset ( );  // virtual from Mesh::Iterator::Core

	void reset ( const tag::StartAt &, Cell::Core * cll );
	// virtual from Mesh::Iterator::Core, here execution forbidden

	void advance ( );  // virtual from Mesh::Iterator::Core
	bool in_range ( );  // virtual from Mesh::Iterator::Core
	Cell deref ( );  // virtual from Mesh::Iterator::Core

	Mesh::Iterator::Core * clone ( );  // virtual from Mesh::Iterator::Core
	
};  // end of class Mesh::Iterator::ReturnOneCell

//------------------------------------------------------------------------------------------------------//


class Mesh::Iterator::AlwaysOutOfRange : public Mesh::Iterator::Core

// note : this iterator was used in a previous version of frontal.cpp, commit 23.09.e
//        in the current version it is never used

{	public :

	Cell::Core * the_only_cell;  // an iterator does not keep the cell alive

	inline AlwaysOutOfRange ( )
	:	Mesh::Iterator::Core ()
	{	}

	inline AlwaysOutOfRange ( const Mesh::Iterator::AlwaysOutOfRange & it )
	:	Mesh::Iterator::Core ()
	{	}

	void reset ( );  // virtual from Mesh::Iterator::Core

	void reset ( const tag::StartAt &, Cell::Core * cll );
	// virtual from Mesh::Iterator::Core, here execution forbidden

	void advance ( );  // virtual from Mesh::Iterator::Core
	bool in_range ( );  // virtual from Mesh::Iterator::Core
	Cell deref ( );  // virtual from Mesh::Iterator::Core

	Mesh::Iterator::Core * clone ( );  // virtual from Mesh::Iterator::Core
	
};  // end of class Mesh::Iterator::AlwaysOutOfRange

//------------------------------------------------------------------------------------------------------//
//------------------------------------------------------------------------------------------------------//


class Cell::Iterator::OverMeshesOfSameDim : public Cell::Iterator::Core

// abstract class, specialized in
// class Cell::Iterator::OverMeshesOfSameDim::OrientationCompatWithCell
// class Cell::Iterator::OverMeshesOfSameDim::OrientationOpposToCell
// class Cell::Iterator::OverMeshesOfSameDim::OrientationIrrelevant::ReturnPositiveMesh
// class Cell::Iterator::OverMeshesOfSameDim::PositiveMeshesOnly::***

// iterates over meshes of the same dimension as the cell

// cell is not vertex

{	public :

	const std::map < Mesh::Core*, Cell::field_to_meshes_same_dim > & meshes_same_dim;
	std::map < Mesh::Core*, Cell::field_to_meshes_same_dim > ::const_iterator iter;

	inline OverMeshesOfSameDim ( Cell::PositiveNotVertex * cll )
	:	Cell::Iterator::Core (), meshes_same_dim { cll->meshes_same_dim }
	{	}  // no need to initialize 'iter', 'reset' will do that
	
	inline OverMeshesOfSameDim
	( const Cell::Iterator::OverMeshesOfSameDim & it )
	:	Cell::Iterator::Core (), meshes_same_dim { it .meshes_same_dim }, iter { it .iter }
	{	}
	
	void reset ( );  // virtual from Cell::Iterator::Core
	void advance ( );  // virtual from Cell::Iterator::Core
	bool in_range ( );  // virtual from Cell::Iterator::Core
	// Mesh deref ( )  stays pure virtual from Cell::Iterator::Core

	// Cell::Iterator::Core * clone ( )  stays pure virtual from Cell::Iterator::Core

	class OrientationCompatWithCell;  class OrientationOpposToCell;
	struct OrientationIrrelevant  {  class ReturnPositiveMesh;  };
	class PositiveMeshesOnly;

};  // end of  class Cell::Iterator::OverMeshesOfSameDim

//------------------------------------------------------------------------------------------------------//


class Cell::Iterator::OverMeshesOfSameDim::OrientationCompatWithCell
	: public Cell::Iterator::OverMeshesOfSameDim

// iterates over meshes of the same dimension as the cell, orientation compatible
// may return negative meshes, produced on-the-fly
// beware: for negative meshes, does not build reverse cells

// cell is not vertex

{	public :

	// attributes inherited from Cell::Iterator::OverMeshesOfSameDim :
	// const std::map < Mesh::Core*, Cell::field_to_meshes_same_dim > & meshes_same_dim
	// std::map < Mesh::Core*, Cell::field_to_meshes_same_dim > ::const_iterator iter

	inline OrientationCompatWithCell ( Cell::PositiveNotVertex * cll )
	:	Cell::Iterator::OverMeshesOfSameDim ( cll )
	{	}  // no need to initialize 'iter', 'reset' will do that
	
	inline OrientationCompatWithCell
	( const Cell::Iterator::OverMeshesOfSameDim::OrientationCompatWithCell & it )
	:	Cell::Iterator::OverMeshesOfSameDim ( it )
	{	}
	
	// void reset ( )  virtual from Cell::Iterator::Core, defined in Cell::Iterator::OverMeshesOfSameDim
	// void advance ( )  virtual from Cell::Iterator::Core, defined in Cell::Iterator::OverMeshesOfSameDim
	// bool in_range ( )  virtual from Cell::Iterator::Core, defined in Cell::Iterator::OverMeshesOfSameDim
	Mesh deref ( );  // virtual from Cell::Iterator::Core

	Cell::Iterator::Core * clone ( );  // virtual from Cell::Iterator::Core

};  // end of  class Cell::Iterator::OverMeshesOfSameDim::OrientationCompatWithCell

//------------------------------------------------------------------------------------------------------//


class Cell::Iterator::OverMeshesOfSameDim::OrientationOpposToCell
	: public Cell::Iterator::OverMeshesOfSameDim

// iterates over meshes of the same dimension as the cell, orientation compatible
// may return negative meshes, produced on-the-fly
// beware: for negative meshes, does not build reverse cells

// cell is not vertex

{	public :

	// attributes inherited from Cell::Iterator::OverMeshesOfSameDim :
	// const std::map < Mesh::Core*, Cell::field_to_meshes_same_dim > & meshes_same_dim
	// std::map < Mesh::Core*, Cell::field_to_meshes_same_dim > ::const_iterator iter

	inline OrientationOpposToCell ( Cell::PositiveNotVertex * cll )
	:	Cell::Iterator::OverMeshesOfSameDim ( cll )
	{	}  // no need to initialize 'iter', 'reset' will do that
	
	inline OrientationOpposToCell
	( const Cell::Iterator::OverMeshesOfSameDim::OrientationOpposToCell & it )
	:	Cell::Iterator::OverMeshesOfSameDim ( it )
	{	}
	
	// void reset ( )  virtual from Cell::Iterator::Core, defined in Cell::Iterator::OverMeshesOfSameDim
	// void advance ( )  virtual from Cell::Iterator::Core, defined in Cell::Iterator::OverMeshesOfSameDim
	// bool in_range ( )  virtual from Cell::Iterator::Core, defined in Cell::Iterator::OverMeshesOfSameDim
	Mesh deref ( );  // virtual from Cell::Iterator::Core

	Cell::Iterator::Core * clone ( );  // virtual from Cell::Iterator::Core

};  // end of  class Cell::Iterator::OverMeshesOfSameDim::OrientationOpposToCell

//------------------------------------------------------------------------------------------------------//


class Cell::Iterator::OverMeshesOfSameDim::OrientationIrrelevant::ReturnPositiveMesh
:	public Cell::Iterator::OverMeshesOfSameDim

// cell is not vertex

{	public :

	// attributes inherited from Cell::Iterator::OverMeshesOfSameDim :
	// const std::map < Mesh::Core*, Cell::field_to_meshes_same_dim > & meshes_same_dim
	// std::map < Mesh::Core*, Cell::field_to_meshes_same_dim > ::const_iterator iter

	inline ReturnPositiveMesh ( Cell::PositiveNotVertex * cll )
	:	Cell::Iterator::OverMeshesOfSameDim ( cll )
	{	}  // no need to initialize 'iter', 'reset' will do that
	
	inline ReturnPositiveMesh
	( const Cell::Iterator::OverMeshesOfSameDim::OrientationIrrelevant::ReturnPositiveMesh & it )
	:	Cell::Iterator::OverMeshesOfSameDim ( it )
	{	}
	
	// void reset ( )  virtual from Cell::Iterator::Core, defined in Cell::Iterator::OverMeshesOfSameDim
	// void advance ( )  virtual from Cell::Iterator::Core, defined in Cell::Iterator::OverMeshesOfSameDim
	// bool in_range ( )  virtual from Cell::Iterator::Core, defined in Cell::Iterator::OverMeshesOfSameDim
	Mesh deref ( );  // virtual from Cell::Iterator::Core

	Cell::Iterator::Core * clone ( );  // virtual from Cell::Iterator::Core

};  // end of  class Cell::Iterator::OverMeshesOfSameDim::OrientationIrrelevant::ReturnPositiveMesh

//------------------------------------------------------------------------------------------------------//


class Cell::Iterator::OverMeshesOfSameDim::PositiveMeshesOnly
	: public Cell::Iterator::OverMeshesOfSameDim

// abstract class, specialized in
// class Cell::Iterator::OverMeshesOfSameDim::PositiveMeshesOnly::OrientationCompatWithCell
// class Cell::Iterator::OverMeshesOfSameDim::PositiveMeshesOnly::OrientationOpposToCell

// cell is not vertex

{	public :

	// attributes inherited from Cell::Iterator::OverMeshesOfSameDim :
	// const std::map < Mesh::Core*, Cell::field_to_meshes_same_dim > & meshes_same_dim
	// std::map < Mesh::Core*, Cell::field_to_meshes_same_dim > ::const_iterator iter

	inline PositiveMeshesOnly ( Cell::PositiveNotVertex * cll )
	:	Cell::Iterator::OverMeshesOfSameDim ( cll )
	{	}  // no need to initialize 'iter', 'reset' will do that
	
	inline PositiveMeshesOnly
	( const Cell::Iterator::OverMeshesOfSameDim::PositiveMeshesOnly & it )
	:	Cell::Iterator::OverMeshesOfSameDim ( it )
	{	}
	
	// void reset ( )    virtual from Cell::Iterator::Core, will be overridden
	// void advance ( )  virtual from Cell::Iterator::Core, will be overridden
	// bool in_range ( ) virtual from Cell::Iterator::Core, defined in Cell::Iterator::OverMeshesOfSameDim
	Mesh deref ( );   // virtual from Cell::Iterator::Core

	// Cell::Iterator::Core * clone ( )  stays pure virtual from Cell::Iterator::Core

	class OrientationCompatWithCell;  class OrientationOpposToCell;

};  // end of  class Cell::Iterator::OverMeshesOfSameDim::PositiveMeshesOnly

//------------------------------------------------------------------------------------------------------//


class Cell::Iterator::OverMeshesOfSameDim::PositiveMeshesOnly::OrientationCompatWithCell
	: public Cell::Iterator::OverMeshesOfSameDim::PositiveMeshesOnly

// cell is not vertex

{	public :

	// attributes inherited from Cell::Iterator::OverMeshesOfSameDim :
	// const std::map < Mesh::Core*, Cell::field_to_meshes_same_dim > & meshes_same_dim
	// std::map < Mesh::Core*, Cell::field_to_meshes_same_dim > ::const_iterator iter

	inline OrientationCompatWithCell ( Cell::PositiveNotVertex * cll )
	:	Cell::Iterator::OverMeshesOfSameDim::PositiveMeshesOnly ( cll )
	{	}  // no need to initialize 'iter', 'reset' will do that
	
	inline OrientationCompatWithCell
	( const Cell::Iterator::OverMeshesOfSameDim::PositiveMeshesOnly::OrientationCompatWithCell & it )
	:	Cell::Iterator::OverMeshesOfSameDim::PositiveMeshesOnly ( it )
	{	}
	
	void reset ( ) override;    // virtual from Cell::Iterator::Core
	void advance ( ) override;  // virtual from Cell::Iterator::Core
	// bool in_range ( )  virtual from Cell::Iterator::Core, defined in Cell::Iterator::OverMeshesOfSameDim
	// Mesh deref ( )     virtual from Cell::Iterator::Core, defined in Cell::Iterator::OverMeshesOfSameDim

	Cell::Iterator::Core * clone ( );  // virtual from Cell::Iterator::Core

	inline void further_advance ( );

};  // end of  class Cell::Iterator::OverMeshesOfSameDim::PositiveMeshesOnly::OrientationCompatWithCell

//------------------------------------------------------------------------------------------------------//


class Cell::Iterator::OverMeshesOfSameDim::PositiveMeshesOnly::OrientationOpposToCell
	: public Cell::Iterator::OverMeshesOfSameDim::PositiveMeshesOnly

// cell is not vertex

{	public :

	// attributes inherited from Cell::Iterator::OverMeshesOfSameDim :
	// const std::map < Mesh::Core*, Cell::field_to_meshes_same_dim > & meshes_same_dim
	// std::map < Mesh::Core*, Cell::field_to_meshes_same_dim > ::const_iterator iter

	inline OrientationOpposToCell ( Cell::PositiveNotVertex * cll )
	:	Cell::Iterator::OverMeshesOfSameDim::PositiveMeshesOnly ( cll )
	{	}  // no need to initialize 'iter', 'reset' will do that
	
	inline OrientationOpposToCell
	( const Cell::Iterator::OverMeshesOfSameDim::PositiveMeshesOnly::OrientationOpposToCell & it )
	:	Cell::Iterator::OverMeshesOfSameDim::PositiveMeshesOnly ( it )
	{	}
	
	void reset ( ) override;    // virtual from Cell::Iterator::Core
	void advance ( ) override;  // virtual from Cell::Iterator::Core
	// bool in_range ( )  virtual from Cell::Iterator::Core, defined in Cell::Iterator::OverMeshesOfSameDim
	// Mesh deref ( )     virtual from Cell::Iterator::Core, defined in Cell::Iterator::OverMeshesOfSameDim

	Cell::Iterator::Core * clone ( );  // virtual from Cell::Iterator::Core

	inline void further_advance ( );

};  // end of  class Cell::Iterator::OverMeshesOfSameDim::PositiveMeshesOnly::OrientationOpposToCell

//------------------------------------------------------------------------------------------------------//


class Cell::Iterator::OverMeshesOfGivenDim::OrientationIrrelevant::ReturnPositiveMesh
:	public Cell::Iterator::Core

// return meshes of dimension greater than the dimension of the cell

{	public :

	const std::map < Mesh::Core*, Cell::field_to_meshes > & meshes;
	std::map < Mesh::Core*, Cell::field_to_meshes > ::const_iterator iter;

	inline ReturnPositiveMesh ( Cell::Positive * cll, const tag::MeshesOfDim &, const size_t d )
	:	Cell::Iterator::Core (), meshes { cll->meshes [ tag::Util::assert_diff ( d, cll->get_dim() ) ] }
	{	assert ( d > cll->get_dim() );  }  // otherwise, use tag::meshes_of_same_dim
	// no need to initialize 'iter', 'reset' will do that
	
	inline ReturnPositiveMesh
	( const Cell::Iterator::OverMeshesOfGivenDim::OrientationIrrelevant::ReturnPositiveMesh & it )
	:	Cell::Iterator::Core (), meshes { it .meshes }, iter { it .iter }
	{	}
	
	void reset ( );     // virtual from Cell::Iterator::Core
	void advance ( );   // virtual from Cell::Iterator::Core
	bool in_range ( );  // virtual from Cell::Iterator::Core
	Mesh deref ( );     // virtual from Cell::Iterator::Core

	Cell::Iterator::Core * clone ( );  // virtual from Cell::Iterator::Core

};  // end of  class Cell::Iterator::OverMeshesOfGivenDim::OrientationIrrelevant::ReturnPositiveMesh

//------------------------------------------------------------------------------------------------------//


}  // end of  namespace maniFEM

#endif  // ifndef MANIFEM_ITERATOR_XX_H
