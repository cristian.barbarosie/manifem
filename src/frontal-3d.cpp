
//   frontal-3d.cpp  2024.12.03

//   This file is part of maniFEM, a C++ library for meshes and finite elements on manifolds.

//   Copyright  2019 - 2024  Cristian Barbarosie  cristian.barbarosie@gmail.com

//   https://maniFEM.rd.ciencias.ulisboa.pt/
//   https://codeberg.org/cristian.barbarosie/maniFEM

//   ManiFEM is free software: you can redistribute it and/or modify it
//   under the terms of the GNU Lesser General Public License as published
//   by the Free Software Foundation, either version 3 of the License
//   or (at your option) any later version.

//   ManiFEM is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty
//   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//   See the GNU Lesser General Public License for more details.

//   You should have received a copy of the GNU Lesser General Public License
//   along with maniFEM.  If not, see <https://www.gnu.org/licenses/>.


// included by "frontal.cpp"


inline bool break_filament ( const Cell & seg, Mesh & interf )    // hidden in anonymous namespace

// return true if did something ('seg' is filament)
// return false if 'seg' is not a filament
	
// used in mesh generation with tetrahedra : fill_tri_membrane_{3,2}_sides
	
// a "filament" is a pair of degenerated 2D cells, like a thin leaf
// 'seg' is a common segment (edge of the leaf)
// "breaking" it  means simply eliminating the two cells from 'interf'

// the two degenerated cells will become inaccessible,
// will be destroyed by the garbage collector if enabled
// the other edge of the leaf will become inaccessible, too
// 'seg' itself may become inaccessible, it's up to the calling code

{	assert ( seg .belongs_to ( interf, tag::cell_has_low_dim ) );
	assert ( seg .dim() == 1 );
	if ( not seg .is_regular ( tag::within_STSI_mesh, interf ) )  return false;
	// a filament is regular, despite its singular apparance

	Cell face_1 = interf .cell_in_front_of ( seg, tag::surely_exists );
	if ( face_1 .boundary() .number_of ( tag::cells_of_max_dim ) == 3 )  return false;
	assert ( face_1 .boundary() .number_of ( tag::segments ) == 2 );
	Cell face_2 = interf .cell_behind ( seg, tag::surely_exists );
	assert ( face_2 .boundary() .number_of ( tag::segments ) == 2 );
	
	#if FRONTAL_VERBOSITY > 1
	std::cout << "frontal-3d.cpp line 55, breaking a filament" << std::endl;
	#endif  // FRONTAL_VERBOSITY > 1
	face_1 .remove_from ( interf );
	face_2 .remove_from ( interf );

	// general cleanup : eliminate ghost entries in extrem_filam_dist_{0,1}
	for ( std::multimap < Cell, Cell > ::iterator 
	      it = extrem_filam_dist_0 .begin(); it != extrem_filam_dist_0 .end();  )
	{	Cell filam = it->second;
		assert ( filam .dim() == 1 );
		if ( not filam .belongs_to ( interf, tag::cell_has_low_dim ) )
			it = extrem_filam_dist_0 .erase ( it );
		else  it ++;                                                    }
	for ( std::multimap < Cell, Cell > ::iterator 
	      it = extrem_filam_dist_1 .begin(); it != extrem_filam_dist_1 .end();  )
	{	Cell filam = it->second;
		assert ( filam .dim() == 1 );
		if ( not filam .belongs_to ( interf, tag::cell_has_low_dim ) )
			it = extrem_filam_dist_1 .erase ( it );
		else  it ++;                                                    }
	
	return true;                                                                        }
	
//------------------------------------------------------------------------------------------------------//


#ifndef NDEBUG  // DEBUG mode

template < class environ >
inline bool make_filament_membrane ( const Cell & seg, Mesh & interf )    // hidden in anonymous namespace

// transform a filament into a diamond-shaped membrane, just before exporting interface
// return true if did something ('seg' is filament)
	
// a "filament" is a pair of degenerated 2D cells, like a thin leaf
// 'seg' is a common segment (edge of the leaf)

{	assert ( seg .belongs_to ( interf, tag::cell_has_low_dim ) );
	assert ( seg .dim() == 1 );
	if ( not seg .is_regular ( tag::within_STSI_mesh, interf ) )  return false;
	// a filament is regular, despite its singular apparance

	Cell face_1 = interf .cell_in_front_of ( seg, tag::surely_exists );
	if ( face_1 .boundary() .number_of ( tag::cells_of_max_dim ) == 3 )  return false;
	if ( face_1 .boundary() .number_of ( tag::cells_of_max_dim ) == 4 )  return false;
	// before exporting interface, filaments are transformed into diamond-shaped membranes
	assert ( face_1 .boundary() .number_of ( tag::segments ) == 2 );
	Cell face_2 = interf .cell_behind ( seg, tag::surely_exists );
	assert ( face_2 .boundary() .number_of ( tag::segments ) == 2 );
	face_1 .remove_from ( interf );
	face_2 .remove_from ( interf );

	Cell V ( tag::vertex );
	Cell W ( tag::vertex );
	Cell A = seg .base() .reverse();
	Cell B = seg .tip();
	std::vector < double > e = environ::manif_type::get_vector ( seg );
	for ( size_t i = 0; i < frontal_nb_of_coords; i++ )
	{	const Function & x = Manifold::working .coordinates() [i];
		x(V) = x(A) + e[i] / 2.;
		x(W) = x(A) + e[i] / 2.;                                    }

	double norm = 0.;
	for ( size_t i = 0; i < frontal_nb_of_coords; i++ )  norm += e[i] * e[i];
	norm = std::sqrt ( norm );
	size_t i_min = 0;
	double dx_min = std::abs ( e[0] );
	for ( size_t i = 1; i < frontal_nb_of_coords; i++ )
		if ( std::abs ( e[i] ) < dx_min )
		{	i_min = i;  dx_min = std::abs ( e[i] );  }
	const Function & x = Manifold::working .coordinates() [ i_min ];
	x(V) += 0.05 * norm;
	x(W) -= 0.05 * norm;

	Cell AV ( tag::segment, A .reverse(), V );
	Cell BV ( tag::segment, B .reverse(), V );
	Cell AW ( tag::segment, A .reverse(), W );
	Cell BW ( tag::segment, B .reverse(), W );
	Cell AVBW ( tag::quadrangle, AV, BV .reverse(), BW, AW .reverse() );
	Cell AWBV ( tag::quadrangle, AW, BW .reverse(), BV, AV .reverse() );

	AVBW .add_to ( interf );
	AWBV .add_to ( interf );
	return true;                                                                 }

#endif  // DEBUG

//------------------------------------------------------------------------------------------------------//


template < class environ >
inline void break_longest_filament  // hidden in anonymous namespace
( const Cell & seg_1, const Cell & seg_2, const Cell & ver, Mesh & interf )

// 'ver' is useful for calling 'sq_dist'

// 'environ' describes the type of manifold we are working in
// and how we treat information about metric and exterior product
// environ::manif_type could be ManifoldNoWinding or ManifoldQuotient (defined in frontal.cpp)
// environ::metric_type may be one of ISRContainer::{Inactive,Active} (defined in frontal.cpp)
// environ::ext_prod_type may be ExtProd3d, 3dRev (defined in frontal.cpp)
	
{	assert ( ver   .dim() == 0 );
	assert ( seg_1 .dim() == 1 );
	assert ( seg_2 .dim() == 1 );
	assert ( ver   .belongs_to ( interf, tag::cell_has_low_dim ) );
	assert ( seg_1 .belongs_to ( interf, tag::cell_has_low_dim ) );
	assert ( seg_2 .belongs_to ( interf, tag::cell_has_low_dim ) );
	assert ( seg_1 .is_regular ( tag::within_STSI_mesh, interf ) );
	assert ( seg_2 .is_regular ( tag::within_STSI_mesh, interf ) );
	// a filament is regular, despite its singular apparance

	std::vector < double > e_1 = environ::manif_type::get_vector ( seg_1 );
	std::vector < double > e_2 = environ::manif_type::get_vector ( seg_2 );
	if ( Manifold::working .sq_dist ( ver, e_1 ) > Manifold::working .sq_dist ( ver, e_2 ) )
		break_filament ( seg_1, interf );
	else  break_filament ( seg_2, interf );                                                   }

//------------------------------------------------------------------------------------------------------//


inline void update_filaments_near ( const Cell & seg, Mesh & interf )  // hidden in anonymous namespace

// 'seg' is a newly created segment which may shorten
// the topological distance between extremities of a filament, within 'interf'
// 'seg' is not yet part of 'interf'
// well, sometimes it is part of 'interf' but it is filament so does not count

{	label_restart :
	
	if ( ( extrem_filam_dist_0 .size() == 0 ) and ( extrem_filam_dist_1 .size() == 0 ) )
		return;
	
	Cell A = seg .base() .reverse ( tag::surely_exists );
	Cell B = seg .tip();

	std::pair < std::multimap < Cell, Cell > ::iterator,
              std::multimap < Cell, Cell > ::iterator >
		range_A_0 = extrem_filam_dist_0 .equal_range (A),
		range_B_0 = extrem_filam_dist_0 .equal_range (B);
	std::pair < std::multimap < Cell, Cell > ::iterator,
	            std::multimap < Cell, Cell > ::iterator >
		range_A_1 = extrem_filam_dist_1 .equal_range (A),
		range_B_1 = extrem_filam_dist_1 .equal_range (B);
	bool A_0 = range_A_0 .first == range_A_0 .second;
	bool B_0 = range_B_0 .first == range_B_0 .second;
	bool A_1 = range_A_1 .first == range_A_1 .second;
	bool B_1 = range_B_1 .first == range_B_1 .second;
	
	// we begin by dealing with the simplest, most common, case
	// neither A nor B are near any filament
	// or one of them is near a filament but the other one is not
	if ( not ( A_0 or B_0 or ( A_1 and B_1 ) ) )  return;

	// case "A0B0": there should not be a filament with extremities A and B
	// filaments are broken before their extremities can get so close
	#ifndef NDEBUG  // DEBUG mode
	if ( A_0 and B_0 )
		// search for commom filaments (there may be none)
		// there should be none belonging to 'interf'
		for ( std::multimap < Cell, Cell > ::iterator
		      it_A = range_A_0 .first; it_A != range_A_0 .second; it_A ++ )
		{	Cell filam_A = it_A->second;
			assert ( filam_A .dim() == 1 );
			assert ( filam_A .belongs_to ( interf, tag::cell_has_low_dim ) );
			assert ( filam_A .is_regular ( tag::within_STSI_mesh, interf ) );
			Cell face = interf .cell_behind ( filam_A, tag::surely_exists );
			for ( std::multimap < Cell, Cell > ::iterator
			      it_B = range_B_0 .first; it_B != range_B_0 .second; it_B ++  )
			{	Cell filam_B = it_B->second;
				assert ( filam_B .dim() == 1 );
				assert ( filam_B .belongs_to ( interf, tag::cell_has_low_dim ) );
				assert ( filam_B .is_regular ( tag::within_STSI_mesh, interf ) );
				// are 'filam_A' and 'filam_B' two sides of the same filament ? should not
				assert ( not filam_B .belongs_to ( face .boundary(), tag::not_oriented ) );  }  }
	#endif  // DEBUG

	// case "A0B1": there should not be a filament with extremities A and a neighbour of B
	// filaments are broken before their extremities can get so close
	#ifndef NDEBUG  // DEBUG mode
	if ( A_0 and B_1 )
		// search for commom filaments (there may be none)
		// there should be none belonging to 'interf'
		for ( std::multimap < Cell, Cell > ::iterator
		      it_A = range_A_0 .first; it_A != range_A_0 .second; it_A ++ )
		{	Cell filam_A = it_A->second;
			assert ( filam_A .dim() == 1 );
			assert ( filam_A .belongs_to ( interf, tag::cell_has_low_dim ) );
			assert ( filam_A .is_regular ( tag::within_STSI_mesh, interf ) );
			Cell face = interf .cell_behind ( filam_A, tag::surely_exists );
			for ( std::multimap < Cell, Cell > ::iterator
			      it_B = range_B_1 .first; it_B != range_B_1 .second; it_B ++  )
			{	Cell filam_B = it_B->second;
				assert ( filam_B .dim() == 1 );
				assert ( filam_B .belongs_to ( interf, tag::cell_has_low_dim ) );
				assert ( filam_B .is_regular ( tag::within_STSI_mesh, interf ) );
				// are 'filam_A' and 'filam_B' two sides of the same filament ? should not
				assert ( not filam_B .belongs_to ( face .boundary(), tag::not_oriented ) );  }  }
	#endif  // DEBUG

	// case "A1B0": there should not be a filament with extremities B and a neighbour of A
	// filaments are broken before their extremities can get so close
	#ifndef NDEBUG  // DEBUG mode
	if ( A_1 and B_0 )
		// search for commom filaments (there may be none)
		// there should be none belonging to 'interf'
		for ( std::multimap < Cell, Cell > ::iterator
		      it_A = range_A_1 .first; it_A != range_A_1 .second; it_A ++ )
		{	Cell filam_A = it_A->second;
			assert ( filam_A .dim() == 1 );
			assert ( filam_A .belongs_to ( interf, tag::cell_has_low_dim ) );
			assert ( filam_A .is_regular ( tag::within_STSI_mesh, interf ) );
			Cell face = interf .cell_behind ( filam_A, tag::surely_exists );
			for ( std::multimap < Cell, Cell > ::iterator
			      it_B = range_B_0 .first; it_B != range_B_0 .second; it_B ++  )
			{	Cell filam_B = it_B->second;
				assert ( filam_B .dim() == 1 );
				assert ( filam_B .belongs_to ( interf, tag::cell_has_low_dim ) );
				assert ( filam_B .is_regular ( tag::within_STSI_mesh, interf ) );
				// are 'filam_A' and 'filam_B' two sides of the same filament ? should not
				assert ( not filam_B .belongs_to ( face .boundary(), tag::not_oriented ) );  }  }
	#endif  // DEBUG

	// case "A1B1": there may be a filament with extremities neighbours of A and B, respectively
	if ( A_1 and B_1 )
		// search for commom filaments (there may be none)
		for ( std::multimap < Cell, Cell > ::iterator
		      it_A = range_A_1 .first; it_A != range_A_1 .second; it_A ++ )
		{	Cell filam_A = it_A->second;
			assert ( filam_A .dim() == 1 );
			assert ( filam_A .belongs_to ( interf, tag::cell_has_low_dim ) );
			assert ( filam_A .is_regular ( tag::within_STSI_mesh, interf ) );
			Cell face = interf .cell_behind ( filam_A, tag::surely_exists );
			for ( std::multimap < Cell, Cell > ::iterator
			      it_B = range_B_1 .first; it_B != range_B_1 .second; it_B ++  )
			{	Cell filam_B = it_B->second;
				assert ( filam_B .dim() == 1 );
				assert ( filam_B .belongs_to ( interf, tag::cell_has_low_dim ) );
				assert ( filam_B .is_regular ( tag::within_STSI_mesh, interf ) );
				// are 'filam_A' and 'filam_B' two sides of the same filament ? 
				if ( filam_B .belongs_to ( face .boundary(), tag::not_oriented ) )
				{	break_filament ( filam_A, interf );  // or break filam_B, does not matter
					goto label_restart;                  }                           }  }

}  // end of  update_filaments_near

//------------------------------------------------------------------------------------------------------//


template < class S > class iterator_over_union;    // hidden in anonymous namespace


template < class S >  // S will be environ::manif_type::set_of_winding_cells
class union_of    // hidden in anonymous namespace

{	public :

	std::vector < S * > components;

	inline union_of ( S & a, S & b )
	:	components { &a, &b }
	{	}

	typedef iterator_over_union < S > iterator;

	inline iterator begin ()
	{	return iterator ( this );  }

	inline size_t size ( )
	{	size_t res = 0;
		for ( typename std::vector < S * > ::const_iterator
		      it = components .begin(); it != components .end(); it++ )
			res += (*it)->size();
		return res;                                                      }

	inline bool has_element ( const typename S::key_type & el )
	{	for ( typename std::vector < S * > ::const_iterator
		      it = components .begin(); it != components .end(); it ++ )
			{	if ( (*it)->find ( el ) != (*it)->end() )  return true;  }
		return false;                                                     }
	
};


template < class S >  // S will be environ::manif_type::set_of_winding_cells
class iterator_over_union    // hidden in anonymous namespace

{	public :

	std::vector < S * > & union_comp;
	size_t index;
	std::vector < typename S::iterator > iterator_component;

	inline iterator_over_union ( union_of < S > * that )
	:	union_comp { that->components }, index { 0 }
	{	iterator_component .reserve ( union_comp .size() );
		for ( size_t i = 0; i < union_comp .size(); i ++ )
			iterator_component .push_back ( union_comp [i]->begin() );
		advance_to_regular_position();                                 }

	inline void advance_to_regular_position ( )
	// we want  iterator_component [ index ] != union_comp [ index ]->end()
	// or else  index == union_comp .size()
	{	while ( ( index < union_comp .size() ) and
		        ( iterator_component [ index ] == union_comp [ index ]->end() ) )
			index ++;                                                                }

	inline iterator_over_union & operator++ ( int )
	{ assert ( in_range() );
		iterator_component [ index ] ++;
		advance_to_regular_position();
		return * this;                    }

	inline typename S::const_reference operator* ( )
	// all iterators in a set point to const elements
	{	assert ( in_range() );
		return  * ( iterator_component [ index ] );  }

	inline bool in_range ( )   // I don't know how to implement 'end', sorry
	{	return ( index < union_comp .size() );  }

};

//------------------------------------------------------------------------------------------------------//


class constraint_violated  // measures the violation of the constraint
// this class is meant to order the set of 'constraints', see below
// upon inserting a new element, the set is automatically ordered
{	public :
	bool operator() ( const std::pair < double, std::vector < double > > & a,
	                  const std::pair < double, std::vector < double > > & b) const
	{	return  a .first > b .first;  }                                               };


template < class environ >    // hidden in anonymous namespace
double relocate_core_3d
( const Mesh & interf, const Cell & P,
  const typename environ::manif_type::type_of_walls & walls,
  union_of < typename environ::manif_type::set_of_winding_cells > & vertices,
  std::vector < std::vector < double > > & inertia, const double tol_2       )

// try to locate P at distance approx 1. to all given neighbours
// thus, vertices which are far pull P, vertices which are close push P

// a similar treatment is given to 'walls', which are faces
// we want the distance to each wall to be approx sqrt_two_thirds
// 'walls' is a set of triangles if manif non-winding
// it is a map (triangle, vertex with winding) if manif winding

// a similar treatment is given to wide angles in triangles around P
		
{	// tol_2 is the square of the tolerance, i.e. stopping criterion is  ||step|| < tol = sqrt ( tol_2 )
	
	#if FRONTAL_VERBOSITY > 1
	std::cout << std::endl << "relocate_core_3d " << vertices .size() << " " << walls .size() << ", ";
	#endif

	for ( size_t iter = 0; iter < 100; iter++ )

	{	// in 'constraints' we keep the value and the gradient of several constraints :
		// dist(A,P) large (desired value 0.8)
		// dist(A,P) small (desired value 1.2)
		// dist(A,tri) large (desired value 0.8)
		// cos alpha large (desired value 0.3)
		std::set < std::pair < double, std::vector < double > >, constraint_violated > constraints;

		for ( typename union_of < typename environ::manif_type::set_of_winding_cells >
		      ::iterator it = vertices .begin(); it .in_range(); it ++                 )
		{	const typename environ::manif_type::winding_cell & AA = *it;
			std::vector < double > e = environ::manif_type::get_vector ( P, AA );  // e[i] = A_i - P_i
			Cell A = environ::manif_type::remove_winding ( AA );
			const double dist2 = Manifold::working .sq_dist ( P, A, e );
			if ( ( dist2 > 0.81 ) and ( dist2 < 1.21 ) )  continue;  // 'll' between 0.9 and 1.1
			const double ll = approx_sqrt ( dist2 );
			if ( ll > 1.1 )   // pull P towards A
			{	constraints .insert ( { ll - 1.1, e } );
				#if FRONTAL_VERBOSITY > 1
				std::cout << ll << " ";
				#endif
				continue;                                 }  // to next neighbour vertex
			if ( ll < 0.9 )  // push P away from A
			{	for ( size_t i = 0; i < frontal_nb_of_coords; i++ )  e[i] *= -1.;
				#if FRONTAL_VERBOSITY > 1
				std::cout << ll << " ";
				#endif
				// add asymptotic repulsive force
				const double var = ll - 0.2;
				if ( var < 0. )  // ll < 0.2
					for ( size_t i = 0; i < frontal_nb_of_coords; i++ )
						e[i] *= 1. + 250. * var * var;
				constraints .insert ( { 0.9 - ll, e } );                           }  }
		#if FRONTAL_VERBOSITY > 1
		std::cout << "| ";
		#endif

		for ( typename environ::manif_type::type_of_walls::const_iterator
		      it = walls .begin(); it != walls .end(); it ++              )
		{	const Cell tri = environ::manif_type::get_tri_from_wall ( it );
			assert ( tri .dim() == 2 );
			const std::vector < double > e = environ::manif_type::get_vector_from_wall ( P, it );
			// note that e[i] is not P_i - V_i but V_i - P_i, V is some vertex of 'tri'
			std::vector < double > norm_tri =
				environ::ext_prod_type::get_normal_vector ( tag::domain_3d, tag::at_tri, tri );
			// although the interface is looking "outwards",
			// towards the zone where we do not intend to build tetrahedra,
			// 'get_normal_vector' returns a normal vector pointing "inwards",
			// that is, towards the zone we intend to mesh
			// 'norm_tri' will push 'P' away from 'tri'
			const double d = - Manifold::working .inner_prod ( P, e, norm_tri );
			if ( d < 0.8 )  {
				#if FRONTAL_VERBOSITY > 1
				std::cout << d << " ";
				#endif
				// add asymptotic repulsive force
				const double var = d - 0.2;
				if ( var < 0. )  // d < 0.2
					for ( size_t i = 0; i < frontal_nb_of_coords; i++ )
						norm_tri [i] *= 1. + 250. * var * var;
				constraints .insert ( { 0.8 - d, norm_tri } );   }                                   }
		#if FRONTAL_VERBOSITY > 1
		std::cout << "| ";
		#endif

		// discourage wide angles
		if ( P .belongs_to ( interf, tag::cell_has_low_dim ) )
		{	Mesh::Iterator it = interf .iterator  // orientation of triangles irrelevant
				( tag::over_cells_of_max_dim, tag::as_found, tag::around, P );
			for ( it .reset(); it .in_range(); it ++ )
			{	Cell tri = * it;
				Cell PQ = tri .boundary() .cell_in_front_of ( P, tag::surely_exists );
				Cell Q = PQ .tip();
				Cell RP = tri .boundary() .cell_behind ( P, tag::surely_exists );
				Cell R = RP .base() .reverse ( tag::surely_exists );
				Cell QR = tri .boundary() .cell_behind ( R, tag::surely_exists );
				assert ( Q == QR .base() .reverse ( tag::surely_exists ) );
				std::vector < double > e_PQ = environ::manif_type::get_vector ( PQ );
				normalize_vector_approx ( P, e_PQ );
				std::vector < double > e_QR = environ::manif_type::get_vector ( QR );
				normalize_vector_approx ( Q, e_QR );
				std::vector < double > e_RP = environ::manif_type::get_vector ( RP );
				normalize_vector_approx ( R, e_RP );
				// below we provide as direction e_QR or e_RQ, respectively
				// this is not the gradient of cos alpha but is a reasonable descent direction
				const double minus_cos_PQR = Manifold::working .inner_prod ( Q, e_PQ, e_QR );
				if ( minus_cos_PQR > - 0.2 )  {  // cos_PQR < 0.2
					#if FRONTAL_VERBOSITY > 1
					std::cout << - minus_cos_PQR << " ";
					#endif
					// add asymptotic repulsive force
					const double var = minus_cos_PQR - 0.5;
					if ( var > 0. )  // cos_PQR < -0.5 
						for ( size_t i = 0; i < frontal_nb_of_coords; i++ )
							e_QR [i] *= 1. + 50. * var * var;
					constraints .insert ( { 0.2 + minus_cos_PQR, e_QR } );
					continue;                                               }  // to next triangle around P
				const double minus_cos_QRP = Manifold::working .inner_prod ( Q, e_QR, e_RP );
				if ( minus_cos_QRP > - 0.2 )  // cos_QRP < 0.2
				{	for ( size_t i = 0; i < frontal_nb_of_coords; i++ )  e_QR [i] *= -1.;
					#if FRONTAL_VERBOSITY > 1
					std::cout << - minus_cos_QRP << " ";
					#endif
					// add asymptotic repulsive force
					const double var = minus_cos_QRP - 0.5;
					if ( var > 0. )  // cos_QRP < -0.5 
						for ( size_t i = 0; i < frontal_nb_of_coords; i++ )
							e_QR [i] *= 1. + 50. * var * var;
					constraints .insert ( { 0.2 + minus_cos_QRP, e_QR } );                 }     }  }
		#if FRONTAL_VERBOSITY > 1
		std::cout << "| ";
		#endif

		// discourage tight angles
		if ( P .belongs_to ( interf, tag::cell_has_low_dim ) )
		{	Mesh::Iterator it_around_P =
				interf .iterator ( tag::over_segments, tag::pointing_away_from, P );
			for ( it_around_P .reset(); it_around_P .in_range(); it_around_P ++ )
			{	Cell PQ = * it_around_P;
				std::vector < double > e_PQ = environ::manif_type::get_vector ( PQ );
				normalize_vector_approx ( P, e_PQ );
				Cell Q = PQ .tip();
				Mesh::Iterator it_around_Q =
					interf .iterator ( tag::over_segments, tag::pointing_towards, Q );
				for ( it_around_Q .reset(); it_around_Q .in_range(); it_around_Q ++ )
				{	Cell RQ = * it_around_Q;
					if ( RQ == PQ )  continue;
					std::vector < double > e_RQ = environ::manif_type::get_vector ( RQ );
					normalize_vector_approx ( Q, e_RQ );
					const double cos_PQR = Manifold::working .inner_prod ( Q, e_PQ, e_RQ );
					if ( cos_PQR > 0.6 )  {  // push P away from R
						#if FRONTAL_VERBOSITY > 1
						std::cout << cos_PQR << " ";
						#endif
						std::vector < double > e_RP ( frontal_nb_of_coords );
						for ( size_t i = 0; i < frontal_nb_of_coords; i++ )
							e_RP [i] = e_RQ [i] - e_PQ [i];
						// add asymptotic repulsive force
						const double var = cos_PQR - 0.8;
						if ( var > 0. )  // cos_PQR > 0.8
							for ( size_t i = 0; i < frontal_nb_of_coords; i++ )
								e_RP [i] *= 1. + 250. * var * var;
						constraints .insert ( { cos_PQR - 0.6, e_RP } );       }               }  }  }
		#if FRONTAL_VERBOSITY > 1
		std::cout << std::endl;
		#endif

		assert ( inertia .size() == 3 );
		inertia [0] .swap ( inertia [1] );
		inertia [1] .swap ( inertia [2] );
		std::vector < double > & grad = inertia [2];
		grad = std::vector < double > ( frontal_nb_of_coords, 0. );  // is there a 'fill' method ?
		
		static const std::vector < double > weights_grad
			{ 0.0225, 0.02, 0.0175, 0.015, 0.013, 0.011, 0.0095, 0.008, 0.0065, 0.005 };
		// { 0.02, 0.0175, 0.015, 0.0125, 0.01, 0.0075, 0.005, 0.0025, 0.002, 0.00175 }
		std::set < std::pair < double, std::vector < double > >, constraint_violated >
			::const_iterator it = constraints .begin();
		for ( size_t j = 0; j < weights_grad .size(); j++ )
		{	if ( it == constraints .end() )  break;
			#if FRONTAL_VERBOSITY > 1
			std::cout << it->first << " ";
			#endif
			const double w = weights_grad [j] * it->first;
			for ( size_t i = 0; i < frontal_nb_of_coords; i++ )
				grad [i] += w * it->second [i];
			it ++;                                               }

		static const std::vector < double > weights_delta { 0.2, 0.4, 0.8 };
		assert ( weights_delta .size() == inertia .size() );
		std::vector < double > delta ( frontal_nb_of_coords, 0. );
		for ( size_t j = 0; j < inertia .size(); j++ )
		{	const std::vector < double > & inert = inertia [j];
			const double & w = weights_delta [j];
			for ( size_t i = 0; i < frontal_nb_of_coords; i++ )
				delta [i] += w * inert [i];                        }
				
		double norm_2 = Manifold::working .inner_prod ( P, delta, delta );
		// std::cout << std::sqrt ( Manifold::working .inner_prod ( P, delta, delta ) ) << std::endl;
		Manifold::working .core->move_vertex ( P, delta, norm_2 );
		// move P, project it if necessary, may change norm_2
		#if FRONTAL_VERBOSITY > 1
		std::cout << "norm " << std::sqrt ( norm_2 ) << std::endl << std::flush;
		#endif
		if ( norm_2 < tol_2 )  return norm_2;  //  norm of delta < tol

	}  // end of  for  over  iter < 100

	std::cout << std::endl << "frontal mesh generation process is unstable" << std::endl;
	std::cout << "perhaps the curvature of your manifold is too high ?" << std::endl;
	std::cout << "one possible solution is to decrease the segment size" << std::endl;
	std::cerr << "frontal mesh generation process is unstable" << std::endl;
	exit (1);

}  // end of  relocate_core_3d

//------------------------------------------------------------------------------------------------------//


template < class environ >
bool danger_of_flat_tetra   // hidden in anonymous namespace
( const Mesh & interf, const Cell & AB, const Cell & ABC, const Cell & BAD,
  const tag::PreviousSeg &, const Cell & OA,
  const tag::MobileVertex &, const Cell & mobile_vertex,
  const tag::StartedAt &, const Cell & start_tri                           )

// the code wants to fill some tetrahedron (with two existent faces)
// needs to ensure no flat tetrahedron will show up in the sequel

// 'mobile_vertex' tells us that C or D may be still in the process of location
// if that is not the case, 'mobile_vertex' is a non-existent Cell

// perhaps change code to receive more arguments
	
// 'environ' describes the type of manifold we are working in
// and how we treat information about metric and exterior product
// environ::manif_type could be ManifoldNoWinding or ManifoldQuotient (defined in frontal.cpp)
// environ::metric_type may be one of ISRContainer::{Inactive,Active} (defined in frontal.cpp)
// environ::ext_prod_type may be ExtProd3d, 3dRev (defined in frontal.cpp)
	
{	if ( ABC == start_tri )  // we are back to the initial pair of triangles, enough is enough
		return false;

	assert ( AB .belongs_to ( ABC .boundary(), tag::same_dim, tag::orientation_compatible_with_mesh ) );
	// (the reverse of) AB does not always belong to the boundary of BAD
	Cell B = AB .tip();
	Cell A = AB .base() .reverse ( tag::surely_exists );
	Cell BC = ABC .boundary() .cell_in_front_of ( B, tag::surely_exists );
	Cell C = BC .tip();
	Cell CA = ABC .boundary() .cell_behind ( A, tag::surely_exists );
	assert ( CA .base() .reverse ( tag::surely_exists ) == C );
	Cell AD = BAD .boundary() .cell_in_front_of ( A, tag::surely_exists );
	Cell D = AD .tip();
	Cell DB = BAD .boundary() .cell_in_front_of ( D, tag::surely_exists );

	const std::vector < double > n_ABC =
		environ::ext_prod_type::get_normal_vector ( tag::domain_3d, tag::at_tri, ABC );
	const std::vector < double > n_BAD =
		environ::ext_prod_type::get_normal_vector ( tag::domain_3d, tag::at_tri, BAD );
	// although the interface is looking "outwards",
	// towards the zone where we do not intend to build tetrahedra,
	// 'get_normal_vector' returns a normal vector pointing "inwards",
	// that is, towards the zone we intend to mesh

	// despite their names, triangles ABC and BAD do not always share vertex B
	// they surely do share vertex A
	if ( interf .cell_in_front_of ( AB, tag::seen_from, ABC, tag::surely_exists ) == BAD )
		// triangles ABC and BAD share side AB and vertex B, 
		// so we have four triangles around A : OAC, ODA, ABC, BAD

	{	assert ( DB .tip() == B );

		if ( interf .cell_in_front_of ( BC, tag::seen_from, ABC, tag::surely_exists ) ==
		     interf .cell_in_front_of ( DB, tag::seen_from, BAD, tag::surely_exists )    )
			// this is extremely rare, may happen during 'add_walls_first_layer'
			// ABCD will become isolated tetrahedron
			return false;
			  
		// if C or D is mobile, we accept a wide angle, it will probably get better soon, see e.g.
		// https://codeberg.org/cristian.barbarosie/manifem-manual, files 'tetrahedra-03.{png,msh}'
		if ( ( C != mobile_vertex ) and ( D != mobile_vertex ) )
		{	const double prod = Manifold::working .inner_prod ( D, n_ABC, n_BAD );
			// in 3D we do not need the special inner_prod linked to ext_prod_type
			if ( prod > 0.55 )    // angle between ABC and BAD too wide
				return true;                                                         }

		Cell BFC = interf .cell_in_front_of ( BC, tag::seen_from, ABC, tag::surely_exists );
		Cell BDF = interf .cell_in_front_of ( DB, tag::seen_from, BAD, tag::surely_exists );
		Cell BF = BFC .boundary() .cell_in_front_of ( B, tag::surely_exists );
		return  danger_of_flat_tetra < environ >
			( interf, BF, BFC, BDF, tag::previous_seg, AB, tag::mobile_vertex, mobile_vertex,
			  tag::started_at, start_tri                                                     );
		
	}  // end of if
	
	// else  // more than four triangles around A
	// in this case we still want to check whether the future segment CD
	// is almost coplanar to AB or to BA (BA is not the reverse of AB)
	// Cell BA = BAD .boundary() .cell_in_front_of ( A, tag::surely_exists )
	// this is to prevent degenerated situations like the ones shown in
	// https://codeberg.org/cristian.barbarosie/manifem-manual, files 'tetrahedra-{01,02}.msh'
	
	// caveat : one of the vertices may be mobile (it will still be repositioned)
	// in that case, criteria below become meaningless

	// caveat : deal with possible membranes (to do)

	Cell other_B = DB .tip();
	// if one of B, other_B, C, D is mobile, the danger may be eliminated later, by moving that vertex
	// A may be mobile but this does not eliminate the "danger"

	// below we could replace the criterion involving the angle between OA and normals
	// by the angle between CA+CD and AD
	// this would spare computing the normal vector and transmitting OA as parameter
	// is this worth ? is computing the normal vector cheap or expensive ?
	
	const std::vector < double > e_OA = environ::manif_type::get_vector ( OA );

	if ( ( ( D == mobile_vertex ) or ( B == mobile_vertex ) ) and
	     ( ( C == mobile_vertex ) or ( other_B == mobile_vertex ) ) )
		assert ( false );  // does this ever happen ?
		// return false;  // safe to build tetrahedron OACD

	// if BAD is a membrane ( DB .tip() == O ) things may go rogue
	// if we ask below whether prod < 0. we may get wrong configurations
	// changing the threshold to -0.2 avoids this

	const bool danger_ABC = Manifold::working .inner_prod ( A, e_OA, n_ABC ) < -0.2;
	const bool danger_BAD = Manifold::working .inner_prod ( A, e_OA, n_BAD ) < -0.2;

	if ( ( not danger_ABC or ( D == mobile_vertex ) or ( B == mobile_vertex ) ) and
	     ( not danger_BAD or ( C == mobile_vertex ) or ( other_B == mobile_vertex ) ) )
		return false;  // safe to build tetrahedron OACD
	// if ( ( not danger_ABC ) and ( not danger_BAD ) )  return false;
	
	const std::vector < double > e_CA = environ::manif_type::get_vector ( CA );
	const std::vector < double > e_AD = environ::manif_type::get_vector ( AD );
	std::vector < double > e_CD ( frontal_nb_of_coords );
	for ( size_t i = 0; i < frontal_nb_of_coords; i++ )  e_CD [i] = e_CA [i] + e_AD [i];
	normalize_vector_approx ( A, e_CD );

	if ( danger_ABC and ( D != mobile_vertex ) and ( B != mobile_vertex ) )
		// alternatively, think in terms of mixed product
		if ( Manifold::working .inner_prod ( A, e_CD, n_ABC ) < 0.3 )  // was  0.15
			return true;  // danger, do not build tetrahedron OACD

	if ( danger_BAD and ( C != mobile_vertex ) and ( other_B != mobile_vertex ) )
		// alternatively, think in terms of mixed product
		if ( Manifold::working .inner_prod ( A, e_CD, n_BAD ) > -0.3 )  // was  -0.15
			return true;  // danger, do not build tetrahedron OACD

	return false;  // safe to build tetrahedron OACD

}  // end of  danger_of_flat_tetra  with tag::started_at
	
//------------------------------------------------------------------------------------------------------//


template < class environ >
inline bool danger_of_flat_tetra   // hidden in anonymous namespace
( const Mesh & interf, const Cell & AB, const Cell & ABC, const Cell & BAD,
  const tag::MobileVertex &, const Cell & mobile_vertex, const tag::BothDirections & )

// should triangles ABC and BAD be faces of a new tetrahedron ?
// we do not test ABCD but its neighbours (two chains of them, one on each side)
	
// perhaps change code to receive more arguments
	
// 'environ' describes the type of manifold we are working in
// and how we treat information about metric and exterior product
// environ::manif_type could be ManifoldNoWinding or ManifoldQuotient (defined in frontal.cpp)
// environ::metric_type may be one of ISRContainer::{Inactive,Active} (defined in frontal.cpp)
// environ::ext_prod_type may be ExtProd3d, 3dRev (defined in frontal.cpp)
	
{	assert ( interf .cell_in_front_of ( AB, tag::seen_from, ABC, tag::surely_exists ) == BAD );
	Cell B = AB .tip();
	Cell A = AB .base() .reverse ( tag::surely_exists );
	Cell BC = ABC .boundary() .cell_in_front_of ( B, tag::surely_exists );
	Cell C = BC .tip();
	Cell CA = ABC .boundary() .cell_behind ( A, tag::surely_exists );
	assert ( CA .base() .reverse ( tag::surely_exists ) == C );
	Cell AD = BAD .boundary() .cell_in_front_of ( A, tag::surely_exists );
	Cell D = AD .tip();
	Cell DB = BAD .boundary() .cell_behind ( B, tag::surely_exists );
	assert ( DB .base() .reverse ( tag::surely_exists ) == D );

	Cell ACE = interf .cell_in_front_of ( CA, tag::seen_from, ABC, tag::surely_exists );
	Cell DAE = interf .cell_in_front_of ( AD, tag::seen_from, BAD, tag::surely_exists );
	Cell AE = DAE .boundary() .cell_in_front_of ( A, tag::surely_exists );
	if ( danger_of_flat_tetra < environ >
	     ( interf, AE, DAE, ACE, tag::previous_seg, AB .reverse ( tag::surely_exists ),
	       tag::mobile_vertex, mobile_vertex, tag::started_at, BAD                     ) )
		return true;

	Cell BFC = interf .cell_in_front_of ( BC, tag::seen_from, ABC, tag::surely_exists );
	Cell BDF = interf .cell_in_front_of ( DB, tag::seen_from, BAD, tag::surely_exists );
	Cell BF = BFC .boundary() .cell_in_front_of ( B, tag::surely_exists );
	if ( danger_of_flat_tetra < environ >
	     ( interf, BF, BFC, BDF, tag::previous_seg, AB,
	       tag::mobile_vertex, mobile_vertex, tag::started_at, ABC ) )
		return true;
	
	return false;

}  // end of  danger_of_flat_tetra  with tag::both_directions
	
//------------------------------------------------------------------------------------------------------//


template < class environ >
inline bool danger_of_flat_tetra   // hidden in anonymous namespace
( const Mesh & interf, const Cell & AB, const Cell & ABC, const Cell & BAD,
  const tag::MobileVertex &, const Cell & mobile_vertex, const tag::OneDirection & )

// should triangles ABC and BAD be faces of a new tetrahedron ?
// we do not test ABCD but its neighbours (on one side)

// perhaps change code to receive more arguments
	
// 'environ' describes the type of manifold we are working in
// and how we treat information about metric and exterior product
// environ::manif_type could be ManifoldNoWinding or ManifoldQuotient (defined in frontal.cpp)
// environ::metric_type may be one of ISRContainer::{Inactive,Active} (defined in frontal.cpp)
// environ::ext_prod_type may be ExtProd3d, 3dRev (defined in frontal.cpp)
	
{	Cell B = AB .tip();
	Cell A = AB .base() .reverse ( tag::surely_exists );
	Cell BC = ABC .boundary() .cell_in_front_of ( B, tag::surely_exists );
	Cell C = BC .tip();
	Cell CA = ABC .boundary() .cell_behind ( A, tag::surely_exists );
	assert ( CA .base() .reverse ( tag::surely_exists ) == C );
	Cell AD = BAD .boundary() .cell_in_front_of ( A, tag::surely_exists );
	Cell D = AD .tip();
	Cell DB = BAD .boundary() .cell_behind ( B, tag::surely_exists );
	assert ( DB .base() .reverse ( tag::surely_exists ) == D );

	Cell BEC = interf .cell_in_front_of ( BC, tag::seen_from, ABC, tag::surely_exists );
	Cell BDE = interf .cell_in_front_of ( DB, tag::seen_from, BAD, tag::surely_exists );
	Cell BE = BEC .boundary() .cell_in_front_of ( B, tag::surely_exists );
	if ( danger_of_flat_tetra < environ >
	     ( interf, BE, BEC, BDE, tag::previous_seg, AB,
	  	    tag::mobile_vertex, mobile_vertex, tag::started_at, ABC ) )
		return true;
	
	return false;

}  // end of  danger_of_flat_tetra  with tag::one_direction
	
//------------------------------------------------------------------------------------------------------//


template < class environ >
bool fit_for_one_tetra   // hidden in anonymous namespace
( const Mesh & interf, const Cell & AB, const Cell & ABC, const Cell & BAD, double & prod )

// should triangles ABC and BAD be faces of a new tetrahedron ?
// besides the boolean answer, return also the inner product 'prod'

// 'environ' describes the type of manifold we are working in
// and how we treat information about metric and exterior product
// environ::manif_type could be ManifoldNoWinding or ManifoldQuotient (defined in frontal.cpp)
// environ::metric_type may be one of ISRContainer::{Inactive,Active} (defined in frontal.cpp)
// environ::ext_prod_type may be ExtProd3d, 3dRev (defined in frontal.cpp)
	
{	Cell B = AB .tip();
	Cell A = AB .base() .reverse ( tag::surely_exists );
	Cell BC = ABC .boundary() .cell_in_front_of ( B, tag::surely_exists );
	Cell C = BC .tip();
	Cell CA = ABC .boundary() .cell_behind ( A, tag::surely_exists );
	assert ( CA .base() .reverse ( tag::surely_exists ) == C );
	Cell AD = BAD .boundary() .cell_in_front_of ( A, tag::surely_exists );
	Cell D = AD .tip();
	Cell DB = BAD .boundary() .cell_behind ( B, tag::surely_exists );
	assert ( DB .base() .reverse ( tag::surely_exists ) == D );

	// twice the height of an equilateral triangle : sqrt 3
	// height of an equilateral tetrahedron : sqrt 2/3
	const std::vector < double > e_DC =
		environ::manif_type::get_vector ( D, environ::manif_type::add_winding_2 ( C, DB, BC ) );
	double DC2 = Manifold::working .sq_dist ( D, C, e_DC );
	if ( DC2 > 3. )  return false;  // frontal_seg_size 3D
	// if return, 'prod' remains unchanged, on purpose
	// if not return, DC < sqrt 3 = 1.73, we may want to fill tetrahedron ABCD

	// we could use 'angle_3d' as criterion
	// but the approach below saves time in case of an early return
	// and also takes the length of DC into account

	// check curvature :
	const std::vector < double > e_AD = environ::manif_type::get_vector ( AD );
	const std::vector < double > n_ABC =
		environ::ext_prod_type::get_normal_vector ( tag::domain_3d, tag::at_tri, ABC );
	// although the interface is looking "outwards",
	// towards the zone where we do not intend to build tetrahedra,
	// 'get_normal_vector' returns a normal vector pointing "inwards",
	// that is, towards the zone we intend to mesh
	const double tmp = Manifold::working .inner_prod ( D, e_AD, n_ABC );
	// in 3D we do not need the special inner_prod linked to ext_prod_type
	if ( tmp < 0. )  return false;   // no need for high precision n_ABC !
	// if return, 'prod' remains unchanged, on purpose

	const std::vector < double > n_BAD =
		environ::ext_prod_type::get_normal_vector ( tag::domain_3d, tag::at_tri, BAD );
	// although the interface is looking "outwards",
	// towards the zone where we do not intend to build tetrahedra,
	// 'get_normal_vector' returns a normal vector pointing "inwards",
	// that is, towards the zone we intend to mesh
	prod = Manifold::working .inner_prod ( D, n_ABC, n_BAD );
	// in 3D we do not need the special inner_prod linked to ext_prod_type

	#if FRONTAL_VERBOSITY > 5  // never
	std::cout << "fit_for_one_tetra line 4671 " << prod << " " << DC2 << std::endl;
	#endif
	// formula below was obtained empirically
	// return  2. * prod + DC2 < 1.;
	// return  1.5 * prod + DC2 < 1.2;
	// return  prod + DC2 < 1.;
	return  prod + DC2 < 1.2;

}  // end of  fit_for_one_tetra
	
//------------------------------------------------------------------------------------------------------//


inline Cell fill_isolated_tetra_core // hidden in anonymous namespace
( Mesh & msh, Mesh & interf, Cell & ABC, Cell & BAD, Cell & DCB, Cell & ACD )	

// invoked from 'add_walls_first_layer' 'fill_isolated_tetrahedron'
	
{	Cell tetra ( tag::tetrahedron, ABC, BAD, DCB, ACD );
	tetra .add_to ( msh );

	#if FRONTAL_VERBOSITY > 1
	std::cout << "frontal-3d.cpp line 655, fill isolated tetrahedron" << std::endl << std::flush;
	#endif

	// we do not bother to eliminate segments and faces from sets/maps ***_fill_tetrah***
	// at the beginning of each cycle, we ask whether the cell still belongs to interf_copy
		
	#if FRONTAL_VERBOSITY > 1
	frontal_counter ++;
	#endif

	return tetra;

}  // end of  fill_isolated_tetra_core	

//------------------------------------------------------------------------------------------------------//
												

inline Cell fill_one_tetra_ground_core  // hidden in anonymous namespace
( Mesh & msh, Mesh & interf, Cell & ABC, Cell & BAD, Cell & ACD, Cell & BC, Cell & CD, Cell & DB,
                             Cell & A, Cell & B, Cell & C, Cell & D                              )

// return newly created tetrahedron
// in 'add_walls' first call, this triangle is kept in 'walls'
// in 'add_walls' second call, in 'fill_one_tetra_ground', this triangle is not used
	
{	// Cell DCB ( tag::triangle, BC .reverse ( tag::surely_exists ),
	//            DB .reverse ( tag::surely_exists ), CD .reverse ( tag::surely_exists ) );
	#if FRONTAL_VERBOSITY > 1
	std::cout << "frontal-3d.cpp line 683, fill one tetra ground, counter "
	          << frontal_counter << std::endl << std::flush;
	#endif

	Cell BCD ( tag::triangle, BC, CD, DB );
	BCD .add_to ( interf );
	Cell tetra ( tag::tetrahedron, ABC, BAD, ACD,
	                               BCD .reverse ( tag::does_not_exist, tag::build_now ) );
	tetra .add_to ( msh );

	// we do not bother to eliminate segments and faces from sets/maps ***_fill_tetrah***
	// once the faces have been removed from 'interf',
	// they will be eliminated by the 'if' at the beginning of the loop
		
	// add pairs (triangle,segment) to 'dangerous_cavities'
	Mesh::Iterator it_sides = BCD .boundary() .iterator
		( tag::over, tag::cells_of_max_dim, tag::orientation_compatible_with_mesh );
	for ( it_sides .reset(); it_sides .in_range(); it_sides ++ )
	{	Cell seg = * it_sides;
		Cell P = seg .tip();  // P in {B,C,D}
		// rotate around P :
		Cell tri = BCD;
		while ( true )
		{	Cell next_tri = interf .cell_in_front_of ( seg, tag::seen_from, tri, tag::surely_exists );
			if ( next_tri == BCD )  break;
			dangerous_cavities .push_back ( { tri, seg } );
			tri = next_tri;
			seg = tri .boundary() .cell_behind ( P, tag::surely_exists );                               }  }

	tri_fill_isolated .insert ( BCD );
	ver_fill_burried .insert (B);
	ver_fill_burried .insert (C);
	ver_fill_burried .insert (D);
	seg_fill_crest .insert ( { BCD, BC } );
	seg_fill_crest .insert ( { BCD, CD } );
	seg_fill_crest .insert ( { BCD, DB } );
	ver_fill_cavity .insert (B);
	ver_fill_cavity .insert (C);
	ver_fill_cavity .insert (D);
	seg_fill_two_crest .push_back ( { BCD, BC } );
	seg_fill_two_crest .push_back ( { BCD, CD } );
	seg_fill_two_crest .push_back ( { BCD, DB } );
						 
	#if FRONTAL_VERBOSITY > 1	
	frontal_counter ++;
	#endif

	return  tetra;

}  // end of  fill_one_tetra_ground_core

//------------------------------------------------------------------------------------------------------//


template < class environ >
inline Cell fill_one_tetra_crest_core    // hidden in anonymous namespace
( Mesh & msh, Mesh & interf,
  Cell & ABC, Cell & BAD,
  Cell AB, Cell & CA, Cell & AD, Cell & BC, Cell & DB,
  Cell & A, Cell & B, Cell & C, Cell & D              )
	
// create tetrahedron ABCD           interface before            interface after
	
//                                      A --- C                     A --- C
//                                      | \   |                     |   / |
//                                      |   \ |                     | /   |
//                                      D --- B                     D --- B		
	
// return newly built tetrahedron, or non-existent cell if none was built
// in 'add_walls' first call, this triangle is kept in 'walls'
// in 'add_walls' second call, 'fill_dangerous_cavity', 'fill_one_tetrahedron_crest',
//    this triangle is not used
	
// 'environ' describes the type of manifold we are working in
// and how we treat information about metric and exterior product
// environ::manif_type could be ManifoldNoWinding or ManifoldQuotient (defined in frontal.cpp)
// environ::metric_type may be one of ISRContainer::{Inactive,Active} (defined in frontal.cpp)
// environ::ext_prod_type may be ExtProd3d, 3dRev (defined in frontal.cpp)
	
{	Cell CD ( tag::non_existent );  // searching for a previously existing segment CD
	
	Mesh::Iterator it_around_C = interf .iterator ( tag::over_segments, tag::pointing_away_from, C );
	for ( it_around_C .reset(); it_around_C .in_range(); it_around_C ++ )
	{	Cell candidate_CD = * it_around_C;
		if ( candidate_CD .tip() == D )
		{	CD = candidate_CD;
			#if FRONTAL_VERBOSITY > 1
			std::cout << "using existing segment for creating one tetra, crest"
			          << std::endl << std::flush;
			#endif
			break;                                                               }  }
	
	bool CD_prev_exists = CD .exists();
	if ( CD_prev_exists )  assert ( not break_filament ( CD, interf ) );
	// such a filament must have been previously erased by 'update_filaments_near'
	else
	{	CD = Cell ( tag::segment, C .reverse ( tag::surely_exists ), D );
		environ::manif_type::set_winding_2 ( CD, CA, AD );
		update_filaments_near ( CD, interf );                              }
	#if FRONTAL_VERBOSITY > 1
	std::cout << "frontal-3d.cpp line 900, insert one tetra crest, counter "
	          << frontal_counter + 1 << ", CD ";
	const std::vector < double > e_CD = environ::manif_type::get_vector ( CD );
	std::cout << std::sqrt ( Manifold::working .sq_dist ( D, C, e_CD ) ) << std::endl << std::flush;
	#endif  // FRONTAL_VERBOSITY > 1
	Cell CAD ( tag::triangle, CA, AD, CD .reverse ( tag::build_if_not_exists ) );
	Cell BCD ( tag::triangle, BC, CD, DB );
	// unlike in 'fill_isolated_tetra_core', 'fill_one_tetra_ground_core',
	// here we use a segment CD which may exist previously (see above)
	// if it existed previously, we want to "cut" the interface at CD
	// this is the purpose of tag::force
	// tag::force only matters if CD_prev_exist
	CAD .add_to ( interf, tag::force );
	BCD .add_to ( interf, tag::force );
	Cell tetra ( tag::tetrahedron, ABC, BAD,
	                               CAD .reverse ( tag::does_not_exist, tag::build_now ),
	                               BCD .reverse ( tag::does_not_exist, tag::build_now ) );
	tetra .add_to ( msh );

	// we do not bother to eliminate segments and faces from sets/maps ***_fill_tetrah***
	// once the faces have been removed from 'interf',
	// they will be eliminated by the 'if' at the beginning of the loop

	// however, we need to eliminate segments from 'seg_fill_membrane_3s'
	// to prevent situations like the one depicted in
	// https://codeberg.org/cristian.barbarosie/manifem-manual, files 'tetrahedra-{04,05,06}.msh'
	seg_fill_membrane_3s .erase ( AB .get_positive() );
	seg_fill_membrane_3s .erase ( BC .get_positive() );
	seg_fill_membrane_3s .erase ( CA .get_positive() );
	seg_fill_membrane_3s .erase ( AD .get_positive() );
	seg_fill_membrane_3s .erase ( DB .get_positive() );

	// add pairs (triangle,segment) to 'dangerous_cavities'
	// since CD is a crest, we do not add CA, AD, DB, BC
	{  // just a block of code for hiding variable names
	// rotate around A :
	Cell tri = interf .cell_in_front_of ( CA, tag::seen_from, CAD, tag::surely_exists );
	Cell seg = tri .boundary() .cell_behind ( A, tag::surely_exists );
	while ( true )
	{	Cell next_tri = interf .cell_in_front_of ( seg, tag::seen_from, tri, tag::surely_exists );
		if ( next_tri == CAD )  break;
		dangerous_cavities .push_back ( { tri, seg } );
		tri = next_tri;
		seg = tri .boundary() .cell_behind ( A, tag::surely_exists );                               }
	// rotate around B :
	tri = interf .cell_in_front_of ( DB, tag::seen_from, BCD, tag::surely_exists );
	seg = tri .boundary() .cell_behind ( B, tag::surely_exists );
	while ( true )
	{	Cell next_tri = interf .cell_in_front_of ( seg, tag::seen_from, tri, tag::surely_exists );
		if ( next_tri == BCD )  break;
		dangerous_cavities .push_back ( { tri, seg } );
		tri = next_tri;
		seg = tri .boundary() .cell_behind ( B, tag::surely_exists );                               }
	}  // just a block of code

	if ( CD_prev_exists )  // the configuration will get more "tight" in this case
	{	tri_fill_isolated .insert ( CAD );
		tri_fill_isolated .insert ( BCD );
		ver_fill_burried .insert (C);
		ver_fill_burried .insert (D);       }
	ver_fill_burried .insert (A);
	ver_fill_burried .insert (B);
	seg_fill_membrane_3s .insert ( CD .get_positive() );
	seg_fill_crest .insert ( { CAD, CA } );
	seg_fill_crest .insert ( { CAD, AD } );
	seg_fill_crest .insert ( { BCD, BC } );
	seg_fill_crest .insert ( { BCD, DB } );
	seg_fill_membrane_2s .insert ( { BCD, CD } );
	seg_fill_membrane_2s .insert ( { CAD, CD .reverse ( tag::surely_exists ) } );
	ver_fill_cavity .insert (A);
	ver_fill_cavity .insert (B);
	seg_fill_two_crest .push_back ( { CAD, CA } );
	seg_fill_two_crest .push_back ( { CAD, AD } );
	seg_fill_two_crest .push_back ( { BCD, BC } );
	seg_fill_two_crest .push_back ( { BCD, CD } );
		
	#if FRONTAL_VERBOSITY > 1	
	frontal_counter ++;
	#endif

	return tetra;
	
}  // end of  fill_one_tetra_crest_core
	
//------------------------------------------------------------------------------------------------------//


template < class environ >
inline bool concave_edge   // hidden in anonymous namespace
( const Mesh & interf, const Cell & AB, const Cell & ABC, const Cell & BAD )

// 'environ' describes the type of manifold we are working in
// and how we treat information about metric and exterior product
// environ::manif_type could be ManifoldNoWinding or ManifoldQuotient (defined in frontal.cpp)
// environ::metric_type may be one of ISRContainer::{Inactive,Active} (defined in frontal.cpp)
// environ::ext_prod_type may be ExtProd3d, 3dRev (defined in frontal.cpp)
	
{	assert ( AB .belongs_to ( ABC .boundary(), tag::same_dim ) );
	assert ( AB .reverse ( tag::surely_exists )
	         .belongs_to ( BAD .boundary(), tag::same_dim ) );
	assert ( interf .cell_in_front_of ( AB, tag::seen_from, ABC, tag::surely_exists ) == BAD );
	Cell B = AB .tip();
	Cell DB = BAD .boundary() .cell_behind ( B, tag::surely_exists );

	const std::vector < double > e_DB = environ::manif_type::get_vector ( DB );
	const std::vector < double > n_ABC =
		environ::ext_prod_type::get_normal_vector ( tag::domain_3d, tag::at_tri, ABC );
	// although the interface is looking "outwards",
	// towards the zone where we do not intend to build tetrahedra,
	// 'get_normal_vector' returns a normal vector pointing "inwards",
	// that is, towards the zone we intend to mesh
	const double tmp = Manifold::working .inner_prod ( B, e_DB, n_ABC );
	// in 3D we do not need the special inner_prod linked to ext_prod_type
	// another possibility : think in terms of mixed product
	if ( tmp > 0. )  return false;
	const std::vector < double > n_BAD =
		environ::ext_prod_type::get_normal_vector ( tag::domain_3d, tag::at_tri, BAD );
	// although the interface is looking "outwards",
	// towards the zone where we do not intend to build tetrahedra,
	// 'get_normal_vector' returns a normal vector pointing "inwards",
	// that is, towards the zone we intend to mesh
	const double prod = Manifold::working .inner_prod ( B, n_ABC, n_BAD );
	// in 3D we do not need the special inner_prod linked to ext_prod_type

	return  prod < 0.2;

}  // end of  concave_edge

//------------------------------------------------------------------------------------------------------//


template < class environ >   // hidden in anonymous namespace
inline double angle_to_eliminate ( const Mesh & interf, const Cell & ABC, const Cell & AB )

// 'interf' is a STSI mesh of triangles
// we want to measure the angle between triangle ABC and its neighbour BAD

// 'environ' describes the type of manifold we are working in
// and how we treat information about metric and exterior product
// environ::manif_type could be ManifoldNoWinding or ManifoldQuotient (defined in frontal.cpp)
// environ::metric_type may be one of ISRContainer::{Inactive,Active} (defined in frontal.cpp)
// environ::ext_prod_type may be ExtProd3d, 3dRev (defined in frontal.cpp)
	
{	Cell BAD = interf .cell_in_front_of ( AB, tag::seen_from, ABC, tag::surely_exists );
	const std::vector < double > n_BAD = 
		environ::ext_prod_type::get_normal_vector ( tag::domain_3d, tag::at_tri, BAD );
	// although the interface is looking "outwards",
	// towards the zone where we do not intend to build tetrahedra,
	// 'get_normal_vector' returns a normal vector pointing "inwards",
	// that is, towards the zone we intend to mesh
	Cell B = AB .tip();
	Cell BC = ABC .boundary() .cell_in_front_of ( B, tag::surely_exists );
	assert ( AB .tip() == BC .base() .reverse ( tag::surely_exists ) );
	std::vector < double > e_AB = environ::manif_type::get_vector ( AB );
	std::vector < double > e_BC = environ::manif_type::get_vector ( BC );
	// BC = BC - < AB, BC > AB / |AB|^2
	const double coef = Manifold::working .inner_prod ( B, e_AB, e_BC ) /
	                    Manifold::working .sq_dist ( B, e_AB )           ;
	for ( size_t i = 0; i < frontal_nb_of_coords; i++ )  e_BC [i] -= coef * e_AB[i];
	// now e_BC is orthogonal to AB
	return  Manifold::working .inner_prod ( B, n_BAD, e_BC );                             }
	// another possibility : think in terms of mixed product
	
//------------------------------------------------------------------------------------------------------//
												

template < class environ >   // hidden in anonymous namespace
inline double angle_3d ( const Mesh & interf, const Cell & ABC, const Cell & AB )

// return a quantity between -3 and 3
// 0 means flat boundary
// a negative value means we are in a tight place (a lot has been meshed already)
// a positive value means there is a lot of free space (to be meshed)
// see paragraph 13.8 in the manual
// does this work as expected for an anisotropic metric ? what do we expect exactly ?
  
// 'interf' is a STSI mesh of triangles
// we want to measure the angle between triangle ABC and its neighbour BAD

// 'environ' describes the type of manifold we are working in
// and how we treat information about metric and exterior product
// environ::manif_type could be ManifoldNoWinding or ManifoldQuotient (defined in frontal.cpp)
// environ::metric_type may be one of ISRContainer::{Inactive,Active} (defined in frontal.cpp)
// environ::ext_prod_type may be ExtProd3d, 3dRev (defined in frontal.cpp)
	
{	Cell BAD = interf .cell_in_front_of ( AB, tag::seen_from, ABC, tag::surely_exists );
	const std::vector < double > n_ABC = 
		environ::ext_prod_type::get_normal_vector ( tag::domain_3d, tag::at_tri, ABC );
	const std::vector < double > n_BAD = 
		environ::ext_prod_type::get_normal_vector ( tag::domain_3d, tag::at_tri, BAD );
	// although the interface is looking "outwards",
	// towards the zone where we do not intend to build tetrahedra,
	// 'get_normal_vector' returns a normal vector pointing "inwards",
	// that is, towards the zone we intend to mesh
	Cell B = AB .tip();
	Cell BC = ABC .boundary() .cell_in_front_of ( B, tag::surely_exists );
	assert ( AB .tip() == BC .base() .reverse ( tag::surely_exists ) );
	std::vector < double > e_AB = environ::manif_type::get_vector ( AB );
	std::vector < double > e_BC = environ::manif_type::get_vector ( BC );
	// BC = BC - < AB, BC > AB / |AB|^2
	const double coef = Manifold::working .inner_prod ( B, e_AB, e_BC ) /
	                    Manifold::working .sq_dist ( B, e_AB )           ;
	for ( size_t i = 0; i < frontal_nb_of_coords; i++ )  e_BC [i] -= coef * e_AB[i];
	normalize_vector_approx ( B, e_BC );
	// now e_BC is orthogonal to AB (corresponds to minus tau_AB in the manual)
	const double prod_nn =   Manifold::working .inner_prod ( B, n_BAD, n_ABC );
	const double prod_tn = - Manifold::working .inner_prod ( B, n_BAD, e_BC );
	if ( prod_tn < 0. )
		if ( prod_nn < 0. )  return  prod_nn -2.;               // case (a)
		else                 return  prod_nn + prod_tn - 1.;    // case (b)
	else
		if ( prod_nn > 0. )  return  1. - prod_nn + prod_tn;    // case (c)
		else                 return  2. - prod_nn;              // case (d)
	
}  // end of  angle_3d
	
//------------------------------------------------------------------------------------------------------//
												

template < class environ >  // hidden in anonymous namespace
inline bool is_crest ( const Mesh & interf, const Cell & ABC, const Cell & AB )
// to eliminate

// 'interf' is a STSI mesh of triangles
// we want to measure the angle between triangle ABC and its neighbour BAD

// 'environ' describes the type of manifold we are working in
// and how we treat information about metric and exterior product
// environ::manif_type could be ManifoldNoWinding or ManifoldQuotient (defined in frontal.cpp)
// environ::metric_type may be one of ISRContainer::{Inactive,Active} (defined in frontal.cpp)
// environ::ext_prod_type may be ExtProd3d, 3dRev (defined in frontal.cpp)
	
{	Cell BAD = interf .cell_in_front_of ( AB, tag::seen_from, ABC, tag::surely_exists );
	const std::vector < double > n_BAD = 
		environ::ext_prod_type::get_normal_vector ( tag::domain_3d, tag::at_tri, BAD );
	// although the interface is looking "outwards",
	// towards the zone where we do not intend to build tetrahedra,
	// 'get_normal_vector' returns a normal vector pointing "inwards",
	// that is, towards the zone we intend to mesh
	// for this normal vector we do not need high precision,
	// in particular it does not have to be unitary
	// perhaps define a different function for computing low-precision normals
	// another possibility : think in terms of mixed product
	Cell B = AB .tip();
	Cell BC = ABC .boundary() .cell_in_front_of ( B, tag::surely_exists );
	assert ( AB .tip() == BC .base() .reverse ( tag::surely_exists ) );
	std::vector < double > e_BC = environ::manif_type::get_vector ( BC );
	return  Manifold::working .inner_prod ( B, n_BAD, e_BC ) > 0.;                        }

// only the sign of the product is relevant
// for computing the (cosine of the) angle between the two faces
// more compicated computations are needed -- see function 'angle'
	
//------------------------------------------------------------------------------------------------------//
												

inline size_t nb_of_triangles_around   // hidden in anonymous namespace
( const Mesh & interf, const Cell & A, const Cell & ABC )

// count neighbours of A, starting from ABC

{	assert ( ABC .belongs_to ( interf, tag::same_dimension, tag::orientation_compatible_with_mesh ) );
	assert ( A .belongs_to ( ABC .boundary(), tag::cell_has_low_dim ) );

	size_t counter = 0;
	Cell tri = ABC;
	while ( true )
	{	counter ++;
		Cell seg = tri .boundary(). cell_behind ( A, tag::surely_exists );
		tri = interf .cell_in_front_of ( seg, tag::seen_from, tri, tag::surely_exists );
		if ( tri == ABC )  break;                                                         }

	return counter;                                                                                     }
	
//------------------------------------------------------------------------------------------------------//
												

inline size_t nb_of_triangles_around_with_iterator   // hidden in anonymous namespace
( const Mesh & interf, const Cell & A, const Cell & ABC )

// count neighbours of A, starting from ABC
	
// this function should give the same result as 'nb_of_triangles_around' above
// but it doesn't because the iterator does not work correctly for STSI meshes
// it stops at a singular segment
	
{	assert ( ABC .belongs_to ( interf, tag::same_dimension, tag::orientation_compatible_with_mesh ) );
	assert ( A .belongs_to ( ABC .boundary(), tag::cell_has_low_dim ) );

	size_t counter = 0;
	Mesh::Iterator it = interf .iterator
		( tag::over_cells_of_max_dim, tag::orientation_compatible_with_mesh,
		  tag::around, A, tag::require_order                                );
	for ( it .reset ( tag::start_at, ABC ); it .in_range(); it ++ )  counter ++;

	return counter;                                                                                     }
	
//------------------------------------------------------------------------------------------------------//
												

inline bool four_triangles_around   // hidden in anonymous namespace
( const Mesh & interf, const Cell & A, const Cell & C,
  const Cell & AC, const Cell & ABC, const Cell & ACD )

// analyse the configuration around A
	
{	assert ( interf .cell_behind      ( AC, tag::seen_from, ABC, tag::surely_exists ) == ACD );
	assert ( interf .cell_in_front_of ( AC, tag::seen_from, ACD, tag::surely_exists ) == ABC );
	Cell AB = ABC .boundary() .cell_in_front_of ( A, tag::surely_exists );
	Cell AEB = interf .cell_in_front_of ( AB, tag::seen_from, ABC, tag::surely_exists );
	Cell AE = AEB .boundary() .cell_in_front_of ( A, tag::surely_exists );
	Cell DA = ACD .boundary() .cell_behind ( A, tag::surely_exists );
	assert ( DA != AE .reverse ( tag::surely_exists ) );
	Cell ADF = interf .cell_in_front_of ( DA, tag::seen_from, ACD, tag::surely_exists );
	Cell FA = ADF .boundary() .cell_behind ( A, tag::surely_exists );  // F == E ?
	
	if ( FA == AE .reverse ( tag::surely_exists ) )  return true;

	// else  // more than four triangles around A
	return false;                                                                                }

//------------------------------------------------------------------------------------------------------//


template < class environ >
inline Cell fill_isolated_tetrahedron  // hidden in anonymous namespace
( Mesh & msh, Mesh & interf, const tag::MobileVertex &, const Cell & mobile_vertex )
	
// try to fill an isolated tetrahedron
// return new tetrahedron, or non-existent cell if did not fill one

// 'environ' describes the type of manifold we are working in
// and how we treat information about metric and exterior product
// environ::manif_type could be ManifoldNoWinding or ManifoldQuotient (defined in frontal.cpp)
// environ::metric_type may be one of ISRContainer::{Inactive,Active} (defined in frontal.cpp)
// environ::ext_prod_type may be ExtProd3d, 3dRev (defined in frontal.cpp)
	
// 'msh' is a Mesh::Fuzzy made of tetrahedra (being built now)
// 'interf' is a Mesh::STSI made of triangles
	
{	Cell did_fill ( tag::non_existent );
	
	for ( std::set < Cell > ::iterator it = tri_fill_isolated .begin();
	      it != tri_fill_isolated .end(); it = tri_fill_isolated .erase ( it ) )

	{	Cell ABC = *it;
		if ( not ABC .belongs_to ( interf, tag::same_dim, tag::orientation_compatible_with_mesh ) )
			continue;    // to next triangle in tri_fill_isolated

		Mesh::Iterator itt = ABC .boundary() .iterator
			( tag::over_cells_of_max_dim, tag::orientation_compatible_with_mesh, tag::require_order );
		itt .reset();  assert ( itt .in_range() );
		Cell AB = *itt;
		itt++;  assert ( itt .in_range() );
		Cell BC = *itt;
		itt++;  assert ( itt .in_range() );
		Cell CA = *itt;
		itt++;  assert ( not itt .in_range() );
		Cell A = CA .tip();
		Cell B = AB .tip();
		Cell C = BC .tip();
		Cell BAD = interf .cell_in_front_of ( AB, tag::seen_from, ABC, tag::surely_exists );
		Cell DCB = interf .cell_in_front_of ( BC, tag::seen_from, ABC, tag::surely_exists );
		Cell ACD = interf .cell_in_front_of ( CA, tag::seen_from, ABC, tag::surely_exists );
		if ( BAD == DCB )  // ABC is a membrane
			continue;  // to next triangle in tri_fill_isolated
		if ( BAD == ACD )  // ABC is a membrane
			continue;  // to next triangle in tri_fill_isolated
		if ( DCB == ACD )  // ABC is a membrane
			continue;  // to next triangle in tri_fill_isolated

		Cell AD  = BAD .boundary() .cell_in_front_of ( A, tag::surely_exists );
		Cell D = AD .tip();
		if ( not D .belongs_to ( DCB .boundary() ) )  continue;  // to next triangle in tri_fill_isolated
		if ( not D .belongs_to ( ACD .boundary() ) )  continue;  // to next triangle in tri_fill_isolated
		assert ( AD == ACD .boundary() .cell_in_front_of ( D, tag::surely_exists )
		               .reverse ( tag::surely_exists )                             );
		Cell BD  = DCB .boundary() .cell_in_front_of ( B, tag::surely_exists );
		assert ( BD == BAD .boundary() .cell_in_front_of ( D, tag::surely_exists )
		               .reverse ( tag::surely_exists )                             );
		Cell CD  = ACD .boundary() .cell_in_front_of ( C, tag::surely_exists );
		assert ( CD == DCB .boundary() .cell_in_front_of ( D, tag::surely_exists )
		               .reverse ( tag::surely_exists )                             );

		if ( false )  {
		// check D is on the correct side (normal points to it)
		const std::vector < double > e_AD = environ::manif_type::get_vector ( AD );
		const std::vector < double > n_ABC =
			environ::ext_prod_type::get_normal_vector ( tag::domain_3d, tag::at_tri, ABC );
		// although the interface is looking "outwards",
		// towards the zone where we do not intend to build tetrahedra,
		// 'get_normal_vector' returns a normal vector pointing "inwards",
		// that is, towards the zone we intend to mesh
		// in 3D we do not need the special inner_prod linked to ext_prod_type
		const double prod = Manifold::working .inner_prod ( D, e_AD, n_ABC );
		assert ( prod > 0. );
		if ( prod < 0.)  continue;    // to next triangle in tri_fill_isolated
		}  // end of  if false

		Cell ABC_alt = remove_face ( ABC, interf );
		Cell BAD_alt = remove_face ( BAD, interf );
		Cell DCB_alt = remove_face ( DCB, interf );
		Cell ACD_alt = remove_face ( ACD, interf );
		// ABC .remove_from ( interf );
		// BAD .remove_from ( interf );
		// DCB .remove_from ( interf );
		// ACD .remove_from ( interf );
		did_fill = fill_isolated_tetra_core ( msh, interf, ABC_alt, BAD_alt, DCB_alt, ACD_alt );
		  // if 'fill_one_tetra_ground' is invoked from within 'add_walls',
		  // one vertex may have not been added to the cloud, aka 'node_container'
		if ( A != mobile_vertex )
		  environ::manif_type::node_container .cond_remove ( A, interf );
		if ( B != mobile_vertex )
			environ::manif_type::node_container .cond_remove ( B, interf );
		if ( C != mobile_vertex )
			environ::manif_type::node_container .cond_remove ( C, interf );
		if ( D != mobile_vertex )
			environ::manif_type::node_container .cond_remove ( D, interf );
		environ::metric_type::cond_remove ( A, interf );
		environ::metric_type::cond_remove ( B, interf );
		environ::metric_type::cond_remove ( C, interf );
		environ::metric_type::cond_remove ( D, interf );                                           }

	// the case of a hole with eight triangular sides, diamond-shaped,
	// is dealt with by 'fill_cavity_ground', followed by other filling operations

	return did_fill;

}  // end of fill_isolated_tetrahedron
	
//------------------------------------------------------------------------------------------------------//


template < class environ >
inline Cell fill_one_tetra_ground   // hidden in anonymous namespace
( Mesh & msh, Mesh & interf, const tag::MobileVertex &, const Cell & mobile_vertex )
	
// try to fill a new tetrahedron (like filling a hole in the ground)
// just by joining three neighbour vertices by a new triangle
// return newly built tetrahedron, or non-existent cell if none was built

// 'msh' is a Mesh::Fuzzy made of tetrahedra (being built now)
// 'interf' is a Mesh::STSI made of triangles
	
// 'environ' describes the type of manifold we are working in
// and how we treat information about metric and exterior product
// environ::manif_type could be ManifoldNoWinding or ManifoldQuotient (defined in frontal.cpp)
// environ::metric_type may be one of ISRContainer::{Inactive,Active} (defined in frontal.cpp)
// environ::ext_prod_type may be ExtProd3d, 3dRev (defined in frontal.cpp)
	
{	assert ( tri_fill_isolated  .empty() );

	// here we only erase vertices from 'ver_fill_burried' if they do not belong to 'interf'
	// we erase the others later, in 'fill_two_tetra_ground'
	for ( std::set < Cell > ::iterator
	      it = ver_fill_burried .begin(); it != ver_fill_burried .end();  )
		
	{	Cell A = * it;
		assert ( A .dim() == 0 );
		if ( not A .belongs_to ( interf ) )
		{	it = ver_fill_burried .erase ( it );  continue;  }  // to next vertex in ver_fill_burried
		
		std::set < Cell > already_analysed;

		label_search_for_ABC_three :
		// we try to fill a tetrahedron around A
		// below we invoke an iterator around A without requiring order
		// it sweeps over all triangles, even if some segment is singular
		Cell ABC ( tag::non_existent );
		bool has_filament = false;
		{ // just a block of code for hiding 'itt'
		Mesh::Iterator itt = interf .iterator
			( tag::over_cells_of_max_dim, tag::orientation_compatible_with_mesh, tag::around, A );
		for ( itt .reset(); itt .in_range(); itt++ )
		{	Cell tri = * itt;
			// if 'tri' is a filament, we don't want to fill around A
			if ( tri .boundary() .number_of ( tag::segments ) != 3 )
			{	assert ( tri .boundary() .number_of ( tag::segments ) == 2 );
				has_filament = true;                                           }
			if ( already_analysed .find ( tri ) == already_analysed .end() )
			{	ABC = tri;  break;  }                                             }
		} // just a block of code

		if ( has_filament )       {  it++;  continue;  }  //  ??
		if ( not ABC .exists() )  {  it++;  continue;  }  // to next vertex in ver_fill_burried

		// below, it would be nice to use an ordered iterator
		// which only sweeps over faces connected to each other
		// but these iterators do not work for STSI meshes - a pitty

		assert ( already_analysed .find ( ABC ) == already_analysed .end() );
		already_analysed .insert ( ABC );
		Mesh ABC_bdry = ABC .boundary();
		Cell AB = ABC_bdry .cell_in_front_of ( A, tag::surely_exists );
		Cell B = AB .tip();
		Cell BC = ABC_bdry .cell_in_front_of ( B, tag::surely_exists );
		Cell C = BC .tip();
		Cell CA = ABC_bdry .cell_in_front_of ( C, tag::surely_exists );
		assert ( CA .tip() == A );

		assert ( ABC .belongs_to ( interf, tag::same_dim, tag::orientation_compatible_with_mesh ) );
		Cell BAD = interf .cell_in_front_of ( AB, tag::seen_from, ABC, tag::surely_exists );
		assert ( BAD .belongs_to ( interf, tag::same_dim, tag::orientation_compatible_with_mesh ) );
		assert ( BAD .boundary() .number_of ( tag::segments ) == 3 );
		assert ( already_analysed .find ( BAD ) == already_analysed .end() );
		already_analysed .insert ( BAD );
		Cell ACD = interf .cell_in_front_of ( CA, tag::seen_from, ABC, tag::surely_exists );
		assert ( ACD .belongs_to ( interf, tag::same_dim, tag::orientation_compatible_with_mesh ) );
		assert ( ACD .boundary() .number_of ( tag::segments ) == 3 );
		if ( BAD == ACD )  // membrane touching the interface in A
		  goto label_search_for_ABC_three;  // look for other triangles around A
		assert ( already_analysed .find ( ACD ) == already_analysed .end() );
		already_analysed .insert ( ACD );

		Cell AD = BAD .boundary() .cell_in_front_of ( A, tag::surely_exists );
		Cell DB = BAD .boundary() .cell_behind ( B, tag::surely_exists );
		Cell CD = ACD .boundary() .cell_in_front_of ( C, tag::surely_exists );
		Cell DA = ACD .boundary() .cell_behind ( A, tag::surely_exists );
		// notation is misleading : DA is not always the reverse of AD
		// DA is the reverse of AD only if there is a tetrahedron to close, see below
		// now we check D is on the correct side (normal points to it)
		bool wrong_orientation = false;
		const std::vector < double > n_ABC =
			environ::ext_prod_type::get_normal_vector ( tag::domain_3d, tag::at_tri, ABC );
		// although the interface is looking "outwards",
		// towards the zone where we do not intend to build tetrahedra,
		// 'get_normal_vector' returns a normal vector pointing "inwards",
		// that is, towards the zone we intend to mesh
		const std::vector < double > e_AD = environ::manif_type::get_vector ( AD );
		const std::vector < double > e_DB = environ::manif_type::get_vector ( DB );
		std::vector < double > e ( frontal_nb_of_coords );
		for ( size_t i = 0; i < frontal_nb_of_coords; i++ )  e[i] = e_AD [i] - e_DB [i];
		double prod = Manifold::working .inner_prod ( A, e, n_ABC );
		if ( prod < 0.)  wrong_orientation = true;  // a sort of reversed cavity
		else
		{	const std::vector < double > e_CD = environ::manif_type::get_vector ( CD );
			const std::vector < double > e_DA = environ::manif_type::get_vector ( DA );  // duplicated !
			for ( size_t i = 0; i < frontal_nb_of_coords; i++ )  e[i] = e_CD [i] - e_DA [i];
			prod = Manifold::working .inner_prod ( A, e, n_ABC );  // a sort of reversed cavity
			if ( prod < 0.)  wrong_orientation = true;                                        }

		{  // just a block of code for hiding variable names
		Cell next_tri = interf .cell_in_front_of
			( DA, tag::seen_from, ACD, tag::surely_exists );
		// a hole with four triangular sides is dealt with below
		// a hole with more than four triangular sides is dealt with in 'fill_cavity_ground'
		if ( ( next_tri != BAD ) or wrong_orientation )
			// no tetrahedron to close, search for other faces ABC
			// but before that, insert more faces in 'already_analysed'
		{	while ( next_tri != BAD )
			{	assert ( next_tri .belongs_to
				         ( interf, tag::same_dim, tag::orientation_compatible_with_mesh ) );
				assert ( next_tri .boundary() .number_of ( tag::segments ) == 3 );
				assert ( already_analysed .find ( next_tri ) == already_analysed .end() );
				already_analysed .insert ( next_tri );
				Cell next_seg = next_tri .boundary() .cell_behind ( A, tag::surely_exists );
				next_tri = interf .cell_in_front_of
					( next_seg, tag::seen_from, next_tri, tag::surely_exists );                       }
			goto label_search_for_ABC_three;                                                          }
		}  // just a block of code

		assert ( DA == AD .reverse ( tag::surely_exists ) );
		Cell D =  AD .tip();
		assert ( CD .tip() == D );  // we want to close the tetrahedron if fit_for_one_tetra
		
		Cell ABC_alt = remove_face ( ABC, interf );
		Cell BAD_alt = remove_face ( BAD, interf );
		Cell ACD_alt = remove_face ( ACD, interf );
		// ABC .remove_from ( interf );
		// BAD .remove_from ( interf );
		// ACD .remove_from ( interf );

		Cell tetra = fill_one_tetra_ground_core
			( msh, interf, ABC_alt, BAD_alt, ACD_alt, BC, CD, DB, A, B, C, D );

		if ( A != mobile_vertex )
			// if 'fill_one_tetra_ground' is invoked from within 'add_walls',
			// A may have not been added to the cloud, aka 'node_container'
			environ::manif_type::node_container .cond_remove ( A, interf );
		environ::metric_type::cond_remove ( A, interf );
			
		#if FRONTAL_VERBOSITY > 1
		if ( frontal_counter < frontal_counter_max )  frontal_counter ++;
		#endif

		return tetra;                                                                                    }
	
	return  Cell ( tag::non_existent );  // did nothing

}  // end of  fill_one_tetra_ground

//------------------------------------------------------------------------------------------------------//


template < class environ >   // hidden in anonymous namespace
inline Cell fill_one_tetrahedron_crest
( Mesh & msh, Mesh & interf, const tag::MobileVertex &, const Cell & mobile_vertex )
	
// try to fill a new tetrahedron using two existing triangles having a common edge
// just join two vertices by a new segment and build two new triangles
// the set 'seg_fill_crest' contains a pair (triangle, side)
// return newly built tetrahedron, or non-existent cell if none was built

// 'msh' is a Mesh::Fuzzy made of tetrahedra (being built now)
// 'interf' is a Mesh::STSI made of triangles
	
// 'environ' describes the type of manifold we are working in
// and how we treat information about metric and exterior product
// environ::manif_type could be ManifoldNoWinding or ManifoldQuotient (defined in frontal.cpp)
// environ::metric_type may be one of ISRContainer::{Inactive,Active} (defined in frontal.cpp)
// environ::ext_prod_type may be ExtProd3d, 3dRev (defined in frontal.cpp)
	
{	assert ( tri_fill_isolated .empty() );
	// assert ( dangerous_cavities   .empty() );
	// assert ( seg_fill_membrane_3s .empty() );
	// assert ( ver_fill_burried     .empty() );

	label_restart :
	
	// preliminary search for the shortest segment to be created
	std::set < std::pair < Cell, Cell > > ::iterator kept_it = seg_fill_crest .end();
	double min_prod = 20.;
	for ( std::set < std::pair < Cell, Cell > > ::iterator
	      it = seg_fill_crest .begin(); it != seg_fill_crest .end();  )

	{	Cell ABC = it->first;
		if ( not ABC .belongs_to ( interf, tag::same_dim, tag::orientation_compatible_with_mesh ) )
		{	it = seg_fill_crest .erase ( it );  continue;  }  // to next segment in seg_fill_crest

		Cell AB = it->second;
		assert ( AB .belongs_to ( ABC .boundary(), tag::same_dim, tag::orientation_compatible_with_mesh ) );
		// if ( ABC .boundary() .number_of ( tag::segments ) != 3 )
		// {	assert ( ABC .boundary() .number_of ( tag::segments ) == 2 );
		// 	continue;                           }  // to next segment in seg_fill_crest
		assert ( ABC .boundary() .number_of ( tag::segments ) == 3 );
		Cell BAD = interf .cell_in_front_of ( AB, tag::seen_from, ABC, tag::surely_exists );
		assert ( BAD .boundary() .number_of ( tag::segments ) == 3 );

		double prod = 0.;  // will be overwritten if fit,
		                   // we set it to zero just to avoid compilation warnings
		if ( danger_of_flat_tetra < environ >
		     ( interf, AB, ABC, BAD, tag::mobile_vertex, mobile_vertex, tag::both_directions ) or
		     not fit_for_one_tetra < environ > ( interf, AB, ABC, BAD, prod )                     )
		     // 'fit_for_one_tetra' computes 'prod' if fit
		{	it = seg_fill_crest .erase ( it );  continue;  }  // to next segment in seg_fill_crest
		
		if ( prod < min_prod )
		{	kept_it = it;  min_prod = prod;  }
		it++;
	}  // end of for (preliminary search)

	if ( kept_it == seg_fill_crest .end() )  return  Cell ( tag::non_existent );
		
	Cell ABC = kept_it->first;
	Cell AB = kept_it->second;
	seg_fill_crest .erase ( kept_it );
	Cell B = AB .tip();
	Cell A = AB .base() .reverse ( tag::surely_exists );
	Cell BAD = interf .cell_in_front_of ( AB, tag::seen_from, ABC, tag::surely_exists );
	Cell BC = ABC .boundary() .cell_in_front_of ( B, tag::surely_exists );
	Cell C = BC .tip();
	Cell CA = ABC .boundary() .cell_behind ( A, tag::surely_exists );
	assert ( CA .base() .reverse() == C );
	Cell AD = BAD .boundary() .cell_in_front_of ( A, tag::surely_exists );
	Cell D = AD .tip();
	Cell DB = BAD .boundary() .cell_behind ( B, tag::surely_exists );
	assert ( DB .base() .reverse() == D );
	
	if ( C == D )  goto label_restart;  // ABC and BAD are two faces of a membrane_2s

	// if CD exists already, either as a filament or (very rarely) as a side of a triangle
	// 'fill_one_tetra_crest_core' will detect CD exists already and will destroy the filament

	Cell ABC_alt = remove_face ( ABC, interf );
	// ABC .remove_from ( interf )
	Cell BAD_alt = remove_face ( BAD, interf );
	// BAD .remove_from ( interf )
	return  fill_one_tetra_crest_core < environ >
			( msh, interf, ABC_alt, BAD_alt, AB, CA, AD, BC, DB, A, B, C, D );	
	
}  // end of  fill_one_tetrahedron_crest
	
//------------------------------------------------------------------------------------------------------//


template < class environ >
inline void eliminate_far_vertices      // hidden in anonymous namespace
( const Cell & P, Mesh & interf,
  typename environ::manif_type::type_of_walls & walls,
  union_of < typename environ::manif_type::set_of_winding_cells > & vertices, double dist_sq )

{	// assert ( false );  // to implement
};

//------------------------------------------------------------------------------------------------------//


template < class environ >
bool compare_two_lists_3d      // hidden in anonymous namespace
( Mesh & msh, Mesh & interf, const Cell & P,
  typename environ::manif_type::type_of_walls & walls,
  union_of < typename environ::manif_type::set_of_winding_cells > & already,
  std::list < typename environ::manif_type::winding_cell > & candidates, double dist_sq )

// return false if 'candidates' included in 'already'

// 'msh' is a Mesh::Fuzzy made of tetrahedra (being built now)
// 'interf' is a Mesh::STSI made of triangles

// 'walls' is a set of triangles if manif non-winding
// it is a map (triangle, vertex with winding) if manif winding

// 'already' contains vertices already taken into account when locating a new vertex
// has two components, one coming from walls, other of freelancers
	
// 'candidates' contains vertices close enough (sqrt of two) to the newly created vertex P
// 'candidates' is the result of a call to 'cloud .find_close_neighbours_of (P)'
// one of the candidates not already in 'already' (the closest to P)
// will be added to second component of 'already', then return true

// we do not assume that 'already' is included in 'candidates'
// because 'candidates' may have shrunk along the relocation process

// why the last argument 'dist_sq' ?
// if the metric is anisotropic, the list 'candidates', produced by 'find_close_neighbours_of',
// may contain vertices which are too far from P
// so we filter it with 'dist_sq'

// even for an isotropic, non-uniform, metric, some vertices may be too far from P
// because here we use the distance averaged at P and V
// while in 'find_close_neighbours_of' we use inner_spectral_radius at P only
	
{	// first, we erase from 'candidates' all cells belonging to 'already'

	// implement this search the other way around !
	// iterate over 'candidates' and at each step use 'already .has_elem'
	for ( typename union_of < typename environ::manif_type::set_of_winding_cells >
	      ::iterator it_al = already .begin(); it_al .in_range(); it_al ++         )
	{	for ( typename std::list < typename environ::manif_type::winding_cell > ::iterator
		      it_ca = candidates .begin(); it_ca != candidates .end();           )
			// if ( *it_al == *it_ca )   should work but does not ...
			// if ( it_al->first == it_ca->first )  does not work if manif_type is no-winding
			if ( environ::manif_type::same_cell ( *it_al, *it_ca ) )
			{	it_ca = candidates .erase ( it_ca );  break;  }
			else  it_ca++;
		// assert ( found )  // we cannot assume that 'already' is included in 'candidates'
		if ( candidates .empty() ) return false;  // 'candidates' included in 'already'
	} // end of for 'already'

	#if FRONTAL_VERBOSITY > 1
	std::cout << " a " << already .size() << " " << candidates .size() << ", ";
	#endif

	assert ( already .components .size() == 2 );
	typename environ::manif_type::set_of_winding_cells & vertices_from_walls = * already .components [0];
	typename environ::manif_type::set_of_winding_cells & other_vertices = * already .components [1];
	
	// we expand 'vertices_from_walls' following neighbourhood relations in 'msh'
	// vertices in the resulting list will be eliminated from the list of candidates
	std::set < Cell > inadmissible_vertices;
	for ( typename environ::manif_type::set_of_winding_cells ::const_iterator
	      it = vertices_from_walls .begin() ; it != vertices_from_walls .end(); it ++ )
		inadmissible_vertices .insert ( environ::manif_type::remove_winding ( *it ) );
	for ( size_t i = 0; i < 2; i++ )  // repeat expansion loop
	for ( std::set < Cell > ::const_iterator
	      it = inadmissible_vertices .begin(); it != inadmissible_vertices .end(); it ++ )
	{	Cell V = * it;
		// at the beginning of the mesh generation process, there are no tetrahedra around V yet
		if ( V .belongs_to ( msh, tag::cell_has_low_dim ) )
		{	Mesh::Iterator itt = msh .iterator ( tag::over_vertices, tag::connected_to, V );
			for ( itt .reset(); itt .in_range(); itt ++ )
				inadmissible_vertices .insert ( * itt );                                        }
		// some of the vertices may have been left behind the interface
		if ( V .belongs_to ( interf, tag::cell_has_low_dim ) )
		{	Mesh::Iterator itt = interf .iterator ( tag::over_vertices, tag::connected_to, V );
			for ( itt .reset(); itt .in_range(); itt ++ )
				inadmissible_vertices .insert ( * itt );                                           }  }
	for ( typename std::list < typename environ::manif_type::winding_cell >
	      ::iterator it_ca = candidates .begin(); it_ca != candidates .end();  )
	{	typename environ::manif_type::winding_cell VV = * it_ca;
		Cell V = environ::manif_type::remove_winding (VV);
  	if ( inadmissible_vertices .find (V) != inadmissible_vertices .end() )
			it_ca = candidates .erase ( it_ca );
		else  it_ca++;                                                          }
		
	#if FRONTAL_VERBOSITY > 1
	std::cout << "b " << already .size() << " " << candidates .size() << ", ";
	#endif

	// before adding a vertex to 'already', check it against all walls
	// the vertex should be on the correct side of the wall
	for ( typename std::list < typename environ::manif_type::winding_cell >
	      ::iterator it_ca = candidates .begin(); it_ca != candidates .end();  )
	{	typename environ::manif_type::winding_cell VV = * it_ca;
		Cell V = environ::manif_type::remove_winding ( VV );
		const std::vector < double > e_PV = environ::manif_type::get_vector ( P, VV );
		bool eliminate_candidate = false;
		for ( typename environ::manif_type::type_of_walls::const_iterator
		      it_w = walls .begin(); it_w != walls .end(); it_w ++       )
		{	const Cell tri = environ::manif_type::get_tri_from_wall ( it_w );
			assert ( tri .dim() == 2 );
			const std::vector < double > e_PW = environ::manif_type::get_vector_from_wall ( P, it_w );
			// e_PW [i] = W_i - P_i, W is some vertex of 'tri'
			std::vector < double > e_WV ( frontal_nb_of_coords, 0. );
			for ( size_t i = 0; i < frontal_nb_of_coords; i++ )  e_WV [i] = e_PV [i] - e_PW [i];
			std::vector < double > norm_tri =
				environ::ext_prod_type::get_normal_vector ( tag::domain_3d, tag::at_tri, tri );
			// although the interface is looking "outwards",
			// towards the zone where we do not intend to build tetrahedra,
			// 'get_normal_vector' returns a normal vector pointing "inwards",
			// that is, towards the zone we intend to mesh
			// we only need the direction (and sense) ot 'norm_tri', no need to normalize it !
			// actually, we want the mixed product with e_WV !
			if ( Manifold::working .inner_prod ( V, e_WV, norm_tri ) < 0. )
			{	eliminate_candidate = true;  break;  }                                                     }
  	if ( eliminate_candidate )  it_ca = candidates .erase ( it_ca );
		else  it_ca++;                                                                                    }

	#if FRONTAL_VERBOSITY > 1
	std::cout << "c " << already .size() << " " << candidates .size() << ", ";
	#endif
	if ( candidates .empty() ) return false;
	
	double d2min = 1000.;
	typename std::list < typename environ::manif_type::winding_cell > ::iterator kept;
	for ( typename std::list < typename environ::manif_type::winding_cell > ::iterator
	      it_ca = candidates .begin(); it_ca != candidates .end(); it_ca ++  )
	{	typename environ::manif_type::winding_cell V = * it_ca;
		double d2 = environ::manif_type::sq_dist::working_manif ( P, V );  // rescaled metric
		if ( d2 < d2min )
		{	d2min = d2;  kept = it_ca;  }                          }
	if ( d2min > dist_sq )
	{	// list 'candidates' contains vertices too far from P, due to the anisotropy
		#if FRONTAL_VERBOSITY > 1
		std::cout << "(" << std::sqrt(d2min) << ", " << candidates .size() << " vertices too far) ";
		#endif
		return false;                                                                                 }

	assert ( kept != candidates .end() );
	typename environ::manif_type::winding_cell QQ = * kept;
	Cell Q = environ::manif_type::remove_winding ( QQ );

	#if FRONTAL_VERBOSITY > 1
	std::cout << std::sqrt ( d2min ) << " [ ";
	for ( size_t i = 0; i < frontal_nb_of_coords; i++ )
	{	const Function & x = Manifold::working .coordinates() [i];
		std::cout << x(Q) << " ";                                   }
	std::cout << "] ";
	#endif  // FRONTAL_VERBOSITY > 1
		
	other_vertices .insert ( QQ );

	// we add to 'walls' triangles neighbour to Q
	// if all their vertices belong to 'already'
	Mesh::Iterator it_around_Q = interf .iterator
		( tag::over_cells_of_max_dim, tag::orientation_compatible_with_mesh, tag::around, Q );
	for ( it_around_Q .reset(); it_around_Q .in_range(); it_around_Q ++ )

	{	Cell tri = * it_around_Q;
		assert ( tri .dim() == 2 );
		// ignore filaments :
		if ( tri .boundary() .number_of ( tag::cells_of_max_dim ) != 3 )
		{	assert ( tri .boundary() .number_of ( tag::cells_of_max_dim ) == 2 );
			continue;                                                              }
		if ( walls .find ( tri ) != walls .end() )  continue;

		Cell QR = tri .boundary() .cell_in_front_of ( Q, tag::surely_exists );
		Cell R = QR .tip();
		if ( not already .has_element (R) )  continue;
		Cell SQ = tri .boundary() .cell_behind ( Q, tag::surely_exists );
		Cell S = SQ .base() .reverse ( tag::surely_exists );
		assert ( tri .boundary() .cell_in_front_of ( R, tag::surely_exists ) ==
		         tri .boundary() .cell_behind ( S, tag::surely_exists )         );
		if ( not already .has_element (S) )  continue;

		// check orientation, could be a double membrane
		std::vector < double > norm_tri =
			environ::ext_prod_type::get_normal_vector ( tag::domain_3d, tag::at_tri, tri );
		// although the interface is looking "outwards",
		// towards the zone where we do not intend to build tetrahedra,
		// 'get_normal_vector' returns a normal vector pointing "inwards",
		// that is, towards the zone we intend to mesh
		const std::vector < double > e_PQ = environ::manif_type::get_vector ( P, QQ );
		const double prod = Manifold::working .inner_prod ( P, Q, e_PQ, norm_tri );
		if ( prod > 0. )  continue;

		environ::manif_type::insert_wall ( walls, tri, QQ );
		other_vertices .erase ( Q );
		other_vertices .erase ( R );
		other_vertices .erase ( S );
		vertices_from_walls .insert ( QQ );
		vertices_from_walls .insert ( environ::manif_type::add_winding_1 ( R, QR, tag::from, QQ ) );
		vertices_from_walls .insert ( environ::manif_type::add_winding_neg_1 ( S, SQ, tag::from, QQ ) );  }

	return true;
		
}  // end of  compare_two_lists_3d

//------------------------------------------------------------------------------------------------------//


template < class environ >
inline void add_one_wall       // hidden in anonymous namespace
( Mesh & msh, Mesh & interf, Cell & P, Cell & PXY,
  typename environ::manif_type::type_of_walls & walls,
  union_of < typename environ::manif_type::set_of_winding_cells > & vertices )

// enlarge collection 'walls'
// build a tetrahedron adjacent to PXY
	
// 'environ' describes the type of manifold we are working in
// and how we treat information about metric and exterior product
// environ::manif_type could be ManifoldNoWinding or ManifoldQuotient (defined in frontal.cpp)
// environ::metric_type may be one of ISRContainer::{Inactive,Active} (defined in frontal.cpp)
// environ::ext_prod_type may be ExtProd3d, 3dRev (defined in frontal.cpp)

{	assert ( vertices .components .size() == 2 );
	typename environ::manif_type::set_of_winding_cells & vertices_from_walls = * vertices .components [0];
	typename environ::manif_type::set_of_winding_cells & other_vertices = * vertices .components [1];
	
	Cell PX = PXY .boundary() .cell_in_front_of ( P, tag::surely_exists );
	Cell X = PX .tip();
	Cell XY = PXY .boundary() .cell_in_front_of ( X, tag::surely_exists );
	Cell Y = XY .tip();
	Cell YP = PXY .boundary() .cell_behind ( P, tag::surely_exists );
	assert ( YP .base() .reverse ( tag::surely_exists ) == Y );
	Cell RYX = interf .cell_in_front_of ( XY, tag::seen_from, PXY, tag::surely_exists );
	Cell XR = RYX .boundary() .cell_in_front_of ( X, tag::surely_exists );
	Cell R = XR .tip();
	Cell RY = RYX .boundary() .cell_behind ( Y, tag::surely_exists );
	assert ( RY .base() .reverse ( tag::surely_exists ) == R );

	Cell PYZ = interf .cell_in_front_of ( YP, tag::seen_from, PXY, tag::surely_exists );
	Cell YZ = PYZ .boundary() .cell_in_front_of ( Y, tag::surely_exists );
	if ( interf .cell_in_front_of ( YZ, tag::seen_from, PYZ, tag::surely_exists ) == RYX )

	{	Cell PVX = interf .cell_in_front_of ( PX, tag::seen_from, PXY, tag::surely_exists );
		Cell PV = PVX .boundary() .cell_in_front_of ( P, tag::surely_exists );
		Cell ZP = PYZ .boundary() .cell_behind ( P, tag::surely_exists );

		if ( PV == ZP .reverse ( tag::surely_exists ) )
				
		{	// this is a closed tetrahedral cavity
			assert ( interf .cell_in_front_of ( PV, tag::seen_from, PVX, tag::surely_exists ) == PYZ );
			Cell VX = PVX .boundary() .cell_behind ( X, tag::surely_exists );
			Cell V = PV .tip();
			assert ( VX .base() .reverse ( tag::surely_exists ) == V );
			assert ( V == R );
			assert ( interf .cell_in_front_of ( VX, tag::seen_from, PVX, tag::surely_exists ) == RYX );
			#if FRONTAL_VERBOSITY > 1
			std::cout << "frontal-3d.cpp line 1641, " << std::endl << std::flush;
			#endif
			Cell RYX_alt = remove_face ( RYX, interf );
			// RYX .remove_from ( interf )
			PXY .remove_from ( interf );
			PVX .remove_from ( interf );
			PYZ .remove_from ( interf );                                   // V == R == Z
			fill_isolated_tetra_core ( msh, interf, PXY, PVX, PYZ, RYX_alt );
			environ::manif_type::node_container .cond_remove ( X, interf );
			environ::manif_type::node_container .cond_remove ( Y, interf );
			environ::manif_type::node_container .cond_remove ( V, interf );
			environ::metric_type::cond_remove ( X, interf );
			environ::metric_type::cond_remove ( Y, interf );
			environ::metric_type::cond_remove ( V, interf );
			environ::metric_type::cond_remove ( P, interf );
			environ::manif_type::insert_wall
				( walls, RYX_alt, environ::manif_type::add_winding_1 ( X, PX ) );
			// no need to change 'vertices'
			return;                                                                                      }
				
		// else : this is a tetrahedral cavity open at PXZ
		#if FRONTAL_VERBOSITY > 1
		std::cout << "frontal-3d.cpp line 1663, " << std::endl << std::flush;
		#endif
		Cell Z = YZ .tip();
		assert ( R == Z );
		Cell XZ = RYX .boundary() .cell_behind ( Z, tag::surely_exists );
		assert ( XZ .base() .reverse ( tag::surely_exists ) == X );
		assert ( ZP .base() .reverse ( tag::surely_exists ) == Z );
		Cell RYX_alt = remove_face ( RYX, interf );
		// RYX .remove_from ( interf )
		PXY .remove_from ( interf );
		PYZ .remove_from ( interf );
		fill_one_tetra_ground_core                //  R == Z
			( msh, interf, PXY, PYZ, RYX_alt, PX, XZ, ZP, Y, X, P, Z );
		environ::manif_type::node_container .cond_remove ( Y, interf );
		environ::metric_type::cond_remove ( Y, interf );
		environ::manif_type::insert_wall
			( walls, RYX_alt, environ::manif_type::add_winding_1 ( X, PX ) );
		// no need to change 'vertices'
		return;                                                               }

	Cell PVX = interf .cell_in_front_of ( PX, tag::seen_from, PXY, tag::surely_exists );
	Cell VX = PVX .boundary() .cell_behind ( X, tag::surely_exists );
	if ( interf .cell_in_front_of ( VX, tag::seen_from, PVX, tag::surely_exists ) == RYX )
	{	// this is a tetrahedral cavity open at PVY
		#if FRONTAL_VERBOSITY > 1
		std::cout << "frontal-3d.cpp line 1688, " << std::endl << std::flush;
		#endif
		assert ( VX .base() .reverse ( tag::surely_exists ) == R );  //  V == R
		Cell VY = RYX .boundary() .cell_behind ( Y, tag::surely_exists );
		Cell PV = PVX .boundary() .cell_behind ( R, tag::surely_exists );  //  V == R
		assert ( VY .base() .reverse ( tag::surely_exists ) == R );
		assert ( PV .base() .reverse ( tag::surely_exists ) == P );
		Cell RYX_alt = remove_face ( RYX, interf );
		// RYX .remove_from ( interf )
		PXY .remove_from ( interf );
		PVX .remove_from ( interf );
		fill_one_tetra_ground_core
			( msh, interf, PVX, PXY, RYX_alt, PV, VY, YP, X, P, R, Y );  // V == R
		//				( msh, interf, ABC, BAD, ACD_alt, BC, CD, DB, A, B, C, D );
		environ::manif_type::node_container .cond_remove ( X, interf );
		environ::metric_type::cond_remove ( X, interf );
		environ::manif_type::insert_wall
			( walls, RYX_alt, environ::manif_type::add_winding_1 ( X, PX ) );
		// no need to change 'vertices'
		return;                                                                             }

	if ( danger_of_flat_tetra < environ >
	     ( interf, XY, PXY, RYX, tag::mobile_vertex, P, tag::both_directions ) )
		return;
	
	#if FRONTAL_VERBOSITY > 1
	std::cout << "frontal-3d.cpp line 1714, " << std::endl << std::flush;
	#endif
	Cell RYX_alt = remove_face ( RYX, interf );
	// RYX .remove_from ( interf )
	PXY .remove_from ( interf );
	fill_one_tetra_crest_core < environ >
			( msh, interf, PXY, RYX_alt, XY, PX, XR, YP, RY, X, Y, P, R );
	environ::manif_type::insert_wall
		( walls, RYX, environ::manif_type::add_winding_1 ( X, PX ) );
	typename environ::manif_type::winding_cell RR = environ::manif_type::add_winding_2 ( R, PX, XR );
	vertices_from_walls .insert ( RR );
	other_vertices .erase ( R );

}  // end of  add_one_wall

//------------------------------------------------------------------------------------------------------//


template < class environ >
inline bool add_walls_first_layer       // hidden in anonymous namespace
( Mesh & msh, Mesh & interf, Cell & P,
  typename environ::manif_type::type_of_walls & walls,
  union_of < typename environ::manif_type::set_of_winding_cells > & vertices )

// enlarge collection 'walls'
// may build new segments, triangles and tetrahedra around P, add them to 'interf' and to 'msh'
	
// 'environ' describes the type of manifold we are working in
// and how we treat information about metric and exterior product
// environ::manif_type could be ManifoldNoWinding or ManifoldQuotient (defined in frontal.cpp)
// environ::metric_type may be one of ISRContainer::{Inactive,Active} (defined in frontal.cpp)
// environ::ext_prod_type may be ExtProd3d, 3dRev (defined in frontal.cpp)

{	std::vector < Cell > vec_of_tri;
	vec_of_tri .reserve ( 10 );

	// look at each triangle around P
	// P is regular (we just built it), so the mesh around P is like a fuzzy one
	Mesh::Iterator it_around_P = interf .iterator
		( tag::over_cells_of_max_dim, tag::orientation_compatible_with_mesh,
		  tag::around, P, tag::require_order                                );
 	for ( it_around_P .reset();  it_around_P .in_range(); it_around_P ++ )
	{	Cell PXY = * it_around_P;
		Cell PX = PXY .boundary() .cell_in_front_of ( P, tag::surely_exists );
		Cell X = PX .tip();
		Cell XY = PXY .boundary() .cell_in_front_of ( X, tag::surely_exists );
		Cell RYX = interf .cell_in_front_of ( XY, tag::seen_from, PXY, tag::surely_exists );
		double var;  // not used here
		#if FRONTAL_VERBOSITY > 1
		if ( fit_for_one_tetra < environ > ( interf, XY, PXY, RYX, var ) )
		{	std::cout << "+";  vec_of_tri .push_back ( PXY );  }
		else  std::cout << "-";
		#else  // FRONTAL_VERBOSITY <= 1
		if ( fit_for_one_tetra < environ > ( interf, XY, PXY, RYX, var ) )
			vec_of_tri .push_back ( PXY );
		#endif  // FRONTAL_VERBOSITY
	}  // end of for around P
	#if FRONTAL_VERBOSITY > 1
	std::cout << " ";
	#endif

	bool did_add = false;
	while ( not vec_of_tri .empty() )

	{	Cell PXY = vec_of_tri .back();
		vec_of_tri .pop_back();
		if ( not PXY .belongs_to ( interf, tag::same_dim, tag::orientation_compatible_with_mesh ) )
			continue;

		did_add = true;
		add_one_wall < environ > ( msh, interf, P, PXY, walls, vertices );
	
		#if FRONTAL_VERBOSITY > 1
		frontal_counter ++;
		if ( frontal_counter + 1 >= frontal_counter_max )  return false;
		#endif
		
	}  // end of  while

	return did_add;

}  // end of  add_walls_first_layer

//------------------------------------------------------------------------------------------------------//


template < class environ >
inline std::pair < Cell, typename environ::manif_type::winding_cell > face_opposite_to
( Cell & P, const tag::Within &, Cell & tetra )

// 'environ' describes the type of manifold we are working in
// and how we treat information about metric and exterior product
// environ::manif_type could be ManifoldNoWinding or ManifoldQuotient (defined in frontal.cpp)
// environ::metric_type may be one of ISRContainer::{Inactive,Active} (defined in frontal.cpp)
// environ::ext_prod_type may be ExtProd3d, 3dRev (defined in frontal.cpp)

{	assert ( tetra .dim() == 3 );
	assert ( P .belongs_to ( tetra .boundary(), tag::cell_has_low_dim ) );
	Mesh::Iterator it = tetra .boundary() .iterator
	                     ( tag::over_segments, tag::pointing_away_from, P );
	it .reset();  assert ( it .in_range() );
	Cell PQ = *it;
	Cell Q = PQ .tip();
	Cell PQR = tetra .boundary() .cell_behind ( PQ, tag::surely_exists );
	Cell QR = PQR .boundary() .cell_in_front_of ( Q, tag::surely_exists );
	Cell tri = tetra .boundary() .cell_in_front_of ( QR, tag::surely_exists );
	typename environ::manif_type::winding_cell QQ
	          = environ::manif_type::add_winding_1 ( Q, PQ );
	return { tri, QQ };                                                         }	

//------------------------------------------------------------------------------------------------------//


template < class environ >
inline bool add_walls       // hidden in anonymous namespace
( Mesh & msh, Mesh & interf, Cell & P,
  typename environ::manif_type::type_of_walls & walls,
  union_of < typename environ::manif_type::set_of_winding_cells > & vertices )

// enlarge collection 'walls'
// may build new segments, triangles and tetrahedra around P, add them to 'interf' and to 'msh'
// sometimes, even far from P (not adjacent to)

// 'environ' describes the type of manifold we are working in
// and how we treat information about metric and exterior product
// environ::manif_type could be ManifoldNoWinding or ManifoldQuotient (defined in frontal.cpp)
// environ::metric_type may be one of ISRContainer::{Inactive,Active} (defined in frontal.cpp)
// environ::ext_prod_type may be ExtProd3d, 3dRev (defined in frontal.cpp)

{	assert ( vertices .components .size() == 2 );
	typename environ::manif_type::set_of_winding_cells & vertices_from_walls = * vertices .components [0];
	typename environ::manif_type::set_of_winding_cells & other_vertices = * vertices .components [1];

	bool did_add = false;

	while ( true )
	{
		#if FRONTAL_VERBOSITY > 1
		std::cout << "frontal-3d.cpp line 2136, tri_fill_isolated size " << tri_fill_isolated .size()
		          << ", counter " << frontal_counter + 1 << std::endl << std::flush;
		#endif
		Cell tetra = fill_isolated_tetrahedron < environ > ( msh, interf, tag::mobile_vertex, P );
		if ( tetra .exists() )
		{	if ( P .belongs_to ( tetra .boundary(), tag::cell_has_low_dim ) )
			{	std::pair < Cell, typename environ::manif_type::winding_cell > pa  //  ( tri, X )
						= face_opposite_to < environ > ( P, tag::within, tetra );
				environ::manif_type::insert_wall ( walls, pa .first, pa .second );
				// no need to change 'vertices'
				did_add = true;                                                     }
			#if FRONTAL_VERBOSITY > 1
			frontal_counter ++;
			if ( frontal_counter + 1 >= frontal_counter_max )  return did_add;
			#endif
			continue;                                                                 }
		
		#if FRONTAL_VERBOSITY > 1
		std::cout << "frontal-3d.cpp line 2154, ver_fill_burried (one tetra) size "
					 << ver_fill_burried .size()
		          << ", counter " << frontal_counter + 1 << std::endl << std::flush;
		#endif  // FRONTAL_VERBOSITY
		tetra = fill_one_tetra_ground < environ > ( msh, interf, tag::mobile_vertex, P );
		if ( tetra .exists() )
		{	if ( P .belongs_to ( tetra .boundary(), tag::cell_has_low_dim ) )
			{	std::pair < Cell, typename environ::manif_type::winding_cell > pa  //  ( tri, X )
						= face_opposite_to < environ > ( P, tag::within, tetra );
				environ::manif_type::insert_wall ( walls, pa .first, pa .second );
				// no need to change 'vertices'
				did_add = true;                                                     }
			#if FRONTAL_VERBOSITY > 1
			frontal_counter ++;
			if ( frontal_counter + 1 >= frontal_counter_max )  return did_add;
			#endif
			continue;                                                                 }

		#if FRONTAL_VERBOSITY > 1
		std::cout << "frontal-3d.cpp line 2173, seg_fill_crest size " << seg_fill_crest .size()
		          << ", counter " << frontal_counter + 1 << std::endl << std::flush;
		#endif
		tetra = fill_one_tetrahedron_crest < environ > ( msh, interf, tag::mobile_vertex, P );
		if ( tetra .exists() )
		{	if ( P .belongs_to ( tetra .boundary(), tag::cell_has_low_dim ) )
			{	std::pair < Cell, typename environ::manif_type::winding_cell > pa  //  ( tri, RR )
						= face_opposite_to < environ > ( P, tag::within, tetra );
				Cell tri = pa .first;
				typename environ::manif_type::winding_cell RR = pa .second;
				Cell R = environ::manif_type::remove_winding ( RR );
				environ::manif_type::insert_wall ( walls, tri, RR );
				did_add = true;
				// one of the three vertices of 'tri' may not belong to 'vertices_from_walls'
				// but we don't know which one, so we insert all three
				vertices_from_walls .insert ( RR );
				other_vertices .erase ( R );
				Cell seg = tri .boundary() .cell_in_front_of ( R, tag::surely_exists );
				R = seg .tip();
				RR = environ::manif_type::add_winding_1 ( R, seg, tag::from, RR );
				vertices_from_walls .insert ( RR );
				other_vertices .erase ( R );
				seg = tri .boundary() .cell_in_front_of ( R, tag::surely_exists );
				R = seg .tip();
				RR = environ::manif_type::add_winding_1 ( R, seg, tag::from, RR );       }
			#if FRONTAL_VERBOSITY > 1
			frontal_counter ++;
			if ( frontal_counter + 1 >= frontal_counter_max )  return did_add;
			#endif
			continue;                                                                      }

		return did_add;                                                                      }
	
}  // end of  add_walls

//------------------------------------------------------------------------------------------------------//


template < class environ >
void relocate_3d    // hidden in anonymous namespace
( Mesh & msh, Mesh & interf, Cell & P,
  typename environ::manif_type::type_of_walls & walls,
  union_of < typename environ::manif_type::set_of_winding_cells > & vertices,
  std::list < std::pair < Cell, Cell > > seg_tri_concave = { }               )
	
// improve the location of P, taking into account nearby vertices and triangles
// also build filaments when appropriate
// may build new segments, triangles and tetrahedra around P, add them to 'interf' and to 'msh'

// 'walls' is a set of triangles if manif non-winding
// it is a map (triangle, vertex with winding) if manif winding
// 'seg_tri_concave' is meaningful only if called from 'fill_one_valley'
// or from 'fill_cavity_ground', section "we build six new tetrahedra"

// 'vertices' is the union of two sets
// first : the vertices of all walls
// second : other vertices, not belonging to any wall (freelancers)

// 'environ' describes the type of manifold we are working in
// and how we treat information about metric and exterior product
// environ::manif_type could be ManifoldNoWinding or ManifoldQuotient (defined in frontal.cpp)
// environ::metric_type may be one of ISRContainer::{Inactive,Active} (defined in frontal.cpp)
// environ::ext_prod_type may be ExtProd3d, 3dRev (defined in frontal.cpp)
	
{	std::vector < std::vector < double > > inertia
		{ std::vector < double > ( frontal_nb_of_coords, 0. ),
			std::vector < double > ( frontal_nb_of_coords, 0. ),
			std::vector < double > ( frontal_nb_of_coords, 0. ) };

	bool did_not_add_walls = true, displacement_large = true;
	while ( displacement_large and did_not_add_walls )
	{	did_not_add_walls =
			not add_walls_first_layer < environ > ( msh, interf, P, walls, vertices );
		#if FRONTAL_VERBOSITY > 1
		if ( frontal_counter + 1 >= frontal_counter_max )  return;
		#endif
		displacement_large = relocate_core_3d < environ >
			( interf, P, walls, vertices, inertia, 1. ) > 0.001;                       }
			// above we give a huge tolerance, meaning we only want one iteration
			// 'relocate_core_3d' returns the size of the step performed, squared
			// thus, 'displacement_large' acts as a stopping criterion

	// we must call 'relocate_core_3d' at least twice in order for 'inertia'
	// to get properly initialized
	// it has been called above, at least once, so we call it once more to be sure
	#if FRONTAL_VERBOSITY > 1
	if ( frontal_counter + 1 >= frontal_counter_max )  return;
	#endif
	if ( interf .number_of ( tag::vertices ) == 0 )  return;
	std::list < typename environ::manif_type::winding_cell > close_neighb =
		// tag::Util::sqrt_2  // frontal_seg_size 3D
		environ::manif_type::node_container .find_close_neighbours_of ( P, frontal_seg_size );
	#if FRONTAL_VERBOSITY > 1
	std::cout << vertices .size() << " " << close_neighb .size() << " ";
	#endif
	compare_two_lists_3d < environ >
		( msh, interf, P, walls, vertices, close_neighb, frontal_seg_size_2 );
	relocate_core_3d < environ > ( interf, P, walls, vertices, inertia, 1. );
	// above we give a huge tolerance, meaning we only want one iteration
		
	add_walls < environ > ( msh, interf, P, walls, vertices );
		
	double tol = 0.001;
	while ( true )
	{ 
		#if FRONTAL_VERBOSITY > 1
		if ( frontal_counter + 1 >= frontal_counter_max )  return;
		#endif
		if ( interf .number_of ( tag::vertices ) == 0 )  break;

		eliminate_far_vertices < environ > ( P, interf, walls, vertices, frontal_seg_size_2 );
		// tag::Util::sqrt_2  // frontal_seg_size 3D
		
		close_neighb = environ::manif_type::node_container
		                   .find_close_neighbours_of ( P, frontal_seg_size );
		#if FRONTAL_VERBOSITY > 1
		std::cout << vertices .size() << " " << close_neighb .size() << ", ";
		#endif

		bool did_add_vertices = compare_two_lists_3d < environ >
			( msh, interf, P, walls, vertices, close_neighb, frontal_seg_size_2 );
		#if FRONTAL_VERBOSITY > 1
		std::cout << vertices .size() << " " << close_neighb .size() << " ";
		#endif

		// we should take into account more vertices
		// for instance, after fill_valley the configuration becomes like this
		// https://codeberg.org/cristian.barbarosie/manifem-manual, files 'valley-0{1,2}.msh'

		relocate_core_3d < environ > ( interf, P, walls, vertices, inertia, tol );

		// since P's location has changed, we populate again 'seg_fill_crest'
		if ( P .belongs_to ( interf, tag::cell_has_low_dim ) )
		{	Mesh::Iterator it = interf .iterator
				( tag::over_cells_of_max_dim, tag::orientation_compatible_with_mesh, tag::around, P );
			for ( it .reset(); it .in_range(); it ++ )
			{	Cell tri = *it;
				assert ( tri .boundary() .number_of ( tag::cells_of_max_dim ) == 3 );
				Cell seg = tri .boundary() .cell_in_front_of ( P, tag::surely_exists );
				seg = tri .boundary() .cell_in_front_of ( seg .tip(), tag::surely_exists );
				seg_fill_crest .insert ( { tri, seg } );                                     }
			// sometimes we want to add one or two more segments
			for ( std::list < std::pair < Cell, Cell > > ::iterator
			      it_c = seg_tri_concave .begin(); it_c != seg_tri_concave .end();  )
			{	Cell seg_c = it_c->first;
				assert ( seg_c .dim() == 1 );
				Cell tri_c = it_c->second;
				assert ( tri_c .dim() == 2 );
				assert ( seg_c .belongs_to ( tri_c .boundary(),
				                             tag::orientation_compatible_with_mesh ) );
				if ( tri_c .belongs_to ( interf, tag::orientation_compatible_with_mesh ) )
				  {	seg_fill_crest .insert ( { tri_c, seg_c } );  it_c ++;  }
				else  it_c = seg_tri_concave .erase ( it_c );                               }           }
		
		bool did_add_walls = add_walls < environ > ( msh, interf, P, walls, vertices );
		#if FRONTAL_VERBOSITY > 1
		std::cout << "line 2003, " << vertices .size() << " " << close_neighb .size() << ", ";
		#endif

		if ( did_add_walls or did_add_vertices )  tol = 0.001;
		else   //  0.001  0.0006  0.00036  0.000216  0.0001296
		{	if ( tol < 0.0002 )  break;
			tol *= 0.6;                   }
	}  // end of  while

	#if FRONTAL_VERBOSITY > 1
		for ( typename union_of < typename environ::manif_type::set_of_winding_cells >
		      ::iterator it = vertices .begin(); it .in_range(); it ++                 )
	{	Cell V = environ::manif_type::remove_winding ( *it );
		std::cout << std::sqrt ( environ::manif_type::sq_dist::working_manif  // rescaled metric
		                         ( V, environ::manif_type::add_winding ( P, 0 ) ) ) << " ";  }
	std::cout << std::endl << std::flush;
	#endif  // FRONTAL_VERBOSITY > 1

	if ( not P .belongs_to ( interf, tag::cell_has_low_dim ) )  return;

	assert ( vertices .components .size() == 2 );
	typename environ::manif_type::set_of_winding_cells & vertices_from_walls = * vertices .components [0];
	typename environ::manif_type::set_of_winding_cells & other_vertices = * vertices .components [1];
	// vertices in 'other_vertices' but not in 'top_close_msh' must be linked to P through filaments
	// 'top_close_msh' is not inteded for avoiding vertices behind a thin strip (as was before)
	// now it is just for avoiding filaments with vertices which are rather close to P
	
	// 'top_close_msh' is built starting from 'vertices_from_walls'
	// then expanded following the neighbourhood relations in 'msh' (mesh of tetrahedra)
	// copy 'vertices_from_walls' into 'top_close_msh' and 'layer' (stripped of windings)
	std::set < Cell > top_close_msh, layer;
	for ( typename environ::manif_type::set_of_winding_cells::const_iterator
	      it = vertices_from_walls .begin(); it != vertices_from_walls .end(); it ++ )
	{	typename environ::manif_type::winding_cell VV = *it;
		Cell V = environ::manif_type::remove_winding ( VV );
		top_close_msh .insert ( V );
		layer .insert ( V );                                  }

	for ( size_t i = 0; i < 2; i++ )
	{	std::set < Cell > next_layer;
		for ( std::set < Cell > ::iterator it = layer .begin(); it != layer .end(); it ++ )
		{	Cell V = * it;
			Mesh::Iterator itt = msh .iterator ( tag::over_vertices, tag::connected_to, V );
			for ( itt .reset(); itt .in_range(); itt ++ )
			{	Cell W = * itt;
				top_close_msh .insert ( W );
				next_layer .insert ( W );     }                                                 }
		next_layer .swap ( layer );                                                            }

	for ( typename environ::manif_type::set_of_winding_cells::iterator
	      it = other_vertices .begin(); it != other_vertices .end(); it ++ )

	{ typename environ::manif_type::winding_cell VV = *it;
		Cell V = environ::manif_type::remove_winding ( VV );
		// some of the vertices may have been removed from 'interf' in the meanwhile
		if ( not V .belongs_to ( interf, tag::cell_has_low_dim ) )  continue;
		if ( top_close_msh .find ( V ) != top_close_msh .end() )  continue;
		Cell PV ( tag::segment, P .reverse ( tag::surely_exists ), V );
		environ::manif_type::set_winding ( PV, VV );
		Cell VP ( tag::segment, V .reverse ( tag::surely_exists ), P );
		environ::manif_type::set_winding ( VP, VV );
		
		#if FRONTAL_VERBOSITY > 1
		const std::vector < double > e_PV = environ::manif_type::get_vector ( P, VV );
		std::cout << "frontal-3d.cpp line 2067, building filament "
		          << Manifold::working .sq_dist ( P, e_PV ) << ", [";
		for ( size_t i = 0; i < frontal_nb_of_coords; i++ )
		{	const Function & x = Manifold::working .coordinates() [i];
			std::cout << x(P) << " ";                                   }
		std::cout << "] [";
		for ( size_t i = 0; i < frontal_nb_of_coords; i++ )
		{	const Function & x = Manifold::working .coordinates() [i];
			std::cout << x(V) << " ";                                   }
		std::cout << "]" << std::endl << std::flush;
		#endif  // FRONTAL_VERBOSITY > 1

		extrem_filam_dist_0 .insert ( { V, VP } );
		Mesh::Iterator it_around_V = interf .iterator ( tag::over_segments, tag::pointing_away_from, V );
		for ( it_around_V .reset(); it_around_V .in_range(); it_around_V ++ )
		{	Cell seg = * it_around_V;
			// do not take filaments into account
			if ( seg .is_regular ( tag::within_STSI_mesh, interf ) )
			// a filament is regular, despite its singular apparance
				if ( interf .cell_behind ( seg, tag::surely_exists )
					  .boundary() .number_of ( tag::cells_of_max_dim ) == 2 )  continue;
			extrem_filam_dist_1 .insert ( { seg .tip(), VP } );                         }
		extrem_filam_dist_0 .insert ( { P, PV } );
		Mesh::Iterator it_around_P = interf .iterator ( tag::over_segments, tag::pointing_away_from, P );
		for ( it_around_P .reset(); it_around_P .in_range(); it_around_P ++ )
		{	Cell seg = * it_around_P;
			// do not take filaments into account
			if ( seg .is_regular ( tag::within_STSI_mesh, interf ) )
			// a filament is regular, despite its singular apparance
				if ( interf .cell_behind ( seg, tag::surely_exists )
					  .boundary() .number_of ( tag::cells_of_max_dim ) == 2 )  continue;
			extrem_filam_dist_1 .insert ( { seg .tip(), PV } );                         }
		
		Mesh::Connected::OneDim * bdry_filam = new Mesh::Connected::OneDim
		                  ( tag::with, 2, tag::segments, tag::one_dummy_wrapper );
		PV .core->add_to_mesh ( bdry_filam, tag::do_not_bother );
		VP .core->add_to_mesh ( bdry_filam, tag::do_not_bother );
		bdry_filam->first_ver = P .reverse ( tag::surely_exists );
		bdry_filam-> last_ver = P;
		Cell filam ( tag::whose_core_is,
		             new Cell::PositiveHighDim
		             ( tag::whose_boundary_is,
		               Mesh ( tag::whose_core_is, bdry_filam, tag::move ),
		               tag::one_dummy_wrapper                             ), tag::move );
		filam .add_to ( interf );
		seg_fill_membrane_3s .insert ( PV .get_positive() );
		seg_fill_membrane_2s .insert ( { filam, PV } );
		bdry_filam = new Mesh::Connected::OneDim
			( tag::with, 2, tag::segments, tag::one_dummy_wrapper );
		PV .reverse ( tag::surely_exists )  // apparently 'filam.add_to' has created the reverse
			.core->add_to_mesh ( bdry_filam, tag::do_not_bother );
		VP .reverse ( tag::surely_exists )  // apparently 'filam.add_to' has created the reverse
			.core->add_to_mesh ( bdry_filam, tag::do_not_bother );
		bdry_filam->first_ver = P .reverse ( tag::surely_exists );
		bdry_filam-> last_ver = P;
		filam = Cell ( tag::whose_core_is,
		               new Cell::PositiveHighDim
		               ( tag::whose_boundary_is,
		                 Mesh ( tag::whose_core_is, bdry_filam, tag::move ),
		                 tag::one_dummy_wrapper                             ), tag::move );
		filam .add_to ( interf );
		assert ( PV .is_regular ( tag::within_STSI_mesh, interf ) );
		assert ( VP .is_regular ( tag::within_STSI_mesh, interf ) );
		assert ( PV .reverse ( tag::surely_exists ) .is_regular ( tag::within_STSI_mesh, interf ) );
		assert ( VP .reverse ( tag::surely_exists ) .is_regular ( tag::within_STSI_mesh, interf ) );
		assert ( PV .is_positive() );
		seg_fill_membrane_2s .insert ( { filam, PV .reverse ( tag::surely_exists ) } );               }
	
}  //  end of  relocate_3d

//------------------------------------------------------------------------------------------------------//


template < class environ >
inline void fill_two_tetra_core    // hidden in anonymous namespace
( Mesh & msh, Mesh & interf, Cell & A, Cell & B, Cell & C, Cell & D, Cell & E,
  Cell & AB, Cell & AC, Cell & AD, Cell & AE, Cell & ABC, Cell & ACD, Cell & ADE, Cell & AEB )

// create tetrahedra ABCD and ABDE
// first two triangles define one tetrahedron,
// third and fourth triangles define another tetrahedron
	
// 'environ' describes the type of manifold we are working in
// and how we treat information about metric and exterior product
// environ::manif_type could be ManifoldNoWinding or ManifoldQuotient (defined in frontal.cpp)
// environ::metric_type may be one of ISRContainer::{Inactive,Active} (defined in frontal.cpp)
// environ::ext_prod_type may be ExtProd3d, 3dRev (defined in frontal.cpp)
	
{	Cell BC = ABC .boundary() .cell_in_front_of ( B, tag::surely_exists );
	Cell CD = ACD .boundary() .cell_in_front_of ( C, tag::surely_exists );
	Cell DE = ADE .boundary() .cell_in_front_of ( D, tag::surely_exists );
	Cell EB = AEB .boundary() .cell_in_front_of ( E, tag::surely_exists );
	Cell BD ( tag::segment, B .reverse ( tag::surely_exists ), D );
	environ::manif_type::set_winding_diff ( BD, AD, AB );
	update_filaments_near ( BD, interf );
	
	Cell ABD ( tag::triangle, AB, BD, AD .reverse ( tag::surely_exists ) );
	Cell BDC ( tag::triangle, BD, CD .reverse ( tag::surely_exists ), BC .reverse ( tag::surely_exists ) );
	Cell BED ( tag::triangle, EB .reverse ( tag::surely_exists ), DE .reverse ( tag::surely_exists ),
	                          BD .reverse ( tag::does_not_exist, tag::build_now )                    );
	Cell ABC_alt = remove_face ( ABC, interf );
	Cell ACD_alt = remove_face ( ACD, interf );
	Cell ADE_alt = remove_face ( ADE, interf );
	Cell AEB_alt = remove_face ( AEB, interf );
	// ABC .remove_from ( interf );
	// ACD .remove_from ( interf );
	// ADE .remove_from ( interf );
	// AEB .remove_from ( interf );
	Cell ABCD ( tag::tetrahedron, ABC_alt, ACD_alt,
	                              ABD .reverse ( tag::does_not_exist, tag::build_now ), BDC );
	Cell ABDE ( tag::tetrahedron, ADE_alt, AEB_alt, ABD, BED );
	
	#if FRONTAL_VERBOSITY > 1
	std::cout << "frontal-3d.cpp line 2157, fill two tetra" << std::endl << std::flush;
	#endif

	ABCD .add_to ( msh );
	ABDE .add_to ( msh );
	Cell BCD = BDC .reverse ( tag::does_not_exist, tag::build_now );
	BCD .add_to ( interf );
	Cell BDE = BED .reverse ( tag::does_not_exist, tag::build_now );
	BDE .add_to ( interf );
	environ::manif_type::node_container .cond_remove ( A, interf );
	environ::metric_type::               cond_remove ( A, interf );

	// we do not bother to eliminate segments and faces from sets/maps ***_fill_tetrah***
	// once the faces have been removed from interf,
	// they will be eliminated by the 'if' at the beginning of the loop
	
	// add pairs (triangle,segment) to 'dangerous_cavities'
	// since BD is (in most cases) a crest, we do not add CD, BC, EB, DE
	{  // just a block of code for hiding variable names
	// rotate around C :
	Cell tri = interf .cell_in_front_of ( BC, tag::seen_from, BCD, tag::surely_exists );
	Cell seg = tri .boundary() .cell_behind ( C, tag::surely_exists );
	while ( true )
	{	Cell next_tri = interf .cell_in_front_of ( seg, tag::seen_from, tri, tag::surely_exists );
		if ( next_tri == BCD )  break;
		dangerous_cavities .push_back ( { tri, seg } );
		tri = next_tri;
		seg = tri .boundary() .cell_behind ( C, tag::surely_exists );                               }
	// rotate around E :
	tri = interf .cell_in_front_of ( DE, tag::seen_from, BDE, tag::surely_exists );
	seg = tri .boundary() .cell_behind ( E, tag::surely_exists );
	while ( true )
	{	Cell next_tri = interf .cell_in_front_of ( seg, tag::seen_from, tri, tag::surely_exists );
		if ( next_tri == BDE )  break;
		dangerous_cavities .push_back ( { tri, seg } );
		tri = next_tri;
		seg = tri .boundary() .cell_behind ( E, tag::surely_exists );                               }
	}  // just a block of code

	ver_fill_burried .insert (B);
	ver_fill_burried .insert (C);
	ver_fill_burried .insert (D);
	ver_fill_burried .insert (E);
	seg_fill_membrane_3s .insert ( BD .get_positive() );
	seg_fill_crest .insert ( { BCD, BC } );
	seg_fill_crest .insert ( { BCD, CD } );
	seg_fill_crest .insert ( { BDE, DE } );
	seg_fill_crest .insert ( { BDE, EB } );
	seg_fill_membrane_2s .insert ( { BDE, BD } );
	seg_fill_membrane_2s .insert ( { BCD, BD .reverse ( tag::surely_exists ) } );
	ver_fill_cavity .insert (B);
	ver_fill_cavity .insert (C);
	ver_fill_cavity .insert (D);
	ver_fill_cavity .insert (E);
	seg_fill_two_crest .push_back ( { BCD, BC } );
	seg_fill_two_crest .push_back ( { BCD, CD } );
	seg_fill_two_crest .push_back ( { BDE, DE } );
	seg_fill_two_crest .push_back ( { BDE, EB } );
	seg_fill_two_crest .push_back ( { BDE, BD } );

	#if FRONTAL_VERBOSITY > 1
	frontal_counter ++;
	#endif

}  // end of  fill_two_tetra_core
	
//------------------------------------------------------------------------------------------------------//


// this function is invoked twice

template < class environ >
inline void fill_two_tetrahedra_crest_core    // hidden in anonymous namespace
( Mesh & msh, Mesh & interf, Cell & ABC, Cell & AB )

// create two tetrahedra starting from faces ABC and BAD
// create vertex P and relocate it

// 'environ' describes the type of manifold we are working in
// and how we treat information about metric and exterior product
// environ::manif_type could be ManifoldNoWinding or ManifoldQuotient (defined in frontal.cpp)
// environ::metric_type may be one of ISRContainer::{Inactive,Active} (defined in frontal.cpp)
// environ::ext_prod_type may be ExtProd3d, 3dRev (defined in frontal.cpp)
	
{	Cell A = AB .base() .reverse ( tag::surely_exists );
	Cell B = AB .tip();
	Cell CA = ABC .boundary() .cell_behind ( A, tag::surely_exists );
	Cell BAD = interf .cell_in_front_of ( AB, tag::seen_from, ABC, tag::surely_exists );
	assert ( BAD .boundary() .number_of ( tag::segments ) == 3 );
	Cell BC = ABC .boundary() .cell_in_front_of ( B, tag::surely_exists );
	Cell C = BC .tip();
	assert ( CA .base() .reverse() == C );
	Cell AD = BAD .boundary() .cell_in_front_of ( A, tag::surely_exists );
	Cell D = AD .tip();
	Cell DB = BAD .boundary() .cell_behind ( B, tag::surely_exists );
	assert ( DB .base() .reverse() == D );

	#if FRONTAL_VERBOSITY > 1
	std::cout << "frontal-3d.cpp line 2255, insert two tetra, crest, reloc" << std::endl << std::flush;
	#endif

	const std::vector < double > e_AB = environ::manif_type::get_vector ( AB );
	const std::vector < double > e_CA = environ::manif_type::get_vector ( CA );
	const std::vector < double > e_BC = environ::manif_type::get_vector ( BC );
	const std::vector < double > e_AD = environ::manif_type::get_vector ( AD );
	const std::vector < double > e_DB = environ::manif_type::get_vector ( DB );
	const std::vector < double > n_ABC =
		environ::ext_prod_type::get_normal_vector ( tag::domain_3d, tag::at_tri, ABC );
	const std::vector < double > n_BAD =
		environ::ext_prod_type::get_normal_vector ( tag::domain_3d, tag::at_tri, BAD );
	// although the interface is looking "outwards",
	// towards the zone where we do not intend to build tetrahedra,
	// 'get_normal_vector' returns a normal vector pointing "inwards",
	// that is, towards the zone we intend to mesh

	// we first place P based on ABC and BAD
	// we later improve the location of P, taking into account neighbour triangles
	std::vector < double > e_AP ( frontal_nb_of_coords );
	for ( size_t i = 0; i < frontal_nb_of_coords; i++ )
		e_AP [i] = e_AB [i] / 2. + ( ( e_BC [i] - e_CA [i] + e_AD [i] - e_DB [i] ) / 12.
		                             + ( n_ABC [i] + n_BAD [i] ) * tag::Util::sqrt_sixth ) * 0.7;
	// last multiplication means we choose to project less, aiming at a more stable process
	// 'relocate_3d' will improve the location anyway
	
	// 'walls' is a set of triangles if manif non-winding
	// it is a map (triangle, vertex with winding) if manif winding
	typename environ::manif_type::type_of_walls walls;	
	typename environ::manif_type::winding_cell AA = environ::manif_type::add_winding ( A, 0 );
	environ::manif_type::insert_wall ( walls, ABC, AA );
	environ::manif_type::insert_wall ( walls, BAD, AA );

	Cell P ( tag::vertex );
	for ( size_t i = 0; i < frontal_nb_of_coords; i++ )
	{	const Function & x = Manifold::working .coordinates() [i];
		x(P) = x(A) + 0.8 * e_AP [i];                               }
		// we consider both A and P have zero winding
		// we multiply, again, by a subunitary value, aiming at a more stable process
	// Manifold::working .project (P)  not needed in 3D
	// 'compute_data' uses information from A to compute eigendirection and isr at P
	environ::metric_type::compute_data ( tag::at_point, P, tag::use_information_from, A );
	// P will be added to the cloud later, after 'relocate_3d'

	Cell PA ( tag::segment, P .reverse ( tag::does_not_exist, tag::build_now ), A );  // zero winding
	Cell PB ( tag::segment, P .reverse ( tag::surely_exists ), B );
	environ::manif_type::set_winding_1 ( PB, AB );
	Cell PC ( tag::segment, P .reverse ( tag::surely_exists ), C );
	environ::manif_type::set_winding_2 ( PC, AB, BC );
	Cell PD ( tag::segment, P .reverse ( tag::surely_exists ), D );
	environ::manif_type::set_winding_1 ( PD, AD );
	update_filaments_near ( PA, interf );
	update_filaments_near ( PB, interf );
	update_filaments_near ( PC, interf );
	update_filaments_near ( PD, interf );

	Cell ACP ( tag::triangle, CA .reverse ( tag::surely_exists ),
	                          PC .reverse ( tag::does_not_exist, tag::build_now ), PA );
	Cell BCP ( tag::triangle, BC, PC .reverse ( tag::surely_exists ), PB );
	Cell ADP ( tag::triangle, AD, PD .reverse ( tag::does_not_exist, tag::build_now ), PA );
	Cell BDP ( tag::triangle, DB .reverse ( tag::surely_exists ), PD .reverse ( tag::surely_exists ), PB );

	Cell ABC_alt = remove_face ( ABC, interf );
	// ABC .remove_from ( interf );
	Cell BAD_alt = remove_face ( BAD, interf );
	// BAD .remove_from ( interf );
	Cell ABP ( tag::triangle, AB, PB .reverse ( tag::does_not_exist, tag::build_now ), PA );
	ACP .reverse ( tag::does_not_exist, tag::build_now ) .add_to ( interf );
	BCP .add_to ( interf );
	ADP .add_to ( interf );
	BDP .reverse ( tag::does_not_exist, tag::build_now ) .add_to ( interf );

	Cell ABCP ( tag::tetrahedron, ABC_alt, BCP .reverse ( tag::does_not_exist, tag::build_now ),
	                              ACP, ABP .reverse ( tag::does_not_exist, tag::build_now )     );
	Cell ABDP ( tag::tetrahedron, BAD_alt, ABP, BDP,
	                              ADP .reverse ( tag::does_not_exist, tag::build_now ) );
	ABCP .add_to ( msh );
	ABDP .add_to ( msh );

	// we do not bother to eliminate segments and faces from sets/maps ***_fill_tetrah***
	// once the faces have been removed from interf,
	// they will be eliminated by the 'if' at the beginning of the loop

	ver_fill_burried .insert (A);
	ver_fill_burried .insert (B);
	seg_fill_crest .insert ( { ACP .reverse ( tag::surely_exists ), CA } );
	seg_fill_crest .insert ( { BCP, BC } );
	seg_fill_crest .insert ( { ADP, AD } );
	seg_fill_crest .insert ( { BDP .reverse ( tag::surely_exists ), DB } );
	seg_fill_membrane_2s .insert ( { ADP, PA } );
	seg_fill_membrane_2s .insert ( { BCP, PB } );
	seg_fill_membrane_2s .insert ( { ACP .reverse ( tag::surely_exists ), PC } );
	seg_fill_membrane_2s .insert ( { BDP .reverse ( tag::surely_exists ), PD } );
	ver_fill_cavity .insert (A);
	ver_fill_cavity .insert (B);
	seg_fill_two_crest .push_back ( { ACP .reverse ( tag::surely_exists ), CA } );
	seg_fill_two_crest .push_back ( { BCP, BC } );
	seg_fill_two_crest .push_back ( { ADP, AD } );
	seg_fill_two_crest .push_back ( { BDP .reverse ( tag::surely_exists ), DB } );

	// add segments to 'dangerous_cavities' as done in 'fill_one_tetra_crest_core' !

	// improve the location of P, taking into account nearby vertices and triangles
	// also build filaments when appropriate
	// may build new segments, triangles and tetrahedra around P, add them to 'interf' and to 'msh'
	typename environ::manif_type::set_of_winding_cells other_vertices;  // empty
	typename environ::manif_type::set_of_winding_cells vertices_from_walls
		{ environ::manif_type::add_winding ( A, 0 ),
		  environ::manif_type::add_winding_1 ( B, AB ),
		  environ::manif_type::add_winding_neg_1 ( C, CA ),
		  environ::manif_type::add_winding_1 ( D, AD )     };
	union_of < typename environ::manif_type::set_of_winding_cells > vertices
		{	vertices_from_walls, other_vertices };
	relocate_3d < environ > ( msh, interf, P, walls, vertices );

	#if FRONTAL_VERBOSITY > 1
	std::cout << "frontal-3d.cpp line 2705, P [ ";
	for ( size_t i = 0; i < frontal_nb_of_coords; i++ )
	{	const Function & x = Manifold::working .coordinates() [i];
		std::cout << x(P) << " ";                                   }
	std::cout << "]" << std::endl;
	#endif  // FRONTAL_VERBOSITY > 1

	// we insert P in the cloud only now because in relocate_3d we do not want it there
	// another reason is that P has been moved during 'relocate_3d'
	// now its location in space is frozen
	// so it will be inserted at the right position in the metric tree
	// 'relocate_3d' may have eliminated P from 'interf' so we must ask :
	if ( P .belongs_to ( interf, tag::cell_has_low_dim ) )
		environ::manif_type::node_container .insert (P);

	#if FRONTAL_VERBOSITY > 1
	std::cout << "drawers " << seg_fill_two_crest .size() << " + ";
	for ( size_t i = 0; i < seg_fill_two_crest_drawer .size(); i++ )
		std::cout << " " << seg_fill_two_crest_drawer [i] .size();
	std::cout << std::endl << std::flush;
	#endif  // FRONTAL_VERBOSITY > 1
	
	#if FRONTAL_VERBOSITY > 1	
	if ( frontal_counter < frontal_counter_max )  frontal_counter ++;
	// 'relocate_3d' may have already incremented 'frontal_counter'
	#endif

}  // end of  fill_two_tetrahedra_crest_core

//------------------------------------------------------------------------------------------------------//


template < class environ >
inline bool fill_tri_membrane_3_sides ( Mesh & msh, Mesh & interf )   // hidden in anonymous namespace
	
// try to fill a new triangle whose three sides are already in 'msh'
// a pair of triangles, actually, like a double membrane
// ideally, these two faces should be each other's reverse
// however, even STSI meshes are unable to handle such a configuration
// this is why we create two independent faces and keep the pair in 'dummy_faces'
// later, the first one will be replaced by the second one's reverse (this is the job of 'remove_face')

// the set 'seg_fill_membrane_3s' contains positive segments

// return false if did nothing

// perhaps add an orientation check
// for the extreme cases of an initial interface having a thin passage

// 'msh' is a Mesh::Fuzzy made of tetrahedra (being built now)
// 'interf' is a Mesh::STSI made of triangles
	
// 'environ' describes the type of manifold we are working in
// and how we treat information about metric and exterior product
// environ::manif_type could be ManifoldNoWinding or ManifoldQuotient (defined in frontal.cpp)
// environ::metric_type may be one of ISRContainer::{Inactive,Active} (defined in frontal.cpp)
// environ::ext_prod_type may be ExtProd3d, 3dRev (defined in frontal.cpp)
	
{	assert ( tri_fill_isolated  .empty() );

	for ( std::set < Cell > ::iterator
	      it = seg_fill_membrane_3s .begin(); it != seg_fill_membrane_3s .end();  )

	{	Cell AB = * it;
		assert ( AB .dim() == 1 );
		if ( not AB .belongs_to ( interf, tag::cell_has_low_dim ) )
		{	it = seg_fill_membrane_3s .erase ( it );
			continue;                                 }  // to next segment in seg_fill_membrane_3s

		if ( not AB .is_regular ( tag::within_STSI_mesh, interf ) )
			// AB may belong to a membrane, it will still be regular
			// the only case we are discarding is when AB belongs to a wall of membranes
			// even if we wanted to deal with this exotic case,
			// it would be difficult to decide the neighbourhood relations after the insertion
		{	it = seg_fill_membrane_3s .erase ( it );
			continue;                                 }  // to next segment in seg_fill_membrane_3s
		
		Cell B = AB .tip();
		Cell A = AB .base() .reverse ( tag::surely_exists );
		Cell ABC = interf .cell_behind ( AB, tag::surely_exists );
		Cell BAD = interf .cell_in_front_of ( AB, tag::surely_exists );
		Cell BC = ABC .boundary() .cell_in_front_of ( B, tag::surely_exists );
		Cell C = BC .tip();
		Cell AD = BAD .boundary() .cell_in_front_of ( A, tag::surely_exists );
		Cell D = AD .tip();
				
		std::set < Cell > forbidden_vertices { A, B, C, D };
		// 'forbidden_vertices' will have four elements,
		// or just three if AB is the edge of a membrane, or just two if AB is filament

		// analyse all segments in 'interf' having A as base
		// 'interf' is a Mesh::STSI, we use an iterator with no order required
		// which produces segments possibly belonging to different components of interf,
		// possibly filaments
		std::map < Cell, Cell > map_A_ver_seg;
		{ // just a block of code for hiding 'it_A'
		Mesh::Iterator it_A = interf .iterator ( tag::over_segments, tag::pointing_away_from, A );
		for ( it_A .reset(); it_A .in_range(); it_A ++ )
		{	Cell seg = * it_A;
			assert ( seg .base() .reverse() == A );
			Cell ver = seg .tip();
			#if FRONTAL_VERBOSITY > 1
			std::cout << "A";
			#endif
			if ( forbidden_vertices .find ( ver ) !=  forbidden_vertices .end() )  continue;
			map_A_ver_seg .insert ( { ver, seg } );                                           }
		} // just a block of code
						 
		// analyse all segments in 'interf' having B as base
		// 'interf' is a Mesh::STSI, we use an iterator with no order required
		// which produces segments possibly belonging to different components of interf,
		// possibly filaments
		// we olny insert those vertices already in keys of 'map_A_ver_seg',
		// so this is actually the intersection between them
		std::set < Cell > set_B_seg;
		{ // just a block of code for hiding 'it_B'
		Mesh::Iterator it_B = interf .iterator ( tag::over_segments, tag::pointing_away_from, B );
		for ( it_B .reset(); it_B .in_range(); it_B ++ )
		{	Cell seg = * it_B;
			assert ( seg .base() .reverse() == B );
			#if FRONTAL_VERBOSITY > 1
			std::cout << "B";
			#endif
			Cell ver = seg .tip();
			if ( map_A_ver_seg .find ( ver ) == map_A_ver_seg .end() )  continue;
			set_B_seg .insert ( seg );                                             }
		} // just a block of code

		#if FRONTAL_VERBOSITY > 1
		std::cout << " map_A_ver_seg " << map_A_ver_seg .size()
		          << " set_B_seg " << set_B_seg .size() << std::endl << std::flush;
		#endif
		
		size_t size_set_B = set_B_seg .size();

		if ( size_set_B == 0 )
		{	it = seg_fill_membrane_3s .erase ( it );
			continue;                                 }  // to next segment in seg_fill_membrane_3s
		
		Cell BP = * set_B_seg .begin();
		assert ( BP .base() .reverse() == B );
		Cell P = BP .tip();
		std::map < Cell, Cell > ::iterator itt = map_A_ver_seg .find (P);
		assert ( itt != map_A_ver_seg .end() );
		Cell AP = itt->second;
		assert ( AP .base() .reverse() == A );
		assert ( AP .tip() == P );

		#ifndef NDEBUG  // DEBUG mode
		// let's check triangle ABP does not already exist in 'msh'
		Mesh::Iterator it_AB = msh .iterator ( tag::over_cells_of_dim, 2, tag::around, AB );
		for ( it_AB .reset(); it_AB .in_range(); it_AB ++ )
		{	Cell tri = * it_AB;
			assert ( not P .belongs_to ( tri .boundary(), tag::cell_has_low_dim ) );  }
		#endif  // DEBUG

		#if FRONTAL_VERBOSITY > 1
		std::cout << "frontal-3d.cpp line 2568, insert one tri membrane, 3 sides"
		          << std::endl << std::flush;
		#endif

		{  // just a block of code for hiding variable names
		bool BP_is_filament = false;
		if ( BP .is_regular ( tag::within_STSI_mesh, interf ) )
		{	Cell tri = interf .cell_in_front_of ( BP, tag::surely_exists );
			BP_is_filament = tri .boundary() .number_of ( tag::cells_of_max_dim ) == 2;  }

		if ( BP_is_filament )  size_set_B --;  // a filament counts twice
		if ( size_set_B <= 1 )  it = seg_fill_membrane_3s .erase ( it );
		// if more than one candidate found, leave AB in seg_fill_membrane_3s
		// the other candidates will be dealt with at next calls of fill_tri_membrane_3_sides
		// unless the configuration changes before that

		bool AB_is_filament = ABC .boundary() .number_of ( tag::cells_of_max_dim ) == 2;
		bool AP_is_filament = false;
		if ( AP .is_regular ( tag::within_STSI_mesh, interf ) )
			// note : a filament is regular (it has two neighbour faces, albeit degenerated)
			// a singular face is e.g. where a previously existing membrane touches other triangle
		{	Cell tri = interf .cell_in_front_of ( AP, tag::surely_exists );
			AP_is_filament = tri .boundary() .number_of ( tag::cells_of_max_dim ) == 2;  }

		// we never have three filaments
		assert ( not ( AB_is_filament and AP_is_filament and BP_is_filament ) );

		// if we have two filaments, eliminate the longest one
		// membrane ABP may be built by 'fill_tri_membrane_2_sides'
		if ( AB_is_filament and AP_is_filament )
		{	break_longest_filament < environ > ( AB, AP, A, interf );  continue;  }  // to next candidate
		if ( AB_is_filament and BP_is_filament )
		{	break_longest_filament < environ > ( AB, BP, A, interf );  continue;  }  // to next candidate
		if ( BP_is_filament and AP_is_filament )
		{	break_longest_filament < environ > ( BP, AP, A, interf );  continue;  }  // to next candidate

		// if we have one filament, eliminate it
		// membrane ABP may later be built by 'fill_tri_membrane_2_sides'
		if ( AB_is_filament )
		{	break_filament ( AB, interf );  continue;  }  // to next candidate
		if ( AP_is_filament )
		{	break_filament ( AP, interf );  continue;  }  // to next candidate
		if ( BP_is_filament )
		{	break_filament ( BP, interf );  continue;  }  // to next candidate
		}  // just a block of code

		// in regular cases, it suffices to add one of the faces of the membrane with tag::force
		// the second face can be added with no supplementary tag,
		// it will just fit where neighbours are missing
		// however, if AB or AP or BP are singular in the STSI mesh interface,
		// for instance, if the future membrane touches a wall of adjacent membranes,
		// tag::force is not enough; maniFEM does not know who's who's neighbour
		// in this case, we need to provide explicit neighbourhood relations when we insert faces
		// for AB, it is obvious, we just use ABC
		// for AP and BP it is really difficult
		// one solution is to start from what we know : ABC
		// rotate around A until we meet AP, rotate around B until we meet BP
		
		// the above works OK if there is a path, within the triangles around A, from AB to AP
		// in rare cases, however, there is no such path; the triangles around B make a disconnected graph
		// in such cases we just pray AP is regular (the same for BP)
		
		// we know that none of AB, AP and BP is filament (see above)
		// but there are cases like this which do not involve any filament
		// see https://codeberg.org/cristian.barbarosie/manifem-manual
		// files 'membrane-01.{msh,png}' (this is for membrane_2_sides)

		// loop below detects if such a path exists
		// we take this opportunity to fill in 'rels',
		// neighbourhood relations used when adding the first face to 'interf'
		std::map < Cell, Cell > rels { { AB, ABC } };  // rels [ AB ] = ABC

		bool path_from_AB_to_AP = true;  // path from AB to AP, around A
		Cell AC = ABC .boundary() .cell_in_front_of ( C, tag::surely_exists )
			.reverse ( tag::surely_exists );
		{ // just a block of code for hiding 'tri'
		Cell tri = ABC;
		while ( true )
		{	Cell seg = tri .boundary() .cell_in_front_of ( A, tag::surely_exists );
			if ( seg == AC )  // we completed a loop around B, haven't met AP
			{	path_from_AB_to_AP = false;  break;  }
			if ( seg == AP )
			{	tag::Util::insert_new_elem_in_map ( rels, { AP, tri } );  // rels [ AP ] = tri
				break;                                             }
			tri = interf .cell_in_front_of ( seg, tag::seen_from, tri, tag::surely_exists );  }
		} // just a block of code
  
		if ( not path_from_AB_to_AP )  // let's pray AP is regular
			if ( not AP .is_regular ( tag::within_STSI_mesh, interf ) )
			{	std::cout << "trouble in frontal-3d.cpp, fill_tri_membrane_3_sides AP"
				          << std::endl << std::flush;
				exit (1);                                                               }

		bool path_from_AB_to_BP = true;  // path from AB to BP, around B
		Cell BA = AB .reverse ( tag::surely_exists );
		{ // just a block of code for hiding 'tri'
		Cell tri = ABC;
		while ( true )
		{	Cell seg = tri .boundary() .cell_in_front_of ( B, tag::surely_exists );
			if ( seg == BA )  // we completed a loop around B, haven't met BP
			{	path_from_AB_to_BP = false;  break;  }
			if ( seg == BP )
			{	tag::Util::insert_new_elem_in_map ( rels, { BP, tri } );  // rels [ BP ] = tri
				break;                                             }
			tri = interf .cell_in_front_of ( seg, tag::seen_from, tri, tag::surely_exists );  }
		} // just a block of code
  
		if ( not path_from_AB_to_BP )  // let's pray BP is regular
			if ( not BP .is_regular ( tag::within_STSI_mesh, interf ) )  // kept_seg == BP
			{	std::cout << "trouble in frontal-3d.cpp, fill_tri_membrane_3_sides BP"
				          << std::endl << std::flush;
				exit (1);                                                               }
	
		Cell PB = BP .reverse ( tag::surely_exists );
		Cell BAP ( tag::triangle, AB .reverse ( tag::surely_exists ), AP, PB );
		Cell PA = AP .reverse ( tag::surely_exists );
		Cell ABP ( tag::triangle, AB, BP, PA );
		tag::Util::insert_new_elem_in_map ( dummy_faces, { BAP, ABP } );
		tag::Util::insert_new_elem_in_map ( dummy_faces, { ABP, BAP } );
		// dummy_faces [ BAP ] = ABP; dummy_faces [ ABP ] = BAP;
		// 'BAP' will be later replaced by  ABP .reverse(), or the other way around

		BAP .add_to ( interf, tag::neighbourhood_relations, rels, tag::force_other_faces );
		ABP .add_to ( interf );
		
		// add pairs (triangle,segment) to 'dangerous_cavities'
		{  // just a block of code for hiding variable names
		std::vector < Cell > two_faces_of_membrane { BAP, ABP };
		for ( std::vector < Cell > ::const_iterator
		        it_two_faces = two_faces_of_membrane .begin();
		      it_two_faces != two_faces_of_membrane .end();  it_two_faces ++ )
		{	Cell face = * it_two_faces;
			Mesh::Iterator it_sides = face .boundary() .iterator
				( tag::over, tag::cells_of_max_dim, tag::orientation_compatible_with_mesh );
			for ( it_sides .reset(); it_sides .in_range(); it_sides ++ )
			{	Cell seg = * it_sides;
				Cell Q = seg .tip();  // Q in {B,C,D}
				// rotate around Q :
				Cell tri = face;
				while ( true )
				{	Cell next_tri =
						interf .cell_in_front_of ( seg, tag::seen_from, tri, tag::surely_exists );
					if ( next_tri == face )  break;
					dangerous_cavities .push_back ( { tri, seg } );
					tri = next_tri;
					seg = tri .boundary() .cell_behind ( Q, tag::surely_exists );                 }  }  }
		}  // just a block of code

		// we do not insert ABP and BAP in 'tri_fill_isolated' because
		// this happens after 'fill_one_tetra_ground'
		ver_fill_burried .insert (A);
		ver_fill_burried .insert (B);
		ver_fill_burried .insert (P);
		seg_fill_crest .insert ( { ABP, AB } );
		seg_fill_crest .insert ( { BAP, AB .reverse ( tag::surely_exists ) } );
		seg_fill_crest .insert ( { BAP, AP } );
		seg_fill_crest .insert ( { ABP, AP .reverse ( tag::surely_exists ) } );
		seg_fill_crest .insert ( { ABP, BP } );
		seg_fill_crest .insert ( { BAP, BP .reverse ( tag::surely_exists ) } );
		ver_fill_cavity .insert (A);
		ver_fill_cavity .insert (B);
		ver_fill_cavity .insert (P);
		seg_fill_two_crest .push_back ( { ABP, AB } );
		seg_fill_two_crest .push_back ( { BAP, AB .reverse ( tag::surely_exists ) } );
		seg_fill_two_crest .push_back ( { BAP, AP } );
		seg_fill_two_crest .push_back ( { ABP, AP .reverse ( tag::surely_exists ) } );
		seg_fill_two_crest .push_back ( { ABP, BP } );
		seg_fill_two_crest .push_back ( { BAP, BP .reverse ( tag::surely_exists ) } );
			
		#if FRONTAL_VERBOSITY > 1	
		frontal_counter ++;
		#endif
		return true;                                                                              }

	return false;

}  // end of  fill_tri_membrane_3_sides
	
//------------------------------------------------------------------------------------------------------//


template < class environ >
inline bool fill_dangerous_cavity ( Mesh & msh, Mesh & interf )   // hidden in anonymous namespace
	
// some cavities are considered dangerous - they have two "completely concave" vertices
// we call a vertex "completely concave" if all segments adjacent to it form concave angles
// however, the criterion is more complex, see below

// 'msh' is a Mesh::Fuzzy made of tetrahedra (being built now)
// 'interf' is a Mesh::STSI made of triangles
	
// 'environ' describes the type of manifold we are working in
// and how we treat information about metric and exterior product
// environ::manif_type could be ManifoldNoWinding or ManifoldQuotient (defined in frontal.cpp)
// environ::metric_type may be one of ISRContainer::{Inactive,Active} (defined in frontal.cpp)
// environ::ext_prod_type may be ExtProd3d, 3dRev (defined in frontal.cpp)
	
{	assert ( tri_fill_isolated .empty() );
	assert ( seg_fill_membrane_3s .empty() );

	while ( not dangerous_cavities .empty() )

	{	Cell ABC = dangerous_cavities .back() .first;
		Cell AB  = dangerous_cavities .back() .second;
		dangerous_cavities .pop_back();
		
		if ( not ABC .belongs_to ( interf, tag::orientation_compatible_with_mesh ) )
			continue;   // to next dangerous cavity
		assert ( AB .belongs_to ( ABC .boundary(), tag::orientation_compatible_with_mesh ) );
		Cell B = AB .tip();
		Cell A = AB .base() .reverse ( tag::surely_exists );
		Cell BC = ABC .boundary() .cell_in_front_of ( B, tag::surely_exists );
		Cell CA = ABC .boundary() .cell_behind ( A, tag::surely_exists );
		assert ( BC .tip() == CA .base() .reverse ( tag::surely_exists ) );
		Cell C = BC .tip();
		Cell BAD = interf .cell_in_front_of ( AB, tag::seen_from, ABC, tag::surely_exists );
		Cell AD = BAD .boundary() .cell_in_front_of ( A, tag::surely_exists );
		Cell DB = BAD .boundary() .cell_behind ( B, tag::surely_exists );
		Cell D = AD .tip();
		assert ( D == DB .base() .reverse ( tag::surely_exists ) );
		if ( C == D )  // 'AB' is the free edge of a membrane
			continue;   // to next dangerous cavity

		// is this cavity still "dangerous" ? or has it changed in the meanwhile ?
		// in many cases, it wasn't really dangerous in the first place

		#if FRONTAL_VERBOSITY > 1
		std::cout << "frontal-3d.cpp line 3173, [";
		for ( size_t i = 0; i < frontal_nb_of_coords; i++ )
		{	const Function & x = Manifold::working .coordinates() [i];
			std::cout << x(A) << " ";                                   }
		std::cout << "] [";
		for ( size_t i = 0; i < frontal_nb_of_coords; i++ )
		{	const Function & x = Manifold::working .coordinates() [i];
			std::cout << x(B) << " ";                                   }
		std::cout << "], ";
		#endif  // FRONTAL_VERBOSITY > 1

		const double angle_at_AB = angle_3d < environ > ( interf, ABC, AB );
		#if FRONTAL_VERBOSITY > 1
		std::cout << angle_at_AB << std::endl;
		#endif
		// if angle at AB is wide, give up
		if ( angle_at_AB > 0. )  continue;   // to next dangerous cavity

		const size_t neighb_A = nb_of_triangles_around ( interf, A, ABC );
		const size_t neighb_B = nb_of_triangles_around ( interf, B, ABC );
		
		// if angle at AB is tight (90º), we consider the cavity is dangerous,
		// independently of the angles in the neighbourhood
		if ( angle_at_AB < -2. )  goto label_fill_cavity;
		// if nothing else fits, 'fill_two_tetrahedra_crest_core' will be invoked

		#if FRONTAL_VERBOSITY > 1
		std::cout << "frontal-3d.cpp line 3197, ";
		#endif

		{  // just a block of code for hiding variable names
		// second test : measure angles
		size_t nb_wide_angles = 0;
		double min_angle = 0.;
		bool not_dangerous = false;

		// loop below is equivalent to using an iterator around A with tag::backwards
		// but such an iterator does not work correctly for a STSI mesh
		Cell tri = ABC;
		Cell seg = AB;  // AB has already been analysed
		while ( true )
		{	tri = interf .cell_in_front_of ( seg, tag::seen_from, tri, tag::surely_exists );
			if ( tri == ABC )  break;
			seg = tri .boundary() .cell_in_front_of ( A, tag::surely_exists );
			const double ang = angle_3d < environ > ( interf, tri, seg );
			#if FRONTAL_VERBOSITY > 1
			std::cout << ang << " ";
			#endif
			if ( ang > 0.4 )
			{	not_dangerous = true;  break;  }
			if ( ang < min_angle )  min_angle = ang;
			if ( ang > 0. )  nb_wide_angles ++;                                                }
		#if FRONTAL_VERBOSITY > 1
		std::cout << std::endl;
		#endif
		if ( not_dangerous    )  goto label_fill_valley;
		if ( min_angle > -0.6 )  goto label_fill_valley;

		#if FRONTAL_VERBOSITY > 1
		std::cout << "frontal-3d.cpp line 3233, ";
		#endif

		// loop below is equivalent to using an iterator around B with tag::backwards
		// but such an iterator does not work correctly for a STSI mesh
		assert ( tri == ABC );
		seg = BC;
		min_angle = 0.;
		while ( true )
		{	const double ang = angle_3d < environ > ( interf, tri, seg );
			#if FRONTAL_VERBOSITY > 1
			std::cout << ang << " ";
			#endif
			if ( ang > 0.4 )
			{	not_dangerous = true;  break;  }
			if ( ang < min_angle )  min_angle = ang;
			if ( ang > 0. )  nb_wide_angles ++;
			tri = interf .cell_in_front_of ( seg, tag::seen_from, tri, tag::surely_exists );
			seg = tri .boundary() .cell_in_front_of ( B, tag::surely_exists );
			if ( seg == AB .reverse ( tag::surely_exists ) )  break;                          }
		#if FRONTAL_VERBOSITY > 1
		std::cout << std::endl;
		#endif
		if ( not_dangerous      )  goto label_fill_valley;
		if ( min_angle > -0.6   )  goto label_fill_valley;
		if ( nb_wide_angles > 2 )  goto label_fill_valley;
		}  // just a block of code

		label_fill_cavity :
		// if we reached this line of the code, the cavity is guaranteed to be filled
		// if nothing else fits, 'fill_two_tetrahedra_crest_core' will be invoked
		
		#if FRONTAL_VERBOSITY > 1
		std::cout << "frontal-3d.cpp line 3266, found dangerous cavity" << std::endl;
		#endif

		double var;   // not used here
		if ( fit_for_one_tetra < environ > ( interf, AB, ABC, BAD, var )
		     and not danger_of_flat_tetra < environ >
			  ( interf, AB, ABC, BAD, tag::mobile_vertex,
		       Cell ( tag::non_existent ), tag::both_directions )        )
		{	if ( ( neighb_A == 4 ) and ( neighb_B == 4 ) )
			{	Cell CEA = interf .cell_in_front_of ( CA, tag::seen_from, ABC, tag::surely_exists );
				Cell EA = CEA .boundary() .cell_behind ( A, tag::surely_exists );
				Cell DAE = interf .cell_in_front_of ( EA, tag::seen_from, CEA, tag::surely_exists );
				assert ( DAE == interf .cell_in_front_of ( AD, tag::seen_from, BAD, tag::surely_exists ) );
				Cell BFC = interf .cell_in_front_of ( BC, tag::seen_from, ABC, tag::surely_exists );
				Cell BF = BFC .boundary() .cell_in_front_of ( B, tag::surely_exists );
				Cell DFB = interf .cell_in_front_of ( BF, tag::seen_from, BFC, tag::surely_exists );
				assert ( DFB == interf .cell_in_front_of ( DB, tag::seen_from, BAD, tag::surely_exists ) );
				if ( fit_for_one_tetra < environ > ( interf, EA, CEA, DAE, var ) and
				     fit_for_one_tetra < environ > ( interf, BF, BFC, DFB, var )     )
					// build three tetrahedra, ABCD, ACDE and BCDF
				{	Cell ABC_alt = remove_face ( ABC, interf );
					// ABC .remove_from ( interf )
					Cell BAD_alt = remove_face ( BAD, interf );
					// BAD .remove_from ( interf )
					fill_one_tetra_crest_core < environ >
							( msh, interf, ABC_alt, BAD_alt, AB, CA, AD, BC, DB, A, B, C, D );
					// tetrahedra ACDE and BCDF will be subsequently built by 'fill_one_tetra_ground'
					return true;                                                              }
			}  // end of  if ( ( neighb_A == 4 ) and ( neighb_B == 4 ) )
			if ( ( neighb_A == 4 ) and ( neighb_B > 4 ) )
			{	Cell CEA = interf .cell_in_front_of ( CA, tag::seen_from, ABC, tag::surely_exists );
				Cell EA = CEA .boundary() .cell_behind ( A, tag::surely_exists );
				Cell DAE = interf .cell_in_front_of ( EA, tag::seen_from, CEA, tag::surely_exists );
				assert ( DAE == interf .cell_in_front_of ( AD, tag::seen_from, BAD, tag::surely_exists ) );
				if ( fit_for_one_tetra < environ > ( interf, EA, CEA, DAE, var ) )
					// build two tetrahedra, ABCD and ACDE
				{	Cell ABC_alt = remove_face ( ABC, interf );
					// ABC .remove_from ( interf )
					Cell BAD_alt = remove_face ( BAD, interf );
					// BAD .remove_from ( interf )
					fill_one_tetra_crest_core < environ >
							( msh, interf, ABC_alt, BAD_alt, AB, CA, AD, BC, DB, A, B, C, D );
					// tetrahedron ACDE will be subsequently built by 'fill_one_tetra_ground'
					return true;                                                              }
			}  // end of  if ( ( neighb_A == 4 ) and ( neighb_B > 4 ) )
			if ( ( neighb_A > 4 ) and ( neighb_B == 4 ) )
			{	Cell BFC = interf .cell_in_front_of ( BC, tag::seen_from, ABC, tag::surely_exists );
				Cell BF = BFC .boundary() .cell_in_front_of ( B, tag::surely_exists );
				Cell DFB = interf .cell_in_front_of ( BF, tag::seen_from, BFC, tag::surely_exists );
				assert ( DFB == interf .cell_in_front_of ( DB, tag::seen_from, BAD, tag::surely_exists ) );
				if ( fit_for_one_tetra < environ > ( interf, BF, BFC, DFB, var )     )
					// build two tetrahedra, ABCD and BCDF
				{	Cell ABC_alt = remove_face ( ABC, interf );
					// ABC .remove_from ( interf )
					Cell BAD_alt = remove_face ( BAD, interf );
					// BAD .remove_from ( interf )
					fill_one_tetra_crest_core < environ >
							( msh, interf, ABC_alt, BAD_alt, AB, CA, AD, BC, DB, A, B, C, D );
					// tetrahedron BCDF will be subsequently built by 'fill_one_tetra_ground'
					return true;                                                              }
			}  // end of  if ( ( neighb_A > 4 ) and ( neighb_B == 4 ) )
			if ( ( neighb_A > 4 ) and ( neighb_B > 4 ) )
				// build tetrahedron ABCD
			{	Cell ABC_alt = remove_face ( ABC, interf );
				// ABC .remove_from ( interf )
				Cell BAD_alt = remove_face ( BAD, interf );
				// BAD .remove_from ( interf )
				fill_one_tetra_crest_core < environ >
						( msh, interf, ABC_alt, BAD_alt, AB, CA, AD, BC, DB, A, B, C, D );
				return true;                                                              }
		}  // end of  if ( fit_for_one_tetra < environ > ( interf, AB, ABC, BAD, var ) )

		if ( neighb_A == 4 )
		{	Cell ACE = interf .cell_in_front_of ( CA, tag::seen_from, ABC, tag::surely_exists );
			Cell EA = ACE .boundary() .cell_behind ( A, tag::surely_exists );
			Cell DAE = interf .cell_in_front_of ( EA, tag::seen_from, ACE, tag::surely_exists );
			assert ( DAE == interf .cell_in_front_of ( AD, tag::seen_from, BAD, tag::surely_exists ) );
			if ( fit_for_one_tetra < environ > ( interf, CA, ABC, ACE, var ) and
			     fit_for_one_tetra < environ > ( interf, AD, BAD, DAE, var ) and
			     not danger_of_flat_tetra < environ >
			         ( interf, CA .reverse ( tag::surely_exists ), ACE, ABC,
			           tag::mobile_vertex, Cell ( tag::non_existent ), tag::one_direction ) and
			     not danger_of_flat_tetra < environ >
			         ( interf, AD, BAD, DAE,
			           tag::mobile_vertex, Cell ( tag::non_existent ), tag::one_direction )      )
				// build two tetrahedra, ABCE and ABDE
			{	Cell CE = ACE .boundary() .cell_in_front_of ( C, tag::surely_exists );
				Cell E = CE .tip();
				assert ( E == EA .base() .reverse ( tag::surely_exists ) );
				Cell ABC_alt = remove_face ( ABC, interf );
				// ABC .remove_from ( interf )
				Cell ACE_alt = remove_face ( ACE, interf );
				// ACE .remove_from ( interf )
				fill_one_tetra_crest_core < environ >
						( msh, interf, ACE_alt, ABC_alt,
						  CA .reverse ( tag::surely_exists ), EA, AB, CE, BC, A, C, E, B );
				// tetrahedron ABDE will be subsequently built by 'fill_one_tetra_ground'
				return true;                                                               }
		}  // end of  if ( neighb_A == 4 )
		
		if ( neighb_B == 4 )
		{	Cell CBF = interf .cell_in_front_of ( BC, tag::seen_from, ABC, tag::surely_exists );
			Cell BF = CBF .boundary() .cell_in_front_of ( B, tag::surely_exists );
			Cell DFB = interf .cell_in_front_of ( BF, tag::seen_from, CBF, tag::surely_exists );
			assert ( DFB == interf .cell_in_front_of ( DB, tag::seen_from, BAD, tag::surely_exists ) );
			if ( fit_for_one_tetra < environ > ( interf, BC, ABC, CBF, var ) and
			     fit_for_one_tetra < environ > ( interf, DB, BAD, DFB, var ) and
			     not danger_of_flat_tetra < environ >
			         ( interf, BC, ABC, CBF,
			           tag::mobile_vertex, Cell ( tag::non_existent ), tag::one_direction ) and
			     not danger_of_flat_tetra < environ >
			         ( interf, DB .reverse ( tag::surely_exists ), DFB, BAD,
			           tag::mobile_vertex, Cell ( tag::non_existent ), tag::one_direction )      )
				// build two tetrahedra, ABCF and ABDF
			{	Cell FC = CBF .boundary() .cell_behind ( C, tag::surely_exists );
				Cell F = BF .tip();
				assert ( F == FC .base() .reverse ( tag::surely_exists ) );
				Cell ABC_alt = remove_face ( ABC, interf );
				// ABC .remove_from ( interf )
				Cell CBF_alt = remove_face ( CBF, interf );
				// CBF .remove_from ( interf )
				fill_one_tetra_crest_core < environ >
						( msh, interf, CBF_alt, ABC_alt,
						  BC .reverse ( tag::surely_exists ), FC, CA, BF, AB, C, B, F, A );
				// tetrahedron ABDF will be subsequently built by 'fill_one_tetra_ground'
				return true;                                                               }
		}  // end of  if ( neighb_B == 4 )

		if ( ( neighb_A == 4 ) and ( neighb_B == 4 ) )
			// we build six new tetrahedra
			// a new vertex will be placed at the barycenter of the six vertices of the cavity
			// we relocate it, otherwise thin tetrahedra may show up

 			//                  C
			//
			//      E       A       B       F
			//
			//                  D

		{	// ABC, AB, BC, CA, A, B, C already declared
			Cell ACE = interf .cell_in_front_of ( CA, tag::seen_from, ABC, tag::surely_exists );
			Cell CE = ACE .boundary() .cell_in_front_of ( C, tag::surely_exists );
			Cell E = CE .tip();
			Cell EA = ACE .boundary() .cell_behind ( A, tag::surely_exists );
			assert ( EA .base() .reverse ( tag::surely_exists ) == E );
			Cell AED = interf .cell_in_front_of ( EA, tag::seen_from, ACE, tag::surely_exists );
			Cell ED = AED .boundary() .cell_in_front_of ( E, tag::surely_exists );
			assert ( ED .tip() == D );
			Cell DA = AED .boundary() .cell_behind ( A, tag::surely_exists );
			assert ( DA .base() .reverse ( tag::surely_exists ) == D );
			// BAD, AD, DB, D already declared
			Cell BDF = interf .cell_in_front_of ( DB, tag::seen_from, BAD, tag::surely_exists );
			Cell DF = BDF .boundary() .cell_in_front_of ( D, tag::surely_exists );
			Cell F = DF .tip();
			Cell FB = BDF .boundary() .cell_behind (B, tag::surely_exists );
			assert ( FB .base() .reverse ( tag::surely_exists ) == F );
			Cell BFC = interf .cell_in_front_of ( FB, tag::seen_from, BDF, tag::surely_exists );
			Cell FC = BFC .boundary() .cell_behind ( C, tag::surely_exists );
			assert ( BFC .boundary() .cell_in_front_of ( F, tag::surely_exists ) == FC );
			assert ( interf .cell_in_front_of ( BC, tag::seen_from, ABC, tag::surely_exists ) == BFC );
			
			#if FRONTAL_VERBOSITY > 1
			std::cout << "building six new tetrahedra" << std::endl << std::flush;
			frontal_counter ++;
			#endif
		
			// 'walls' is a set of triangles if manif non-winding
			// it is a map (triangle, vertex with winding) if manif winding
			typename environ::manif_type::type_of_walls walls;
			std::set < Cell > triangles_on_floor { ABC, BAD, ACE, AED, BDF, BFC };
			typename environ::manif_type::winding_cell AA = environ::manif_type::add_winding ( A, 0 );
			for ( std::set < Cell > ::const_iterator it = triangles_on_floor .begin();
						it != triangles_on_floor .end(); it ++                            )
				environ::manif_type::insert_wall ( walls, * it, AA );

			const std::vector < double > e_AB = environ::manif_type::get_vector ( AB );
			const std::vector < double > e_CA = environ::manif_type::get_vector ( CA );
			const std::vector < double > e_EA = environ::manif_type::get_vector ( EA );
			const std::vector < double > e_DA = environ::manif_type::get_vector ( DA );
			const std::vector < double > e_FB = environ::manif_type::get_vector ( FB );

			Cell P ( tag::vertex );
			for ( size_t i = 0; i < frontal_nb_of_coords; i++ )
			{	const Function & x = Manifold::working .coordinates() [i];
				x(P) = x(A) + ( 2 * e_AB [i] - e_CA [i] - e_EA [i] - e_DA [i] - e_FB [i] ) / 6;  }
			// we consider both  A  and  P  have zero winding
			// Manifold::working .project (P)  not needed in 3D
			// 'compute_data' uses information from A to compute eigendirection and isr at P
			environ::metric_type::compute_data ( tag::at_point, P, tag::use_information_from, A );
			// P will be added to the cloud later, after 'relocate_3d'

			Cell PA ( tag::segment, P .reverse ( tag::does_not_exist, tag::build_now ), A );  // zero winding
			Cell PB ( tag::segment, P .reverse ( tag::surely_exists ), B );
			environ::manif_type::set_winding_1 ( PB, AB );
			Cell PC ( tag::segment, P .reverse ( tag::surely_exists ), C );
			environ::manif_type::set_winding_neg_1 ( PC, CA );
			Cell PD ( tag::segment, P .reverse ( tag::surely_exists ), D );
			environ::manif_type::set_winding_neg_1 ( PD, DA );
			Cell PE ( tag::segment, P .reverse ( tag::surely_exists ), E );
			environ::manif_type::set_winding_neg_1 ( PE, EA );
			Cell PF ( tag::segment, P .reverse ( tag::surely_exists ), F );
			environ::manif_type::set_winding_diff ( PF, AB, FB );
			update_filaments_near ( PA, interf );
			update_filaments_near ( PB, interf );
			update_filaments_near ( PC, interf );
			update_filaments_near ( PD, interf );
			update_filaments_near ( PE, interf );
			update_filaments_near ( PF, interf );

			Cell ABC_alt = remove_face ( ABC, interf );
			// ABC .remove_from ( interf )
			Cell BAD_alt = remove_face ( BAD, interf );
			// BAD .remove_from ( interf )
			Cell ACE_alt = remove_face ( ACE, interf );
			// ACE .remove_from ( interf )
			Cell AED_alt = remove_face ( AED, interf );
			// AED .remove_from ( interf )
			Cell BDF_alt = remove_face ( BDF, interf );
			// BDF .remove_from ( interf )
			Cell BFC_alt = remove_face ( BFC, interf );
			// BFC .remove_from ( interf )
			Cell PAB ( tag::triangle, PA, AB, PB .reverse ( tag::does_not_exist, tag::build_now ) );
			Cell PAC ( tag::triangle, PA, CA .reverse ( tag::surely_exists ),
			           PC .reverse ( tag::does_not_exist, tag::build_now )   );
			Cell PBC ( tag::triangle, PB, BC, PC .reverse ( tag::surely_exists ) );
			Cell PAD ( tag::triangle, PA, AD, PD .reverse ( tag::does_not_exist, tag::build_now ) );
			Cell PAE ( tag::triangle, PA, EA .reverse ( tag::surely_exists ),
			           PE .reverse ( tag::does_not_exist, tag::build_now )   );
			Cell PBD ( tag::triangle, PB, DB .reverse ( tag::surely_exists ),
			           PD .reverse ( tag::surely_exists )                    );
			Cell PBF ( tag::triangle, PB, FB .reverse ( tag::surely_exists ),
			           PF .reverse ( tag::does_not_exist, tag::build_now )   );
			Cell PCE ( tag::triangle, PC, CE, PE .reverse ( tag::surely_exists ) );
			Cell PED ( tag::triangle, PE, ED, PD .reverse ( tag::surely_exists ) );
			Cell PFC ( tag::triangle, PF, FC, PC .reverse ( tag::surely_exists ) );
			Cell PDF ( tag::triangle, PD, DF, PF .reverse ( tag::surely_exists ) );
			PCE .add_to ( interf );
			PED .add_to ( interf );
			PFC .add_to ( interf );
			PDF .add_to ( interf );

			Cell PABC ( tag::tetrahedron, ABC_alt, PAC,
			                              PAB .reverse ( tag::does_not_exist, tag::build_now ),
			                              PBC .reverse ( tag::does_not_exist, tag::build_now ) );
			Cell PABD ( tag::tetrahedron, BAD_alt, PBD, PAB,
			                              PAD .reverse ( tag::does_not_exist, tag::build_now ) );
			Cell PAEC ( tag::tetrahedron, ACE_alt, PAE,
			                              PCE .reverse ( tag::does_not_exist, tag::build_now ),
			                              PAC .reverse ( tag::does_not_exist, tag::build_now ) );
			Cell PAED ( tag::tetrahedron, AED_alt, PAD,
			                              PAE .reverse ( tag::does_not_exist, tag::build_now ),
			                              PED .reverse ( tag::does_not_exist, tag::build_now ) );
			Cell PBCF ( tag::tetrahedron, BFC_alt, PBC,
			                              PBF .reverse ( tag::does_not_exist, tag::build_now ),
			                              PFC .reverse ( tag::does_not_exist, tag::build_now ) );
			Cell PBDF ( tag::tetrahedron, BDF_alt, PBF,
			                              PBD .reverse ( tag::does_not_exist, tag::build_now ),
			                              PDF .reverse ( tag::does_not_exist, tag::build_now ) );
			PABC .add_to ( msh );
			PABD .add_to ( msh );
			PAEC .add_to ( msh );
			PAED .add_to ( msh );
			PBCF .add_to ( msh );
			PBDF .add_to ( msh );
			environ::manif_type::node_container .cond_remove ( A, interf );
			environ::metric_type::               cond_remove ( A, interf );
			environ::manif_type::node_container .cond_remove ( B, interf );
			environ::metric_type::               cond_remove ( B, interf );
			
			// improve the location of P, taking into account nearby vertices and triangles
			// also build filaments when appropriate
			// may build new segments, triangles and tetrahedra around P, add them to 'interf' and to 'msh'
			typename environ::manif_type::set_of_winding_cells vertices_from_walls
				{ environ::manif_type::add_winding ( A, 0 ),
				  environ::manif_type::add_winding_1 ( B, AB ),
				  environ::manif_type::add_winding_neg_1 ( C, CA ),
				  environ::manif_type::add_winding_1 ( D, AD ),
				  environ::manif_type::add_winding_neg_1 ( E, EA ),
				  environ::manif_type::add_winding_2 ( F, AD, DF )  };
			typename environ::manif_type::set_of_winding_cells other_vertices;  // empty
			union_of < typename environ::manif_type::set_of_winding_cells > vertices
					{	vertices_from_walls, other_vertices };
			relocate_3d < environ > ( msh, interf, P, walls, vertices, { { PC, PCE }, { PD, PDF } } );
			// see https://codeberg.org/cristian.barbarosie/manifem-manual, files 'diamond-0{1,2}.msh'
	
			#if FRONTAL_VERBOSITY > 1
			std::cout << "frontal-3d.cpp line 3621, P [ ";
			for ( size_t i = 0; i < frontal_nb_of_coords; i++ )
			{	const Function & x = Manifold::working .coordinates() [i];
				std::cout << x(P) << " ";                                   }
			std::cout << "]" << std::endl;
			#endif  // FRONTAL_VERBOSITY > 1

			// we insert P in the cloud only now because in relocate_3d we do not want it there
			// another reason is that P has been moved during 'relocate_3d'
			// now its location in space is frozen
			// so it will be inserted at the right position in the metric tree
			// 'relocate_3d' may have eliminated P from 'interf' so we must ask :
			if ( P .belongs_to ( interf, tag::cell_has_low_dim ) )
				environ::manif_type::node_container .insert (P);

			ver_fill_burried .insert (P);
			ver_fill_burried .insert (C);
			ver_fill_burried .insert (D);
			ver_fill_burried .insert (E);
			ver_fill_burried .insert (F);
			seg_fill_crest .insert ( { PCE, CE } );
			seg_fill_crest .insert ( { PED, ED } );
			seg_fill_crest .insert ( { PFC, FC } );
			seg_fill_crest .insert ( { PDF, DF } );
			seg_fill_membrane_2s .insert ( { PCE, PC } );
			seg_fill_membrane_2s .insert ( { PED, PE } );
			seg_fill_membrane_2s .insert ( { PFC, PF } );
			seg_fill_membrane_2s .insert ( { PDF, PD } );
			ver_fill_cavity .insert (P);
			ver_fill_cavity .insert (C);
			ver_fill_cavity .insert (D);
			ver_fill_cavity .insert (E);
			ver_fill_cavity .insert (F);
			seg_fill_two_crest .push_back ( { PCE, CE } );
			seg_fill_two_crest .push_back ( { PCE, PC } );
			seg_fill_two_crest .push_back ( { PED, ED } );
			seg_fill_two_crest .push_back ( { PED, PE } );
			seg_fill_two_crest .push_back ( { PFC, FC } );
			seg_fill_two_crest .push_back ( { PFC, PF } );
			seg_fill_two_crest .push_back ( { PDF, DF } );
			seg_fill_two_crest .push_back ( { PDF, PD } );
	
			return true;
		}  // end of  if ( ( neighb_A == 4 ) and ( neighb_B == 4 ) )

		#if FRONTAL_VERBOSITY > 1
		std::cout << "frontal-3d.cpp line 3667, building two tetrahedra, crest" << std::endl;
		#endif
		fill_two_tetrahedra_crest_core < environ > ( msh, interf, ABC, AB );
		return true;

		label_fill_valley :
		// if two tight angles are adjacent to the same triangle, treat them as a dangerous cavity
		// but only after trying to build a membrane_2s
		// otherwise, go to next dangerous cavity

		#if FRONTAL_VERBOSITY > 1
		if ( angle_at_AB < threshold_angle_for_valley )
		{	std::cout << "frontal-3d.cpp line 3679, possible valley" << std::endl;
			valley_tri_ver .push_back ( { ABC, A } );
			valley_tri_ver .push_back ( { ABC, B } );
			valley_tri_ver .push_back ( { BAD, A } );
			valley_tri_ver .push_back ( { BAD, B } );                               }
		else  std::cout << "frontal-3d.cpp line 3684, leave cavity as it is" << std::endl;
		#else  // FRONTAL_VERBOSITY <= 1
		if ( angle_at_AB < threshold_angle_for_valley )
		{	valley_tri_ver .push_back ( { ABC, A } );
			valley_tri_ver .push_back ( { ABC, B } );
			valley_tri_ver .push_back ( { BAD, A } );
			valley_tri_ver .push_back ( { BAD, B } );  }
		#endif  // FRONTAL_VERBOSITY

		continue;       // to next dangerous cavity
				
	}  // end of  for  over 'dangerous_cavities'

	return false;  // no dangerous cavity found, at least not dangerous enough
		
}  // end of  fill_dangerous_cavity

//------------------------------------------------------------------------------------------------------//


template < class environ >
inline bool fill_two_tetra_ground ( Mesh & msh, Mesh & interf )   // hidden in anonymous namespace
	
// try to fill two tetrahedra, building a new segment and two new triangles
// return false if did nothing

// 'msh' is a Mesh::Fuzzy made of tetrahedra (being built now)
// 'interf' is a Mesh::STSI made of triangles
	
// 'environ' describes the type of manifold we are working in
// and how we treat information about metric and exterior product
// environ::manif_type could be ManifoldNoWinding or ManifoldQuotient (defined in frontal.cpp)
// environ::metric_type may be one of ISRContainer::{Inactive,Active} (defined in frontal.cpp)
// environ::ext_prod_type may be ExtProd3d, 3dRev (defined in frontal.cpp)
	
{	assert ( tri_fill_isolated    .empty() );
	assert ( dangerous_cavities   .empty() );
	assert ( seg_fill_membrane_3s .empty() );

	// below we search for four adjacent triangles
	for ( std::set < Cell > ::iterator it  = ver_fill_burried .begin();
	      it != ver_fill_burried .end(); it = ver_fill_burried .erase ( it ) )
		
	{	Cell A = * it;
		assert ( A .belongs_to ( interf ) );
		std::set < Cell > already_analysed;

		label_search_for_ABC :
		// we look for four adjacent triangles around A
		// below we invoke an iterator around A without order required
		// it sweeps over all triangles, even if some segment is singular
		Cell ABC ( tag::non_existent );
		Mesh::Iterator itt = interf .iterator
			( tag::over_cells_of_max_dim, tag::orientation_compatible_with_mesh, tag::around, A );
		for ( itt .reset(); itt .in_range(); itt++ )
		{	Cell tri = * itt;
			if ( tri .boundary() .number_of ( tag::segments ) != 3 )  // a filament
			{	assert ( tri .boundary() .number_of ( tag::segments ) == 2 );
				// we could add the two (degenerate) faces of the filament to 'already_analysed'
				// but it's not worth the effort - just ignore filaments
				continue;                                                      }
			// here 'tri' is a real triangle, not a filament
			if ( already_analysed .find ( tri ) == already_analysed .end() )
			{	ABC = tri;  break;  }                                                            }
 
		if ( not ABC .exists() )  continue;  // to next vertex in ver_fill_burried

		// below, it would be nice to use an ordered iterator
		// which only sweeps over faces connected to each other
		// but these iterators do not work for STSI meshes - a pitty
		// 23.08  Cristian thinks they work now, we should try

		assert ( already_analysed .find ( ABC ) == already_analysed .end() );
		already_analysed .insert ( ABC );
		Mesh ABC_bdry = ABC .boundary();
		Cell AB = ABC_bdry .cell_in_front_of ( A, tag::surely_exists );
		Cell B = AB .tip();
		Cell BC = ABC_bdry .cell_in_front_of ( B, tag::surely_exists );
		Cell C = BC .tip();
		Cell CA = ABC_bdry .cell_in_front_of ( C, tag::surely_exists );
		assert ( CA .tip() == A );

		assert ( ABC .belongs_to ( interf, tag::same_dim, tag::orientation_compatible_with_mesh ) );
		Cell AEB = interf .cell_in_front_of ( AB, tag::seen_from, ABC, tag::surely_exists );
		assert ( AEB .boundary() .number_of ( tag::segments ) == 3 );
		assert ( already_analysed .find ( AEB ) == already_analysed .end() );
		already_analysed .insert ( AEB );
		Cell AE = AEB .boundary() .cell_in_front_of ( A, tag::surely_exists );
		Cell ADE = interf .cell_in_front_of ( AE, tag::seen_from, AEB, tag::surely_exists );
		assert ( ADE .boundary() .number_of ( tag::segments ) == 3 );
		std::pair < std::set < Cell > ::iterator, bool > inserted =  already_analysed .insert ( ADE );
		if ( not inserted .second )
		{	assert ( ADE == ABC );  // this must be a membrane
			goto label_search_for_ABC;  }
		Cell ACD = interf .cell_in_front_of ( CA, tag::seen_from, ABC, tag::surely_exists );
		assert ( ACD .boundary() .number_of ( tag::segments ) == 3 );
		inserted =  already_analysed .insert ( ACD );
		if ( not inserted .second )
		{	assert ( ACD == ADE );  // this must be a "reversed" cavity
			goto label_search_for_ABC;  }
		Cell AD = ADE .boundary() .cell_in_front_of ( A, tag::surely_exists );
		Cell AC = CA .reverse ( tag::surely_exists );
		Cell DA = ACD .boundary() .cell_behind ( A, tag::surely_exists );

		Cell next_tri = interf .cell_in_front_of ( DA, tag::seen_from, ACD, tag::surely_exists );
		if ( next_tri != ADE )
		// no four adjacent triangles, search for other faces ABC
		{	while ( true )     // but before that, let us insert more faces in 'already_analysed'
			{	assert ( next_tri .belongs_to
				         ( interf, tag::same_dim, tag::orientation_compatible_with_mesh ) );
				assert ( next_tri .boundary() .number_of ( tag::segments ) == 3 );
				assert ( already_analysed .find ( next_tri ) == already_analysed .end() );
				already_analysed .insert ( next_tri );
				Cell next_seg = next_tri .boundary() .cell_behind ( A, tag::surely_exists );
				next_tri = interf .cell_in_front_of
					( next_seg, tag::seen_from, next_tri, tag::surely_exists );
				if ( next_tri == ADE )  break;                                                }
			goto label_search_for_ABC;                                                         }

		assert ( AD .reverse ( tag::surely_exists ) == DA );
		Cell D = AD .tip();
		Cell E = AE .tip();
		// order of vertices around A is B C D E

		double var;  // not used here
		if ( fit_for_one_tetra < environ > ( interf, AD, ADE, ACD, var ) and
		     fit_for_one_tetra < environ > ( interf, AB, ABC, AEB, var ) and
		     not danger_of_flat_tetra < environ >
		         ( interf, AD, ADE, ACD,
		           tag::mobile_vertex, Cell ( tag::non_existent ), tag::one_direction ) and
		     not danger_of_flat_tetra < environ >
		         ( interf, AB, ABC, AEB,
		           tag::mobile_vertex, Cell ( tag::non_existent ), tag::one_direction )     )
		{	fill_two_tetra_core < environ >
				( msh, interf, A, C, D, E, B, AC, AD, AE, AB, ACD, ADE, AEB, ABC );
			return true;                                                           }
		if ( fit_for_one_tetra < environ > ( interf, AC, ACD, ABC, var ) and
		     fit_for_one_tetra < environ > ( interf, AE, AEB, ADE, var ) and
		     not danger_of_flat_tetra < environ >
		         ( interf, AC, ACD, ABC,
		           tag::mobile_vertex, Cell ( tag::non_existent ), tag::one_direction ) and
		     not danger_of_flat_tetra < environ >
		         ( interf, AE, AEB, ADE,
		           tag::mobile_vertex, Cell ( tag::non_existent ), tag::one_direction )     )
		{	fill_two_tetra_core < environ >
				( msh, interf, A, D, E, B, C, AD, AE, AB, AC, ADE, AEB, ABC, ACD );
			return true;                                                           }
		
		goto label_search_for_ABC;
		
	}  // end of for over ver_fill_burried
	
	return false;  // did nothing

}  // end of  fill_two_tetra_ground

//------------------------------------------------------------------------------------------------------//


template < class environ >    // hidden in anonymous namespace
inline bool fill_cavity_ground ( Mesh & msh, Mesh & interf )
	
// try to fill several tetrahedra around a given vertex
// create a new vertex, several segments and several triangles
// return false if did nothing

// 'msh' is a Mesh::Fuzzy made of tetrahedra (being built now)
// 'interf' is a Mesh::STSI made of triangles
	
// 'environ' describes the type of manifold we are working in
// and how we treat information about metric and exterior product
// environ::manif_type could be ManifoldNoWinding or ManifoldQuotient (defined in frontal.cpp)
// environ::metric_type may be one of ISRContainer::{Inactive,Active} (defined in frontal.cpp)
// environ::ext_prod_type may be ExtProd3d, 3dRev (defined in frontal.cpp)
	
{	assert ( tri_fill_isolated    .empty() );
	assert ( dangerous_cavities   .empty() );
	assert ( seg_fill_membrane_3s .empty() );
	assert ( ver_fill_burried     .empty() );
	assert ( seg_fill_crest       .empty() );

	// below we search for four or more adjacent triangles forming a cavity
	for ( std::set < Cell > ::iterator it  = ver_fill_cavity .begin();
	      it != ver_fill_cavity .end(); it = ver_fill_cavity .erase ( it ) )
		
	{	Cell A = * it;
		if ( not A .belongs_to ( interf ) )  continue;    // to next vertex in ver_fill_cavity
		std::set < Cell > already_analysed;

		label_search_for_ABC :
		// we look for four adjacent triangles around A
		// below we invoke an iterator around A without order required
		// it sweeps over all triangles, even if some segment is singular
		Cell ABC ( tag::non_existent );
		bool has_filament = false;
		{ // just a block of code for hiding 'itt'
		Mesh::Iterator itt = interf .iterator
			( tag::over_cells_of_max_dim, tag::orientation_compatible_with_mesh, tag::around, A );
		for ( itt .reset(); itt .in_range(); itt ++ )
		{	Cell tri = * itt;
			// if 'tri' is a filament, we don't want to fill around A
			if ( tri .boundary() .number_of ( tag::segments ) != 3 )
			{	assert ( tri .boundary() .number_of ( tag::segments ) == 2 );
			  has_filament = true;                                           }  //  break !!
			if ( already_analysed .find ( tri ) == already_analysed .end() )
			{	ABC = tri;  break;  }                                             }
		} // just a block of code
 
		if ( has_filament )       continue;  // to next vertex in ver_fill_cavity
		if ( not ABC .exists() )  continue;  // to next vertex in ver_fill_cavity

		assert ( already_analysed .find ( ABC ) == already_analysed .end() );
		already_analysed .insert ( ABC );
		Mesh ABC_bdry = ABC .boundary();
		Cell AB = ABC_bdry .cell_in_front_of ( A, tag::surely_exists );
		Cell B = AB .tip();
		Cell BC = ABC_bdry .cell_in_front_of ( B, tag::surely_exists );
		Cell C = BC .tip();
		Cell CA = ABC_bdry .cell_in_front_of ( C, tag::surely_exists );
		assert ( CA .tip() == A );
		assert ( ABC .belongs_to ( interf, tag::same_dim, tag::orientation_compatible_with_mesh ) );

		#if FRONTAL_VERBOSITY > 2
		std::cout << "frontal 3d line 3721 [ ";
		for ( size_t i = 0; i < frontal_nb_of_coords; i++ )
		{	const Function & x = Manifold::working .coordinates() [i];
			std::cout << x(A) << " ";                                   }
		std::cout << "]" << std::endl;
		#endif  // FRONTAL_VERBOSITY > 2
		// check all edges around vertices around A
		{ // just a block of code for hiding variable names
		Cell tri = ABC;
		Cell side_forward = CA;
		bool not_concave_enough = false;
		bool reversed_cavity = false;
		size_t counter = 0;
		while ( true )
		{	assert ( side_forward .tip() == A );
			Cell next_tri = interf .cell_in_front_of
				( side_forward, tag::seen_from, tri, tag::surely_exists );
			assert ( next_tri .belongs_to ( interf, tag::same_dim,
			                                tag::orientation_compatible_with_mesh ) );
			assert ( next_tri .boundary() .number_of ( tag::segments ) == 3 );
			if ( not ( reversed_cavity and not_concave_enough ) )
			{	const double angle_at_AB = angle_3d < environ > ( interf, tri, side_forward );
				#if FRONTAL_VERBOSITY > 2
				std::cout << angle_at_AB << " ";
				#endif
				if ( angle_at_AB > 0.   )  reversed_cavity = true;
				if ( angle_at_AB > -0.5 )  not_concave_enough = true;                         }
			counter ++;
			if ( next_tri == ABC )  break;
			assert ( already_analysed .find ( next_tri ) == already_analysed .end() );
			already_analysed .insert ( next_tri );
			tri = next_tri;
			side_forward = tri .boundary() .cell_behind (A);                                   }
		#if FRONTAL_VERBOSITY > 2
		std::cout << std::endl;
		#endif

		if ( counter < 3 )  // this must be a membrane
		{	assert ( counter == 2 );  goto label_search_for_ABC;  }
		if ( reversed_cavity )  goto label_search_for_ABC;
		if ( not_concave_enough )  goto label_search_for_ABC;
		} // just a block of code

		// before creating a new vertex P, compute only its coordinates,
		// measure the distances to outer vertices
		// if any of them is larger than sqrt 1.8, do nothing      // frontal_seg_size 3D

		std::vector < double > e_side ( frontal_nb_of_coords, 0. ), e_norm ( frontal_nb_of_coords, 0. );
		size_t count_tri = 0;
		// loop below is equivalent to using an iterator around A with tag::backwards
		// but such an iterator does not work correctly for a STSI mesh
		Cell AXY = ABC;
		while ( true )
		{	Cell AX = AXY .boundary() .cell_in_front_of ( A, tag::surely_exists );
			std::vector < double > e_AX = environ::manif_type::get_vector ( AX );
			std::vector < double > n_AXY =
				environ::ext_prod_type::get_normal_vector ( tag::domain_3d, tag::at_tri, AXY );
			// although the interface is looking "outwards",
			// towards the zone where we do not intend to build tetrahedra,
			// 'get_normal_vector' returns a normal vector pointing "inwards",
			// that is, towards the zone we intend to mesh
			for ( size_t i = 0; i < frontal_nb_of_coords; i++ )
			{	e_side [i] += e_AX [i];
				e_norm [i] += n_AXY [i];  }
			count_tri ++;
			AXY = interf .cell_in_front_of ( AX, tag::seen_from, AXY, tag::surely_exists );
			if ( AXY == ABC )  break;                                                          }
		
		std::vector < double > e_AP ( frontal_nb_of_coords );
		for ( size_t i = 0; i < frontal_nb_of_coords; i++ )
			e_AP [i] = ( 0.66 * e_side [i] + tag::Util::sqrt_two_thirds * e_norm [i] ) / count_tri * 0.7;
		// last multiplication means we choose to project less, aiming at a more stable process
		// 'relocate_3d' will improve the location anyway

		// loop below is equivalent to using an iterator around A with tag::backwards
		// but such an iterator does not work correctly for a STSI mesh
		AXY = ABC;
		while ( true )
		{	Cell AX = AXY .boundary() .cell_in_front_of ( A, tag::surely_exists );
			Cell X = AX .tip();
			std::vector < double > e_AX = environ::manif_type::get_vector ( AX );
			std::vector < double > e ( frontal_nb_of_coords );
			for ( size_t i = 0; i < frontal_nb_of_coords; i++ )
				e[i] = e_AP [i] - e_AX [i];            // frontal_seg_size 3D  // was > 1.8
			if ( Manifold::working .sq_dist ( A, e ) > frontal_seg_size_2 )
				// these triangles form a cavity which is not concave enough
				// not a good idea to create a vertex P at this location, do nothing
				goto label_search_for_ABC;
			AXY = interf .cell_in_front_of ( AX, tag::seen_from, AXY, tag::surely_exists );
			if ( AXY == ABC )  break;                                                          }

		#if FRONTAL_VERBOSITY > 1
		std::cout << "frontal-3d.cpp line 3845, insert " << count_tri << " tetra, ground"
		          << std::endl << std::flush;
		#endif

		// we could have built 'tri_on_floor' and 'triangles_on_floor' earlier
		// we choose to build them here because, due to the jump 'goto label_search_for_ABC',
		// the execution rarely gets to this part of the code, so we save execution time
		std::vector < Cell > tri_on_floor;  // needed below to build triangles and tetrahedra around AP
		tri_on_floor .reserve ( 10 );
		std::set < Cell > triangles_on_floor;   // do we need both ?
		// same as 'tri_on_floor', will be transfered to 'walls'
		typename environ::manif_type::winding_cell AA = environ::manif_type::add_winding ( A, 0 );
		typename environ::manif_type::set_of_winding_cells vertices_from_walls { AA };
		// loop below is equivalent to using an iterator around A with tag::require_order
		// but such an iterator does not work correctly for a STSI mesh
		AXY = ABC;
		while ( true )
		{	triangles_on_floor .insert ( AXY );
			tri_on_floor .push_back ( AXY );
			Cell AX = AXY .boundary() .cell_in_front_of ( A, tag::surely_exists );
			Cell YA = AXY .boundary() .cell_behind ( A, tag::surely_exists );
			vertices_from_walls .insert
				( environ::manif_type::add_winding_1 ( AX .tip(), AX ) );
			AXY = interf .cell_in_front_of ( YA, tag::seen_from, AXY, tag::surely_exists );
			if ( AXY == ABC )  break;                                                        }
		assert ( tri_on_floor       .size() == count_tri );
		assert ( triangles_on_floor .size() == count_tri );

		// 'walls' is a set of triangles if manif non-winding
		// it is a map (triangle, vertex with winding) if manif winding
		typename environ::manif_type::type_of_walls walls;
		for ( std::set < Cell > ::const_iterator itt = triangles_on_floor .begin();
					itt != triangles_on_floor .end(); itt ++                           )
			environ::manif_type::insert_wall ( walls, * itt, AA );

		Cell P ( tag::vertex );
		for ( size_t i = 0; i < frontal_nb_of_coords; i++ )
		{	const Function & x = Manifold::working .coordinates() [i];
			x(P) = x(A) + 0.7 * e_AP [i];                               }
		  // we consider A and P have the same winding
			// we multiply, again, by a subunitary value, aiming at a more stable process
			// 'relocate_3d' will improve the location anyway
		// Manifold::working .project (P)  not needed in 3D
		// 'compute_data' uses information from A to compute eigendirection and isr at P
		environ::metric_type::compute_data ( tag::at_point, P, tag::use_information_from, A );
		// P will be added to the cloud later, after 'relocate_3d'

		Cell AP ( tag::segment, A .reverse ( tag::surely_exists ), P );  // zero winding
		update_filaments_near ( AP, interf );

		// build segments, triangles and tetrahedra around P, add them to 'interf' and to 'msh'
		std::vector < Cell > ::const_iterator iter = tri_on_floor .begin();
		assert ( iter != tri_on_floor .end() );
		AXY = * iter;
		Cell AX = AXY .boundary() .cell_in_front_of ( A, tag::surely_exists );
		Cell X = AX .tip();
		Cell XP ( tag::segment, X .reverse ( tag::surely_exists ), P );
		environ::manif_type::set_winding_neg_1 ( XP, AX );
		update_filaments_near ( XP, interf );
		Cell AXP ( tag::triangle, AX, XP, AP .reverse ( tag::does_not_exist, tag::build_now ) );
		Cell first_X = X;
		Cell first_XP = XP;
		Cell first_AXP = AXP;
		while ( true )
		{ Cell XY = AXY .boundary() .cell_in_front_of ( X, tag::surely_exists );
			Cell YA = AXY .boundary() .cell_behind ( A, tag::surely_exists );
			Cell Y = XY .tip();
			assert ( Y == YA .base() .reverse ( tag::surely_exists ) );
			Cell YP ( tag::non_existent );
			Cell AYP ( tag::non_existent );
			if ( Y == first_X )  // this is the last tetrahedron to create, use existent face
			{	YP = first_XP;
				AYP = first_AXP;  }
			else  // not the last tetrahedron, build new face
			{	YP = Cell ( tag::segment, Y .reverse ( tag::surely_exists ), P );
				environ::manif_type::set_winding_1 ( YP, YA );
				update_filaments_near ( YP, interf );
				AYP = Cell ( tag::triangle, YA .reverse ( tag::surely_exists ),
				             YP, AP .reverse ( tag::surely_exists )            );  }
			Cell AXY_alt = remove_face ( AXY, interf );
			// AXY .remove_from ( interf )
			Cell PYX ( tag::triangle, YP .reverse ( tag::build_if_not_exists ),
			           XY .reverse ( tag::surely_exists ), XP                  );
			// YP is newly built, except for the last tetrahedron when it already has a reverse
			PYX .reverse ( tag::does_not_exist, tag::build_now ) .add_to ( interf );
			Cell tetra ( tag::tetrahedron, AXY_alt,
			             AXP .reverse ( tag::does_not_exist, tag::build_now ), PYX, AYP );
			tetra .add_to ( msh );
			ver_fill_burried .insert (X);
			seg_fill_crest .insert ( { PYX .reverse ( tag::surely_exists ), XY } );
			seg_fill_membrane_2s .insert
				( { PYX .reverse ( tag::surely_exists ), XP .reverse ( tag::build_if_not_exists ) } );
			// XP has already a reverse, except for the first tetrahedron when the reverse must be built
			ver_fill_cavity .insert (X);
			seg_fill_two_crest .push_back ( { PYX .reverse ( tag::surely_exists ), XY } );
			iter ++;
			if ( iter == tri_on_floor .end() )
			{	assert ( Y == first_X );
				break;                    }
			X = Y;
			XP = YP;
			AXP = AYP;
			AXY = * iter;
		}  // end of  while true  (rotation around AP)

		environ::manif_type::node_container .cond_remove ( A, interf );
		environ::metric_type::cond_remove ( A, interf );
	
		// improve the location of P, taking into account nearby vertices and triangles
		// also build filaments when appropriate
		// may build new segments, triangles and tetrahedra around P, add them to 'interf' and to 'msh'
		typename environ::manif_type::set_of_winding_cells other_vertices;  // stays empty
		union_of < typename environ::manif_type::set_of_winding_cells > vertices
			{	vertices_from_walls, other_vertices };
		relocate_3d < environ > ( msh, interf, P, walls, vertices );
	
		#if FRONTAL_VERBOSITY > 1
		std::cout << "frontal-3d.cpp line 3958, P [ ";
		for ( size_t i = 0; i < frontal_nb_of_coords; i++ )
		{	const Function & x = Manifold::working .coordinates() [i];
			std::cout << x(P) << " ";                                   }
		std::cout << "]" << std::endl;
		#endif  // FRONTAL_VERBOSITY > 1

		// we insert P in the cloud only now because in relocate_3d we do not want it there
		// another reason is that P has been moved during 'relocate_3d'
		// now its location in space is frozen
		// so it will be inserted at the right position in the metric tree
		// 'relocate_3d' may have eliminated P from 'interf' so we must ask :
		if ( P .belongs_to ( interf, tag::cell_has_low_dim ) )
			environ::manif_type::node_container .insert (P);

		#if FRONTAL_VERBOSITY > 1
		if ( frontal_counter < frontal_counter_max )  frontal_counter ++;
		// 'relocate_3d' may have already incremented 'frontal_counter'
		#endif

		return true;

	}  // end of  for  over  ver_fill_cavity
	
	return false;  // did nothing

}  // end of  fill_cavity_ground

//------------------------------------------------------------------------------------------------------//


template < class environ >   // hidden in anonymous namespace
inline bool fill_tri_membrane_2_sides ( Mesh & msh, Mesh & interf )
	
// try to fill a new triangle with two sides already in 'msh'
// a pair of triangles, actually, like a double membrane
// ideally, these two faces should be each other's reverse
// however, even STSI meshes are unable to handle such a configuration
// this is why we create two independent faces and keep the pair in 'dummy_faces'
// later, the first one will be replaced by the second one's reverse (this is the job of 'remove_face')

// the set 'seg_fill_membrane_2s' contains pairs (triangle,edge)
// the edge points towards the common vertex
// the newly built segment does not contain this common vertex

// return false if did nothing

// 'msh' is a Mesh::Fuzzy made of tetrahedra (being built now)
// 'interf' is a Mesh::STSI made of triangles
	
// 'environ' describes the type of manifold we are working in
// and how we treat information about metric and exterior product
// environ::manif_type could be ManifoldNoWinding or ManifoldQuotient (defined in frontal.cpp)
// environ::metric_type may be one of ISRContainer::{Inactive,Active} (defined in frontal.cpp)
// environ::ext_prod_type may be ExtProd3d, 3dRev (defined in frontal.cpp)
	
{	assert ( tri_fill_isolated    .empty() );
	assert ( dangerous_cavities   .empty() );
	assert ( seg_fill_membrane_3s .empty() );
	assert ( ver_fill_burried     .empty() );
	assert ( seg_fill_crest       .empty() );
	assert ( ver_fill_cavity      .empty() );

	start :
	
	// preliminary search for the shortest segment to be created
	std::set < std::pair < Cell, Cell > > ::iterator kept_it = seg_fill_membrane_2s .end();
	Cell kept_seg ( tag::non_existent );
	double shortest = 10.;
	for ( std::set < std::pair < Cell, Cell > > ::iterator
	      it = seg_fill_membrane_2s .begin(); it != seg_fill_membrane_2s .end();  )

	{	Cell ABC = it->first;
		if ( not ABC .belongs_to ( interf, tag::same_dim ) )
		{	it = seg_fill_membrane_2s .erase ( it );
			continue;                                 }  // to next segment in seg_fill_membrane_2s
		Cell AB = it->second;
		assert ( AB .belongs_to ( ABC .boundary(), tag::orientation_compatible_with_mesh ) );

		// given AB, we are looking for another segment BP (both must exist already in interf)
		// such that dist(A,P) is small; segment AP does not exist yet
		// if found such a P, we create segment AP then triangles ABP and APB

		Cell B = AB .tip();
		Cell A = AB .base() .reverse ( tag::surely_exists );
		Cell BAD = interf .cell_in_front_of ( AB, tag::seen_from, ABC, tag::surely_exists );
		Cell BC = ABC .boundary() .cell_in_front_of ( B, tag::surely_exists );
		Cell C = BC .tip();
		Cell AD = BAD .boundary() .cell_in_front_of ( A, tag::surely_exists );
		Cell D = AD .tip();
				
		std::set < Cell > forbidden_vertices { A };
		bool AB_is_not_filament = C != A;
		// 'forbidden_vertices' will have three elements, or just one if AB is filament
		if ( AB_is_not_filament )
		{	assert ( ABC .boundary() .number_of ( tag::segments ) == 3 );
			forbidden_vertices .insert (C);
			assert ( D != B );
			assert ( BAD .boundary() .number_of ( tag::segments ) == 3 );
			forbidden_vertices .insert (D);                                }
		else
		{	assert ( ABC .boundary() .number_of ( tag::segments ) == 2 );
			assert ( BAD .boundary() .number_of ( tag::segments ) == 2 );  }

		// analyse all segments in 'interf' having B as base
		// 'interf' is a Mesh::STSI, we use an iterator with no order required
		// which produces segments possibly belonging to different components of interf,
		// possibly filaments
		bool no_candidate_found = true;
		const std::vector < double > e_AB = environ::manif_type::get_vector ( AB );
		const std::vector < double > e_BC = environ::manif_type::get_vector ( BC );
		const std::vector < double > e_AD = environ::manif_type::get_vector ( AD );

		Mesh::Iterator it_B = interf .iterator ( tag::over_segments, tag::pointing_away_from, B );
		for ( it_B .reset(); it_B .in_range(); it_B++ )

		{	Cell BP = * it_B;
			assert ( BP .base() .reverse() == B );
			Cell P = BP .tip();
			if ( forbidden_vertices .find (P) != forbidden_vertices .end() )  continue;

			const std::vector < double > e_BP = environ::manif_type::get_vector ( BP );
			std::vector < double > e_AP ( frontal_nb_of_coords );
			for ( size_t i = 0; i < frontal_nb_of_coords; i++ )
				e_AP [i] = e_AB [i] + e_BP [i];
			const double AP2 = Manifold::working .sq_dist ( A, P, e_AP );
			if ( AP2 > 1.8 )  continue;  // frontal_seg_size 3D  // was AP2 > 1.8
			// AP < sqrt 1.8, we may want to fill the triangle ABP (double membrane)
			
			// do not fill triangle if angle is wide :
			if ( Manifold::working .inner_prod ( B, e_AB, e_BP ) > - 0.1 )  continue;
			// below we repeat the above test with a tighter threshold but not for filaments ...

			// in most cases this is a complete waste of time, but in rare configurations
			// the algorithm builds a membrane within a flat portion of the interface
			// we must prevent that
			if ( AB_is_not_filament )
			{	// do not fill triangle if P is near C and ABCP are nearly coplanar
				std::vector < double > e_CP ( frontal_nb_of_coords );
				for ( size_t i = 0; i < frontal_nb_of_coords; i++ )
					e_CP [i] = e_BP [i] - e_BC [i];
				const double CP2 = Manifold::working .sq_dist ( C, P, e_CP );
				if ( CP2 < frontal_seg_size_2 )  // frontal_seg_size 3D  // was  CP2 < 1.8
				{	const std::vector < double > n_ABC =
						environ::ext_prod_type::get_normal_vector ( tag::domain_3d, tag::at_tri, ABC );
				  // although the interface is looking "outwards",
				  // towards the zone where we do not intend to build tetrahedra,
				  // 'get_normal_vector' returns a normal vector pointing "inwards",
				  // that is, towards the zone we intend to mesh
					if ( Manifold::working .inner_prod ( P, n_ABC, e_CP ) < 0.3 )  continue;           }
				// do not fill triangle if P is near D and BADP are nearly coplanar
				std::vector < double > e_DP ( frontal_nb_of_coords );
				for ( size_t i = 0; i < frontal_nb_of_coords; i++ )
					e_DP [i] = e_AP [i] - e_AD [i];
				const double DP2 = Manifold::working .sq_dist ( D, P, e_DP );
				if ( DP2 < frontal_seg_size_2 )  // frontal_seg_size 3D  // was DP2 < 1.8
				{	const std::vector < double > n_BAD =
						environ::ext_prod_type::get_normal_vector ( tag::domain_3d, tag::at_tri, BAD );
				  // although the interface is looking "outwards",
				  // towards the zone where we do not intend to build tetrahedra,
				  // 'get_normal_vector' returns a normal vector pointing "inwards",
				  // that is, towards the zone we intend to mesh
					if ( Manifold::working .inner_prod ( P, n_BAD, e_DP ) < 0.3 )  continue;           }  }

			no_candidate_found = false;
			if ( AP2 < shortest )
			{	kept_it = it;  kept_seg = BP;  shortest = AP2;  }                                           }

		if ( no_candidate_found )
		{	it = seg_fill_membrane_2s .erase ( it );  continue;  }
			// to next segment in seg_fill_membrane_2s

		it++;
	}	// end of for (preliminary search)

	if ( not kept_seg .exists() )  return false;

	assert ( kept_it != seg_fill_membrane_2s .end() );
	Cell ABC = kept_it->first;
	Cell AB = kept_it->second;
	assert ( AB .belongs_to ( ABC .boundary(), tag::orientation_compatible_with_mesh ) );
	seg_fill_membrane_2s .erase ( kept_it );
	Cell B = AB .tip();
	Cell A = AB .base() .reverse ( tag::surely_exists );
	assert ( kept_seg .belongs_to ( interf, tag::cell_has_low_dim ) );
	Cell P = kept_seg .tip();  //   kept_seg == BP
	assert ( B == kept_seg .base() .reverse() );

	// note : a filament is regular (it has two neighbour faces, albeit degenerated)
	//        a singular face is e.g. where a previously existing membrane touches other triangle
	bool AB_is_filament = ABC .boundary() .number_of ( tag::cells_of_max_dim ) == 2;
	bool BP_is_filament = false;
	if ( kept_seg .is_regular ( tag::within_STSI_mesh, interf ) )
	{	Cell tri = interf .cell_in_front_of ( kept_seg, tag::surely_exists );
		BP_is_filament = tri .boundary() .number_of ( tag::cells_of_max_dim ) == 2;  }

	// if there are two triangles, one adjacent to AB, the other adjacent to BP
	// and these two triangles have a common edge, this means that a tetrahedron-crest
	// could be built there
	// but it hasn't been built so it must have been rejected by some criterion
	// so don't build the membrane, goto start
	if ( not ( AB_is_filament or BP_is_filament ) )
	{	Mesh::Iterator it_AB = interf .iterator
			( tag::over_cells_of_max_dim, tag::orientation_compatible_with_mesh,
			  tag::around, AB                                                   );
		Mesh::Iterator it_BP = interf .iterator  //   kept_seg == BP
			( tag::over_cells_of_max_dim, tag::orientation_compatible_with_mesh,
			  tag::around, kept_seg                                             );
		for ( it_AB .reset(); it_AB .in_range(); it_AB ++ )
		for ( it_BP .reset(); it_BP .in_range(); it_BP ++ )
		{	Cell ABQ = *it_AB;
			Cell BPQ = *it_BP;
			if ( AB .belongs_to ( ABQ .boundary (), tag::same_dim,
			                      tag::orientation_compatible_with_mesh ) )
			{	if ( not kept_seg .belongs_to ( BPQ .boundary (), tag::same_dim,   //   kept_seg == BP
				                          tag::orientation_compatible_with_mesh ) )  continue;
				Cell QA = ABQ .boundary() .cell_behind ( A, tag::surely_exists );
				Cell QB = BPQ .boundary() .cell_behind ( B, tag::surely_exists );
				if ( QA .base() == QB .base() )  goto start;
				continue;                                                                        }
			Cell BA = AB .reverse ( tag::surely_exists );
			Cell PB = kept_seg .reverse ( tag::surely_exists );   //   kept_seg == BP
			assert ( BA .belongs_to ( ABQ .boundary (), tag::same_dim,
			                          tag::orientation_compatible_with_mesh ) );
			if ( not PB .belongs_to ( BPQ .boundary (), tag::same_dim,
			                          tag::orientation_compatible_with_mesh ) )  continue;
			Cell QB = ABQ .boundary() .cell_behind ( B, tag::surely_exists );
			Cell QP = BPQ .boundary() .cell_behind ( P, tag::surely_exists );
			if ( QB .base() == QP .base() )  goto start;                                          }  }

	std::vector < double > e_AB;
	std::vector < double > e_BP;
		
	// only build a membrane if
	//  1. one side is filament
	//  2. no filaments, but the angle is sharp
	if ( not ( AB_is_filament or BP_is_filament ) )  // kept_seg == BP
	{	e_AB = environ::manif_type::get_vector ( AB );
		e_BP = environ::manif_type::get_vector ( kept_seg );
		normalize_vector_approx ( B, e_AB );
		normalize_vector_approx ( B, e_BP );   // cos 37 deg = 0.79863
		#if FRONTAL_VERBOSITY > 2
		std::cout << "line 3456, " << Manifold::working .inner_prod ( B, e_AB, e_BP ) << " [ ";
		for ( size_t i = 0; i < frontal_nb_of_coords; i++ )
		{	const Function & x = Manifold::working .coordinates() [i];
			std::cout << x(A) << " ";                                   }
		std::cout << "] [ ";
		for ( size_t i = 0; i < frontal_nb_of_coords; i++ )
		{	const Function & x = Manifold::working .coordinates() [i];
			std::cout << x(B) << " ";                                   }
		std::cout << "] [ ";
		for ( size_t i = 0; i < frontal_nb_of_coords; i++ )
		{	const Function & x = Manifold::working .coordinates() [i];
			std::cout << x(P) << " ";                                   }
		std::cout << "]" << std::endl;
		#endif  // FRONTAL_VERBOSITY > 2
		if ( Manifold::working .inner_prod ( B, e_AB, e_BP ) > - 0.5 )  goto start;                  }
		
	// if ( AB_is_filament and BP_is_filament )  // kept_seg == BP
	// {	break_longest_filament < environ > ( AB, kept_seg, A, interf );
	// 	goto start;                                                      }  // to next candidate
		
	// sometimes the segments AB and BP (sides of the membrane to be built) point backwards
	// to detect this situation, Cristian has tried to measure the angles
	// between faces meeting at AB and at BP (function 'is_crest', later renamed 'angle')
	// bool AB_is_crest = false, BP_is_crest = false;
	// if ( not AB_is_filament )  AB_is_crest = is_crest < environ > ( interf, ABC, AB );
	// if ( ( not BP_is_filament ) and kept_seg .is_regular ( tag::within_STSI_mesh, interf ) )
	// 	BP_is_crest = is_crest < environ > ( interf, interf .cell_behind  // kept_seg == BP
	// 	                  ( kept_seg, tag::surely_exists ), kept_seg )
	// // if 'BP', aka 'kept_seg', is singular within the STSI mesh 'interf'
	// // we still have a chance, later (in the loop defining 'path_exists'),
	// // to ask whether BP is crest or not
	// later :
	// if ( AB_is_crest and BP_is_crest )  goto start;
	// this worked well in most situations but not in all

	// Cristian has tried using the average normal to triangles around B, compare it with e_BA + e_BP,
	// however, the average normal becomes ill-defined in complicated configurations
	// like the one depicted in
	// https://codeberg.org/cristian.barbarosie/manifem-manual  files 'membrane-02.{msh,png}'
	// so we only use four triangles, those having AB or BP as side
	// if triangles around B do not form a closed path, this criterion does not apply

	// it is obvious that no such path exists if either AB or BP is filament
	// but there are also cases like this which do not involve any filament
	// see https://codeberg.org/cristian.barbarosie/manifem-manual
	// files 'membrane-01.{png,msh}', 'mere-touch-01.msh'

	// loop below detects if such a path exists
	// we take this opportunity 
	// to fill in 'rels', neighbourhood relations used when adding the first face to 'interf'
	std::map < Cell, Cell > rels;
	if ( not AB_is_filament )  tag::Util::insert_new_elem_in_map ( rels, { AB, ABC } );  // rels [ AB ] = ABC
	
	if ( AB_is_filament or BP_is_filament )  goto label_build_membrane;  // no more verifications

	{ // just a block of code for hiding variable names
	bool path_exists = false;  // path from AB to BP, around B
	Cell tri = ABC;
	// loop below is equivalent to using an interator around B without order required
	while ( true )
	{	Cell seg = tri .boundary() .cell_in_front_of ( B, tag::surely_exists );
		if ( seg == AB .reverse ( tag::surely_exists ) )  break;
		if ( seg == kept_seg )  // kept_seg == BP
		{	tag::Util::insert_new_elem_in_map ( rels, { kept_seg, tri } );  // rels [ BP ] = tri
			path_exists = true;                                      }
		tri = interf .cell_in_front_of ( seg, tag::seen_from, tri, tag::surely_exists );  }

	// AB or BP may belong to a previous membrane, as in
	// https://codeberg.org/cristian.barbarosie/manifem-manual  files 'membrane-03.{msh,png}'
	// in that case, we assume there is no danger of AB and BP "bending backwards"
	
	// even if no path exists, there is still danger of AB and BP "bending backwards"
	// see https://codeberg.org/cristian.barbarosie/manifem-manual
	// files 'membrane-{04,05}.msh' 'membrane-04.png'

	bool lets_check_bent = path_exists or    // kept_seg == BP
	  ( kept_seg .is_regular ( tag::within_STSI_mesh, interf ) and not BP_is_filament );

	if ( lets_check_bent )

	{	Cell BAD = interf .cell_in_front_of ( AB, tag::seen_from, ABC, tag::surely_exists );
		std::map < Cell, Cell > ::iterator it = dummy_faces .find ( ABC );
		if ( it != dummy_faces .end() )	// we are touching a previous membrane	
		if ( BAD == it->second )  goto label_build_membrane;  // no more verifications

		Cell BPQ ( tag::non_existent ), PBR ( tag::non_existent );   // kept_seg == BP

		if ( path_exists )
		{	std::map < Cell, Cell > ::const_iterator itt = rels .find ( kept_seg );
			assert ( itt != rels .end() );
			BPQ = itt->second;
			PBR = interf .cell_in_front_of ( kept_seg, tag::seen_from, BPQ, tag::surely_exists );  }

		else  //  no path exists
		{	assert ( not BP_is_filament );
			BPQ = interf .cell_behind      ( kept_seg, tag::surely_exists );  // BP is regular
			PBR = interf .cell_in_front_of ( kept_seg, tag::surely_exists );  }
			
		it = dummy_faces .find ( BPQ );
		if ( it != dummy_faces .end() )	// we are touching a previous membrane
		if ( PBR == it->second )  goto label_build_membrane;  // no more verifications
			
		// we compute the average of normals to triangles neighbour to AB
		std::vector < double > average_normal =   // not actually an average, just a sum
			environ::ext_prod_type::get_normal_vector ( tag::domain_3d, tag::at_tri, ABC );
		std::vector < double > n_tri =
			environ::ext_prod_type::get_normal_vector ( tag::domain_3d, tag::at_tri, BAD );
		// although the interface is looking "outwards",
		// towards the zone where we do not intend to build tetrahedra,
		// 'get_normal_vector' returns a normal vector pointing "inwards",
		// that is, towards the zone we intend to mesh
		for ( size_t i = 0; i < frontal_nb_of_coords; i++ )
			average_normal [i] += n_tri [i];
		// alternatively, think in terms of mixed product
		bool bent_back = Manifold::working .inner_prod ( B, e_BP, average_normal ) < 0.;
		// e_BP has been computed if neither AB or BP are filament, that's OK
		
		#ifndef NDEBUG  // DEBUG mode
		// we compute the average of normals to triangles neighbour to BP
		average_normal =   // not actually an average, just a sum
			environ::ext_prod_type::get_normal_vector ( tag::domain_3d, tag::at_tri, BPQ );
		n_tri = environ::ext_prod_type::get_normal_vector ( tag::domain_3d, tag::at_tri, PBR );
		// although the interface is looking "outwards",
		// towards the zone where we do not intend to build tetrahedra,
		// 'get_normal_vector' returns a normal vector pointing "inwards",
		// that is, towards the zone we intend to mesh
		for ( size_t i = 0; i < frontal_nb_of_coords; i++ )
			average_normal [i] += n_tri [i];
		// alternatively, think in terms of mixed product
		assert ( bent_back == ( Manifold::working .inner_prod ( B, e_AB, average_normal ) > 0. ) );
		// e_AB has been computed if neither AB or BP are filament, that's OK
		#endif  // DEBUG
		
		if ( bent_back )  goto start;                                                                }
	} // just a block of code
		
	label_build_membrane :

	if ( rels .size() != 2 )  // no information at BP, let's pray BP is regular
	{	assert ( rels .size() <= 1 );  // just AB, or empty if AB is filament
		if ( not kept_seg .is_regular ( tag::within_STSI_mesh, interf ) )  // kept_seg == BP
		{	std::cout << "trouble in frontal-3d.cpp, fill_tri_membrane_2_sides" << std::endl;
			exit (1);                                                                          }  }
	
	// since the creation of such a membrane is a rare event,
	// we take this opportunity to clean up 'seg_fill_two_crest_drawer'

	#if FRONTAL_VERBOSITY > 1
	std::cout << "cleaning seg_fill_two_crest_drawer" << std::endl << "before ";
	for ( size_t i = 0; i < seg_fill_two_crest_drawer .size(); i++ )
		std::cout << " " << seg_fill_two_crest_drawer [i] .size();
	std::cout << std::endl << std::flush;
	#endif  // FRONTAL_VERBOSITY > 1

	for ( size_t i = 0; i < seg_fill_two_crest_drawer .size(); i++ )
	{	std::list < std::pair < Cell, Cell > > & drawer = seg_fill_two_crest_drawer [i];
		for ( std::list < std::pair < Cell, Cell > > ::iterator
						it_dr  = drawer .begin(); it_dr != drawer .end(); )
		{	Cell tri = it_dr->first;
			assert ( tri .dim() == 2 );
			if ( tri .belongs_to ( interf, tag::same_dim,
			                       tag::orientation_compatible_with_mesh ) )
				it_dr ++;
			else  it_dr = drawer .erase ( it_dr );                            }             }

	#if FRONTAL_VERBOSITY > 1
	std::cout << "after";
	for ( size_t i = 0; i < seg_fill_two_crest_drawer .size(); i++ )
		std::cout << " " << seg_fill_two_crest_drawer [i] .size();
	std::cout << std::endl << std::flush;
	#endif  // FRONTAL_VERBOSITY > 1

	#if FRONTAL_VERBOSITY > 1
	std::cout << "frontal-3d.cpp line 3607, insert one tri membrane, 2 sides, "
				 << std::sqrt ( shortest ) << std::endl << std::flush;
	#endif

	const bool AB_is_not_filament = not break_filament ( AB, interf );
	const bool BP_is_not_filament = not break_filament ( kept_seg, interf );  // kept_seg == BP
	assert ( AB_is_not_filament == not AB_is_filament );
	assert ( BP_is_not_filament == not BP_is_filament );
	// a configuration with two filaments cannot happen
	assert ( AB_is_not_filament or BP_is_not_filament );
	// if either AB or BP is filament, the filament has been broken above,
	// but the segment will be used for building the membrane
	// so we must call 'update_filaments_near'
	if ( AB_is_filament )  update_filaments_near ( AB, interf );
	if ( BP_is_filament )  update_filaments_near ( kept_seg, interf );  // kept_seg == BP

	// we prefer to build AP only after invoking 'update_filaments_near' above
	// if we did it the other way around, invocation of 'update_filaments_near' below
	// would destroy the filament AP/BP and the outcome could be messy
	Cell AP ( tag::segment, A .reverse ( tag::surely_exists ), P );
	environ::manif_type::set_winding_2 ( AP, AB, kept_seg );  // kept_seg == BP
	update_filaments_near ( AP, interf );
		
	Cell BAP ( tag::triangle, AB .reverse ( tag::surely_exists ),
	                          AP, kept_seg .reverse ( tag::surely_exists ) );
	Cell ABP ( tag::triangle, AB, kept_seg, AP .reverse ( tag::does_not_exist, tag::build_now ) );
	tag::Util::insert_new_elem_in_map ( dummy_faces, { BAP, ABP } );
	tag::Util::insert_new_elem_in_map ( dummy_faces, { ABP, BAP } );
	// dummy_faces [ BAP ] = ABP; dummy_faces [ ABP ] = BAP;
	// 'BAP' will be later replaced by  ABP .reverse(), or the other way around
	
	BAP .add_to ( interf, tag::neighbourhood_relations, rels, tag::force_other_faces );
	ABP .add_to ( interf );
	
	// we do not bother to eliminate segments and faces from sets/maps ***_fill_tetrah***
	// once the faces have been removed from 'interf',
	// they will be eliminated by the 'if' at the beginning of the loop
		
	if ( AB_is_not_filament and BP_is_not_filament )
	// add pairs (triangle,segment) to 'dangerous_cavities'
	// since AP is a sharp crest, we do not add AB, BP
	{	// rotate around B, starting from ABP :
		Cell tri = interf .cell_in_front_of ( AB, tag::seen_from, ABP, tag::surely_exists );
		Cell seg = tri .boundary() .cell_behind ( B, tag::surely_exists );
		while ( true )
		{	Cell next_tri = interf .cell_in_front_of ( seg, tag::seen_from, tri, tag::surely_exists );
			if ( next_tri == ABP )  break;
			dangerous_cavities .push_back ( { tri, seg } );
			tri = next_tri;
			seg = tri .boundary() .cell_behind ( B, tag::surely_exists );                               }
		// rotate around B, starting from BAP :    // kept_seg .reverse() == PB
		tri = interf .cell_in_front_of
			( kept_seg .reverse ( tag::surely_exists ), tag::seen_from, BAP, tag::surely_exists );
		seg = tri .boundary() .cell_behind ( B, tag::surely_exists );
		while ( true )
		{	Cell next_tri = interf .cell_in_front_of ( seg, tag::seen_from, tri, tag::surely_exists );
			if ( next_tri == BAP )  break;
			dangerous_cavities .push_back ( { tri, seg } );
			tri = next_tri;
			seg = tri .boundary() .cell_behind ( B, tag::surely_exists );                               }
	}  // end of if
	
	if ( AB_is_not_filament and BP_is_not_filament )  ver_fill_burried .insert (B);
	seg_fill_membrane_3s .insert ( AP .get_positive() );
	if ( AB_is_not_filament )
	{	seg_fill_crest .insert ( { ABP, AB } );
		seg_fill_crest .insert ( { BAP, AB .reverse ( tag::surely_exists ) } );  }
	if ( BP_is_not_filament )
	{	seg_fill_crest .insert ( { ABP, kept_seg } );  // kept_seg == BP
		seg_fill_crest .insert ( { BAP, kept_seg .reverse ( tag::surely_exists ) } );  }  //  PB
	seg_fill_membrane_2s .insert ( { BAP, AP } );
	seg_fill_membrane_2s .insert ( { ABP, AP .reverse ( tag::surely_exists ) } );
	if ( AB_is_not_filament and BP_is_not_filament )  ver_fill_cavity .insert (B);
	if ( AB_is_not_filament )
	{	seg_fill_two_crest .push_back ( { ABP, AB } );
		seg_fill_two_crest .push_back ( { BAP, AB .reverse ( tag::surely_exists ) } );  }
	if ( BP_is_not_filament )
	{	seg_fill_two_crest .push_back ( { ABP, kept_seg } );  // kept_seg == BP
		seg_fill_two_crest .push_back ( { BAP, kept_seg .reverse ( tag::surely_exists ) } );  }  //  PB

	#if FRONTAL_VERBOSITY > 1	
	frontal_counter ++;
	#endif

	return true;

}  // end of  fill_tri_membrane_2_sides
	
//------------------------------------------------------------------------------------------------------//


template < class environ >   // hidden in anonymous namespace
inline void fill_one_valley ( Mesh & msh, Mesh & interf, Cell & ABC, Cell & A )
	
{	Cell AB = ABC .boundary() .cell_in_front_of ( A, tag::surely_exists );
	Cell CA = ABC .boundary() .cell_behind ( A, tag::surely_exists );
	Cell C = CA .base() .reverse ( tag::surely_exists );
	Cell B = AB .tip();
	Cell BC = ABC .boundary() .cell_behind ( C, tag::surely_exists );
	Cell BAD = interf .cell_in_front_of ( AB, tag::seen_from, ABC, tag::surely_exists );
	Cell AD = BAD .boundary() .cell_in_front_of ( A, tag::surely_exists );
	Cell D = AD .tip();
	Cell DB = BAD .boundary() .cell_behind ( B, tag::surely_exists );
	Cell ACE = interf .cell_in_front_of ( CA, tag::seen_from, ABC, tag::surely_exists );
	Cell CE = ACE .boundary() .cell_in_front_of ( C, tag::surely_exists );
	Cell E = CE .tip();
	Cell EA = ACE .boundary() .cell_behind ( A, tag::surely_exists );

	#if FRONTAL_VERBOSITY > 1
	std::cout << "frontal-3d.cpp line 4458, filling valley" << std::endl;
	#endif

	const std::vector < double > e_AB = environ::manif_type::get_vector ( AB );
	const std::vector < double > e_CA = environ::manif_type::get_vector ( CA );
	const std::vector < double > e_AD = environ::manif_type::get_vector ( AD );
	const std::vector < double > e_EA = environ::manif_type::get_vector ( EA );

	// we first place P based on B, C, D and E
	// we later improve the location of P, taking into account neighbour triangles
	std::vector < double > e_AP ( frontal_nb_of_coords );
	for ( size_t i = 0; i < frontal_nb_of_coords; i++ )
		e_AP [i] = 0.35 * e_AD [i] + 0.15 * e_AB [i] - 0.15 * e_CA [i] - 0.35 * e_EA [i];
	// 'relocate_3d' will improve the location anyway
	
	// 'walls' is a set of triangles if manif non-winding
	// it is a map (triangle, vertex with winding) if manif winding
	typename environ::manif_type::type_of_walls walls;	
	std::set < Cell > triangles_on_floor { ABC, BAD, ACE };
	typename environ::manif_type::winding_cell AA = environ::manif_type::add_winding ( A, 0 );
	for ( std::set < Cell > ::const_iterator it = triangles_on_floor .begin();
				it != triangles_on_floor .end(); it ++                            )
		environ::manif_type::insert_wall ( walls, * it, AA );

	Cell P ( tag::vertex );
	for ( size_t i = 0; i < frontal_nb_of_coords; i++ )
	{	const Function & x = Manifold::working .coordinates() [i];
		x(P) = x(A) + e_AP [i];                                     }
		// we consider both A and P have zero winding
	
	// Manifold::working .project (P)  not needed in 3D
	// 'compute_data' uses information from A to compute eigendirection and isr at P
	environ::metric_type::compute_data ( tag::at_point, P, tag::use_information_from, A );
	// P will be added to the cloud later, after 'relocate_3d'

	Cell PA ( tag::segment, P .reverse ( tag::does_not_exist, tag::build_now ), A );  // zero winding
	Cell PB ( tag::segment, P .reverse ( tag::surely_exists ), B );
	environ::manif_type::set_winding_1 ( PB, AB );
	Cell PC ( tag::segment, P .reverse ( tag::surely_exists ), C );
	environ::manif_type::set_winding_neg_1 ( PC, CA );
	Cell PD ( tag::segment, P .reverse ( tag::surely_exists ), D );
	environ::manif_type::set_winding_1 ( PD, AD );
	Cell PE ( tag::segment, P .reverse ( tag::surely_exists ), E );
	environ::manif_type::set_winding_neg_1 ( PE, EA );
	update_filaments_near ( PA, interf );
	update_filaments_near ( PB, interf );
	update_filaments_near ( PC, interf );
	update_filaments_near ( PD, interf );
	update_filaments_near ( PE, interf );

	Cell ABP ( tag::triangle, AB, PB .reverse ( tag::does_not_exist, tag::build_now ), PA );
	Cell ACP ( tag::triangle, CA .reverse ( tag::surely_exists ),
	                          PC .reverse ( tag::does_not_exist, tag::build_now ), PA );
	Cell ADP ( tag::triangle, AD, PD .reverse ( tag::does_not_exist, tag::build_now ), PA );
	Cell AEP ( tag::triangle, EA .reverse ( tag::surely_exists ),
	                          PE .reverse ( tag::does_not_exist, tag::build_now ), PA );
	Cell BCP ( tag::triangle, BC, PC .reverse ( tag::surely_exists ), PB );
	Cell BDP ( tag::triangle, DB .reverse ( tag::surely_exists ), PD .reverse ( tag::surely_exists ), PB );
	Cell CEP ( tag::triangle, CE, PE .reverse ( tag::surely_exists ), PC );

	Cell ABC_alt = remove_face ( ABC, interf );
	// ABC .remove_from ( interf );
	Cell BAD_alt = remove_face ( BAD, interf );
	// BAD .remove_from ( interf );
	Cell ACE_alt = remove_face ( ACE, interf );
	// ACE .remove_from ( interf );
	BCP .add_to ( interf );
	ADP .add_to ( interf );
	BDP .reverse ( tag::does_not_exist, tag::build_now ) .add_to ( interf );
	CEP .add_to ( interf );
	AEP .reverse ( tag::does_not_exist, tag::build_now ) .add_to ( interf );
	
	Cell ABCP ( tag::tetrahedron, ABC_alt, BCP .reverse ( tag::does_not_exist, tag::build_now ),
	                              ACP, ABP .reverse ( tag::does_not_exist, tag::build_now )     );
	Cell ABDP ( tag::tetrahedron, BAD_alt, ABP, BDP,
	                              ADP .reverse ( tag::does_not_exist, tag::build_now ) );
	Cell ACEP ( tag::tetrahedron, ACE_alt, CEP .reverse ( tag::does_not_exist, tag::build_now ),
	                              AEP, ACP .reverse ( tag::does_not_exist, tag::build_now )     );
	ABCP .add_to ( msh );
	ABDP .add_to ( msh );
	ACEP .add_to ( msh );
	
	ver_fill_burried .insert (A);
	ver_fill_burried .insert (B);
	ver_fill_burried .insert (C);
	seg_fill_crest .insert ( { BCP, BC } );
	seg_fill_crest .insert ( { ADP, AD } );
	seg_fill_crest .insert ( { BDP .reverse ( tag::surely_exists ), DB } );
	seg_fill_crest .insert ( { CEP, CE } );
	seg_fill_crest .insert ( { AEP .reverse ( tag::surely_exists ), EA } );
	// { ADP, PA } is persistently added during 'relocate_3d'
	seg_fill_membrane_2s .insert ( { ADP, PA } );
	seg_fill_membrane_2s .insert ( { BCP, PB } );
	seg_fill_membrane_2s .insert ( { CEP, PC } );
	seg_fill_membrane_2s .insert ( { BDP .reverse ( tag::surely_exists ), PD } );
	seg_fill_membrane_2s .insert ( { AEP .reverse ( tag::surely_exists ), PE } );
	ver_fill_cavity .insert (A);
	ver_fill_cavity .insert (B);
	ver_fill_cavity .insert (C);
	seg_fill_two_crest .push_back ( { BCP, BC } );
	seg_fill_two_crest .push_back ( { ADP, AD } );
	seg_fill_two_crest .push_back ( { BDP .reverse ( tag::surely_exists ), DB } );
	seg_fill_two_crest .push_back ( { CEP, CE } );
	seg_fill_two_crest .push_back ( { AEP .reverse ( tag::surely_exists ), EA } );
   seg_fill_two_crest .push_back ( { ADP, PA } );

	// improve the location of P, taking into account nearby vertices and triangles
	// also build filaments when appropriate
	// may build new segments, triangles and tetrahedra around P, add them to 'interf' and to 'msh'
	typename environ::manif_type::set_of_winding_cells other_vertices;  // empty
	typename environ::manif_type::set_of_winding_cells vertices_from_walls
		{ environ::manif_type::add_winding ( A, 0 ),
		  environ::manif_type::add_winding_1 ( B, AB ),
		  environ::manif_type::add_winding_neg_1 ( C, CA ),
		  environ::manif_type::add_winding_1 ( D, AD ),
		  environ::manif_type::add_winding_neg_1 ( E, EA ) };
	union_of < typename environ::manif_type::set_of_winding_cells > vertices
		{	vertices_from_walls, other_vertices };
	relocate_3d < environ > ( msh, interf, P, walls, vertices, { { PA, ADP } } );

	#if FRONTAL_VERBOSITY > 1
	std::cout << "frontal-3d.cpp line 3175, P [ ";
	for ( size_t i = 0; i < frontal_nb_of_coords; i++ )
	{	const Function & x = Manifold::working .coordinates() [i];
		std::cout << x(P) << " ";                                   }
	std::cout << "]" << std::endl;
	#endif  // FRONTAL_VERBOSITY > 1

	// we insert P in the cloud only now because in relocate_3d we do not want it there
	// another reason is that P has been moved during 'relocate_3d'
	// now its location in space is frozen
	// so it will be inserted at the right position in the metric tree
	// 'relocate_3d' may have eliminated P from 'interf' so we must ask :
	if ( P .belongs_to ( interf, tag::cell_has_low_dim ) )
		environ::manif_type::node_container .insert (P);

	#if FRONTAL_VERBOSITY > 1	
	if ( frontal_counter < frontal_counter_max )  frontal_counter ++;
	// 'relocate_3d' may have already incremented 'frontal_counter'
	#endif

}  // end of  fill_one_valley
	
//------------------------------------------------------------------------------------------------------//


template < class environ >
inline bool fill_valley ( Mesh & msh, Mesh & interf )   // hidden in anonymous namespace

// we use a list of (tri,ver), candidates for "valley", that is,
// with two "walls" meeting at 'ver'
  
// 'msh' is a Mesh::Fuzzy made of tetrahedra (being built now)
// 'interf' is a Mesh::STSI made of triangles
	
// 'environ' describes the type of manifold we are working in
// and how we treat information about metric and exterior product
// environ::manif_type could be ManifoldNoWinding or ManifoldQuotient (defined in frontal.cpp)
// environ::metric_type may be one of ISRContainer::{Inactive,Active} (defined in frontal.cpp)
// environ::ext_prod_type may be ExtProd3d, 3dRev (defined in frontal.cpp)
	
{	assert ( tri_fill_isolated    .empty() );
	assert ( seg_fill_membrane_3s .empty() );
	assert ( ver_fill_burried     .empty() );
	assert ( seg_fill_crest       .empty() );
	assert ( ver_fill_cavity      .empty() );
	assert ( seg_fill_membrane_2s .empty() );

	while ( not valley_tri_ver .empty() )
	
	{	Cell ABC = valley_tri_ver .back() .first;
		Cell A  = valley_tri_ver .back() .second;
		valley_tri_ver .pop_back();
		
		if ( not ABC .belongs_to ( interf, tag::orientation_compatible_with_mesh ) )
			continue;   // to next valley candidate

		Cell AB = ABC .boundary() .cell_in_front_of ( A, tag::surely_exists );
		Cell CA = ABC .boundary() .cell_behind ( A, tag::surely_exists );
		Cell B = AB .tip();
		Cell C = CA .base() .reverse ( tag::surely_exists );
		// Cell BC = ABC .boundary() .cell_behind ( C, tag::surely_exists );

		const double angle_at_AB = angle_3d < environ > ( interf, ABC, AB );
		if ( angle_at_AB > threshold_angle_for_valley )  continue;    // to next valley candidate
		const double angle_at_CA = angle_3d < environ > ( interf, ABC, CA );
		if ( angle_at_CA > threshold_angle_for_valley )  continue;    // to next valley candidate
	
		fill_one_valley < environ > ( msh, interf, ABC, A );
		return true;                                                                      }

	return false;

}  // end of  fill_valley
	
//------------------------------------------------------------------------------------------------------//


template < class environ >
inline size_t concave_segs_around_A    // hidden in anonymous namespace
( const Cell & A, const Mesh & interf, const Cell & ABC, const Cell & AB, const Cell & CA )

// if there are (very) convex edges, we only want to count
// the concave edges which are contiguous to 'tri'

{	size_t nb_concave_segs_start = 0, nb_concave_segs_end = 0;
	bool previously_encountered_a_convex_edge = false;
	Cell tri = ABC;
	Cell seg = CA;
	while ( seg != AB .reverse ( tag::surely_exists ) )
	{	std::vector < double > norm_tri =
			environ::ext_prod_type::get_normal_vector ( tag::domain_3d, tag::at_tri, tri );
	  // although the interface is looking "outwards",
	  // towards the zone where we do not intend to build tetrahedra,
	  // 'get_normal_vector' returns a normal vector pointing "inwards",
	  // that is, towards the zone we intend to mesh
		tri = interf .cell_in_front_of ( seg, tag::seen_from, tri );
		seg = tri .boundary() .cell_behind ( A, tag::surely_exists );
		std::vector < double > e = environ::manif_type::get_vector ( seg );
		const double prod = Manifold::working .inner_prod ( A, norm_tri, e );
		if ( prod > 0.3 )  // (very) convex
		{	if ( previously_encountered_a_convex_edge )  nb_concave_segs_end = 0;
			else  previously_encountered_a_convex_edge = true;                     }
		else if ( prod < -0.3 )  // concave
		{	if ( previously_encountered_a_convex_edge )  nb_concave_segs_end ++;
			else  nb_concave_segs_start ++;                                       }          }
		// else  do nothing
	return nb_concave_segs_start + nb_concave_segs_end;                                     }

//------------------------------------------------------------------------------------------------------//


template < class environ >
inline size_t concave_segs_around_B    // hidden in anonymous namespace
( const Cell & B, const Mesh & interf, const Cell & ABC, const Cell & AB, const Cell & BC )

// if there are (very) convex edges, we only want to count
// the concave edges which are contiguous to 'tri'

{	size_t nb_concave_segs_start = 0, nb_concave_segs_end = 0;
	bool previously_encountered_a_convex_edge = false;
	Cell tri = ABC;
	Cell seg = BC;
	while ( seg != AB .reverse ( tag::surely_exists ) )
	{	std::vector < double > norm_tri =
			environ::ext_prod_type::get_normal_vector ( tag::domain_3d, tag::at_tri, tri );
	  // although the interface is looking "outwards",
	  // towards the zone where we do not intend to build tetrahedra,
	  // 'get_normal_vector' returns a normal vector pointing "inwards",
	  // that is, towards the zone we intend to mesh
		tri = interf .cell_in_front_of ( seg, tag::seen_from, tri );
		seg = tri .boundary() .cell_in_front_of ( B, tag::surely_exists );
		std::vector < double > e = environ::manif_type::get_vector ( seg );
		const double prod = Manifold::working .inner_prod ( B, norm_tri, e );
		if ( prod < -0.3 )  // (very) convex
		{	if ( previously_encountered_a_convex_edge )  nb_concave_segs_end = 0;
			else  previously_encountered_a_convex_edge = true;                     }
		else if ( prod > 0.3 )  // concave
		{	if ( previously_encountered_a_convex_edge )  nb_concave_segs_end ++;
			else  nb_concave_segs_start ++;                                       }          }
		// else  do nothing
	return  nb_concave_segs_start + nb_concave_segs_end;                                    }

//------------------------------------------------------------------------------------------------------//


template < class environ >   // hidden in anonymous namespace
inline bool fill_two_tetrahedra_crest ( Mesh & msh, Mesh & interf )
	
// fill two tetrahedra using two existing triangles having a common edge
// build four new segments and five new triangles
// seg_fill_two_crest contains a pair (triangle, side)
// return false if did nothing

// 'msh' is a Mesh::Fuzzy made of tetrahedra (being built now)
// 'interf' is a Mesh::STSI made of triangles
	
// 'environ' describes the type of manifold we are working in
// and how we treat information about metric and exterior product
// environ::manif_type could be ManifoldNoWinding or ManifoldQuotient (defined in frontal.cpp)
// environ::metric_type may be one of ISRContainer::{Inactive,Active} (defined in frontal.cpp)
// environ::ext_prod_type may be ExtProd3d, 3dRev (defined in frontal.cpp)

{	assert ( tri_fill_isolated    .empty() );
	assert ( dangerous_cavities   .empty() );
	assert ( seg_fill_membrane_3s .empty() );
	assert ( ver_fill_burried     .empty() );
	assert ( seg_fill_crest       .empty() );
	assert ( ver_fill_cavity      .empty() );
	assert ( seg_fill_membrane_2s .empty() );
	assert ( valley_tri_ver       .empty() );

	// move elements of 'seg_fill_two_crest' into 'seg_fill_two_crest_drawer'
	// if a segment has 'm' "concave" neighbour segments, goes to drawer number 'm'

	#if FRONTAL_VERBOSITY > 1
	{	std::cout << "drawers " << seg_fill_two_crest .size() << " + ";
		for ( size_t i = 0; i < seg_fill_two_crest_drawer .size(); i++ )
			std::cout << " " << seg_fill_two_crest_drawer [i] .size();
		std::cout << std::endl << std::flush;                                          }
	#endif  // FRONTAL_VERBOSITY > 1
	
	for ( std::list < std::pair < Cell, Cell > > ::iterator
	      it = seg_fill_two_crest .begin(); it != seg_fill_two_crest .end();
	      it = seg_fill_two_crest .erase ( it )                             )

	{	Cell ABC = it->first;
		if ( not ABC .belongs_to ( interf, tag::same_dim, tag::orientation_compatible_with_mesh ) )
			continue;   // to next segment in seg_fill_two_crest
		assert ( ABC .boundary() .number_of ( tag::segments ) == 3 );
		Cell AB = it->second;
		assert ( AB .belongs_to ( ABC .boundary(), tag::same_dim, tag::orientation_compatible_with_mesh ) );
		Cell A = AB .base() .reverse ( tag::surely_exists );
		Cell B = AB .tip();
		Cell BC = ABC .boundary() .cell_in_front_of ( B, tag::surely_exists );
		Cell CA = ABC .boundary() .cell_behind ( A, tag::surely_exists );

		size_t nb_concave_segs = 0;

		// we test for a (double) membrane

		Cell other_tri_BC = interf .cell_in_front_of ( BC, tag::seen_from, ABC, tag::surely_exists );
		if ( other_tri_BC .boundary() .cell_in_front_of ( B, tag::surely_exists )
		     != AB .reverse ( tag::surely_exists ) )  // BC is not the edge of a membrane
			nb_concave_segs += concave_segs_around_A < environ > ( A, interf, ABC, AB, CA );
		Cell other_tri_CA = interf .cell_in_front_of ( CA, tag::seen_from, ABC, tag::surely_exists );
		if ( other_tri_CA .boundary() .cell_behind ( A, tag::surely_exists )
		     != AB .reverse ( tag::surely_exists ) )  // CA is not the edge of a membrane
			nb_concave_segs += concave_segs_around_B < environ > ( B, interf, ABC, AB, BC );

		assert ( nb_concave_segs < seg_fill_two_crest_drawer .size() );
		seg_fill_two_crest_drawer [ nb_concave_segs ] .push_back ( { ABC, AB } );
	}  // end of  for  over  seg_fill_two_crest

	#if FRONTAL_VERBOSITY > 1
	{	std::cout << "drawers " << seg_fill_two_crest .size() << " + ";
		for ( size_t i = 0; i < seg_fill_two_crest_drawer .size(); i++ )
			std::cout << " " << seg_fill_two_crest_drawer [i] .size();
		std::cout << std::endl << std::flush;                                          }
	#endif  // FRONTAL_VERBOSITY > 1
	
	next_drawer :
	size_t kept_m = seg_fill_two_crest_drawer .size();
	for ( int m = seg_fill_two_crest_drawer .size() - 1; m >= 0; m-- )
	{	if ( seg_fill_two_crest_drawer [m] .empty() )  continue;
		kept_m = size_t (m);  break;                              }

	if ( kept_m == seg_fill_two_crest_drawer .size() )  // all drawers empty
		return  false;

	std::list < std::pair < Cell, Cell > > & drawer = seg_fill_two_crest_drawer [ kept_m ];
		
	// preliminary search for the sharpest angle
	// height of an equilateral tetrahedron : sqrt 2/3
	static const double target_dist_2 = 8./3.;  // frontal_seg_size 3D
	std::list < std::pair < Cell, Cell > > ::iterator kept_it = drawer .end();
	double prod_max = -10.;

	for ( std::list < std::pair < Cell, Cell > > ::iterator
	      it = drawer .begin(); it != drawer .end();        )

	{	Cell ABC = it->first;
		if ( not ABC .belongs_to ( interf, tag::same_dim, tag::orientation_compatible_with_mesh ) )
		{	it = drawer .erase ( it );  continue;  }  // to next segment in drawer
		
	  // if ( ABC .boundary() .number_of ( tag::segments ) != 3 )
		// {	assert ( ABC .boundary() .number_of ( tag::segments ) == 2 );
		// 	continue;                           }  // to next segment in drawer
		assert ( ABC .boundary() .number_of ( tag::segments ) == 3 );
		Cell AB = it->second;
		assert ( AB .belongs_to ( ABC .boundary(), tag::same_dim, tag::orientation_compatible_with_mesh ) );
		Cell BAD = interf .cell_in_front_of ( AB, tag::seen_from, ABC, tag::surely_exists );
		assert ( BAD .boundary() .number_of ( tag::segments ) == 3 );
		Cell B = AB .tip();
		Cell A = AB .base() .reverse ( tag::surely_exists );
		Cell BC = ABC .boundary() .cell_in_front_of ( B, tag::surely_exists );
		Cell C = BC .tip();
		Cell CA = ABC .boundary() .cell_behind ( A, tag::surely_exists );
		assert ( CA .base() .reverse() == C );
		Cell AD = BAD .boundary() .cell_in_front_of ( A, tag::surely_exists );
		Cell D = AD .tip();
		Cell DB = BAD .boundary() .cell_behind ( B, tag::surely_exists );
		assert ( DB .base() .reverse() == D );
		
		// twice the height of an equilateral triangle : sqrt 3 = 1.73 = 2 * 0.866
		// height of an equilateral tetrahedron : sqrt 2/3 = 0.8165
		const std::vector < double > e_DC =
			environ::manif_type::get_vector ( D, environ::manif_type::add_winding_2 ( C, DB, BC ) );
		const double DC2 = Manifold::working .sq_dist ( D, C, e_DC );
		if ( DC2 > target_dist_2 )
		{	it = drawer .erase ( it );  continue;  }  // to next segment in drawer
		// DC < target_dist_2, we may want to fill tetrahedron ABCD
		// check curvature :
		const std::vector < double > e_AD = environ::manif_type::get_vector ( AD );
		const std::vector < double > e_DB = environ::manif_type::get_vector ( DB );
		std::vector < double > e_D ( frontal_nb_of_coords );
		for ( size_t i = 0; i < frontal_nb_of_coords; i++ )  e_D [i] = e_AD [i] - e_DB [i];
		const std::vector < double > n_ABC =
			environ::ext_prod_type::get_normal_vector ( tag::domain_3d, tag::at_tri, ABC );
		// although the interface is looking "outwards",
		// towards the zone where we do not intend to build tetrahedra,
		// 'get_normal_vector' returns a normal vector pointing "inwards",
		// that is, towards the zone we intend to mesh
		if ( Manifold::working .inner_prod ( D, e_D, n_ABC ) < 0.1 )  // was 0.25
		{	it = drawer .erase ( it );  continue;   }  // to next segment in drawer
		// do not create tetrahedron if angle is wide :
		const std::vector < double > e_CA = environ::manif_type::get_vector ( CA );
		const std::vector < double > e_BC = environ::manif_type::get_vector ( BC );
		std::vector < double > e_C ( frontal_nb_of_coords );
		for ( size_t i = 0; i < frontal_nb_of_coords; i++ )  e_C [i] = e_BC [i] - e_CA [i];
		// threshold angle has cos = - sqrt 7 / 3 = - 0.881917 ;  sqrt 7 = 2.64575
		normalize_vector_approx ( C, e_C, tag::around, tag::Util::sqrt_3 );
		normalize_vector_approx ( D, e_D, tag::around, tag::Util::sqrt_3 );
		const double prod = Manifold::working .inner_prod ( C, e_C, e_D );
		if ( prod < - 0.8 )
		{	it = drawer .erase ( it );  continue;  }  // to next segment in drawer
		if ( prod > prod_max )
		{	kept_it = it;  prod_max = prod;  }
		it++;                                                                                         }
		// end of for (preliminary search)

	if ( drawer .empty() )  goto next_drawer;

	assert ( kept_it != drawer .end() );
	Cell ABC = kept_it->first;
	Cell AB = kept_it->second;
	drawer .erase ( kept_it );

	fill_two_tetrahedra_crest_core < environ > ( msh, interf, ABC, AB );

	return true;
	
}  // end of fill_two_tetrahedra_crest
	
//------------------------------------------------------------------------------------------------------//


template < class environ >    // hidden in anonymous namespace
inline bool fill_tetra_out_of_the_blue ( Mesh & msh, Mesh & interf )
	
// build a brand new tetrahedron

// return false if did nothing

// 'msh' is a Mesh::Fuzzy made of tetrahedra (being built now)
// 'interf' is a Mesh::STSI made of triangles
	
// 'environ' describes the type of manifold we are working in
// and how we treat information about metric and exterior product
// environ::manif_type could be ManifoldNoWinding or ManifoldQuotient (defined in frontal.cpp)
// environ::metric_type may be one of ISRContainer::{Inactive,Active} (defined in frontal.cpp)
// environ::ext_prod_type may be ExtProd3d, 3dRev (defined in frontal.cpp)
	
{	assert ( tri_fill_isolated    .empty() );
	assert ( dangerous_cavities   .empty() );
	assert ( seg_fill_membrane_3s .empty() );
	assert ( ver_fill_burried     .empty() );
	assert ( seg_fill_crest       .empty() );
	assert ( ver_fill_cavity      .empty() );
	assert ( seg_fill_membrane_2s .empty() );
	assert ( valley_tri_ver       .empty() );
	assert ( seg_fill_two_crest   .empty() );

	Mesh::Iterator it = interf .iterator
		( tag::over_cells_of_max_dim, tag::orientation_compatible_with_mesh );
	it .reset();  assert ( it .in_range() );
	Cell ABC = * it;
	Mesh::Iterator itt = ABC .boundary() .iterator
		( tag::over_cells_of_max_dim, tag::orientation_compatible_with_mesh );
	itt .reset();  assert ( itt .in_range() );
	Cell AB = * itt;
	Cell A = AB .base() .reverse ( tag::surely_exists );
	Cell B = AB .tip();
	Cell CA = ABC .boundary() .cell_behind ( A, tag::surely_exists );
	Cell BC = ABC .boundary() .cell_in_front_of ( B, tag::surely_exists );
	Cell C = BC .tip();
	assert ( CA .base() .reverse() == C );
	
	std::vector < double > e_AB = environ::manif_type::get_vector ( AB );
	std::vector < double > e_CA = environ::manif_type::get_vector ( CA );
	const std::vector < double > n_ABC =
		environ::ext_prod_type::get_normal_vector ( tag::domain_3d, tag::at_tri, ABC );
	// although the interface is looking "outwards",
	// towards the zone where we do not intend to build tetrahdra,
	// 'get_normal_vector' returns a normal vector pointing "inwards",
	// that is, towards the zone we intend to mesh
	
	// 'walls' is a set of triangles if manif non-winding
	// it is a map (triangle, vertex with winding) if manif winding
	typename environ::manif_type::type_of_walls walls;	
	typename environ::manif_type::winding_cell AA = environ::manif_type::add_winding ( A, 0 );
	environ::manif_type::insert_wall ( walls, ABC, AA );

	Cell P ( tag::vertex );
	// twice the height of an equilateral triangle : sqrt 3
	// height of an equilateral tetrahedron : sqrt 2/3
	for ( size_t i = 0; i < frontal_nb_of_coords; i++ )
	{	const Function & x = Manifold::working .coordinates() [i];
		x(P) = x(A) + ( e_AB [i] - e_CA [i] ) / 3.   // this is the barycenter of ABC
		            + n_ABC [i] * tag::Util::sqrt_two_thirds;       }
		// we consider both A and P have zero winding
	// Manifold::working .project (P)  not needed in 3D
	// 'compute_data' uses information from A to compute eigendirection and isr at P
	environ::metric_type::compute_data ( tag::at_point, P, tag::use_information_from, A );
	// P will be added to the cloud later, after 'relocate_3d'

	Cell P_rev = P .reverse ( tag::does_not_exist, tag::build_now );
	Cell PA ( tag::segment, P_rev, A );  // zero winding
	Cell PB ( tag::segment, P_rev, B );
	environ::manif_type::set_winding_1 ( PB, AB );
	Cell PC ( tag::segment, P_rev, C );
	environ::manif_type::set_winding_neg_1 ( PC, CA );
	update_filaments_near ( PA, interf );
	update_filaments_near ( PB, interf );
	update_filaments_near ( PC, interf );
	Cell ABP ( tag::triangle, AB, PB .reverse ( tag::does_not_exist, tag::build_now ), PA );
	Cell BCP ( tag::triangle, BC, PC .reverse ( tag::does_not_exist, tag::build_now ), PB );
	Cell CAP ( tag::triangle, CA, PA .reverse ( tag::does_not_exist, tag::build_now ), PC );
	
	Cell ABC_alt = remove_face ( ABC, interf );
	// ABC .remove_from ( interf );
	ABP .add_to ( interf );
	BCP .add_to ( interf );
	CAP .add_to ( interf );  // no need to propagate normals in 3D

	Cell tetra ( tag::tetrahedron, ABC_alt,
	             ABP .reverse ( tag::does_not_exist, tag::build_now ),
	             BCP .reverse ( tag::does_not_exist, tag::build_now ),
	             CAP .reverse ( tag::does_not_exist, tag::build_now ) );
	tetra .add_to ( msh );
	
	seg_fill_crest .insert ( { ABP, AB } );
	seg_fill_crest .insert ( { BCP, BC } );
	seg_fill_crest .insert ( { CAP, CA } );
	seg_fill_membrane_2s .insert ( { ABP, PA } );
	seg_fill_membrane_2s .insert ( { BCP, PB } );
	seg_fill_membrane_2s .insert ( { CAP, PC } );
	seg_fill_two_crest .push_back ( { ABP, AB } );
	seg_fill_two_crest .push_back ( { BCP, BC } );
	seg_fill_two_crest .push_back ( { CAP, CA } );

	// improve the location of P, taking into account nearby vertices and triangles
	// also build filaments when appropriate
	// may build new segments, triangles and tetrahedra around P, add them to 'interf' and to 'msh'
	typename environ::manif_type::set_of_winding_cells other_vertices;  // empty
	typename environ::manif_type::set_of_winding_cells vertices_from_walls
		{ environ::manif_type::add_winding ( A, 0 ),
		  environ::manif_type::add_winding_1 ( B, AB ),
		  environ::manif_type::add_winding_neg_1 ( C, CA ) };
	union_of < typename environ::manif_type::set_of_winding_cells > vertices
		{	vertices_from_walls, other_vertices };
	relocate_3d < environ > ( msh, interf, P, walls, vertices );
	
	#if FRONTAL_VERBOSITY > 1
	std::cout << "frontal-3d.cpp line 4463, P [";
	for ( size_t i = 0; i < frontal_nb_of_coords; i++ )
	{	const Function & x = Manifold::working .coordinates() [i];
		std::cout << x(P) << " ";                                   }
	std::cout << "]" << std::endl;
	#endif  // FRONTAL_VERBOSITY > 1

	// we insert P in the cloud only now because in relocate_3d we do not want it there
	// another reason is that P has been moved during 'relocate_3d'
	// now its location in space is frozen
	// so it will be inserted at the right position in the metric tree
	// 'relocate_3d' may have eliminated P from 'interf' so we must ask :
	if ( P .belongs_to ( interf, tag::cell_has_low_dim ) )
		environ::manif_type::node_container .insert (P);

	#if FRONTAL_VERBOSITY > 1
	if ( frontal_counter < frontal_counter_max )  frontal_counter ++;
	// 'relocate_3d' may have already incremented 'frontal_counter'
	#endif	
	
	return true;

}  // end of fill_tetra_out_of_the_blue
	
//------------------------------------------------------------------------------------------------------//


template < class environ > void print_volumes (	Mesh & msh )    // hidden in anonymous namespace
	
{	double min_prod = 100., max_prod = -100.;
	Mesh::Iterator it_tetra = msh .iterator ( tag::over_cells_of_max_dim );
	for ( it_tetra .reset(); it_tetra .in_range(); it_tetra ++ )
	{	Cell tetra = * it_tetra;
		assert ( tetra .dim() == 3 );
		Mesh::Iterator it_tri = tetra .boundary() .iterator ( tag::over_cells_of_max_dim );
		for ( it_tri .reset(); it_tri .in_range(); it_tri ++ )
		{	Cell ABC = * it_tri;
			assert ( ABC .dim() == 2 );
			Mesh::Iterator it_seg = ABC .boundary() .iterator ( tag::over_cells_of_max_dim );
			it_seg .reset();  assert ( it_seg .in_range() );
			Cell AB = * it_seg;
			assert ( AB .dim() == 1 );
			Cell A = AB .base() .reverse();
			Cell B = AB .tip();
			Cell BAD = tetra .boundary() .cell_in_front_of ( AB, tag::surely_exists );
			assert ( BAD .dim() == 2 );
			Cell AD = BAD .boundary() .cell_in_front_of ( A, tag::surely_exists );
			Cell D = AD .tip();
			// alternatively, think in terms of mixed product
			const std::vector < double > e_AD = environ::manif_type::get_vector ( AD );
			const std::vector < double > n_ABC =
				environ::ext_prod_type::get_normal_vector ( tag::domain_3d, tag::at_tri, ABC );
			// although the interface is looking "outwards",
			// towards the zone where we do not intend to build tetrahedra,
			// 'get_normal_vector' returns a normal vector pointing "inwards",
			// that is, towards the zone we intend to mesh
			double tmp = Manifold::working .inner_prod ( A, D, e_AD, n_ABC );
			// in 3D we do not need the special inner_prod linked to ext_prod_type
			if ( tmp > max_prod )  max_prod = tmp;
			if ( tmp < min_prod )  min_prod = tmp;                                              }  }
	std::cout << "inner prod between " << min_prod << " and " << max_prod << std::endl << std::flush;

}  // end of print_volumes
	
//------------------------------------------------------------------------------------------------------//


template < class environ >     // line 4541
void frontal_construct_3d            // hidden in anonymous namespace
( Mesh & msh, const tag::Boundary &, const Mesh & bdry,
  const tag::StartAt &, Cell start                     )

// for three-dimensional meshes
	
// 'start' is a triangle belonging to 'bdry'
// 'normal' is a vector orthogonal to 'start'

// 'environ' describes the type of manifold we are working in
// and how we treat information about metric and exterior product
// environ::manif_type could be ManifoldNoWinding or ManifoldQuotient (defined in frontal.cpp)
// environ::metric_type may be one of ISRContainer::{Inactive,Active} (defined in frontal.cpp)
// environ::ext_prod_type may be ExtProd3d, 3dRev (defined in frontal.cpp)
	
{	assert ( bdry .has_empty_boundary() );

	#if FRONTAL_VERBOSITY > 1
	std::cout << "frontal-3d.cpp line 4544, manif_type "
	          << environ::manif_type::info() << std::endl << std::flush;
	#endif
	assert ( bdry  .dim() == 2 );
	assert ( msh   .dim() == 3 );
	if ( start .dim() == 1 )  start = bdry .cell_in_front_of ( start );
	if ( start .dim() == 0 )
	{	Mesh::Iterator it = bdry .iterator
			( tag::over_cells_of_max_dim, tag::orientation_compatible_with_mesh, tag::around, start );
		it .reset();  assert ( it .in_range() );
		start = *it;                                                                                 }
	assert ( start .dim() == 2 );
	assert ( start .belongs_to ( bdry, tag::same_dim, tag::orientation_compatible_with_mesh ) );

	#if FRONTAL_VERBOSITY > 1
	assert ( tri_fill_isolated    .empty() );
	assert ( dangerous_cavities   .empty() );
	assert ( ver_fill_burried     .empty() );
	assert ( seg_fill_membrane_3s .empty() );
	assert ( seg_fill_crest       .empty() );
	assert ( ver_fill_cavity      .empty() );
	assert ( seg_fill_membrane_2s .empty() );
	assert ( valley_tri_ver       .empty() );
	assert ( seg_fill_two_crest   .empty() );
	assert ( environ::manif_type::node_container .empty() );
	assert ( environ::metric_type::empty() );
	#endif  // FRONTAL_VERBOSITY > 1

	for ( size_t m = 0; m < seg_fill_two_crest_drawer .size(); m++ )
		seg_fill_two_crest_drawer [m] .clear();
	
	{ // just a block of code for hiding 'it'
	Mesh::Iterator it = bdry .iterator ( tag::over_vertices );
	for ( it .reset(); it .in_range(); it++ )
		environ::manif_type::node_container .insert ( *it );
	} // just a block of code

	// we don't want to change 'bdry' so we make a copy of it
	// one more reason : bdry may be Mesh::Connected::OneDim or Mesh::Fuzzy,
	// we want a Mesh::STSI interface to play with

	Mesh interface ( tag::STSI, tag::of_dim, 2 );  // initially empty

	build_component ( tag::domain_3d, bdry, interface, tag::start_at, start );
	// add faces to interface, tri_fill_isolated, ver_fill_burried,
	// seg_fill_membrane_3s, seg_fill_crest, seg_fill_membrane_2s, seg_fill_two_crest

	// if the metric is anisotropic, compute_data computes
	// the inner spectral radius for all vertices along the interface
	// if the metric is isotropic, does nothing at all
	// environ::metric_type::compute_data ( tag::at_point, start .tip() );
	// environ::metric_type::compute_data ( tag::along, interface, tag::start_at, start .tip() );

	// no need to compute anything for the exterior product in 3D (codimension 0)

	#if FRONTAL_VERBOSITY > 1
	std::cout << "counter max = ";  std::cin >> frontal_counter_max;
	frontal_counter = 0;
	#endif
		
	while ( interface .number_of ( tag::cells_of_max_dim ) > 0 )

	{
		#if FRONTAL_VERBOSITY > 1

		std::cout << "frontal-3d.cpp line 5146, tri_fill_isolated size " << tri_fill_isolated .size()
		          << ", counter " << frontal_counter + 1 << std::endl << std::flush;
		if ( fill_isolated_tetrahedron < environ >
		     ( msh, interface, tag::mobile_vertex, Cell ( tag::non_existent ) ) .exists() )
		{	if ( frontal_counter >= frontal_counter_max )  break;
			continue;                                              }

		std::cout << "frontal-3d.cpp line 5154, ver_fill_burried (one tetra) size "
					 << ver_fill_burried .size()
		          << ", counter " << frontal_counter + 1 << std::endl << std::flush;
		if ( fill_one_tetra_ground < environ >
		     ( msh, interface, tag::mobile_vertex, Cell ( tag::non_existent ) ) .exists() )
		{	if ( frontal_counter >= frontal_counter_max )  break;
			continue;                                              }

		std::cout << "frontal-3d.cpp line 5161, seg_fill_membrane_3s size " << seg_fill_membrane_3s .size()
		          << ", counter " << frontal_counter + 1 << std::endl << std::flush;
		if ( fill_tri_membrane_3_sides < environ > ( msh, interface ) )
		{	if ( frontal_counter >= frontal_counter_max )  break;
			continue;                                              }

		std::cout << "frontal-3d.cpp line 5167, dangerous_cavities size " << dangerous_cavities .size()
		          << ", counter " << frontal_counter + 1 << std::endl << std::flush;
		if ( fill_dangerous_cavity < environ > ( msh, interface ) )  // a new vertex may be created
		{	if ( frontal_counter >= frontal_counter_max )  break;
			continue;                                              }

		std::cout << "frontal-3d.cpp line 5173, ver_fill_burried (two tetra) size "
					 << ver_fill_burried .size()
		          << ", counter " << frontal_counter + 1 << std::endl << std::flush;
		if ( fill_two_tetra_ground < environ > ( msh, interface ) )
		{	if ( frontal_counter >= frontal_counter_max )  break;
			continue;                                              }

		std::cout << "frontal-3d.cpp line 5180, seg_fill_crest size " << seg_fill_crest .size()
		          << ", counter " << frontal_counter + 1 << std::endl << std::flush;
		if ( fill_one_tetrahedron_crest < environ >
		     ( msh, interface, tag::mobile_vertex, Cell ( tag::non_existent ) ) .exists() )
		{	if ( frontal_counter >= frontal_counter_max )  break;
			continue;                                              }

		std::cout << "frontal-3d.cpp line 5187, ver_fill_cavity size " << ver_fill_cavity .size()
		          << ", counter " << frontal_counter + 1 << std::endl << std::flush;
		if ( fill_cavity_ground < environ > ( msh, interface ) )  // new vertex, reloc
		{	if ( frontal_counter >= frontal_counter_max )  break;
	  	continue;                                              }

		std::cout << "frontal-3d.cpp line 5193, seg_fill_membrane_2s size " << seg_fill_membrane_2s .size()
		          << ", counter " << frontal_counter + 1 << std::endl << std::flush;
		if ( fill_tri_membrane_2_sides < environ > ( msh, interface ) )
		{	if ( frontal_counter >= frontal_counter_max )  break;
			continue;                                              }

		std::cout << "frontal-3d.cpp line 5199, valley_tri_ver size " << valley_tri_ver .size();
		std::cout << ", counter " << frontal_counter + 1 << std::endl << std::flush;
		if ( fill_valley < environ > ( msh, interface ) )  // new vertex, reloc
		{	if ( frontal_counter >= frontal_counter_max )  break;
			continue;                                              }

		std::cout << "frontal-3d.cpp line 5205, seg_fill_two_crest size " << seg_fill_two_crest .size();
		std::cout << ", counter " << frontal_counter + 1 << std::endl << std::flush;
		if ( fill_two_tetrahedra_crest < environ > ( msh, interface ) )  // new vertex, reloc
		{	if ( frontal_counter >= frontal_counter_max )  break;
			continue;                                              }

		std::cout << "frontal-3d.cpp line 5211, counter " << frontal_counter + 1 << std::endl << std::flush;
		fill_tetra_out_of_the_blue < environ > ( msh, interface );  // new vertex, reloc
		if ( frontal_counter >= frontal_counter_max )  break;

		#else  // FRONTAL_VERBOSITY <= 1

		if ( fill_isolated_tetrahedron < environ >
		     ( msh, interface, tag::mobile_vertex, Cell ( tag::non_existent ) ) .exists() )  continue;

		if ( fill_one_tetra_ground < environ >
		     ( msh, interface, tag::mobile_vertex, Cell ( tag::non_existent ) ) .exists() )  continue;

		if ( fill_tri_membrane_3_sides < environ > ( msh, interface ) )  continue;

		if ( fill_dangerous_cavity < environ > ( msh, interface ) )  continue;
		// a new vertex may be created

		if ( fill_two_tetra_ground < environ > ( msh, interface ) )  continue;

		if ( fill_one_tetrahedron_crest < environ >
		     ( msh, interface, tag::mobile_vertex, Cell ( tag::non_existent ) ) .exists() )  continue;

		if ( fill_cavity_ground < environ > ( msh, interface ) )  continue;  // new vertex, reloc

		if ( fill_tri_membrane_2_sides < environ > ( msh, interface ) )  continue;

		if ( fill_valley < environ > ( msh, interface ) )  continue;  // new vertex, reloc

		if ( fill_two_tetrahedra_crest < environ > ( msh, interface ) )  continue;  // new vertex, reloc

		fill_tetra_out_of_the_blue < environ > ( msh, interface );  // new vertex, reloc
		
		#endif  // FRONTAL_VERBOSITY
		
	}  // end of  while ( interface .number_of ( tag::cells_of_max_dim ) > 0 )

	#if FRONTAL_VERBOSITY > 1
	std::cout << "frontal-3d.cpp line 4704, interface has "
	          << interface .number_of ( tag::cells_of_max_dim ) << " faces" << std::endl << std::flush;
	if ( frontal_counter < frontal_counter_max )
	{
	#endif  // FRONTAL_VERBOSITY > 1

	assert ( environ::manif_type::node_container .empty() );
	assert ( environ::metric_type::empty() );

	#ifndef NDEBUG  // DEBUG mode
	{ // just a block of code for hiding 'it'
	Mesh::Iterator it = msh .iterator ( tag::over_vertices );
	for ( it .reset(); it .in_range(); it++ )
	{	Cell ver = *it;
		assert ( ver .core->hook .find ( tag::node_in_cloud )  == ver .core->hook .end() );
		assert ( ver .core->hook .find ( tag::isr_inv_matrix ) == ver .core->hook .end() );
		assert ( ver .core->hook .find ( tag::isr_eigen_dir )  == ver .core->hook .end() );
		assert ( ver .core->hook .find ( tag::sqrt_det )       == ver .core->hook .end() );
		assert ( ver .core->hook .find ( tag::isr_radius )     == ver .core->hook .end() );  }
	} // just a block of code
	#endif  // DEBUG
	
	#if FRONTAL_VERBOSITY > 1
	}
	std::cout << "counter " << frontal_counter << std::endl << std::flush;

	// transform filaments in membranes before exporting
	{ // just a block of code for hiding 'it'
	Mesh::Iterator it = interface .iterator ( tag::over_segments );
	for ( it .reset(); it .in_range(); it ++ )
	{	if ( not (*it) .is_regular ( tag::within_STSI_mesh, interface ) )  continue;
		if ( make_filament_membrane < environ > ( *it, interface ) )
		{	std::cout << "transformed a filament in a membrane" << std::endl << std::flush;
			it .reset();                                               }               }
	} // just a block of code

	print_volumes < environ > ( msh );
	if	 ( frontal_counter < frontal_counter_max )
	for ( size_t i = 20; i < 20; i++ )  // no barycenters
	{	Mesh::Iterator it = msh .iterator ( tag::over_vertices );
		for ( it .reset(); it .in_range(); it ++ )
		{	Cell P = * it;
			if ( P .belongs_to ( bdry ) )  continue;
			msh .barycenter ( P, tag::inner_vertex, tag::mesh_has_high_dim );  }
		print_volumes < environ > ( msh );                                       }

	// membranes can be saved in an "msh" file and can be drawn by 'gmsh'
	// but maniFEM is unable to import the file back (takes the reverse cell)

	// for ( std::map < Cell, Cell > ::const_iterator
	//       it = dummy_faces .begin(); it != dummy_faces .end(); it ++ )
	// {	Cell tri = it->first;
	// 	Cell other_tri = it->second;
	// 	if ( other_tri .belongs_to ( interface, tag::same_dim ) )
	// 		tri .remove_from ( interface );                    }
	
	#endif  // FRONTAL_VERBOSITY > 1

	#ifndef NDEBUG  // DEBUG mode
	// assert there are no filaments
	{ // just a block of code for hiding 'it'
	Mesh::Iterator it = interface .iterator ( tag::over_segments );
	for ( it .reset(); it .in_range(); it ++ )
	{	if ( not (*it) .is_regular ( tag::within_STSI_mesh, interface ) )  continue;
		assert ( not break_filament ( *it, interface ) );                             }
	} // just a block of code
	#endif  // DEBUG

	interface .export_to_file ( tag::gmsh,"interf.msh");

	return;
	
} // end of  frontal_construct_3d

