
//   frontal.cpp  2025.01.16

//   This file is part of maniFEM, a C++ library for meshes and finite elements on manifolds.

//   Copyright  2019 - 2025  Cristian Barbarosie  cristian.barbarosie@gmail.com

//   https://maniFEM.rd.ciencias.ulisboa.pt/
//   https://codeberg.org/cristian.barbarosie/maniFEM

//   ManiFEM is free software: you can redistribute it and/or modify it
//   under the terms of the GNU Lesser General Public License as published
//   by the Free Software Foundation, either version 3 of the License
//   or (at your option) any later version.

//   ManiFEM is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty
//   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//   See the GNU Lesser General Public License for more details.

//   You should have received a copy of the GNU Lesser General Public License
//   along with maniFEM.  If not, see <https://www.gnu.org/licenses/>.


#ifndef MANIFEM_NO_FRONTAL

#define FRONTAL_VERBOSITY 0   // 1: warnings  2: count steps  4: approx_sqrt

#include "math.h"
#include <memory>
#include <random>

#include "maniFEM.h"
#include "manifold-xx.h"
#include "metric-tree.h"

namespace maniFEM {

namespace tag

{	struct OrthogonalTo { };  static const OrthogonalTo orthogonal_to;
	struct AtSeg { };  static const AtSeg at_seg;
	struct AtTri { };  static const AtTri at_tri;
	struct Domain2D { };  static const Domain2D domain_2d;
	struct Domain3D { };  static const Domain3D domain_3d;
	struct FirstTime { };  static const FirstTime first_time;
	struct MobileVertex { };  static const MobileVertex mobile_vertex;
	struct PreviousSeg { };  static const PreviousSeg previous_seg;
	struct SetCorrectSize { };  static const SetCorrectSize set_correct_size;
	struct TangentVec { };  static const TangentVec tangent_vec;
	struct NoRelocate { };  static const NoRelocate no_relocate;
	struct TakeOneNeighbIntoCons { };  static const TakeOneNeighbIntoCons take_one_neighb_into_cons;
	struct TakeTwoNeighbIntoCons { };  static const TakeTwoNeighbIntoCons take_two_neighb_into_cons;
	struct StartedAt { };  static const StartedAt started_at;
	struct BothDirections { };  static const BothDirections both_directions;
	struct OneDirection { };  static const OneDirection one_direction;
	static const bool force_other_faces = true;                                                       }

}  // end of  namespace maniFEM

using namespace maniFEM;

//------------------------------------------------------------------------------------------------------//


// we want to deal with Euclidian manifolds and implicit submanifolds, on one side
// quotient manifolds on the other side
// not easy to reach both goals while keeping some of the common code ... common

// for quotient manifolds, we use a dirty trick
// we provide as Point not just a Cell but a Cell together with a winding number
// each call to SqDist will set the "winning" winding of the second Point B
// relatively to the first one A
// (the winding number of a future segment AB where the minimum distance is achieved)

// we adapted metric-tree.h like
// 
// template < typename Point, typename SqDist,
//            typename RichPoint = Point, typename Enrich = DoNothing < Point >,
//            typename SqDistRaw = SqDist                                       >
// class MetricTree
// 
// where RichPoint contains a winding number while Point does not
// we implement two versions of SqDist :
//    SqDistRaw ( const Point & A, const Point & B )
//    SqDist    ( const Point & A,   RichPoint & B )
// the latter sets the winding of B to that winding which achieves the minimum distance


// there are three difficulties here, actually

// first, we must distinguish between a "usual" manifold and a quotient one
// a "usual" manifold may be e.g. Euclidian or implicit
// we introduce below two classes ManifoldNoWinding and ManifoldQuotient
// they will help us make this distinction
// they allow to have two types of cells (simple and rich, the latter having winding)
// they will appear as 'manif_type' in templates

// second, we must distinguish between different types of metric :
// trivial, isotropic (constant zoom, variable zoom), anisotropic
// virtual methods of the metric itself will allow us to treat differently these situations

// and finally, if the metric is Variable::Matrix or Variable::Matrix::ISR
// or scaled, having a base Variable::Matrix or Variable::Matrix::ISR,
// we want to use the isr_container, for other metrics no need of such

// there is an unexpected relation between the normal vectors and the isr_container
// if we are in pure 2D or pure 3D we build directly each normal vector using the exterior product
// (we do not use information on normals at neighbour segments)
// but, if the metric is anisotropic, we use  det A  A^-2  times the exterior product
// i.e.  sqrt det M  M^-1  times the exterior product
// and we use isr_container to propagate information about the inverse matrix
// and about the square root of the determinant

//------------------------------------------------------------------------------------------------------//


// global variables and functions for this file, not visible for other object files
namespace {  // anonymous namespace, mimics static linkage

Cell temporary_vertex ( tag::non_existent );
// used mainly for projecting vectors, for ensuring they are tangent to the manifold

size_t frontal_nb_of_coords;  // dimension of the surrounding Euclidian space
// also known as "geometric dimension"

// used for mesh generation with triangles (frontal_construct_2d line 1255) :

std::set < Cell > seg_fill_isolated;
// candidate segments for filling an isolated triangle
	
std::set < Cell > seg_fill_one_tri;
// candidate vertices for filling a new triangle just by joining
// the two neighbour vertices by a new segment (60 degree-angle, sort of)
// instead of a vertex, we keep a segment whose tip is the vertex
	
std::map < Cell, double > seg_fill_two_tri;
// candidate vertices for filling two new triangles (120 degree-angle, sort of)
// instead of a vertex, we keep a segment whose tip is the vertex
// double value associated is the inner product of two segments, around 0.5
// a value of 21 means "not yet computed"
	
// used for mesh generation with tetrahedra (line 4541) :

std::set < Cell > tri_fill_isolated;  // make list ?  vector ?
// candidate triangles for filling an isolated tetrahedron

std::set < Cell > ver_fill_burried;  // make list ?  vector ?
std::set < Cell > ver_fill_cavity;   // make list ?  vector ?
// candidate vertices for filling a hole in the ground

std::set < std::pair < Cell, Cell > > seg_fill_crest;  // tri, seg  // make list ?  vector ?
// candidate segments for filling a tetrahedron, like a crest of a mountain
	
std::list < std::pair < Cell, Cell > > seg_fill_two_crest;
std::vector < std::list < std::pair < Cell, Cell > > > seg_fill_two_crest_drawer (10);
// nb_concave_seg -> tri, seg
// candidate segments for filling two tetrahedra, like a crest of a mountain
// cleansed from time to time in 'fill_tri_membrane_2_sides'
	
std::set < Cell > seg_fill_membrane_3s;  // positive segments
// candidate segments for filling a triangular (double) membrane with three sides alrady existing

std::set < std::pair < Cell, Cell > > seg_fill_membrane_2s;  // make list ? vector ?
// candidate segments for filling a triangular (double) membrane with two sides alrady existing
// because these segments may be singular in interface, we keep pairs tri, seg
// 'seg' points towards the common vertex

// to do (but not important) :
// when inserting an element in seg_fill_membrane_2s,
// rather than inserting a pair < seg, tri >, insert a triplet < seg, tri1, tri2 >
// later, if any of tri1, tri2 does not belong any more to interf_copy,
// discard that segment

std::vector < std::pair < Cell, Cell > > dangerous_cavities;   // pairs (tri,seg)
// we implement this as a vector because it is faster to manipulate than a set
// we accept the disadvantage of duplicates showing up
// even if we used a set duplicate segments could appear, as sides of different triangles
// it would be very difficult to consistently eliminate duplicates
// think of a segment appearing on both sides of a wall of membranes

const double threshold_angle_for_valley = -1.33;  // 120 deg  
std::vector < std::pair < Cell, Cell > > valley_tri_ver;   // pairs (tri,ver)
// 'ver' is the "tip" of the valley, where the two walls meet

std::multimap < Cell, Cell > extrem_filam_dist_0;  // vertex -> positive segment
// to each extremity of a filament, associate the segment pointing away from it

std::multimap < Cell, Cell > extrem_filam_dist_1;  // vertex -> positive segment
// to each vertex near a filament, associate the segment pointing away from it


// used for both 2D (triangles) and 3D (tetrahedra) :
	
std::map < Cell, Cell > dummy_faces;
// 'build_bridge' and 'fill_tri_membrane_{2,3}_sides' produce a configuration of the interface
// which even a STSI mesh cannot handle
// so we build dummy faces, to be later replaced by correct ones
// something similar happens when building filaments in 'relocate_3d'
// but there we don't need to keep the pair

// average segment size is 1., below is a sort of upper limit
const double frontal_seg_size = 1.2;
const double frontal_seg_size_2 =	frontal_seg_size * frontal_seg_size;
	
#if FRONTAL_VERBOSITY > 1
int frontal_counter_max = 99999;
int frontal_counter;
#endif	

tag::Util::Metric trivial_metric ( tag::whose_core_is, new tag::Util::Metric::Trivial );

//------------------------------------------------------------------------------------------------------//

template < class Key, class Value >
inline void insert_or_replace ( std::map < Key, Value > & m, const Key & cll, const Value & v )
// hidden in anonymous namespace
// inspired in item 24 of the book : Scott Meyers, Effective STL

{	typename std::map < Key, Value > ::iterator lb = m .lower_bound ( cll );
	if ( ( lb == m .end() ) or m .key_comp() ( cll, lb->first ) )
		m .insert ( lb, { cll, v } );  // key not found, create pair
	else  // key found
		lb->second = v;                                                        }
	

//------------------------------------------------------------------------------------------------------//

	
inline void update_info_connected_one_dim ( const Mesh & msh, const Cell & start, const Cell & stop )
// hidden in anonymous namespace

// 'start' and 'stop' are positive vertices (may be one and the same)

// define this as a method of class Mesh !!

{	assert ( start .dim() == 0 );
	assert ( stop  .dim() == 0 );
	assert ( start .is_positive() );
	assert ( stop  .is_positive() );

	Mesh::Connected::OneDim * msh_core = tag::Util::assert_cast
		< Mesh::Core*, Mesh::Connected::OneDim* > ( msh .core );
	msh_core->first_ver = start;
	msh_core->last_ver  = stop;
	// now we can use an iterator

	Mesh::Iterator it = msh .iterator ( tag::over_segments );
	size_t n = 0;
	for ( it .reset(); it .in_range(); it++ )  n++;
	msh_core->nb_of_segs = n;                                       }
	
//------------------------------------------------------------------------------------------------------//


// for some anisotropic metrics, we need to store, for each vertex of the interface,
// the inner spectral radius, the inverse matrix, the eigenvector

// for tag::Util::Metric::Variable::Matrix::Simple we keep everything
// for tag::Util::Metric::Variable::Matrix::SquareRoot we keep everything
// for tag::Util::Metric::Variable::Matrix::ISR we only keep the radius
// for tag::Util::Metric::Variable::Matrix::SquareRoot::ISR there is no need to keep anything

// however, we may need to keep more information than described above
// if we want normals to be produced on-the-fly using the exterior product
// if the metric is anisotropic, we use  det A  A^-2  times the exterior product
// i.e.  sqrt det M  M^-1  times the exterior product
// and we use isr_container to propagate information about the inverse matrix
// and about the square root of the determinant
// virtual methods tag::Util::Metric::Core::codim_{0,1} change the metric's behaviour accordingly

struct ISRContainer  // hidden in anonymous namespace

{	class Inactive;  class Active;  };


class ISRContainer::Inactive

{	public :

	static inline void compute_data ( const tag::AtPoint &, const Cell & W )
	{	}

	static inline void compute_data
	( const tag::AtPoint &, const Cell & W, const tag::UseInformationFrom &, const Cell & V )
	{	}

	static inline void compute_data
	( const tag::Along &, const Mesh & interf, const tag::StartAt &, const Cell & V )
	{	}

	static inline void cond_remove ( const Cell & ver, const Mesh & interf )
	{	assert ( ver .dim() == 0 );  }

	static inline bool empty ( )
	{	return true;  }

	static inline double rescale ( const double & d )  // not used ?
	{	return d;  }

	static inline void codim_0 ( )
	{	}

	static inline void codim_1 ( )
	{	}

	static inline tag::Util::Metric registered_metric ( )
	// there is no registered metric in this ISRContainer, so we return the current working metric
	{	return Manifold::working .core->metric;  }

	static inline void de_activate ( )
	{	}

	static inline void correct_ext_prod_2d ( const Cell & P, std::vector < double > & n );
		
	static inline void correct_ext_prod_3d ( const Cell & P, std::vector < double > & n );
		
};  // end of  class ISRContainer::Inactive

	
class ISRContainer::Active

{	public :

	// the container has a metric associated with it
	// which, in the case of a scaled metric, will be different
	// from the one of Manifold::working

	static tag::Util::Metric metric;

	static inline void compute_data ( const tag::AtPoint &, const Cell & V )
	{	assert ( V .dim() == 0 );
		assert ( ISRContainer::Active::metric .core );
		ISRContainer::Active::metric .core->compute_data ( tag::at_point, V );  }

	static inline void compute_data
	( const tag::AtPoint &, const Cell & W, const tag::UseInformationFrom &, const Cell & V )
	{	assert ( V .dim() == 0 );
		assert ( ISRContainer::Active::metric .core );  // has been registered by register_active_metric
		ISRContainer::Active::metric .core->compute_data
			( tag::at_point, W, tag::use_information_from, V );  }
	
	static inline void compute_data
	( const tag::Along &, const Mesh & interf, const tag::StartAt &, const Cell & V )
	{	assert ( V .dim() == 0 );
		assert ( ISRContainer::Active::metric .core );  // has been registered by register_active_metric
		Cell seg = interf .cell_in_front_of ( V, tag::surely_exists );
		Cell A = seg .base() .reverse ( tag::surely_exists );
		while ( true )
		{	Cell B = seg .tip();
			if ( B == V ) break;
			ISRContainer::Active::metric .core->compute_data
				( tag::at_point, B, tag::use_information_from, A );
			seg = interf .cell_in_front_of ( B, tag::surely_exists );
			A = B;  B = seg .tip();                                   }     }

	// we have to use 'cond_remove' rather than 'remove' because 'interf' is a Mesh::STSI
	// and vertices are sometimes repeated, so we check if the vertex still belongs to 'interf'
	static inline void cond_remove ( const Cell & ver, const Mesh & interf )
	{	assert ( ver .dim() == 0 );
		assert ( ISRContainer::Active::metric .core );  // has been registered by register_active_metric
		if ( ver .belongs_to ( interf ) )  return;
		ISRContainer::Active::metric .core->remove_node_isr_p ( ver );  }

	static inline bool empty ( )
	// there is no affordable way to check all the hooks have been cleared
	{	return true;  }

	static inline void codim_0 ( )
	{	assert ( ISRContainer::Active::metric .core );  // has been registered by register_active_metric
		ISRContainer::Active::metric .core->codim_0();  }

	static inline void codim_1 ( )
	{	assert ( ISRContainer::Active::metric .core );  // has been registered by register_active_metric
		ISRContainer::Active::metric .core->codim_1();  }

	static inline tag::Util::Metric registered_metric ( )
	{	assert ( ISRContainer::Active::metric .core );  // has been registered by register_active_metric
		return ISRContainer::Active::metric;  }

	static inline void de_activate ( )
	// only called if metric is tag::Util::Metric::Variable::Matrix::SquareRoot::ISR
	{	assert ( ISRContainer::Active::metric .core );  // has been registered by register_active_metric
		ISRContainer::Active::metric .core->de_activate();  }

	static inline void correct_ext_prod_2d ( const Cell & P, std::vector < double > & n );
		
	static inline void correct_ext_prod_3d ( const Cell & P, std::vector < double > & n );
		
};  // end of  class ISRContainer::Active


tag::Util::Metric ISRContainer::Active::metric ( tag::whose_core_is, nullptr );
// non-existent metric

//------------------------------------------------------------------------------------------------------//


// 'register_active_metric' keeps (a copy of) the Variable::Matrix metric
// that will interact with the container

// usually,  Manifold::working .core->metric .core  is a ScaledSq(Inv) metric
// but then 'regist' goes down to the base until it finds a class derived from
// tag::Util::Metric::Variable::Matrix

inline void register_active_metric ( )  // hidden in anonymous namespace

{	ISRContainer::Active::metric =
		tag::Util::Metric ( tag::whose_core_is, Manifold::working .core->metric .core->regist() );  }

//------------------------------------------------------------------------------------------------------//


inline double compute_isr_loc   // hidden in anonymous namespace
( const tag::Util::Tensor < double > & matrix, tag::Util::Tensor < double > & inv_matr )

// apply Gauss-Seidel SOR to compute the inverse of 'matrix'
// using 'inv_matr' as initial guess (and updating it upon return)

// then apply the power method to 'inv_matr'
// starting simultaneously from several different vectors
// the first one to stabilize will be our eigenvector

{	assert ( matrix .dimensions .size() == 2 );
	size_t n = matrix .dimensions [0];
	assert ( n == matrix .dimensions [1] );

	tag::Util::improve_inverse_matrix ( matrix, inv_matr );

	// we take the canonical basis in RRn and add the eigen_candidate
	std::vector < std::vector < double > > starting_vectors = tag::Util::ortho_basis_double [n];
	assert ( starting_vectors .size() == n );
	// all these vectors have norm infty equal to 1.
	
	// apply power method to inv_matr
	std::vector < double > eigenval ( starting_vectors .size() );
	size_t d_good = tag::Util::power_method ( inv_matr, starting_vectors, eigenval );
	assert ( d_good < starting_vectors .size() );
				
	return  1. / eigenval [ d_good ];

}  // end of  compute_isr_loc


// create two versions of compute_isr_loc, eliminating argument candidate_meaningful !

inline double compute_isr_loc    // hidden in anonymous namespace
( const tag::Util::Tensor < double > & matrix, tag::Util::Tensor < double > & inv_matr,
  std::vector < double > & eigen_candidate, bool candidate_meaningful                  )

// apply Gauss-Seidel SOR to compute the inverse of 'matrix'
// using 'inv_matr' as initial guess (and updating it upon return)

// then apply the power method to 'inv_matr'
// starting simultaneously from several different vectors
// the first one to stabilize will be our eigenvector

// last argument tells whether eigen_candidate is meaningful as input
// if true, eigen_candidate has norm infty equal to 1. (will be updated upon return)
// if false, eigen_candidate is only meaningful upon return

{	assert ( matrix .dimensions .size() == 2 );
	size_t n = matrix .dimensions [0];
	assert ( n == matrix .dimensions [1] );

	tag::Util::improve_inverse_matrix ( matrix, inv_matr );

	// we take the canonical basis in RRn and add the eigen_candidate
	std::vector < std::vector < double > > starting_vectors = tag::Util::ortho_basis_double [n];
	if ( candidate_meaningful )
	{	starting_vectors .push_back ( eigen_candidate );
		assert ( starting_vectors .size() == n+1 );       }
	else  assert ( starting_vectors .size() == n );
	// all these vectors have norm infty equal to 1.

	// apply power method to inv_matr
	std::vector < double > eigenval ( starting_vectors .size() );
	size_t d_good = tag::Util::power_method ( inv_matr, starting_vectors, eigenval );
	assert ( d_good < starting_vectors .size() );
	eigen_candidate = starting_vectors [ d_good ];
				
	return  1. / eigenval [ d_good ];

}  // end of  compute_isr_loc  with candidate

//------------------------------------------------------------------------------------------------------//
//------------------------------------------------------------------------------------------------------//


// we need to store, for each face of the interface,
// a vector which is tangent to the manifold, orthogonal to the interface
// and oriented towards the region we want to mesh

// version of NormalContainer using an internal map : kept in attic/manifem.cpp

class NormalContainer  // hidden in anonymous namespace

{	public :

	inline NormalContainer ( ) { }

	inline std::vector < double > * attach_new_vector_to
	( const Cell & seg, const tag::SetCorrectSize & )
	{	assert ( seg .dim() == 1 );
		assert ( not has_vector ( seg ) );
		std::vector < double > * res = new std::vector < double > ( frontal_nb_of_coords );
		tag::Util::insert_new_elem_in_map ( seg .core->hook,
			{ tag::normal_vector, static_cast < void* > ( res ) } );
		return res;                                                                          }

	inline std::vector < double > * get_vector ( const Cell & seg )
	{	assert ( seg .dim() == 1 );
		std::map < tag::KeyForHook, void * > ::iterator it =
			seg .core->hook .find ( tag::normal_vector );
		assert ( it != seg .core->hook .end() );
		return static_cast < std::vector < double > * > ( it->second );  }
		
	inline void erase_vector ( const Cell & seg )
	{	assert ( seg .dim() == 1 );
		std::map < tag::KeyForHook, void * > ::iterator it =
			seg .core->hook .find ( tag::normal_vector );
		assert ( it != seg .core->hook .end() );
		delete ( static_cast < std::vector < double > * > ( it->second ) );
		seg .core->hook .erase ( it );                                       }

	inline bool has_vector ( const Cell & seg )
	{	assert ( seg .dim() == 1 );
		return seg .core->hook .find ( tag::normal_vector ) != seg .core->hook .end();  }

};  // end of  class NormalContainer

	
NormalContainer normal_container;  // hidden in anonymous namespace

//------------------------------------------------------------------------------------------------------//
//------------------------------------------------------------------------------------------------------//


// we need to store, for each vertex of the interface, a node in the cloud

template < class manif_type >
class NodeContainer  // hidden in anonymous namespace

// manif_type could be ManifoldNoWinding or ManifoldQuotient (defined below)
	
{	public :

	typename manif_type::metric_tree cloud;

	// a version of NodeContainer using an internal map is kept in attic/manifem.cpp
	// std::map < Cell, typename manif_type::metric_tree::Node * > internal_map;
	
	inline NodeContainer
	( typename manif_type::sq_dist sd, typename manif_type::sq_dist_raw sdr, double d, double r )
	: cloud ( sd, sdr , d, r )  //  d desired distance, r tree ratio
	{ }            

	inline void insert ( const Cell & ver )
	{	assert ( ver .dim() == 0 );
		assert ( not has_node ( ver ) );
		typename manif_type::metric_tree::Node * res = cloud .add ( ver );
		tag::Util::insert_new_elem_in_map ( ver .core->hook,
			{ tag::node_in_cloud, static_cast < void* > ( res ) } );          }

	inline typename manif_type::metric_tree::Node * get_node ( const Cell & ver )
	{	assert ( ver .dim() == 0 );
		std::map < tag::KeyForHook, void * > ::iterator it =
			ver .core->hook .find ( tag::node_in_cloud );
		assert ( it != ver .core->hook .end() );
		return static_cast < typename manif_type::metric_tree::Node * > ( it->second );  }

	inline void remove ( const Cell & ver )
	{	assert ( ver .dim() == 0 );
		std::map < tag::KeyForHook, void * > ::iterator it =
			ver .core->hook .find ( tag::node_in_cloud );
		assert ( it != ver .core->hook .end() );
		cloud .remove ( static_cast < typename manif_type::metric_tree::Node * > ( it->second ) );
		ver .core->hook .erase ( it );                                                             }

	// we have to use 'cond_remove' rather than 'remove' because 'interf' is a Mesh::STSI
	// and vertices are sometimes repeated, so we check if the vertex still belongs to 'interf'
	inline void cond_remove ( const Cell & ver, const Mesh & interf )
	{	assert ( ver .dim() == 0 );
		if ( ver .belongs_to ( interf ) )  return;
		remove ( ver );                             }

	inline bool has_node ( const Cell & ver )
	{	assert ( ver .dim() == 0 );
		return ver .core->hook .find ( tag::node_in_cloud ) != ver .core->hook .end();  }

	inline bool empty ( )
	{	return cloud .nb_of_nodes() == 0;  }

	inline std::list < typename manif_type::winding_cell > find_close_neighbours_of
	( const Cell & P, double d );

};  // end of  class NodeContainer

//------------------------------------------------------------------------------------------------------//


template < class manif_type >
inline std::list < typename manif_type::winding_cell >
NodeContainer < manif_type > ::find_close_neighbours_of ( const Cell & P, double d )

// manif_type could be ManifoldNoWinding or ManifoldQuotient (defined below)
	
// calling code wants a list of nodes which are closer to P than the threshold d
// measured in the (rescaled) metric of Manifold::working

// we must scale back d because the cloud works with a different metric (trivial Euclidian metric)
// so we use  d / inner_spectral_radius

{	// std::cout << "find_close_neighbours_of, d " << d << ", isr " << Manifold::working .core->metric .core->inner_spectral_radius (P) << ", d/isr " << d / Manifold::working .core->metric .core->inner_spectral_radius (P) << std::endl;
	return this->cloud .find_close_neighbours_of
		( P, d / Manifold::working .core->metric .core->inner_spectral_radius (P) );  }

//------------------------------------------------------------------------------------------------------//
//------------------------------------------------------------------------------------------------------//


inline double determinant ( tag::Util::Tensor < double > matr )

{	assert ( matr .dimensions .size() == 2 );
	size_t n = matr .dimensions [0];
	assert ( n == matr .dimensions [1] );

	for ( size_t i = 0; i < n; i++ )
	for ( size_t j = i+1; j < n; j++ )
	{	const double coef = matr (j,i) / matr (i,i);
		for ( size_t k = i; k < n; k++ )
			matr (j,k) -= coef * matr (i,k);            }

	double res = 1.;
	for ( size_t i = 0; i < n; i++ )  res *= matr (i,i);
	return res;                                           }
	

inline double approx_sqrt ( const double x )  // hidden in anonymous namespace

// a good approximation of the square root of x, for x between 0.25 and 4.

{
	if ( ( x <= 0.2 ) or ( x >= 5. ) )
	{
		#if FRONTAL_VERBOSITY > 3
		std::cout << "frontal.cpp line 574 approx_sqrt, x = " << x << std::endl;
		#endif
		return  std::sqrt (x);                                                    }
	constexpr double coef = 0.27;
	constexpr double coef1 = 0.5 - coef;
	constexpr double coef2 = 4.*coef;
	const double tmp = x + 1.;
	return coef1 * tmp + coef2 * x / tmp;                                        }


inline double approx_sqrt ( const double x, const tag::Around &, const double candidate_sqrt_x )
// hidden in anonymous namespace

{	return approx_sqrt ( x / candidate_sqrt_x / candidate_sqrt_x ) * candidate_sqrt_x;  }


inline void project_tangent  // hidden in anonymous namespace
( const Cell & A, const Cell & B, std::vector < double > & tangent )

// modifies 'tangent', sets coordinates of B
// often, B == temporary_vertex (from improve_tangent)
	
{	for ( size_t i = 0; i < frontal_nb_of_coords; i++ )
	{	const Function & x = Manifold::working .coordinates() [i];
		x(B) = x(A) + tangent [i];                                 }
	Manifold::working .project (B);
	for ( size_t i = 0; i < frontal_nb_of_coords; i++ )
	{	const Function & x = Manifold::working .coordinates() [i];
		tangent[i] = x(B) - x(A);                                  }  }

	
inline void normalize_vector  // hidden in anonymous namespace
( const Cell & A, std::vector < double > & v )
	
// ensure the norm is 1. (in the Riemannian metric)
// initially, the norm of 'v' may be far from 1.
	
{	const double n2 = Manifold::working .inner_prod ( A, v, v );
	// v may have norm far away from 1., so we use true square root below
	const double norm = std::sqrt ( n2 );
	for ( size_t i = 0; i < frontal_nb_of_coords; i++ )  v[i] /= norm;  }
		
	
inline void normalize_vector  // hidden in anonymous namespace
( const Cell & A, const Cell & B, std::vector < double > & v )
	
// ensure the norm is 1. (in the Riemannian metric)
// initially, the norm of 'v' may be far from 1.
// here we use two vertices for computing the norm
	
{	const double n2 = Manifold::working .sq_dist ( A, B, v );
	// v may have norm far away from 1., so we use true square root below
	const double norm = std::sqrt ( n2 );
	for ( size_t i = 0; i < frontal_nb_of_coords; i++ )  v[i] /= norm;  }
		
	
inline void normalize_vector_approx  // hidden in anonymous namespace
( const Cell & A, std::vector < double > & v )
	
// ensure the norm of 'v' is 1. (in the Riemannian metric)
// we assume the norm of 'v' is initially not far from 1.
	
{	const double n2 = Manifold::working .inner_prod ( A, v, v );
	const double norm = approx_sqrt ( n2 );
	for ( size_t i = 0; i < frontal_nb_of_coords; i++ )  v[i] /= norm;  }
		
	
inline void normalize_vector_approx  // hidden in anonymous namespace
( const Cell & A, const Cell & B, std::vector < double > & v )
	
// ensure the norm of 'v' is 1. (in the Riemannian metric)
// we assume the norm of 'v' is initially not far from 1.
// here we use two vertices for computing the norm
	
{	const double n2 = Manifold::working .sq_dist ( A, B, v );
	const double norm = approx_sqrt ( n2 );
	for ( size_t i = 0; i < frontal_nb_of_coords; i++ )  v[i] /= norm;  }


inline void normalize_vector_approx  // hidden in anonymous namespace
( const Cell & A, std::vector < double > & v, const tag::Around &, double r )
	
// ensure the norm of 'v' is 1. (in the Riemannian metric)
// we assume the norm of 'v' is initially close to r
	
{	const double n2 = Manifold::working .inner_prod ( A, v, v );
	const double norm = approx_sqrt ( n2, tag::around, r );
	for ( size_t i = 0; i < frontal_nb_of_coords; i++ )  v[i] /= norm;  }
		
	
inline void normalize_vector_approx  // hidden in anonymous namespace
( const Cell & A, const Cell & B, std::vector < double > & v, const tag::Around &, double r )
	
// ensure the norm of 'v' is 1. (in the Riemannian metric)
// we assume the norm of 'v' is initially close to r
// here we use two vertices for computing the norm
	
{	const double n2 = Manifold::working .sq_dist ( A, B, v );
	const double norm = approx_sqrt ( n2, tag::around, r );
	for ( size_t i = 0; i < frontal_nb_of_coords; i++ )  v[i] /= norm;  }


inline void make_orthogonal  // hidden in anonymous namespace
( const Cell & P, std::vector < double > & normal, const std::vector < double > & given_vec )

// make 'normal' orthogonal to a given vector
			
{	const double n2 = Manifold::working .inner_prod ( P, given_vec, given_vec );
	const double prod = Manifold::working .inner_prod ( P, normal, given_vec ) / n2;
	for ( size_t i = 0; i < frontal_nb_of_coords; i++ )
		normal [i] -= prod * given_vec [i];                                            }
	
	
inline void improve_tangent  // hidden in anonymous namespace
( const Cell & A, std::vector < double > & tangent )
	
// ensure the norm is 1. (in the Riemannian metric), project, ensure again the norm is 1.
// uses temporary_vertex

// in this version we do not assume the norm of 'tangent' is close to 1.
// also, 'tangent' may be far from the tangent line or tangent plane
	
{	normalize_vector ( A, tangent );
	project_tangent ( A, temporary_vertex, tangent );
	// statement above modifies 'tangent', sets coordinates of temp_ver
	normalize_vector ( A, temporary_vertex, tangent );  }
	
	
inline void improve_tangent_approx  // hidden in anonymous namespace
( const Cell & A, std::vector < double > & tangent )
	
// ensure the norm is 1. (in the Riemannian metric), project, ensure again the norm is 1.
// uses temporary_vertex

// in this version we assume the norm of 'tangent' is close to 1.
// also, 'tangent' is assumed to be almost tangent to the manifold
// this is why we use approx_sqrt
	
{	normalize_vector_approx ( A, tangent );
	project_tangent ( A, temporary_vertex, tangent );  }
	// statement above modifies 'tangent', sets coordinates of temp_ver

	// normalize_vector_approx ( A, temporary_vertex, tangent );  }
	// this last statement has been eliminated, it was too precise


inline Cell project_vertex_forward  // hidden in anonymous namespace
( const Cell & A, std::vector < double > & tangent )

// used in 'frontal_construct_1d'  // line 2497

// similar to 'improve_tangent'
// we assume the norm of 'tangent' is close to 1.
// we also assume it is nearly tangent to the working manifold
	
{ normalize_vector_approx ( A, tangent );
	Cell B ( tag::vertex );
	project_tangent ( A, B, tangent );  // modifies 'tangent', sets coordinates of B
	const double n2 = Manifold::working .sq_dist ( A, B, tangent );
	const double norm = approx_sqrt ( n2 );
	for ( size_t i = 0; i < frontal_nb_of_coords; i++ )
	{	const Function & x = Manifold::working .coordinates() [i];
		x(B) = x(A) + tangent [i] / norm;                           }
	Manifold::working .project (B);
	return B;                                                        }
	
//------------------------------------------------------------------------------------------------------//


inline double get_z_baryc ( const Cell & tri )
// hidden in anonymous namespace

{	assert ( frontal_nb_of_coords == 3 );
	Mesh::Iterator it = tri.boundary().iterator ( tag::over_vertices );
	Function z = Manifold::working.coordinates()[2];
	double zz = 0.;
	size_t counter = 0;
	for ( it .reset(); it .in_range(); it++, counter++ )  zz += z(*it);
	assert ( counter == 3 );
	return  zz/3.;                                                       }

//------------------------------------------------------------------------------------------------------//


inline double ext_prod_R2 ( const double vx, const double vy, const double wx, const double wy )
// hidden in anonymous namespace
{	return vx*wy - wx*vy;  }


inline bool opposite_signs ( const double a, const double b )
// hidden in anonymous namespace
{	if ( a < 0. )  return b >= 0.;
	if ( a == 0. )  return true;
	return b <= 0.;                 }

	
inline bool origin_outside ( const double & Ax, const double & Ay,
                             const double & Bx, const double & By,
                             const double & Cx, const double & Cy )
// hidden in anonymous namespace

{	return
	   opposite_signs ( - ext_prod_R2 ( Bx-Ax, By-Ay, Ax, Ay ),
	                      ext_prod_R2 ( Bx-Ax, By-Ay, Cx-Ax, Cy-Ay ) )
	or opposite_signs ( - ext_prod_R2 ( Cx-Bx, Cy-By, Bx, By ),
	                      ext_prod_R2 ( Cx-Bx, Cy-By, Ax-Bx, Ay-By ) )
	or opposite_signs ( - ext_prod_R2 ( Ax-Cx, Ay-Cy, Cx, Cy ),
	                      ext_prod_R2 ( Ax-Cx, Ay-Cy, Bx-Cx, By-Cy ) );  }


inline bool tri_correctly_oriented ( const Cell & tri )
// hidden in anonymous namespace

{	assert ( tri.dim() == 2 );

	Mesh::Iterator it = tri .boundary() .iterator
		( tag::over_segments, tag::orientation_compatible_with_mesh, tag::require_order );
	it.reset();  assert ( it .in_range() );
	Cell AB = *it;
	it++;  assert ( it .in_range() );
	Cell BC = *it;
	it++;  assert ( it .in_range() );
	// Cell CA = *it;
	it++;  assert ( not it .in_range() );

	Cell A = AB .base() .reverse ( tag::surely_exists );
	Cell B = AB .tip();
	assert ( B == BC .base() .reverse ( tag::surely_exists ) );
	Cell C = BC .tip();

	const Function & x = Manifold::working .coordinates() [0];
	const Function & y = Manifold::working .coordinates() [1];
	double  xAB = x(B) - x(A),  yAB = y(B) - y(A),
	        xBC = x(C) - x(B),  yBC = y(C) - y(B);
	return  xAB * yBC > yAB * xBC;;                                                         }

//------------------------------------------------------------------------------------------------------//


bool correctly_oriented    // hidden in anonymous namespace
( const Mesh msh )

// tells whether 'msh's orientation is consistent with the orientation of the
// surrounding Euclidian space

{	// Manifold::Implicit::OneEquation * m_impl = tag::Util::assert_cast
	// 	< Manifold::Core*, Manifold::Implicit::OneEquation* > ( Manifold::working .core );
  // Manifold::Euclid * m_euclid = tag::Util::assert_cast
	// 	< Manifold::Core*, Manifold::Euclid* > ( m_impl->surrounding_space .core );
	
	// for surfaces, we search the vertex with zmax and check the orientation
	// of all surrounding triangles (we need an iterator over cells around that vertex)
	if ( msh .dim() != 1 )
	{	assert ( msh .dim() == 2 );
		assert ( frontal_nb_of_coords == 3 );
		Mesh::Iterator it = msh .iterator
			( tag::over_cells_of_max_dim, tag::orientation_compatible_with_mesh );
		it .reset();  assert ( it .in_range() );
		Cell trimax = *it;
		double zmax = get_z_baryc ( trimax );
		for ( it++; it .in_range(); it++ )
		{	double zz = get_z_baryc ( *it );
			if ( zz > zmax )
			{	zmax = zz;  trimax = *it;  }   }
		return tri_correctly_oriented ( trimax );                                 }

	const Function & x = Manifold::working .coordinates() [0];
	const Function & y = Manifold::working .coordinates() [1];

	Mesh::Iterator it = msh .iterator ( tag::over_vertices );
	it .reset();  assert ( it .in_range() );
	Cell ver = *it;
	double ymax = y (ver);
	for ( it++; it .in_range(); it++ )
	{	Cell other_ver = *it;
		double other_y = y (other_ver);
		if ( other_y > ymax )
		{	ymax = other_y;  ver = other_ver;  }  }
	Cell prev_seg = msh .cell_behind ( ver );
	Cell next_seg = msh .cell_in_front_of ( ver );
	assert ( prev_seg .tip() == ver );
	assert ( next_seg .base() .reverse ( tag::surely_exists ) == ver );
	Cell A = prev_seg .base() .reverse ( tag::surely_exists );
	Cell C = next_seg .tip();
	bool prev_orient = ( x (ver) < x (A) );
	bool next_orient = ( x (C) < x (ver) );
	if ( prev_orient == next_orient )  return prev_orient;
	double  xAB = x(ver) - x(A),  yAB = y(ver) - y(A),
	        xBC = x(C) - x(ver),  yBC = y(C) - y(ver);
	return  xAB * yBC > yAB * xBC;
	
}  // end of correctly_oriented


// a more complicated version of correctly_oriented can be found at
// https://codeberg.org/cristian.barbarosie/attic - manifem.cpp

//------------------------------------------------------------------------------------------------------//


inline void switch_orientation_direct ( Mesh & msh )  // hidden in anonymous namespace

// reverse each cell
// invoked for a boundary of a 2D or 3D cell, from switch_orientation_of_each_cell
// or for a closed loop, freshly generated, from frontal_construct_1d  line 2161  line 2199
	
{	std::vector < Cell > vec_of_cells;
	vec_of_cells .reserve ( msh .number_of ( tag::cells_of_max_dim ) );
	Mesh::Iterator itt = msh .iterator
		( tag::over_cells_of_max_dim, tag::orientation_compatible_with_mesh );
	for ( itt .reset(); itt .in_range(); itt++ )  vec_of_cells .push_back ( *itt );
	for ( std::vector < Cell > ::iterator it = vec_of_cells .begin();
	      it != vec_of_cells .end(); it++                            )
		it->remove_from ( msh, tag::do_not_bother );
	for ( std::vector < Cell > ::iterator it = vec_of_cells .begin();
	      it != vec_of_cells .end(); it++                            )
		it->reverse() .add_to ( msh, tag::do_not_bother );                         }
// the meaning of tag::do_not_bother is explained at the end of paragraph 12.6 in the manual
// no need to call update_info_connected_one_dim because we are at hands with a closed loop
// nb_of_segs remains the same, as well as first_ver and last_ver

//------------------------------------------------------------------------------------------------------//


inline void switch_orientation_extremities ( Mesh & msh )  // hidden in anonymous namespace

// reverse each cell
// invoked for an open chain, freshly generated, from frontal_construct_1d  line 2199
	
{	std::vector < Cell > vec_of_cells;
	vec_of_cells .reserve ( msh .number_of ( tag::cells_of_max_dim ) );
	Mesh::Iterator itt = msh .iterator
		( tag::over_cells_of_max_dim, tag::orientation_compatible_with_mesh );
	for ( itt .reset(); itt .in_range(); itt++ )  vec_of_cells .push_back ( *itt );
	for ( std::vector < Cell > ::iterator it = vec_of_cells .begin();
	      it != vec_of_cells .end(); it++                            )
		it->remove_from ( msh, tag::do_not_bother );
	for ( std::vector < Cell > ::iterator it = vec_of_cells .begin();
	      it != vec_of_cells .end(); it++                            )
		it->reverse() .add_to ( msh, tag::do_not_bother );
	Mesh::Connected::OneDim * msh_p = tag::Util::assert_cast
		< Mesh::Core*, Mesh::Connected::OneDim* > ( msh .core );
	Cell tmp = msh_p->first_ver;  assert ( tmp .is_positive() );
	msh_p->first_ver = msh_p->last_ver;
	msh_p->last_ver = tmp;                                                          }
// the meaning of tag::do_not_bother is explained at the end of paragraph 12.6 in the manual
// no need to call update_info_connected_one_dim because we are at hands with a closed loop
// nb_of_segs remains the same, as well as first_ver and last_ver

//------------------------------------------------------------------------------------------------------//


void switch_orientation_of_each_cell ( Mesh & msh )  // hidden in anonymous namespace

// 'msh' is a closed mesh (has no boundary)
	
// since 'msh' has just been created, we choose not to reverse each cell of msh
// instead, we call switch_orientation_direct on boundary of each cell

// does not work for one-dimensional meshes
// because switch_orientation_direct does not work for segments

{	assert ( msh .dim() >= 2 );
	std::vector < Cell > vec_of_cells;
	vec_of_cells .reserve ( msh .number_of ( tag::cells_of_max_dim ) );
	Mesh::Iterator itt = msh .iterator
		( tag::over_cells_of_max_dim, tag::orientation_compatible_with_mesh );
	for ( itt .reset(); itt .in_range(); itt++ )  vec_of_cells .push_back ( *itt );
	for ( std::vector < Cell > ::iterator it = vec_of_cells .begin();
	      it != vec_of_cells .end(); it++                            )
		it->remove_from ( msh, tag::do_not_bother );
	for ( std::vector < Cell > ::iterator it = vec_of_cells .begin();
	      it != vec_of_cells .end(); it++                            )
	{	Mesh bdry = it->boundary();
		switch_orientation_direct ( bdry );  }
	for ( std::vector < Cell > ::iterator it = vec_of_cells .begin();
	      it != vec_of_cells .end(); it++                            )
		it->add_to ( msh, tag::do_not_bother );                                  }
// the meaning of tag::do_not_bother is explained at the end of paragraph 12.6 in the manual
// no need to call update_info_connected_one_dim because we are at hands with a closed loop
// nb_of_segs remains the same, as well as first_ver and last_ver

//------------------------------------------------------------------------------------------------------//


std::vector < double > compute_tangent_vec      // hidden in anonymous namespace
( const Cell & start, bool check_orth, std::vector < double > given_vec )
	
// computes a vector tangent to Manifold::working at point 'start'
// here the working manifold is not a quotient manifold

// if second argument is true, candidates will be projected onto the space orthogonal to given_vec
// given_vec must be tangent to Manifold::working at point 'start'
// and must have length (approximately) equal to 1.
	
{	// Manifold::Implicit * m_impl =  dynamic_cast<Manifold::Implicit*> ( Manifold::working.core );
	// assert ( m_impl );
	// Manifold::Euclid * m_euclid =
	// 	dynamic_cast<Manifold::Euclid*> ( m_impl->surrounding_space.core );
	// assert ( m_euclid );

	std::vector < double > best_tangent;
	assert ( Manifold::working .core );
	assert ( Manifold::working .core->metric .core );
	double longest_projection = 0.;
	// 'direc' contains 8 directions for 2D, 26 directions for 3D
	const std::vector < std::vector < double > > & direc =
		tag::Util::directions_double [ frontal_nb_of_coords ];
	const size_t n_dir = direc .size();
	for ( size_t dir = 0; dir < n_dir; dir++ )
	{	std::vector < double > tangent = direc [dir];
		double norm = std::sqrt ( Manifold::working .inner_prod ( start, tangent, tangent ) );
		for ( size_t i = 0; i < frontal_nb_of_coords; i++ )  tangent [i] /= norm;
		project_tangent ( start, temporary_vertex, tangent );  // modifies 'tangent'
		if ( check_orth ) make_orthogonal ( start, tangent, given_vec );
		// we choose the longest projection
		double n2 = Manifold::working .inner_prod ( start, tangent, tangent );
		if ( n2 > longest_projection )  // move tangent instead of copying !
		{	best_tangent = tangent;  longest_projection = n2;  }                 }

	// normalize best_tangent
	double norm = std::sqrt ( Manifold::working .inner_prod ( start, best_tangent, best_tangent ) );
	for ( size_t i = 0; i < frontal_nb_of_coords; i++ )  best_tangent [i] /= norm;
	return best_tangent;

}  // end of  compute_tangent_vec

//------------------------------------------------------------------------------------------------------//


inline std::vector < double > compute_tangent_vec  // hidden in anonymous namespace
( const tag::AtPoint &, Cell start, const tag::OrthogonalTo &, std::vector < double > given_vec )

// computes a vector tangent to Manifold::working at point 'start', normal to given_vec

// given_vec must be tangent to Manifold::working at point 'start'
// and must have length approximately equal to desired_length

{	return compute_tangent_vec ( start, true, given_vec );  }
	// 'true' as second argument means "do check orthogonality"
	

inline std::vector < double > compute_tangent_vec ( const tag::AtPoint &, Cell start )
// hidden in anonymous namespace

// computes a vector tangent to Manifold::working at point 'start'

{	return compute_tangent_vec ( start, false, std::vector<double>() );  }
	// 'false' as second argument means "do not check orthogonality"

//------------------------------------------------------------------------------------------------------//

	
template < class manif_type >
inline void build_one_normal      // hidden in anonymous namespace
( const Cell & P, const std::vector < double > & old_e, const std::vector < double > & old_f,
  const Cell & new_seg, std::vector < double > & new_f                                       )

// 'e' is the vector of the segment, 'f' is orthogonal
// they have all length approximately equal to 1.
// the computed normal is 'new_f'
// new_f is computed (but must have already the right size)
// old_e and old_f are not touched

// P is a vertex near new_seg (e.g. is one of its extremities)
	
// the 90 deg rotation transforming old_e into old_f can be expressed as
// R v = v - (ve+vf) e + (ve-vf) f
// note that this rotation happens in a plane included in RRn
// and the angle is 90 deg when measured by the Riemannian metric

// if the curvature and torsion of the implicit manifold are not too high,
// we may assume v belongs to the plane of the rotation (tangent to the manifold)
// thus  R v = ve f - vf e

// note that the interface may have sharp angles, thus new_e may be very different from old_e
	
// we then define new_f = R new_e

{	assert ( old_e .size() == frontal_nb_of_coords );
	assert ( old_f .size() == frontal_nb_of_coords );
	assert ( new_f .size() == frontal_nb_of_coords );
	assert ( new_seg .dim() == 1 );

	std::vector < double > new_e = manif_type::get_vector ( new_seg );
	// scalar products :
	const double prod_with_e = Manifold::working .inner_prod ( P, new_e, old_e );
	const double prod_with_f = Manifold::working .inner_prod ( P, new_e, old_f );
	// we rotate 'new_e' with 90 degrees,
	// in the same plane and in the same sense as 'f' is rotated from 'e'
	for ( size_t i = 0; i < frontal_nb_of_coords; i++ )
		new_f [i] = prod_with_e * old_f [i] - prod_with_f * old_e [i];

	// below, we should view new_f as based in the middle of new_seg
	// instead, we view it as based at P - it's an acceptable approximation
	// assuming that P is near new_seg (e.g. is one of its extremities)
	improve_tangent_approx ( P, new_f );  // uses temporary_vertex

}  // end of  build_one_normal
	
//------------------------------------------------------------------------------------------------------//


template < class manif_type, class metric_type >
inline void build_each_normal      // hidden in anonymous namespace
( const Cell & P, std::vector < double > & old_e, const std::vector < double > & old_f,
  const Cell & new_seg, std::vector < double > & new_f                                 )

// 'e' is the vector of the segment, 'f' is orthogonal
// they have all length approximately equal to 1.
// the computed normal is 'new_f'
// 'new_f' is computed (but must have already the right size)
// 'old_e' is updated, 'old_f' is not touched

// P is a vertex near new_seg (e.g. is one of its extremities)

// the 90 deg rotation transforming old_e into old_f can be expressed as
// R v = v - (ve+vf) e + (ve-vf) f
// note that this rotation happens in a plane included in RRn
// and the angle is 90 deg when measured by the Riemannian metric

// if the curvature and torsion of the implicit manifold are not too high,
// we may assume v belongs to the plane of the rotation (tangent to the manifold)
// thus  R v = ve f - vf e

// note that the interface may have sharp angles, thus new_e may be very different from old_e
	
// we then define new_f = R new_e

{	assert ( old_e .size() == frontal_nb_of_coords );
	assert ( old_f .size() == frontal_nb_of_coords );
	assert ( new_f .size() == frontal_nb_of_coords );
	assert ( new_seg .dim() == 1 );

	std::vector < double > new_e = manif_type::get_vector ( new_seg );
	// scalar products :
	const double prod_with_e = Manifold::working .inner_prod ( P, new_e, old_e );
	const double prod_with_f = Manifold::working .inner_prod ( P, new_e, old_f );
	// we rotate 'new_e' with 90 degrees,
	// in the same plane and in the same sense as 'f' is rotated from 'e'
	for ( size_t i = 0; i < frontal_nb_of_coords; i++ )
		new_f [i] = prod_with_e * old_f [i] - prod_with_f * old_e [i];
	old_e = new_e;

	// below, we should view new_f as based in the middle of new_seg
	// instead, we view it as based at P - it's an acceptable approximation
	// assuming that P is near new_seg (e.g. is one of its extremities)
	improve_tangent_approx ( P, new_f );  // uses temporary_vertex
	metric_type::correct_ext_prod_2d ( P, new_f );

}  // end of  build_each_normal
	
//------------------------------------------------------------------------------------------------------//


inline void build_component     // hidden in anonymous namespace
( const tag::Domain2D &, const Mesh & interf_orig, Mesh & interf_copy,
  const tag::StartAt &, const Cell & start                            )

// just walk along interf_orig and insert faces into interf_copy
// thus building a connected component of the interface
// this connected component must be a closed loop

{	assert ( start .dim() == 1 );
	assert ( start .belongs_to ( interf_orig, tag::same_dim, tag::orientation_compatible_with_mesh ) );

	Cell seg = start;
	#if FRONTAL_VERBOSITY > 1
	size_t counter = 0;
	#endif
	while ( true )
	// 'interf_orig' may be disconnected (Mesh::Fuzzy), so we cannot use Mesh::Iterators
	// this loop will only cover its current connected component
	{	seg .add_to ( interf_copy );
		seg_fill_isolated .insert ( seg );
		seg_fill_one_tri  .insert ( seg );
		seg_fill_two_tri  .insert ( { seg, 21.} );
		Cell B = seg .tip();
 		seg = interf_orig .cell_in_front_of ( B, tag::surely_exists );
		#if FRONTAL_VERBOSITY > 1
		counter ++;
		#endif
		if ( seg == start )
		{
			#if FRONTAL_VERBOSITY > 1
			std::cout << "interface has " << counter << " segments" << std::endl;
			#endif
			return;                                                               }   }                      }

//------------------------------------------------------------------------------------------------------//

	
inline void build_component     // hidden in anonymous namespace
( const tag::Domain3D &, const Mesh & interf_orig, Mesh & interf_copy,
  const tag::StartAt &, const Cell & start                            )

// at present, sweep the entire 'interf_orig' and insert faces into 'interf_copy'
	
// should be :
// just walk along 'interf_orig' and insert faces into 'interf_copy'
// thus building a connected component of the interface
// this connected component must be a closed loop

{	assert ( start .dim() == 2 );
	assert ( start .belongs_to ( interf_orig, tag::same_dim, tag::orientation_compatible_with_mesh ) );

	#if FRONTAL_VERBOSITY > 1
	size_t counter = 0;
	#endif
	{ // just a block of code for hiding 'it'
	Mesh::Iterator it = interf_orig .iterator
		( tag::over_cells_of_max_dim, tag::orientation_compatible_with_mesh );
	for ( it .reset(); it .in_range(); it++ )
	{	Cell tri = *it;
		tri .add_to ( interf_copy );
		#if FRONTAL_VERBOSITY > 1
		counter ++;
		#endif
		tri_fill_isolated .insert ( tri );  }
	} { // just a block of code for hiding 'it'
	Mesh::Iterator it = interf_orig .iterator ( tag::over_vertices );
	for ( it .reset(); it .in_range(); it++ )
	{	Cell P = *it;
		ver_fill_burried .insert (P);
		ver_fill_cavity  .insert (P);  }
	} { // just a block of code for hiding 'it'
	#if FRONTAL_VERBOSITY > 0
	std::cout << "we should only insert salient segments !" << std::endl;
	#endif
	Mesh::Iterator it = interf_orig .iterator ( tag::over_segments );
	for ( it .reset(); it .in_range(); it++ )
	{	Cell seg = *it;
		seg_fill_membrane_3s .insert ( seg .get_positive() );
		seg_fill_crest       .insert ( { interf_orig .cell_behind ( seg, tag::surely_exists ), seg } );
		seg_fill_membrane_2s .insert ( { interf_orig .cell_behind ( seg, tag::surely_exists ), seg } );
		seg_fill_membrane_2s .insert
			( { interf_orig .cell_in_front_of ( seg, tag::surely_exists ),
			    seg .reverse ( tag::surely_exists )                       } );
		seg_fill_two_crest .push_back ( { interf_orig .cell_behind ( seg, tag::surely_exists ), seg } );  }
	} // just a block of code for hiding 'it'
	
	#if FRONTAL_VERBOSITY > 1
	std::cout << "interface has " << counter << " cells" << std::endl;
	#endif
	
}  // end of  build_component domain3D
	
//------------------------------------------------------------------------------------------------------//

	
template < class manif_type, class metric_type >
inline void build_first_normal     // hidden in anonymous namespace
( const tag::AtPoint &, const Cell & start,
  const tag::NormalVector &, std::vector < double > & normal )

// used in  ExtProdCodimOne::compute_data  with  tag::first_time
  
{	assert ( start .dim() == 1 );

	Cell A = start .base() .reverse ( tag::surely_exists );
	std::vector < double > e = manif_type::get_vector ( start );
	make_orthogonal ( A, normal, e );
	improve_tangent ( A, normal );  // uses temporary_vertex
	metric_type::correct_ext_prod_2d ( A, normal );

	std::vector < double > * f =
		normal_container .attach_new_vector_to ( start, tag::set_correct_size );
	*f = normal;

}  // end of build_first_normal


template < class manif_type, class metric_type >
inline void build_normals     // hidden in anonymous namespace
( const tag::Along &, const Mesh & interf, const tag::StartAt &, const Cell & start )

// from a cell 'start', propagate normals along interf_orig
// (will only cover the connected component containing 'start')
// see paragraph 13.9 in the manual

// 'interf' is a Mesh::STSI made of segments

{	assert ( start .dim() == 1 );
	assert ( start .belongs_to ( interf, tag::same_dim, tag::orientation_compatible_with_mesh ) );

	Cell seg = start;
	Cell A = seg .base() .reverse ( tag::surely_exists );
	std::vector < double > e = manif_type::get_vector ( start );
	std::vector < double > * f = normal_container .get_vector ( start );

	while ( true )
	// 'interf' may be disconnected, so we cannot use Mesh::Iterators
	// this loop will only cover its current connected component
	{	Cell B = seg .tip();
 		seg = interf .cell_in_front_of ( B, tag::surely_exists );
		if ( normal_container .has_vector ( seg ) )
		{	assert ( seg == start );
			return;                   }
		std::vector < double > * new_f =
			normal_container .attach_new_vector_to ( seg, tag::set_correct_size );
		build_each_normal < manif_type, metric_type > ( B, e, *f, seg, *new_f );
		// in the above, *new_f is computed and 'e' is updated ('*f' untouched)
		f = new_f;                                                                 }

}  // end of build_normals
	
//------------------------------------------------------------------------------------------------------//
//------------------------------------------------------------------------------------------------------//


class PointerVector         // hidden in anonymous namespace

{	public :

	std::vector < double > * pointer;

	inline PointerVector ( std::vector < double > * const p )
	:	pointer {p}
	{	assert (p);  }
													
	inline double & operator[] ( size_t i )
	{	assert ( pointer );
		return (*pointer) [i];  }

};  // end of  class PointerVector
	

template < class manif, class metric, class ext_prod >
class Environment         // hidden in anonymous namespace

{	public :

	typedef manif manif_type;
	typedef metric metric_type;
	typedef ext_prod ext_prod_type;

};  // end of  class Environment
	
//------------------------------------------------------------------------------------------------------//
//------------------------------------------------------------------------------------------------------//


// classes ExtProd*** below provide a way to obtain a vector tangent to the manifold,
// orthogonal to the interface and with the correct orientation
// that is, pointing towards the region to be meshed

// for surfaces in RR3 (codim 1), we build each normal based on information 
// from a neighbour segment

// for pure 2D or 3D, we use exterior product plus information from metric
// by "exterior product" we mean a 90 deg rotation in 2D
// if isotropic metric, rescale the exterior product
// if anisotropic metric, use  det A  A^-2  times the exterior product
//                        i.e. sqrt det M  M^-1  times the exterior product

//------------------------------------------------------------------------------------------------------//


template < class manif_type, class metric_type >
class ExtProd2d   // pure 2D         // hidden in anonymous namespace
// simply rotate the vector by 90 deg then correct it if metric is anisotropic

{	public :

	typedef std::vector < double > pointer_vector;
	typedef void * vector_or_void;

	inline static double inner_prod
	( const Cell & P, const std::vector < double > & tau, const std::vector < double > & n )
	{	return  Manifold::working .inner_prod ( P, tau, n );  }

	static inline bool orientation_correct
	( const tag::Domain2D &, const tag::NormalVector &, const std::vector < double > & normal,
	  const tag::AtSeg &, const Cell & seg                                                    )
	{	assert ( frontal_nb_of_coords == 2 );
		std::vector < double > e = manif_type::get_vector ( seg );
		assert ( e .size() == 2 );
		assert ( normal .size() == 2 );
		return  e[0] * normal[1] > e[1] * normal[0];                  }

	static inline void * build_double_normal
	( const tag::AtSeg &, const Cell & seg, const Cell & seg_rev, const tag::SetCorrectSize &,
	  const tag::UseInformationFrom &, const Cell & P,
	  const std::vector < double > & e, const std::vector < double > & n                      )
	{	return nullptr;  }
	
	static inline void compute_data
	( const tag::Domain2D &, const tag::FirstTime &, const tag::AtSeg &, const Cell & start )
	{	assert ( start .dim() == 1 );  }
	// no need to compute anything, we just rotate the vector by 90 deg
	
	static inline void compute_data
	( const tag::Domain2D &, const tag::FirstTime &, const tag::AtSeg &, const Cell & start,
	  const tag::NormalVector &, const std::vector < double > & normal                      )
	{	assert ( start .dim() == 1 );  }
	// no need to compute anything, we just rotate the vector by 90 deg
	
	static inline void compute_data
	( const tag::Domain2D &, const tag::AtSeg &, const Cell & start,
	  const tag::UseInformationFrom &, const Cell & P,
	  const std::vector < double > & e, void * const & n            )
	{	assert ( start .dim() == 1 );
		assert ( P .dim() == 0 );      }
	// no need to compute anything, we just rotate the vector by 90 deg
	// 'n' being void *
	// is an indication that normals are not propagated, just computed later, when needed
	
	static inline void compute_data
	( const tag::Domain2D &, const tag::AtSeg &, const Cell & start,
	  const tag::UseInformationFrom &, const Cell & P,
	  const std::vector < double > & e, const std::vector < double > & n )
	{	assert ( start .dim() == 1 );
		assert ( P .dim() == 0 );      }
	// no need to compute anything, we just rotate the vector by 90 deg
	// 'n' being std::vector  [this comment does not make sense 2024.03.26]
	// is an indication that normals are not propagated, just computed later, when needed
		
	static inline void compute_data
	( const tag::Domain2D &, const tag::Along &, const Mesh & bdry,
	  const tag::StartAt &, const Cell & start                     )
	{	assert ( start .dim() == 1 );  }
	// no need to compute anything, we just rotate the vector by 90 deg
	
	static inline std::vector < double > get_normal_vector
	( const tag::Domain2D &, const tag::AtSeg &, const Cell & seg,
	  const tag::TangentVec &, const std::vector < double > & tau )
	// the interface is "looking outwards", towards the zone where we do not intend to build
	// triangles (either because it is not to be meshed or it has already been meshed)
	// we rotate 'tau' in such a way for the resulting normal to point inwards,
	// that is, towards the zone we intend to mesh
	{	assert ( frontal_nb_of_coords == 2 );
		assert ( tau .size() == 2 );
		std::vector < double > n (2);
		n[0] = - tau[1];
		n[1] =   tau[0];
		metric_type::correct_ext_prod_2d ( seg .tip(), n );
		return n;                                            }
	
	static inline std::vector < double > get_normal_vector
	( const tag::Domain2D &, const tag::AtSeg &, const Cell & seg )
	// the interface is "looking outwards", towards the zone where we do not intend to build
	// triangles (either because it is not to be meshed or it has already been meshed)
	// we rotate 'tau' in such a way for the resulting normal to point inwards,
	// that is, towards the zone we intend to mesh
	{	assert ( frontal_nb_of_coords == 2 );
		std::vector < double > tau = manif_type::get_vector ( seg );
		assert ( tau .size() == 2 );
		std::vector < double > n (2);
		n[0] = - tau[1];
		n[1] =   tau[0];
		metric_type::correct_ext_prod_2d ( seg .tip(), n );
		return n;                                                     }
	
	static void inline erase_normal_vector ( const tag::AtSeg &, const Cell & seg )
	{	assert ( seg .dim() == 1 );  }

};  // end of  class ExtProd2d

//------------------------------------------------------------------------------------------------------//


template < class manif_type, class metric_type >
class ExtProd2dRev   // 2D with reverse orientation       // hidden in anonymous namespace
// simply rotate the vector by 90 deg in the reverse direction
// then correct it if metric is anisotropic

{	public :

	typedef std::vector < double > pointer_vector;
	typedef void * vector_or_void;
	
	inline static double inner_prod
	( const Cell & P, const std::vector < double > & tau, const std::vector < double > & n )
	{	return  Manifold::working .inner_prod ( P, tau, n );  }

	static inline bool orientation_correct
	( const tag::Domain2D &, const tag::NormalVector &, const std::vector < double > & normal,
	  const tag::AtSeg &, const Cell & seg                                                    )
	{	assert ( frontal_nb_of_coords == 2 );
		std::vector < double > e = manif_type::get_vector ( seg );
		assert ( e .size() == 2 );
		assert ( normal .size() == 2 );
		return e[0] * normal[1] < e[1] * normal[0];                  }
			
	static inline void * build_double_normal
	( const tag::AtSeg &, const Cell & seg, const Cell & seg_rev, const tag::SetCorrectSize &,
	  const tag::UseInformationFrom &, const Cell & P,
	  const std::vector < double > & e, const std::vector < double > & n                      )
	{	return nullptr;  }
	
	static inline void compute_data
	( const tag::Domain2D &, const tag::FirstTime &, const tag::AtSeg &, const Cell & start )
	{	assert ( start .dim() == 1 );  }
	// no need to compute anything, we just rotate the vector by 90 deg rev
	
	static inline void compute_data
	( const tag::Domain2D &, const tag::FirstTime &, const tag::AtSeg &, const Cell & start,
	  const tag::NormalVector &, const std::vector < double > & normal                      )
	{	assert ( start .dim() == 1 );  }
	// no need to compute anything, we just rotate the vector by 90 deg rev
	
	static inline void compute_data
	( const tag::Domain2D &, const tag::AtSeg &, const Cell & start,
	  const tag::UseInformationFrom &, const Cell & P,
	  const std::vector < double > & e, void * const & n            )
	{	assert ( start .dim() == 1 );
		assert ( P .dim() == 0 );      }
	// no need to compute anything, we just rotate the vector by 90 deg rev
	// 'n' being void *
	// is an indication that normals are not propagated, just computed later, when needed
	
	static inline void compute_data
	( const tag::Domain2D &, const tag::AtSeg &, const Cell & start,
	  const tag::UseInformationFrom &, const Cell & P,
	  const std::vector < double > & e, const std::vector < double > & n )
	{	assert ( start .dim() == 1 );
		assert ( P .dim() == 0 );      }
	// no need to compute anything, we just rotate the vector by 90 deg rev
	// 'n' being std::vector  [this comment does not make sense 2024.03.26]
	// is an indication that normals are not propagated, just computed later, when needed

	static inline void compute_data
	( const tag::Domain2D &, const tag::Along &, const Mesh & bdry,
	  const tag::StartAt &, const Cell & start                     )
	{	assert ( start .dim() == 1 );  }
	// no need to compute anything, we just rotate the vector by 90 deg rev
	
	static inline std::vector < double > get_normal_vector
	( const tag::Domain2D &, const tag::AtSeg &, const Cell & seg,
	  const tag::TangentVec &, const std::vector < double > & tau )
	// the interface is "looking outwards", towards the zone where we do not intend to build
	// triangles (either because it is not to be meshed or it has already been meshed)
	// we rotate 'tau' in such a way for the resulting normal to point inwards,
	// that is, towards the zone we intend to mesh
	// here, the surrounding space RR2 has reverse orientation
	{	assert ( frontal_nb_of_coords == 2 );
		assert ( tau .size() == 2 );
		std::vector < double > n (2);
		n[0] =   tau[1];
		n[1] = - tau[0];
		metric_type::correct_ext_prod_2d ( seg .tip(), n );
		return n;                                            }
	
	static inline std::vector < double > get_normal_vector
	( const tag::Domain2D &, const tag::AtSeg &, const Cell & seg )
	// the interface is "looking outwards", towards the zone where we do not intend to build
	// triangles (either because it is not to be meshed or it has already been meshed)
	// we rotate 'tau' in such a way for the resulting normal to point inwards,
	// that is, towards the zone we intend to mesh
	// here, the surrounding space RR2 has reverse orientation
	{	assert ( frontal_nb_of_coords == 2 );
		std::vector < double > tau = manif_type::get_vector ( seg );
		assert ( tau .size() == 2 );
		std::vector < double > n (2);
		n[0] =   tau[1];
		n[1] = - tau[0];
		metric_type::correct_ext_prod_2d ( seg .tip(), n );
		return n;                                                    }
	
	static void inline erase_normal_vector ( const tag::AtSeg &, const Cell & seg )
	{	assert ( seg .dim() == 1 );  }

};  // end of  class ExtProd2dRev

//------------------------------------------------------------------------------------------------------//


template < class manif_type, class metric_type >
class ExtProdCodimOne        // hidden in anonymous namespace
// surface in 3D, we use neighbour normals

{	public :

	typedef PointerVector pointer_vector;
	typedef PointerVector vector_or_void;
	
	inline static double inner_prod
	( const Cell & P, const std::vector < double > & tau, const PointerVector & n )
	{	const std::vector < double > & nn = * n .pointer;
		return  Manifold::working .inner_prod ( P, tau, nn );  }

	static inline bool orientation_correct
	( const tag::Domain2D &, const tag::NormalVector &, const std::vector < double > & normal,
	  const tag::AtSeg &, const Cell & seg                                                    )
	// locally, there is no way to check the orientation
	{	return true;  }
			
	static inline PointerVector build_double_normal
	( const tag::AtSeg &, const Cell & QP, const Cell & PQ, const tag::SetCorrectSize &,
	  const tag::UseInformationFrom &, const Cell & P,
	  std::vector < double > & e, PointerVector n                                       )
	{	std::vector < double > * normal_QP =
			normal_container .attach_new_vector_to ( QP, tag::set_correct_size );
		const std::vector < double > & nn = * n .pointer;
		build_each_normal < manif_type, metric_type > ( P, e, nn, QP, * normal_QP );
		std::vector < double > * normal_PQ =
			normal_container .attach_new_vector_to ( PQ, tag::set_correct_size );
		for ( size_t i = 0; i < frontal_nb_of_coords; i++ )
			( * normal_PQ ) [i] = - ( * normal_QP ) [i];
		return PointerVector { normal_QP };                       }
	
	static inline void compute_data
	( const tag::Domain2D &, const tag::FirstTime &, const tag::AtSeg &, const Cell & start )
	{	assert ( false );	 }   // we cannot guess the orientation
	
	static inline void compute_data
	( const tag::Domain2D &, const tag::FirstTime &, const tag::AtSeg &, const Cell & start,
	  const tag::NormalVector &, std::vector < double > & normal                            )
	{	build_first_normal < manif_type, metric_type > ( tag::at_point, start, tag::normal_vect, normal );	}
	
	static inline void compute_data
	( const tag::Domain2D &, const tag::AtSeg &, const Cell & start,
	  const tag::UseInformationFrom &, const Cell & P,
	  const std::vector < double > & e, const PointerVector & n     )
	{	assert ( start .dim() == 1 );
		assert ( P .dim() == 0 );
		std::vector < double > & ns =
			* normal_container .attach_new_vector_to ( start, tag::set_correct_size );
		const std::vector < double > & nn = * n .pointer;
		build_one_normal < manif_type > ( P, e, nn, start, ns );                     }
	
	static inline void compute_data
	( const tag::Domain2D &, const tag::Along &, const Mesh & bdry,
	  const tag::StartAt &, const Cell & start                     )
	{	assert ( start .dim() == 1 );
		build_normals < manif_type, metric_type > ( tag::along, bdry, tag::start_at, start );  }
	
	static inline PointerVector get_normal_vector
	( const tag::Domain2D &, const tag::AtSeg &, const Cell & seg,
	  const tag::TangentVec &, const std::vector < double > & tau )
	{	assert ( frontal_nb_of_coords == 3 );
		assert ( tau .size() == 3 );
		return PointerVector { normal_container .get_vector ( seg ) };  }
	
	static inline PointerVector get_normal_vector
	( const tag::Domain2D &, const tag::AtSeg &, const Cell & seg )
	{	assert ( frontal_nb_of_coords == 3 );
		return PointerVector { normal_container .get_vector ( seg ) };  }

	static inline void erase_normal_vector ( const tag::AtSeg &, const Cell & seg )
	{	assert ( seg .dim() == 1 );
		normal_container .erase_vector ( seg );  }
	
};  // end of  class ExtProdCodimOne

//------------------------------------------------------------------------------------------------------//


template < class manif_type, class metric_type >
class ExtProd3d   // pure 3D         // hidden in anonymous namespace
// compute exterior product, then correct it if metric is anisotropic

{	public :

	typedef std::vector < double > pointer_vector;
	typedef void * vector_or_void;

	static inline double inner_prod
	( const Cell & P, const std::vector < double > & tau, const std::vector < double > & n )
	{	return  Manifold::working .inner_prod ( P, tau, n );  }

	static inline bool orientation_correct
	( const tag::Domain3D &, const tag::NormalVector &, const std::vector < double > & normal,
	  const tag::AtTri &, const Cell & tri                                                    )
	{	assert ( frontal_nb_of_coords == 3 );
		assert ( normal .size() == 3 );
		assert ( false );  // to implement
		return  true;                          }

	static inline std::vector < double > get_normal_vector
	( const tag::Domain3D &, const tag::AtTri &, const Cell & tri )
	// the interface is "looking outwards", towards the zone where we do not intend to build
	// tetrahedra (either because it is not to be meshed or it has already been meshed)
	// we reverse the exterior product for the resulting normal to point inwards,
	// that is, towards the zone we intend to mesh
	{	assert ( frontal_nb_of_coords == 3 );
		Mesh::Iterator it = tri .boundary() .iterator
			( tag::over_segments, tag::orientation_compatible_with_mesh, tag::require_order );
		it .reset();  assert ( it .in_range() );
		Cell AB = *it;
		it++;  assert ( it .in_range() );
		Cell BC = *it;
		#ifndef NDEBUG  // DEBUG mode
		it++;  assert ( it .in_range() );
		it++;  assert ( not it .in_range() );
		#endif
		std::vector < double > u = manif_type::get_vector ( AB );
		std::vector < double > v = manif_type::get_vector ( BC );
		assert ( u .size() == 3 );
		assert ( v .size() == 3 );
		std::vector < double > n { u[2]*v[1] - u[1]*v[2], u[0]*v[2] - u[2]*v[0], u[1]*v[0] - u[0]*v[1] };
		metric_type::correct_ext_prod_3d ( AB .tip(), n );
		return n;                                                                                          }
	
	static inline void erase_normal_vector ( const tag::AtSeg &, const Cell & seg )
	{	assert ( seg .dim() == 1 );  }

};  // end of  class ExtProd3d

//------------------------------------------------------------------------------------------------------//


template < class manif_type, class metric_type >
class ExtProd3dRev   // 3D with reverse orientation    // hidden in anonymous namespace
// compute exterior product, then correct it if metric is anisotropic

{	public :

	typedef std::vector < double > pointer_vector;
	typedef void * vector_or_void;
	
	inline static double inner_prod
	( const Cell & P, const std::vector < double > & tau, const std::vector < double > & n )
	{	return  Manifold::working .inner_prod ( P, tau, n );  }

	static inline bool orientation_correct
	( const tag::Domain3D &, const tag::NormalVector &, const std::vector < double > & normal,
	  const tag::AtTri &, const Cell & tri                                                    )
	{	assert ( frontal_nb_of_coords == 3 );
		assert ( normal .size() == 3 );
		assert ( false );  // to implement
		return true;                  }
			
	static inline std::vector < double > get_normal_vector
	( const tag::Domain3D &, const tag::AtTri &, const Cell & tri )
	// the interface is "looking outwards", towards the zone where we do not intend to build
	// tetrahedra (either because it is not to be meshed or it has already been meshed)
	// we reverse the exterior product for the resulting normal to point inwards,
	// that is, towards the zone we intend to mesh
	// here, the surrounding space RR3 has reverse orientation
	{	assert ( frontal_nb_of_coords == 3 );
		std::vector < double > n (3);
		assert ( false );  // to implement
		return n;                                                    }
	
	static void inline erase_normal_vector ( const tag::AtSeg &, const Cell & seg )
	{	assert ( seg .dim() == 1 );  }

};  // end of  class ExtProd3dRev

//------------------------------------------------------------------------------------------------------//
//------------------------------------------------------------------------------------------------------//


class ManifoldNoWinding  // hidden in anonymous namespace

// Euclidian manifold, implicit submanifold, parametric

{	public :

	class sq_dist;
	typedef sq_dist sq_dist_raw;
	class Enrich;   // same as add_winding ?
	typedef Cell winding_cell;
	typedef std::set < Cell > set_of_winding_cells;
	typedef std::set < Cell > type_of_walls;
	typedef std::vector < double > winding_vector;
	typedef MetricTree < Cell, sq_dist > metric_tree;
	typedef size_t winding;
	// 'winding' above is never used for this type of manifold
	// we could have defined it as 'void' but C++ does not accept variables of type void

	inline static bool has_winding ( )  {  return false;  }
	
	inline static const size_t substract_winding ( const size_t, const Cell & seg )
	{	assert ( seg .dim() == 1 );
		return 0;                    }

	inline static const size_t add_winding ( const size_t, const Cell & seg )
	{	assert ( seg .dim() == 1 );
		return 0;                    }

	inline static const Cell & add_winding ( const Cell & P, const size_t & w )
	{	assert ( P .dim() == 0 );
		return P;                  }    // same as Enrich ?

	inline static const Cell & add_winding_1 ( const Cell & P, const Cell & seg )
	{	assert ( P   .dim() == 0 );
		assert ( seg .dim() == 1 );
		return P;                    }

	inline static const Cell & add_winding_1
	( const Cell & P, const Cell & seg, const tag::From &, const Cell & orig )
	{	assert ( P   .dim() == 0 );
		assert ( seg .dim() == 1 );
		return P;                   }

	inline static const Cell & add_winding_neg_1 ( const Cell & P, const Cell & seg )
	{	assert ( P   .dim() == 0 );
		assert ( seg .dim() == 1 );
		return P;                    }

	inline static const Cell & add_winding_neg_1
	( const Cell & P, const Cell & seg, const tag::From &, const Cell & orig )
	{	assert ( P   .dim() == 0 );
		assert ( seg .dim() == 1 );
		return P;                    }

	inline static const Cell & add_winding_2 ( const Cell & P, const Cell & seg1, const Cell & seg2 )
	{	assert ( P    .dim() == 0 );
		assert ( seg1 .dim() == 1 );
		assert ( seg2 .dim() == 1 );
		return P;                     }

	inline static const Cell & add_winding_3
	( const Cell & P, const Cell & seg1, const Cell & seg2, const Cell & seg3 )
	{	assert ( P    .dim() == 0 );
		assert ( seg1 .dim() == 1 );
		assert ( seg2 .dim() == 1 );
		assert ( seg3 .dim() == 1 );
		return P;                     }

	inline static const Cell & remove_winding ( const Cell & P )
	{ assert ( P .dim() == 0 );
		return P;                  }

	inline static const std::vector < double > & remove_winding ( const std::vector < double > & v )
	{	return v;  }

	inline static std::vector < double > get_vector ( const Cell & seg );
	inline static std::vector < double > get_vector ( const Cell & A, const Cell & B );
	
	inline static size_t get_winding ( const Cell & seg )
	{	assert ( seg .dim() == 1 );
		return 0;                    }

	inline static Cell build_seg ( const Cell & A, const Cell & B )
	{	return Cell ( tag::segment, A, B );  }  // A is negative

	inline static void set_winding ( const Cell & seg, const Cell & V )
	{	assert ( seg .dim() == 1 );
		assert ( V   .dim() == 0 );  }

	inline static void set_winding_neg ( const Cell & seg, const Cell & V )
	{	assert ( seg .dim() == 1 );
		assert ( V   .dim() == 0 );  }

	inline static void set_winding_1 ( const Cell & seg, const Cell & seg1 )
	{	assert ( seg  .dim() == 1 );
		assert ( seg1 .dim() == 1 );  }

	inline static void set_winding_2 ( const Cell & seg, const Cell & seg1, const Cell & seg2 )
	{	assert ( seg  .dim() == 1 );
		assert ( seg1 .dim() == 1 );
		assert ( seg2 .dim() == 1 );  }

	inline static void set_winding_3
	( const Cell & seg, const Cell & seg1, const Cell & seg2, const Cell & seg3 )
	{	assert ( seg  .dim() == 1 );
		assert ( seg1 .dim() == 1 );
		assert ( seg2 .dim() == 1 );
		assert ( seg3 .dim() == 1 );  }

	inline static void set_winding_neg_1 ( const Cell & seg, const Cell & seg1 )
	{	assert ( seg  .dim() == 1 );
		assert ( seg1 .dim() == 1 );  }

	inline static void set_winding_neg_2 ( const Cell & seg, const Cell & seg1, const Cell & seg2 )
	{	assert ( seg  .dim() == 1 );
		assert ( seg1 .dim() == 1 );
		assert ( seg2 .dim() == 1 );  }

	inline static void set_winding_diff ( const Cell & seg, const Cell & seg1, const Cell & seg2 )
	{	assert ( seg  .dim() == 1 );
		assert ( seg1 .dim() == 1 );
		assert ( seg2 .dim() == 1 );  }

	inline static void set_winding_neg_3
	( const Cell & seg, const Cell & seg1, const Cell & seg2, const Cell & seg3 )
	{	assert ( seg  .dim() == 1 );
		assert ( seg1 .dim() == 1 );
		assert ( seg2 .dim() == 1 );
		assert ( seg3 .dim() == 1 );  }

	inline static bool windings_match_1 ( const Cell & A, const Cell & AB, const Cell & B )
	{	assert ( A  .dim() == 0 );
		assert ( AB .dim() == 1 );
		assert ( B  .dim() == 0 );
		return true;                }
		
	inline static bool same_cell ( const Cell & A, const Cell & B )
	{	return A == B;  }
			
	inline static void insert_wall
	( type_of_walls & walls, const Cell & tri, const winding_cell & P )
	{	walls .insert ( tri );  }

	inline static Cell get_tri_from_wall ( const type_of_walls::const_iterator & it )
	{	return * it;  }
	
	inline static std::vector < double > get_vector_from_wall
	( const Cell & P, const type_of_walls::const_iterator & it )
	{	Cell tri = * it;
		Mesh::Iterator iter = tri .boundary() .iterator ( tag::over_vertices );
		iter .reset();
		assert ( iter .in_range() );
		Cell V = * iter;
		return get_vector ( P, V );                                              }

	inline static Cell get_first ( const std::set < Cell > ::const_iterator it )
	{	return * it;  }
	
	inline static void redistribute_vertices
	( const Mesh & msh, const Cell & start, const Cell & stop, size_t n );

	inline static std::string info ( )
	{	return "no-winding";  }

	static NodeContainer < ManifoldNoWinding > node_container;
		
};  // end of  class ManifoldNoWinding
	
//------------------------------------------------------------------------------------------------------//


inline std::vector < double > ManifoldNoWinding::get_vector ( const Cell & seg )
// static
{	return ManifoldNoWinding::get_vector ( seg .base() .reverse ( tag::surely_exists ), seg .tip() );  }
			

inline std::vector < double > ManifoldNoWinding::get_vector ( const Cell & A, const Cell & B )
// static
	
{	const Function & coord = Manifold::working .coordinates();
	assert ( coord .number_of ( tag::components ) == frontal_nb_of_coords );
	std::vector < double > res ( frontal_nb_of_coords );
	for ( size_t i = 0; i < frontal_nb_of_coords; i++ )
	{	const Function & x = coord [i];
		res [i] = x(B) - x(A);          }
	return res;                                                    }
			
//------------------------------------------------------------------------------------------------------//

	
class ManifoldNoWinding::sq_dist

// a callable object returning the square of the distance between two points
// used for MetricTree, see paragraphs 13.14 and 13.15 in the manual
 
{	public :

	static inline double working_manif ( const Cell & A, const Cell & B )
	{	return Manifold::working .sq_dist  ( A, B );  }  // rescaled metric, not to be used by the cloud

	inline double operator() ( const Cell & A, const Cell & B )  // Euclidian distance, used by the cloud
	{	return trivial_metric .core->sq_dist ( A, B, ManifoldNoWinding::get_vector ( A, B ) );  }
		
};  // end of  class ManifoldNoWinding::sq_dist

//------------------------------------------------------------------------------------------------------//


class ManifoldNoWinding::Enrich
{	public:
	static inline Cell & enrich ( Cell & P )
	{	return P; }
	static inline const Cell & enrich ( const Cell & P )
	{	return P; }
};  // end of  class ManifoldNoWinding::Enrich

//------------------------------------------------------------------------------------------------------//
//------------------------------------------------------------------------------------------------------//


#ifndef MANIFEM_NO_QUOTIENT

class ManifoldQuotient  // hidden in anonymous namespace

{	public :

	class sq_dist;	
	class sq_dist_raw;	
	class Enrich;     // same as add_winding ?
	typedef std::pair < Cell, Manifold::Action > winding_cell;
	typedef std::map < Cell, Manifold::Action > set_of_winding_cells;
	typedef std::map < Cell, winding_cell > type_of_walls;
	typedef std::pair < std::vector < double >, Manifold::Action > winding_vector;
	typedef MetricTree < Cell, sq_dist, winding_cell, Enrich, sq_dist_raw > metric_tree;
	typedef Manifold::Action winding;

	inline static bool has_winding ( )  {  return true;  }
	
	inline static const Manifold::Action add_winding ( const Manifold::Action & w, const Cell & seg )
	{	assert ( seg .dim() == 1 );
		return w + seg .winding();   }

	inline static const Manifold::Action substract_winding ( const Manifold::Action & w, const Cell & seg )
	{	assert ( seg .dim() == 1 );
		return w - seg .winding();   }

	inline static const winding_cell add_winding ( const Cell & P, const Manifold::Action & w )
	{	assert ( P .dim() == 0 );
		return { P, w };           }   // same as Enrich ?

	inline static const winding_cell add_winding_1 ( const Cell & P, const Cell & seg )
	{	assert ( P   .dim() == 0 );
		assert ( seg .dim() == 1 );
		return { P, seg .winding() };  }

	inline static const winding_cell add_winding_1
	( const Cell & P, const Cell & seg, const tag::From &, const winding_cell & orig )
	{	assert ( P .dim() == 0 );
		assert ( orig .first .dim() == 0 );
		assert ( seg .dim() == 1 );
		return { P, orig .second + seg .winding() };  }

	inline static const winding_cell add_winding_neg_1 ( const Cell & P, const Cell & seg )
	{	assert ( P   .dim() == 0 );
		assert ( seg .dim() == 1 );
		return { P, - seg .winding() };  }

	inline static const winding_cell add_winding_neg_1
	( const Cell & P, const Cell & seg, const tag::From &, const winding_cell & orig )
	{	assert ( P   .dim() == 0 );
		assert ( seg .dim() == 1 );
		return { P, orig .second - seg .winding() };  }

	inline static const winding_cell add_winding_2 ( const Cell & P, const Cell & seg1, const Cell & seg2 )
	{	assert ( P    .dim() == 0 );
		assert ( seg1 .dim() == 1 );
		assert ( seg2 .dim() == 1 );
		return { P, seg1 .winding() + seg2 .winding() };  }

	inline static const winding_cell add_winding_3
	( const Cell & P, const Cell & seg1, const Cell & seg2, const Cell & seg3 )
	{	assert ( P    .dim() == 0 );
		assert ( seg1 .dim() == 1 );
		assert ( seg2 .dim() == 1 );
		assert ( seg3 .dim() == 1 );
		return { P, seg1 .winding() + seg2 .winding() + seg3 .winding() };  }

	inline static const Cell & remove_winding ( const winding_cell & P )
	{	return P .first;  }

	inline static const std::vector < double > & remove_winding ( const winding_vector & v )
	{	return v .first;  }

	inline static std::vector < double > get_vector ( const Cell & seg );
	inline static std::vector < double > get_vector ( const Cell & A, const Cell & B );
	inline static std::vector < double > get_vector ( const Cell & A, const winding_cell & B );
	inline static std::vector < double > get_vector ( const winding_cell & A, const winding_cell & B );
			
	inline static Manifold::Action get_winding ( const Cell & seg )
	{	assert ( seg .dim() == 1 );
		return seg .winding();       }

	inline static Cell build_seg ( const Cell & A, const winding_cell & B )
	{	Cell seg ( tag::segment, A, B .first );   // A is negative
		seg .winding() = B .second;
		return seg;                              }

	inline static void set_winding ( const Cell & seg, const std::pair < Cell, Manifold::Action > & VV )
	{	assert ( seg  .dim() == 1 );
		assert ( VV .first .dim() == 0 );
		seg .winding() = VV .second;       }

	inline static void set_winding_neg
	( const Cell & seg, const std::pair < Cell, Manifold::Action > & VV )
	{	assert ( seg  .dim() == 1 );
		assert ( VV .first .dim() == 0 );
		seg .winding() = - VV .second;     }

	inline static void set_winding_1 ( const Cell & seg, const Cell & seg1 )
	{	assert ( seg  .dim() == 1 );
		assert ( seg1 .dim() == 1 );
		seg .winding() = seg1 .winding();  }

	inline static void set_winding_2 ( const Cell & seg, const Cell & seg1, const Cell & seg2 )
	{	assert ( seg  .dim() == 1 );
		assert ( seg1 .dim() == 1 );
		assert ( seg2 .dim() == 1 );
		seg .winding() = seg1 .winding() + seg2 .winding();  }

	inline static void set_winding_3
	( const Cell & seg, const Cell & seg1, const Cell & seg2, const Cell & seg3 )
	{	assert ( seg  .dim() == 1 );
		assert ( seg1 .dim() == 1 );
		assert ( seg2 .dim() == 1 );
		assert ( seg3 .dim() == 1 );
		seg .winding() = seg1 .winding() + seg2 .winding() + seg3 .winding();  }

	inline static void set_winding_neg_1 ( const Cell & seg, const Cell & seg1 )
	{	assert ( seg  .dim() == 1 );
		assert ( seg1 .dim() == 1 );
		seg .winding() = - seg1 .winding();  }

	inline static void set_winding_neg_2 ( const Cell & seg, const Cell & seg1, const Cell & seg2 )
	{	assert ( seg  .dim() == 1 );
		assert ( seg1 .dim() == 1 );
		assert ( seg2 .dim() == 1 );
		seg .winding() = - seg1 .winding() - seg2 .winding();  }

	inline static void set_winding_diff ( const Cell & seg, const Cell & seg1, const Cell & seg2 )
	{	assert ( seg  .dim() == 1 );
		assert ( seg1 .dim() == 1 );
		assert ( seg2 .dim() == 1 );
		seg .winding() = seg1 .winding() - seg2 .winding();  }

	inline static void set_winding_neg_3
	( const Cell & seg, const Cell & seg1, const Cell & seg2, const Cell & seg3 )
	{	assert ( seg  .dim() == 1 );
		assert ( seg1 .dim() == 1 );
		assert ( seg2 .dim() == 1 );
		assert ( seg3 .dim() == 1 );
		seg .winding() = - seg1 .winding() - seg2 .winding() - seg3 .winding();  }

	inline static bool windings_match_1
	( const winding_cell & AA, const Cell & AB, const winding_cell & BB )
	{	assert ( AA .first .dim() == 0 );
		assert ( AB .dim() == 1 );
		assert ( BB .first .dim() == 0 );
		return  AA .second + AB .winding() == BB .second;  }

	inline static bool same_cell ( const winding_cell & A, const winding_cell & B )
	{	if ( A .first == B .first )
		{	assert ( A .second == B .second );
			return true;                        }
		return false;                            }

	inline static void insert_wall
	( type_of_walls & walls, const Cell & tri, const winding_cell & P )
	{	walls .insert ( { tri, P } );  }
	
	inline static Cell get_tri_from_wall ( const type_of_walls::const_iterator & it )
	{	return it->first;  }
	
	inline static std::vector < double > get_vector_from_wall
	( const Cell & P, const type_of_walls::const_iterator & it )
	{	winding_cell VV = it->second;
		return get_vector ( P, VV );            }
	
	inline static Cell get_first ( const type_of_walls::const_iterator & it )
	{	return it->first;  }
	
	inline static void redistribute_vertices
	( const Mesh & msh, const Cell & start,
	  const std::pair < Cell, Manifold::Action > & stop, size_t n );
		
	inline static std::string info ( )
	{	return "quotient";  }
		
	static NodeContainer < ManifoldQuotient > node_container;
		
};  // end of  class ManifoldQuotient

// we only need the winding for searching close neighbours of a given vertex
// we need 'sq_dist' to keep the "winning" winding, see below
	

//------------------------------------------------------------------------------------------------------//


inline std::vector < double > ManifoldQuotient::get_vector ( const Cell & seg )  // static
{	return  ManifoldQuotient::get_vector
		( seg .base() .reverse ( tag::surely_exists ), { seg .tip(), seg .winding() } );  }


inline std::vector < double > ManifoldQuotient::get_vector  // static
( const Cell & A, const Cell & B )
	
{	return ManifoldNoWinding::get_vector ( A, B );  }


inline std::vector < double > ManifoldQuotient::get_vector  // static
( const Cell & A, const ManifoldQuotient::winding_cell & B )
	
{	Manifold space = Manifold::working;
	assert ( space .exists() );  // we use the current (quotient) manifold
	std::shared_ptr < Manifold::Quotient > manif_q = tag::Util::assert_shared_cast
		< Manifold::Core, Manifold::Quotient > ( space .core );
	const Function & coords_q = space .coordinates();
	Manifold mani_Eu = manif_q->base_space;  // underlying Euclidian manifold
	const Function & coords_Eu = mani_Eu .coordinates();
	assert ( coords_q  .number_of ( tag::components ) == frontal_nb_of_coords );
	assert ( coords_Eu .number_of ( tag::components ) == frontal_nb_of_coords );
	std::vector < double > e = coords_q ( B .first, tag::winding, B .second );
	for ( size_t i = 0; i < frontal_nb_of_coords; i++ )
		e[i] -= coords_Eu [i] (A);  // zero winding
	return e;                                                                   }
			

inline std::vector < double > ManifoldQuotient::get_vector  // static
( const ManifoldQuotient::winding_cell & A, const ManifoldQuotient::winding_cell & B )
	
{	Manifold space = Manifold::working;
	assert ( space .exists() );  // we use the current (quotient) manifold
	std::shared_ptr < Manifold::Quotient > manif_q = tag::Util::assert_shared_cast
		< Manifold::Core, Manifold::Quotient > ( space .core );
	const Function & coords_q = space.coordinates();
	Manifold mani_Eu = manif_q->base_space;  // underlying Euclidian manifold
	const Function & coords_Eu = mani_Eu .coordinates();
	assert ( coords_q  .number_of ( tag::components ) == frontal_nb_of_coords );
	assert ( coords_Eu .number_of ( tag::components ) == frontal_nb_of_coords );
	std::vector < double > e_A = coords_q ( A .first, tag::winding, A .second );
	std::vector < double > e_B = coords_q ( B .first, tag::winding, B .second );
	for ( size_t i = 0; i < frontal_nb_of_coords; i++ )  e_B [i] -= e_A [i];
	return e_B;                                                                   }
			
//------------------------------------------------------------------------------------------------------//

	
class ManifoldQuotient::sq_dist

// a callable object returning the square of the distance between two points
// used for MetricTree, see paragraphs 13.14 and 13.15 in the manual
// first argument is Cell, second argument is winding Cell

{	public :

	static inline double working_manif  // rescaled metric, not to be used by the cloud
	( const Cell & A, const std::pair < Cell, Manifold::Action > & B );

	static inline double working_manif  // rescaled metric, not to be used by the cloud
	( const std::pair < Cell, Manifold::Action > & A, const std::pair < Cell, Manifold::Action > & B );

	inline double dist2_internal  // invoked by  operator(),  used by the cloud
	( const Cell & A, const Cell & B, const Manifold::Action & s );
	
	// we provide as second Point not just a Cell but a Cell together with a winding number
	// each call to SqDist will set the "winning" winding of the second Point B
	// relatively to the first one A
	// (the winding number of a future segment AB where the minimum distance is achieved)

	inline double operator() ( const Cell & A, std::pair < Cell, Manifold::Action > & B );
	// Euclidian distance, used by the cloud
		
};  // end of  class ManifoldQuotient::sq_dist

//------------------------------------------------------------------------------------------------------//


inline double ManifoldQuotient::sq_dist::working_manif  // rescaled metric, not to be used by the cloud
( const Cell & A, const std::pair < Cell, Manifold::Action > & B )  // static
	
{	return Manifold::working .sq_dist ( A, B .first, ManifoldQuotient::get_vector ( A, B ) );   }


inline double ManifoldQuotient::sq_dist::working_manif  // rescaled metric, not to be used by the cloud
( const std::pair < Cell, Manifold::Action > & A, const std::pair < Cell, Manifold::Action > & B )
// static
	
{	return Manifold::working .sq_dist ( A .first, B .first, ManifoldQuotient::get_vector ( A, B ) );   }


inline double ManifoldQuotient::sq_dist::dist2_internal   // invoked by  operator()
( const Cell & A, const Cell & B, const Manifold::Action & s )  // used by the cloud
	
{	std::pair < Cell, Manifold::Action > BB { B, s };
	return trivial_metric .core->sq_dist ( A, B, ManifoldQuotient::get_vector ( A, BB ) );  }

//------------------------------------------------------------------------------------------------------//


inline double ManifoldQuotient::sq_dist::operator()  // Euclidian distance, used by the cloud
( const Cell & A, std::pair < Cell, Manifold::Action > & B )

// changes B .second, sets the 'winning' winding number
	
// implements a discrete descent method rather than a blind spiral search
// as in export_to_file with tag::eps, tag::unfold

{	Manifold space = Manifold::working;
	assert ( space .exists() );  // we use the current (quotient) manifold
	std::shared_ptr < Manifold::Quotient > manif_q = tag::Util::assert_shared_cast
		< Manifold::Core, Manifold::Quotient > ( space .core );
	// the action group may have one or two generators
	const size_t ng = manif_q->actions .size();  // number of generators
	assert ( manif_q->winding_nbs .size() == ng );

	const Function & coords_q = space.coordinates();
	Manifold mani_Eu = manif_q->base_space;  // underlying Euclidian manifold
	const Function & coords_Eu = mani_Eu .coordinates();
	assert ( coords_Eu .number_of ( tag::components ) == 2 );

	const std::vector < std::vector < short int > > & directions = tag::Util::directions_int [ ng ];

	Manifold::Action act = 0;
	double dist_min = this->dist2_internal ( A, B .first, act );
	while ( true )
	{	size_t kept_d = directions .size();
		Manifold::Action better_act = 0;
		for ( size_t d = 0; d < directions .size(); d++ )
		{	const std::vector < short int > & direc = directions [d];
			assert ( direc .size() == ng );
		  Manifold::Action neighb_act = act;
			for ( size_t ig = 0; ig < ng; ig++ )
				neighb_act += direc [ ig ] * manif_q->actions [ ig ];
			double di = this->dist2_internal ( A, B .first, neighb_act );
			if ( di < dist_min )
			{	dist_min = di;
				better_act = neighb_act;
				kept_d = d;              }                                       }
		if ( kept_d == directions .size() )  // no better neighbour than 'act'
		{	B .second = act;
			return dist_min;  }
		act = better_act;                                                        }

	assert ( false );
		
}  // end of  ManifoldQuotient::sq_dist::operator()

//------------------------------------------------------------------------------------------------------//


class ManifoldQuotient::sq_dist_raw

// a callable object returning the square of the distance between two points
// used for MetricTree, see paragraphs 13.14 and 13.15 in the manual
// both arguments are Cells

{	public :

	static inline double working_manif  // rescaled metric, not to be used by the cloud
	( const Cell & A, const std::pair < Cell, Manifold::Action > & B );

	inline double dist2_internal   // invoked by  operator(),  used by the cloud
	( const Cell & A, const Cell & B, const Manifold::Action & s );
	
	inline double operator() ( const Cell & A, const Cell & B );  // Euclidian distance, used by the cloud
		
};  // end of  class Manifold::Type::Quotient::sq_dist_raw

//------------------------------------------------------------------------------------------------------//


inline double ManifoldQuotient::sq_dist_raw::working_manif  // static
( const Cell & A, const std::pair < Cell, Manifold::Action > & B )
// rescaled metric, not to be used by the cloud
	
{	return Manifold::working .sq_dist ( A, B .first, ManifoldQuotient::get_vector ( A, B ) );   }


inline double ManifoldQuotient::sq_dist_raw::dist2_internal   // invoked by  operator()
( const Cell & A, const Cell & B, const Manifold::Action & s )  // used by the cloud
	
{	std::pair < Cell, Manifold::Action > BB { B, s };
	return trivial_metric .core->sq_dist ( A, B, ManifoldQuotient::get_vector ( A, BB ) );  }

//------------------------------------------------------------------------------------------------------//


inline double ManifoldQuotient::sq_dist_raw::operator()  // Euclidian distance, used by the cloud
( const Cell & A, const Cell & B )
	
// implements a discrete descent method rather than a blind spiral search
// as in export_to_file with tag::eps, tag::unfold

{	Manifold space = Manifold::working;
	assert ( space .exists() );  // we use the current (quotient) manifold
	std::shared_ptr < Manifold::Quotient > manif_q = tag::Util::assert_shared_cast
		< Manifold::Core, Manifold::Quotient > ( space .core );
	// the action group may have one or two generators
	const size_t ng = manif_q->actions .size();  // number of generators
	assert ( manif_q->winding_nbs .size() == ng );

	const Function & coords_q = space.coordinates();
	Manifold mani_Eu = manif_q->base_space;  // underlying Euclidian manifold
	const Function & coords_Eu = mani_Eu .coordinates();
	assert ( coords_Eu .number_of ( tag::components ) == 2 );

	const std::vector < std::vector < short int > > & directions = tag::Util::directions_int [ ng ];

	Manifold::Action act = 0;
	double dist_min = this->dist2_internal ( A, B, act );
	while ( true )
	{	size_t kept_d = directions .size();
		Manifold::Action better_act = 0;
		for ( size_t d = 0; d < directions .size(); d++ )
		{	const std::vector < short int > & direc = directions [d];
			assert ( direc .size() == ng );
		  Manifold::Action neighb_act = act;
			for ( size_t ig = 0; ig < ng; ig++ )
				neighb_act += direc [ ig ] * manif_q->actions [ ig ];
			double di = this->dist2_internal ( A, B, neighb_act );
			if ( di < dist_min )
			{	dist_min = di;
				better_act = neighb_act;
				kept_d = d;              }                               }
		if ( kept_d == directions .size() )  // no better neighbour than 'act'
			return dist_min;
		act = better_act;                                                        }

	assert ( false );

}  // end of  ManifoldQuotient::sq_dist_raw::operator()

#endif  // ifndef MANIFEM_NO_QUOTIENT

//------------------------------------------------------------------------------------------------------//
//------------------------------------------------------------------------------------------------------//


ManifoldNoWinding::sq_dist square_dist_nw;  // hidden in anonymous namespace
ManifoldNoWinding::sq_dist_raw square_dist_raw_nw;  // hidden in anonymous namespace

NodeContainer < ManifoldNoWinding > ManifoldNoWinding::node_container
( square_dist_nw, square_dist_raw_nw, 1., 6. );

#ifndef MANIFEM_NO_QUOTIENT

ManifoldQuotient::sq_dist square_dist_q;  // hidden in anonymous namespace
ManifoldQuotient::sq_dist_raw square_dist_raw_q;  // hidden in anonymous namespace

NodeContainer < ManifoldQuotient > ManifoldQuotient::node_container
( square_dist_q, square_dist_raw_q, 1., 6. );

#endif  // ifndef MANIFEM_NO_QUOTIENT

// rank zero distance : 1 (will be reset later), tree ratio : 6

//------------------------------------------------------------------------------------------------------//

#ifndef MANIFEM_NO_QUOTIENT

class ManifoldQuotient::Enrich
{	public:
	static inline std::pair < Cell, Manifold::Action > enrich ( const Cell & P )
	{	return { P, 0 }; }
};  // end of  class ManifoldQuotient::Enrich

#endif  // ifndef MANIFEM_NO_QUOTIENT

//------------------------------------------------------------------------------------------------------//


inline void ManifoldNoWinding::redistribute_vertices  // static
( const Mesh & msh, const Cell & start, const Cell & stop, size_t n )

// redistribute last n vertices along a newly created 1D mesh by just making some barycenters
	
{	Cell A = stop;
	// just in case n is too large, or the curve is too short, we look for 'start'
	// the statement below is important for closed loops, where start == stop
	A = msh .cell_behind (A) .base() .reverse ( tag::surely_exists );
	for ( size_t i = 2; i < n; i++ )
	{	if ( A == start )  {  n = i;  break;  }
		A = msh .cell_behind ( A, tag::surely_exists ) .base() .reverse ( tag::surely_exists );  }
	assert ( n > 2 );
	Cell C = stop;
	Cell B = msh .cell_behind ( C, tag::surely_exists ) .base() .reverse ( tag::surely_exists );
	A = msh .cell_behind ( B, tag::surely_exists ) .base() .reverse ( tag::surely_exists );
	double coef = 0.35;
	for ( size_t i = 3; ; i++ )  // endless loop
	{	Manifold::working .interpolate ( B, 0.5 + coef, A, -2.*coef, B, 0.5 + coef, C );
		if ( coef > 0. ) coef -= 0.04;
		// the above method is not compatible with a non-uniform metric
		// so it will produce imperfect (but hopefully acceptable) results
		if ( i ==  n )  break;
		assert ( A != start );
		C = B;  B = A;
		A = msh .cell_behind ( B, tag::surely_exists ) .base() .reverse ( tag::surely_exists );  }   }


#ifndef MANIFEM_NO_QUOTIENT

inline void ManifoldQuotient::redistribute_vertices  // static
( const Mesh & msh, const Cell & start,
  const std::pair < Cell, Manifold::Action > & stop_w, size_t n )

// redistribute last n vertices along a newly created 1D mesh by just making some barycenters
// stop_w provides winding number
	
{	Manifold space = Manifold::working;
	assert ( space .exists() );  // we use the current (quotient) manifold
	std::shared_ptr < Manifold::Quotient > manif_q = tag::Util::assert_shared_cast
		< Manifold::Core, Manifold::Quotient > ( space .core );
	assert ( manif_q );
	const Function & coords_q = space .coordinates();
	Manifold mani_Eu = manif_q->base_space;  // underlying Euclidian manifold
	const Function & coords_Eu = mani_Eu .coordinates();
	assert ( coords_q  .number_of ( tag::components ) == frontal_nb_of_coords );
	assert ( coords_Eu .number_of ( tag::components ) == frontal_nb_of_coords );

	Cell A = stop_w .first;
	// just in case n is too large, or the curve is too short, we look for 'start'
	// the statement below is important for closed loops, where start == stop
	A = msh .cell_behind (A) .base() .reverse ( tag::surely_exists );
	for ( size_t i = 2; i < n; i++ )
	{	if ( A == start )  {  n = i;  break;  }
		A = msh .cell_behind ( A, tag::surely_exists ) .base() .reverse ( tag::surely_exists );  }
	assert ( n > 2 );
	Manifold::Action w = stop_w .second;
	Cell C = stop_w .first;
	Cell B = msh .cell_behind ( C, tag::surely_exists ) .base() .reverse ( tag::surely_exists );
	A = msh .cell_behind ( B, tag::surely_exists ) .base() .reverse ( tag::surely_exists );
	double coef = 0.35;
	for ( size_t i = 3; ; i++ )  // endless loop
	{	std::vector < double > v = coords_q ( C, tag::winding, w );
		coords_Eu ( temporary_vertex ) = v;
		mani_Eu .interpolate ( B, 0.5 + coef, A, -2.*coef, B, 0.5 + coef, temporary_vertex );
		if ( coef > 0. ) coef -= 0.04;
		// the above method is not compatible with a non-uniform metric
		// so it will produce imperfect (but hopefully acceptable) results
		w = 0;
		if ( i ==  n )  break;
		assert ( A != start );
		C = B;  B = A;
		A = msh .cell_behind ( B, tag::surely_exists ) .base() .reverse ( tag::surely_exists );  }   }

#endif  // ifndef MANIFEM_NO_QUOTIENT

//------------------------------------------------------------------------------------------------------//
//------------------------------------------------------------------------------------------------------//


template < class manif_type >                // line 2497
void frontal_construct_1d          // hidden in anonymous namespace
( Mesh & msh, const tag::StartWithInconsistentMesh &,
  const tag::StartAt &, const Cell & start,
  const tag::Towards &, std::vector < double > tangent,
  const tag::StopAt &, const typename manif_type::winding_cell & stop_w )

// manif_type could be ManifoldNoWinding or ManifoldQuotient (defined below)
	
// builds a one-dimensional mesh (a curve)
// orientation given by 'tangent'
	
// 'start' and 'stop' are positive vertices (may be one and the same)
// for quotient manifolds, 'stop' may be the same vertex as 'start' but with a certain winding number
	
{	const Cell & stop = manif_type::remove_winding ( stop_w );
	
	assert ( start .dim() == 0 );
	assert ( stop  .dim() == 0 );
	assert ( start .is_positive() );
	assert ( stop  .is_positive() );
	assert ( msh .dim() == 1 );
	assert ( msh .is_positive() );

	improve_tangent ( start, tangent );  // modifies 'tangent'

	Cell A = start;
	// the manifold's metric has been scaled, so the desired distance is now one
	for ( size_t step = 0; ; step++ )  // endless loop
		
	{	const std::vector < double > e = manif_type::get_vector ( A, stop_w );
		if ( Manifold::working .sq_dist ( A, stop, e ) < 2.618 )
		// distance is less than 1.618034, we may stop now  // golden number
		{	// check that 'stop' is in front of us
			// because, at the beginning, it could be behind us (or, even worse, A == stop)
			const double prod = Manifold::working .inner_prod ( A, e, tangent );
			// assert ( ( std::abs ( prod - 1.) < 0.3 ) or ( std::abs ( prod + 1.) < 0.3 ) );
			if ( prod > 0. )  // yes, 'stop' is in front of us
			{	Cell last = manif_type::build_seg ( A .reverse(), stop_w );
				// for a quotient manifold, 'stop_w' contains winding number
				last .add_to ( msh, tag::do_not_bother );
				// the meaning of tag::do_not_bother is explained at the end of paragraph 12.6 in the manual
				manif_type::redistribute_vertices ( msh, start, stop_w, 8 );
				// for a quotient manifold, 'stop_w' provides the winding number
				update_info_connected_one_dim ( msh, start, stop );
				return;                                                       }    }

		// test below has already been performed during a preliminary search of the shortest path
		// but if the user provided a starting direction, it has not been perfomed
		// so we repeat it here
		if ( step >= 5 )
		{	// if, after 5 or more steps, we meet 'start' (but haven't met 'stop' yet),
			// something is wrong, we are headed towards an endless loop
			const double sd = Manifold::working .sq_dist ( A, start );
			if ( sd < 1.44 )  // dist < 1.2
			{	std::cout << "cannot find stopping point" << std::endl;
				exit (1);                                                }  }
			// note that the above test breaks an endless loop if the current manifold is compact
			// (a closed loop) but not for an unbounded manifold
		
		Cell B = project_vertex_forward ( A, tangent );  // modifies 'tangent'
		Cell AB ( tag::segment, A .reverse(), B );        // zero winding
		AB .add_to ( msh, tag::do_not_bother );
		// the meaning of tag::do_not_bother is explained at the end of paragraph 12.6 in the manual
		A = B;                                                                     }

	assert ( false );
	
} // end of  frontal_construct_1d

//------------------------------------------------------------------------------------------------------//


void frontal_construct_1d          // hidden in anonymous namespace   //  line 2161
( Mesh & msh, const tag::StartWithInconsistentMesh &,
  const tag::StartAt &, const Cell & start,
  const tag::Orientation &, const tag::OrientationChoice & oc )

// builds a one-dimensional mesh
	
// meshing process starts and stops at the same vertex
// if orientation not specified, we choose inherent orientation

// here the manifold is not a quotient manifold

{	assert ( start .dim() == 0 );
	assert ( start .is_positive() );
	assert ( msh .dim() == 1 );
	assert ( msh .is_positive() );

	// here the working manifold is not a quotient manifold
	#ifndef NDEBUG  // DEBUG mode
	#ifndef MANIFEM_NO_QUOTIENT
	{ // just a block of code for hiding 'm'
	std::shared_ptr < Manifold::Quotient > m =
	   std::dynamic_pointer_cast < Manifold::Quotient > ( Manifold::working .core );
	assert ( m == nullptr );
	} // just a block of code
	#endif  // ifndef MANIFEM_NO_QUOTIENT
	#endif  // DEBUG

	std::vector < double > tangent = compute_tangent_vec ( tag::at_point, start );

	frontal_construct_1d < ManifoldNoWinding >  // line 2497
	( msh, tag::start_with_inconsistent_mesh, tag::start_at, start,
	       tag::towards, tangent,             tag::stop_at,  start );
	// frontal_construct calls update_info_connected_one_dim ( msh, start, start );

	if ( oc == tag::inherent )
		if ( not correctly_oriented ( msh ) )  switch_orientation_direct ( msh );                   }
	
//------------------------------------------------------------------------------------------------------//


void frontal_construct_1d          // hidden in anonymous namespace   // line 2199
( Mesh & msh, const tag::StartWithInconsistentMesh &,
  const tag::StartAt &, const Cell & start,
  const tag::StopAt &, const Cell & stop,
  const tag::Orientation &, const tag::OrientationChoice & oc )

// builds a one-dimensional mesh
	
// 'start' and 'stop' are positive vertices

// here the working manifold is not a quotient manifold

// uses two new temporary vertices

{	assert ( start .dim() == 0 );
	assert ( stop  .dim() == 0 );
	assert ( start .is_positive() );
	assert ( stop  .is_positive() );
	assert ( msh .dim() == 1 );
	assert ( start != stop );
		
	// here the working manifold is not a quotient manifold
	#ifndef NDEBUG  // DEBUG mode
	#ifndef MANIFEM_NO_QUOTIENT
	{ // just a block of code for hiding 'm'
	std::shared_ptr < Manifold::Quotient > m =
		std::dynamic_pointer_cast < Manifold::Quotient > ( Manifold::working .core );
	assert ( m == nullptr );
	} // just a block of code
	#endif  // ifndef MANIFEM_NO_QUOTIENT
	#endif  // DEBUG

	if ( oc == tag::not_provided )
	{	std::cout << "when starting and stopping points are provided," << std::endl;
		std::cout << "maniFEM needs to know how to choose the orientation of the curve;" << std::endl;
		std::cout << "please call .orientation or .shortest_path or .towards"
		          << std::endl;
		exit (1);                                                                                       }
	
	if ( oc == tag::intrinsic )

	{	if ( frontal_nb_of_coords != 1 )
		{	std::cout << "implicit manifolds have no intrinsic orientation" << std::endl;
			exit ( 1 );                                                                    }
		
		const Function & x = Manifold::working .coordinates() [0];
		std::vector < double > best_tangent ( 1, x ( stop ) - x ( start ) );

		frontal_construct_1d < ManifoldNoWinding >  // line 2497
		( msh, tag::start_with_inconsistent_mesh, tag::start_at, start,
		       tag::towards, best_tangent,        tag::stop_at,  stop  );
		// frontal_construct calls update_info_connected_one_dim ( msh, start, stop );
		return;                                                                         }
		
	std::vector < double > best_tangent = compute_tangent_vec ( tag::at_point, start );

	if ( oc == tag::geodesic )   // shortest path

	// the procedure below counts the number of steps, not the actual length
	// for a non-constant desired length, this may be not what the user wanted

	{	// start walking along the manifold from 'start' in the direction of best_tangent
		// and, simultaneously, in the opposite direction, given by  - best_tangent
		std::vector < double > tan1 = best_tangent, tan2 = best_tangent;
		for ( size_t i = 0; i < frontal_nb_of_coords; i++ ) tan2 [i] *= -1.;
		Cell ver1 ( tag::vertex ), ver2 ( tag::vertex );
		for ( size_t i = 0; i < frontal_nb_of_coords; i++ )
		{	const Function & x = Manifold::working .coordinates() [i];
			x ( ver1 ) = x ( start );  x ( ver2 ) = x ( start );        }
		double winner = 0.;  //  will be 1. or -1.
		for ( size_t step = 0; ; step++ )  // endless loop
		{	for ( size_t i = 0; i < frontal_nb_of_coords; i++ )
			{	const Function & x = Manifold::working .coordinates() [i];
				x ( temporary_vertex ) = x ( ver1 ) + tan1 [i];             }
			Manifold::working .project ( temporary_vertex );
			for ( size_t i = 0; i < frontal_nb_of_coords; i++ )
			{	const Function & x = Manifold::working .coordinates() [i];
				tan1 [i] = x ( temporary_vertex ) - x ( ver1 );
				x ( ver1 ) = x ( temporary_vertex );                        }
			double sd = Manifold::working .sq_dist ( ver1, stop );
			if ( sd < 1.44 )  // dist < 1.2
			{	// check that 'stop' is in front of us
				// below we could use the Riemannian inner product
				// however, the sign should be the same so it does not really matter
				double prod = 0.;
				for ( size_t i = 0; i < frontal_nb_of_coords; i++ )
				{	const Function & x = Manifold::working .coordinates() [i];
					prod += tan1[i] * ( x (stop) - x (ver1) );                  }
				if ( prod > 0. )  { winner = 1.;  break;  }                       }
			for ( size_t i = 0; i < frontal_nb_of_coords; i++ )
			{	const Function & x = Manifold::working .coordinates() [i];
				x ( temporary_vertex ) = x ( ver2 ) + tan2 [i];             }
			Manifold::working .project ( temporary_vertex );
			for ( size_t i = 0; i < frontal_nb_of_coords; i++ )
			{	const Function & x = Manifold::working .coordinates() [i];
				tan2 [i] = x ( temporary_vertex ) - x ( ver2 );
				x ( ver2 ) = x ( temporary_vertex );                        }
			sd = Manifold::working .sq_dist ( ver2, stop );
			if ( sd < 1.44 )  // dist < 1.2
			{	// check that 'stop' is in front of us
				// below we could use the Riemannian inner product
				// however, the sign should be the same so it does not really matter
				double prod = 0.;
				for ( size_t i = 0; i < frontal_nb_of_coords; i++ )
				{	const Function & x = Manifold::working .coordinates() [i];
					prod += tan2 [i] * ( x (stop) - x (ver2) );                 }
				if ( prod > 0. )  {  winner = -1.;  break;  }                     }
			if ( step < 5 )  continue;
			// if, after 5 or more steps, we meet 'start' (but haven't met 'stop' yet),
			// something is wrong, we are headed towards an endless loop
			sd = Manifold::working .sq_dist ( ver2, start );
			if ( sd < 1.44 )  // dist < 1.2
			{	std::cout << "cannot find stopping point" << std::endl;
				exit (1);                                                }
			// note that the above test breaks an endless loop if the current manifold is compact
			// (a closed loop) but not for an unbounded manifold
		}  // end of  endless loop

		assert ( ( winner == 1. ) or ( winner == -1. ) );
		for ( size_t i = 0; i < frontal_nb_of_coords; i++ )  best_tangent [i] *= winner;
		frontal_construct_1d < ManifoldNoWinding >  // line 2497
		( msh, tag::start_with_inconsistent_mesh, tag::start_at, start,
		       tag::towards, best_tangent,        tag::stop_at,  stop  );
		// frontal_construct calls update_info_connected_one_dim ( msh, start, stop );
		return;
	}  // end of  if

	if ( ( oc == tag::inherent ) or ( oc == tag::random ) )

	{	//if ( start == stop )
		//{	assert ( frontal_nb_of_coords >= 2 );
		//	frontal_construct_1d ( msh, tag::start_with_inconsistent_mesh,   // line 2161
		//	                       tag::start_at, start, tag::orientation, oc );
		//	// frontal_construct calls update_info_connected_one_dim ( msh, start, stop );
		//	return;                                                           }

		frontal_construct_1d < ManifoldNoWinding >  // line 2497
		( msh, tag::start_with_inconsistent_mesh, tag::start_at, start,
		       tag::towards, best_tangent,        tag::stop_at,  stop  );
		// frontal_construct calls update_info_connected_one_dim ( msh, start, stop );

		if ( oc == tag::random ) return;                                               }

	assert ( oc == tag::inherent );

	assert ( frontal_nb_of_coords >= 2 );
	for ( size_t i = 0; i < frontal_nb_of_coords; i++ )  best_tangent [i] *= -1.;
	// the number of segments does not count, and we don't know it yet
	Mesh msh2 ( tag::whose_core_is,
	    new Mesh::Connected::OneDim ( tag::with, 1, tag::segments, tag::one_dummy_wrapper ),
	    tag::move, tag::is_positive                                                         );
	// the number of segments does not count, and we don't know it yet
	// we compute it after the mesh is built, by counting segments
	// but we count segments using an iterator, and the iterator won't work
	// if msh.core->nb_of_segs == 0, so we set nb_of_segs to 1 (dirty trick)
	// see Mesh::Iterator::Over::VerticesOfConnectedOneDimMesh::NormalOrder::reset
	// in iterator.cpp
	frontal_construct_1d < ManifoldNoWinding >  // line 2497
	( msh2, tag::start_with_inconsistent_mesh, tag::start_at, start,
	        tag::towards, best_tangent,        tag::stop_at,  stop  );
	// frontal_construct calls update_info_connected_one_dim ( msh2, start, stop );
	switch_orientation_extremities ( msh2 );
		
	Mesh whole = Mesh::Build ( tag::join ) .meshes ( { msh, msh2 } );
	if ( not correctly_oriented ( whole ) )
	{	switch_orientation_extremities ( msh2 );  msh = msh2;  }
	
} // end of  frontal_construct_1d

//------------------------------------------------------------------------------------------------------//
//------------------------------------------------------------------------------------------------------//

#include "frontal-2d.cpp"  // the entire file is hidden in anonymous namespace

//------------------------------------------------------------------------------------------------------//
//------------------------------------------------------------------------------------------------------//

#include "frontal-3d.cpp"  // the entire file is hidden in anonymous namespace

//------------------------------------------------------------------------------------------------------//
//------------------------------------------------------------------------------------------------------//


template < class manif_type, class isr_container >   // line 4021
void frontal_construct          // hidden in anonymous namespace
( Mesh & msh, const tag::StartWithNonExistentMesh &,
  const tag::StartAt &, const Cell & start,
  const tag::Orientation &, tag::OrientationChoice oc )

// manif_type could be ManifoldNoWinding or ManifoldQuotient (defined below)
// isr_container may be one of ISRContainer::{Inactive,Active} (defined above)
	
// mesh the entire manifold
// 'start' is a positive vertex

{	assert ( not msh .exists() );
	assert ( start .exists() );

	if ( Manifold::working .topological_dim() == 1 )
		
	{	msh .core = new Mesh::Connected::OneDim
			( tag::with, 1, tag::segments, tag::one_dummy_wrapper );
		msh .is_pos = & tag::Util::return_true;
		// the number of segments does not count, and we don't know it yet
		// we compute it after the mesh is built, by counting segments
		// but we count segments using an iterator, and the iterator won't work
		// if this->msh->nb_of_segs == 0, so we set nb_of_segs to 1 (dirty trick)
		// see Mesh::Iterator::Over::VerticesOfConnectedOneDimMesh::NormalOrder::reset in iterator.cpp

		// no stopping point, we assume the user wants to mesh the entire working manifold
		// we assume the working manifold is compact
		if ( frontal_nb_of_coords == 1 )  // geometric dimension 1, topological dimension 1
		#ifndef MANIFEM_NO_QUOTIENT
		{	assert ( manif_type::has_winding() );
			assert ( ( oc == tag::intrinsic ) or ( oc == tag::random ) or ( oc == tag::not_provided ) );
			std::shared_ptr < Manifold::Quotient > manif_q = tag::Util::assert_shared_cast
				< Manifold::Core, Manifold::Quotient > ( Manifold::working .core );
			assert ( manif_q->actions .size() == 1 );
			Manifold::Action w = manif_q->actions [0];  // recover the only generator
			// assert the generator is compatible with the intrinsic orientation
			#ifndef NDEBUG  // DEBUG mode
			const Function & x = Manifold::working .coordinates();
			assert ( x .number_of ( tag::components ) == 1 );
			Function::Scalar::MultiValued::JumpIsSum * xx = tag::Util::assert_cast
				< Function::Core *, Function::Scalar::MultiValued::JumpIsSum * > ( x .core );
			assert ( xx->actions .size() == 1 );
			assert ( xx->actions [0] == w );
			assert ( xx->beta .size() == 1 );
			assert ( xx->beta [0] > 0. );  // compatibility with the intrinsic orientation
			#endif  // DEBUG
			// method below is virtual, here calls Quotient version
			Manifold::working .core->frontal_method_1d
			( msh, tag::start_with_inconsistent_mesh,
			  tag::start_at, start, tag::stop_at, start, tag::winding, w );                              }
			// calls  frontal_construct_1d  line 2497
			// frontal_construct calls update_info_connected_one_dim ( msh, start, start );
		#else  // MANIFEM_NO_QUOTIENT
			assert ( false );
		#endif  // ifndef MANIFEM_NO_QUOTIENT
		
		else if ( frontal_nb_of_coords == 2 )  // geometric dimension 2, topological dimension 1
		{	assert ( not manif_type::has_winding() );
			// so, we want a compact 1D mesh in RR2
			if ( oc == tag::not_provided ) oc = tag::inherent;
			if ( oc == tag::intrinsic )
			// we are in an implicit manifold
			{	std::cout << "intrinsic orientation makes no sense here, did you mean 'inherent' ?"
				          << std::endl;
				exit (1);                                                                           }
			assert ( ( oc == tag::inherent ) or ( oc == tag::random ) );
			frontal_construct_1d   // line 2161
			( msh, tag::start_with_inconsistent_mesh, tag::start_at, start, tag::orientation, oc );  }
			// frontal_construct calls update_info_connected_one_dim ( msh, start, start );

		else  // geometric dimension 3, topological dimension 1
		{	assert ( not manif_type::has_winding() );
			assert ( frontal_nb_of_coords == 3 );
			// so, we want a compact 1D mesh in RR3
			if ( oc != tag::random )
			{	std::cout << "please provide an initial vertex and a direction, ";
				std::cout << "or specify .orientation ( tag::random )" << std::endl;
				exit ( 1 );                                                           }
			frontal_construct_1d  // line 2161
			( msh, tag::start_with_inconsistent_mesh,
			  tag::start_at, start, tag::orientation, tag::random );
			// frontal_construct calls update_info_connected_one_dim ( msh, start, start );
		}  // end of  else  geom dim == 3
	}  // end of  if ( Manifold::working .topological_dim() == 1 )

	else  // topological dimension >= 2
		
	{	assert ( Manifold::working .topological_dim() == 2 );  // 3D meshing makes no sense here
		assert ( frontal_nb_of_coords >= 2 );

		if ( ( not manif_type::has_winding() ) and ( frontal_nb_of_coords == 2 ) )
		{ std::cout << "you are trying to mesh the entire plane, this makes no sense" << std::endl;
			exit ( 1 );                                                                                }

		if ( ( not manif_type::has_winding() ) and ( oc == tag::intrinsic ) )
			// we are in an implicit manifold
		{	std::cout << "intrinsic orientation makes no sense here, did you mean 'inherent' ?"
			          << std::endl;
			exit (1);                                                                            }

		msh .core = new Mesh::Fuzzy ( tag::of_dim, 3, tag::minus_one, tag::one_dummy_wrapper );
		msh .is_pos = & tag::Util::return_true;

		Cell B ( tag::vertex );
		std::vector < double > tangent = compute_tangent_vec ( tag::at_point, start );
		for ( size_t i = 0; i < frontal_nb_of_coords; i++ )
		{	Function x = Manifold::working .coordinates() [i];
			x ( B ) = x ( start ) + tangent [i];               }
		Cell AB ( tag::segment, start .reverse(), B );   // zero winding
		std::vector < double > normal =
			compute_tangent_vec ( tag::at_point, start, tag::orthogonal_to, tangent );
		Cell C ( tag::vertex );
		for ( size_t i = 0; i < frontal_nb_of_coords; i++ )
		{	Function x = Manifold::working.coordinates()[i];
			x ( C ) = x ( start ) + 0.5 * tangent [i] - tag::Util::sqrt_three_quarters * normal [i];  }
		// use a membrane rahter than a triangle !
		Cell BC ( tag::segment, B .reverse(), C );       // zero winding
		Cell CA ( tag::segment, C .reverse(), start );   // zero winding
		Cell tri ( tag::triangle, AB, BC, CA );
		tri .add_to ( msh );

		// or a 2d quotient manifold : we use the scaled rotated vector if metric is isotropic
		//   we use  det A A^-2  times the rotated vector if metric is anisotropic
		//   (we assume the intrinsic orientation)

		if ( manif_type::has_winding() )
		#ifndef MANIFEM_NO_QUOTIENT
		{	assert ( frontal_nb_of_coords == 2 );  // geometric dimension 2, topological dimension 2
			isr_container::codim_0();
			frontal_construct_2d < Environment      // line 1255
				< ManifoldQuotient, isr_container, ExtProd2d < ManifoldQuotient, isr_container > > >
				( msh, tag::boundary, tri .boundary() .reverse(), tag::start_at, AB .reverse() );     }
			// a copy of  tri .boundary() .reverse()  will be built, as Mesh::STSI
		#else  // MANIFEM_NO_QUOTIENT
			assert ( false );
		#endif  // ifndef MANIFEM_NO_QUOTIENT

		else  //  no winding
		{	assert ( frontal_nb_of_coords == 3 );  // geometric dimension 3, topological dimension 2
			// since no boundary is provided, we are dealing with a compact surface in RR3 :
			//   we build each normal vector from neighbour ones

			isr_container::codim_1();
			// if metric is tag::Util::Metric::Variable::Matrix::SquareRoot::ISR, de-activate
			tag::Util::Metric::Variable::Matrix::SquareRoot::ISR * m =
				dynamic_cast < tag::Util::Metric::Variable::Matrix::SquareRoot::ISR * >
				( isr_container::registered_metric() .core .get() );
			if ( m )
			{	isr_container::de_activate();
				frontal_construct_2d < Environment      // line 1437  then  line 1255
					< ManifoldNoWinding, ISRContainer::Inactive,
					  ExtProdCodimOne < ManifoldNoWinding, ISRContainer::Inactive > > >
					( msh, tag::boundary, tri .boundary() .reverse(),
					  tag::start_at, AB .reverse(), tag::towards, normal );              }
				// a copy of  tri .boundary() .reverse()  will be built, as Mesh::STSI
			else  // metric is not tag::Util::Metric::Variable::Matrix::SquareRoot::ISR
				frontal_construct_2d < Environment      // line 1437  then  line 1255
					< ManifoldNoWinding, isr_container,
					  ExtProdCodimOne < ManifoldNoWinding, isr_container > > >
					( msh, tag::boundary, tri .boundary() .reverse(),
					  tag::start_at, AB .reverse(), tag::towards, normal );                 }
				// a copy of  tri .boundary() .reverse()  will be built, as Mesh::STSI

		if ( oc == tag::random ) return;
		assert ( ( oc == tag::inherent ) or ( oc == tag::not_provided ) );
		// here we interpret "not provided" as "inherent"
		if ( not correctly_oriented ( msh ) )  switch_orientation_of_each_cell ( msh );
		assert ( correctly_oriented ( msh ) );
		return;
	}  // end of  else  topological dim >= 2

} // end of  frontal_construct

//------------------------------------------------------------------------------------------------------//


template < class manif_type, class isr_container >   // line 4188
void frontal_construct          // hidden in anonymous namespace
( Mesh & msh, const tag::Boundary &, const Mesh & interface,
  const tag::StartAt &, const Cell & start,
  const tag::Orientation &, tag::OrientationChoice oc       )

// manif_type could be ManifoldNoWinding or ManifoldQuotient (defined below)
// isr_container may be one of ISRContainer::{Inactive,Active} (defined above)
	
// for two-dimensional meshes in RR^2 (intrinsic orientation)
//   or in a 2D submanifold of RR^3 (random or inherent orientation)
// or three-dimensional meshes
	
// 'start' is a segment or triangle belonging to 'interface'
// no normal vector provided, we need to build our own

{	assert ( frontal_nb_of_coords >= 2 );
	assert ( msh .dim() == Manifold::working .topological_dim() );
	assert ( start .dim() + 1 == Manifold::working .topological_dim() );
	assert ( start .belongs_to ( interface, tag::same_dim, tag::orientation_compatible_with_mesh ) );
	
	if ( frontal_nb_of_coords == 2 )
			
	{	assert ( Manifold::working .topological_dim() == 2 );
		// domain in the plane RR^2 or quotient manifold, intrinsic orientation
		assert ( ( oc == tag::intrinsic ) or ( oc == tag::not_provided ) );
		// here we interpret "not provided" as "intrinsic"
		
		isr_container::codim_0();
		ExtProd2d < manif_type, isr_container > ::compute_data
			( tag::domain_2d, tag::first_time, tag::at_seg, start );
		
		// isr_container == ISRContainer::Inactive
		frontal_construct_2d < Environment                   // line 1255
			< manif_type, isr_container, ExtProd2d < manif_type, isr_container > > >
			( msh, tag::boundary, interface, tag::start_at, start );

		return;                                                                     }
	
	assert ( frontal_nb_of_coords == 3 );
	
	if ( Manifold::working .topological_dim() == 2 )  // 2D mesh in RR3
		
	{	// a compact surface in RR3 : we will build each normal vector from neighbour ones
		isr_container::codim_1();

		// compute a normal vector, on an arbitrary side of 'start'
		std:: vector < double > tangent = manif_type::get_vector ( start );
		Cell A = start .base() .reverse ( tag::surely_exists );
		std::vector < double > normal =
			compute_tangent_vec ( tag::at_point, A, tag::orthogonal_to, tangent );

		frontal_construct_2d < Environment                  // line 1437  then  line 1255
			< ManifoldNoWinding, isr_container, ExtProdCodimOne < ManifoldNoWinding, isr_container > > >
			( msh, tag::boundary, interface, tag::start_at, start, tag::towards, normal );

		if ( oc == tag::random ) return;
		assert ( ( oc == tag::inherent ) or ( oc == tag::not_provided ) );
		// here we interpret "not provided" as "inherent"
	
		// std::cout << "wait a minute ..." << std::endl;
		// build the mesh on the other side of 'interface'
		for ( size_t i = 0; i < frontal_nb_of_coords; i++ )  normal [i] *= -1.;
		Mesh msh2 ( tag::whose_core_is,   // of dimesion two
			new Mesh::Fuzzy ( tag::of_dim, 3, tag::minus_one, tag::one_dummy_wrapper ),
			tag::move, tag::is_positive                                                );
		frontal_construct_2d < Environment                  // line 1437  then  line 1255
			< ManifoldNoWinding, isr_container, ExtProdCodimOne < ManifoldNoWinding, isr_container > > >
	  ( msh2, tag::boundary, interface .reverse(),
		  tag::start_at, start .reverse(), tag::towards, normal );	
		// join everything to get a mesh on the entire manifold
		Mesh whole = Mesh::Build ( tag::join ) .meshes ( { msh, msh2 } );
		
		if ( not correctly_oriented ( whole ) )
		{	switch_orientation_of_each_cell ( msh2 );  msh = msh2;  }
		return;                                                                                           }

	assert ( Manifold::working .topological_dim() == 3 );  // mesh of tetrahedra
	assert ( start .dim() == 2 );
	frontal_construct_3d < Environment                   // line 4541
		< manif_type, isr_container, ExtProd3d < manif_type, isr_container > > >
		( msh, tag::boundary, interface, tag::start_at, start );
	

} // end of  frontal_construct

//------------------------------------------------------------------------------------------------------//


inline void compute_data_sqrt_matrix   // hidden in anonymous namespace
// common code for tag::Util::Metric::Variable::Matrix::SquareRoot::compute_data_3_codim_{0,1}
( const tag::Util::Metric::Variable::Matrix::SquareRoot * that, const tag::AtPoint &, const Cell & V )

{	assert ( V .dim() == 0 );
	assert ( that );

	tag::Util::Tensor < double > matr ( frontal_nb_of_coords, frontal_nb_of_coords );
	for ( size_t i = 0; i < frontal_nb_of_coords; i++ )
	for ( size_t j = 0; j < frontal_nb_of_coords; j++ )
		matr (i,j) = that->matrix (i,j) (V);
	
	tag::Util::Tensor < double > * inv_matr =
		new tag::Util::Tensor < double > ( frontal_nb_of_coords, frontal_nb_of_coords );
  // candidate for inverse matrix
	double trace = 0.;
	for ( size_t i = 0; i < frontal_nb_of_coords; i++ )  trace += matr (i,i);
	trace = frontal_nb_of_coords / trace;
	for ( size_t i = 0; i < frontal_nb_of_coords; i++ )  ( * inv_matr ) (i,i) = trace;

	std::vector < double > * eigen_dir = new std::vector < double > ( frontal_nb_of_coords );
	double * radius = new double ( compute_isr_loc ( matr, * inv_matr, * eigen_dir, false ) );
	// last argument false means eigen_dir is not meaningful as input, only useful upon return

	tag::Util::insert_new_elem_in_map ( V .core->hook,
		{ tag::isr_inv_matrix, static_cast < void * > ( inv_matr ) } );

	tag::Util::insert_new_elem_in_map ( V .core->hook,
		{ tag::isr_eigen_dir, static_cast < void * > ( eigen_dir ) } );

	tag::Util::insert_new_elem_in_map ( V .core->hook,
		{ tag::isr_radius, static_cast < void * > ( radius ) } );

}  // end of  compute_data_sqrt_matrix
	

inline void compute_data_sqrt_matrix   // hidden in anonymous namespace
// common code for tag::Util::Metric::Variable::Matrix::SquareRoot::compute_data_5_codim_{0,1}
( const tag::Util::Metric::Variable::Matrix::SquareRoot * that,
  const tag::AtPoint &, const Cell & W, const tag::UseInformationFrom &, const Cell & V )

{	assert ( V .dim() == 0 );
	assert ( W .dim() == 0 );
	assert ( that );

	std::map < tag::KeyForHook, void * > ::iterator it_V = V .core->hook .find ( tag::isr_inv_matrix );
	assert ( it_V != V .core->hook .end() );
	tag::Util::Tensor < double > * inv_matr = new tag::Util::Tensor < double >  // copy constructor
		( * ( static_cast < tag::Util::Tensor < double > * > ( it_V->second ) ) );
	tag::Util::insert_new_elem_in_map ( W .core->hook,
		{ tag::isr_inv_matrix, static_cast < void * > ( inv_matr ) } );

	it_V = V .core->hook .find ( tag::isr_eigen_dir );
	assert ( it_V != V .core->hook .end() );
	std::vector < double > * eigen_dir = new std::vector < double >   // copy constructor
		( * ( static_cast < std::vector < double > * > ( it_V->second ) ) );
	tag::Util::insert_new_elem_in_map ( W .core->hook,
		{ tag::isr_eigen_dir, static_cast < void * > ( eigen_dir ) } );

	tag::Util::Tensor < double > matr ( frontal_nb_of_coords, frontal_nb_of_coords );
	for ( size_t i = 0; i < frontal_nb_of_coords; i++ )
	for ( size_t j = 0; j < frontal_nb_of_coords; j++ )
		matr (i,j) = that->matrix (i,j) (W);
	double * radius = new double ( compute_isr_loc ( matr, * inv_matr, * eigen_dir, true ) );
	// last argument true means eigen_dir is meaningful as input, updated upon return
	tag::Util::insert_new_elem_in_map ( W .core->hook,
		{ tag::isr_radius, static_cast < void * > ( radius ) } );

}  // end of  compute_data_sqrt_matrix

}  // end of  anonymous namespace

//------------------------------------------------------------------------------------------------------//
//------------------------------------------------------------------------------------------------------//


inline void ISRContainer::Active::correct_ext_prod_2d  // static
( const Cell & P, std::vector < double > & n )

{	assert ( ISRContainer::Active::metric .core );  // has been registered by register_active_metric
	ISRContainer::Active::metric .core->correct_ext_prod_2d ( P, n );
	normalize_vector_approx ( P, n );                                  }


inline void ISRContainer::Active::correct_ext_prod_3d  // static
( const Cell & P, std::vector < double > & n )
	
{	assert ( ISRContainer::Active::metric .core );  // has been registered by register_active_metric
	ISRContainer::Active::metric .core->correct_ext_prod_3d ( P, n );
	assert ( false );  // how do we compensate for zoom ? code below is of course wrong
	// take into account two cases : 3D mesh (tetrahedra) or surface mesh in 3D (triangles)
	#ifndef NDEBUG  // DEBUG mode
	tag::Util::Metric::Core * mani = Manifold::working .core->metric .core .get();
	assert ( mani->isotropic() );
	#endif
	normalize_vector_approx ( P, n, tag::around, 1. );                              }
		

inline void ISRContainer::Inactive::correct_ext_prod_2d  // static
( const Cell & P, std::vector < double > & n )

{	normalize_vector_approx ( P, n );  }
		

inline void ISRContainer::Inactive::correct_ext_prod_3d  // static
( const Cell & P, std::vector < double > & n )

// there is no ISRContainer::Active::metric, Manifold::working .metric is isotropic
// the zoom must be taken into account, we must resize the exterior product accordingly

{	tag::Util::Metric::Core * mani = Manifold::working .core->metric .core .get();
	assert ( mani->isotropic() );
	// since this is the exterior product of two sides of a triangle, we multiply by
	// sqrt ( sqrt(0.75) ) = 0.93
	// however, 'approx_square_root' reports often a value below threshold,
	// never above, so we give an artificial lower value  0.7
	normalize_vector_approx ( P, n, tag::around, 0.7 / mani->inner_spectral_radius (P) );  }
	                                // was  tag::Util::fourth_root_three_quarters

//------------------------------------------------------------------------------------------------------//


void tag::Util::Metric::Variable::Matrix::Simple::codim_0 ( )
// virtual from tag::Util::Metric::Core, here overridden
{	this->compute_inner_data_3 = & tag::Util::Metric::Variable::Matrix::Simple::compute_data_3_codim_0;
	this->compute_inner_data_5 = & tag::Util::Metric::Variable::Matrix::Simple::compute_data_5_codim_0;
	this->remove_node_isr_p    = & tag::Util::Metric::Variable::Matrix::Simple::remove_node_isr_codim_0;  }


void tag::Util::Metric::Variable::Matrix::Simple::codim_1 ( )
// virtual from tag::Util::Metric::Core, here overridden
{	this->compute_inner_data_3 = & tag::Util::Metric::Variable::Matrix::Simple::compute_data_3_codim_1;
	this->compute_inner_data_5 = & tag::Util::Metric::Variable::Matrix::Simple::compute_data_5_codim_1;
	this->remove_node_isr_p    = & tag::Util::Metric::Variable::Matrix::Simple::remove_node_isr_codim_1;  }


void tag::Util::Metric::Variable::Matrix::SquareRoot::codim_0 ( )
// virtual from tag::Util::Metric::Core, here overridden
{	this->compute_inner_data_3 =
		& tag::Util::Metric::Variable::Matrix::SquareRoot::compute_data_3_codim_0;
	this->compute_inner_data_5 =
		& tag::Util::Metric::Variable::Matrix::SquareRoot::compute_data_5_codim_0;
	this->remove_node_isr_p = & tag::Util::Metric::Variable::Matrix::SquareRoot::remove_node_isr_codim_0;  }


void tag::Util::Metric::Variable::Matrix::SquareRoot::codim_1 ( )
//this-> virtual from tag::Util::Metric::Core, here overridden
{	this->compute_inner_data_3 =
		& tag::Util::Metric::Variable::Matrix::SquareRoot::compute_data_3_codim_1;
	this->compute_inner_data_5 =
		& tag::Util::Metric::Variable::Matrix::SquareRoot::compute_data_5_codim_1;
	this->remove_node_isr_p = & tag::Util::Metric::Variable::Matrix::SquareRoot::remove_node_isr_codim_1;  }


void tag::Util::Metric::Variable::Matrix::ISR::codim_0 ( )
// virtual from tag::Util::Metric::Core, here overridden
{	this->compute_inner_data_3 = & tag::Util::Metric::Variable::Matrix::ISR::compute_data_3_codim_0;
	this->compute_inner_data_5 = & tag::Util::Metric::Variable::Matrix::ISR::compute_data_5_codim_0;
	this->remove_node_isr_p    = & tag::Util::Metric::Variable::Matrix::ISR::remove_node_isr_codim_0;  }


void tag::Util::Metric::Variable::Matrix::ISR::codim_1 ( )
// virtual from tag::Util::Metric::Core, here overridden
{	this->compute_inner_data_3 = & tag::Util::Metric::Variable::Matrix::ISR::compute_data_3_codim_1;
	this->compute_inner_data_5 = & tag::Util::Metric::Variable::Matrix::ISR::compute_data_5_codim_1;
	this->remove_node_isr_p    = & tag::Util::Metric::Variable::Matrix::ISR::remove_node_isr_codim_1;  }


void tag::Util::Metric::Variable::Matrix::SquareRoot::ISR::codim_0 ( )
// virtual from tag::Util::Metric::Core, here overridden
{	this->compute_inner_data_3 =
		& tag::Util::Metric::Variable::Matrix::SquareRoot::ISR::compute_data_3_codim_0;
	this->compute_inner_data_5 =
		& tag::Util::Metric::Variable::Matrix::SquareRoot::ISR::compute_data_5_codim_0;
	this->remove_node_isr_p =
		& tag::Util::Metric::Variable::Matrix::SquareRoot::ISR::remove_node_isr_codim_0; }


void tag::Util::Metric::Variable::Matrix::SquareRoot::ISR::codim_1 ( )
// virtual from tag::Util::Metric::Core, here overridden
{	this->compute_inner_data_3 =
		& tag::Util::Metric::Variable::Matrix::SquareRoot::ISR::compute_data_3_codim_1;
	this->compute_inner_data_5 =
		& tag::Util::Metric::Variable::Matrix::SquareRoot::ISR::compute_data_5_codim_1;
	this->remove_node_isr_p =
		& tag::Util::Metric::Variable::Matrix::SquareRoot::ISR::remove_node_isr_codim_1; }

//------------------------------------------------------------------------------------------------------//
//------------------------------------------------------------------------------------------------------//


void tag::Util::Metric::Variable::Matrix::Simple::compute_data
// virtual from tag::Util::Metric::Core, here overridden
( const tag::AtPoint &, const Cell & V )
{	this->compute_inner_data_3 ( this, tag::at_point, V );  }


void tag::Util::Metric::Variable::Matrix::Simple::compute_data
// virtual from tag::Util::Metric::Core, here overridden
( const tag::AtPoint &, const Cell & W, const tag::UseInformationFrom &, const Cell & V )
{	this->compute_inner_data_5 ( this, tag::at_point, W, tag::use_information_from, V );  }


void tag::Util::Metric::Variable::Matrix::SquareRoot::compute_data
// virtual from tag::Util::Metric::Core, here overridden
( const tag::AtPoint &, const Cell & V )
{	this->compute_inner_data_3 ( this, tag::at_point, V );  }


void tag::Util::Metric::Variable::Matrix::SquareRoot::compute_data
// virtual from tag::Util::Metric::Core, here overridden
( const tag::AtPoint &, const Cell & W, const tag::UseInformationFrom &, const Cell & V )
{	this->compute_inner_data_5 ( this, tag::at_point, W, tag::use_information_from, V );  }


void tag::Util::Metric::Variable::Matrix::ISR::compute_data
// virtual from tag::Util::Metric::Core, here overridden
( const tag::AtPoint &, const Cell & V )
{	this->compute_inner_data_3 ( this, tag::at_point, V );  }


void tag::Util::Metric::Variable::Matrix::ISR::compute_data
// virtual from tag::Util::Metric::Core, here overridden
( const tag::AtPoint &, const Cell & W, const tag::UseInformationFrom &, const Cell & V )
{	this->compute_inner_data_5 ( this, tag::at_point, W, tag::use_information_from, V );  }


void tag::Util::Metric::Variable::Matrix::SquareRoot::ISR::compute_data
// virtual from tag::Util::Metric::Core, here overridden
( const tag::AtPoint &, const Cell & V )
{	this->compute_inner_data_3 ( this, tag::at_point, V );  }


void tag::Util::Metric::Variable::Matrix::SquareRoot::ISR::compute_data
// virtual from tag::Util::Metric::Core, here overridden
( const tag::AtPoint &, const Cell & W, const tag::UseInformationFrom &, const Cell & V )
{	this->compute_inner_data_5 ( this, tag::at_point, W, tag::use_information_from, V );  }

//------------------------------------------------------------------------------------------------------//


void tag::Util::Metric::Variable::Matrix::Simple::compute_data_3_forbid
// static method, called through a pointer compute_inner_data_3
( const tag::Util::Metric::Variable::Matrix::Simple * that, const tag::AtPoint &, const Cell & V )
{	assert ( false );  }


void tag::Util::Metric::Variable::Matrix::Simple::compute_data_5_forbid
// static method, called through a pointer compute_inner_data_5
( const tag::Util::Metric::Variable::Matrix::Simple * that,
  const tag::AtPoint &, const Cell & W, const tag::UseInformationFrom &, const Cell & V )
{	assert ( false );  }


void tag::Util::Metric::Variable::Matrix::Simple::compute_data_3_codim_0
// static method, called through a pointer compute_inner_data_3
( const tag::Util::Metric::Variable::Matrix::Simple * that, const tag::AtPoint &, const Cell & V )

{	assert ( V .dim() == 0 );
	assert ( that );

	tag::Util::Tensor < double > matr ( frontal_nb_of_coords, frontal_nb_of_coords );
	for ( size_t i = 0; i < frontal_nb_of_coords; i++ )
	for ( size_t j = 0; j < frontal_nb_of_coords; j++ )
		matr (i,j) = that->matrix (i,j) (V);

	double * sqrt_det_M = new double ( std::sqrt ( determinant ( matr ) ) );
	
	tag::Util::Tensor < double > * inv_matr =
		new tag::Util::Tensor < double > ( frontal_nb_of_coords, frontal_nb_of_coords );
  // candidate for inverse matrix
	double trace = 0.;
	for ( size_t i = 0; i < frontal_nb_of_coords; i++ )  trace += matr (i,i);
	trace = frontal_nb_of_coords / trace;
	for ( size_t i = 0; i < frontal_nb_of_coords; i++ )  ( * inv_matr ) (i,i) = trace;

	std::vector < double > * eigen_dir = new std::vector < double > ( frontal_nb_of_coords );
	double sq_radius = compute_isr_loc ( matr, * inv_matr, * eigen_dir, false );
	// last argument false means eigen_dir is not meaningful as input, only useful upon return
	// updates inv_matr

	double * radius = new double ( std::sqrt ( sq_radius ) );
	
	tag::Util::insert_new_elem_in_map ( V .core->hook,
		{ tag::isr_inv_matrix, static_cast < void * > ( inv_matr ) } );

	tag::Util::insert_new_elem_in_map ( V .core->hook,
		{ tag::isr_eigen_dir, static_cast < void * > ( eigen_dir ) } );

	tag::Util::insert_new_elem_in_map ( V .core->hook,
		{ tag::isr_radius, static_cast < void * > ( radius ) } );

	tag::Util::insert_new_elem_in_map ( V .core->hook,
		{ tag::sqrt_det, static_cast < void * > ( sqrt_det_M ) } );

}  // end of  tag::Util::Metric::Variable::Matrix::Simple::compute_data_3_codim_0
	

void tag::Util::Metric::Variable::Matrix::Simple::compute_data_5_codim_0
// static method, called through a pointer compute_inner_data_5
( const tag::Util::Metric::Variable::Matrix::Simple * that,
  const tag::AtPoint &, const Cell & W, const tag::UseInformationFrom &, const Cell & V )

{	assert ( V .dim() == 0 );
	assert ( W .dim() == 0 );
	assert ( that );

	std::map < tag::KeyForHook, void * > ::iterator it_V = V .core->hook .find ( tag::isr_inv_matrix );
	assert ( it_V != V .core->hook .end() );
	tag::Util::Tensor < double > * inv_matr = new tag::Util::Tensor < double >  // copy constructor
		( * ( static_cast < tag::Util::Tensor < double > * > ( it_V->second ) ) );
	tag::Util::insert_new_elem_in_map ( W .core->hook,
		{ tag::isr_inv_matrix, static_cast < void * > ( inv_matr ) } );

	it_V = V .core->hook .find ( tag::isr_eigen_dir );
	assert ( it_V != V .core->hook .end() );
	std::vector < double > * eigen_dir = new std::vector < double >   // copy constructor
		( * ( static_cast < std::vector < double > * > ( it_V->second ) ) );
	tag::Util::insert_new_elem_in_map ( W .core->hook,
		{ tag::isr_eigen_dir, static_cast < void * > ( eigen_dir ) } );

	it_V = V .core->hook .find ( tag::isr_radius );
	assert ( it_V != V .core->hook .end() );
	double * radius = new double ( * ( static_cast < double * > ( it_V->second ) ) );
	tag::Util::insert_new_elem_in_map ( W .core->hook,
		{ tag::isr_radius, static_cast < void * > ( radius ) } );

	it_V = V .core->hook .find ( tag::sqrt_det );
	assert ( it_V != V .core->hook .end() );
	double * sqrt_det_M = new double ( * ( static_cast < double * > ( it_V->second ) ) );
	tag::Util::insert_new_elem_in_map ( W .core->hook,
		{ tag::sqrt_det, static_cast < void * > ( sqrt_det_M ) } );

	tag::Util::Tensor < double > matr ( frontal_nb_of_coords, frontal_nb_of_coords );
	for ( size_t i = 0; i < frontal_nb_of_coords; i++ )
	for ( size_t j = 0; j < frontal_nb_of_coords; j++ )
		matr (i,j) = that->matrix (i,j) (W);
	double sq_radius = compute_isr_loc ( matr, * inv_matr, * eigen_dir, true );
	// last argument true means eigen_dir is meaningful as input, updated upon return

	* radius = approx_sqrt ( sq_radius, tag::around, * radius );

	* sqrt_det_M = approx_sqrt ( determinant ( matr ), tag::around, * sqrt_det_M );

}  // end of  tag::Util::Metric::Variable::Matrix::Simple::compute_data_5_codim_0
	

void tag::Util::Metric::Variable::Matrix::Simple::compute_data_3_codim_1
// static method, called through a pointer compute_inner_data_3
( const tag::Util::Metric::Variable::Matrix::Simple * that, const tag::AtPoint &, const Cell & V )

{	assert ( V .dim() == 0 );
	assert ( that );

	tag::Util::Tensor < double > matr ( frontal_nb_of_coords, frontal_nb_of_coords );
	for ( size_t i = 0; i < frontal_nb_of_coords; i++ )
	for ( size_t j = 0; j < frontal_nb_of_coords; j++ )
		matr (i,j) = that->matrix (i,j) (V);
	
	tag::Util::Tensor < double > * inv_matr =
		new tag::Util::Tensor < double > ( frontal_nb_of_coords, frontal_nb_of_coords );
  // candidate for inverse matrix
	double trace = 0.;
	for ( size_t i = 0; i < frontal_nb_of_coords; i++ )  trace += matr (i,i);
	trace = frontal_nb_of_coords / trace;
	for ( size_t i = 0; i < frontal_nb_of_coords; i++ )  ( * inv_matr ) (i,i) = trace;

	std::vector < double > * eigen_dir = new std::vector < double > ( frontal_nb_of_coords );
	double sq_radius = compute_isr_loc ( matr, * inv_matr, * eigen_dir, false );
	// last argument false means eigen_dir is not meaningful as input, only useful upon return

	double * radius = new double ( std::sqrt ( sq_radius ) );
	
	tag::Util::insert_new_elem_in_map ( V .core->hook,
		{ tag::isr_inv_matrix, static_cast < void * > ( inv_matr ) } );

	tag::Util::insert_new_elem_in_map ( V .core->hook,
		{ tag::isr_eigen_dir, static_cast < void * > ( eigen_dir ) } );

	tag::Util::insert_new_elem_in_map ( V .core->hook,
		{ tag::isr_radius, static_cast < void * > ( radius ) } );

}  // end of  tag::Util::Metric::Variable::Matrix::Simple::compute_data_3_codim_1


void tag::Util::Metric::Variable::Matrix::Simple::compute_data_5_codim_1
// static method, called through a pointer compute_inner_data_5
( const tag::Util::Metric::Variable::Matrix::Simple * that,
  const tag::AtPoint &, const Cell & W, const tag::UseInformationFrom &, const Cell & V )

{	assert ( V .dim() == 0 );
	assert ( W .dim() == 0 );
	assert ( that );

	std::map < tag::KeyForHook, void * > ::iterator it_V = V .core->hook .find ( tag::isr_inv_matrix );
	assert ( it_V != V .core->hook .end() );
	tag::Util::Tensor < double > * inv_matr = new tag::Util::Tensor < double >  // copy constructor
		( * ( static_cast < tag::Util::Tensor < double > * > ( it_V->second ) ) );
	tag::Util::insert_new_elem_in_map ( W .core->hook,
		{ tag::isr_inv_matrix, static_cast < void * > ( inv_matr ) } );

	it_V = V .core->hook .find ( tag::isr_eigen_dir );
	assert ( it_V != V .core->hook .end() );
	std::vector < double > * eigen_dir = new std::vector < double >   // copy constructor
		( * ( static_cast < std::vector < double > * > ( it_V->second ) ) );
	tag::Util::insert_new_elem_in_map ( W .core->hook,
		{ tag::isr_eigen_dir, static_cast < void * > ( eigen_dir ) } );

	it_V = V .core->hook .find ( tag::isr_radius );
	assert ( it_V != V .core->hook .end() );
	double * radius = new double ( * ( static_cast < double * > ( it_V->second ) ) );
	tag::Util::insert_new_elem_in_map ( W .core->hook,
		{ tag::isr_radius, static_cast < void * > ( radius ) } );

	tag::Util::Tensor < double > matr ( frontal_nb_of_coords, frontal_nb_of_coords );
	for ( size_t i = 0; i < frontal_nb_of_coords; i++ )
	for ( size_t j = 0; j < frontal_nb_of_coords; j++ )
		matr (i,j) = that->matrix (i,j) (W);
	double sq_radius = compute_isr_loc ( matr, * inv_matr, * eigen_dir, true );
	// last argument true means eigen_dir is meaningful as input, updated upon return

	* radius = approx_sqrt ( sq_radius, tag::around, * radius );

}  // end of  tag::Util::Metric::Variable::Matrix::Simple::compute_data_5_codim_1


void tag::Util::Metric::Variable::Matrix::SquareRoot::compute_data_3_forbid
// static method, called through a pointer compute_inner_data_3
( const tag::Util::Metric::Variable::Matrix::SquareRoot * that, const tag::AtPoint &, const Cell & V )
{	assert ( false );  }


void tag::Util::Metric::Variable::Matrix::SquareRoot::compute_data_5_forbid
// static method, called through a pointer compute_inner_data_5
( const tag::Util::Metric::Variable::Matrix::SquareRoot * that,
  const tag::AtPoint &, const Cell & W, const tag::UseInformationFrom &, const Cell & V )
{	assert ( false );  }


void tag::Util::Metric::Variable::Matrix::SquareRoot::compute_data_3_codim_0
// static method, called through a pointer compute_inner_data_3
( const tag::Util::Metric::Variable::Matrix::SquareRoot * that, const tag::AtPoint &, const Cell & V )
{	compute_data_sqrt_matrix ( that, tag::at_point, V );  }


void tag::Util::Metric::Variable::Matrix::SquareRoot::compute_data_5_codim_0
// static method, called through a pointer compute_inner_data_5
( const tag::Util::Metric::Variable::Matrix::SquareRoot * that,
  const tag::AtPoint &, const Cell & W, const tag::UseInformationFrom &, const Cell & V )
{	compute_data_sqrt_matrix ( that, tag::at_point, W, tag::use_information_from, V );  }


void tag::Util::Metric::Variable::Matrix::SquareRoot::compute_data_3_codim_1
// static method, called through a pointer compute_inner_data_3
( const tag::Util::Metric::Variable::Matrix::SquareRoot * that, const tag::AtPoint &, const Cell & V )
{	assert ( false );  // to be tested
	compute_data_sqrt_matrix ( that, tag::at_point, V );  }


void tag::Util::Metric::Variable::Matrix::SquareRoot::compute_data_5_codim_1
// static method, called through a pointer compute_inner_data_5
( const tag::Util::Metric::Variable::Matrix::SquareRoot * that,
  const tag::AtPoint &, const Cell & W, const tag::UseInformationFrom &, const Cell & V )
{	assert ( false );  // to be tested
	compute_data_sqrt_matrix ( that, tag::at_point, W, tag::use_information_from, V );  }


void tag::Util::Metric::Variable::Matrix::ISR::compute_data_3_forbid
// static method, called through a pointer compute_inner_data_3
( const tag::Util::Metric::Variable::Matrix::ISR * that, const tag::AtPoint &, const Cell & V )
{	assert ( false );  }


void tag::Util::Metric::Variable::Matrix::ISR::compute_data_5_forbid
// static method, called through a pointer compute_inner_data_5
( const tag::Util::Metric::Variable::Matrix::ISR * that,
  const tag::AtPoint &, const Cell & W, const tag::UseInformationFrom &, const Cell & V )
{	assert ( false );  }


void tag::Util::Metric::Variable::Matrix::ISR::compute_data_3_codim_0
// static method, called through a pointer compute_inner_data_3
( const tag::Util::Metric::Variable::Matrix::ISR * that, const tag::AtPoint &, const Cell & V )

{	assert ( V .dim() == 0 );
	assert ( that );

	tag::Util::Tensor < double > matr ( frontal_nb_of_coords, frontal_nb_of_coords );
	for ( size_t i = 0; i < frontal_nb_of_coords; i++ )
	for ( size_t j = 0; j < frontal_nb_of_coords; j++ )
		matr (i,j) = that->matrix (i,j) (V);
	const double tmp = that->isr (V);
	for ( size_t i = 0; i < frontal_nb_of_coords; i++ )  matr (i,i) += tmp;

	double * sqrt_det_M = new double ( std::sqrt ( determinant ( matr ) ) );
	
	tag::Util::Tensor < double > * inv_matr =
		new tag::Util::Tensor < double > ( frontal_nb_of_coords, frontal_nb_of_coords );
  // candidate for inverse matrix
	double trace = 0.;
	for ( size_t i = 0; i < frontal_nb_of_coords; i++ )  trace += matr (i,i);
	trace = frontal_nb_of_coords / trace;
	for ( size_t i = 0; i < frontal_nb_of_coords; i++ )  ( * inv_matr ) (i,i) = trace;

	tag::Util::improve_inverse_matrix ( matr, * inv_matr );

	tag::Util::insert_new_elem_in_map ( V .core->hook,
		{ tag::isr_radius, static_cast < void * > ( new double ( std::sqrt ( that->isr (V) ) ) ) } ) ;

	tag::Util::insert_new_elem_in_map ( V .core->hook,
		{ tag::isr_inv_matrix, static_cast < void * > ( inv_matr ) } );
	
	tag::Util::insert_new_elem_in_map ( V .core->hook,
		{ tag::sqrt_det, static_cast < void * > ( sqrt_det_M ) } );

}  // end of  tag::Util::Metric::Variable::Matrix::ISR::compute_data_3_codim_0


void tag::Util::Metric::Variable::Matrix::ISR::compute_data_5_codim_0
// static method, called through a pointer compute_inner_data_5
( const tag::Util::Metric::Variable::Matrix::ISR * that,
  const tag::AtPoint &, const Cell & W, const tag::UseInformationFrom &, const Cell & V )

{	assert ( V .dim() == 0 );
	assert ( W .dim() == 0 );
	assert ( that );

	std::map < tag::KeyForHook, void * > ::iterator it_V = V .core->hook .find ( tag::isr_radius );
	assert ( it_V != V .core->hook .end() );
	double * radius = new double ( * ( static_cast < double * > ( it_V->second ) ) );
	tag::Util::insert_new_elem_in_map ( W .core->hook,
		{ tag::isr_radius, static_cast < void * > ( radius ) } );
	* radius = approx_sqrt ( that->isr (W), tag::around, * radius );

	tag::Util::Tensor < double > matr ( frontal_nb_of_coords, frontal_nb_of_coords );
	for ( size_t i = 0; i < frontal_nb_of_coords; i++ )
	for ( size_t j = 0; j < frontal_nb_of_coords; j++ )
		matr (i,j) = that->matrix (i,j) (W);
	const double tmp = that->isr (W);
	for ( size_t i = 0; i < frontal_nb_of_coords; i++ )  matr (i,i) += tmp;

	it_V = V .core->hook .find ( tag::isr_inv_matrix );
	assert ( it_V != V .core->hook .end() );
	tag::Util::Tensor < double > * inv_matr = new tag::Util::Tensor < double >  // copy constructor
		( * ( static_cast < tag::Util::Tensor < double > * > ( it_V->second ) ) );
	tag::Util::insert_new_elem_in_map ( W .core->hook,
		{ tag::isr_inv_matrix, static_cast < void * > ( inv_matr ) } );

	tag::Util::improve_inverse_matrix ( matr, * inv_matr );

	it_V = V .core->hook .find ( tag::sqrt_det );
	assert ( it_V != V .core->hook .end() );
	double * sqrt_det_M = new double ( * ( static_cast < double * > ( it_V->second ) ) );
	tag::Util::insert_new_elem_in_map ( W .core->hook,
		{ tag::sqrt_det, static_cast < void * > ( sqrt_det_M ) } );

	* sqrt_det_M = approx_sqrt ( determinant ( matr ), tag::around, * sqrt_det_M );

}  // end of  tag::Util::Metric::Variable::Matrix::ISR::compute_data_5_codim_0


void tag::Util::Metric::Variable::Matrix::ISR::compute_data_3_codim_1     // virtual, override
// static method, called through a pointer compute_inner_data_3
( const tag::Util::Metric::Variable::Matrix::ISR * that, const tag::AtPoint &, const Cell & V )

{	assert ( false );  // to be tested
	assert ( V .dim() == 0 );
	assert ( that );

	tag::Util::insert_new_elem_in_map ( V .core->hook,
		{ tag::isr_radius, static_cast < void * > ( new double ( std::sqrt ( that->isr (V) ) ) ) } );

}  // end of  tag::Util::Metric::Variable::Matrix::ISR::compute_data_3_codim_1

	
void tag::Util::Metric::Variable::Matrix::ISR::compute_data_5_codim_1
// static method, called through a pointer compute_inner_data_5
( const tag::Util::Metric::Variable::Matrix::ISR * that,
  const tag::AtPoint &, const Cell & W, const tag::UseInformationFrom &, const Cell & V )

{	assert ( false );  // to be tested
	assert ( V .dim() == 0 );
	assert ( W .dim() == 0 );
	assert ( that );

	std::map < tag::KeyForHook, void * > ::iterator it_V = V .core->hook .find ( tag::isr_radius );
	assert ( it_V != V .core->hook .end() );
	double * radius = new double ( * ( static_cast < double * > ( it_V->second ) ) );
	tag::Util::insert_new_elem_in_map ( W .core->hook,
		{ tag::isr_radius, static_cast < void * > ( radius ) } );
	* radius = approx_sqrt ( that->isr (W), tag::around, * radius );

}  // end of  tag::Util::Metric::Variable::Matrix::ISR::compute_data_5_codim_1


void tag::Util::Metric::Variable::Matrix::SquareRoot::ISR::compute_data_3_forbid
// static method, called through a pointer compute_inner_data_3
( const tag::Util::Metric::Variable::Matrix::SquareRoot::ISR * that,
  const tag::AtPoint &, const Cell & V                              )
{	assert ( false );  }


void tag::Util::Metric::Variable::Matrix::SquareRoot::ISR::compute_data_5_forbid
// static method, called through a pointer compute_inner_data_5
( const tag::Util::Metric::Variable::Matrix::SquareRoot::ISR * that,
  const tag::AtPoint &, const Cell & W, const tag::UseInformationFrom &, const Cell & V )
{	assert ( false );  }


void tag::Util::Metric::Variable::Matrix::SquareRoot::ISR::compute_data_3_codim_0
// static method, called through a pointer compute_inner_data_3
( const tag::Util::Metric::Variable::Matrix::SquareRoot::ISR * that,
  const tag::AtPoint &, const Cell & V                              )

{	assert ( V .dim() == 0 );
	assert ( that );

	tag::Util::Tensor < double > matr ( frontal_nb_of_coords, frontal_nb_of_coords );
	for ( size_t i = 0; i < frontal_nb_of_coords; i++ )
	for ( size_t j = 0; j < frontal_nb_of_coords; j++ )
		matr (i,j) = that->matrix (i,j) (V);
	const double tmp = that->isr (V);
	for ( size_t i = 0; i < frontal_nb_of_coords; i++ )  matr (i,i) += tmp;

	tag::Util::Tensor < double > * inv_matr =
		new tag::Util::Tensor < double > ( frontal_nb_of_coords, frontal_nb_of_coords );
  // candidate for inverse matrix
	double trace = 0.;
	for ( size_t i = 0; i < frontal_nb_of_coords; i++ )  trace += matr (i,i);
	trace = frontal_nb_of_coords / trace;
	for ( size_t i = 0; i < frontal_nb_of_coords; i++ )  ( * inv_matr ) (i,i) = trace;

	tag::Util::improve_inverse_matrix ( matr, * inv_matr );

	tag::Util::insert_new_elem_in_map ( V .core->hook,
		{ tag::isr_inv_matrix, static_cast < void * > ( inv_matr ) } );

}  // end of  tag::Util::Metric::Variable::Matrix::SquareRoot::ISR::compute_data_3_codim_0


void tag::Util::Metric::Variable::Matrix::SquareRoot::ISR::compute_data_5_codim_0
// static method, called through a pointer compute_inner_data_5
( const tag::Util::Metric::Variable::Matrix::SquareRoot::ISR * that,
  const tag::AtPoint &, const Cell & W, const tag::UseInformationFrom &, const Cell & V )

{	assert ( V .dim() == 0 );
	assert ( W .dim() == 0 );
	assert ( that );

	tag::Util::Tensor < double > matr ( frontal_nb_of_coords, frontal_nb_of_coords );
	for ( size_t i = 0; i < frontal_nb_of_coords; i++ )
	for ( size_t j = 0; j < frontal_nb_of_coords; j++ )
		matr (i,j) = that->matrix (i,j) (W);
	const double tmp = that->isr (W);
	for ( size_t i = 0; i < frontal_nb_of_coords; i++ )  matr (i,i) += tmp;

	std::map < tag::KeyForHook, void * > ::iterator it_V =
		V .core->hook .find ( tag::isr_inv_matrix );
	assert ( it_V != V .core->hook .end() );
	tag::Util::Tensor < double > * inv_matr = new tag::Util::Tensor < double >  // copy constructor
		( * ( static_cast < tag::Util::Tensor < double > * > ( it_V->second ) ) );
	tag::Util::insert_new_elem_in_map ( W .core->hook,
		{ tag::isr_inv_matrix, static_cast < void * > ( inv_matr ) } );

	tag::Util::improve_inverse_matrix ( matr, * inv_matr );
	
}  // end of  tag::Util::Metric::Variable::Matrix::SquareRoot::ISR::compute_data_5_codim_0


void tag::Util::Metric::Variable::Matrix::SquareRoot::ISR::compute_data_3_codim_1
// static method, called through a pointer compute_inner_data_3
( const tag::Util::Metric::Variable::Matrix::SquareRoot::ISR * that,
  const tag::AtPoint &, const Cell & V                              )
{	assert ( false );  }   // to be tested


void tag::Util::Metric::Variable::Matrix::SquareRoot::SquareRoot::ISR::compute_data_5_codim_1
// static method, called through a pointer compute_inner_data_5
( const tag::Util::Metric::Variable::Matrix::SquareRoot::ISR * that,
  const tag::AtPoint &, const Cell & W, const tag::UseInformationFrom &, const Cell & V )
{	assert ( false );  }   // to be tested

//------------------------------------------------------------------------------------------------------//


void tag::Util::Metric::Variable::Matrix::SquareRoot::ISR::de_activate ( )
// virtual from tag::Util::Metric::Core, here overridden
{	this->compute_inner_data_3 =
		& tag::Util::Metric::Variable::Matrix::SquareRoot::ISR::compute_data_3_forbid;
	this->compute_inner_data_5 =
		& tag::Util::Metric::Variable::Matrix::SquareRoot::ISR::compute_data_5_forbid;  }

//------------------------------------------------------------------------------------------------------//


// we correct the exterior product
// see comments in manifold.h before class tag::Util::Metric::Core

void tag::Util::Metric::Constant::Isotropic::correct_ext_prod_2d
( const Cell & P, std::vector < double > & n )  // virtual from tag::Util::MetricCore
{	assert ( false );  }   // never executed


void tag::Util::Metric::Constant::Isotropic::correct_ext_prod_3d
( const Cell & P, std::vector < double > & n )  // virtual from tag::Util::MetricCore
{	assert ( false );  }   // never executed


void tag::Util::Metric::Constant::Matrix::Simple::correct_ext_prod_2d
( const Cell & P, std::vector < double > & n )  // virtual from tag::Util::MetricCore
{	assert ( false );  }   // to implement


void tag::Util::Metric::Constant::Matrix::Simple::correct_ext_prod_3d
( const Cell & P, std::vector < double > & n )  // virtual from tag::Util::MetricCore
{	assert ( false );  }   // to implement


void tag::Util::Metric::Constant::Matrix::SquareRoot::correct_ext_prod_2d
( const Cell & P, std::vector < double > & n )  // virtual from tag::Util::MetricCore
{	assert ( false );  }   // to implement


void tag::Util::Metric::Constant::Matrix::SquareRoot::correct_ext_prod_3d
( const Cell & P, std::vector < double > & n )  // virtual from tag::Util::MetricCore
{	assert ( false );  }   // to implement


void tag::Util::Metric::Variable::Isotropic::correct_ext_prod_2d
( const Cell & P, std::vector < double > & n )  // virtual from tag::Util::MetricCore
{	assert ( false );  }   // never executed


void tag::Util::Metric::Variable::Isotropic::correct_ext_prod_3d
( const Cell & P, std::vector < double > & n )  // virtual from tag::Util::MetricCore
{	assert ( false );  }   // never executed


void tag::Util::Metric::Variable::Matrix::Simple::correct_ext_prod_2d
( const Cell & P, std::vector < double > & n )  // virtual from tag::Util::MetricCore

// 'n' is a tangent vector (a segment) rotated by 90 deg
// since the metric is anisotropic, we must correct it by multiplying (to the left)
// with  sqrt det M  M^-1

{	assert ( P .dim() == 0 );
	assert ( n .size() == frontal_nb_of_coords );
	std::map < tag::KeyForHook, void * > ::iterator it = P .core->hook .find ( tag::isr_inv_matrix );
	assert ( it != P .core->hook .end() );
	tag::Util::Tensor < double > & inv_matr =
		* ( static_cast < tag::Util::Tensor < double > * > ( it->second ) );
	it = P .core->hook .find ( tag::sqrt_det );
	assert ( it != P .core->hook .end() );
	double & sqrt_det_M = * ( static_cast < double * > ( it->second ) );
	std::vector < double > nn = n;
	for ( size_t i = 0; i < frontal_nb_of_coords; i++ )
	{	double & sum = n[i];
		sum = 0.;
		for ( size_t j = 0; j < frontal_nb_of_coords; j++ )
			sum += inv_matr (i,j) * nn[j];
		sum *= sqrt_det_M;                                   }                                           }


void tag::Util::Metric::Variable::Matrix::Simple::correct_ext_prod_3d
( const Cell & P, std::vector < double > & n )  // virtual from tag::Util::MetricCore
{	assert ( false );  }   // to implement


void tag::Util::Metric::Variable::Matrix::SquareRoot::correct_ext_prod_2d
( const Cell & P, std::vector < double > & n )  // virtual from tag::Util::MetricCore

// 'n' is a tangent vector (a segment) rotated by 90 deg
// since the metric is anisotropic, we must correct it by multiplying (to the left)
// with  det A  A^-2

{	assert ( P .dim() == 0 );
	assert ( n .size() == frontal_nb_of_coords );
	tag::Util::Tensor < double > matr ( frontal_nb_of_coords, frontal_nb_of_coords );
	for ( size_t i = 0; i < frontal_nb_of_coords; i++ )
	for ( size_t j = 0; j < frontal_nb_of_coords; j++ )
		matr (i,j) = this->matrix (i,j) (P);
	const double det_A = determinant ( matr );
	std::map < tag::KeyForHook, void * > ::iterator it = P .core->hook .find ( tag::isr_inv_matrix );
	assert ( it != P .core->hook .end() );
	tag::Util::Tensor < double > & inv_matr =
		* ( static_cast < tag::Util::Tensor < double > * > ( it->second ) );
	std::vector < double > nn = n;
	for ( size_t i = 0; i < frontal_nb_of_coords; i++ )
	{	double & sum = n[i];
		sum = 0.;
		for ( size_t j = 0; j < frontal_nb_of_coords; j++ )
			sum += inv_matr (i,j) * nn[j];                     }
	nn = n;
	for ( size_t i = 0; i < frontal_nb_of_coords; i++ )
	{	double & sum = n[i];
		sum = 0.;
		for ( size_t j = 0; j < frontal_nb_of_coords; j++ )
			sum += inv_matr (i,j) * nn[j];
		sum *= det_A;                                        }                                            }


void tag::Util::Metric::Variable::Matrix::SquareRoot::correct_ext_prod_3d
( const Cell & P, std::vector < double > & n )  // virtual from tag::Util::MetricCore
{	assert ( false );  }   // to implement


void tag::Util::Metric::Variable::Matrix::ISR::correct_ext_prod_2d
( const Cell & P, std::vector < double > & n )  // virtual from tag::Util::MetricCore

// 'n' is a tangent vector (a segment) rotated by 90 deg
// since the metric is anisotropic, we must correct it by multiplying (to the left)
// with  sqrt det M  M^-1

{	assert ( P .dim() == 0 );
	assert ( n .size() == frontal_nb_of_coords );
	std::map < tag::KeyForHook, void * > ::iterator it = P .core->hook .find ( tag::isr_inv_matrix );
	assert ( it != P .core->hook .end() );
	tag::Util::Tensor < double > & inv_matr =
		* ( static_cast < tag::Util::Tensor < double > * > ( it->second ) );
	it = P .core->hook .find ( tag::sqrt_det );
	assert ( it != P .core->hook .end() );
	double & sqrt_det_M = * ( static_cast < double * > ( it->second ) );
	std::vector < double > nn = n;
	for ( size_t i = 0; i < frontal_nb_of_coords; i++ )
	{	double & sum = n[i];
		sum = 0.;
		for ( size_t j = 0; j < frontal_nb_of_coords; j++ )
			sum += inv_matr (i,j) * nn[j];
		sum *= sqrt_det_M;                                   }                                           }


void tag::Util::Metric::Variable::Matrix::ISR::correct_ext_prod_3d
( const Cell & P, std::vector < double > & n )  // virtual from tag::Util::MetricCore
{	assert ( false );  }   // to implement


void tag::Util::Metric::Variable::Matrix::SquareRoot::ISR::correct_ext_prod_2d
( const Cell & P, std::vector < double > & n )    // virtual from tag::Util::MetricCore

// 'n' is a tangent vector (a segment) rotated by 90 deg
// since the metric is anisotropic, we must correct it by multiplying (to the left)
// with  det A  A^-2

{	assert ( P .dim() == 0 );
	assert ( n .size() == frontal_nb_of_coords );
	tag::Util::Tensor < double > matr ( frontal_nb_of_coords, frontal_nb_of_coords );
	for ( size_t i = 0; i < frontal_nb_of_coords; i++ )
	for ( size_t j = 0; j < frontal_nb_of_coords; j++ )
		matr (i,j) = this->matrix (i,j) (P);
	const double tmp = this->isr (P);
	for ( size_t i = 0; i < frontal_nb_of_coords; i++ )  matr (i,i) += tmp;
	const double det_A = determinant ( matr );
	std::map < tag::KeyForHook, void * > ::iterator it = P .core->hook .find ( tag::isr_inv_matrix );
	assert ( it != P .core->hook .end() );
	tag::Util::Tensor < double > & inv_matr =
		* ( static_cast < tag::Util::Tensor < double > * > ( it->second ) );
	std::vector < double > nn = n;
	for ( size_t i = 0; i < frontal_nb_of_coords; i++ )
	{	double & sum = n[i];
		sum = 0.;
		for ( size_t j = 0; j < frontal_nb_of_coords; j++ )
			sum += inv_matr (i,j) * nn[j];                     }
	nn = n;
	for ( size_t i = 0; i < frontal_nb_of_coords; i++ )
	{	double & sum = n[i];
		sum = 0.;
		for ( size_t j = 0; j < frontal_nb_of_coords; j++ )
			sum += inv_matr (i,j) * nn[j];
		sum *= det_A;                                        }                                            }


void tag::Util::Metric::Variable::Matrix::SquareRoot::ISR::correct_ext_prod_3d
( const Cell & P, std::vector < double > & n )    // virtual from tag::Util::MetricCore
{	assert ( false );  }   // to implement


void tag::Util::Metric::ScaledSq::correct_ext_prod_2d
( const Cell & P, std::vector < double > & n )  // virtual from tag::Util::MetricCore
{	assert ( false );  }   // never executed


void tag::Util::Metric::ScaledSq::correct_ext_prod_3d
( const Cell & P, std::vector < double > & n )  // virtual from tag::Util::MetricCore
{	assert ( false );  }   // never executed


void tag::Util::Metric::ScaledSqInv::correct_ext_prod_2d
( const Cell & P, std::vector < double > & n )  // virtual from tag::Util::MetricCore
{	assert ( false );  }   // never executed


void tag::Util::Metric::ScaledSqInv::correct_ext_prod_3d
( const Cell & P, std::vector < double > & n )  // virtual from tag::Util::MetricCore
{	assert ( false );  }   // never executed

//------------------------------------------------------------------------------------------------------//
//------------------------------------------------------------------------------------------------------//


double tag::Util::Metric::Variable::Matrix::Simple::inner_spectral_radius ( const Cell & V )
{	assert ( V .dim() == 0 );        // virtual from tag::Util::Metric::Core
	std::map < tag::KeyForHook, void * > ::iterator it_V = V .core->hook .find ( tag::isr_radius );
	assert ( it_V != V .core->hook .end() );
	return * ( static_cast < double * > ( it_V->second ) );                                         }


double tag::Util::Metric::Variable::Matrix::SquareRoot::inner_spectral_radius ( const Cell & V )
{	assert ( V .dim() == 0 );        // virtual from tag::Util::Metric::Core
	std::map < tag::KeyForHook, void * > ::iterator it_V = V .core->hook .find ( tag::isr_radius );
	assert ( it_V != V .core->hook .end() );
	return * ( static_cast < double * > ( it_V->second ) );                                         }


double tag::Util::Metric::Variable::Matrix::ISR::inner_spectral_radius ( const Cell & V )
{	assert ( V .dim() == 0 );        // virtual from tag::Util::Metric::Core
	std::map < tag::KeyForHook, void * > ::iterator it_V = V .core->hook .find ( tag::isr_radius );
	assert ( it_V != V .core->hook .end() );
	return * ( static_cast < double * > ( it_V->second ) );                                         }


double tag::Util::Metric::Variable::Matrix::SquareRoot::ISR::inner_spectral_radius ( const Cell & V )
{	return this->isr (V);  }      // virtual from tag::Util::Metric::Core

//------------------------------------------------------------------------------------------------------//


void tag::Util::Metric::Core::remove_node_isr_forbid ( const Cell & V )
// static method, called through pointer remove_node_isr_p
{	assert ( false );  }
	

void tag::Util::Metric::Variable::Matrix::Simple::remove_node_isr_codim_0 ( const Cell & V )
// static method, called through pointer remove_node_isr_p

{	assert ( V .dim() == 0 );

	std::map < tag::KeyForHook, void * > ::iterator it_V = V .core->hook .find ( tag::isr_inv_matrix );
	assert ( it_V != V .core->hook .end() );
	V .core->hook .erase ( it_V );
	delete static_cast < tag::Util::Tensor < double > * > ( it_V->second );
	
	it_V = V .core->hook .find ( tag::isr_eigen_dir );
	assert ( it_V != V .core->hook .end() );
	V .core->hook .erase ( it_V );
	delete static_cast < std::vector < double > * > ( it_V->second );

	it_V = V .core->hook .find ( tag::isr_radius );
	assert ( it_V != V .core->hook .end() );
	V .core->hook .erase ( it_V );
	delete static_cast < double * > ( it_V->second );

	it_V = V .core->hook .find ( tag::sqrt_det );
	assert ( it_V != V .core->hook .end() );
	V .core->hook .erase ( it_V );
	delete static_cast < double * > ( it_V->second );

}  // end of  tag::Util::Metric::Variable::Matrix::Simple::remove_node_isr_codim_0


void tag::Util::Metric::Variable::Matrix::Simple::remove_node_isr_codim_1 ( const Cell & V )
// static method, called through pointer remove_node_isr_p

{	assert ( V .dim() == 0 );

	std::map < tag::KeyForHook, void * > ::iterator it_V = V .core->hook .find ( tag::isr_inv_matrix );
	assert ( it_V != V .core->hook .end() );
	V .core->hook .erase ( it_V );
	delete static_cast < tag::Util::Tensor < double > * > ( it_V->second );
	
	it_V = V .core->hook .find ( tag::isr_eigen_dir );
	assert ( it_V != V .core->hook .end() );
	V .core->hook .erase ( it_V );
	delete static_cast < std::vector < double > * > ( it_V->second );

	it_V = V .core->hook .find ( tag::isr_radius );
	assert ( it_V != V .core->hook .end() );
	V .core->hook .erase ( it_V );
	delete static_cast < double * > ( it_V->second );

}  // end of  tag::Util::Metric::Variable::Matrix::Simple::remove_node_isr_codim_1


void tag::Util::Metric::Variable::Matrix::SquareRoot::remove_node_isr_codim_0 ( const Cell & V )
// static method, called through pointer remove_node_isr_p

{	assert ( V .dim() == 0 );

	std::map < tag::KeyForHook, void * > ::iterator it_V = V .core->hook .find ( tag::isr_inv_matrix );
	assert ( it_V != V .core->hook .end() );
	V .core->hook .erase ( it_V );
	delete static_cast < tag::Util::Tensor < double > * > ( it_V->second );
	
	it_V = V .core->hook .find ( tag::isr_eigen_dir );
	assert ( it_V != V .core->hook .end() );
	V .core->hook .erase ( it_V );
	delete static_cast < std::vector < double > * > ( it_V->second );

	it_V = V .core->hook .find ( tag::isr_radius );
	assert ( it_V != V .core->hook .end() );
	V .core->hook .erase ( it_V );
	delete static_cast < double * > ( it_V->second );

}  // end of  tag::Util::Metric::Variable::Matrix::SquareRoot::remove_node_isr_codim_0
	

void tag::Util::Metric::Variable::Matrix::SquareRoot::remove_node_isr_codim_1 ( const Cell & V )
// static method, called through pointer remove_node_isr_p

{	assert ( V .dim() == 0 );

	std::map < tag::KeyForHook, void * > ::iterator it_V = V .core->hook .find ( tag::isr_inv_matrix );
	assert ( it_V != V .core->hook .end() );
	V .core->hook .erase ( it_V );
	delete static_cast < tag::Util::Tensor < double > * > ( it_V->second );
	
	it_V = V .core->hook .find ( tag::isr_eigen_dir );
	assert ( it_V != V .core->hook .end() );
	V .core->hook .erase ( it_V );
	delete static_cast < std::vector < double > * > ( it_V->second );

	it_V = V .core->hook .find ( tag::isr_radius );
	assert ( it_V != V .core->hook .end() );
	V .core->hook .erase ( it_V );
	delete static_cast < double * > ( it_V->second );

}  // end of  tag::Util::Metric::Variable::Matrix::SquareRoot::remove_node_isr_codim_1
	

void tag::Util::Metric::Variable::Matrix::ISR::remove_node_isr_codim_0 ( const Cell & V )
// static method, called through pointer remove_node_isr_p
	
{	assert ( V .dim() == 0 );

	std::map < tag::KeyForHook, void * > ::iterator it_V = V .core->hook .find ( tag::isr_radius );
	assert ( it_V != V .core->hook .end() );
	V .core->hook .erase ( it_V );
	delete static_cast < double * > ( it_V->second );

	it_V = V .core->hook .find ( tag::isr_inv_matrix );
	assert ( it_V != V .core->hook .end() );
	V .core->hook .erase ( it_V );
	delete static_cast < tag::Util::Tensor < double > * > ( it_V->second );
	
	it_V = V .core->hook .find ( tag::sqrt_det );
	assert ( it_V != V .core->hook .end() );
	V .core->hook .erase ( it_V );
	delete static_cast < double * > ( it_V->second );

}  // end of  tag::Util::Metric::Variable::Matrix::ISR::remove_node_isr_codim_0


void tag::Util::Metric::Variable::Matrix::ISR::remove_node_isr_codim_1 ( const Cell & V )
// virtual from tag::Util::Metric::Core, here overridden
	
{	assert ( V .dim() == 0 );

	std::map < tag::KeyForHook, void * > ::iterator it_V = V .core->hook .find ( tag::isr_radius );
	assert ( it_V != V .core->hook .end() );
	V .core->hook .erase ( it_V );
	delete static_cast < double * > ( it_V->second );

}  // end of  tag::Util::Metric::Variable::Matrix::ISR::remove_node_isr_codim_1


void tag::Util::Metric::Variable::Matrix::SquareRoot::ISR::remove_node_isr_codim_0 ( const Cell & V )
// virtual from tag::Util::Metric::Core, here overridden

{	assert ( V .dim() == 0 );

	std::map < tag::KeyForHook, void * > ::iterator it_V = V .core->hook .find ( tag::isr_inv_matrix );
	assert ( it_V != V .core->hook .end() );
	V .core->hook .erase ( it_V );
	delete static_cast < tag::Util::Tensor < double > * > ( it_V->second );
	
}  // end of  tag::Util::Metric::Variable::Matrix::SquareRoot::ISR::remove_node_isr_codim_0


void tag::Util::Metric::Variable::Matrix::SquareRoot::ISR::remove_node_isr_codim_1 ( const Cell & V )
// virtual from tag::Util::Metric::Core, here overridden, does nothing
{	}
	
//------------------------------------------------------------------------------------------------------//
//------------------------------------------------------------------------------------------------------//

	
Cell Manifold::Euclid::search_start_ver ( )  // virtual from  Manifold::Core
// we return a non-existent cell just to avoid compilation errors
{	assert ( false );  return Cell ( tag::non_existent );  }


Cell Manifold::Implicit::OneEquation::search_start_ver ( )  // virtual from  Manifold::Core

// search for a starting point in a manifold of co-dimension one
// e.g. a curve in the plane or a surface in 3D

{	std::default_random_engine random_generator;
	Cell tmp_ver ( tag::vertex );
	Cell tmp_ver_1 ( tag::vertex );
	Cell tmp_ver_2 ( tag::vertex );
	
	// this function is not called at some point in the manifold
	// (that's why it is here, to find a starting point)
	// we could compute the desired_length by using the metric at the origin of the space
	// but in order to reduce the risk of falling into a singularity
	// we prefer to introduce some randomness
	std::uniform_real_distribution < double > distr11 ( -1., 1. );
	for ( size_t i = 0; i < frontal_nb_of_coords; i++ )
	{	const Function & x = Manifold::working .coordinates() [i];
		x ( tmp_ver ) = distr11 ( random_generator );               }
	// recall the metric has been rescaled
	
	assert ( Manifold::working .core );
	assert ( Manifold::working .core->metric .core );
	double desired_length = 1. / Manifold::working .core->metric .core->avrg_spectral_radius ( tmp_ver );

	size_t size_of_cube = 5;
	while ( true )
	{	double s = size_of_cube * desired_length;
		std::uniform_real_distribution < double > distr ( -s, s );
		size_t nb = 1;
		for ( size_t j = 0; j < frontal_nb_of_coords; j++ ) nb *= size_of_cube;
		for ( size_t j = 0; j < nb; j++ )
		{	for ( size_t i = 0; i < frontal_nb_of_coords; i++ )
			{	const Function & x = Manifold::working .coordinates() [i];
				x ( tmp_ver_1 ) = distr ( random_generator );
				x ( tmp_ver_2 ) = distr ( random_generator );               }
			double v1 = this->level_function ( tmp_ver_1 ),
			       v2 = this->level_function ( tmp_ver_2 );
			if ( opposite_signs ( v1, v2 ) )
				// refine by applying bissection algorithm
				while ( true )
				{	if ( Manifold::working .sq_dist ( tmp_ver_1, tmp_ver_2 ) < 1. )
					{	Manifold::working .project ( tmp_ver_1 );
						return tmp_ver_1;                         }
					this->surrounding_space .interpolate ( tmp_ver, 0.5, tmp_ver_1, 0.5, tmp_ver_2 );
					double v = this->level_function ( tmp_ver );
					if ( opposite_signs ( v, v2 ) )
					{	for ( size_t i = 0; i < frontal_nb_of_coords; i++ )
						{	const Function & x = Manifold::working .coordinates() [i];
							x ( tmp_ver_1 ) = x ( tmp_ver );                            }
						v1 = v;                                                          }
					else if ( opposite_signs ( v, v1 ) )
					{	for ( size_t i = 0; i < frontal_nb_of_coords; i++ )
						{	const Function & x = Manifold::working .coordinates() [i];
							x ( tmp_ver_2 ) = x ( tmp_ver );                            }
						v2 = v;                                                           }
					else  assert( false );                                                  }  }
		size_of_cube *= 2;                                                                          }

}  // end of  Manifold::Implicit::OneEquation::search_start_ver


Cell Manifold::Implicit::TwoEquations::search_start_ver ( )  // virtual from  Manifold::Core

// search for a starting point in a manifold of co-dimension two (a curve in 3D)

{	// this function is not called at some point in the manifold
	// (thats's why it is here, to find a starting point)
	// we could compute the desired_length by using the metric at the origin of the space
	// but in order to reduce the risk of falling into a singularity
	// we prefer to introduce some randomness
	std::default_random_engine random_generator;
	std::uniform_real_distribution < double > distr11 ( -1., 1. );
	Cell tmp_A ( tag::vertex );
	Cell tmp_B ( tag::vertex );
	Cell tmp_C ( tag::vertex );
	Cell tmp_AB ( tag::vertex );
	Cell tmp_BC ( tag::vertex );
	Cell tmp_CA ( tag::vertex );
	for ( size_t i = 0; i < frontal_nb_of_coords; i++ )
	{	const Function & x = Manifold::working .coordinates() [i];
		x ( tmp_A ) = distr11 ( random_generator );                }
	// recall the metric has been rescaled
	assert ( Manifold::working .core );
	assert ( Manifold::working .core->metric .core );
	double desired_length = 1. / Manifold::working .core->metric .core->avrg_spectral_radius ( tmp_A );
	
	size_t size_of_cube = 5;
	while ( true )
	{	double s = size_of_cube * desired_length;
		std::uniform_real_distribution < double > distr ( -s, s );
		size_t nb = 1;
		restart :
		for ( size_t j = 0; j < frontal_nb_of_coords; j++ ) nb *= size_of_cube;
		for ( size_t j = 0; j < nb; j++ )
		{	for ( size_t i = 0; i < frontal_nb_of_coords; i++ )
			{	const Function & x = Manifold::working .coordinates() [i];
				x ( tmp_A ) = distr ( random_generator );
				x ( tmp_B ) = distr ( random_generator );
				x ( tmp_C ) = distr ( random_generator );            }
			double vA1 = this->level_function_1 ( tmp_A ),
			       vA2 = this->level_function_2 ( tmp_A );
			double vB1 = this->level_function_1 ( tmp_B ),
			       vB2 = this->level_function_2 ( tmp_B );
			double vC1 = this->level_function_1 ( tmp_C ),
			       vC2 = this->level_function_2 ( tmp_C );
			if ( not origin_outside ( vA1, vA2, vB1, vB2, vC1, vC2 ) )
				// refine by repeatedly cutting the triangle
				while ( true )
				{	if ( ( Manifold::working .sq_dist ( tmp_A, tmp_B ) < 1. ) and
					     ( Manifold::working .sq_dist ( tmp_B, tmp_C ) < 1. ) and
					     ( Manifold::working .sq_dist ( tmp_C, tmp_A ) < 1. )     )
					{	Manifold::working .project ( tmp_A );
						return tmp_A;                         }
					this->surrounding_space .interpolate ( tmp_AB, 0.5, tmp_A, 0.5, tmp_B );
					double vAB1 = this->level_function_1 ( tmp_AB ),
					       vAB2 = this->level_function_2 ( tmp_AB );
				  this->surrounding_space .interpolate ( tmp_BC, 0.5, tmp_B, 0.5, tmp_C );
					double vBC1 = this->level_function_1 ( tmp_BC ),
					       vBC2 = this->level_function_2 ( tmp_BC );
				  this->surrounding_space .interpolate ( tmp_CA, 0.5, tmp_C, 0.5, tmp_A );
					double vCA1 = this->level_function_1 ( tmp_CA ),
					       vCA2 = this->level_function_2 ( tmp_CA );
					if ( not origin_outside ( vA1, vA2, vAB1, vAB2, vCA1, vCA2 ) )
					{	for ( size_t i = 0; i < frontal_nb_of_coords; i++ )
						{	const Function & x = Manifold::working .coordinates() [i];
							x ( tmp_B ) = x ( tmp_AB );  x ( tmp_C ) = x ( tmp_CA );   }
						vB1 = vAB1;  vB2 = vAB2;  vC1 = vCA1;  vC2 = vCA2;             }
					else if ( not origin_outside ( vB1, vB2, vAB1, vAB2, vBC1, vBC2 ) )
					{	for ( size_t i = 0; i < frontal_nb_of_coords; i++ )
						{	const Function & x = Manifold::working .coordinates() [i];
							x ( tmp_A ) = x ( tmp_AB );  x ( tmp_C ) = x ( tmp_BC );   }
						vA1 = vAB1;  vA2 = vAB2;  vC1 = vBC1;  vC2 = vBC2;             }
					else if ( not origin_outside ( vC1, vC2, vBC1, vBC2, vCA1, vCA2 ) )
					{	for ( size_t i = 0; i < frontal_nb_of_coords; i++ )
						{	const Function & x = Manifold::working .coordinates() [i];
							x ( tmp_A ) = x ( tmp_CA );  x ( tmp_B ) = x ( tmp_BC );  }
						vA1 = vCA1;  vA2 = vCA2;  vB1 = vBC1;  vB2 = vBC2;            }
					else if ( not origin_outside ( vAB1, vAB2, vBC1, vBC2, vCA1, vCA2 ) )
					{	for ( size_t i = 0; i < frontal_nb_of_coords; i++ )
						{	const Function & x = Manifold::working .coordinates() [i];
							x ( tmp_A ) = x ( tmp_BC );  x ( tmp_B ) = x ( tmp_CA );
							x ( tmp_C ) = x ( tmp_AB );                                }
						vA1 = vBC1;  vA2 = vBC2;  vB1 = vCA1;  vB2 = vCA2;
						vC1 = vAB1;  vC2 = vAB2;                                        }
					else  // nasty nonlinear level functions ...
						goto restart;                                                               }  }
		size_of_cube *= 2;                                                                          }

}  // end of  Manifold::Implicit::TwoEquations::search_start_ver
	

Cell Manifold::Implicit::ThreeEquationsOrMore::search_start_ver ( )  // virtual from  Manifold::Core
// we return a non-existent cell just to avoid compilation errors
{	assert ( false );  return Cell ( tag::non_existent );  }


Cell Manifold::Parametric::search_start_ver ( )  // virtual from  Manifold::Core
// we return a non-existent cell just to avoid compilation errors
// we should give arbitrary values to the parameteres, then project
// projection updates the values of other coordinates
{	assert ( false );  return Cell ( tag::non_existent );  }


#ifndef MANIFEM_NO_QUOTIENT

Cell Manifold::Quotient::search_start_ver ( )  // virtual from  Manifold::Core

// search for a starting point in a quotient manifold
// we probably want to mesh the entire manifold, so any point will do

{	Cell tmp_ver ( tag::vertex );
	for ( size_t i = 0; i < frontal_nb_of_coords; i++ )
	{	const Function & x = Manifold::working .coordinates() [i];
		x ( tmp_ver ) = 0.;                                        }
	return tmp_ver;                                                }

#endif  // ifndef MANIFEM_NO_QUOTIENT

//------------------------------------------------------------------------------------------------------//


void Manifold::Euclid::move_vertex
( const Cell & V, std::vector < double > & delta, double & norm_2 ) const

// norm_2 is the squared norm of delta (in the Riemannian metric), not used here
	
{	const Function & coords = this->get_coord_func();
	for ( size_t i = 0; i < frontal_nb_of_coords; i++ )
	{	const Function & x = coords [i];  x(V) += delta [i];  }  }


void Manifold::Implicit::move_vertex
( const Cell & V, std::vector < double > & delta, double & norm_2 ) const

// norm_2 is the squared norm of delta (in the Riemannian metric)

// moves V by delta, projects, updates delta and norm_2 to reflect the actual change in position

{	const Function & coords = this->get_coord_func();
	std::vector < double > old_pos = coords (V);
	for ( size_t i = 0; i < frontal_nb_of_coords; i++ )
	{	const Function & x = coords [i];  x(V) += delta [i];  }
	Cell::Positive::Vertex * Vc = tag::Util::assert_cast
		< Cell::Core*, Cell::Positive::Vertex* > ( V .core );
	this->project ( Vc) ;
	for ( size_t i = 0; i < frontal_nb_of_coords; i++ )
	{	const Function & x = coords [i];
		delta [i] = x(V) - old_pos [i];  }
	assert ( Manifold::working .core );
	assert ( Manifold::working .core->metric .core );
	norm_2 = this->metric .core->inner_prod ( V, delta, delta );    }


void Manifold::Parametric::move_vertex
( const Cell & V, std::vector < double > & delta, double & norm_2 ) const

// norm_2 is the squared norm of delta (in the Riemannian metric)

// moves V by delta, projects, updates delta and norm_2 to reflect the actual change in position

{	const Function & coords = this->get_coord_func();
	std::vector < double > old_pos = coords (V);
	for ( size_t i = 0; i < frontal_nb_of_coords; i++ )
	{	const Function & x = coords [i];  x(V) += delta [i];  }
	Cell::Positive::Vertex * Vc = tag::Util::assert_cast
		< Cell::Core*, Cell::Positive::Vertex* > ( V .core );
	this->project ( Vc) ;
	for ( size_t i = 0; i < frontal_nb_of_coords; i++ )
	{	const Function & x = coords [i];
		delta [i] = x(V) - old_pos [i];  }
	assert ( Manifold::working .core );
	assert ( Manifold::working .core->metric .core );
	norm_2 = this->metric .core->inner_prod ( V, delta, delta );     }


#ifndef MANIFEM_NO_QUOTIENT

void Manifold::Quotient::move_vertex
( const Cell & V, std::vector < double > & delta, double & norm_2 ) const

// norm_2 is the squared norm of delta (in the Riemannian metric), not used here

// only makes sense for a group of translations (as opposed to rotations or other linear transformations)
	
{	const Function & coords = this->get_coord_func();
	for ( size_t i = 0; i < frontal_nb_of_coords; i++ )
	{	const Function & x = coords [i];  x(V) += delta [i];  }  }

#endif  // ifndef MANIFEM_NO_QUOTIENT

//------------------------------------------------------------------------------------------------------//
//------------------------------------------------------------------------------------------------------//


void Manifold::Core::frontal_method_1d  // virtual, later overridden by Manifold::Quotient
( Mesh & msh, const tag::StartWithInconsistentMesh &,
  const tag::StartAt &, const Cell & start,
  const tag::Towards &, std::vector<double> tangent,
  const tag::StopAt &,  const Cell & stop            )

{	frontal_construct_1d < ManifoldNoWinding >       // line 2497
	( msh, tag::start_with_inconsistent_mesh,  
	  tag::start_at, start, tag::towards, tangent,
	  tag::stop_at, stop                          );  }


#ifndef MANIFEM_NO_QUOTIENT

void Manifold::Quotient::frontal_method_1d  // virtual from Manifold::Core, here overridden
( Mesh & msh, const tag::StartWithInconsistentMesh &,
  const tag::StartAt &, const Cell & start,
  const tag::Towards &, std::vector<double> tangent,
  const tag::StopAt &,  const Cell & stop            )

{	assert ( false );  }

#endif  // ifndef MANIFEM_NO_QUOTIENT

//------------------------------------------------------------------------------------------------------//


void Manifold::Core::frontal_method_1d  // virtual, later overridden by Manifold::Quotient
( Mesh & msh, const tag::StartWithInconsistentMesh &,
  const tag::StartAt &, const Cell & start,
  const tag::StopAt &,  const Cell & stop,
  const tag::Orientation &, const tag::OrientationChoice & oc )

{	frontal_construct_1d  // line 2199
	( msh, tag::start_with_inconsistent_mesh,
	  tag::start_at, start, tag::stop_at, stop, tag::orientation, oc );  }


#ifndef MANIFEM_NO_QUOTIENT

void Manifold::Quotient::frontal_method_1d  // virtual from Manifold::Core, here overridden
( Mesh & msh, const tag::StartWithInconsistentMesh &,
  const tag::StartAt &, const Cell & start,
  const tag::StopAt &,  const Cell & stop,
  const tag::Orientation &, const tag::OrientationChoice & oc )

{	assert ( false );  }

#endif  // ifndef MANIFEM_NO_QUOTIENT

//------------------------------------------------------------------------------------------------------//


void Manifold::Core::frontal_method_1d  // virtual, later overridden by Manifold::Quotient
( Mesh & msh, const tag::StartWithInconsistentMesh &,
  const tag::StartAt &, const Cell & start,
  const tag::Orientation &, const tag::OrientationChoice & oc )

{	frontal_construct_1d   // line 2161
	( msh, tag::start_with_inconsistent_mesh,
	  tag::start_at, start,tag::orientation, oc );  }


#ifndef MANIFEM_NO_QUOTIENT

void Manifold::Quotient::frontal_method_1d  // virtual from Manifold::Core, here overridden
( Mesh & msh, const tag::StartWithInconsistentMesh &,
  const tag::StartAt &, const Cell & start,
  const tag::Orientation &, const tag::OrientationChoice & oc )

{	assert ( false );  }

#endif  // ifndef MANIFEM_NO_QUOTIENT

//------------------------------------------------------------------------------------------------------//


void Manifold::Core::frontal_method_1d  // virtual, later overridden by Manifold::Quotient
( Mesh & msh, const tag::StartWithInconsistentMesh &,
  const tag::StartAt &, const Cell & start,
  const tag::StopAt &,  const Cell & stop,
  const tag::Winding &, const Manifold::Action & g   )

{	assert ( false );  }


#ifndef MANIFEM_NO_QUOTIENT

void Manifold::Quotient::frontal_method_1d  // virtual from Manifold::Core, here overridden
( Mesh & msh, const tag::StartWithInconsistentMesh &,
  const tag::StartAt &, const Cell & start,
  const tag::StopAt &,  const Cell & stop,
  const tag::Winding &, const Manifold::Action & g   )

{	const ManifoldQuotient::winding_cell stop_w { stop, g };
	const std::vector < double > tan = ManifoldQuotient::get_vector ( start, stop_w );
	assert ( tan .size() == 1 );
	
	frontal_construct_1d < ManifoldQuotient >  // line 2497
	( msh, tag::start_with_inconsistent_mesh, tag::start_at, start,
	       tag::towards, tan,                 tag::stop_at, stop_w );  }

#endif  // ifndef MANIFEM_NO_QUOTIENT

//------------------------------------------------------------------------------------------------------//


void Manifold::Core::frontal_method  // virtual, later overridden by Manifold::Quotient
( Mesh & msh, const tag::StartWithNonExistentMesh &,
  const tag::StartAt &, const Cell & start,
  const tag::Orientation &, const tag::OrientationChoice & oc )

{	if ( frontal_nb_of_coords == Manifold::working .topological_dim() )
	{	std::cout << "you are trying to mesh an entire Euclidian space ";
		std::cout << "(which is not compact)" << std::endl;
		exit ( 1 );                                                       }

	assert ( Manifold::working .core );
	assert ( Manifold::working .core->metric .core );
	Manifold::working .core->metric .core->frontal_method_nw_nem  // pointer to a static method
	( msh, tag::start_with_non_existent_mesh, tag::start_at, start, tag::orientation, oc );  }


void tag::Util::Metric::frontal_method_nw_inact_nem  // static
( Mesh & msh, const tag::StartWithNonExistentMesh &,
  const tag::StartAt &, const Cell & start,
  const tag::Orientation &, const tag::OrientationChoice & oc )

{	frontal_construct < ManifoldNoWinding, ISRContainer::Inactive >  // line 4021
	( msh, tag::start_with_non_existent_mesh, tag::start_at, start, tag::orientation, oc );  }
	// calls  frontal_construct_1d  line 2497  or  line 2161  or  frontal_construct_2d  line 1255


void tag::Util::Metric::frontal_method_nw_active_nem  // static
( Mesh & msh, const tag::StartWithNonExistentMesh &,
  const tag::StartAt &, const Cell & start,
  const tag::Orientation &, const tag::OrientationChoice & oc )

{	register_active_metric();
	frontal_construct < ManifoldNoWinding, ISRContainer::Active >  // line 4021
	( msh, tag::start_with_non_existent_mesh, tag::start_at, start, tag::orientation, oc );  }
	// calls  frontal_construct_1d  line 2497  or  line 2161  or  frontal_construct_2d  line 1255


#ifndef MANIFEM_NO_QUOTIENT

void Manifold::Quotient::frontal_method  // virtual from Manifold::Core, here overridden
( Mesh & msh, const tag::StartWithNonExistentMesh &,
  const tag::StartAt &, const Cell & start,
  const tag::Orientation &, const tag::OrientationChoice & oc )

{	assert ( Manifold::working .core );
	assert ( Manifold::working .core->metric .core );
	Manifold::working .core->metric .core->frontal_method_q_nem  // pointer to a static method
	( msh, tag::start_with_non_existent_mesh, tag::start_at, start, tag::orientation, oc );  }
	

void tag::Util::Metric::frontal_method_q_inact_nem  // static
( Mesh & msh, const tag::StartWithNonExistentMesh &,
  const tag::StartAt &, const Cell & start,
  const tag::Orientation &, const tag::OrientationChoice & oc )

{	frontal_construct < ManifoldQuotient, ISRContainer::Inactive >  // line 4021
	( msh, tag::start_with_non_existent_mesh, tag::start_at, start, tag::orientation, oc );  }
	// calls  frontal_construct_1d  line 2497  or  line 2161  or  frontal_construct_2d  line 1255

void tag::Util::Metric::frontal_method_q_active_nem  // static
( Mesh & msh, const tag::StartWithNonExistentMesh &,
  const tag::StartAt &, const Cell & start,
  const tag::Orientation &, const tag::OrientationChoice & oc )

{	register_active_metric();
	frontal_construct < ManifoldNoWinding, ISRContainer::Active >  // line 4021
	( msh, tag::start_with_non_existent_mesh, tag::start_at, start, tag::orientation, oc );  }
	// calls  frontal_construct_1d  line 2497  or  line 2161  or  frontal_construct_2d  line 1255

#endif  // ifndef MANIFEM_NO_QUOTIENT

//------------------------------------------------------------------------------------------------------//


void Manifold::Core::frontal_method  // virtual, later overridden by Manifold::Quotient
( Mesh & msh, const tag::Boundary &, const Mesh & bdry,
  const tag::StartAt &, const Cell & start,
  const tag::Towards &, const std::vector < double > & normal )

{	assert ( Manifold::working .core );
	assert ( Manifold::working .core->metric .core );
	Manifold::working .core->metric .core->frontal_method_nw_tow  // pointer to a static method
	( msh, tag::boundary, bdry, tag::start_at, start, tag::towards, normal );  }


void tag::Util::Metric::frontal_method_nw_inact_tow  // static
( Mesh & msh, const tag::Boundary &, const Mesh & bdry,
  const tag::StartAt &, const Cell & start,
  const tag::Towards &, const std::vector < double > & normal )

{	if ( Manifold::working .topological_dim() == frontal_nb_of_coords )
		frontal_construct_2d < Environment            // line 1437  then  line 1255
			< ManifoldNoWinding, ISRContainer::Inactive,
			  ExtProd2d < ManifoldNoWinding, ISRContainer::Inactive > > >
			( msh, tag::boundary, bdry, tag::start_at, start, tag::towards, normal );
	else
		frontal_construct_2d < Environment      // line 1437  then  line 1255
			< ManifoldNoWinding, ISRContainer::Inactive,
			  ExtProdCodimOne < ManifoldNoWinding, ISRContainer::Inactive > > > 
			( msh, tag::boundary, bdry, tag::start_at, start, tag::towards, normal );  }


void tag::Util::Metric::frontal_method_nw_active_tow  // static
( Mesh & msh, const tag::Boundary &, const Mesh & bdry,
  const tag::StartAt &, const Cell & start,
  const tag::Towards &, const std::vector < double > & normal )

{	register_active_metric();
	if ( Manifold::working .topological_dim() == frontal_nb_of_coords )
		frontal_construct_2d < Environment            // line 1437  then  line 1255
			< ManifoldNoWinding, ISRContainer::Active,
			  ExtProd2d < ManifoldNoWinding, ISRContainer::Active > > >
			( msh, tag::boundary, bdry, tag::start_at, start, tag::towards, normal );
	else
		frontal_construct_2d < Environment      // line 1437  then  line 1255
			< ManifoldNoWinding, ISRContainer::Active,
			  ExtProdCodimOne < ManifoldNoWinding, ISRContainer::Active > > > 
			( msh, tag::boundary, bdry, tag::start_at, start, tag::towards, normal );  }


#ifndef MANIFEM_NO_QUOTIENT

void Manifold::Quotient::frontal_method  // virtual from Manifold::Core, here overridden
( Mesh & msh, const tag::Boundary &, const Mesh & bdry,
  const tag::StartAt &, const Cell & start,
  const tag::Towards &, const std::vector < double > & normal )

{	assert ( Manifold::working .core );
	assert ( Manifold::working .core->metric .core );
	Manifold::working .core->metric .core->frontal_method_q_tow  // pointer to a static method
	( msh, tag::boundary, bdry, tag::start_at, start, tag::towards, normal );  }
	

void tag::Util::Metric::frontal_method_q_inact_tow  // static
( Mesh & msh, const tag::Boundary &, const Mesh & bdry,
  const tag::StartAt &, const Cell & start,
  const tag::Towards &, const std::vector < double > & normal )

{	if ( Manifold::working .topological_dim() == frontal_nb_of_coords )
		frontal_construct_2d < Environment            // line 1437  then  line 1255
			< ManifoldQuotient, ISRContainer::Inactive,
			  ExtProd2d < ManifoldQuotient, ISRContainer::Inactive > > >
			( msh, tag::boundary, bdry, tag::start_at, start, tag::towards, normal );
	else
		frontal_construct_2d < Environment      // line 1437  then  line 1255
			< ManifoldQuotient, ISRContainer::Inactive,
			  ExtProdCodimOne < ManifoldQuotient, ISRContainer::Inactive > > > 
			( msh, tag::boundary, bdry, tag::start_at, start, tag::towards, normal );  }


void tag::Util::Metric::frontal_method_q_active_tow  // static
( Mesh & msh, const tag::Boundary &, const Mesh & bdry,
  const tag::StartAt &, const Cell & start,
  const tag::Towards &, const std::vector < double > & normal )

{	register_active_metric();
	if ( Manifold::working .topological_dim() == frontal_nb_of_coords )
		frontal_construct_2d < Environment            // line 1437  then  line 1255
			< ManifoldQuotient, ISRContainer::Active,
			  ExtProd2d < ManifoldQuotient, ISRContainer::Active > > >
			( msh, tag::boundary, bdry, tag::start_at, start, tag::towards, normal );
	else
		frontal_construct_2d < Environment      // line 1437  then  line 1255
			< ManifoldQuotient, ISRContainer::Active,
			  ExtProdCodimOne < ManifoldQuotient, ISRContainer::Active > > > 
			( msh, tag::boundary, bdry, tag::start_at, start, tag::towards, normal );  }

#endif  // ifndef MANIFEM_NO_QUOTIENT

//------------------------------------------------------------------------------------------------------//


void Manifold::Core::frontal_method  // virtual, later overridden by Manifold::Quotient
( Mesh & msh, const tag::Boundary &, const Mesh & interface,
  const tag::StartAt &, const Cell & start,
  const tag::Orientation &, const tag::OrientationChoice & oc )

{	if ( frontal_nb_of_coords == 2 )
	{	// domain in the plane RR^2, intrinsic orientation
		if ( oc == tag::random )
		{	std::cout << "it is unsafe to try to mesh a plane region with random orientation;";
			std::cout << std::endl << "the region may be unbounded" << std::endl;
			exit (1);                                                                           }
		if ( oc == tag::inherent )
		{	std::cout << "inherent orientation does not apply to a plane region; ";
			std::cout << "did you mean 'intrinsic' ?" << std::endl;
			exit (1);                                                               }
		assert ( ( oc == tag::intrinsic ) or ( oc == tag::not_provided ) );                      }
		// here we interpret "not provided" as "intrinsic"

	if ( ( frontal_nb_of_coords == 3 ) and ( interface .dim() == 1 ) )
		// domain in a surface embedded in RR^3 (triangles), random or inherent orientation
	{	if ( oc == tag::not_provided )
		{	std::cout << "please specify on which side of the interface ";
			std::cout << "the mesh should start growing" << std::endl;
			exit (1);                                                      }
		if ( oc == tag::intrinsic )
		{	std::cout << "intrinsic orientation only makes sense when building" << std::endl;
			std::cout << "a mesh of the same dimension as the working manifold" << std::endl;
			exit (1);                                                                          }  }
		
	assert ( Manifold::working .core );
	assert ( Manifold::working .core->metric .core );
	Manifold::working .core->metric .core->frontal_method_nw_oc  // pointer to a static method
		( msh, tag::boundary, interface, tag::start_at, start, tag::orientation, oc );              }


void tag::Util::Metric::frontal_method_nw_inact_oc  // static
( Mesh & msh, const tag::Boundary &, const Mesh & interface,
  const tag::StartAt &, const Cell & start,
  const tag::Orientation &, const tag::OrientationChoice & oc )
	
{	frontal_construct < ManifoldNoWinding, ISRContainer::Inactive >  // line 4188
		( msh, tag::boundary, interface, tag::start_at, start, tag::orientation, oc );  }
	// calls  frontal_construct_2d  line 1255  or  _3d  line 4541


void tag::Util::Metric::frontal_method_nw_active_oc  // static
( Mesh & msh, const tag::Boundary &, const Mesh & interface,
  const tag::StartAt &, const Cell & start,
  const tag::Orientation &, const tag::OrientationChoice & oc )
	
{	register_active_metric();
	frontal_construct < ManifoldNoWinding, ISRContainer::Active >  // line 4188
		( msh, tag::boundary, interface, tag::start_at, start, tag::orientation, oc );  }
	// calls  frontal_construct_2d  line 1255  or  _3d  line 4541


#ifndef MANIFEM_NO_QUOTIENT

void Manifold::Quotient::frontal_method  // virtual from Manifold::Core, here overridden
( Mesh & msh, const tag::Boundary &, const Mesh & interface,
  const tag::StartAt &, const Cell & start,
  const tag::Orientation &, const tag::OrientationChoice & oc )

{	if ( frontal_nb_of_coords == 2 )
	{	// domain in the plane RR^2, intrinsic orientation
		if ( oc == tag::inherent )
		{	std::cout << "inherent orientation does not apply to a plane region;" << std::endl;
			std::cout << "did you mean 'intrinsic' ?" << std::endl;
			exit (1);                                                                           }
		assert ( ( oc == tag::intrinsic ) or ( oc == tag::random ) or ( oc == tag::not_provided ) ); }
		// here we interpret "not provided" as "intrinsic"

	assert ( Manifold::working .core );
	assert ( Manifold::working .core->metric .core );
	Manifold::working .core->metric .core->frontal_method_q_oc  // pointer to a static method
		( msh, tag::boundary, interface, tag::start_at, start, tag::orientation, oc );                  }


void tag::Util::Metric::frontal_method_q_inact_oc  // static
( Mesh & msh, const tag::Boundary &, const Mesh & interface,
  const tag::StartAt &, const Cell & start,
  const tag::Orientation &, const tag::OrientationChoice & oc )
	
{	frontal_construct < ManifoldQuotient, ISRContainer::Inactive >  // line 4188
		( msh, tag::boundary, interface, tag::start_at, start, tag::orientation, oc );  }
	// calls  frontal_construct_2d  line 1255  or  _3d  line 4541


void tag::Util::Metric::frontal_method_q_active_oc  // static
( Mesh & msh, const tag::Boundary &, const Mesh & interface,
  const tag::StartAt &, const Cell & start,
  const tag::Orientation &, const tag::OrientationChoice & oc )
	
{	register_active_metric();
	assert ( false );  // should be quotient, right ?
	frontal_construct < ManifoldNoWinding, ISRContainer::Active >  // line 4188
		( msh, tag::boundary, interface, tag::start_at, start, tag::orientation, oc );  }
	// calls  frontal_construct_2d  line 1255  or  _3d  line 4541

#endif  // ifndef MANIFEM_NO_QUOTIENT

//------------------------------------------------------------------------------------------------------//
//------------------------------------------------------------------------------------------------------//


void Mesh::Build::Frontal::start_at ( const Cell & cll )
// virtual from Mesh::Build::Core, here overridden
{	assert ( not this->start .exists() );
	this->ready_for_towards = true;
	this->ready_for_stop_at = true;
	this->start = cll;                   }

Mesh::Build Mesh::Build::Core::set_boundary ( const Mesh & msh )  // virtual
{	std::cout << "cannot set boundary for this Mesh::Build" << std::endl;
	exit ( 1 );                                                            }

Mesh::Build Mesh::Build::Core::set_constant_length ( double l )  // virtual
{	std::cout << "cannot set segment length for this Mesh::Build" << std::endl;
	exit ( 1 );                                                                  }

Mesh::Build Mesh::Build::Core::set_variable_length ( const Function & l )  // virtual
{	std::cout << "cannot set segment length for this Mesh::Build" << std::endl;
	exit ( 1 );                                                                  }

void Mesh::Build::Core::set_towards ( const std::vector < double > & dir )  // virtual
{	std::cout << "cannot set initial direction for this Mesh::Build" << std::endl;
	exit ( 1 );                                                                     }

void Mesh::Build::Frontal::set_towards ( const std::vector < double > & dir )
// virtual from Mesh::Build::Core, here overridden
{	if ( not this->ready_for_towards )
	{	std::cout << "towards can only be invoked after start_at" << std::endl;
		exit ( 1 );                                                              }
	this->ready_for_towards = false;
	this->ready_for_stop_at = true;
	assert ( this->towards .size() == 0 );
	// {	std::cout << "you cannot specify initial direction twice" << std::endl;
	// 	exit ( 1 );                                                              }
	this->towards = dir;                                                           }

void Mesh::Build::Core::set_orientation ( const tag::OrientationChoice & oc )  // virtual
{	std::cout << "cannot set orientation for this Mesh::Build" << std::endl;
	std::cout << "(shortest_path counts as orientation)" << std::endl;
	exit ( 1 );                                                               }

void Mesh::Build::Frontal::set_orientation ( const tag::OrientationChoice & oc )
// virtual from Mesh::Build::Core, here overridden
{	if ( this->orientation != tag::not_provided )
	{	std::cout << "you cannot specify orientation twice" << std::endl;
		std::cout << "(shortest_path and towards count as orientation)" << std::endl;
		exit ( 1 );                                                         }
	this->ready_for_towards = false;
	this->ready_for_stop_at = false;
	this->orientation = oc;                                                   }

void Mesh::Build::Core::set_entire_manifold ( )  // virtual
{	std::cout << "cannot mesh entire manifold with this Mesh::Build" << std::endl;
	exit ( 1 );                                                                     }

void Mesh::Build::Frontal::set_entire_manifold ( )  // virtual from Mesh::Build::Core, here overridden
{	if ( this->entire_manifold )
	{	std::cout << "you cannot call entire_manifold twice" << std::endl;
		exit ( 1 );                                                         }
	this->ready_for_towards = false;
	this->ready_for_stop_at = false;
	this->entire_manifold = true;                                             }

//------------------------------------------------------------------------------------------------------//


class Mesh::Build::Frontal::FromBoundary : public Mesh::Build::Frontal

{	public :

	// orientation       inherited from Mesh::Build::Frontal
	// entire_manifold   inherited from Mesh::Build::Frontal
	// Cell start        inherited from Mesh::Build::Frontal
	// vector towards    inherited from Mesh::Build::Frontal

	Mesh boundary { tag::non_existent };

	inline FromBoundary ( )
	:	Mesh::Build::Frontal ()
	{	}  // Mesh::Build::Frontal::set_boundary ensures not this->entire_manifold

	// define_faces, define_vertices, set_winding, set_winding_true, set_singular, 
	// shape_segment, shape_triangle, shape_quadrangle, shape_cube, set_with_triangles,
	// set_format, set_file_name, set_import_numbering,
	// set_swatch, set_track, seg_follow_curvature, set_follow_torsion,
	// build_mesh, build_composite_mesh, add_mesh, add_cell, add_function
	//   virtual from Mesh::Build::Core, defined there, execution forbidden

	// start_at, towards, set_orientation, set_entire_manifold
	//   virtual from Mesh::Build::Core, defined in Mesh::Build::Frontal
	
	using Mesh::Build::Core::stop_at;  // virtual, execution forbidden
	// because a boundary is already set, a stopping point makes no sense
	using Mesh::Build::Core::set_boundary;  // virtual, execution forbidden
	// because calling 'Mesh::Build::boundary' twice is not acceptable -- why not ?

	Mesh::Build set_constant_length ( double l ) override;            // virtual from Mesh::Build::Core
	Mesh::Build set_variable_length ( const Function & l ) override;  // virtual from Mesh::Build::Core

};  // end of class Mesh::Frontal::Build::FromBoundary


Mesh::Build Mesh::Build::Frontal::set_boundary ( const Mesh & msh )
// virtual from Mesh::Build::Core, here overridden
{	if ( this->entire_manifold )
	{	std::cout << "if you want to mesh the entire manifold," << std::endl;
		std::cout << "you cannot declare a boundary" << std::endl;
		exit ( 1 );                                                            }
	std::shared_ptr < Mesh::Build::Frontal::FromBoundary > res_core =
		std::make_shared < Mesh::Build::Frontal::FromBoundary > ();
	res_core->orientation = this->orientation;
	res_core->entire_manifold = this->entire_manifold;
	res_core->start = this->start;
	res_core->towards = this->towards;
	res_core->boundary = msh;
	return  Mesh::Build ( tag::whose_core_is, res_core );                        }
	
//------------------------------------------------------------------------------------------------------//


class Mesh::Build::Frontal::StartStopPoints : public Mesh::Build::Frontal

{	public :

	// orientation       inherited from Mesh::Build::Frontal
	// entire_manifold   inherited from Mesh::Build::Frontal
	// Cell start        inherited from Mesh::Build::Frontal
	// vector towards    inherited from Mesh::Build::Frontal

	Cell stop { tag::non_existent };
	
	inline StartStopPoints ( )
	:	Mesh::Build::Frontal ()
	{	}

	// define_faces, define_vertices, set_winding, set_winding_true, set_singular, 
	// shape_segment, shape_triangle, shape_quadrangle, shape_cube, set_with_triangles,
	// set_format, set_file_name, set_import_numbering,
	// set_swatch, set_track, seg_follow_curvature, set_follow_torsion,
	// build_mesh, build_composite_mesh, add_mesh, add_cell, add_function
	//   virtual from Mesh::Build::Core, defined there, execution forbidden

	// start_at, towards, set_orientation, set_entire_manifold
	//   virtual from Mesh::Build::Core, defined in Mesh::Build::Frontal

	using Mesh::Build::Core::set_boundary;  // virtual, execution forbidden
	// because calling 'Mesh::Build::boundary' after 'stop_at' is not acceptable

	Mesh::Build set_constant_length ( double l ) override;            // virtual from Mesh::Build::Core
	Mesh::Build set_variable_length ( const Function & l ) override;  // virtual from Mesh::Build::Core

};  // end of class Mesh::Build::Frontal::StartStopPoints


Mesh::Build Mesh::Build::Frontal::stop_at ( const Cell & P )
// virtual from Mesh::Build::Core, here overridden
{	if ( not this->ready_for_stop_at )
	{	std::cout << "stop_at can only be invoked after start_at ";
		std::cout << "or after towards" << std::endl;
		exit ( 1 );                                                  }
	assert ( not this->entire_manifold );
	// {	std::cout << "if you want to mesh the entire manifold," << std::endl;
	// 	std::cout << "you cannot declare a stopping point" << std::endl;
	// 	exit ( 1 );                                                            }
	std::shared_ptr < Mesh::Build::Frontal::StartStopPoints > res_core =
		std::make_shared < Mesh::Build::Frontal::StartStopPoints > ();
	res_core->orientation = this->orientation;
	res_core->entire_manifold = this->entire_manifold;
	res_core->start = this->start;
	res_core->towards = this->towards;
	res_core->stop = P;
	return  Mesh::Build ( tag::whose_core_is, res_core );                        }

//------------------------------------------------------------------------------------------------------//


class Mesh::Build::Frontal::ConstantSegLength : public Mesh::Build::Frontal

{	public :

	// orientation       inherited from Mesh::Build::Frontal
	// entire_manifold   inherited from Mesh::Build::Frontal
	// Cell start        inherited from Mesh::Build::Frontal
	// vector towards    inherited from Mesh::Build::Frontal

	double seg_len { 0. };
	
	inline ConstantSegLength ( )
	:	Mesh::Build::Frontal()
	{	}

	// define_faces, define_vertices, set_winding, set_winding_true, set_singular, 
	// shape_segment, shape_triangle, shape_quadrangle, shape_cube, set_with_triangles,
	// set_format, set_file_name, set_import_numbering,
	// set_swatch, set_track, seg_follow_curvature, set_follow_torsion,
	// build_composite_mesh, add_mesh, add_cell, add_function
	//   virtual from Mesh::Build::Core, defined there, execution forbidden

	// start_at, towards, set_orientation, set_entire_manifold
	//   virtual from Mesh::Build::Core, defined in Mesh::Build::Frontal

	Mesh::Build set_boundary ( const Mesh & msh ) override;  // virtual from Mesh::Build::Core

	Mesh::Build stop_at ( const Cell & P ) override;  // virtual from Mesh::Build::Core
	
	Mesh::Build set_constant_length ( double l ) override;  // virtual from Mesh::Build::Core
	Mesh::Build set_variable_length ( const Function & l ) override;  // virtual from Mesh::Build::Core

	Mesh build_mesh ( ) override;            // virtual from Mesh::Build::Core
	
	class FromBoundary;  class StartStopPoints;
	
};  // end of class Mesh::Build::Frontal::ConstantSegLength


Mesh::Build Mesh::Build::Frontal::ConstantSegLength::set_constant_length ( double l )
{	std::cout << "you cannot specify twice the segment length" << std::endl;
	exit ( 1 );                                                               }

Mesh::Build Mesh::Build::Frontal::ConstantSegLength::set_variable_length ( const Function & l )
{	std::cout << "you cannot specify twice the segment length" << std::endl;
	exit ( 1 );                                                               }

Mesh::Build Mesh::Build::Frontal::set_constant_length ( double l )
// virtual from Mesh::Build::Core, here overridden
{	std::shared_ptr < Mesh::Build::Frontal::ConstantSegLength > res_core =
		std::make_shared < Mesh::Build::Frontal::ConstantSegLength > ();
	res_core->orientation = this->orientation;
	res_core->entire_manifold = this->entire_manifold;
	res_core->start = this->start;
	res_core->towards = this->towards;
	res_core->seg_len = l;
	return  Mesh::Build ( tag::whose_core_is, res_core );                   }
	
//------------------------------------------------------------------------------------------------------//


class Mesh::Build::Frontal::ConstantSegLength::FromBoundary :
public Mesh::Build::Frontal::ConstantSegLength

{	public :

	// orientation       inherited from Mesh::Build::Frontal
	// entire_manifold   inherited from Mesh::Build::Frontal
	// Cell start        inherited from Mesh::Build::Frontal
	// vector towards    inherited from Mesh::Build::Frontal
	// double seg_len    inherited from Mesh::Build::Frontal::ConstantSegLength

	Mesh boundary { tag::non_existent };

	inline FromBoundary ( )
	:	Mesh::Build::Frontal::ConstantSegLength ()
	{	}

	// define_faces, define_vertices, set_winding, set_winding_true, set_singular, 
	// shape_segment, shape_triangle, shape_quadrangle, shape_cube, set_with_triangles,
	// set_format, set_file_name, set_import_numbering,
	// set_swatch, set_track, seg_follow_curvature, set_follow_torsion,
	// build_composite_mesh, add_mesh, add_cell, add_function
	//   virtual from Mesh::Build::Core, defined there, execution forbidden

	// start_at, towards, set_orientation, set_entire_manifold
	//   virtual from Mesh::Build::Core, defined in Mesh::Build::Frontal

	using Mesh::Build::Core::set_boundary;  // virtual, execution forbidden
	// because calling 'Mesh::Build::boundary' twice is not acceptable - why not ?

	using Mesh::Build::Frontal::ConstantSegLength::set_constant_length;  // virtual from Mesh::Build::Core
	using Mesh::Build::Frontal::ConstantSegLength::set_variable_length;  // virtual from Mesh::Build::Core

	Mesh build_mesh ( ) override;            // virtual from Mesh::Build::Core
	
};  // end of class Mesh::Build::Frontal::ConstantSegLength::FromBoundary


Mesh::Build Mesh::Build::Frontal::FromBoundary::set_constant_length ( double l )
// virtual from Mesh::Build::Core, here overridden
{	std::shared_ptr  < Mesh::Build::Frontal::ConstantSegLength::FromBoundary > res_core =
		std::make_shared < Mesh::Build::Frontal::ConstantSegLength::FromBoundary > ();
	res_core->orientation = this->orientation;
	assert ( not this->entire_manifold );
	res_core->entire_manifold = false;
	res_core->start = this->start;
	res_core->towards = this->towards;
	res_core->boundary = this->boundary;
	res_core->seg_len = l;
	return  Mesh::Build ( tag::whose_core_is, res_core );                                  }

Mesh::Build Mesh::Build::Frontal::ConstantSegLength::set_boundary ( const Mesh & msh )
// virtual from Mesh::Build::Core, here overridden
{	if ( this->entire_manifold )
	{	std::cout << "if you want to mesh the entire manifold," << std::endl;
		std::cout << "you cannot declare a boundary" << std::endl;
		exit ( 1 );                                                            }
	std::shared_ptr  < Mesh::Build::Frontal::ConstantSegLength::FromBoundary > res_core =
		std::make_shared < Mesh::Build::Frontal::ConstantSegLength::FromBoundary > ();
	res_core->orientation = this->orientation;
	res_core->entire_manifold = false;
	res_core->start = this->start;
	res_core->towards = this->towards;
	res_core->seg_len = this->seg_len;
	res_core->boundary = msh;
	return  Mesh::Build ( tag::whose_core_is, res_core );                                  }
	
//------------------------------------------------------------------------------------------------------//


class Mesh::Build::Frontal::ConstantSegLength::StartStopPoints :
public Mesh::Build::Frontal::ConstantSegLength

{	public :

	// orientation       inherited from Mesh::Build::Frontal
	// entire_manifold   inherited from Mesh::Build::Frontal
	// Cell start        inherited from Mesh::Build::Frontal
	// vector towards    inherited from Mesh::Build::Frontal
	// double seg_len    inherited from Mesh::Build::Frontal::ConstantSegLength

	Cell stop { tag::non_existent };
	
	inline StartStopPoints ( )
	:	Mesh::Build::Frontal::ConstantSegLength ()
	{	}

	// define_faces, define_vertices, set_winding, set_winding_true, set_singular, 
	// shape_segment, shape_triangle, shape_quadrangle, shape_cube, set_with_triangles,
	// set_format, set_file_name, set_import_numbering,
	// set_swatch, set_track, seg_follow_curvature, set_follow_torsion,
	// build_composite_mesh, add_mesh, add_cell, add_function
	//   virtual from Mesh::Build::Core, defined there, execution forbidden

	// set_constant_length, set_variable_length
	//   virtual from Mesh::Build::Core, defined in Mesh::Build::Frontal::ConstantLength

	using Mesh::Build::Frontal::start_at;             // virtual from Mesh::Build::Core
	using Mesh::Build::Frontal::towards;              // virtual from Mesh::Build::Core
	using Mesh::Build::Frontal::set_orientation;      // virtual from Mesh::Build::Core
	using Mesh::Build::Frontal::set_entire_manifold;  // virtual from Mesh::Build::Core

	Mesh build_mesh ( ) override;            // virtual from Mesh::Build::Core
	
};  // end of class Mesh::Build::Frontal::ConstantSegLength::StartStopPoints


Mesh::Build Mesh::Build::Frontal::StartStopPoints::set_constant_length ( double l )
// virtual from Mesh::Build::Core, here overridden

{	std::shared_ptr < Mesh::Build::Frontal::ConstantSegLength::StartStopPoints > res_core =
		std::make_shared < Mesh::Build::Frontal::ConstantSegLength::StartStopPoints > ();
	res_core->orientation = this->orientation;
	assert ( not this->entire_manifold );
	res_core->entire_manifold = false;
	res_core->start = this->start;
	res_core->towards = this->towards;
	res_core->stop = this->stop;
	res_core->seg_len = l;
	return  Mesh::Build ( tag::whose_core_is, res_core );                                    }


Mesh::Build Mesh::Build::Frontal::ConstantSegLength::stop_at ( const Cell & P )
// virtual from Mesh::Build::Core, here overridden

{	if ( not this->ready_for_stop_at )
	{	std::cout << "stop_at can only be invoked after start_at ";
		std::cout << "or after towards" << std::endl;
		exit ( 1 );                                                  }
	assert ( not this->entire_manifold );
	// {	std::cout << "if you want to mesh the entire manifold," << std::endl;
	// 	std::cout << "you cannot declare a stopping point" << std::endl;
	// 	exit ( 1 );                                                            }
	std::shared_ptr < Mesh::Build::Frontal::ConstantSegLength::StartStopPoints > res_core =
		std::make_shared < Mesh::Build::Frontal::ConstantSegLength::StartStopPoints > ();
	res_core->orientation = this->orientation;
	res_core->entire_manifold = this->entire_manifold;
	res_core->start = this->start;
	res_core->towards = this->towards;
	res_core->seg_len = this->seg_len;
	res_core->stop = P;
	return  Mesh::Build ( tag::whose_core_is, res_core );                                    }

//------------------------------------------------------------------------------------------------------//


class Mesh::Build::Frontal::VariableSegLength : public Mesh::Build::Frontal

{	public :

	// orientation       inherited from Mesh::Build::Frontal
	// entire_manifold   inherited from Mesh::Build::Frontal
	// Cell start        inherited from Mesh::Build::Frontal
	// vector towards    inherited from Mesh::Build::Frontal

	Function seg_len { tag::non_existent };
	
	inline VariableSegLength ( )
	:	Mesh::Build::Frontal()
	{	}

	// define_faces, define_vertices, set_winding, set_winding_true, set_singular, 
	// shape_segment, shape_triangle, shape_quadrangle, shape_cube, set_with_triangles,
	// set_format, set_file_name, set_import_numbering,
	// set_swatch, set_track, seg_follow_curvature, set_follow_torsion,
	// build_composite_mesh, add_mesh, add_cell, add_function
	//   virtual from Mesh::Build::Core, defined there, execution forbidden

	// start_at, towards, set_orientation, set_entire_manifold
	//   virtual from Mesh::Build::Core, defined in Mesh::Build::Frontal

	Mesh::Build set_boundary ( const Mesh & msh ) override;  // virtual from Mesh::Build::Core

	Mesh::Build stop_at ( const Cell & P ) override;  // virtual from Mesh::Build::Core
	
	Mesh::Build set_constant_length ( double l ) override;  // virtual from Mesh::Build::Core
	Mesh::Build set_variable_length ( const Function & l ) override;  // virtual from Mesh::Build::Core

	Mesh build_mesh ( ) override;            // virtual from Mesh::Build::Core
	
	class FromBoundary;  class StartStopPoints;
	
};  // end of class Mesh::Build::Frontal::VariableSegLength


Mesh::Build Mesh::Build::Frontal::VariableSegLength::set_constant_length ( double l )
{	std::cout << "you cannot specify twice the segment length" << std::endl;
	exit ( 1 );                                                               }

Mesh::Build Mesh::Build::Frontal::VariableSegLength::set_variable_length ( const Function & l )
{	std::cout << "you cannot specify twice the segment length" << std::endl;
	exit ( 1 );                                                               }

Mesh::Build Mesh::Build::Frontal::set_variable_length ( const Function & l )
// virtual from Mesh::Build::Core, here overridden
{	std::shared_ptr < Mesh::Build::Frontal::VariableSegLength > res_core =
		std::make_shared < Mesh::Build::Frontal::VariableSegLength > ();
	res_core->orientation = this->orientation;
	res_core->entire_manifold = this->entire_manifold;
	res_core->start = this->start;
	res_core->towards = this->towards;
	res_core->seg_len = l;
	return  Mesh::Build ( tag::whose_core_is, res_core );                   }
	
//------------------------------------------------------------------------------------------------------//


class Mesh::Build::Frontal::VariableSegLength::FromBoundary :
public Mesh::Build::Frontal::VariableSegLength

{	public :

	// orientation       inherited from Mesh::Build::Frontal
	// entire_manifold   inherited from Mesh::Build::Frontal
	// Cell start        inherited from Mesh::Build::Frontal
	// vector towards    inherited from Mesh::Build::Frontal
	// Function seg_len  inherited from Mesh::Build::Frontal::VariableSegLength

	Mesh boundary { tag::non_existent };

	inline FromBoundary ( )
	:	Mesh::Build::Frontal::VariableSegLength ()
	{	}

	// define_faces, define_vertices, set_winding, set_winding_true, set_singular, 
	// shape_segment, shape_triangle, shape_quadrangle, shape_cube, set_with_triangles,
	// set_format, set_file_name, set_import_numbering,
	// set_swatch, set_track, seg_follow_curvature, set_follow_torsion,
	// build_composite_mesh, add_mesh, add_cell, add_function
	//   virtual from Mesh::Build::Core, defined there, execution forbidden

	// set_constant_length, set_variable_length
	//   virtual from Mesh::Build::Core, defined in Mesh::Build::Frontal::VariableSegLength

	using Mesh::Build::Core::set_boundary;  // virtual, execution forbidden
	// because calling 'Mesh::Build::boundary' twice is not acceptable - why not ?

	using Mesh::Build::Frontal::start_at;             // virtual from Mesh::Build::Core
	using Mesh::Build::Frontal::towards;              // virtual from Mesh::Build::Core
	using Mesh::Build::Frontal::set_orientation;      // virtual from Mesh::Build::Core
	using Mesh::Build::Frontal::set_entire_manifold;  // virtual from Mesh::Build::Core

	Mesh build_mesh ( ) override;            // virtual from Mesh::Build::Core
	
};  // end of class Mesh::Build::Frontal::VariableSegLength::FromBoundary


Mesh::Build Mesh::Build::Frontal::FromBoundary::set_variable_length ( const Function & l )
// virtual from Mesh::Build::Core, here overridden
{	std::shared_ptr  < Mesh::Build::Frontal::VariableSegLength::FromBoundary > res_core =
		std::make_shared < Mesh::Build::Frontal::VariableSegLength::FromBoundary > ();
	res_core->orientation = this->orientation;
	assert ( not this->entire_manifold );
	res_core->entire_manifold = false;
	res_core->start = this->start;
	res_core->towards = this->towards;
	res_core->boundary = this->boundary;
	res_core->seg_len = l;
	return  Mesh::Build ( tag::whose_core_is, res_core );                                  }

Mesh::Build Mesh::Build::Frontal::VariableSegLength::set_boundary ( const Mesh & msh )
// virtual from Mesh::Build::Core, here overridden
{	if ( this->entire_manifold )
	{	std::cout << "if you want to mesh the entire manifold," << std::endl;
		std::cout << "you cannot declare a boundary" << std::endl;
		exit ( 1 );                                                            }
	std::shared_ptr  < Mesh::Build::Frontal::VariableSegLength::FromBoundary > res_core =
		std::make_shared < Mesh::Build::Frontal::VariableSegLength::FromBoundary > ();
	res_core->orientation = this->orientation;
	res_core->entire_manifold = false;
	res_core->start = this->start;
	res_core->towards = this->towards;
	res_core->seg_len = this->seg_len;
	res_core->boundary = msh;
	return  Mesh::Build ( tag::whose_core_is, res_core );                                  }
	
//------------------------------------------------------------------------------------------------------//


class Mesh::Build::Frontal::VariableSegLength::StartStopPoints :
public Mesh::Build::Frontal::VariableSegLength

{	public :

	// orientation       inherited from Mesh::Build::Frontal
	// entire_manifold   inherited from Mesh::Build::Frontal
	// Cell start        inherited from Mesh::Build::Frontal
	// vector towards    inherited from Mesh::Build::Frontal
	// Function seg_len  inherited from Mesh::Build::Frontal::VariableSegLength

	Cell stop { tag::non_existent };
	
	inline StartStopPoints ( )
	:	Mesh::Build::Frontal::VariableSegLength ()
	{	}

	// define_faces, define_vertices, set_winding, set_winding_true, set_singular, 
	// shape_segment, shape_triangle, shape_quadrangle, shape_cube, set_with_triangles,
	// set_format, set_file_name, set_import_numbering,
	// set_swatch, set_track, seg_follow_curvature, set_follow_torsion,
	// build_composite_mesh, add_mesh, add_cell, add_function
	//   virtual from Mesh::Build::Core, defined there, execution forbidden

	// set_constant_length, set_variable_length
	//   virtual from Mesh::Build::Core, defined in Mesh::Build::Frontal::VariableSegLength

	using Mesh::Build::Frontal::start_at;             // virtual from Mesh::Build::Core
	using Mesh::Build::Frontal::towards;              // virtual from Mesh::Build::Core
	using Mesh::Build::Frontal::set_orientation;      // virtual from Mesh::Build::Core
	using Mesh::Build::Frontal::set_entire_manifold;  // virtual from Mesh::Build::Core

	Mesh build_mesh ( ) override;            // virtual from Mesh::Build::Core
	
};  // end of class Mesh::Build::Frontal::VariableSegLength::StartStopPoints


Mesh::Build Mesh::Build::Frontal::StartStopPoints::set_variable_length ( const Function & l )
// virtual from Mesh::Build::Core, here overridden
{	std::shared_ptr < Mesh::Build::Frontal::VariableSegLength::StartStopPoints > res_core =
		std::make_shared < Mesh::Build::Frontal::VariableSegLength::StartStopPoints > ();
	res_core->orientation = this->orientation;
	assert ( not this->entire_manifold );
	res_core->entire_manifold = false;
	res_core->start = this->start;
	res_core->towards = this->towards;
	res_core->stop = this->stop;
	res_core->seg_len = l;
	return  Mesh::Build ( tag::whose_core_is, res_core );                                    }
	
Mesh::Build Mesh::Build::Frontal::VariableSegLength::stop_at ( const Cell & P )
// virtual from Mesh::Build::Core, here overridden
{	if ( not this->ready_for_stop_at )
	{	std::cout << "stop_at can only be invoked after start_at ";
		std::cout << "or after towards" << std::endl;
		exit ( 1 );                                                  }
	assert ( not this->entire_manifold );
	// {	std::cout << "if you want to mesh the entire manifold," << std::endl;
	// 	std::cout << "you cannot declare a stopping point" << std::endl;
	// 	exit ( 1 );                                                            }
	std::shared_ptr < Mesh::Build::Frontal::VariableSegLength::StartStopPoints > res_core =
		std::make_shared < Mesh::Build::Frontal::VariableSegLength::StartStopPoints > ();
	res_core->orientation = this->orientation;
	res_core->entire_manifold = this->entire_manifold;
	res_core->start = this->start;
	res_core->towards = this->towards;
	res_core->seg_len = this->seg_len;
	res_core->stop = P;
	return  Mesh::Build ( tag::whose_core_is, res_core );                                    }

//------------------------------------------------------------------------------------------------------//
//------------------------------------------------------------------------------------------------------//


Mesh Mesh::Build::Frontal::ConstantSegLength::build_mesh ()
// virtual from Mesh::Build::Core, here overridden

// since no boundary or stopping point are provided,
// we assume the user wants the entire working manifold
// we assume the working manifold is compact
// we also assume the user wants the inherent orientation
// a special case : mesh a quotient manifold entirely, intrinsic orientation
	
{	if ( not this->entire_manifold )
	{	std::cout << "you must specify entire manifold is to be meshed," << std::endl;
		std::cout << "or prescribe a boundary, or starting and stopping points" << std::endl;
		exit ( 1 );                                                                            }
	
	if ( this->seg_len <= 0. )
	{	std::cout << "you must specify a positive desired segment length" << std::endl;
		exit ( 1 );                                                                            }
	
	Mesh res ( tag::non_existent );
	temporary_vertex = Cell ( tag::vertex );
	frontal_nb_of_coords = Manifold::working .coordinates() .number_of ( tag::components );

	// rescale the working manifold's metric
	// thus we may work with a desired distance of 1.
	tag::Util::Metric old_metric = Manifold::working .core->metric;
	Manifold::working .core->metric =
		tag::Util::Metric ( tag::whose_core_is, old_metric .core->scale_sq_inv ( this->seg_len ) );

	Cell start_point = this->start;

	if ( not start_point .exists() )
	{	assert ( this->towards .size() == 0 );
		start_point = Manifold::working .core->search_start_ver();  }
		// call to 'search_start_ver' does not depend on the dimension of the mesh

	else  // 'this->start' exists, is there 'towards' ?
		if ( this->towards .size() > 0 )
		{	if ( this->orientation != tag::not_provided )  // orientation becomes irrelevant
			{	std::cout << "you cannot prescribe a direction and an orientation," << std::endl;
				std::cout << "please prescribe either one or another" << std::endl;
				exit ( 1 );                                                                        }
			res = Mesh ( tag::whose_core_is,
			      new Mesh::Connected::OneDim ( tag::with, 1, tag::segments, tag::one_dummy_wrapper ),
			      tag::move, tag::is_positive                                                         );
			// the number of segments does not count, and we don't know it yet
			// we compute it after the mesh is built, by counting segments
			// but we count segments using an iterator, and the iterator won't work
			// if this->core->nb_of_segs == 0, so we set nb_of_segs to 1 (dirty trick)
			// see Mesh::Iterator::Over::VerticesOfConnectedOneDimMesh::NormalOrder::reset in iterator.cpp
			// method below is virtual, calls Core or Quotient version
			// we could have used instead a dynamic_cast followed by an 'if'
			Manifold::working .core->frontal_method_1d
				( res, tag::start_with_inconsistent_mesh, tag::start_at, start_point,
				  tag::towards, this->towards, tag::stop_at, start_point             );
			// calls frontal_construct_1d  line 2497
			// frontal_construct calls update_info_connected_one_dim ( *this, start, stop )
			goto finish;
		}  // end of  if ( this->towards .size() > 0 )

	// method below is virtual, calls Core or Quotient version
	// we could have used instead a dynamic_cast followed by an 'if'
	Manifold::working .core->frontal_method
	( res, tag::start_with_non_existent_mesh,
	  tag::start_at, start_point, tag::orientation, this->orientation );
	// calls  frontal_construct  line 4021
	// calls  frontal_construct_1d  line 2497  or  line 2161  or  frontal_construct_2d  line 1255
	// frontal_construct calls update_info_connected_one_dim
	
	finish:
	Manifold::working .core->metric = old_metric;
	return res;
	
}  // end of  Mesh::Build::Frontal::ConstantSegLength::build_mesh


Mesh Mesh::Build::Frontal::ConstantSegLength::FromBoundary::build_mesh ()
// virtual from Mesh::Build::Core, here overridden

{	if ( this->seg_len <= 0. )
	{	std::cout << "you must specify a positive desired segment length" << std::endl;
		exit ( 1 );                                                                            }
	
	assert ( not this->entire_manifold );
	assert ( this->boundary .exists() );
	Mesh res ( tag::whose_core_is,
	           new Mesh::Fuzzy ( tag::of_dim, Manifold::working .topological_dim(),
	                             tag::one_dummy_wrapper                            ),
	           tag::move, tag::is_positive                                           );

	temporary_vertex = Cell ( tag::vertex );
	frontal_nb_of_coords = Manifold::working .coordinates() .number_of ( tag::components );

	// rescale the working manifold's metric
	// thus we may work with a desired distance of 1.
	tag::Util::Metric old_metric = Manifold::working .core->metric;
	Manifold::working .core->metric =
		tag::Util::Metric ( tag::whose_core_is, old_metric .core->scale_sq_inv ( this->seg_len ) );

	Cell start_cll = this->start;

	if ( not start_cll .exists() )
		// we search for a starting cell
	{	Mesh::Iterator it = this->boundary .iterator
			( tag::over_cells_of_max_dim, tag::orientation_compatible_with_mesh );
		it .reset();  assert ( it .in_range() );
		start_cll = *it;
		assert ( start_cll .dim() + 1 == Manifold::working .topological_dim() );   }

	else  // we have a starting point, we should have also a direction

	{	if ( this->towards .size() == 0 )
		{	std::cout << "the only reason to provide a starting point "
			          << "in this context" << std::endl;
			std::cout << "is if you want to prescribe a direction "
			          << "for the mesh to grow" << std::endl;
			exit ( 1 );                                                  }

		if ( this->orientation != tag::not_provided )
		{	std::cout << "you cannot provide orientation twice" << std::endl;
			std::cout << "(shortest_path and towards count as orientation)" << std::endl;
			exit ( 1 );                                                                  }

		// method below is virtual, calls Core or Quotient version
		// we could have used instead a dynamic_cast followed by an 'if'
		Manifold::working .core->frontal_method
			( res, tag::boundary, this->boundary,
			  tag::start_at, start_cll, tag::towards, this->towards );
		// calls  frontal_construct_2d  line 1437  then  line 1255
		goto finish;                                                       }

	// method below is virtual, calls Core or Quotient version
	// we could have used instead a dynamic_cast followed by an 'if'
	Manifold::working .core->frontal_method  // calls  frontal_construct  line 4188
		( res, tag::boundary, this->boundary,
		  tag::start_at, start_cll, tag::orientation, this->orientation );
	
	finish:
	Manifold::working .core->metric = old_metric;
	return res;

}  // end of  Mesh::Build::Frontal::ConstantSegLength::FromBoundary::build_mesh


Mesh Mesh::Build::Frontal::ConstantSegLength::StartStopPoints::build_mesh ()
// virtual from Mesh::Build::Core, here overridden

// 'this->start' and 'this->stop' are positive vertices

{	if ( this->seg_len <= 0. )
	{	std::cout << "you must specify a positive desired segment length" << std::endl;
		exit ( 1 );                                                                            }
	
	assert ( not this->entire_manifold );

	Mesh res ( tag::whose_core_is,
	           new Mesh::Connected::OneDim ( tag::with, 1, tag::segments, tag::one_dummy_wrapper ),
	           tag::move, tag::is_positive                                                         );
	// the number of segments does not count, and we don't know it yet
	// we compute it after the mesh is built, by counting segments
	// but we count segments using an iterator, and the iterator won't work
	// if this->core->nb_of_segs == 0, so we set nb_of_segs to 1 (dirty trick)
	// see Mesh::Iterator::Over::VerticesOfConnectedOneDimMesh::NormalOrder::reset in iterator.cpp

	temporary_vertex = Cell ( tag::vertex );
	frontal_nb_of_coords = Manifold::working .coordinates() .number_of ( tag::components );

	// rescale the working manifold's metric
	// thus we may work with a desired distance of 1.
	tag::Util::Metric old_metric = Manifold::working .core->metric;
	Manifold::working .core->metric =
		tag::Util::Metric ( tag::whose_core_is, old_metric .core->scale_sq_inv ( this->seg_len ) );

	if ( this->towards .size() == 0 )

		// method below is virtual, calls Core or Quotient version
		// we could have used instead a dynamic_cast followed by an 'if'
		Manifold::working .core->frontal_method_1d
			( res, tag::start_with_inconsistent_mesh, tag::start_at, this->start,
			  tag::stop_at, this->stop, tag::orientation, this->orientation      );
		// calls  frontal_construct  line 2199
		// frontal_construct calls update_info_connected_one_dim ( *this, start, stop )

	else  // 'towards' exists
		
		// method below is virtual, calls Core or Quotient version
		// we could have used instead a dynamic_cast followed by an 'if'
		Manifold::working .core->frontal_method_1d
		( res, tag::start_with_inconsistent_mesh, tag::start_at, this->start,
		  tag::towards, this->towards, tag::stop_at, this->stop              );
		//  calls frontal_construct_1d  line 2497
		// frontal_construct calls update_info_connected_one_dim ( *this, start, stop )

	Manifold::working .core->metric = old_metric;
	return res;
	
}  // end of  Mesh Mesh::Build::Frontal::ConstantSegLength::StartStopPoints::build_mesh

//------------------------------------------------------------------------------------------------------//


Mesh Mesh::Build::Frontal::VariableSegLength::build_mesh ()
// virtual from Mesh::Build::Core, here overridden

// since no boundary or stopping point are provided,
// we assume the user wants the entire working manifold
// we assume the working manifold is compact
// we also assume the user wants the inherent orientation
// a special case : mesh a quotient manifold entirely, intrinsic orientation
	
{	if ( not this->entire_manifold )
	{	std::cout << "you must specify entire manifold is to be meshed," << std::endl;
		std::cout << "or prescribe a boundary, or starting and stopping points" << std::endl;
		exit ( 1 );                                                                            }
	
	if ( not this->seg_len .exists() )
	{	std::cout << "you must specify a desired segment length" << std::endl;
		exit ( 1 );                                                             }
	
	Mesh res ( tag::non_existent );
	temporary_vertex = Cell ( tag::vertex );
	frontal_nb_of_coords = Manifold::working .coordinates() .number_of ( tag::components );

	// rescale the working manifold's metric
	// thus we may work with a desired distance of 1.
	tag::Util::Metric old_metric = Manifold::working .core->metric;
	Manifold::working .core->metric =
		tag::Util::Metric ( tag::whose_core_is, old_metric .core->scale_sq_inv ( this->seg_len ) );

	Cell start_point = this->start;

	if ( not start_point .exists() )
	{	assert ( this->towards .size() == 0 );
		start_point = Manifold::working .core->search_start_ver();  }
		// call to 'search_start_ver' does not depend on the dimension of the mesh

	else  // 'this->start' exists, is there 'towards' ?
		if ( this->towards .size() > 0 )
		{	if ( this->orientation != tag::not_provided )  // orientation becomes irrelevant
			{	std::cout << "you cannot prescribe a direction and an orientation," << std::endl;
				std::cout << "please prescribe either one or another" << std::endl;
				exit ( 1 );                                                                        }
			res = Mesh ( tag::whose_core_is,
			      new Mesh::Connected::OneDim ( tag::with, 1, tag::segments, tag::one_dummy_wrapper ),
			      tag::move, tag::is_positive                                                         );
			// the number of segments does not count, and we don't know it yet
			// we compute it after the mesh is built, by counting segments
			// but we count segments using an iterator, and the iterator won't work
			// if this->core->nb_of_segs == 0, so we set nb_of_segs to 1 (dirty trick)
			// see Mesh::Iterator::Over::VerticesOfConnectedOneDimMesh::NormalOrder::reset in iterator.cpp
			// method below is virtual, calls Core or Quotient version
			// we could have used instead a dynamic_cast followed by an 'if'
			Manifold::working .core->frontal_method_1d
				( res, tag::start_with_inconsistent_mesh, tag::start_at, start_point,
				  tag::towards, this->towards, tag::stop_at, start_point             );
			// calls frontal_construct_1d  line 2497
			// frontal_construct calls update_info_connected_one_dim ( *this, start, stop )
			goto finish;
		}  // end of  if ( this->towards .size() > 0 )

	// method below is virtual, calls Core or Quotient version
	// we could have used instead a dynamic_cast followed by an 'if'
	Manifold::working .core->frontal_method
	( res, tag::start_with_non_existent_mesh,
	  tag::start_at, start_point, tag::orientation, this->orientation );
	// calls  frontal_construct  line 4021
	// calls  frontal_construct_1d  line 2497  or  line 2161  or  frontal_construct_2d  line 1255
	// frontal_construct calls update_info_connected_one_dim
	
	finish:
	Manifold::working .core->metric = old_metric;
	return res;
	
}  // end of  Mesh::Build::Frontal::VariableSegLength::build_mesh


Mesh Mesh::Build::Frontal::VariableSegLength::FromBoundary::build_mesh ()
// virtual from Mesh::Build::Core, here overridden

{	if ( not this->seg_len .exists() )
	{	std::cout << "you must specify a desired segment length" << std::endl;
		exit ( 1 );                                                             }
	
	assert ( not this->entire_manifold );
	assert ( this->boundary .exists() );
	Mesh res ( tag::whose_core_is,
	           new Mesh::Fuzzy ( tag::of_dim, Manifold::working .topological_dim(),
	                             tag::one_dummy_wrapper                            ),
	           tag::move, tag::is_positive                                           );

	temporary_vertex = Cell ( tag::vertex );
	frontal_nb_of_coords = Manifold::working .coordinates() .number_of ( tag::components );

	// rescale the working manifold's metric
	// thus we may work with a desired distance of 1.
	tag::Util::Metric old_metric = Manifold::working .core->metric;
	Manifold::working .core->metric =
		tag::Util::Metric ( tag::whose_core_is, old_metric .core->scale_sq_inv ( this->seg_len ) );

	Cell start_cll = this->start;

	if ( not start_cll .exists() )
		// we search for a starting cell
	{	Mesh::Iterator it = this->boundary .iterator
			( tag::over_cells_of_max_dim, tag::orientation_compatible_with_mesh );
		it .reset();  assert ( it .in_range() );
		start_cll = *it;
		assert ( start_cll .dim() + 1 == Manifold::working .topological_dim() );   }

	else  // we have a starting point, we should have also a direction
	{	if ( this->towards .size() == 0 )
		{	std::cout << "the only reason to provide a starting point "
			          << "in this context" << std::endl;
			std::cout << "is if you want to prescribe a direction "
			          << "for the mesh to grow" << std::endl;
			exit ( 1 );                                                  }

		if ( this->orientation != tag::not_provided )
		{	std::cout << "you cannot provide orientation twice" << std::endl;
			std::cout << "(shortest_path and towards count as orientation)" << std::endl;
			exit ( 1 );                                                                  }

		// method below is virtual, calls Core or Quotient version
		// we could have used instead a dynamic_cast followed by an 'if'
		Manifold::working .core->frontal_method
			( res, tag::boundary, this->boundary,
			  tag::start_at, start_cll, tag::towards, this->towards );
		// calls  frontal_construct_2d  line 1437  then  line 1255
		goto finish;                                                       }

	// method below is virtual, calls Core or Quotient version
	// we could have used instead a dynamic_cast followed by an 'if'
	Manifold::working .core->frontal_method  // calls  frontal_construct  line 4188
		( res, tag::boundary, this->boundary,
		  tag::start_at, start_cll, tag::orientation, this->orientation );
	
	finish:
	Manifold::working .core->metric = old_metric;
	return res;

}  // end of  Mesh::Build::Frontal::VariableSegLength::FromBoundary::build_mesh


Mesh Mesh::Build::Frontal::VariableSegLength::StartStopPoints::build_mesh ()
// virtual from Mesh::Build::Core, here overridden

// 'this->start' and 'this->stop' are positive vertices

{	if ( not this->seg_len .exists() )
	{	std::cout << "you must specify a desired segment length" << std::endl;
		exit ( 1 );                                                             }
	
	assert ( not this->entire_manifold );

	Mesh res ( tag::whose_core_is,
	           new Mesh::Connected::OneDim ( tag::with, 1, tag::segments, tag::one_dummy_wrapper ),
	           tag::move, tag::is_positive                                                         );
	// the number of segments does not count, and we don't know it yet
	// we compute it after the mesh is built, by counting segments
	// but we count segments using an iterator, and the iterator won't work
	// if this->core->nb_of_segs == 0, so we set nb_of_segs to 1 (dirty trick)
	// see Mesh::Iterator::Over::VerticesOfConnectedOneDimMesh::NormalOrder::reset in iterator.cpp

	temporary_vertex = Cell ( tag::vertex );
	frontal_nb_of_coords = Manifold::working .coordinates() .number_of ( tag::components );

	// rescale the working manifold's metric
	// thus we may work with a desired distance of 1.
	tag::Util::Metric old_metric = Manifold::working .core->metric;
	Manifold::working .core->metric =
		tag::Util::Metric ( tag::whose_core_is, old_metric .core->scale_sq_inv ( this->seg_len ) );

	if ( this->towards .size() == 0 )

		// method below is virtual, calls Core or Quotient version
		// we could have used instead a dynamic_cast followed by an 'if'
		Manifold::working .core->frontal_method_1d
			( res, tag::start_with_inconsistent_mesh, tag::start_at, this->start,
			  tag::stop_at, this->stop, tag::orientation, this->orientation      );
		// calls  frontal_construct  line 2199
		// frontal_construct calls update_info_connected_one_dim ( *this, start, stop )

	else  // 'towards' exists
		
		// method below is virtual, calls Core or Quotient version
		// we could have used instead a dynamic_cast followed by an 'if'
		Manifold::working .core->frontal_method_1d
		( res, tag::start_with_inconsistent_mesh, tag::start_at, this->start,
		  tag::towards, this->towards, tag::stop_at, this->stop              );
		//  calls frontal_construct_1d  line 2497
		// frontal_construct calls update_info_connected_one_dim ( *this, start, stop )

	Manifold::working .core->metric = old_metric;
	return res;
	
}  // end of  Mesh Mesh::Build::Frontal::VariableSegLength::StartStopPoints::build_mesh

//------------------------------------------------------------------------------------------------------//

#endif  // ifndef MANIFEM_NO_FRONTAL


	
