
//   iterator.h  2025.01.05

//   This file is part of maniFEM, a C++ library for meshes and finite elements on manifolds.

//   Copyright  2019 - 2025  Cristian Barbarosie  cristian.barbarosie@gmail.com

//   https://maniFEM.rd.ciencias.ulisboa.pt/
//   https://codeberg.org/cristian.barbarosie/maniFEM

//   ManiFEM is free software: you can redistribute it and/or modify it
//   under the terms of the GNU Lesser General Public License as published
//   by the Free Software Foundation, either version 3 of the License
//   or (at your option) any later version.

//   ManiFEM is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//   See the GNU Lesser General Public License for more details.

//   You should have received a copy of the GNU Lesser General Public License
//   along with maniFEM.  If not, see <https://www.gnu.org/licenses/>.


#ifndef MANIFEM_ITERATOR_H
#define MANIFEM_ITERATOR_H


namespace maniFEM {

namespace tag {
	struct OverTwoVerticesOfSeg { };
	static const OverTwoVerticesOfSeg over_two_vertices_of_seg;
	struct OverVerticesOf { };  static const OverVerticesOf over_vertices_of;
	struct OverSegmentsOf { };  static const OverSegmentsOf over_segments_of;
	struct OverCellsOf { };  static const OverCellsOf over_cells_of;
	struct FuzzyPosMesh { };  static const FuzzyPosMesh fuzzy_pos_mesh;
	struct Inner { };  static const Inner inner;
}

//------------------------------------------------------------------------------------------------------//

// class Mesh::Iterator defined in mesh.h

//------------------------------------------------------------------------------------------------------//

class Mesh::Iterator::Core

// abstract class

// iterates over all cells of a given mesh (cells of given dimension)
// or, iterates over all cells above a given cell (containing a given cell)

{	public :

	inline Core ( ) { };
	
	virtual ~Core ( ) { };

	virtual void reset ( ) = 0;
	virtual void reset ( const tag::StartAt &, Cell::Core * cll ) = 0;

	virtual Cell deref ( ) = 0;
	virtual void advance ( ) = 0;
	virtual bool in_range ( ) = 0;

	// in most cases, 'clone' makes a mere copy
	// could do more complicated things in very specific cases
	virtual Mesh::Iterator::Core * clone ( ) = 0;
	
};  // end of class Mesh::Iterator::Core


inline Mesh::Iterator::CellIterator ( const Mesh::Iterator & it )
:	core { it .core->clone() }
{	}
	

inline Mesh::Iterator::CellIterator ( Mesh::Iterator && it )
:	core { std::move ( it .core ) }
{	}
	

inline Mesh::Iterator & Mesh::Iterator::operator= ( const Mesh::Iterator & it )
{	this->core .reset ( it .core->clone() );
	return *this;                             }


inline Mesh::Iterator & Mesh::Iterator::operator= ( Mesh::Iterator && it )
{	this->core = std::move ( it .core );
	return *this;                         }

	
inline void Mesh::Iterator::reset ( )  {  this->core->reset();  }


inline void Mesh::Iterator::reset ( const tag::StartAt &, const Cell & cll )
{  this->core->reset( tag::start_at, cll.core );  }
	

inline Cell Mesh::Iterator::operator* ( )
{	return this->core->deref();  }


// unfortunately code below does not work because often 'deref' returns
// a newly created Cell wrapper (usually with a previously existing core)
// inline Cell & Mesh::Iterator::operator* ( )
// {	return this->core->deref();  }
// inline Cell * Mesh::Iterator::operator-> ( )
// {	return & this->core->deref();  }


inline Mesh::Iterator & Mesh::Iterator::operator++ ( )  {  return this->advance();  }


inline Mesh::Iterator & Mesh::Iterator::operator++ ( int )  {  return this->advance();  }


inline Mesh::Iterator & Mesh::Iterator::advance ( )
{	this->core->advance();  return *this;  }


inline bool Mesh::Iterator::in_range ( )  {  return this->core->in_range();  }
		
//------------------------------------------------------------------------------------------------------//
//------------------------------------------------------------------------------------------------------//


class Cell::Iterator::Core

// abstract class

// iterates over all meshes containing a given cell (meshes of given dimension)

{	public :

	inline Core ( ) { };
	
	virtual ~Core ( ) { };

	virtual void reset ( ) = 0;

	virtual Mesh deref ( ) = 0;
	virtual void advance ( ) = 0;
	virtual bool in_range ( ) = 0;

	// in most cases, 'clone' makes a mere copy
	// could do more complicated things in very specific cases
	virtual Cell::Iterator::Core * clone ( ) = 0;
	
};  // end of class Cell::Iterator::Core


inline Cell::Iterator::MeshIterator ( const Cell::Iterator & it)
:	core { it .core->clone() }
{	}
	

inline Cell::Iterator::MeshIterator ( Cell::Iterator && it)
:	core { std::move ( it .core ) }
{	}
	

inline Cell::Iterator & Cell::Iterator::operator= ( const Cell::Iterator & it )
{	this->core .reset ( it .core->clone() );
	return *this;                             }


inline Cell::Iterator & Cell::Iterator::operator= ( Cell::Iterator && it )
{	this->core = std::move ( it .core );
	return *this;                         }

	
inline void Cell::Iterator::reset ( )  {  this->core->reset();  }


inline Mesh Cell::Iterator::operator* ( )
{	return this->core->deref();  }


// unfortunately code below does not work because often 'deref' returns
// a newly created Cell wrapper (usually with a previously existing core)
// inline Mesh & Cell::Iterator::operator* ( )
// {	return this->core->deref();  }
// inline Mesh * Cell::Iterator::operator-> ( )
// {	return & this->core->deref();  }


inline Cell::Iterator & Cell::Iterator::operator++ ( )  {  return this->advance();  }


inline Cell::Iterator & Cell::Iterator::operator++ ( int )  {  return this->advance();  }


inline Cell::Iterator & Cell::Iterator::advance ( )
{	this->core->advance();  return *this;  }


inline bool Cell::Iterator::in_range ( )  {  return this->core->in_range();  }
		
//------------------------------------------------------------------------------------------------------//
//------------------------------------------------------------------------------------------------------//


class Mesh::Iterator::Adaptor::ForcePositive : public Mesh::Iterator::Core

// modifies the behaviour of another Mesh::Iterator
// forcing the resulting cells to be positive

{	public :

	Mesh::Iterator base;

	inline ForcePositive ( Mesh::Iterator && b )
	:	Mesh::Iterator::Core::Core (), base { b }
	{	};

	void reset ( );  // virtual from Mesh::Iterator::Core
	void reset ( const tag::StartAt &, Cell::Core * cll );
	// virtual from Mesh::Iterator::Core, here execution forbidden
	// Cell deref ( )  stays pure virtual from Mesh::Iterator::Core
	void advance ( );  // virtual from Mesh::Iterator::Core
	bool in_range ( );  // virtual from Mesh::Iterator::Core

};  // end of class Mesh::Iterator::Adaptor::ForcePositive


//------------------------------------------------------------------------------------------------------//

//      ITERATORS OVER VERTICES

//------------------------------------------------------------------------------------------------------//
		

inline Mesh::Iterator Mesh::iterator ( const tag::OverVertices &, const tag::AsFound & ) const
// if this->dim() == 0, return oriented vertices
// if this->dim() >= 1, return positive vertices
// it does not matter whether  this->is_positive()  or not
{	return  Mesh::Iterator ( tag::whose_core_is, this->core->iterator
		( tag::over_vertices, tag::as_found, tag::this_mesh_is_positive ) );  }


inline Mesh::Iterator Mesh::iterator
( const tag::OverVertices &, const tag::ForcePositive & ) const
// it does not matter whether  this->is_positive()  or not
{	return  Mesh::Iterator ( tag::whose_core_is, this->core->iterator
		( tag::over_vertices, tag::force_positive, tag::this_mesh_is_positive ) );  }


inline Mesh::Iterator Mesh::iterator ( const tag::OverVertices & ) const
// if this->dim() == 0, print error message
// if this->dim() >= 1, return positive vertices
// it does not matter whether  this->is_positive()  or not
{	return  Mesh::Iterator ( tag::whose_core_is, this->core->iterator
		( tag::over_vertices, tag::this_mesh_is_positive )              );  }


inline Mesh::Iterator Mesh::iterator ( const tag::OverVertices &, const tag::OrientCompWithMesh & ) const
// if this->dim() == 0, return oriented vertices
// if this->dim() >= 1, print error message
{	if ( this->is_positive() )
		return  Mesh::Iterator ( tag::whose_core_is, this->core->iterator
			( tag::over_vertices, tag::orientation_compatible_with_mesh, tag::this_mesh_is_positive ) );
	// else : negative mesh
	return  Mesh::Iterator ( tag::whose_core_is, this->core->iterator
		( tag::over_vertices, tag::orientation_opposed_to_mesh, tag::this_mesh_is_positive ) );    }


inline Mesh::Iterator Mesh::iterator ( const tag::OverVertices &, const tag::OrientOpposToMesh & ) const
// if this->dim() == 0, return oriented vertices
// if this->dim() >= 1, print error message
{	if ( this->is_positive() )
		return  Mesh::Iterator ( tag::whose_core_is, this->core->iterator
			( tag::over_vertices, tag::orientation_opposed_to_mesh, tag::this_mesh_is_positive ) );
	// else : negative mesh
	return  Mesh::Iterator ( tag::whose_core_is, this->core->iterator
		( tag::over_vertices, tag::orientation_compatible_with_mesh, tag::this_mesh_is_positive ) );    }


inline Mesh::Iterator Mesh::iterator
( const tag::OverVertices &, const tag::AsFound &, const tag::RequireOrder & ) const
// if this->dim() == 0, return oriented vertices
// for Connected::OneDim, return positive vertices, ordered
// for a Fuzzy mesh, print error message (no order)
{	if ( this->is_positive() )
		return  Mesh::Iterator ( tag::whose_core_is, this->core->iterator
			( tag::over_vertices, tag::as_found,
			  tag::require_order, tag::this_mesh_is_positive )             );
	// else : negative mesh
	return  Mesh::Iterator ( tag::whose_core_is, this->core->iterator
		( tag::over_vertices, tag::as_found,
		  tag::backwards, tag::this_mesh_is_positive )                 );     }


inline Mesh::Iterator Mesh::iterator
( const tag::OverVertices &, const tag::ForcePositive &, const tag::RequireOrder & ) const
// for a Fuzzy mesh, print error message (no order)
{	if ( this->is_positive() )
		return  Mesh::Iterator ( tag::whose_core_is, this->core->iterator
			( tag::over_vertices, tag::force_positive, tag::require_order, tag::this_mesh_is_positive ) );
	// else : negative mesh
	return  Mesh::Iterator ( tag::whose_core_is, this->core->iterator
		( tag::over_vertices, tag::force_positive,
		  tag::backwards, tag::this_mesh_is_positive )                 );                                  }


inline Mesh::Iterator Mesh::iterator ( const tag::OverVertices &, const tag::RequireOrder & ) const
// for ZeroDim, print error message
// for Connected::OneDim, return positive vertices, ordered
// for a Fuzzy mesh, print error message (no order)
{	if ( this->is_positive() )
		return  Mesh::Iterator ( tag::whose_core_is, this->core->iterator
			( tag::over_vertices, tag::require_order, tag::this_mesh_is_positive ) );
	// else : negative mesh
	return  Mesh::Iterator ( tag::whose_core_is, this->core->iterator
		( tag::over_vertices, tag::backwards, tag::this_mesh_is_positive ) );         }


inline Mesh::Iterator Mesh::iterator
( const tag::OverVertices &, const tag::OrientCompWithMesh &, const tag::RequireOrder & ) const
// if this->dim() == 0, return oriented vertices
// if this->dim() >= 1, print error message
{	if ( this->is_positive() )
		return  Mesh::Iterator ( tag::whose_core_is, this->core->iterator
			( tag::over_vertices, tag::orientation_compatible_with_mesh,
			  tag::require_order, tag::this_mesh_is_positive            )   );
	// else : negative mesh
	return  Mesh::Iterator ( tag::whose_core_is, this->core->iterator
		( tag::over_vertices, tag::orientation_opposed_to_mesh,
		  tag::backwards, tag::this_mesh_is_positive           )       );      }


inline Mesh::Iterator Mesh::iterator
( const tag::OverVertices &, const tag::OrientOpposToMesh &, const tag::RequireOrder & ) const
// if this->dim() == 0, return oriented vertices
// if this->dim() >= 1, print error message
{	if ( this->is_positive() )
		return  Mesh::Iterator ( tag::whose_core_is, this->core->iterator
			( tag::over_vertices, tag::orientation_opposed_to_mesh,
			  tag::require_order, tag::this_mesh_is_positive       )        );
	// else : negative mesh
	return  Mesh::Iterator ( tag::whose_core_is, this->core->iterator
		( tag::over_vertices, tag::orientation_compatible_with_mesh,
		  tag::backwards, tag::this_mesh_is_positive                )  );      }


inline Mesh::Iterator Mesh::iterator
( const tag::OverVertices &, const tag::AsFound &, const tag::Backwards & ) const
// for a Fuzzy mesh, print error message (no order)
{	if ( this->is_positive() )
		return  Mesh::Iterator ( tag::whose_core_is, this->core->iterator
			( tag::over_vertices, tag::as_found,
			  tag::backwards, tag::this_mesh_is_positive )                 );
	// else : negative mesh, back to normal order
	return  Mesh::Iterator ( tag::whose_core_is, this->core->iterator
	  ( tag::over_vertices, tag::as_found,
		  tag::require_order, tag::this_mesh_is_positive )             );     }


inline Mesh::Iterator Mesh:: iterator
( const tag::OverVertices &, const tag::ForcePositive &, const tag::Backwards & ) const
// for a Fuzzy mesh, print error message (no order)
{ if ( this->is_positive() )
		return  Mesh::Iterator ( tag::whose_core_is, this->core->iterator
			( tag::over_vertices, tag::force_positive,
			  tag::backwards, tag::this_mesh_is_positive )                 );
	// else : negative mesh, back to normal order
	return  Mesh::Iterator ( tag::whose_core_is, this->core->iterator
		( tag::over_vertices, tag::force_positive,
		  tag::require_order, tag::this_mesh_is_positive )             );     }


inline Mesh::Iterator Mesh::iterator ( const tag::OverVertices &, const tag::Backwards & ) const
// for ZeroDim, print error message
// for Connected::OneDim, return positive vertices in reverse order
// for a Fuzzy mesh, print error message (no order)
{	if ( this->is_positive() )
		return  Mesh::Iterator ( tag::whose_core_is, this->core->iterator
			( tag::over_vertices, tag::backwards, tag::this_mesh_is_positive ) );
	// else : negative mesh, back to normal order
	return  Mesh::Iterator ( tag::whose_core_is, this->core->iterator
		( tag::over_vertices, tag::require_order, tag::this_mesh_is_positive ) );  }


inline Mesh::Iterator Mesh::iterator
( const tag::OverVertices &, const tag::OrientCompWithMesh &, const tag::Backwards & ) const
// if this->dim() == 0, return oriented vertices
// if this->dim() >= 1, print error message
{	if ( this->is_positive() )
		return  Mesh::Iterator ( tag::whose_core_is, this->core->iterator
			( tag::over_vertices, tag::orientation_compatible_with_mesh,
			  tag::backwards, tag::this_mesh_is_positive                )  );
	// else : negative mesh, back to normal order
	return  Mesh::Iterator ( tag::whose_core_is, this->core->iterator
		( tag::over_vertices, tag::orientation_opposed_to_mesh,
		  tag::require_order, tag::this_mesh_is_positive       )       );     }


inline Mesh::Iterator Mesh::iterator
( const tag::OverVertices &, const tag::OrientOpposToMesh &, const tag::Backwards & ) const
// if this->dim() == 0, return oriented vertices
// if this->dim() >= 1, print error message
{	if ( this->is_positive() )
		return  Mesh::Iterator ( tag::whose_core_is, this->core->iterator
			( tag::over_vertices, tag::orientation_opposed_to_mesh,
			  tag::backwards, tag::this_mesh_is_positive           )       );
	// else : negative mesh, back to normal order
	return  Mesh::Iterator ( tag::whose_core_is, this->core->iterator
		( tag::over_vertices, tag::orientation_compatible_with_mesh,
		  tag::require_order, tag::this_mesh_is_positive            )  );     }


//------------------------------------------------------------------------------------------------------//

//      ITERATORS OVER SEGMENTS

//------------------------------------------------------------------------------------------------------//


inline Mesh::Iterator Mesh::iterator ( const tag::OverSegments &, const tag::AsFound & ) const
// if this->dim() == 0, print error message
// if this->dim() == 1, return oriented segments
// if this->dim() >= 2, return positive segments
// it does not matter whether  this->is_positive()  or not
{	return  Mesh::Iterator ( tag::whose_core_is, this->core->iterator
		( tag::over_segments, tag::as_found, tag::this_mesh_is_positive ) );  }


inline Mesh::Iterator Mesh::iterator ( const tag::OverSegments &, const tag::ForcePositive & ) const
// if this->dim() == 0, print error message
// it does not matter whether  this->is_positive()  or not
{	return  Mesh::Iterator ( tag::whose_core_is, this->core->iterator
		( tag::over_segments, tag::force_positive, tag::this_mesh_is_positive ) );  }


inline Mesh::Iterator Mesh::iterator ( const tag::OverSegments &, const tag::OrientCompWithMesh & ) const
// if this->dim() == 0, print error message
// if this->dim() == 1, return oriented segments
// if this->dim() >= 2, print error message
{	if ( this->is_positive() )
		return  Mesh::Iterator ( tag::whose_core_is, this->core->iterator
			( tag::over_segments, tag::orientation_compatible_with_mesh, tag::this_mesh_is_positive ) );
	// else : negative mesh
	return  Mesh::Iterator ( tag::whose_core_is, this->core->iterator
		( tag::over_segments, tag::orientation_opposed_to_mesh, tag::this_mesh_is_positive ) );          }


inline Mesh::Iterator Mesh::iterator ( const tag::OverSegments &, const tag::OrientOpposToMesh & ) const
// if this->dim() == 0, print error message
// if this->dim() == 1, return oriented segments
// if this->dim() >= 2, print error message
{	if ( this->is_positive() )
		return  Mesh::Iterator ( tag::whose_core_is, this->core->iterator
			( tag::over_segments, tag::orientation_opposed_to_mesh, tag::this_mesh_is_positive ) );
	// else : negative mesh
	return  Mesh::Iterator ( tag::whose_core_is, this->core->iterator
		( tag::over_segments, tag::orientation_compatible_with_mesh, tag::this_mesh_is_positive ) );  }


inline Mesh::Iterator Mesh::iterator
( const tag::OverSegments &, const tag::AsFound &, const tag::RequireOrder & ) const
// if this->dim() == 0, print error message
// for Connected::OneDim, return oriented segments, ordered
// for a Fuzzy mesh, print error message (no order)
{	if ( this->is_positive() )
		return  Mesh::Iterator ( tag::whose_core_is, this->core->iterator
			( tag::over_segments, tag::as_found, tag::require_order, tag::this_mesh_is_positive ) );
	// else : negative mesh
	return  Mesh::Iterator ( tag::whose_core_is, this->core->iterator
		( tag::over_segments, tag::as_found, tag::backwards, tag::this_mesh_is_positive ) );         }


inline Mesh::Iterator Mesh::iterator
( const tag::OverSegments &, const tag::ForcePositive &, const tag::RequireOrder & ) const
// if this->dim() == 0, print error message
// for a Fuzzy mesh, print error message (no order)
{	if ( this->is_positive() )
		return  Mesh::Iterator ( tag::whose_core_is, this->core->iterator
			( tag::over_segments, tag::force_positive, tag::require_order, tag::this_mesh_is_positive ) );
	// else : negative mesh
	return  Mesh::Iterator ( tag::whose_core_is, this->core->iterator
		( tag::over_segments, tag::force_positive, tag::backwards, tag::this_mesh_is_positive ) );         }


inline Mesh::Iterator Mesh::iterator
( const tag::OverSegments &, const tag::OrientCompWithMesh &, const tag::RequireOrder & ) const
// if this->dim() == 0, print error message
// if this->dim() == 1, return oriented segments
// if this->dim() >= 2, print error message
{	if ( this->is_positive() )
		return  Mesh::Iterator ( tag::whose_core_is, this->core->iterator
			( tag::over_segments, tag::orientation_compatible_with_mesh,
			  tag::require_order, tag::this_mesh_is_positive            )  );
	// else : negative mesh
	return  Mesh::Iterator ( tag::whose_core_is, this->core->iterator
		( tag::over_segments, tag::orientation_opposed_to_mesh,
		  tag::backwards, tag::this_mesh_is_positive           )       );     }


inline Mesh::Iterator Mesh::iterator
( const tag::OverSegments &, const tag::OrientOpposToMesh &, const tag::RequireOrder & ) const
// if this->dim() == 0, print error message
// if this->dim() == 1, return oriented segments
// if this->dim() >= 2, print error message
{	if ( this->is_positive() )
		return  Mesh::Iterator ( tag::whose_core_is, this->core->iterator
			( tag::over_segments, tag::orientation_opposed_to_mesh,
			  tag::require_order, tag::this_mesh_is_positive       )       );
	// else : negative mesh
	return  Mesh::Iterator ( tag::whose_core_is, this->core->iterator
		( tag::over_segments, tag::orientation_compatible_with_mesh,
		  tag::backwards, tag::this_mesh_is_positive                )  );     }


inline Mesh::Iterator Mesh::iterator
( const tag::OverSegments &, const tag::AsFound &, const tag::Backwards & ) const
// if this->dim() == 0, print error message
// if this->dim() == 1, return oriented segments
// if this->dim() >= 2, print error message
{	if ( this->is_positive() )
		return  Mesh::Iterator ( tag::whose_core_is, this->core->iterator
			( tag::over_segments, tag::as_found, tag::backwards, tag::this_mesh_is_positive ) );
	// else : negative mesh, back to normal order
	return  Mesh::Iterator ( tag::whose_core_is, this->core->iterator
	  ( tag::over_segments, tag::as_found, tag::require_order, tag::this_mesh_is_positive ) );  }


inline Mesh::Iterator Mesh:: iterator
( const tag::OverSegments &, const tag::ForcePositive &, const tag::Backwards & ) const
// if this->dim() == 0, print error message
// if this->dim() >= 2, print error message (no order)
{ if ( this->is_positive() )
		return  Mesh::Iterator ( tag::whose_core_is, this->core->iterator
			( tag::over_segments, tag::force_positive, tag::backwards, tag::this_mesh_is_positive ) );
	// else : negative mesh, back to normal order
	return  Mesh::Iterator ( tag::whose_core_is, this->core->iterator
		( tag::over_segments, tag::force_positive, tag::require_order, tag::this_mesh_is_positive ) );  }


inline Mesh::Iterator Mesh::iterator
( const tag::OverSegments &, const tag::OrientCompWithMesh &, const tag::Backwards & ) const
// if this->dim() == 0, print error message
// if this->dim() == 1, return oriented segments
// if this->dim() >= 2, print error message
{	if ( this->is_positive() )
		return  Mesh::Iterator ( tag::whose_core_is, this->core->iterator
			( tag::over_segments, tag::orientation_compatible_with_mesh,
			  tag::backwards, tag::this_mesh_is_positive                )  );
	// else : negative mesh, back to normal order
	return  Mesh::Iterator ( tag::whose_core_is, this->core->iterator
		( tag::over_segments, tag::orientation_opposed_to_mesh,
		  tag::require_order, tag::this_mesh_is_positive       )       );     }


inline Mesh::Iterator Mesh::iterator
( const tag::OverSegments &, const tag::OrientOpposToMesh &, const tag::Backwards & ) const
// if this->dim() == 0, print error message
// if this->dim() == 1, return oriented segments
// if this->dim() >= 2, print error message
{	if ( this->is_positive() )
		return  Mesh::Iterator ( tag::whose_core_is, this->core->iterator
			( tag::over_segments, tag::orientation_opposed_to_mesh,
			  tag::backwards, tag::this_mesh_is_positive           )       );
	// else : negative mesh, back to normal order
	return  Mesh::Iterator ( tag::whose_core_is, this->core->iterator
		( tag::over_segments, tag::orientation_compatible_with_mesh,
		  tag::require_order, tag::this_mesh_is_positive            )  );     }


//------------------------------------------------------------------------------------------------------//

//      ITERATORS OVER HIGH DIMENSIONAL CELLS

//------------------------------------------------------------------------------------------------------//


inline Mesh::Iterator Mesh::iterator
( const tag::OverCellsOfDim &, const size_t d, const tag::AsFound & ) const
// if this->dim() == d, return oriented cells
// if d < this->dim(),  return positive cells
// it does not matter whether  this->is_positive()  or not
{	return  Mesh::Iterator ( tag::whose_core_is, this->core->iterator
		( tag::over_cells_of_dim, d, tag::as_found, tag::this_mesh_is_positive ) );  }


inline Mesh::Iterator Mesh::iterator
( const tag::OverCellsOfDim &, const size_t d, const tag::CellHasLowDim & ) const
// d < this->dim(),  return positive cells
// it does not matter whether  this->is_positive()  or not
{	return  Mesh::Iterator ( tag::whose_core_is, this->core->iterator
		( tag::over_cells_of_dim, d, tag::cell_has_low_dim, tag::this_mesh_is_positive ) );  }


inline Mesh::Iterator Mesh::iterator
( const tag::OverCellsOfDim &, const size_t d, const tag::ForcePositive & ) const
// it does not matter whether  this->is_positive()  or not
{	return  Mesh::Iterator ( tag::whose_core_is, this->core->iterator
		( tag::over_cells_of_dim, d, tag::force_positive, tag::this_mesh_is_positive ) );  }


inline Mesh::Iterator Mesh::iterator ( const tag::OverCellsOfDim &, const size_t d ) const
// if this->dim() == d, return oriented cells
// special case : if this->dim() == d == 0, print error message
// if d < this->dim(),  return positive cells
// it does not matter whether  this->is_positive()  or not
{	return  Mesh::Iterator ( tag::whose_core_is, this->core->iterator
		( tag::over_cells_of_dim, d, tag::this_mesh_is_positive )      );  }


inline Mesh::Iterator Mesh::iterator
( const tag::OverCellsOfDim &, const size_t d, const tag::OrientCompWithMesh & ) const
// assert  this->dim() == d
{	if ( this->is_positive() )
		return  Mesh::Iterator ( tag::whose_core_is, this->core->iterator
			( tag::over_cells_of_dim, d, tag::orientation_compatible_with_mesh,
			  tag::this_mesh_is_positive                                       ) );
	// else : negative mesh
	return  Mesh::Iterator ( tag::whose_core_is, this->core->iterator
		( tag::over_cells_of_dim, d, tag::orientation_opposed_to_mesh, tag::this_mesh_is_positive ) );  }


inline Mesh::Iterator Mesh::iterator
( const tag::OverCellsOfDim &, const size_t d, const tag::OrientOpposToMesh & ) const
// assert  this->dim() == d
{	if ( this->is_positive() )
		return  Mesh::Iterator ( tag::whose_core_is, this->core->iterator
			( tag::over_cells_of_dim, d, tag::orientation_opposed_to_mesh, tag::this_mesh_is_positive ) );
	// else : negative mesh
	return  Mesh::Iterator ( tag::whose_core_is, this->core->iterator
		( tag::over_cells_of_dim, d, tag::orientation_compatible_with_mesh,
		  tag::this_mesh_is_positive                                       ) );                            }


inline Mesh::Iterator Mesh::iterator
( const tag::OverCellsOfDim &, const size_t d, const tag::AsFound &, const tag::RequireOrder & ) const
// for a Fuzzy mesh, print error message (no order)
{	if ( this->is_positive() )
		return  Mesh::Iterator ( tag::whose_core_is, this->core->iterator
			( tag::over_cells_of_dim, d, tag::as_found, tag::require_order, tag::this_mesh_is_positive ) );
	// else : negative mesh
	return  Mesh::Iterator ( tag::whose_core_is, this->core->iterator
		( tag::over_cells_of_dim, d, tag::as_found, tag::backwards, tag::this_mesh_is_positive ) );        }


inline Mesh::Iterator Mesh::iterator ( const tag::OverCellsOfDim &, const size_t d,
                                       const tag::ForcePositive &, const tag::RequireOrder & ) const
// for a Fuzzy mesh, print error message (no order)
{	if ( this->is_positive() )
		return  Mesh::Iterator ( tag::whose_core_is, this->core->iterator
			( tag::over_cells_of_dim, d, tag::force_positive,
			  tag::require_order, tag::this_mesh_is_positive )             );
	// else : negative mesh
	return  Mesh::Iterator ( tag::whose_core_is, this->core->iterator
		( tag::over_cells_of_dim, d, tag::force_positive,
		  tag::backwards, tag::this_mesh_is_positive     )             );     }


inline Mesh::Iterator Mesh::iterator
( const tag::OverCellsOfDim &, const size_t d, const tag::RequireOrder & ) const
// if this->dim() == d, return oriented cells
// special case : if this->dim() == d == 0, print error message
// if d < this->dim(),  return positive cells
// for a Fuzzy mesh, print error message (no order)
{	if ( this->is_positive() )
		return  Mesh::Iterator ( tag::whose_core_is, this->core->iterator
			( tag::over_cells_of_dim, d, tag::require_order, tag::this_mesh_is_positive ) );
	// else : negative mesh
	return  Mesh::Iterator ( tag::whose_core_is, this->core->iterator
		( tag::over_cells_of_dim, d, tag::backwards, tag::this_mesh_is_positive ) );         }


inline Mesh::Iterator Mesh::iterator ( const tag::OverCellsOfDim &, const size_t d,
                                       const tag::OrientCompWithMesh &, const tag::RequireOrder & ) const
// assert  this->dim() == d
// for a Fuzzy mesh, print error message (no order)
{	if ( this->is_positive() )
		return  Mesh::Iterator ( tag::whose_core_is, this->core->iterator
			( tag::over_cells_of_dim, d, tag::orientation_compatible_with_mesh,
			  tag::require_order, tag::this_mesh_is_positive                   ) );
	// else : negative mesh
	return  Mesh::Iterator ( tag::whose_core_is, this->core->iterator
		( tag::over_cells_of_dim, d, tag::orientation_opposed_to_mesh,
		  tag::backwards, tag::this_mesh_is_positive                  ) );          }


inline Mesh::Iterator Mesh::iterator ( const tag::OverCellsOfDim &, const size_t d,
                                       const tag::OrientOpposToMesh &, const tag::RequireOrder & ) const
// assert  this->dim() == d
// for a Fuzzy mesh, print error message (no order)
{	if ( this->is_positive() )
		return  Mesh::Iterator ( tag::whose_core_is, this->core->iterator
			( tag::over_cells_of_dim, d, tag::orientation_opposed_to_mesh,
			  tag::require_order, tag::this_mesh_is_positive              ) );
	// else : negative mesh
	return  Mesh::Iterator ( tag::whose_core_is, this->core->iterator
		( tag::over_cells_of_dim, d, tag::orientation_compatible_with_mesh,
		  tag::backwards, tag::this_mesh_is_positive                       ) );  }


inline Mesh::Iterator Mesh::iterator
( const tag::OverCellsOfDim &, const size_t d, const tag::AsFound &, const tag::Backwards & ) const
// for a Fuzzy mesh, print error message (no order)
{	if ( this->is_positive() )
		return  Mesh::Iterator ( tag::whose_core_is, this->core->iterator
			( tag::over_cells_of_dim, d, tag::as_found, tag::backwards, tag::this_mesh_is_positive ) );
	// else : negative mesh, back to normal order
	return  Mesh::Iterator ( tag::whose_core_is, this->core->iterator
	  ( tag::over_cells_of_dim, d, tag::as_found, tag::require_order, tag::this_mesh_is_positive ) );  }


inline Mesh::Iterator Mesh:: iterator
( const tag::OverCellsOfDim &, const size_t d, const tag::ForcePositive &, const tag::Backwards & ) const
// for a Fuzzy mesh, print error message (no order)
{ if ( this->is_positive() )
		return  Mesh::Iterator ( tag::whose_core_is, this->core->iterator
			( tag::over_cells_of_dim, d, tag::force_positive,
			  tag::backwards, tag::this_mesh_is_positive     )             );
	// else : negative mesh, back to normal order
	return  Mesh::Iterator ( tag::whose_core_is, this->core->iterator
		( tag::over_cells_of_dim, d, tag::force_positive,
		  tag::require_order, tag::this_mesh_is_positive )             );     }


inline Mesh::Iterator Mesh::iterator
( const tag::OverCellsOfDim &, const size_t d, const tag::Backwards & ) const
// if this->dim() == d, return oriented cells
// special case : if this->dim() == d == 0, print error message
// if d < this->dim(),  return positive cells
// for a Fuzzy mesh, print error message (no order)
{	if ( this->is_positive() )
		return  Mesh::Iterator ( tag::whose_core_is, this->core->iterator
			( tag::over_cells_of_dim, d, tag::backwards, tag::this_mesh_is_positive ) );
	// else : negative mesh, back to normal order
	return  Mesh::Iterator ( tag::whose_core_is, this->core->iterator
	  ( tag::over_cells_of_dim, d, tag::require_order, tag::this_mesh_is_positive ) );  }


inline Mesh::Iterator Mesh::iterator ( const tag::OverCellsOfDim &, const size_t d,
                                       const tag::OrientCompWithMesh &, const tag::Backwards & ) const
// assert  this->dim() == d
// for a Fuzzy mesh, print error message (no order)
{	if ( this->is_positive() )
		return  Mesh::Iterator ( tag::whose_core_is, this->core->iterator
			( tag::over_cells_of_dim, d, tag::orientation_compatible_with_mesh,
			  tag::backwards, tag::this_mesh_is_positive                       ) );
	// else : negative mesh, back to normal order
	return  Mesh::Iterator ( tag::whose_core_is, this->core->iterator
		( tag::over_cells_of_dim, d, tag::orientation_opposed_to_mesh,
		  tag::require_order, tag::this_mesh_is_positive              ) );          }


inline Mesh::Iterator Mesh::iterator ( const tag::OverCellsOfDim &, const size_t d,
                                       const tag::OrientOpposToMesh &, const tag::Backwards & ) const
// assert  this->dim() == d
// for a Fuzzy mesh, print error message (no order)
{	if ( this->is_positive() )
		return  Mesh::Iterator ( tag::whose_core_is, this->core->iterator
			( tag::over_cells_of_dim, d, tag::orientation_opposed_to_mesh,
			  tag::backwards, tag::this_mesh_is_positive                  ) );
	// else : negative mesh, back to normal order
	return  Mesh::Iterator ( tag::whose_core_is, this->core->iterator
		( tag::over_cells_of_dim, d, tag::orientation_compatible_with_mesh,
		  tag::require_order, tag::this_mesh_is_positive                   ) );  }


//------------------------------------------------------------------------------------------------------//

//      ITERATORS OVER CELLS OF MAXIMUM DIMENSION

//------------------------------------------------------------------------------------------------------//
		

inline Mesh::Iterator Mesh::iterator ( const tag::OverCellsOfMaxDim &, const tag::AsFound & ) const
// return oriented cells
// it does not matter whether  this->is_positive()  or not
{	return  Mesh::Iterator ( tag::whose_core_is, this->core->iterator
		( tag::over_cells_of_max_dim, tag::as_found, tag::this_mesh_is_positive ) );  }


inline Mesh::Iterator Mesh::iterator
( const tag::OverCellsOfMaxDim &, const tag::ForcePositive & ) const
// it does not matter whether  this->is_positive()  or not
{	return  Mesh::Iterator ( tag::whose_core_is, this->core->iterator
		( tag::over_cells_of_max_dim, tag::force_positive, tag::this_mesh_is_positive ) );  }


inline Mesh::Iterator Mesh::iterator
( const tag::OverCellsOfMaxDim &, const tag::OrientCompWithMesh & ) const
// return oriented cells
{	if ( this->is_positive() )
		return  Mesh::Iterator ( tag::whose_core_is, this->core->iterator
			( tag::over_cells_of_max_dim, tag::orientation_compatible_with_mesh,
			  tag::this_mesh_is_positive                                        ) );
	// else : negative mesh
	return  Mesh::Iterator ( tag::whose_core_is, this->core->iterator
		( tag::over_cells_of_max_dim, tag::orientation_opposed_to_mesh, tag::this_mesh_is_positive ) );  }


inline Mesh::Iterator Mesh::iterator
( const tag::OverCellsOfMaxDim &, const tag::OrientOpposToMesh & ) const
// return oriented cells
{	if ( this->is_positive() )
		return  Mesh::Iterator ( tag::whose_core_is, this->core->iterator
			( tag::over_cells_of_max_dim, tag::orientation_opposed_to_mesh,
			  tag::this_mesh_is_positive                                   ) );
	// else : negative mesh
	return  Mesh::Iterator ( tag::whose_core_is, this->core->iterator
		( tag::over_cells_of_max_dim, tag::orientation_compatible_with_mesh,
		  tag::this_mesh_is_positive                                        ) );  }


inline Mesh::Iterator Mesh::iterator
( const tag::OverCellsOfMaxDim &, const tag::AsFound &, const tag::RequireOrder & ) const
// for a Fuzzy mesh, print error message (no order)
{	if ( this->is_positive() )
		return  Mesh::Iterator ( tag::whose_core_is, this->core->iterator
			( tag::over_cells_of_max_dim, tag::as_found, tag::require_order,
			  tag::this_mesh_is_positive                                    ) );
	// else : negative mesh
	return  Mesh::Iterator ( tag::whose_core_is, this->core->iterator
		( tag::over_cells_of_max_dim, tag::as_found, tag::backwards, tag::this_mesh_is_positive ) );  }


inline Mesh::Iterator Mesh::iterator
( const tag::OverCellsOfMaxDim &, const tag::ForcePositive &, const tag::RequireOrder & ) const
// for a Fuzzy mesh, print error message (no order)
{	if ( this->is_positive() )
		return  Mesh::Iterator ( tag::whose_core_is, this->core->iterator
			( tag::over_cells_of_max_dim, tag::force_positive,
			  tag::require_order, tag::this_mesh_is_positive  )            );
	// else : negative mesh
	return  Mesh::Iterator ( tag::whose_core_is, this->core->iterator
		( tag::over_cells_of_max_dim, tag::force_positive,
		  tag::backwards, tag::this_mesh_is_positive      )            );     }


inline Mesh::Iterator Mesh::iterator
( const tag::OverCellsOfMaxDim &, const tag::OrientCompWithMesh &, const tag::RequireOrder & ) const
// for a Fuzzy mesh, print error message (no order)
{	if ( this->is_positive() )
		return  Mesh::Iterator ( tag::whose_core_is, this->core->iterator
			( tag::over_cells_of_max_dim, tag::orientation_compatible_with_mesh,
			  tag::require_order, tag::this_mesh_is_positive                    ) );
	// else : negative mesh
	return  Mesh::Iterator ( tag::whose_core_is, this->core->iterator
		( tag::over_cells_of_max_dim, tag::orientation_opposed_to_mesh,
		  tag::backwards, tag::this_mesh_is_positive                   ) );          }


inline Mesh::Iterator Mesh::iterator
( const tag::OverCellsOfMaxDim &, const tag::OrientOpposToMesh &, const tag::RequireOrder & ) const
// assert  this->dim() == d
// for a Fuzzy mesh, print error message (no order)
{	if ( this->is_positive() )
		return  Mesh::Iterator ( tag::whose_core_is, this->core->iterator
			( tag::over_cells_of_max_dim, tag::orientation_opposed_to_mesh,
			  tag::require_order, tag::this_mesh_is_positive               ) );
	// else : negative mesh
	return  Mesh::Iterator ( tag::whose_core_is, this->core->iterator
		( tag::over_cells_of_max_dim, tag::orientation_compatible_with_mesh,
		  tag::backwards, tag::this_mesh_is_positive                        ) );  }


inline Mesh::Iterator Mesh::iterator
( const tag::OverCellsOfMaxDim &, const tag::AsFound &, const tag::Backwards & ) const
// for a Fuzzy mesh, print error message (no order)
{	if ( this->is_positive() )
		return  Mesh::Iterator ( tag::whose_core_is, this->core->iterator
			( tag::over_cells_of_max_dim, tag::as_found, tag::backwards, tag::this_mesh_is_positive ) );
	// else : negative mesh, back to normal order
	return  Mesh::Iterator ( tag::whose_core_is, this->core->iterator
	  ( tag::over_cells_of_max_dim, tag::as_found, tag::require_order, tag::this_mesh_is_positive ) );    }


inline Mesh::Iterator Mesh:: iterator
( const tag::OverCellsOfMaxDim &, const tag::ForcePositive &, const tag::Backwards & ) const
// for a Fuzzy mesh, print error message (no order)
{ if ( this->is_positive() )
		return  Mesh::Iterator ( tag::whose_core_is, this->core->iterator
			( tag::over_cells_of_max_dim, tag::force_positive,
			  tag::backwards, tag::this_mesh_is_positive      )            );
	// else : negative mesh, back to normal order
	return  Mesh::Iterator ( tag::whose_core_is, this->core->iterator
		( tag::over_cells_of_max_dim, tag::force_positive,
		  tag::require_order, tag::this_mesh_is_positive  )            );     }


inline Mesh::Iterator Mesh::iterator
( const tag::OverCellsOfMaxDim &, const tag::OrientCompWithMesh &, const tag::Backwards & ) const
// for a Fuzzy mesh, print error message (no order)
{	if ( this->is_positive() )
		return  Mesh::Iterator ( tag::whose_core_is, this->core->iterator
			( tag::over_cells_of_max_dim, tag::orientation_compatible_with_mesh,
			  tag::backwards, tag::this_mesh_is_positive                        ) );
	// else : negative mesh, back to normal order
	return  Mesh::Iterator ( tag::whose_core_is, this->core->iterator
		( tag::over_cells_of_max_dim, tag::orientation_opposed_to_mesh,
		  tag::require_order, tag::this_mesh_is_positive               ) );          }


inline Mesh::Iterator Mesh::iterator
( const tag::OverCellsOfMaxDim &, const tag::OrientOpposToMesh &, const tag::Backwards & ) const
// for a Fuzzy mesh, print error message (no order)
{	if ( this->is_positive() )
		return  Mesh::Iterator ( tag::whose_core_is, this->core->iterator
			( tag::over_cells_of_max_dim, tag::orientation_opposed_to_mesh,
			  tag::backwards, tag::this_mesh_is_positive                   ) );
	// else : negative mesh, back to normal order
	return  Mesh::Iterator ( tag::whose_core_is, this->core->iterator
		( tag::over_cells_of_max_dim, tag::orientation_compatible_with_mesh,
		  tag::require_order, tag::this_mesh_is_positive                    ) );  }


//------------------------------------------------------------------------------------------------------//

//      ITERATORS OVER CELLS AROUND A GIVEN CELL

//------------------------------------------------------------------------------------------------------//


inline Mesh::Iterator Mesh::iterator
( const tag::OverCellsOfDim &, const size_t d, const tag::OrientCompWithCenter &,
  const tag::Around &, const Cell & cll                                          ) const

{	assert ( d >= 1 );
	assert ( d <= this->dim() );
	assert ( d == cll .dim() + 1 );
	// it does not matter whether  this->is_positive()  or not
	if ( d == 1 )
		return  this->iterator ( tag::over_segments, tag::pointing_towards, cll );
	// else : d >= 2
	assert ( false );  // iterator below to implement
	return  Mesh::Iterator ( tag::whose_core_is, this->core->iterator
		( tag::over_cells_of_dim, d, tag::orientation_compatible_with_center,
		  tag::around, cll .core, tag::this_mesh_is_positive                 ) );  }


inline Mesh::Iterator Mesh::iterator
( const tag::OverCellsOfDim &, const size_t d, const tag::OrientOpposToCenter &,
  const tag::Around &, const Cell & cll                                         ) const

{	assert ( d >= 1 );
	assert ( d <= this->dim() );
	assert ( d == cll .dim() + 1 );
	// it does not matter whether  this->is_positive()  or not
	if ( d == 1 )
		return  this->iterator ( tag::over_segments, tag::pointing_away_from, cll );
	// else : d >= 2
	assert ( false );  // iterator below to implement
	// return  Mesh::Iterator ( tag::whose_core_is, this->core->iterator
	// 	( tag::over_cells_of_dim, d, tag::orientation_opposed_to_center,
	// 	  tag::around, cll .core, tag::this_mesh_is_positive ) );  }
	return  Mesh::Iterator ( tag::whose_core_is, this->core->iterator
	  ( tag::over_cells_of_dim, d, tag::as_found,
		  tag::around, cll .core, tag::this_mesh_is_positive )         );             }


inline Mesh::Iterator Mesh::iterator
( const tag::OverCellsOfDim &, const size_t d, const tag::OrientCompWithMesh &,
  const tag::Around &, const Cell & cll                                        ) const

{	assert ( d >= 1 );
	assert ( d == this->dim() );
	return  this->iterator ( tag::over_cells_of_max_dim,
	                         tag::orientation_compatible_with_mesh, tag::around, cll );  }


inline Mesh::Iterator Mesh::iterator
( const tag::OverCellsOfDim &, const size_t d, const tag::OrientOpposToMesh &,
  const tag::Around &, const Cell & cll                                       ) const

{	assert ( d >= 1 );
	assert ( d == this->dim() );
	return  this->iterator ( tag::over_cells_of_max_dim,
	                         tag::orientation_opposed_to_mesh, tag::around, cll );  }


inline Mesh::Iterator Mesh::iterator
( const tag::OverCellsOfDim &, const size_t d, const tag::AsFound &,
  const tag::Around &, const Cell & cll                             ) const

{	assert ( d >= 1 );
	assert ( d <= this->dim() );
	// it does not matter whether  this->is_positive()  or not
	if ( d == 1 )
		return  this->iterator ( tag::over_segments, tag::as_found, tag::around, cll );
	// else : d >= 2
	return  Mesh::Iterator ( tag::whose_core_is, this->core->iterator
		( tag::over_cells_of_dim, d, tag::as_found,
		  tag::around, cll .core, tag::this_mesh_is_positive )         );                }


inline Mesh::Iterator Mesh::iterator
( const tag::OverCellsOfDim &, const size_t d, const tag::Around &, const Cell & cll ) const

{	assert ( d >= 1 );
	assert ( d <= this->dim() );
	if ( d == this->dim() )
		return  this->iterator ( tag::over_cells_of_max_dim,
		                         tag::orientation_compatible_with_mesh, tag::around, cll );
	// else : d < this->dim()
	return  this->iterator ( tag::over_cells_of_dim, d, tag::as_found, tag::around, cll );  }


inline Mesh::Iterator Mesh::iterator
( const tag::OverCellsOfDim &, const size_t d, const tag::ForcePositive &,
  const tag::Around &, const Cell & cll                                   ) const

{	assert ( d >= 1 );
	assert ( d <= this->dim() );
	// it does not matter whether  this->is_positive()  or not
	assert ( false );  // add tag::force_positive below
	if ( d == 1 )
		return  this->iterator
			( tag::over_segments, tag::as_found, tag::around, cll );
	// else : d >= 2
	return  Mesh::Iterator ( tag::whose_core_is, this->core->iterator
		( tag::over_cells_of_dim, d, tag::force_positive,
		  tag::around, cll .core, tag::this_mesh_is_positive )         );  }


inline Mesh::Iterator Mesh::iterator
( const tag::OverCellsOfDim &, const size_t d, const tag::OrientCompWithCenter &,
  const tag::Around &, const Cell & cll, const tag::RequireOrder &               ) const

{	std::cout << __FILE__ << ":" << __LINE__ << ": "
	          << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "not yet implemented" << std::endl;
	exit (1);                                                }


inline Mesh::Iterator Mesh::iterator
( const tag::OverCellsOfDim &, const size_t d, const tag::OrientOpposToCenter &,
  const tag::Around &, const Cell & cll, const tag::RequireOrder &              ) const

{	std::cout << __FILE__ << ":" << __LINE__ << ": "
	          << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "not yet implemented" << std::endl;
	exit (1);                                                }


inline Mesh::Iterator Mesh::iterator
( const tag::OverCellsOfDim &, const size_t d, const tag::OrientCompWithMesh &,
  const tag::Around &, const Cell & cll, const tag::RequireOrder &             ) const

{	std::cout << __FILE__ << ":" << __LINE__ << ": "
	          << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "not yet implemented" << std::endl;
	exit (1);                                                }


inline Mesh::Iterator Mesh::iterator
( const tag::OverCellsOfDim &, const size_t d, const tag::AsFound &,
  const tag::Around &, const Cell & cll, const tag::RequireOrder &  ) const

{	assert ( false );  // add require_order below
	assert ( d >= 1 );
	assert ( d <= this->dim() );
	assert ( this->is_positive() );  // deal with negative meshes !
	assert ( cll .dim() + 2 == this->dim() );  // co-dimension two
	if ( d == 1 )
		return  this->iterator ( tag::over_segments, tag::as_found, tag::around, cll );
	// else : d >= 2
	return  Mesh::Iterator ( tag::whose_core_is, this->core->iterator
		( tag::over_cells_of_dim, d, tag::as_found,
		  tag::around, cll .core, tag::this_mesh_is_positive )         );                }

	
inline Mesh::Iterator Mesh::iterator   // AsFound if dim < dim mesh, otherwise CompatWithMesh
( const tag::OverCellsOfDim &, const size_t d, const tag::Around &, const Cell & cll,
  const tag::RequireOrder &                                                          ) const
	
{	assert ( d >= 1 );
	assert ( d <= this->dim() );
	assert ( this->is_positive() );  // deal with negative meshes !
	assert ( cll .dim() + 2 == this->dim() );  // co-dimension two
	if ( d == this->dim() )
		return  this->iterator ( tag::over_cells_of_max_dim, tag::orientation_compatible_with_mesh,
		                         tag::around, cll, tag::require_order                              );
	// else
	return  this->iterator ( tag::over_cells_of_dim, d, tag::as_found,
	                         tag::around, cll, tag::require_order     );                              }

	
inline Mesh::Iterator Mesh::iterator
( const tag::OverCellsOfDim &, const size_t d, const tag::ForcePositive &,
  const tag::Around &, const Cell & cll, const tag::RequireOrder &        ) const

{	std::cout << __FILE__ << ":" << __LINE__ << ": "
	          << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "not yet implemented" << std::endl;
	exit (1);                                                }


inline Mesh::Iterator Mesh::iterator  // AsFound if dim < dim mesh, otherwise CompatWithMesh
( const tag::OverCellsOfDim &, const size_t d, const tag::Around &, const Cell & cll,
  const tag::Backwards &                                                             ) const

{	assert ( d >= 1 );
	assert ( d <= this->dim() );
	assert ( this->is_positive() );  // deal with negative meshes !
	if ( d == this->dim() )
		return  this->iterator ( tag::over_cells_of_max_dim, tag::orientation_compatible_with_mesh,
		                         tag::around, cll, tag::backwards                                  );
	// else
	return  this->iterator ( tag::over_cells_of_dim, d, tag::as_found,
	                         tag::around, cll, tag::backwards         );                              }


inline Mesh::Iterator Mesh::iterator
( const tag::OverCellsOfDim &, const size_t d, const tag::OrientCompWithCenter &,
  const tag::Around &, const Cell & cll, const tag::Backwards &                  ) const

{	std::cout << __FILE__ << ":" << __LINE__ << ": "
	          << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "not yet implemented" << std::endl;
	exit (1);                                                }


inline Mesh::Iterator Mesh::iterator
( const tag::OverCellsOfDim &, const size_t d, const tag::OrientOpposToCenter &,
  const tag::Around &, const Cell & cll, const tag::Backwards &                 ) const

{	std::cout << __FILE__ << ":" << __LINE__ << ": "
	          << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "not yet implemented" << std::endl;
	exit (1);                                                }


inline Mesh::Iterator Mesh::iterator
( const tag::OverCellsOfDim &, const size_t d, const tag::OrientCompWithMesh &,
  const tag::Around &, const Cell & cll, const tag::Backwards &                ) const

{	std::cout << __FILE__ << ":" << __LINE__ << ": "
	          << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "not yet implemented" << std::endl;
	exit (1);                                                }


inline Mesh::Iterator Mesh::iterator
( const tag::OverCellsOfDim &, const size_t d, const tag::AsFound &,
  const tag::Around &, const Cell & cll, const tag::Backwards &     ) const

{	std::cout << __FILE__ << ":" << __LINE__ << ": "
	          << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "not yet implemented" << std::endl;
	exit (1);                                                }


inline Mesh::Iterator Mesh::iterator
( const tag::OverCellsOfDim &, const size_t d, const tag::ForcePositive &,
  const tag::Around &, const Cell & cll, const tag::Backwards &           ) const

{	std::cout << __FILE__ << ":" << __LINE__ << ": "
	          << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "not yet implemented" << std::endl;
	exit (1);                                                }


inline Mesh::Iterator Mesh::iterator
( const tag::OverCellsOfMaxDim &, const tag::OrientCompWithCenter &,
  const tag::Around &, const Cell & cll                             ) const

{	assert ( this->is_positive() );
	return  Mesh::Iterator ( tag::whose_core_is, this->core->iterator
		( tag::over_cells_of_max_dim, tag::orientation_compatible_with_center,
		  tag::around, cll .core, tag::this_mesh_is_positive                  ) );   }


inline Mesh::Iterator Mesh::iterator
( const tag::OverCellsOfMaxDim &, const tag::OrientOpposToCenter &,
  const tag::Around &, const Cell & cll                            ) const

{	assert ( this->is_positive() );
	return  Mesh::Iterator ( tag::whose_core_is, this->core->iterator
		( tag::over_cells_of_max_dim, tag::orientation_opposed_to_center,
		  tag::around, cll .core, tag::this_mesh_is_positive             ) );   }


inline Mesh::Iterator Mesh::iterator
( const tag::OverCellsOfMaxDim &, const tag::OrientCompWithMesh &,
  const tag::Around &, const Cell & cll                           ) const

{	if ( this->is_positive() )
		return  Mesh::Iterator ( tag::whose_core_is, this->core->iterator
			( tag::over_cells_of_max_dim, tag::orientation_compatible_with_mesh,
			  tag::around, cll .get_positive () .core, tag::this_mesh_is_positive ) );
	// else : 'this' mesh is negative
	return  Mesh::Iterator ( tag::whose_core_is, this->core->iterator
		( tag::over_cells_of_max_dim, tag::orientation_opposed_to_mesh,
		  tag::around, cll .get_positive () .core, tag::this_mesh_is_positive ) );   }


inline Mesh::Iterator Mesh::iterator
( const tag::OverCellsOfMaxDim &, const tag::OrientOpposToMesh &,
  const tag::Around &, const Cell & cll                          ) const

{	if ( this->is_positive() )
		return  Mesh::Iterator ( tag::whose_core_is, this->core->iterator
			( tag::over_cells_of_max_dim, tag::orientation_opposed_to_mesh,
			  tag::around, cll .get_positive () .core, tag::this_mesh_is_positive ) );
	// else : 'this' mesh is negative
	return  Mesh::Iterator ( tag::whose_core_is, this->core->iterator
		( tag::over_cells_of_max_dim, tag::orientation_compatible_with_mesh,
		  tag::around, cll .get_positive () .core, tag::this_mesh_is_positive ) );     }


inline Mesh::Iterator Mesh::iterator
( const tag::OverCellsOfMaxDim &, const tag::AsFound &, const tag::Around &, const Cell & cll ) const

{	return  Mesh::Iterator ( tag::whose_core_is, this->core->iterator
		( tag::over_cells_of_max_dim, tag::as_found,
		  tag::around, cll .get_positive () .core, tag::this_mesh_is_positive ) );  }


inline Mesh::Iterator Mesh::iterator
( const tag::OverCellsOfMaxDim &, const tag::OrientCompWithCenter &,
  const tag::Around &, const Cell & cll, const tag::RequireOrder & ) const
{	std::cout << __FILE__ << ":" << __LINE__ << ": "
	          << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "not yet implemented" << std::endl;
	exit (1);                                                }


inline Mesh::Iterator Mesh::iterator
( const tag::OverCellsOfMaxDim &, const tag::OrientOpposToCenter &,
  const tag::Around &, const Cell & cll, const tag::RequireOrder & ) const
{	std::cout << __FILE__ << ":" << __LINE__ << ": "
	          << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "not yet implemented" << std::endl;
	exit (1);                                                }


inline Mesh::Iterator Mesh::iterator
( const tag::OverCellsOfMaxDim &, const tag::OrientCompWithMesh &,
  const tag::Around &, const Cell & cll, const tag::RequireOrder & ) const

{	assert ( cll .dim() + 2 == this->dim() );  // co-dimension two
	if ( this->is_positive() )
		return  Mesh::Iterator ( tag::whose_core_is, this->core->iterator
			( tag::over_cells_of_max_dim, tag::orientation_compatible_with_mesh,
			  tag::around, cll .core, tag::require_order, tag::this_mesh_is_positive ) );
	// else
	return  this->iterator ( tag::over_cells_of_max_dim, tag::orientation_opposed_to_mesh,
	                         tag::around, cll, tag::backwards                             );  }


inline Mesh::Iterator Mesh::iterator
( const tag::OverCellsOfMaxDim &, const tag::OrientOpposToMesh &,
  const tag::Around &, const Cell & cll, const tag::RequireOrder & ) const
{	std::cout << __FILE__ << ":" << __LINE__ << ": "
	          << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "not yet implemented" << std::endl;
	exit (1);                                                }


inline Mesh::Iterator Mesh::iterator
( const tag::OverCellsOfMaxDim &, const tag::ForcePositive &,
  const tag::Around &, const Cell & cll, const tag::RequireOrder & ) const
{	std::cout << __FILE__ << ":" << __LINE__ << ": "
	          << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "not yet implemented" << std::endl;
	exit (1);                                                }


inline Mesh::Iterator Mesh::iterator
( const tag::OverCellsOfMaxDim &, const tag::OrientCompWithCenter &,
  const tag::Around &, const Cell & cll, const tag::Backwards & ) const
{	std::cout << __FILE__ << ":" << __LINE__ << ": "
	          << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "not yet implemented" << std::endl;
	exit (1);                                                }


inline Mesh::Iterator Mesh::iterator
( const tag::OverCellsOfMaxDim &, const tag::OrientOpposToCenter &,
  const tag::Around &, const Cell & cll, const tag::Backwards & ) const
{	std::cout << __FILE__ << ":" << __LINE__ << ": "
	          << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "not yet implemented" << std::endl;
	exit (1);                                                }


inline Mesh::Iterator Mesh::iterator
( const tag::OverCellsOfMaxDim &, const tag::OrientCompWithMesh &,
  const tag::Around &, const Cell & cll, const tag::Backwards & ) const

{	assert ( cll .dim() + 2 == this->dim() );  // co-dimension two
	if ( this->is_positive() )
		return  Mesh::Iterator ( tag::whose_core_is, this->core->iterator
			( tag::over_cells_of_max_dim, tag::orientation_compatible_with_mesh,
			  tag::around, cll .core, tag::backwards, tag::this_mesh_is_positive ) );
	// else
	return  this->iterator ( tag::over_cells_of_max_dim, tag::orientation_opposed_to_mesh,
	                         tag::around, cll, tag::require_order                         );  }


inline Mesh::Iterator Mesh::iterator
( const tag::OverCellsOfMaxDim &, const tag::OrientOpposToMesh &,
  const tag::Around &, const Cell & cll, const tag::Backwards &  ) const
{	std::cout << __FILE__ << ":" << __LINE__ << ": "
	          << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "not yet implemented" << std::endl;
	exit (1);                                                }


inline Mesh::Iterator Mesh::iterator
( const tag::OverCellsOfMaxDim &, const tag::ForcePositive &,
  const tag::Around &, const Cell & cll, const tag::Backwards & ) const
{	std::cout << __FILE__ << ":" << __LINE__ << ": "
	          << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "not yet implemented" << std::endl;
	exit (1);                                                }


inline Mesh::Iterator Mesh::iterator
( const tag::OverSegments &, const tag::AsFound &, const tag::Around &, const Cell & V ) const

{	Cell::Positive::Vertex * VV = tag::Util::assert_cast
		< Cell::Core *, Cell::Positive::Vertex * > ( V .core );
	return  Mesh::Iterator ( tag::whose_core_is, this->core->iterator
		( tag::over_segments, tag::as_found, tag::around, VV, tag::this_mesh_is_positive ) );  }
	

inline Mesh::Iterator Mesh::iterator
( const tag::OverSegments &, const tag::AsFound &,
  const tag::Around &, const Cell & cll, const tag::RequireOrder & ) const
{	std::cout << __FILE__ << ":" << __LINE__ << ": "
	          << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "not yet implemented" << std::endl;
	exit (1);                                                }


inline Mesh::Iterator Mesh::iterator
( const tag::OverSegments &, const tag::AsFound &,
  const tag::Around &, const Cell & cll, const tag::Backwards & ) const
{	std::cout << __FILE__ << ":" << __LINE__ << ": "
	          << __extension__ __PRETTY_FUNCTION__ << ": ";
	std::cout << "not yet implemented" << std::endl;
	exit (1);                                                }


inline Mesh::Iterator Mesh::iterator
( const tag::OverVertices &, const tag::ConnectedTo &, const Cell & V ) const

{	Cell::Positive::Vertex * VV = tag::Util::assert_cast
		< Cell::Core *, Cell::Positive::Vertex * > ( V .core );
	// it does not matter whether  this->is_positive()  or not
	return  Mesh::Iterator ( tag::whose_core_is, this->core->iterator
		( tag::over_vertices, tag::connected_to, VV, tag::this_mesh_is_positive ) );  }


inline Mesh::Iterator Mesh::iterator
( const tag::OverVertices &, const tag::ConnectedTo &, const Cell & V, const tag::RequireOrder & ) const

{	Cell::Positive::Vertex * VV = tag::Util::assert_cast
		< Cell::Core *, Cell::Positive::Vertex * > ( V .core );
	// if 'this' is negative, reverse order !
	return  Mesh::Iterator ( tag::whose_core_is, this->core->iterator
		( tag::over_vertices, tag::connected_to, VV,
		  tag::require_order, tag::this_mesh_is_positive )             );  }


inline Mesh::Iterator Mesh::iterator
( const tag::OverSegments &, const tag::PointingTowards &, const Cell & V ) const

{	Cell::Positive::Vertex * VV = tag::Util::assert_cast
		< Cell::Core *, Cell::Positive::Vertex * > ( V .core );
	// it does not matter whether  this->is_positive()  or not
	return  Mesh::Iterator ( tag::whose_core_is, this->core->iterator
		( tag::over_segments, tag::whose_tip_is, VV, tag::this_mesh_is_positive )  );  }


inline Mesh::Iterator Mesh::iterator
( const tag::OverSegments &, const tag::PointingTowards &, const Cell & V,
  const tag::RequireOrder &                                               ) const

{	assert ( this->dim() == 2 );
	Cell::Positive::Vertex * VV = tag::Util::assert_cast
		< Cell::Core *, Cell::Positive::Vertex * > ( V .core );
	if ( this->is_positive() )
		return  Mesh::Iterator ( tag::whose_core_is, this->core->iterator
			( tag::over_segments, tag::whose_tip_is, VV,
			  tag::require_order, tag::this_mesh_is_positive )             );
	// else : negative mesh, reverse order
	return  Mesh::Iterator ( tag::whose_core_is, this->core->iterator
		( tag::over_segments, tag::whose_tip_is, VV,
		  tag::backwards, tag::this_mesh_is_positive )                 );     }


inline Mesh::Iterator Mesh::iterator
( const tag::OverSegments &, const tag::PointingTowards &, const Cell & V,
  const tag::Backwards &                                                  ) const

{	assert ( this->dim() == 2 );
	Cell::Positive::Vertex * VV = tag::Util::assert_cast
		< Cell::Core *, Cell::Positive::Vertex * > ( V .core );
	if ( this->is_positive() )
		return  Mesh::Iterator ( tag::whose_core_is, this->core->iterator
			( tag::over_segments, tag::whose_tip_is, VV,
			  tag::backwards, tag::this_mesh_is_positive )                 );
	// else : negative mesh, reverse order once more
	return  Mesh::Iterator ( tag::whose_core_is, this->core->iterator
		( tag::over_segments, tag::whose_tip_is, VV,
		  tag::require_order, tag::this_mesh_is_positive )             );     }


inline Mesh::Iterator Mesh::iterator
( const tag::OverSegments &, const tag::PointingAwayFrom &, const Cell & V ) const

{	Cell::Positive::Vertex * VV = tag::Util::assert_cast
		< Cell::Core *, Cell::Positive::Vertex * > ( V .core );
	// it does not matter whether  this->is_positive()  or not
	return  Mesh::Iterator ( tag::whose_core_is, this->core->iterator
		( tag::over_segments, tag::whose_base_is, VV, tag::this_mesh_is_positive ) );  }


inline Mesh::Iterator Mesh::iterator
( const tag::OverSegments &, const tag::PointingAwayFrom &, const Cell & V,
  const tag::RequireOrder &                                                ) const

{	assert ( this->dim() == 2 );
	Cell::Positive::Vertex * VV = tag::Util::assert_cast
		< Cell::Core *, Cell::Positive::Vertex * > ( V .core );
	if ( this->is_positive() )
		return  Mesh::Iterator ( tag::whose_core_is, this->core->iterator
			( tag::over_segments, tag::whose_base_is, VV,
			  tag::require_order, tag::this_mesh_is_positive )             );
	// else : negative mesh, reverse order
	return  Mesh::Iterator ( tag::whose_core_is, this->core->iterator
		( tag::over_segments, tag::whose_base_is, VV,
		  tag::backwards, tag::this_mesh_is_positive )                 );     }


inline Mesh::Iterator Mesh::iterator
( const tag::OverSegments &, const tag::PointingAwayFrom &, const Cell & V,
  const tag::Backwards &                                                   ) const

{	assert ( this->dim() == 2 );
	Cell::Positive::Vertex * VV = tag::Util::assert_cast
		< Cell::Core *, Cell::Positive::Vertex * > ( V .core );
	if ( this->is_positive() )
		return  Mesh::Iterator ( tag::whose_core_is, this->core->iterator
			( tag::over_segments, tag::whose_base_is, VV,
			  tag::backwards, tag::this_mesh_is_positive )                 );
	// else : negative mesh, reverse order once more
	return  Mesh::Iterator ( tag::whose_core_is, this->core->iterator
		( tag::over_segments, tag::whose_base_is, VV,
		  tag::require_order, tag::this_mesh_is_positive )             );     }


//------------------------------------------------------------------------------------------------------//


inline Cell::Iterator Cell::iterator
( const tag::OverMeshesAbove &, const tag::SameDim &,
  const tag::OrientCompWithCell &, const tag::DoNotBuildCells & ) const

{	assert ( this->exists() );
	if ( this->is_positive() )
		return  Cell::Iterator ( tag::whose_core_is, this->core->iterator_1
			( tag::over_meshes_above, tag::of_same_dim, tag::orientation_compatible_with_cell,
			  tag::this_cell_is_positive, tag::do_not_build_cells                             ) );
	// else : negative cell, reverse orientation
	assert ( this->core->reverse_attr .exists() );
	return  Cell::Iterator ( tag::whose_core_is, this->core->reverse_attr .core->iterator_2
		( tag::over_meshes_above, tag::of_same_dim, tag::orientation_opposed_to_cell,
		  tag::this_cell_is_positive, tag::do_not_build_cells                        )       );    }


inline Cell::Iterator Cell::iterator
( const tag::OverMeshesAbove &, const tag::SameDim &,
  const tag::OrientOpposToCell &, const tag::DoNotBuildCells & ) const

{	assert ( this->exists() );
	if ( this->is_positive() )
		return  Cell::Iterator ( tag::whose_core_is, this->core->iterator_2
			( tag::over_meshes_above, tag::of_same_dim, tag::orientation_opposed_to_cell,
			  tag::this_cell_is_positive, tag::do_not_build_cells                        ) );
	// else : negative cell, reverse orientation again
	assert ( this->core->reverse_attr .exists() );
	return  Cell::Iterator ( tag::whose_core_is, this->core->reverse_attr .core->iterator_1
		( tag::over_meshes_above, tag::of_same_dim, tag::orientation_compatible_with_cell,
		  tag::this_cell_is_positive, tag::do_not_build_cells                             )  );  }


inline Cell::Iterator Cell::iterator
( const tag::OverMeshesAbove &, const tag::OfDimension &, const size_t d,
  const tag::OrientationIrrelevant &                                     ) const

{	assert ( this->exists() );
	return  Cell::Iterator
		( tag::whose_core_is, this->core->get_positive() .core->iterator_3
			( tag::over_meshes_above, tag::of_dim, d, tag::orientation_irrelevant,
			  tag::this_cell_is_positive                                          ) );  }

//------------------------------------------------------------------------------------------------------//


}  // end of  namespace maniFEM

#endif  // ifndef MANIFEM_ITERATOR_H
