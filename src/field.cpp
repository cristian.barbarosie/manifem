
//   field.cpp  2024.10.04

//   This file is part of maniFEM, a C++ library for meshes and finite elements on manifolds.

//   Copyright  2019 - 2024  Cristian Barbarosie  cristian.barbarosie@gmail.com

//   https://maniFEM.rd.ciencias.ulisboa.pt/
//   https://codeberg.org/cristian.barbarosie/maniFEM

//   ManiFEM is free software: you can redistribute it and/or modify it
//   under the terms of the GNU Lesser General Public License as published
//   by the Free Software Foundation, either version 3 of the License
//   or (at your option) any later version.

//   ManiFEM is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//   See the GNU Lesser General Public License for more details.

//   You should have received a copy of the GNU Lesser General Public License
//   along with maniFEM.  If not, see <https://www.gnu.org/licenses/>.


#include "mesh.h"
#include "iterator.h"
#include "field.h"
#include "function.h"


using namespace maniFEM;

//------------------------------------------------------------------------------------------------------//

tag::Util::Field::Core::~Core ( )  { }  // virtual

tag::Util::Field::ShortInt::~ShortInt ( )  { }  // virtual from tag::Util::Field::Core

tag::Util::Field::SizeT::~SizeT ( )  { }  // virtual from tag::Util::Field::Core

tag::Util::Field::Double::Base::~Base ( )  { }  // virtual from tag::Util::Field::Core

tag::Util::Field::Double::Scalar::~Scalar ( )  { }  // virtual from tag::Util::Field::Core

tag::Util::Field::Double::Block::~Block ( )  { }  // virtual from tag::Util::Field::Core

//------------------------------------------------------------------------------------------------------//


size_t tag::Util::Field::ShortInt::number_of ( const tag::Components & )
// virtual from tag::Util::Field::Core
{	return 1;  }

size_t tag::Util::Field::Double::Scalar::number_of ( const tag::Components & )
// virtual from tag::Util::Field::Core
{	return 1;  }

size_t tag::Util::Field::SizeT::number_of ( const tag::Components & )
// virtual from tag::Util::Field::Core
{	return 1;  }


size_t tag::Util::Field::Double::Block::number_of ( const tag::Components & )
// virtual from tag::Util::Field::Core
{	return tag::Util::assert_diff ( this->max_index_p1, this->min_index );  }
// tag::Util::assert_diff  provides a safe way to substract two size_t numbers


tag::Util::Field::Double::Scalar * tag::Util::Field::Double::Scalar::component ( size_t i )
// virtual from tag::Util::Field::Core
{	assert ( i == 0 );
	return this;        }


tag::Util::Field::Double::Scalar * tag::Util::Field::Double::Block::component ( size_t i )
// virtual from tag::Util::Field::Core
// we build a new tag::Util::Field::Scalar
// it would be nice to check whether a field for index i exists already ...
{	assert ( i < this->number_of ( tag::components ) );
	assert ( this->lives_on_pos_cells_of_dim .size() == 1 );
	assert ( this->lives_on_neg_cells_of_dim .size() == 0 );
	size_t d = * this->lives_on_pos_cells_of_dim .begin();
	return new tag::Util::Field::Double::Scalar
		( tag::lives_on_positive_cells, tag::of_dim, d, tag::has_index_in_heap, this->min_index + i );  }

//------------------------------------------------------------------------------------------------------//
//------------------------------------------------------------------------------------------------------//


Cell::Numbering::Field::~Field ( )  // virtual from Cell::Numbering::Core
{	}


bool Cell::Numbering::Field::exists ( )  // virtual from Cell::Numbering::Core
{  return  true;  }


size_t Cell::Numbering::Field::size ( )  // virtual from Cell::Numbering::Core
{	return  this->counter;  }


bool Cell::Numbering::Field::has_cell ( const Cell cll )  // virtual from Cell::Numbering::Core
{	return  this->cell_dim == cll .dim();  }
// if used correctly, this numbering covers all cells of a certain dimension


size_t & Cell::Numbering::Field::index_of ( const Cell p )  // virtual from Cell::Numbering::Core
{	return  this->field->on_cell ( p .core );  }


void Cell::Numbering::Field::set_and_increment ( Cell::Core * cll, void * num )  // static
{	Cell::Numbering::Field * num_f = static_cast < Cell::Numbering::Field* > ( num );
	cll->size_t_heap [ num_f->field->index_in_heap ] = num_f->counter;
	num_f->counter++;                                                                  }
		
//------------------------------------------------------------------------------------------------------//
