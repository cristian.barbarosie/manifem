
//   var-form.cpp  2024.12.28

//   This file is part of maniFEM, a C++ library for meshes and finite elements on manifolds.

//   Copyright  2019 - 2024  Cristian Barbarosie  cristian.barbarosie@gmail.com

//   https://maniFEM.rd.ciencias.ulisboa.pt/
//   https://codeberg.org/cristian.barbarosie/maniFEM

//   ManiFEM is free software: you can redistribute it and/or modify it
//   under the terms of the GNU Lesser General Public License as published
//   by the Free Software Foundation, either version 3 of the License
//   or (at your option) any later version.

//   ManiFEM is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//   See the GNU Lesser General Public License for more details.

//   You should have received a copy of the GNU Lesser General Public License
//   along with maniFEM.  If not, see <https://www.gnu.org/licenses/>.


#ifndef MANIFEM_NO_FEM

#include "math.h"
#include <sstream>
#include <typeinfo>

#include "mesh.h"
#include "iterator.h"
#include "function.h"
#include "manifold.h"
#include "finite-elem.h"
#include "var-form.h"

using namespace maniFEM;


namespace {  // anonymous namespace, mimics static linkage


void impose_value_of_unknown  // fast  // see below for an equivalent, more readable, version
// hidden in anonymous namespace
( Eigen::SparseMatrix < double, Eigen::RowMajor > & matrix_A, Eigen::VectorXd & vector_b,
  const size_t i, const double val                                                       )

// in a system of linear equations, destroy equation 'i' and impose u(i) = val
// change also column 'i' of the matrix, just to preserve symmetry

// used for imposing Dirichlet boundary conditions

// only works if 'matrix_A' is row-major
// although in our code 'matrix_A' will be symmetric so it does not really matter

{	size_t size_matrix = matrix_A .innerSize();

	// destroy column i, even the diagonal entry, change vector_b
	for ( size_t j = 0; j < size_matrix; j++ )
	{	const double Aji = matrix_A .coeff ( j, i );
		if ( Aji == 0. )  continue;
		vector_b (j) -= Aji * val;
		matrix_A .coeffRef ( j, i ) = 0.;             }

	// destroy row i, set diagonal entry to 1.
	const size_t oi = matrix_A .outerIndexPtr() [i];     // access to matrix_A .m_outerIndex
	// std::cout << i << " " << oi << " " << matrix_A .innerNonZeroPtr() [i] << " "
	//           << matrix_A .innerIndexPtr() [ oi ] << " " << matrix_A .valuePtr() [ oi ] << std::endl;
	matrix_A .innerNonZeroPtr() [i] = 1;                 // access to matrix_A .m_innerNonZeros
	matrix_A .innerIndexPtr() [ oi ] = i;                // access to matrix_A .m_data .m_indices
	matrix_A .valuePtr() [ oi ] = 1.;                    // access to matrix_A .m_data .m_values

	vector_b (i) = val;                                 }


#ifndef OMIT_OBSOLETE_CODE

void impose_value_of_unknown_readable
// hidden in anonymous namespace
( Eigen::SparseMatrix < double > & matrix_A, Eigen::VectorXd & vector_b,
  const size_t i, const double val                                      )

// in a system of linear equations, destroy equation 'i' and impose u(i) = val
// change also column 'i' of the matrix, just to preserve symmetry

// used for imposing Dirichlet boundary conditions

{	const size_t size_matrix = matrix_A .innerSize();

	// destroy raw i, even the diagonal entry
	for ( size_t j = 0; j < size_matrix; j++ )
		if ( matrix_A .coeff ( i, j ) != 0. )
			matrix_A .coeffRef ( i, j ) = 0.;

	// destroy column i, change vector
	for ( size_t j = 0; j < size_matrix; j++ )
	{	if ( matrix_A .coeff ( j, i ) == 0. )  continue;
		double & Aji = matrix_A .coeffRef ( j, i );
		vector_b (j) -= Aji * val;
		Aji = 0.;                                         }

	// set diagonal entry to 1., change vector
	matrix_A .coeffRef ( i, i ) = 1.;
	vector_b (i) = val;                                     }

#endif  // ifndef OMIT_OBSOLETE_CODE

}  // end of anonymous namespace

//------------------------------------------------------------------------------------------------------//


Function::Form::Core::~Core ( )  {  }  // virtual

//------------------------------------------------------------------------------------------------------//


Function::Form maniFEM::operator+ ( const Function::Form & f, const Function::Form & g )

{	// if one of them is a sum, or both :
	Function::Form::Sum * f_sum = dynamic_cast < Function::Form::Sum * > ( f .core .get() );
	Function::Form::Sum * g_sum = dynamic_cast < Function::Form::Sum * > ( g .core .get() );

	std::shared_ptr < Function::Form::Sum > result =
			std::make_shared < Function::Form::Sum > ();  // empty sum
	if ( g_sum )  // g is a sum
	{	std::forward_list < Function::Form > ::iterator it_g;
		for ( it_g = g_sum->terms .begin(); it_g != g_sum->terms .end(); it_g++ )
			result->terms .push_front ( *it_g );                                    }
	else  result->terms .push_front ( g );
	if ( f_sum )  // f is a sum
	{	std::forward_list < Function::Form > ::iterator it_f;
		for ( it_f = f_sum->terms .begin(); it_f != f_sum->terms .end(); it_f++ )
			result->terms .push_front ( *it_f );                                    }
	else  result->terms .push_front ( f );

	return Function::Form ( tag::whose_core_is, result );                                     }

//------------------------------------------------------------------------------------------------------//


Function::Form Function::Form::DelayedIntegral::product ( double c )
// virtual from Function::Form::Core

{	return  Function::Form ( tag::whose_core_is,
	  std::make_shared < Function::Form::DelayedIntegral >
		( c * this->integrand, this->domain )               );  }


Function::Form Function::Form::ValueOfTest::product ( double c )
// virtual from Function::Form::Core

{	return  Function::Form ( tag::whose_core_is,
	  std::make_shared < Function::Form::ValueOfTest >
		( c * this->coeff, this->test, this->cll )      );  }


Function::Form Function::Form::Sum::product ( double c )
// virtual from Function::Form::Core

{	assert ( false );    // to implement
	// we return an empty Function::Form::Sum just to avoid compilation errors
	return  Function::Form ( tag::whose_core_is,
	  std::make_shared < Function::Form::Sum > () );  }

//------------------------------------------------------------------------------------------------------//


void VariationalFormulation::analyse_equation ( )

{	this->bilinear_form .analyse_form ( *this );
	this->linear_form .analyse_form ( *this );

	// the above analysis must have defined a set of domains
	// and also the unknown and test functions
	assert ( this->domains .size() > 0 );
	assert ( this->unknown .exists() );
	assert ( this->test    .exists() );

	// compute the maximum topological dimension of domains
	size_t dim_max = 0;
	for ( std::set < Mesh > ::const_iterator
	         it = this->domains .begin(); it != this->domains .end(); it ++ )
	{	size_t d = it->dim();
		if ( d > dim_max )  dim_max = d;  }
	assert ( dim_max > 0 );
	// count domains of maximum dimension
	size_t nb_of_domains = 0;
	std::set < Mesh > ::const_iterator kept_it = this->domains .end();
	for ( std::set < Mesh > ::const_iterator
	         it = this->domains .begin(); it != this->domains .end(); it ++ )
		if ( it->dim() == dim_max )
		{	nb_of_domains ++;  kept_it = it;  }
	assert ( nb_of_domains > 0 );
	assert ( kept_it != this->domains .end() );
	
	assert ( not this->numbering .exists() );
	this->numbering = Cell::Numbering
		( tag::whose_core_is, std::make_shared < Cell::Numbering::Map > () );
	{ // just a block of code for hiding 'numb'
	Cell::Numbering & numb = this->numbering;
	if ( nb_of_domains == 1 )
	{	Mesh::Iterator it = kept_it->iterator ( tag::over_vertices );
		size_t counter = 0;
		for ( it .reset(); it .in_range(); it ++ )
		{	Cell V = *it;  numb (V) = counter;  ++counter;  }
		assert ( counter == numb .size() );                            }
	else  // nb_of_domains > 1
	{	// there are several domains of maximum topological dimension
		// most likely, there are common vertices
		size_t counter = 0;
		for ( std::set < Mesh > ::const_iterator it = this->domains .begin();
		      it != this->domains .end(); it ++                               )
		{	Mesh dom = * it;
			if ( dom .dim() != dim_max )  continue;
			Mesh::Iterator itt = dom .iterator ( tag::over_vertices );
			for ( itt .reset(); itt .in_range(); itt ++ )
			{	Cell V = * itt;
				if ( not numb .has_cell (V) )
				{	numb (V) = counter;  ++counter;  }  }                }
		assert ( counter == numb .size() );                                      }
	} // just a block of code

}  // end of  VariationalFormulation::analyse_equation

//------------------------------------------------------------------------------------------------------//


void VariationalFormulation::assemble_matrix ( )

// also apply Dirichlet boundary conditions (they modify the matrix)
// do not deal with Neumann boundary conditions
// also prepare Eigen solver

{	assert ( this->numbering .exists() );
	Cell::Numbering & numb = this->numbering;

	size_t size_matrix = numb .size();
	this->matrix_bilin .resize ( size_matrix, size_matrix );  // sets elements to zero
	Eigen::VectorXd & this_vector_lin = * this->vector_lin;
	this_vector_lin .resize ( size_matrix );
	this_vector_lin .setZero();

	// declare finite element(s)
	std::map < std::pair < size_t, size_t >, FiniteElement > fe;  // dim, nb_of_faces -> finite elem
	
	// insert finite elements needed for the bilinear form
	this->bilinear_form .inspect_cells_of_domain_of_integration ( fe );
	
	// assert ( not ( segments and triangles ) );
	assert ( ( fe .find ( { 1, 2 } ) == fe .end() ) or ( fe .find ( { 2, 3 } ) == fe .end() ) );
	// assert ( not ( segments and quadrilaterals ) );
	assert ( ( fe .find ( { 1, 2 } ) == fe .end() ) or ( fe .find ( { 2, 4 } ) == fe .end() ) );
	// assert ( not ( segments and tetrahedra ) );
	assert ( ( fe .find ( { 1, 2 } ) == fe .end() ) or ( fe .find ( { 3, 4 } ) == fe .end() ) );
	// assert ( not ( triangles and tetrahedra ) );
	assert ( ( fe .find ( { 2, 3 } ) == fe .end() ) or ( fe .find ( { 3, 4 } ) == fe .end() ) );
	// assert ( not ( quadrilaterals and tetrahedra ) );
	assert ( ( fe .find ( { 2, 4 } ) == fe .end() ) or ( fe .find ( { 3, 4 } ) == fe .end() ) );
	// assert ( not ( segments and parallelipipeds ) );
	assert ( ( fe .find ( { 1, 2 } ) == fe .end() ) or ( fe .find ( { 3, 6 } ) == fe .end() ) );
	// assert ( not ( triangles and parallelipipeds ) );
	assert ( ( fe .find ( { 2, 3 } ) == fe .end() ) or ( fe .find ( { 3, 6 } ) == fe .end() ) );
	// assert ( not ( quadrilaterals and parallelipipeds ) );
	assert ( ( fe .find ( { 2, 4 } ) == fe .end() ) or ( fe .find ( { 3, 6 } ) == fe .end() ) );
	// triangles and quadrilaterals are compatible
	// tetrahedra and parallelipipeds are compatible

	// for tetrahedra we reserve space for 30 elements
	// difficult count, make some statistics to check it
	
	// for each type of cell, we reserve so many elements in each column of the sparse matrix
	// if ( triangles and quadrilaterals )  // reserve max between 7 and 9
	if ( ( fe .find ( { 2, 3 } ) != fe .end() ) and ( fe .find ( { 2, 4 } ) != fe .end() ) )
		this->matrix_bilin .reserve ( Eigen::VectorXi::Constant ( size_matrix, 9 ) );
	// if ( tetrahedra and parallelipipeds )  // reserve max between 30 and 27
	else if ( ( fe .find ( { 3, 4 } ) != fe .end() ) and ( fe .find ( { 3, 6 } ) != fe .end() ) )
		this->matrix_bilin .reserve ( Eigen::VectorXi::Constant ( size_matrix, 30 ) );
	else if ( fe .find ( { 1, 2 } ) != fe .end() )  // segments
		this->matrix_bilin .reserve ( Eigen::VectorXi::Constant ( size_matrix, 3 ) );
	else if ( fe .find ( { 2, 3 } ) != fe .end() )  // triangles
		this->matrix_bilin .reserve ( Eigen::VectorXi::Constant ( size_matrix, 7 ) );
	else if ( fe .find ( { 2, 4 } ) != fe .end() )  // quadrilaterals
		this->matrix_bilin .reserve ( Eigen::VectorXi::Constant ( size_matrix, 9 ) );
	else if ( fe .find ( { 3, 4 } ) != fe .end() )  // tetrahedra
		this->matrix_bilin .reserve ( Eigen::VectorXi::Constant ( size_matrix, 30 ) );
	else if ( fe .find ( { 3, 6 } ) != fe .end() )  // parallelipiped
		this->matrix_bilin .reserve ( Eigen::VectorXi::Constant ( size_matrix, 27 ) );

	// add more finite elements, needed for the linear form
	this->linear_form .inspect_cells_of_domain_of_integration ( fe );

	this->bilinear_form .assemble_matrix ( * this, fe );
	this->linear_form .assemble_vector ( * this, fe );
	
	// impose Dirichlet boundary condition
	assert ( this->bdry_cond .size() == 1 );
	Function bdry_val = this->bdry_cond [0] .equality .rhs;
	{ // just a block of code for hiding 'it'
	Mesh::Iterator it = this->bdry_cond [0] .domain .iterator ( tag::over_vertices );
	for ( it .reset(); it .in_range(); it++ )
	{	Cell P = *it;
		impose_value_of_unknown ( this->matrix_bilin, this_vector_lin, numb (P), bdry_val (P) );  }
	} // just a block of code

	// prepare Eigen solver
	this->solver .compute ( this->matrix_bilin );
	
}  // end of  VariationalFormulation::assemble_matrix

//------------------------------------------------------------------------------------------------------//


void VariationalFormulation::solve_linear_system ( Function sol )

{	Function::CoupledWithArray::Scalar * sol_p = tag::Util::assert_cast
		< Function::Core *, Function::CoupledWithArray::Scalar * > ( sol .core );
	assert ( this->numbering .core == sol_p->numbering .core );
	// 'this->solver' has already been initialized with 'this->matrix_bilin'
	* sol_p->vector = this->solver .solve ( * this->vector_lin );
	if ( this->solver .info() != Eigen::Success )
	{	std::cout << "var-form.cpp, VariationalFormulation::solve_linear_system, " << std::endl;
		std::cout << "Eigen solver failed" << std::endl;
		exit (1);                                                                                 }  }

//------------------------------------------------------------------------------------------------------//


void Function::Form::DelayedIntegral::inspect_cells_of_domain_of_integration
// virtual from Function::Form::Core
( std::map < std::pair < size_t, size_t >, FiniteElement > & fe ) const  // override

{	if ( this->domain .dim() == 1 )
	{	if ( fe .find ( { 1, 2 } ) == fe .end() )
		{	FiniteElement fin_elem
				( tag::with_master, tag::segment, tag::Lagrange, tag::of_degree, 1 );
			fin_elem .set_integrator ( tag::Gauss, tag::seg_4 );
			fe .insert ( { { 1, 2 }, fin_elem } );                                    }
		return;                                                                         }

   if ( this->domain .dim() == 2 )
	{	bool triangles = false, quadrilaterals = false;
		Mesh::Iterator it = this->domain .iterator ( tag::over_cells_of_max_dim );
		for ( it .reset(); it .in_range(); it++ )
		{	Cell cll = *it;
			assert ( cll .dim() == 2 );
			const size_t nb_of_faces = cll .boundary() .number_of ( tag::cells_of_max_dim );
			if ( nb_of_faces == 3 )
			{	triangles = true;
				continue;                    }
			assert ( nb_of_faces == 4 );
			quadrilaterals = true;
			continue;                                                                         }
		if ( triangles )
		if ( fe .find ( { 2, 3 } ) == fe .end() )
		{	FiniteElement fin_elem
					( tag::with_master, tag::triangle, tag::Lagrange, tag::of_degree, 1 );
			fin_elem .set_integrator ( tag::Gauss, tag::tri_6 );
			fe .insert ( { { 2, 3 }, fin_elem } );                                        }
		if ( quadrilaterals )
		if ( fe .find ( { 2, 4 } ) == fe .end() )
		{	FiniteElement fin_elem
					( tag::with_master, tag::quadrilateral, tag::Lagrange, tag::of_degree, 1 );
			fin_elem .set_integrator ( tag::Gauss, tag::quad_9 );
			fe .insert ( { { 2, 4 }, fin_elem } );                                             }
		return;                                                                                  }

	assert ( this->domain .dim() == 3 );
	bool tetrahedra = false, hexahedra = false;
	Mesh::Iterator it = this->domain .iterator ( tag::over_cells_of_max_dim );
	for ( it .reset(); it .in_range(); it++ )
	{	Cell cll = *it;
		assert ( cll .dim() == 3 );
		const size_t nb_of_faces = cll .boundary() .number_of ( tag::cells_of_max_dim );
		if ( nb_of_faces == 4 )
		{	tetrahedra = true;
			continue;                    }
		assert ( nb_of_faces == 6 );
		hexahedra = true;
		continue;                                                                         }
	if ( tetrahedra )
	if ( fe .find ( { 3, 4 } ) == fe .end() )
	{	FiniteElement fin_elem
			( tag::with_master, tag::tetrahedron, tag::Lagrange, tag::of_degree, 1 );
		fin_elem .set_integrator ( tag::Gauss, tag::tetra_5 );
		fe .insert ( { { 3, 4 }, fin_elem } );                                        }
	if ( hexahedra )
	if ( fe .find ( { 3, 6 } ) == fe .end() )
	{	FiniteElement fin_elem
		  ( tag::with_master, tag::hexahedron, tag::Lagrange, tag::of_degree, 1 );
		fin_elem .set_integrator ( tag::Gauss, tag::cube_8 );
		fe .insert ( { { 3, 6 }, fin_elem } );                                      }

}  // end of  Function::Form::DelayedIntegral::inspect_cells_of_domain_of_integration


void Function::Form::ValueOfTest::inspect_cells_of_domain_of_integration
// virtual from Function::Form::Core
( std::map < std::pair < size_t, size_t >, FiniteElement > & fe ) const  // override

{	}  // do nothing


void Function::Form::Sum::inspect_cells_of_domain_of_integration
// virtual from Function::Form::Core
( std::map < std::pair < size_t, size_t >, FiniteElement > & fe ) const  // override

{	for ( std::forward_list < Function::Form > ::const_iterator
	         it = this->terms .begin(); it != this->terms .end(); it ++ )
		it->inspect_cells_of_domain_of_integration ( fe );                 }

//------------------------------------------------------------------------------------------------------//


void Function::Core::analyse_integrand ( VariationalFormulation & vf )  // virtual
{	assert ( false );  }


void Function::Sum::analyse_integrand ( VariationalFormulation & vf )  // override
// virtual from Function::Core

{	for ( std::forward_list < Function > ::const_iterator
	         it = this->terms .begin(); it != this->terms .end(); it ++ )
		it->analyse_integrand ( vf );                                       }


void Function::Test::analyse_integrand ( VariationalFormulation & vf )  // override
// virtual from Function::Core

{	if ( vf .test .exists() )
		assert ( vf .test .core == this );
	else
		vf .test = Function ( tag::whose_core_is, this );  }


void Function::Product::analyse_integrand ( VariationalFormulation & vf )  // override
// virtual from Function::Core

// 'this' is a product
// one of the factors must be a Function::Test or its DelayedDerivative
// one of the factors may be a Function::Unknown or its DelayedDerivative

{	std::forward_list < Function > ::const_iterator it_test = this->factors .end();
	std::forward_list < Function > ::const_iterator it_unkn = this->factors .end();
	for ( std::forward_list < Function > ::const_iterator
	         it = this->factors .begin(); it != this->factors .end(); it ++ )
	{	Function::Test * phi = dynamic_cast < Function::Test * > ( it->core );
		if ( phi )
		{	assert ( it_test == this->factors .end() );
			it_test = it;
			if ( vf .test .exists() )
				assert ( vf .test .core == phi );
			else
				vf .test = Function ( tag::whose_core_is, phi );
			continue;                                            }
		Function::Unknown * u = dynamic_cast < Function::Unknown * > ( it->core );
		if ( u )
		{	assert ( it_unkn == this->factors .end() );
			it_unkn = it;
			if ( vf .unknown .exists() )
				assert ( vf .unknown .core == u );
			else
				vf .unknown = Function ( tag::whose_core_is, u );
			continue;                                             }
		Function::DelayedDerivative * der =
			dynamic_cast < Function::DelayedDerivative * > ( it->core );
		if ( der )
		{	Function::Test * der_phi = dynamic_cast < Function::Test * > ( der->base .core );
			if ( der_phi )
			{	assert ( it_test == this->factors .end() );
				it_test = it;
				if ( vf .test .exists() )
					assert ( vf .test .core == der_phi );
				else
					vf .test = Function ( tag::whose_core_is, der_phi );
				continue;                                                }
			Function::Unknown * der_u = dynamic_cast < Function::Unknown * > ( der->base .core );
			if ( der_u )
			{	assert ( it_unkn == this->factors .end() );
				it_unkn = it;
				if ( vf .unknown .exists() )
					assert ( vf .unknown .core == der_u );
				else
					vf .unknown = Function ( tag::whose_core_is, der_u );
				continue;                                                 }                        }  }
	assert ( vf .test .exists() );
	assert ( vf .unknown .exists() );
	assert ( it_test != this->factors .end() );
	// when we analyse a linear form, there will be no unknown in this product

}  // end of  Function::Product::analyse_integrand


void Function::DelayedDerivative::analyse_integrand ( VariationalFormulation & vf )  // override
// virtual from Function::Core

{ this->base .analyse_integrand ( vf );  }


void Function::Form::DelayedIntegral::analyse_form
( VariationalFormulation & vf ) const  // virtual from Function::Form::Core 

{	vf .domains .insert ( this->domain );
	this->integrand .analyse_integrand ( vf );  }


void Function::Form::ValueOfTest::analyse_form ( VariationalFormulation & vf ) const
// virtual from Function::Form::Core

// 'this->test' is not really an integrand, but here we treat it as such
// it's like an integrand for a concentrated measure
{ this->test .analyse_integrand ( vf );  }


void Function::Form::Sum::analyse_form ( VariationalFormulation & vf ) const
// virtual from Function::Form::Core

{	for ( std::forward_list < Function::Form > ::const_iterator
	         it = this->terms .begin(); it != this->terms .end(); it ++ )
		it->analyse_form ( vf );                                            }


Function::Form Function::Form::DelayedIntegral::replace  //  virtual from Function::Form::Core
( const Function & x, const Function & y ) const

{	return  Function::Form ( tag::whose_core_is,
	                         std::make_shared < Function::Form::DelayedIntegral >
	                         ( this->integrand .replace ( x, y ), this->domain )   );  }


Function::Form Function::Form::ValueOfTest::replace  //  virtual from Function::Form::Core
( const Function & x, const Function & y ) const

{	return  Function::Form ( tag::whose_core_is,
	                         std::make_shared < Function::Form::ValueOfTest >
	                         ( this->coeff, this->test .replace ( x, y ), this->cll ) );  }


Function::Form Function::Form::Sum::replace  //  virtual from Function::Form::Core
( const Function & x, const Function & y ) const

{  std::shared_ptr < Function::Form::Sum > res = std::make_shared < Function::Form::Sum > ();
	// empty sum
	for ( std::forward_list < Function::Form > ::const_iterator
	         it = this->terms .begin(); it != this->terms .end(); it ++ )
		res->terms .push_front ( it->replace ( x, y ) );
	return  Function::Form ( tag::whose_core_is,res );                                          }

//------------------------------------------------------------------------------------------------------//


void Function::Form::DelayedIntegral::assemble_matrix  // virtual from Function::Form::Core
( VariationalFormulation & vf, std::map < std::pair < size_t, size_t >, FiniteElement > & fe ) const

{	Cell::Numbering & numb = vf .numbering;
	// run over all cells of the domain of the bilinear form
	const size_t d = this->domain .dim();
	Mesh::Iterator it = this->domain .iterator ( tag::over_cells_of_max_dim );
	for ( it .reset(); it .in_range(); it++ )
	{	Cell cll = *it;
		assert ( cll .dim() == d );
		const size_t nb_of_faces = cll .boundary() .number_of ( tag::cells_of_max_dim );
		std::map < std::pair < size_t, size_t >, FiniteElement > ::iterator it_fe =
		  fe .find ( { d, nb_of_faces } );
		assert ( it_fe != fe .end() );
		FiniteElement fin_elem = it_fe->second;
		fin_elem .dock_on ( cll );
		// run twice over all vertices of 'cll'
		Mesh::Iterator it_V =
			cll .boundary() .iterator ( tag::over_vertices, tag::force_positive );
		Mesh::Iterator it_W =
			cll .boundary() .iterator ( tag::over_vertices, tag::force_positive );
		// tag::force_positive is needed when iterating over the two vertices of a segment
		for ( it_V .reset(); it_V .in_range(); it_V ++ )
		{	Cell V = * it_V;
			Function psi_V = fin_elem .basis_function (V);
			Function integ_V = this->integrand .replace ( vf .unknown, psi_V );
			for ( it_W .reset(); it_W .in_range(); it_W ++ )
			{	Cell W = * it_W;
				Function psi_W = fin_elem .basis_function (W);
				Function integ_VW = integ_V .replace ( vf .test, psi_W );
				// 'fe' is already docked on 'cll' so this will be the domain of integration
				vf .matrix_bilin .coeffRef ( numb (V), numb (W) )
					+= fin_elem .integrate ( integ_VW );                           }  }
	}  // end of 'for' loop over cells of max dim of the domain of the bilinear form
}  // end of Function::Form::DelayedIntegral::assemble_matrix


void Function::Form::ValueOfTest::assemble_matrix  // virtual from Function::Form::Core
( VariationalFormulation & vf, std::map < std::pair < size_t, size_t >, FiniteElement > & fe ) const

{	assert ( false );  }  // this is a linear form, a call to 'assemble_matrix' makes no sense
	

void Function::Form::Sum::assemble_matrix  // virtual from Function::Form::Core
( VariationalFormulation & vf, std::map < std::pair < size_t, size_t >, FiniteElement > & fe ) const

{	for ( std::forward_list < Function::Form > ::const_iterator
	         it = this->terms .begin(); it != this->terms .end(); it ++ )
		it->assemble_matrix ( vf, fe );                                     }
	

void Function::Form::DelayedIntegral::assemble_vector  // virtual from Function::Form::Core
( VariationalFormulation & vf, std::map < std::pair < size_t, size_t >, FiniteElement > & fe ) const

{	Cell::Numbering & numb = vf .numbering;
	Eigen::VectorXd & vec = * vf .vector_lin;
	// run over all cells of the domain of the linear form
	Mesh::Iterator it = this->domain .iterator ( tag::over_cells_of_max_dim );
	const size_t d = this->domain .dim();
	for ( it .reset(); it .in_range(); it++ )
	{	Cell cll = *it;
		const size_t nb_of_faces = cll .boundary() .number_of ( tag::cells_of_max_dim );
		std::map < std::pair < size_t, size_t >, FiniteElement > ::iterator it_fe =
		  fe .find ( { d, nb_of_faces } );
		assert ( it_fe != fe .end() );
		FiniteElement fin_elem = it_fe->second;
		fin_elem .dock_on ( cll );
		// run twice over all vertices of 'cll'
		Mesh::Iterator it_V =
			cll .boundary() .iterator ( tag::over_vertices, tag::force_positive );
		// tag::force_positive is needed when iterating over the two vertices of a segment
		for ( it_V .reset(); it_V .in_range(); it_V ++ )
		{	Cell V = * it_V;
			Function psi_V = fin_elem .basis_function (V);
			Function integ_V = this->integrand .replace ( vf .test, psi_V );
			vec ( numb (V) ) += fin_elem .integrate ( integ_V );              }     }      }


void Function::Form::ValueOfTest::assemble_vector  // virtual from Function::Form::Core
( VariationalFormulation & vf, std::map < std::pair < size_t, size_t >, FiniteElement > & fe ) const

{	Cell::Numbering & numb = vf .numbering;
	Eigen::VectorXd & vec = * vf .vector_lin;
	vec ( numb ( this->cll) ) += coeff;        }
	

void Function::Form::Sum::assemble_vector  // virtual from Function::Form::Core
( VariationalFormulation & vf, std::map < std::pair < size_t, size_t >, FiniteElement > & fe ) const

{	for ( std::forward_list < Function::Form > ::const_iterator
	         it = this->terms .begin(); it != this->terms .end(); it ++ )
		it->assemble_vector ( vf, fe );                                     }
	

//------------------------------------------------------------------------------------------------------//

#endif  // ifndef MANIFEM_NO_FEM
