
//   mesh-iter.h  2024.05.19

//   This file is part of maniFEM, a C++ library for meshes and finite elements on manifolds.

//   Copyright  2019 - 2024  Cristian Barbarosie  cristian.barbarosie@gmail.com

//   https://maniFEM.rd.ciencias.ulisboa.pt/
//   https://codeberg.org/cristian.barbarosie/maniFEM

//   ManiFEM is free software: you can redistribute it and/or modify it
//   under the terms of the GNU Lesser General Public License as published
//   by the Free Software Foundation, either version 3 of the License
//   or (at your option) any later version.

//   ManiFEM is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//   See the GNU Lesser General Public License for more details.

//   You should have received a copy of the GNU Lesser General Public License
//   along with maniFEM.  If not, see <https://www.gnu.org/licenses/>.


	// included from 'mesh.h'
	// this is part of  class Mesh (whence the indentation)
	// iterators not defined here are defined in 'iterator.h'
	
	inline Mesh::Iterator iterator ( const tag::OverVertices &, const tag::AsFound & ) const;
	// if this->dim() == 0, return oriented vertices
	// if this->dim() >= 1, return positive vertices
	inline Mesh::Iterator iterator ( const tag::Over &, const tag::Vertices &, const tag::AsFound & ) const
	{	return  this->iterator ( tag::over_vertices, tag::as_found );  }

	inline Mesh::Iterator iterator ( const tag::OverVertices &, const tag::ForcePositive & ) const;
	inline Mesh::Iterator iterator
	( const tag::Over &, const tag::Vertices &, const tag::ForcePositive & ) const
	{	return  this->iterator ( tag::over_vertices, tag::force_positive );  }

	inline Mesh::Iterator iterator ( const tag::OverVertices & ) const;
	// if this->dim() == 0, print error message
	// if this->dim() >= 1, return positive vertices
	inline Mesh::Iterator iterator ( const tag::Over &, const tag::Vertices & ) const
	{	return  this->iterator ( tag::over_vertices );  }

	inline Mesh::Iterator iterator ( const tag::OverVertices &, const tag::OrientCompWithMesh & ) const;
	// if this->dim() == 0, return oriented vertices
	// if this->dim() >= 1, print error message
	inline Mesh::Iterator iterator
	( const tag::Over &, const tag::Vertices &, const tag::OrientCompWithMesh & ) const
	{	return  this->iterator ( tag::over_vertices, tag::orientation_compatible_with_mesh );  }
	inline Mesh::Iterator iterator
	( const tag::OverVertices &, const tag::Orientation &, const tag::CompatWithMesh & ) const
	{	return  this->iterator ( tag::over_vertices, tag::orientation_compatible_with_mesh );  }
	inline Mesh::Iterator iterator ( const tag::Over &, const tag::Vertices &,
	                                 const tag::Orientation &, const tag::CompatWithMesh & ) const
	{	return  this->iterator ( tag::over_vertices, tag::orientation_compatible_with_mesh );  }
	inline Mesh::Iterator iterator ( const tag::OverVertices &,
		const tag::Orientation &, const tag::CompatWith &, const tag::MEsh & ) const
	{	return  this->iterator ( tag::over_vertices, tag::orientation_compatible_with_mesh );  }
	inline Mesh::Iterator iterator ( const tag::Over &, const tag::Vertices &,
		const tag::Orientation &, const tag::CompatWith &, const tag::MEsh & ) const
	{	return  this->iterator ( tag::over_vertices, tag::orientation_compatible_with_mesh );  }

	inline Mesh::Iterator iterator ( const tag::OverVertices &, const tag::OrientOpposToMesh & ) const;
	// if this->dim() == 0, return oriented vertices
	// if this->dim() >= 1, print error message
	inline Mesh::Iterator iterator
	( const tag::Over &, const tag::Vertices &, const tag::OrientOpposToMesh & ) const
	{	return  this->iterator ( tag::over_vertices, tag::orientation_opposed_to_mesh );  }
	inline Mesh::Iterator iterator
	( const tag::OverVertices &, const tag::Orientation &, const tag::OpposToMesh & ) const
	{	return  this->iterator ( tag::over_vertices, tag::orientation_opposed_to_mesh );  }
	inline Mesh::Iterator iterator ( const tag::Over &, const tag::Vertices &,
	                                 const tag::Orientation &, const tag::OpposToMesh & ) const
	{	return  this->iterator ( tag::over_vertices, tag::orientation_opposed_to_mesh );  }
	inline Mesh::Iterator iterator ( const tag::OverVertices &,
		const tag::Orientation &, const tag::OpposedTo &, const tag::MEsh & ) const
	{	return  this->iterator ( tag::over_vertices, tag::orientation_opposed_to_mesh );  }
	inline Mesh::Iterator iterator ( const tag::Over &, const tag::Vertices &,
		const tag::Orientation &, const tag::OpposedTo &, const tag::MEsh & ) const
	{	return  this->iterator ( tag::over_vertices, tag::orientation_opposed_to_mesh );  }

	inline Mesh::Iterator iterator
	( const tag::OverVertices &, const tag::AsFound &, const tag::RequireOrder & ) const;
	// if this->dim() == 0, return oriented vertices
	// for Connected::OneDim, return positive vertices, ordered
	// for a Fuzzy mesh, print error message (no order)
	inline Mesh::Iterator iterator
	( const tag::Over &, const tag::Vertices &, const tag::AsFound &, const tag::RequireOrder & ) const
	{	return  this->iterator ( tag::over_vertices, tag::as_found, tag::require_order );  }

	inline Mesh::Iterator iterator
	( const tag::OverVertices &, const tag::ForcePositive &, const tag::RequireOrder & ) const;
	// for a Fuzzy mesh, print error message (no order)
	inline Mesh::Iterator iterator ( const tag::Over &, const tag::Vertices &,
	                                 const tag::ForcePositive &, const tag::RequireOrder & ) const
	{	return  this->iterator ( tag::over_vertices, tag::as_found, tag::require_order );  }

	inline Mesh::Iterator iterator ( const tag::OverVertices &, const tag::RequireOrder & ) const;
	// for ZeroDim, print error message
	// for Connected::OneDim, return positive vertices, ordered
	// for Fuzzy, print error message (no order)
	inline Mesh::Iterator iterator
	( const tag::Over &, const tag::Vertices &, const tag::RequireOrder & ) const
	{	return  this->iterator ( tag::over_vertices, tag::require_order );  }

	inline Mesh::Iterator iterator
	( const tag::OverVertices &, const tag::OrientCompWithMesh &, const tag::RequireOrder & ) const;
	// if this->dim() == 0, return oriented vertices
	// if this->dim() >= 1, print error message
	inline Mesh::Iterator iterator
	( const tag::Over &, const tag::Vertices &,
	  const tag::OrientCompWithMesh &, const tag::RequireOrder & ) const
	{	return  this->iterator
			( tag::over_vertices, tag::orientation_compatible_with_mesh, tag::require_order );  }
	inline Mesh::Iterator iterator ( const tag::OverVertices &,
		const tag::Orientation &, const tag::CompatWithMesh &, const tag::RequireOrder & ) const
	{	return  this->iterator
			( tag::over_vertices, tag::orientation_compatible_with_mesh, tag::require_order );  }
	inline Mesh::Iterator iterator ( const tag::Over &, const tag::Vertices &,
	  const tag::Orientation &, const tag::CompatWithMesh &, const tag::RequireOrder & ) const
	{	return  this->iterator
			( tag::over_vertices, tag::orientation_compatible_with_mesh, tag::require_order );  }
	inline Mesh::Iterator iterator ( const tag::OverVertices &, const tag::Orientation &,
		const tag::CompatWith &, const tag::MEsh &, const tag::RequireOrder &              ) const
	{	return  this->iterator
			( tag::over_vertices, tag::orientation_compatible_with_mesh, tag::require_order );  }
	inline Mesh::Iterator iterator ( const tag::Over &, const tag::Vertices &, const tag::Orientation &,
		const tag::CompatWith &, const tag::MEsh &, const tag::RequireOrder &                       ) const
	{	return  this->iterator
			( tag::over_vertices, tag::orientation_compatible_with_mesh, tag::require_order );  }

	inline Mesh::Iterator iterator
	( const tag::OverVertices &, const tag::OrientOpposToMesh &, const tag::RequireOrder & ) const;
	// if this->dim() == 0, return oriented vertices
	// if this->dim() >= 1, print error message
	inline Mesh::Iterator iterator
	( const tag::Over &, const tag::Vertices &,
	  const tag::OrientOpposToMesh &, const tag::RequireOrder & ) const
	{	return  this->iterator
			( tag::over_vertices, tag::orientation_opposed_to_mesh, tag::require_order );  }
	inline Mesh::Iterator iterator ( const tag::OverVertices &,
		const tag::Orientation &, const tag::OpposToMesh &, const tag::RequireOrder & ) const
	{	return  this->iterator
			( tag::over_vertices, tag::orientation_opposed_to_mesh, tag::require_order );  }
	inline Mesh::Iterator iterator ( const tag::Over &, const tag::Vertices &,
	  const tag::Orientation &, const tag::OpposToMesh &, const tag::RequireOrder & ) const
	{	return  this->iterator
			( tag::over_vertices, tag::orientation_opposed_to_mesh, tag::require_order );  }
	inline Mesh::Iterator iterator ( const tag::OverVertices &, const tag::Orientation &,
		const tag::OpposedTo &, const tag::MEsh &, const tag::RequireOrder &              ) const
	{	return  this->iterator
			( tag::over_vertices, tag::orientation_opposed_to_mesh, tag::require_order );  }
	inline Mesh::Iterator iterator ( const tag::Over &, const tag::Vertices &, const tag::Orientation &,
		const tag::OpposedTo &, const tag::MEsh &, const tag::RequireOrder &                        ) const
	{	return  this->iterator
			( tag::over_vertices, tag::orientation_opposed_to_mesh, tag::require_order );  }

	inline Mesh::Iterator iterator
	( const tag::OverVertices &, const tag::AsFound &, const tag::Backwards & ) const;
	inline Mesh::Iterator iterator
	( const tag::Over &, const tag::Vertices &, const tag::AsFound &, const tag::Backwards & ) const
	{	return  this->iterator ( tag::over_vertices, tag::as_found, tag::backwards );  }

	inline Mesh::Iterator iterator
	( const tag::OverVertices &, const tag::ForcePositive &, const tag::Backwards & ) const;
	inline Mesh::Iterator iterator
	( const tag::OverVertices &, const tag::Backwards &, const tag::ForcePositive & ) const
	{	return  this->iterator ( tag::over_vertices, tag::force_positive, tag::backwards );  }

	inline Mesh::Iterator iterator ( const tag::OverVertices &, const tag::Backwards & ) const;
	// for ZeroDim, print error message
	// for Connected::OneDim, return positive vertices, reverse order
	// for Fuzzy, print error message (no order)
	inline Mesh::Iterator iterator
	( const tag::Over &, const tag::Vertices &, const tag::Backwards & ) const
	{	return  this->iterator ( tag::over_vertices, tag::backwards );  }

	inline Mesh::Iterator iterator
	( const tag::OverVertices &, const tag::OrientCompWithMesh &, const tag::Backwards & ) const;
	// if this->dim() == 0, return oriented vertices
	// if this->dim() >= 1, print error message
	inline Mesh::Iterator iterator
	( const tag::Over &, const tag::Vertices &,
	  const tag::OrientCompWithMesh &, const tag::Backwards & ) const
	{	return  this->iterator
			( tag::over_vertices, tag::orientation_compatible_with_mesh, tag::backwards );  }
	inline Mesh::Iterator iterator ( const tag::OverVertices &,
		const tag::Orientation &, const tag::CompatWithMesh &, const tag::Backwards & ) const
	{	return  this->iterator
			( tag::over_vertices, tag::orientation_compatible_with_mesh, tag::backwards );  }
	inline Mesh::Iterator iterator ( const tag::Over &, const tag::Vertices &,
	  const tag::Orientation &, const tag::CompatWithMesh &, const tag::Backwards & ) const
	{	return  this->iterator
			( tag::over_vertices, tag::orientation_compatible_with_mesh, tag::backwards );  }
	inline Mesh::Iterator iterator ( const tag::OverVertices &, const tag::Orientation &,
		const tag::CompatWith &, const tag::MEsh &, const tag::Backwards &                 ) const
	{	return  this->iterator
			( tag::over_vertices, tag::orientation_compatible_with_mesh, tag::backwards );  }
	inline Mesh::Iterator iterator ( const tag::Over &, const tag::Vertices &, const tag::Orientation &,
		const tag::CompatWith &, const tag::MEsh &, const tag::Backwards &                       ) const
	{	return  this->iterator
			( tag::over_vertices, tag::orientation_compatible_with_mesh, tag::backwards );  }

	inline Mesh::Iterator iterator
	( const tag::OverVertices &, const tag::OrientOpposToMesh &, const tag::Backwards & ) const;
	// if this->dim() == 0, return oriented vertices
	// if this->dim() >= 1, print error message
	inline Mesh::Iterator iterator
	( const tag::Over &, const tag::Vertices &,
	  const tag::OrientOpposToMesh &, const tag::Backwards & ) const
	{	return  this->iterator ( tag::over_vertices, tag::orientation_opposed_to_mesh, tag::backwards );  }
	inline Mesh::Iterator iterator ( const tag::OverVertices &,
		const tag::Orientation &, const tag::OpposToMesh &, const tag::Backwards & ) const
	{	return  this->iterator ( tag::over_vertices, tag::orientation_opposed_to_mesh, tag::backwards );  }
	inline Mesh::Iterator iterator ( const tag::Over &, const tag::Vertices &,
	  const tag::Orientation &, const tag::OpposToMesh &, const tag::Backwards & ) const
	{	return  this->iterator ( tag::over_vertices, tag::orientation_opposed_to_mesh, tag::backwards );  }
	inline Mesh::Iterator iterator ( const tag::OverVertices &, const tag::Orientation &,
		const tag::OpposedTo &, const tag::MEsh &, const tag::Backwards &                  ) const
	{	return  this->iterator ( tag::over_vertices, tag::orientation_opposed_to_mesh, tag::backwards );  }
	inline Mesh::Iterator iterator ( const tag::Over &, const tag::Vertices &, const tag::Orientation &,
		const tag::OpposedTo &, const tag::MEsh &, const tag::Backwards &                        ) const
	{	return  this->iterator ( tag::over_vertices, tag::orientation_opposed_to_mesh, tag::backwards );  }

	// we are still in class Mesh

	inline Mesh::Iterator iterator ( const tag::OverSegments &, const tag::AsFound & ) const;
	// if this->dim() == 0, print error message
	// if this->dim() == 1, return oriented segments
	// if this->dim() >= 2, return positive segments
	inline Mesh::Iterator iterator ( const tag::Over &, const tag::Segments &, const tag::AsFound & ) const
	{	return  this->iterator ( tag::over_segments, tag::as_found );  }

	inline Mesh::Iterator iterator ( const tag::OverSegments &, const tag::ForcePositive & ) const;
	// if this->dim() == 0, print error message
	inline Mesh::Iterator iterator
	( const tag::Over &, const tag::Segments &, const tag::ForcePositive & ) const
	{	return  this->iterator ( tag::over_segments, tag::force_positive );  }

	inline Mesh::Iterator iterator ( const tag::OverSegments & ) const
	{	return  this->iterator ( tag::over_segments, tag::as_found );  }
	inline Mesh::Iterator iterator ( const tag::Over &, const tag::Segments & ) const
	{	return  this->iterator ( tag::over_segments, tag::as_found );  }

	inline Mesh::Iterator iterator ( const tag::OverSegments &, const tag::OrientCompWithMesh & ) const;
	// if this->dim() == 0, print error message
	// if this->dim() == 1, return oriented segments
	// if this->dim() >= 2, print error message
	inline Mesh::Iterator iterator
	( const tag::Over &, const tag::Segments &, const tag::OrientCompWithMesh & ) const
	{	return  this->iterator ( tag::over_segments, tag::orientation_compatible_with_mesh );  }
	inline Mesh::Iterator iterator
	( const tag::OverSegments &, const tag::Orientation &, const tag::CompatWithMesh & ) const
	{	return  this->iterator ( tag::over_segments, tag::orientation_compatible_with_mesh );  }
	inline Mesh::Iterator iterator ( const tag::Over &, const tag::Segments &,
	                                 const tag::Orientation &, const tag::CompatWithMesh & ) const
	{	return  this->iterator ( tag::over_segments, tag::orientation_compatible_with_mesh );  }
	inline Mesh::Iterator iterator ( const tag::OverSegments &,
		const tag::Orientation &, const tag::CompatWith &, const tag::MEsh & ) const
	{	return  this->iterator ( tag::over_segments, tag::orientation_compatible_with_mesh );  }
	inline Mesh::Iterator iterator ( const tag::Over &, const tag::Segments &,
		const tag::Orientation &, const tag::CompatWith &, const tag::MEsh & ) const
	{	return  this->iterator ( tag::over_segments, tag::orientation_compatible_with_mesh );  }

	inline Mesh::Iterator iterator ( const tag::OverSegments &, const tag::OrientOpposToMesh & ) const;
	// if this->dim() == 0, print error message
	// if this->dim() == 1, return oriented segments
	// if this->dim() >= 2, print error message
	inline Mesh::Iterator iterator
	( const tag::Over &, const tag::Segments &, const tag::OrientOpposToMesh & ) const
	{	return  this->iterator ( tag::over_segments, tag::orientation_opposed_to_mesh );  }
	inline Mesh::Iterator iterator
	( const tag::OverSegments &, const tag::Orientation &, const tag::OpposToMesh & ) const
	{	return  this->iterator ( tag::over_segments, tag::orientation_opposed_to_mesh );  }
	inline Mesh::Iterator iterator ( const tag::Over &, const tag::Segments &,
	                                 const tag::Orientation &, const tag::OpposToMesh & ) const
	{	return  this->iterator ( tag::over_segments, tag::orientation_opposed_to_mesh );  }
	inline Mesh::Iterator iterator ( const tag::OverSegments &,
		const tag::Orientation &, const tag::OpposedTo &, const tag::MEsh & ) const
	{	return  this->iterator ( tag::over_segments, tag::orientation_opposed_to_mesh );  }
	inline Mesh::Iterator iterator ( const tag::Over &, const tag::Segments &,
		const tag::Orientation &, const tag::OpposedTo &, const tag::MEsh & ) const
	{	return  this->iterator ( tag::over_segments, tag::orientation_opposed_to_mesh );  }

	inline Mesh::Iterator iterator
	( const tag::OverSegments &, const tag::AsFound &, const tag::RequireOrder & ) const;
	// if this->dim() == 0, print error message
	// for Connected::OneDim, return oriented segments, ordered
	// for a Fuzzy mesh, print error message (no order)
	inline Mesh::Iterator iterator
	( const tag::Over &, const tag::Segments &, const tag::AsFound &, const tag::RequireOrder & ) const
	{	return  this->iterator ( tag::over_segments, tag::as_found, tag::require_order );  }

	inline Mesh::Iterator iterator
	( const tag::OverSegments &, const tag::ForcePositive &, const tag::RequireOrder & ) const;
	// if this->dim() == 0, print error message
	// for a Fuzzy mesh, print error message (no order)
	inline Mesh::Iterator iterator ( const tag::Over &, const tag::Segments &,
	                                 const tag::ForcePositive &, const tag::RequireOrder & ) const
	{	return  this->iterator ( tag::over_segments, tag::as_found, tag::require_order );  }

	inline Mesh::Iterator iterator ( const tag::OverSegments &, const tag::RequireOrder & ) const
	{	return  this->iterator ( tag::over_segments, tag::as_found, tag::require_order );  }
	inline Mesh::Iterator iterator
	( const tag::Over &, const tag::Segments &, const tag::RequireOrder & ) const
	{	return  this->iterator ( tag::over_segments, tag::as_found, tag::require_order );  }

	inline Mesh::Iterator iterator
	( const tag::OverSegments &, const tag::OrientCompWithMesh &, const tag::RequireOrder & ) const;
	// if this->dim() == 0, print error message
	// if this->dim() == 1, return oriented segments
	// if this->dim() >= 2, print error message
	inline Mesh::Iterator iterator
	( const tag::Over &, const tag::Segments &,
	  const tag::OrientCompWithMesh &, const tag::RequireOrder & ) const
	{	return  this->iterator
			( tag::over_segments, tag::orientation_compatible_with_mesh, tag::require_order );  }
	inline Mesh::Iterator iterator ( const tag::OverSegments &,
		const tag::Orientation &, const tag::CompatWithMesh &, const tag::RequireOrder & ) const
	{	return  this->iterator
			( tag::over_segments, tag::orientation_compatible_with_mesh, tag::require_order );  }
	inline Mesh::Iterator iterator ( const tag::Over &, const tag::Segments &,
	  const tag::Orientation &, const tag::CompatWithMesh &, const tag::RequireOrder & ) const
	{	return  this->iterator
			( tag::over_segments, tag::orientation_compatible_with_mesh, tag::require_order );  }
	inline Mesh::Iterator iterator ( const tag::OverSegments &, const tag::Orientation &,
		const tag::CompatWith &, const tag::MEsh &, const tag::RequireOrder &              ) const
	{	return  this->iterator
			( tag::over_segments, tag::orientation_compatible_with_mesh, tag::require_order );  }
	inline Mesh::Iterator iterator ( const tag::Over &, const tag::Segments &, const tag::Orientation &,
		const tag::CompatWith &, const tag::MEsh &, const tag::RequireOrder &                       ) const
	{	return  this->iterator
			( tag::over_segments, tag::orientation_compatible_with_mesh, tag::require_order );  }

	inline Mesh::Iterator iterator
	( const tag::OverSegments &, const tag::OrientOpposToMesh &, const tag::RequireOrder & ) const;
	// if this->dim() == 0, print error message
	// if this->dim() == 1, return oriented segments
	// if this->dim() >= 2, print error message
	inline Mesh::Iterator iterator
	( const tag::Over &, const tag::Segments &,
	  const tag::OrientOpposToMesh &, const tag::RequireOrder & ) const
	{	return  this->iterator
			( tag::over_segments, tag::orientation_opposed_to_mesh, tag::require_order );  }
	inline Mesh::Iterator iterator ( const tag::OverSegments &,
		const tag::Orientation &, const tag::OpposToMesh &, const tag::RequireOrder & ) const
	{	return  this->iterator
			( tag::over_segments, tag::orientation_opposed_to_mesh, tag::require_order );  }
	inline Mesh::Iterator iterator ( const tag::Over &, const tag::Segments &,
	  const tag::Orientation &, const tag::OpposToMesh &, const tag::RequireOrder & ) const
	{	return  this->iterator
			( tag::over_segments, tag::orientation_opposed_to_mesh, tag::require_order );  }
	inline Mesh::Iterator iterator ( const tag::OverSegments &, const tag::Orientation &,
		const tag::OpposedTo &, const tag::MEsh &, const tag::RequireOrder &              ) const
	{	return  this->iterator
			( tag::over_segments, tag::orientation_opposed_to_mesh, tag::require_order );  }
	inline Mesh::Iterator iterator ( const tag::Over &, const tag::Segments &, const tag::Orientation &,
		const tag::OpposedTo &, const tag::MEsh &, const tag::RequireOrder &                        ) const
	{	return  this->iterator
			( tag::over_segments, tag::orientation_opposed_to_mesh, tag::require_order );  }

	inline Mesh::Iterator iterator
	( const tag::OverSegments &, const tag::AsFound &, const tag::Backwards & ) const;
	// if this->dim() == 0, print error message
	// if this->dim() == 1, return oriented segments
	// for a Fuzzy mesh, print error message (no order)
	inline Mesh::Iterator iterator
	( const tag::Over &, const tag::Segments &, const tag::AsFound &, const tag::Backwards & ) const
	{	return  this->iterator ( tag::over_segments, tag::as_found, tag::backwards );  }

	inline Mesh::Iterator iterator
	( const tag::OverSegments &, const tag::ForcePositive &, const tag::Backwards & ) const;
	// if this->dim() == 0, print error message
	// for a Fuzzy mesh, print error message (no order)
	inline Mesh::Iterator iterator
	( const tag::Over &, const tag::Segments &, const tag::ForcePositive &, const tag::Backwards & ) const
	{	return  this->iterator ( tag::over_segments, tag::force_positive, tag::backwards );  }

	inline Mesh::Iterator iterator ( const tag::OverSegments &, const tag::Backwards & ) const
	{	return  this->iterator ( tag::over_segments, tag::as_found, tag::backwards );  }
	inline Mesh::Iterator iterator
	( const tag::Over &, const tag::Segments &, const tag::Backwards & ) const
	{	return  this->iterator ( tag::over_segments, tag::as_found, tag::backwards );  }

	inline Mesh::Iterator iterator
	( const tag::OverSegments &, const tag::OrientCompWithMesh &, const tag::Backwards & ) const;
	// if this->dim() == 0, print error message
	// if this->dim() == 1, return oriented segments
	// if this->dim() >= 2, print error message
	inline Mesh::Iterator iterator
	( const tag::Over &, const tag::Segments &,
	  const tag::OrientCompWithMesh &, const tag::Backwards & ) const
	{	return  this->iterator
			( tag::over_segments, tag::orientation_compatible_with_mesh, tag::backwards );  }
	inline Mesh::Iterator iterator ( const tag::OverSegments &,
		const tag::Orientation &, const tag::CompatWithMesh &, const tag::Backwards & ) const
	{	return  this->iterator
			( tag::over_segments, tag::orientation_compatible_with_mesh, tag::backwards );  }
	inline Mesh::Iterator iterator ( const tag::Over &, const tag::Segments &,
	  const tag::Orientation &, const tag::CompatWithMesh &, const tag::Backwards & ) const
	{	return  this->iterator
			( tag::over_segments, tag::orientation_compatible_with_mesh, tag::backwards );  }
	inline Mesh::Iterator iterator ( const tag::OverSegments &, const tag::Orientation &,
		const tag::CompatWith &, const tag::MEsh &, const tag::Backwards &                 ) const
	{	return  this->iterator
			( tag::over_segments, tag::orientation_compatible_with_mesh, tag::backwards );  }
	inline Mesh::Iterator iterator ( const tag::Over &, const tag::Segments &, const tag::Orientation &,
		const tag::CompatWith &, const tag::MEsh &, const tag::Backwards &                       ) const
	{	return  this->iterator
			( tag::over_segments, tag::orientation_compatible_with_mesh, tag::backwards );  }

	inline Mesh::Iterator iterator
	( const tag::OverSegments &, const tag::OrientOpposToMesh &, const tag::Backwards & ) const;
	// if this->dim() == 0, print error message
	// if this->dim() == 1, return oriented segments
	// if this->dim() >= 2, print error message
	inline Mesh::Iterator iterator
	( const tag::Over &, const tag::Segments &,
	  const tag::OrientOpposToMesh &, const tag::Backwards & ) const
	{	return  this->iterator ( tag::over_segments, tag::orientation_opposed_to_mesh, tag::backwards );  }
	inline Mesh::Iterator iterator ( const tag::OverSegments &,
		const tag::Orientation &, const tag::OpposToMesh &, const tag::Backwards & ) const
	{	return  this->iterator ( tag::over_segments, tag::orientation_opposed_to_mesh, tag::backwards );  }
	inline Mesh::Iterator iterator ( const tag::Over &, const tag::Segments &,
	  const tag::Orientation &, const tag::OpposToMesh &, const tag::Backwards & ) const
	{	return  this->iterator ( tag::over_segments, tag::orientation_opposed_to_mesh, tag::backwards );  }
	inline Mesh::Iterator iterator ( const tag::OverSegments &, const tag::Orientation &,
		const tag::OpposedTo &, const tag::MEsh &, const tag::Backwards &                  ) const
	{	return  this->iterator ( tag::over_segments, tag::orientation_opposed_to_mesh, tag::backwards );  }
	inline Mesh::Iterator iterator ( const tag::Over &, const tag::Segments &, const tag::Orientation &,
		const tag::OpposedTo &, const tag::MEsh &, const tag::Backwards &                        ) const
	{	return  this->iterator ( tag::over_segments, tag::orientation_opposed_to_mesh, tag::backwards );  }

	// we are still in class Mesh

	inline Mesh::Iterator iterator
	( const tag::OverCellsOfDim &, const size_t d, const tag::AsFound & ) const;
	// if d == this->dim(), return oriented cells
	// if d < this->dim(),  return positive cells
	inline Mesh::Iterator iterator
	( const tag::Over &, const tag::CellsOfDim &, const size_t d, const tag::AsFound & ) const
	{	return  this->iterator ( tag::over_cells_of_dim, d, tag::as_found );  }

	inline Mesh::Iterator iterator
	( const tag::OverCellsOfDim &, const size_t d, const tag::CellHasLowDim & ) const;
	// d < this->dim(),  return positive cells
	inline Mesh::Iterator iterator
	( const tag::Over &, const tag::CellsOfDim &, const size_t d, const tag::CellHasLowDim & ) const
	{	return  this->iterator ( tag::over_cells_of_dim, d, tag::cell_has_low_dim );  }

	inline Mesh::Iterator iterator
	( const tag::OverCellsOfDim &, const size_t, const tag::ForcePositive & ) const;
	inline Mesh::Iterator iterator
	( const tag::Over &, const tag::CellsOfDim &, const size_t d, const tag::ForcePositive & ) const
	{	return  this->iterator ( tag::over_cells_of_dim, d, tag::force_positive );  }

	inline Mesh::Iterator iterator ( const tag::OverCellsOfDim &, const size_t d ) const;
	// if d == this->dim(), return oriented cells
	// special case : if d == this->dim() == 0, print error message
	// if d < this->dim(),  return positive cells
	inline Mesh::Iterator iterator ( const tag::Over &, const tag::CellsOfDim &, const size_t d ) const
	{	return  this->iterator ( tag::over_cells_of_dim, d );  }

	inline Mesh::Iterator iterator
	( const tag::OverCellsOfDim &, const size_t, const tag::OrientCompWithMesh & ) const;
	// assert  this->dim() == d
	inline Mesh::Iterator iterator
	( const tag::Over &, const tag::CellsOfDim &, const size_t d, const tag::OrientCompWithMesh & ) const
	{	return  this->iterator ( tag::over_cells_of_dim, d, tag::orientation_compatible_with_mesh );  }
	inline Mesh::Iterator iterator ( const tag::OverCellsOfDim &, const size_t d,
	                                 const tag::Orientation &, const tag::CompatWithMesh & ) const
	{	return  this->iterator ( tag::over_cells_of_dim, d, tag::orientation_compatible_with_mesh );  }
	inline Mesh::Iterator iterator ( const tag::Over &, const tag::CellsOfDim &, const size_t d,
	                                 const tag::Orientation &, const tag::CompatWithMesh &      ) const
	{	return  this->iterator ( tag::over_cells_of_dim, d, tag::orientation_compatible_with_mesh );  }
	inline Mesh::Iterator iterator ( const tag::OverCellsOfDim &, const size_t d,
		const tag::Orientation &, const tag::CompatWith &, const tag::MEsh & ) const
	{	return  this->iterator ( tag::over_cells_of_dim, d, tag::orientation_compatible_with_mesh );  }
	inline Mesh::Iterator iterator ( const tag::Over &, const tag::CellsOfDim &, const size_t d,
		const tag::Orientation &, const tag::CompatWith &, const tag::MEsh & ) const
	{	return  this->iterator ( tag::over_cells_of_dim, d, tag::orientation_compatible_with_mesh );  }

	inline Mesh::Iterator iterator
	( const tag::OverCellsOfDim &, const size_t, const tag::OrientOpposToMesh & ) const;
	// assert  this->dim() == d
	inline Mesh::Iterator iterator
	( const tag::Over &, const tag::CellsOfDim &, const size_t d, const tag::OrientOpposToMesh & ) const
	{	return  this->iterator ( tag::over_cells_of_dim, d, tag::orientation_opposed_to_mesh );  }
	inline Mesh::Iterator iterator ( const tag::OverCellsOfDim &, const size_t d,
	                                 const tag::Orientation &, const tag::OpposToMesh & ) const
	{	return  this->iterator ( tag::over_cells_of_dim, d, tag::orientation_opposed_to_mesh );  }
	inline Mesh::Iterator iterator ( const tag::Over &, const tag::CellsOfDim &, const size_t d,
	                                 const tag::Orientation &, const tag::OpposToMesh &         ) const
	{	return  this->iterator ( tag::over_cells_of_dim, d, tag::orientation_opposed_to_mesh );  }
	inline Mesh::Iterator iterator ( const tag::OverCellsOfDim &, const size_t d,
		const tag::Orientation &, const tag::OpposedTo &, const tag::MEsh &        ) const
	{	return  this->iterator ( tag::over_cells_of_dim, d, tag::orientation_opposed_to_mesh );  }
	inline Mesh::Iterator iterator ( const tag::Over &, const tag::CellsOfDim &, const size_t d,
		const tag::Orientation &, const tag::OpposedTo &, const tag::MEsh &                       ) const
	{	return  this->iterator ( tag::over_cells_of_dim, d, tag::orientation_opposed_to_mesh );  }

	inline Mesh::Iterator iterator
	( const tag::OverCellsOfDim &, const size_t, const tag::AsFound &, const tag::RequireOrder & ) const;
	// for a Fuzzy mesh, print error message (no order)
	inline Mesh::Iterator iterator ( const tag::Over &, const tag::CellsOfDim &, const size_t d,
	                                 const tag::AsFound &, const tag::RequireOrder &            ) const
	{	return  this->iterator ( tag::over_cells_of_dim, d, tag::as_found, tag::require_order );  }

	inline Mesh::Iterator iterator ( const tag::OverCellsOfDim &, const size_t,
	                                 const tag::ForcePositive &, const tag::RequireOrder & ) const;
	// for a Fuzzy mesh, print error message (no order)
	inline Mesh::Iterator iterator ( const tag::Over &, const tag::CellsOfDim &, const size_t d,
	                                 const tag::ForcePositive &, const tag::RequireOrder & ) const
	{	return  this->iterator ( tag::over_cells_of_dim, d, tag::as_found, tag::require_order );  }

	inline Mesh::Iterator iterator
	( const tag::OverCellsOfDim &, const size_t, const tag::RequireOrder & ) const;
	// if this->dim() == d, return oriented cells
	// special case : if this->dim() == d == 0, print error message
	// if this->dim() < d,  return positive cells
	// for a Fuzzy mesh, print error message (no order)
	inline Mesh::Iterator iterator
	( const tag::Over &, const tag::CellsOfDim &, const size_t d, const tag::RequireOrder & ) const
	{	return  this->iterator ( tag::over_cells_of_dim, d, tag::require_order );  }

	inline Mesh::Iterator iterator ( const tag::OverCellsOfDim &, const size_t,
	                                 const tag::OrientCompWithMesh &, const tag::RequireOrder & ) const;
	// assert  this->dim() == d
	// for a Fuzzy mesh, print error message (no order)
	inline Mesh::Iterator iterator
	( const tag::Over &, const tag::CellsOfDim &, const size_t d,
	  const tag::OrientCompWithMesh &, const tag::RequireOrder & ) const
	{	return  this->iterator
			( tag::over_cells_of_dim, d, tag::orientation_compatible_with_mesh, tag::require_order );  }
	inline Mesh::Iterator iterator ( const tag::OverCellsOfDim &, const size_t d,
		const tag::Orientation &, const tag::CompatWithMesh &, const tag::RequireOrder & ) const
	{	return  this->iterator
			( tag::over_cells_of_dim, d, tag::orientation_compatible_with_mesh, tag::require_order );  }
	inline Mesh::Iterator iterator ( const tag::Over &, const tag::CellsOfDim &, const size_t d,
	  const tag::Orientation &, const tag::CompatWithMesh &, const tag::RequireOrder &          ) const
	{	return  this->iterator
			( tag::over_cells_of_dim, d, tag::orientation_compatible_with_mesh, tag::require_order );  }
	inline Mesh::Iterator iterator ( const tag::OverCellsOfDim &, const size_t d, const tag::Orientation &,
		const tag::CompatWith &, const tag::MEsh &, const tag::RequireOrder &                        ) const
	{	return  this->iterator
			( tag::over_cells_of_dim, d, tag::orientation_compatible_with_mesh, tag::require_order );  }
	inline Mesh::Iterator iterator ( const tag::Over &, const tag::CellsOfDim &, const size_t d,
		const tag::Orientation &, const tag::CompatWith &, const tag::MEsh &,
		const tag::RequireOrder &                                                                 ) const
	{	return  this->iterator
			( tag::over_cells_of_dim, d, tag::orientation_compatible_with_mesh, tag::require_order );  }

	inline Mesh::Iterator iterator ( const tag::OverCellsOfDim &, const size_t,
	                                 const tag::OrientOpposToMesh &, const tag::RequireOrder & ) const;
	// assert  this->dim() == d
	// for a Fuzzy mesh, print error message (no order)
	inline Mesh::Iterator iterator
	( const tag::Over &, const tag::CellsOfDim &, const size_t d,
	  const tag::OrientOpposToMesh &, const tag::RequireOrder & ) const
	{	return  this->iterator
			( tag::over_cells_of_dim, d, tag::orientation_opposed_to_mesh, tag::require_order );  }
	inline Mesh::Iterator iterator ( const tag::OverCellsOfDim &, const size_t d,
		const tag::Orientation &, const tag::OpposToMesh &, const tag::RequireOrder & ) const
	{	return  this->iterator
			( tag::over_cells_of_dim, d, tag::orientation_opposed_to_mesh, tag::require_order );  }
	inline Mesh::Iterator iterator ( const tag::Over &, const tag::CellsOfDim &, const size_t d,
	  const tag::Orientation &, const tag::OpposToMesh &, const tag::RequireOrder & ) const
	{	return  this->iterator
			( tag::over_cells_of_dim, d, tag::orientation_opposed_to_mesh, tag::require_order );  }
	inline Mesh::Iterator iterator ( const tag::OverCellsOfDim &, const size_t d, const tag::Orientation &,
		const tag::OpposedTo &, const tag::MEsh &, const tag::RequireOrder &              ) const
	{	return  this->iterator
			( tag::over_cells_of_dim, d, tag::orientation_opposed_to_mesh, tag::require_order );  }
	inline Mesh::Iterator iterator ( const tag::Over &, const tag::CellsOfDim &, const size_t d,
		const tag::Orientation &, const tag::OpposedTo &, const tag::MEsh &,
		const tag::RequireOrder &                                                                 ) const
	{	return  this->iterator
			( tag::over_cells_of_dim, d, tag::orientation_opposed_to_mesh, tag::require_order );  }

	inline Mesh::Iterator iterator
	( const tag::OverCellsOfDim &, const size_t, const tag::AsFound &, const tag::Backwards & ) const;
	// for a Fuzzy mesh, print error message (no order)
	inline Mesh::Iterator iterator ( const tag::Over &, const tag::CellsOfDim &, const size_t d,
	                                 const tag::AsFound &, const tag::Backwards &               ) const
	{	return  this->iterator ( tag::over_cells_of_dim, d, tag::as_found, tag::backwards );  }

	inline Mesh::Iterator iterator
	( const tag::OverCellsOfDim &, const size_t, const tag::ForcePositive &, const tag::Backwards & ) const;
	// for a Fuzzy mesh, print error message (no order)
	inline Mesh::Iterator iterator ( const tag::Over &, const tag::CellsOfDim &, const size_t d,
	                                 const tag::ForcePositive &, const tag::Backwards &         ) const
	{	return  this->iterator ( tag::over_cells_of_dim, d, tag::force_positive, tag::backwards );  }

	inline Mesh::Iterator iterator
	( const tag::OverCellsOfDim &, const size_t, const tag::Backwards & ) const;
	// if this->dim() == d, return oriented cells
	// special case : if this->dim() == d == 0, print error message
	// if this->dim() < d,  return positive cells
	// for a Fuzzy mesh, print error message (no order)
	inline Mesh::Iterator iterator
	( const tag::Over &, const tag::CellsOfDim &, const size_t d, const tag::Backwards & ) const
	{	return  this->iterator ( tag::over_cells_of_dim, d, tag::backwards );  }

	inline Mesh::Iterator iterator ( const tag::OverCellsOfDim &, const size_t,
	                                 const tag::OrientCompWithMesh &, const tag::Backwards & ) const;
	// assert  this->dim() == d
	// for a Fuzzy mesh, print error message (no order)
	inline Mesh::Iterator iterator
	( const tag::Over &, const tag::CellsOfDim &, const size_t d,
	  const tag::OrientCompWithMesh &, const tag::Backwards & ) const
	{	return  this->iterator
			( tag::over_cells_of_dim, d, tag::orientation_compatible_with_mesh, tag::backwards );  }
	inline Mesh::Iterator iterator ( const tag::OverCellsOfDim &, const size_t d,
		const tag::Orientation &, const tag::CompatWithMesh &, const tag::Backwards & ) const
	{	return  this->iterator
			( tag::over_cells_of_dim, d, tag::orientation_compatible_with_mesh, tag::backwards );  }
	inline Mesh::Iterator iterator ( const tag::Over &, const tag::CellsOfDim &, const size_t d,
	  const tag::Orientation &, const tag::CompatWithMesh &, const tag::Backwards & ) const
	{	return  this->iterator
			( tag::over_cells_of_dim, d, tag::orientation_compatible_with_mesh, tag::backwards );  }
	inline Mesh::Iterator iterator ( const tag::OverCellsOfDim &, const size_t d, const tag::Orientation &,
		const tag::CompatWith &, const tag::MEsh &, const tag::Backwards &                           ) const
	{	return  this->iterator
			( tag::over_cells_of_dim, d, tag::orientation_compatible_with_mesh, tag::backwards );  }
	inline Mesh::Iterator iterator
	( const tag::Over &, const tag::CellsOfDim &, const size_t d, const tag::Orientation &,
	  const tag::CompatWith &, const tag::MEsh &, const tag::Backwards &                   ) const
	{	return  this->iterator
			( tag::over_cells_of_dim, d, tag::orientation_compatible_with_mesh, tag::backwards );  }

	inline Mesh::Iterator iterator ( const tag::OverCellsOfDim &, const size_t,
	                                const tag::OrientOpposToMesh &, const tag::Backwards & ) const;
	// assert  this->dim() == d
	// for a Fuzzy mesh, print error message (no order)
	inline Mesh::Iterator iterator
	( const tag::Over &, const tag::CellsOfDim &, const size_t d,
	  const tag::OrientOpposToMesh &, const tag::Backwards &     ) const
	{	return  this->iterator
			( tag::over_cells_of_dim, d, tag::orientation_opposed_to_mesh, tag::backwards );  }
	inline Mesh::Iterator iterator ( const tag::OverCellsOfDim &, const size_t d,
		const tag::Orientation &, const tag::OpposToMesh &, const tag::Backwards & ) const
	{	return  this->iterator
			( tag::over_cells_of_dim, d, tag::orientation_opposed_to_mesh, tag::backwards );  }
	inline Mesh::Iterator iterator ( const tag::Over &, const tag::CellsOfDim &, const size_t d,
	  const tag::Orientation &, const tag::OpposToMesh &, const tag::Backwards &                ) const
	{	return  this->iterator
			( tag::over_cells_of_dim, d, tag::orientation_opposed_to_mesh, tag::backwards );  }
	inline Mesh::Iterator iterator ( const tag::OverCellsOfDim &, const size_t d, const tag::Orientation &,
		const tag::OpposedTo &, const tag::MEsh &, const tag::Backwards &                       ) const
	{	return  this->iterator
			( tag::over_cells_of_dim, d, tag::orientation_opposed_to_mesh, tag::backwards );  }
	inline Mesh::Iterator iterator
	( const tag::Over &, const tag::CellsOfDim &, const size_t d, const tag::Orientation &,
	  const tag::OpposedTo &, const tag::MEsh &, const tag::Backwards &                    ) const
	{	return  this->iterator
			( tag::over_cells_of_dim, d, tag::orientation_opposed_to_mesh, tag::backwards );  }

	// we are still in class Mesh

	inline Mesh::Iterator iterator
	( const tag::OverCellsOfMaxDim &, const tag::AsFound & ) const;
	// return oriented cells
	inline Mesh::Iterator iterator
	( const tag::Over &, const tag::CellsOfMaxDim &, const tag::AsFound & ) const
	{	return  this->iterator ( tag::over_cells_of_max_dim, tag::as_found );  }

	inline Mesh::Iterator iterator
	( const tag::OverCellsOfMaxDim &, const tag::ForcePositive & ) const;
	inline Mesh::Iterator iterator
	( const tag::Over &, const tag::CellsOfMaxDim &, const tag::ForcePositive & ) const
	{	return  this->iterator ( tag::over_cells_of_max_dim, tag::force_positive );  }

	inline Mesh::Iterator iterator
	( const tag::OverCellsOfMaxDim &, const tag::OrientCompWithMesh & ) const;
	inline Mesh::Iterator iterator
	( const tag::Over &, const tag::CellsOfMaxDim &, const tag::OrientCompWithMesh & ) const
	{	return  this->iterator ( tag::over_cells_of_max_dim, tag::orientation_compatible_with_mesh );  }
	inline Mesh::Iterator iterator ( const tag::OverCellsOfMaxDim & ) const
	{	return  this->iterator ( tag::over_cells_of_max_dim, tag::orientation_compatible_with_mesh );  }
	inline Mesh::Iterator iterator ( const tag::Over &, const tag::CellsOfMaxDim & ) const
	{	return  this->iterator ( tag::over_cells_of_max_dim, tag::orientation_compatible_with_mesh );  }
	inline Mesh::Iterator iterator ( const tag::OverCellsOfMaxDim &,
	                                 const tag::Orientation &, const tag::CompatWithMesh & ) const
	{	return  this->iterator ( tag::over_cells_of_max_dim, tag::orientation_compatible_with_mesh );  }
	inline Mesh::Iterator iterator ( const tag::Over &, const tag::CellsOfMaxDim &,
	                                 const tag::Orientation &, const tag::CompatWithMesh & ) const
	{	return  this->iterator ( tag::over_cells_of_max_dim, tag::orientation_compatible_with_mesh );  }
	inline Mesh::Iterator iterator ( const tag::OverCellsOfMaxDim &,
		const tag::Orientation &, const tag::CompatWith &, const tag::MEsh & ) const
	{	return  this->iterator ( tag::over_cells_of_max_dim, tag::orientation_compatible_with_mesh );  }
	inline Mesh::Iterator iterator ( const tag::Over &, const tag::CellsOfMaxDim &,
		const tag::Orientation &, const tag::CompatWith &, const tag::MEsh & ) const
	{	return  this->iterator ( tag::over_cells_of_max_dim, tag::orientation_compatible_with_mesh );  }

	inline Mesh::Iterator iterator
	( const tag::OverCellsOfMaxDim &, const tag::OrientOpposToMesh & ) const;
	// assert  this->dim() == d
	inline Mesh::Iterator iterator
	( const tag::Over &, const tag::CellsOfMaxDim &, const tag::OrientOpposToMesh & ) const
	{	return  this->iterator ( tag::over_cells_of_max_dim, tag::orientation_opposed_to_mesh );  }
	inline Mesh::Iterator iterator
	( const tag::OverCellsOfMaxDim &, const tag::Orientation &, const tag::OpposToMesh & ) const
	{	return  this->iterator ( tag::over_cells_of_max_dim, tag::orientation_opposed_to_mesh );  }
	inline Mesh::Iterator iterator ( const tag::Over &, const tag::CellsOfMaxDim &,
	                                 const tag::Orientation &, const tag::OpposToMesh & ) const
	{	return  this->iterator ( tag::over_cells_of_max_dim, tag::orientation_opposed_to_mesh );  }
	inline Mesh::Iterator iterator ( const tag::OverCellsOfMaxDim &,
		const tag::Orientation &, const tag::OpposedTo &, const tag::MEsh & ) const
	{	return  this->iterator ( tag::over_cells_of_max_dim, tag::orientation_opposed_to_mesh );  }
	inline Mesh::Iterator iterator ( const tag::Over &, const tag::CellsOfMaxDim &,
		const tag::Orientation &, const tag::OpposedTo &, const tag::MEsh & ) const
	{	return  this->iterator ( tag::over_cells_of_max_dim, tag::orientation_opposed_to_mesh );  }

	inline Mesh::Iterator iterator
	( const tag::OverCellsOfMaxDim &, const tag::AsFound &, const tag::RequireOrder & ) const;
	// for a Fuzzy mesh, print error message (no order)
	inline Mesh::Iterator iterator ( const tag::Over &, const tag::CellsOfMaxDim &,
	                                 const tag::AsFound &, const tag::RequireOrder & ) const
	{	return  this->iterator ( tag::over_cells_of_max_dim, tag::as_found, tag::require_order );  }

	inline Mesh::Iterator iterator ( const tag::OverCellsOfMaxDim &,
	                                 const tag::ForcePositive &, const tag::RequireOrder & ) const;
	// for a Fuzzy mesh, print error message (no order)
	inline Mesh::Iterator iterator ( const tag::Over &, const tag::CellsOfMaxDim &,
	                                 const tag::ForcePositive &, const tag::RequireOrder & ) const
	{	return  this->iterator ( tag::over_cells_of_max_dim, tag::as_found, tag::require_order );  }


	inline Mesh::Iterator iterator
	( const tag::OverCellsOfMaxDim &, const tag::OrientCompWithMesh &, const tag::RequireOrder & ) const;
	// for a Fuzzy mesh, print error message (no order)
	inline Mesh::Iterator iterator ( const tag::Over &, const tag::CellsOfMaxDim &,
	                                 const tag::OrientCompWithMesh &, const tag::RequireOrder & ) const
	{	return  this->iterator
			( tag::over_cells_of_max_dim, tag::orientation_compatible_with_mesh, tag::require_order );  }
	inline Mesh::Iterator iterator ( const tag::OverCellsOfMaxDim &, const tag::RequireOrder & ) const
	{	return  this->iterator
			( tag::over_cells_of_max_dim, tag::orientation_compatible_with_mesh, tag::require_order );  }
	inline Mesh::Iterator iterator
	( const tag::Over &, const tag::CellsOfMaxDim &, const tag::RequireOrder & ) const
	{	return  this->iterator
			( tag::over_cells_of_max_dim, tag::orientation_compatible_with_mesh, tag::require_order );  }
	inline Mesh::Iterator iterator ( const tag::OverCellsOfMaxDim &,
		const tag::Orientation &, const tag::CompatWithMesh &, const tag::RequireOrder & ) const
	{	return  this->iterator
			( tag::over_cells_of_max_dim, tag::orientation_compatible_with_mesh, tag::require_order );  }
	inline Mesh::Iterator iterator ( const tag::Over &, const tag::CellsOfMaxDim &,
	  const tag::Orientation &, const tag::CompatWithMesh &, const tag::RequireOrder & ) const
	{	return  this->iterator
			( tag::over_cells_of_max_dim, tag::orientation_compatible_with_mesh, tag::require_order );  }
	inline Mesh::Iterator iterator ( const tag::OverCellsOfMaxDim &, const tag::Orientation &,
		const tag::CompatWith &, const tag::MEsh &, const tag::RequireOrder &                   ) const
	{	return  this->iterator
			( tag::over_cells_of_max_dim, tag::orientation_compatible_with_mesh, tag::require_order );  }
	inline Mesh::Iterator iterator ( const tag::Over &, const tag::CellsOfMaxDim &,
		const tag::Orientation &, const tag::CompatWith &, const tag::MEsh &,
		const tag::RequireOrder &                                                    ) const
	{	return  this->iterator
			( tag::over_cells_of_max_dim, tag::orientation_compatible_with_mesh, tag::require_order );  }

	inline Mesh::Iterator iterator
	( const tag::OverCellsOfMaxDim &, const tag::OrientOpposToMesh &, const tag::RequireOrder & ) const;
	// for a Fuzzy mesh, print error message (no order)
	inline Mesh::Iterator iterator
	( const tag::Over &, const tag::CellsOfMaxDim &,
	  const tag::OrientOpposToMesh &, const tag::RequireOrder & ) const
	{	return  this->iterator
			( tag::over_cells_of_max_dim, tag::orientation_opposed_to_mesh, tag::require_order );  }
	inline Mesh::Iterator iterator ( const tag::OverCellsOfMaxDim &,
		const tag::Orientation &, const tag::OpposToMesh &, const tag::RequireOrder & ) const
	{	return  this->iterator
			( tag::over_cells_of_max_dim, tag::orientation_opposed_to_mesh, tag::require_order );  }
	inline Mesh::Iterator iterator ( const tag::Over &, const tag::CellsOfMaxDim &,
	  const tag::Orientation &, const tag::OpposToMesh &, const tag::RequireOrder & ) const
	{	return  this->iterator
			( tag::over_cells_of_max_dim, tag::orientation_opposed_to_mesh, tag::require_order );  }
	inline Mesh::Iterator iterator ( const tag::OverCellsOfMaxDim &, const tag::Orientation &,
		const tag::OpposedTo &, const tag::MEsh &, const tag::RequireOrder &                    ) const
	{	return  this->iterator
			( tag::over_cells_of_max_dim, tag::orientation_opposed_to_mesh, tag::require_order );  }
	inline Mesh::Iterator iterator ( const tag::Over &, const tag::CellsOfMaxDim &,
		const tag::Orientation &, const tag::OpposedTo &, const tag::MEsh &, const tag::RequireOrder & ) const
	{	return  this->iterator
			( tag::over_cells_of_max_dim, tag::orientation_opposed_to_mesh, tag::require_order );  }

	inline Mesh::Iterator iterator
	( const tag::OverCellsOfMaxDim &, const tag::AsFound &, const tag::Backwards & ) const;
	// for a Fuzzy mesh, print error message (no order)
	inline Mesh::Iterator iterator ( const tag::Over &, const tag::CellsOfMaxDim &,
	                                 const tag::AsFound &, const tag::Backwards &  ) const
	{	return  this->iterator ( tag::over_cells_of_max_dim, tag::as_found, tag::backwards );  }

	inline Mesh::Iterator iterator
	( const tag::OverCellsOfMaxDim &, const tag::ForcePositive &, const tag::Backwards & ) const;
	// for a Fuzzy mesh, print error message (no order)
	inline Mesh::Iterator iterator ( const tag::Over &, const tag::CellsOfMaxDim &,
	                                 const tag::ForcePositive &, const tag::Backwards & ) const
	{	return  this->iterator ( tag::over_cells_of_max_dim, tag::force_positive, tag::backwards );  }

	inline Mesh::Iterator iterator
	( const tag::OverCellsOfMaxDim &, const tag::OrientCompWithMesh &, const tag::Backwards & ) const;
	// for a Fuzzy mesh, print error message (no order)
	inline Mesh::Iterator iterator ( const tag::Over &, const tag::CellsOfMaxDim &,
	                                 const tag::OrientCompWithMesh &, const tag::Backwards & ) const
	{	return  this->iterator
			( tag::over_cells_of_max_dim, tag::orientation_compatible_with_mesh, tag::backwards );  }
	inline Mesh::Iterator iterator ( const tag::OverCellsOfMaxDim &, const tag::Backwards & ) const
	{	return  this->iterator
			( tag::over_cells_of_max_dim, tag::orientation_compatible_with_mesh, tag::backwards );  }
	inline Mesh::Iterator iterator
	( const tag::Over &, const tag::CellsOfMaxDim &, const tag::Backwards & ) const
	{	return  this->iterator
			( tag::over_cells_of_max_dim, tag::orientation_compatible_with_mesh, tag::backwards );  }
	inline Mesh::Iterator iterator ( const tag::OverCellsOfMaxDim &,
		const tag::Orientation &, const tag::CompatWithMesh &, const tag::Backwards & ) const
	{	return  this->iterator
			( tag::over_cells_of_max_dim, tag::orientation_compatible_with_mesh, tag::backwards );  }
	inline Mesh::Iterator iterator ( const tag::Over &, const tag::CellsOfMaxDim &,
	  const tag::Orientation &, const tag::CompatWithMesh &, const tag::Backwards & ) const
	{	return  this->iterator
			( tag::over_cells_of_max_dim, tag::orientation_compatible_with_mesh, tag::backwards );  }
	inline Mesh::Iterator iterator ( const tag::OverCellsOfMaxDim &, const tag::Orientation &,
		const tag::CompatWith &, const tag::MEsh &, const tag::Backwards &                           ) const
	{	return  this->iterator
			( tag::over_cells_of_max_dim, tag::orientation_compatible_with_mesh, tag::backwards );  }
	inline Mesh::Iterator iterator
	( const tag::Over &, const tag::CellsOfMaxDim &, const tag::Orientation &,
	  const tag::CompatWith &, const tag::MEsh &, const tag::Backwards &      ) const
	{	return  this->iterator
			( tag::over_cells_of_max_dim, tag::orientation_compatible_with_mesh, tag::backwards );  }

	inline Mesh::Iterator iterator
	( const tag::OverCellsOfMaxDim &, const tag::OrientOpposToMesh &, const tag::Backwards & ) const;
	// for a Fuzzy mesh, print error message (no order)
	inline Mesh::Iterator iterator
	( const tag::Over &, const tag::CellsOfMaxDim &,
	  const tag::OrientOpposToMesh &, const tag::Backwards & ) const
	{	return  this->iterator
			( tag::over_cells_of_max_dim, tag::orientation_opposed_to_mesh, tag::backwards );  }
	inline Mesh::Iterator iterator ( const tag::OverCellsOfMaxDim &,
		const tag::Orientation &, const tag::OpposToMesh &, const tag::Backwards & ) const
	{	return  this->iterator
			( tag::over_cells_of_max_dim, tag::orientation_opposed_to_mesh, tag::backwards );  }
	inline Mesh::Iterator iterator ( const tag::Over &, const tag::CellsOfMaxDim &,
	  const tag::Orientation &, const tag::OpposToMesh &, const tag::Backwards &                ) const
	{	return  this->iterator
			( tag::over_cells_of_max_dim, tag::orientation_opposed_to_mesh, tag::backwards );  }
	inline Mesh::Iterator iterator ( const tag::OverCellsOfMaxDim &, const tag::Orientation &,
		const tag::OpposedTo &, const tag::MEsh &, const tag::Backwards &                       ) const
	{	return  this->iterator
			( tag::over_cells_of_max_dim, tag::orientation_opposed_to_mesh, tag::backwards );  }
	inline Mesh::Iterator iterator
	( const tag::Over &, const tag::CellsOfMaxDim &, const tag::Orientation &,
	  const tag::OpposedTo &, const tag::MEsh &, const tag::Backwards &       ) const
	{	return  this->iterator
			( tag::over_cells_of_max_dim, tag::orientation_opposed_to_mesh, tag::backwards );  }

	// we are still in class Mesh

	inline Mesh::Iterator iterator
	( const tag::OverCellsOfDim &, const size_t d,
	  const tag::OrientCompWithCenter &, const tag::Around &, const Cell & cll ) const;
	inline Mesh::Iterator iterator
	( const tag::OverCells &, const tag::OfDimension &, const size_t d,
	  const tag::OrientCompWithCenter &, const tag::Around &, const Cell & cll ) const
	{	return iterator ( tag::over_cells_of_dim, d,
		                  tag::orientation_compatible_with_center, tag::around, cll );  }
	inline Mesh::Iterator iterator
	( const tag::OverCellsOfDim &, const size_t d,
	  const tag::Orientation &, const tag::CompatWithCenter &,
	  const tag::Around &, const Cell & cll                ) const
	{	return iterator ( tag::over_cells_of_dim, d,
		                  tag::orientation_compatible_with_center, tag::around, cll );  }
	inline Mesh::Iterator iterator
	( const tag::OverCells &, const tag::OfDimension &, const size_t d,
	  const tag::Orientation &, const tag::CompatWithCenter &,
	  const tag::Around &, const Cell & cll                      ) const
	{	return iterator ( tag::over_cells_of_dim, d,
		                  tag::orientation_compatible_with_center, tag::around, cll );  }
	inline Mesh::Iterator iterator
	( const tag::OverCellsOfDim &, const size_t d,
	  const tag::Orientation &, const tag::CompatWith &, const tag::Center &,
	  const tag::Around &, const Cell & cll                                  ) const
	{	return iterator ( tag::over_cells_of_dim, d,
		                  tag::orientation_compatible_with_center, tag::around, cll );  }
	inline Mesh::Iterator iterator
	( const tag::OverCells &, const tag::OfDimension &, const size_t d,
	  const tag::Orientation &, const tag::CompatWith &, const tag::Center &,
	  const tag::Around &, const Cell & cll                                  ) const
	{	return iterator ( tag::over_cells_of_dim, d,
		                  tag::orientation_compatible_with_center, tag::around, cll );  }
	
	// we are still in class Mesh

	inline Mesh::Iterator iterator
	( const tag::OverCellsOfDim &, const size_t d,
	  const tag::OrientOpposToCenter &, const tag::Around &, const Cell & cll ) const;
	inline Mesh::Iterator iterator
	( const tag::OverCells &, const tag::OfDimension &, const size_t d,
	  const tag::OrientOpposToCenter &, const tag::Around &, const Cell & cll ) const
	{	return iterator ( tag::over_cells_of_dim, d,
		                  tag::orientation_opposed_to_center, tag::around, cll );  }
	inline Mesh::Iterator iterator
	( const tag::OverCellsOfDim &, const size_t d,
	  const tag::Orientation &, const tag::OpposToCenter &,
	  const tag::Around &, const Cell & cll             ) const
	{	return iterator ( tag::over_cells_of_dim, d,
		                  tag::orientation_opposed_to_center, tag::around, cll );  }
	inline Mesh::Iterator iterator
	( const tag::OverCells &, const tag::OfDimension &, const size_t d,
	  const tag::Orientation &, const tag::OpposToCenter &,
	  const tag::Around &, const Cell & cll                      ) const
	{	return iterator ( tag::over_cells_of_dim, d,
		                  tag::orientation_opposed_to_center, tag::around, cll );  }
	inline Mesh::Iterator iterator
	( const tag::OverCellsOfDim &, const size_t d,
	  const tag::Orientation &, const tag::OpposedTo &, const tag::Center &,
	  const tag::Around &, const Cell & cll                                 ) const
	{	return iterator ( tag::over_cells_of_dim, d,
		                  tag::orientation_opposed_to_center, tag::around, cll );  }
	inline Mesh::Iterator iterator
	( const tag::OverCells &, const tag::OfDimension &, const size_t d,
	  const tag::Orientation &, const tag::OpposedTo &, const tag::Center &,
	  const tag::Around &, const Cell & cll                                 ) const
	{	return iterator ( tag::over_cells_of_dim, d,
		                  tag::orientation_opposed_to_center, tag::around, cll );  }
	
	// we are still in class Mesh
	
	inline Mesh::Iterator iterator
	( const tag::OverCellsOfDim &, const size_t d,
	  const tag::OrientCompWithMesh &, const tag::Around &, const Cell & cll ) const;
	inline Mesh::Iterator iterator
	( const tag::OverCells &, const tag::OfDimension &, const size_t d,
	  const tag::OrientCompWithMesh &, const tag::Around &, const Cell & cll ) const
	{	return iterator ( tag::over_cells_of_dim, d,
		                  tag::orientation_compatible_with_mesh, tag::around, cll );  }
	inline Mesh::Iterator iterator
	( const tag::OverCellsOfDim &, const size_t d,
	  const tag::Orientation &, const tag::CompatWithMesh &,
	  const tag::Around &, const Cell & cll                 ) const
	{	return iterator ( tag::over_cells_of_dim, d,
		                  tag::orientation_compatible_with_mesh, tag::around, cll );  }
	inline Mesh::Iterator iterator
	( const tag::OverCells &, const tag::OfDimension &, const size_t d,
	  const tag::Orientation &, const tag::CompatWithMesh &,
	  const tag::Around &, const Cell & cll                      ) const
	{	return iterator ( tag::over_cells_of_dim, d,
		                  tag::orientation_compatible_with_mesh, tag::around, cll );  }
	inline Mesh::Iterator iterator
	( const tag::OverCellsOfDim &, const size_t d,
	  const tag::Orientation &, const tag::CompatWith &, const tag::MEsh &,
	  const tag::Around &, const Cell & cll                                ) const
	{	return iterator ( tag::over_cells_of_dim, d,
		                  tag::orientation_compatible_with_mesh, tag::around, cll );  }
	inline Mesh::Iterator iterator
	( const tag::OverCells &, const tag::OfDimension &, const size_t d,
	  const tag::Orientation &, const tag::CompatWith &, const tag::MEsh &,
	  const tag::Around &, const Cell & cll                                ) const
	{	return iterator ( tag::over_cells_of_dim, d,
		                  tag::orientation_compatible_with_mesh, tag::around, cll );  }

	inline Mesh::Iterator iterator
	( const tag::OverCellsOfDim &, const size_t d,
	  const tag::OrientOpposToMesh &, const tag::Around &, const Cell & cll ) const;
	inline Mesh::Iterator iterator
	( const tag::OverCells &, const tag::OfDimension &, const size_t d,
	  const tag::OrientOpposToMesh &, const tag::Around &, const Cell & cll ) const
	{	return iterator ( tag::over_cells_of_dim, d,
		                  tag::orientation_opposed_to_mesh, tag::around, cll );  }
	inline Mesh::Iterator iterator
	( const tag::OverCellsOfDim &, const size_t d,
	  const tag::Orientation &, const tag::OpposToMesh &,
	  const tag::Around &, const Cell & cll              ) const
	{	return iterator ( tag::over_cells_of_dim, d,
		                  tag::orientation_opposed_to_mesh, tag::around, cll );  }
	inline Mesh::Iterator iterator
	( const tag::OverCells &, const tag::OfDimension &, const size_t d,
	  const tag::Orientation &, const tag::OpposToMesh &,
	  const tag::Around &, const Cell & cll              ) const
	{	return iterator ( tag::over_cells_of_dim, d,
		                  tag::orientation_opposed_to_mesh, tag::around, cll );  }
	inline Mesh::Iterator iterator
	( const tag::OverCellsOfDim &, const size_t d,
	  const tag::Orientation &, const tag::OpposedTo &, const tag::MEsh &,
	  const tag::Around &, const Cell & cll                               ) const
	{	return iterator ( tag::over_cells_of_dim, d,
		                  tag::orientation_opposed_to_mesh, tag::around, cll );  }
	inline Mesh::Iterator iterator
	( const tag::OverCells &, const tag::OfDimension &, const size_t d,
	  const tag::Orientation &, const tag::OpposedTo &, const tag::MEsh &,
	  const tag::Around &, const Cell & cll                               ) const
	{	return iterator ( tag::over_cells_of_dim, d,
		                  tag::orientation_opposed_to_mesh, tag::around, cll );  }

	inline Mesh::Iterator iterator
	( const tag::OverCellsOfDim &, const size_t d, const tag::AsFound &,
	  const tag::Around &, const Cell & cll                             ) const;
	inline Mesh::Iterator iterator
	( const tag::OverCells &, const tag::OfDimension &, const size_t d,
	  const tag::AsFound &, const tag::Around &, const Cell & cll ) const
	{	return iterator ( tag::over_cells_of_dim, d, tag::as_found, tag::around, cll );  }

	// AsFound if dim < dim mesh, CompatWithMesh if dim == dim_mesh
	inline Mesh::Iterator iterator
	( const tag::OverCellsOfDim &, const size_t d, const tag::Around &, const Cell & cll ) const;
	inline Mesh::Iterator iterator
	( const tag::OverCells &, const tag::OfDimension &, const size_t d,
	  const tag::Around &, const Cell & cll                            ) const
	{	return iterator ( tag::over_cells_of_dim, d, tag::around, cll );  }

	inline Mesh::Iterator iterator
	( const tag::OverCellsOfDim &, const size_t d, const tag::ForcePositive &,
	  const tag::Around &, const Cell & cll                                   ) const;
	inline Mesh::Iterator iterator
	( const tag::OverCells &, const tag::OfDimension &, const size_t d,
	  const tag::ForcePositive &, const tag::Around &, const Cell & cll ) const
	{	return iterator ( tag::over_cells_of_dim, d, tag::force_positive, tag::around, cll );  }

	// we are still in class Mesh

	inline Mesh::Iterator iterator
	( const tag::OverCellsOfDim &, const size_t d, const tag::OrientCompWithCenter &,
	  const tag::Around &, const Cell & cll, const tag::RequireOrder &               ) const;
	inline Mesh::Iterator iterator
	( const tag::OverCells &, const tag::OfDimension &, const size_t d, const tag::OrientCompWithCenter &,
	  const tag::Around &, const Cell & cll, const tag::RequireOrder &                        ) const
	{	return iterator ( tag::over_cells_of_dim, d, tag::orientation_compatible_with_center,
		                  tag::around, cll, tag::require_order                               );  }
	inline Mesh::Iterator iterator
	( const tag::OverCellsOfDim &, const size_t d,
	  const tag::Orientation &, const tag::CompatWithCenter &,
	  const tag::Around &, const Cell & cll, const tag::RequireOrder & ) const
	{	return iterator ( tag::over_cells_of_dim, d, tag::orientation_compatible_with_center,
		                  tag::around, cll, tag::require_order                               );  }
	inline Mesh::Iterator iterator
	( const tag::OverCells &, const tag::OfDimension &, const size_t d,
	  const tag::Orientation &, const tag::CompatWithCenter &,
	  const tag::Around &, const Cell & cll, const tag::RequireOrder & ) const
	{	return iterator ( tag::over_cells_of_dim, d, tag::orientation_compatible_with_center,
		                  tag::around, cll, tag::require_order                               );  }
	inline Mesh::Iterator iterator
	( const tag::OverCellsOfDim &, const size_t d,
	  const tag::Orientation &, const tag::CompatWith &, const tag::Center &,
	  const tag::Around &, const Cell & cll, const tag::RequireOrder &       ) const
	{	return iterator ( tag::over_cells_of_dim, d, tag::orientation_compatible_with_center,
		                  tag::around, cll, tag::require_order                               );  }
	inline Mesh::Iterator iterator
	( const tag::OverCells &, const tag::OfDimension &, const size_t d,
	  const tag::Orientation &, const tag::CompatWith &, const tag::Center &,
	  const tag::Around &, const Cell & cll, const tag::RequireOrder &       ) const
	{	return iterator ( tag::over_cells_of_dim, d, tag::orientation_compatible_with_center,
		                  tag::around, cll, tag::require_order                               );  }
	
	// we are still in class Mesh

	inline Mesh::Iterator iterator
	( const tag::OverCellsOfDim &, const size_t d, const tag::OrientOpposToCenter &,
	  const tag::Around &, const Cell & cll, const tag::RequireOrder &           ) const;
	inline Mesh::Iterator iterator
	( const tag::OverCells &, const tag::OfDimension &, const size_t d, const tag::OrientOpposToCenter &,
	  const tag::Around &, const Cell & cll, const tag::RequireOrder &                        ) const
	{	return iterator ( tag::over_cells_of_dim, d, tag::orientation_opposed_to_center,
		                  tag::around, cll, tag::require_order                          );  }
	inline Mesh::Iterator iterator
	( const tag::OverCellsOfDim &, const size_t d,
	  const tag::Orientation &, const tag::OpposToCenter &,
	  const tag::Around &, const Cell & cll, const tag::RequireOrder & ) const
	{	return iterator ( tag::over_cells_of_dim, d, tag::orientation_opposed_to_center,
		                  tag::around, cll, tag::require_order                          );  }
	inline Mesh::Iterator iterator
	( const tag::OverCells &, const tag::OfDimension &, const size_t d,
	  const tag::Orientation &, const tag::OpposToCenter &,
	  const tag::Around &, const Cell & cll, const tag::RequireOrder & ) const
	{	return iterator ( tag::over_cells_of_dim, d, tag::orientation_opposed_to_center,
		                  tag::around, cll, tag::require_order                          );  }
	inline Mesh::Iterator iterator
	( const tag::OverCellsOfDim &, const size_t d,
	  const tag::Orientation &, const tag::OpposedTo &, const tag::Center &,
	  const tag::Around &, const Cell & cll, const tag::RequireOrder &      ) const
	{	return iterator ( tag::over_cells_of_dim, d, tag::orientation_opposed_to_center,
		                  tag::around, cll, tag::require_order                          );  }
	inline Mesh::Iterator iterator
	( const tag::OverCells &, const tag::OfDimension &, const size_t d,
	  const tag::Orientation &, const tag::OpposedTo &, const tag::Center &,
	  const tag::Around &, const Cell & cll, const tag::RequireOrder &      ) const
	{	return iterator ( tag::over_cells_of_dim, d, tag::orientation_opposed_to_center,
		                  tag::around, cll, tag::require_order                          );  }
	
	// we are still in class Mesh
	
	inline Mesh::Iterator iterator
	( const tag::OverCellsOfDim &, const size_t d, const tag::OrientCompWithMesh &,
	  const tag::Around &, const Cell & cll, const tag::RequireOrder &           ) const;
	inline Mesh::Iterator iterator
	( const tag::OverCells &, const tag::OfDimension &, const size_t d, const tag::OrientCompWithMesh &,
		const tag::Around &, const Cell & cll, const tag::RequireOrder &                          ) const
	{	return iterator ( tag::over_cells_of_dim, d, tag::orientation_compatible_with_mesh,
		                  tag::around, cll, tag::require_order                             );  }
	inline Mesh::Iterator iterator
	( const tag::OverCellsOfDim &, const size_t d,
	  const tag::Orientation &, const tag::CompatWithMesh &,
	  const tag::Around &, const Cell & cll, const tag::RequireOrder & ) const
	{	return iterator ( tag::over_cells_of_dim, d, tag::orientation_compatible_with_mesh,
		                  tag::around, cll, tag::require_order                             );  }
	inline Mesh::Iterator iterator
	( const tag::OverCells &, const tag::OfDimension &, const size_t d,
	  const tag::Orientation &, const tag::CompatWithMesh &,
	  const tag::Around &, const Cell & cll, const tag::RequireOrder & ) const
	{	return iterator ( tag::over_cells_of_dim, d, tag::orientation_compatible_with_mesh,
		                  tag::around, cll, tag::require_order                             );  }
	inline Mesh::Iterator iterator
	( const tag::OverCellsOfDim &, const size_t d,
	  const tag::Orientation &, const tag::CompatWith &, const tag::MEsh &,
	  const tag::Around &, const Cell & cll, const tag::RequireOrder &     ) const
	{	return iterator ( tag::over_cells_of_dim, d, tag::orientation_compatible_with_mesh,
		                  tag::around, cll, tag::require_order                             );  }
	inline Mesh::Iterator iterator
	( const tag::OverCells &, const tag::OfDimension &, const size_t d,
	  const tag::Orientation &, const tag::CompatWith &, const tag::MEsh &,
	  const tag::Around &, const Cell & cll, const tag::RequireOrder &     ) const
	{	return iterator ( tag::over_cells_of_dim, d, tag::orientation_compatible_with_mesh,
		                  tag::around, cll, tag::require_order                             );  }

	// we are still in class Mesh

	inline Mesh::Iterator iterator
	( const tag::OverCellsOfDim &, const size_t d, const tag::AsFound &,
	  const tag::Around &, const Cell & cll, const tag::RequireOrder &  ) const;
	inline Mesh::Iterator iterator
	( const tag::OverCells &, const tag::OfDimension &, const size_t d,
	  const tag::AsFound &, const tag::Around &, const Cell & cll, const tag::RequireOrder & ) const
	{	return iterator ( tag::over_cells_of_dim, d, tag::as_found, tag::around, cll, tag::require_order );  }

	// AsFound if dim < dim mesh, CompatWithMesh if dim == dim_mesh
	inline Mesh::Iterator iterator
	( const tag::OverCellsOfDim &, const size_t d,
	  const tag::Around &, const Cell & cll, const tag::RequireOrder & ) const;
	inline Mesh::Iterator iterator
	( const tag::OverCells &, const tag::OfDimension &, const size_t d,
	  const tag::Around &, const Cell & cll, const tag::RequireOrder & ) const
	{	return iterator ( tag::over_cells_of_dim, d, tag::around, cll, tag::require_order );  }

	inline Mesh::Iterator iterator
	( const tag::OverCellsOfDim &, const size_t d, const tag::ForcePositive &,
	  const tag::Around &, const Cell & cll, const tag::RequireOrder &        ) const;
	inline Mesh::Iterator iterator
	( const tag::OverCells &, const tag::OfDimension &, const size_t d, const tag::ForcePositive &,
	  const tag::Around &, const Cell & cll, const tag::RequireOrder &                             ) const
	{	return iterator ( tag::over_cells_of_dim, d, tag::force_positive,
		                  tag::around, cll, tag::require_order           );  }

	// we are still in class Mesh

	inline Mesh::Iterator iterator
	( const tag::OverCellsOfDim &, const size_t d, const tag::OrientCompWithCenter &,
	  const tag::Around &, const Cell & cll, const tag::Backwards &               ) const;
	inline Mesh::Iterator iterator
	( const tag::OverCells &, const tag::OfDimension &, const size_t d, const tag::OrientCompWithCenter &,
	  const tag::Around &, const Cell & cll, const tag::Backwards &                              ) const
	{	return iterator ( tag::over_cells_of_dim, d, tag::orientation_compatible_with_center,
		                  tag::around, cll, tag::backwards                                   );  }
	inline Mesh::Iterator iterator
	( const tag::OverCellsOfDim &, const size_t d,
	  const tag::Orientation &, const tag::CompatWithCenter &,
	  const tag::Around &, const Cell & cll, const tag::Backwards & ) const
	{	return iterator ( tag::over_cells_of_dim, d, tag::orientation_compatible_with_center,
		                  tag::around, cll, tag::backwards                                   );  }
	inline Mesh::Iterator iterator
	( const tag::OverCells &, const tag::OfDimension &, const size_t d,
	  const tag::Orientation &, const tag::CompatWithCenter &,
	  const tag::Around &, const Cell & cll, const tag::Backwards & ) const
	{	return iterator ( tag::over_cells_of_dim, d, tag::orientation_compatible_with_center,
		                  tag::around, cll, tag::backwards                                   );  }
	inline Mesh::Iterator iterator
	( const tag::OverCellsOfDim &, const size_t d,
	  const tag::Orientation &, const tag::CompatWith &, const tag::Center &,
	  const tag::Around &, const Cell & cll, const tag::Backwards &       ) const
	{	return iterator ( tag::over_cells_of_dim, d, tag::orientation_compatible_with_center,
		                  tag::around, cll, tag::backwards                                   );  }
	inline Mesh::Iterator iterator
	( const tag::OverCells &, const tag::OfDimension &, const size_t d,
	  const tag::Orientation &, const tag::CompatWith &, const tag::Center &,
	  const tag::Around &, const Cell & cll, const tag::Backwards &       ) const
	{	return iterator ( tag::over_cells_of_dim, d, tag::orientation_compatible_with_center,
		                  tag::around, cll, tag::backwards                                   );  }
	
	// we are still in class Mesh

	inline Mesh::Iterator iterator
	( const tag::OverCellsOfDim &, const size_t d, const tag::OrientOpposToCenter &,
	  const tag::Around &, const Cell & cll, const tag::Backwards &              ) const;
	inline Mesh::Iterator iterator
	( const tag::OverCells &, const tag::OfDimension &, const size_t d, const tag::OrientOpposToCenter &,
	  const tag::Around &, const Cell & cll, const tag::Backwards &                        ) const
	{	return iterator ( tag::over_cells_of_dim, d, tag::orientation_opposed_to_center,
		                  tag::around, cll, tag::backwards                              );  }
	inline Mesh::Iterator iterator
	( const tag::OverCellsOfDim &, const size_t d,
	  const tag::Orientation &, const tag::OpposToCenter &,
	  const tag::Around &, const Cell & cll, const tag::Backwards & ) const
	{	return iterator ( tag::over_cells_of_dim, d, tag::orientation_opposed_to_center,
		                  tag::around, cll, tag::backwards                              );  }
	inline Mesh::Iterator iterator
	( const tag::OverCells &, const tag::OfDimension &, const size_t d,
	  const tag::Orientation &, const tag::OpposToCenter &,
	  const tag::Around &, const Cell & cll, const tag::Backwards & ) const
	{	return iterator ( tag::over_cells_of_dim, d, tag::orientation_opposed_to_center,
		                  tag::around, cll, tag::backwards                              );  }
	inline Mesh::Iterator iterator
	( const tag::OverCellsOfDim &, const size_t d,
	  const tag::Orientation &, const tag::OpposedTo &, const tag::Center &,
	  const tag::Around &, const Cell & cll, const tag::Backwards &      ) const
	{	return iterator ( tag::over_cells_of_dim, d, tag::orientation_opposed_to_center,
		                  tag::around, cll, tag::backwards                              );  }
	inline Mesh::Iterator iterator
	( const tag::OverCells &, const tag::OfDimension &, const size_t d,
	  const tag::Orientation &, const tag::OpposedTo &, const tag::Center &,
	  const tag::Around &, const Cell & cll, const tag::Backwards &      ) const
	{	return iterator ( tag::over_cells_of_dim, d, tag::orientation_opposed_to_center,
		                  tag::around, cll, tag::backwards                              );  }
	
	// we are still in class Mesh
	
	inline Mesh::Iterator iterator
	( const tag::OverCellsOfDim &, const size_t d, const tag::OrientCompWithMesh &,
	  const tag::Around &, const Cell & cll, const tag::Backwards &                ) const;
	inline Mesh::Iterator iterator
	( const tag::OverCells &, const tag::OfDimension &, const size_t d, const tag::OrientCompWithMesh &,
		const tag::Around &, const Cell & cll, const tag::Backwards &                             ) const
	{	return iterator ( tag::over_cells_of_dim, d, tag::orientation_compatible_with_mesh,
		                  tag::around, cll, tag::backwards                                 );  }
	inline Mesh::Iterator iterator
	( const tag::OverCellsOfDim &, const size_t d,
	  const tag::Orientation &, const tag::CompatWithMesh &,
	  const tag::Around &, const Cell & cll, const tag::Backwards & ) const
	{	return iterator ( tag::over_cells_of_dim, d, tag::orientation_compatible_with_mesh,
		                  tag::around, cll, tag::backwards                                 );  }
	inline Mesh::Iterator iterator
	( const tag::OverCells &, const tag::OfDimension &, const size_t d,
	  const tag::Orientation &, const tag::CompatWithMesh &,
	  const tag::Around &, const Cell & cll, const tag::Backwards &    ) const
	{	return iterator ( tag::over_cells_of_dim, d, tag::orientation_compatible_with_mesh,
		                  tag::around, cll, tag::backwards                                 );  }
	inline Mesh::Iterator iterator
	( const tag::OverCellsOfDim &, const size_t d,
	  const tag::Orientation &, const tag::CompatWith &, const tag::MEsh &,
	  const tag::Around &, const Cell & cll, const tag::Backwards &        ) const
	{	return iterator ( tag::over_cells_of_dim, d, tag::orientation_compatible_with_mesh,
		                  tag::around, cll, tag::backwards                                 );  }
	inline Mesh::Iterator iterator
	( const tag::OverCells &, const tag::OfDimension &, const size_t d,
	  const tag::Orientation &, const tag::CompatWith &, const tag::MEsh &,
	  const tag::Around &, const Cell & cll, const tag::Backwards &        ) const
	{	return iterator ( tag::over_cells_of_dim, d, tag::orientation_compatible_with_mesh,
		                  tag::around, cll, tag::backwards                                 );  }
	
	// we are still in class Mesh

	inline Mesh::Iterator iterator
	( const tag::OverCellsOfDim &, const size_t d, const tag::AsFound &,
	  const tag::Around &, const Cell & cll, const tag::Backwards &     ) const;
	inline Mesh::Iterator iterator
	( const tag::OverCells &, const tag::OfDimension &, const size_t d,
	  const tag::AsFound &, const tag::Around &, const Cell & cll, const tag::Backwards & ) const
	{	return iterator ( tag::over_cells_of_dim, d, tag::as_found, tag::around, cll, tag::backwards );  }

	// AsFound if dim < dim mesh, CompatWithMesh if dim == dim_mesh
	inline Mesh::Iterator iterator
	( const tag::OverCellsOfDim &, const size_t d,
	  const tag::Around &, const Cell & cll, const tag::Backwards & ) const;
	inline Mesh::Iterator iterator
	( const tag::OverCells &, const tag::OfDimension &, const size_t d,
	  const tag::Around &, const Cell & cll, const tag::Backwards &    ) const
	{	return iterator ( tag::over_cells_of_dim, d, tag::around, cll, tag::backwards );  }

	inline Mesh::Iterator iterator
	( const tag::OverCellsOfDim &, const size_t d, const tag::ForcePositive &,
	  const tag::Around &, const Cell & cll, const tag::Backwards &           ) const;
	inline Mesh::Iterator iterator
	( const tag::OverCells &, const tag::OfDimension &, const size_t d, const tag::ForcePositive &,
	  const tag::Around &, const Cell & cll, const tag::Backwards &                          ) const
	{	return iterator ( tag::over_cells_of_dim, d, tag::force_positive,
		                  tag::around, cll, tag::backwards               );  }

	// we are still in class Mesh

	inline Mesh::Iterator iterator
	( const tag::OverCellsOfMaxDim &, const tag::OrientCompWithCenter &,
	  const tag::Around &, const Cell & cll                          ) const;
	inline Mesh::Iterator iterator
	( const tag::OverCells &, const tag::OfMaxDim &, const tag::OrientCompWithCenter &,
	  const tag::Around &, const Cell & cll                                 ) const
	{	return iterator ( tag::over_cells_of_max_dim,
		                  tag::orientation_compatible_with_center, tag::around, cll );  }
	inline Mesh::Iterator iterator
	( const tag::OverCellsOfMaxDim &, const tag::Orientation &, const tag::CompatWithCenter &,
	  const tag::Around &, const Cell & cll                                                ) const
	{	return iterator ( tag::over_cells_of_max_dim,
		                  tag::orientation_compatible_with_center, tag::around, cll );  }
	inline Mesh::Iterator iterator
	( const tag::OverCells &, const tag::OfMaxDim &,
	  const tag::Orientation &, const tag::CompatWithCenter &,
	  const tag::Around &, const Cell & cll                ) const
	{	return iterator ( tag::over_cells_of_max_dim,
		                  tag::orientation_compatible_with_center, tag::around, cll );  }
	inline Mesh::Iterator iterator
	( const tag::OverCellsOfMaxDim &, const tag::Orientation &, const tag::CompatWith &,
	  const tag::Center &, const tag::Around &, const Cell & cll                        ) const
	{	return iterator ( tag::over_cells_of_max_dim,
		                  tag::orientation_compatible_with_center, tag::around, cll );  }
	inline Mesh::Iterator iterator
	( const tag::OverCells &, const tag::OfMaxDim &, const tag::Orientation &,
	  const tag::CompatWith &, const tag::Center &, const tag::Around &, const Cell & cll ) const
	{	return iterator ( tag::over_cells_of_max_dim,
		                  tag::orientation_compatible_with_center, tag::around, cll );  }
	
	// we are still in class Mesh

	inline Mesh::Iterator iterator
	( const tag::OverCellsOfMaxDim &,
	  const tag::OrientOpposToCenter &, const tag::Around &, const Cell & cll ) const;
	inline Mesh::Iterator iterator
	( const tag::OverCells &, const tag::OfMaxDim &,
	  const tag::OrientOpposToCenter &, const tag::Around &, const Cell & cll ) const
	{	return iterator ( tag::over_cells_of_max_dim,
		                  tag::orientation_opposed_to_center, tag::around, cll );  }
	inline Mesh::Iterator iterator
	( const tag::OverCellsOfMaxDim &, const tag::Orientation &, const tag::OpposToCenter &,
	  const tag::Around &, const Cell & cll                                             ) const
	{	return iterator ( tag::over_cells_of_max_dim,
		                  tag::orientation_opposed_to_center, tag::around, cll );  }
	inline Mesh::Iterator iterator
	( const tag::OverCells &, const tag::OfMaxDim &,
	  const tag::Orientation &, const tag::OpposToCenter &,
	  const tag::Around &, const Cell & cll             ) const
	{	return iterator ( tag::over_cells_of_max_dim,
		                  tag::orientation_opposed_to_center, tag::around, cll );  }
	inline Mesh::Iterator iterator
	( const tag::OverCellsOfMaxDim &, const tag::Orientation &, const tag::OpposedTo &,
	  const tag::Center &, const tag::Around &, const Cell & cll                       ) const
	{	return iterator ( tag::over_cells_of_max_dim,
		                  tag::orientation_opposed_to_center, tag::around, cll );  }
	inline Mesh::Iterator iterator
	( const tag::OverCells &, const tag::OfMaxDim &, const tag::Orientation &,
	  const tag::OpposedTo &, const tag::Center &, const tag::Around &, const Cell & cll ) const
	{	return iterator ( tag::over_cells_of_max_dim,
		                  tag::orientation_opposed_to_center, tag::around, cll );  }
	
	// we are still in class Mesh
	
	inline Mesh::Iterator iterator
	( const tag::OverCellsOfMaxDim &,
	  const tag::OrientCompWithMesh &, const tag::Around &, const Cell & cll ) const;
	inline Mesh::Iterator iterator
	( const tag::OverCells &, const tag::OfMaxDim &,
	  const tag::OrientCompWithMesh &, const tag::Around &, const Cell & cll ) const
	{	return iterator ( tag::over_cells_of_max_dim,
		                  tag::orientation_compatible_with_mesh, tag::around, cll );  }
	inline Mesh::Iterator iterator
	( const tag::OverCellsOfMaxDim &, const tag::Orientation &, const tag::CompatWithMesh &,
	  const tag::Around &, const Cell & cll                                                 ) const
	{	return iterator ( tag::over_cells_of_max_dim,
		                  tag::orientation_compatible_with_mesh, tag::around, cll );  }
	inline Mesh::Iterator iterator
	( const tag::OverCells &, const tag::OfMaxDim &, const tag::Orientation &,
	  const tag::CompatWithMesh &, const tag::Around &, const Cell & cll      ) const
	{	return iterator ( tag::over_cells_of_max_dim,
		                  tag::orientation_compatible_with_mesh, tag::around, cll );  }
	inline Mesh::Iterator iterator
	( const tag::OverCellsOfMaxDim &,
	  const tag::Orientation &, const tag::CompatWith &, const tag::MEsh &,
	  const tag::Around &, const Cell & cll                                ) const
	{	return iterator ( tag::over_cells_of_max_dim,
		                  tag::orientation_compatible_with_mesh, tag::around, cll );  }
	inline Mesh::Iterator iterator
	( const tag::OverCells &, const tag::OfMaxDim &, const tag::Orientation &,
	  const tag::CompatWith &, const tag::MEsh &, const tag::Around &, const Cell & cll ) const
	{	return iterator ( tag::over_cells_of_max_dim,
		                  tag::orientation_compatible_with_mesh, tag::around, cll );  }
	
	inline Mesh::Iterator iterator
	( const tag::OverCellsOfMaxDim &,
	  const tag::OrientOpposToMesh &, const tag::Around &, const Cell & cll ) const;
	inline Mesh::Iterator iterator
	( const tag::OverCells &, const tag::OfMaxDim &,
	  const tag::OrientOpposToMesh &, const tag::Around &, const Cell & cll ) const
	{	return iterator ( tag::over_cells_of_max_dim,
		                  tag::orientation_opposed_to_mesh, tag::around, cll );  }
	inline Mesh::Iterator iterator
	( const tag::OverCellsOfMaxDim &, const tag::Orientation &, const tag::OpposToMesh &,
	  const tag::Around &, const Cell & cll                                              ) const
	{	return iterator ( tag::over_cells_of_max_dim,
		                  tag::orientation_opposed_to_mesh, tag::around, cll );  }
	inline Mesh::Iterator iterator
	( const tag::OverCells &, const tag::OfMaxDim &, const tag::Orientation &,
	  const tag::OpposToMesh &, const tag::Around &, const Cell & cll         ) const
	{	return iterator ( tag::over_cells_of_max_dim,
		                  tag::orientation_opposed_to_mesh, tag::around, cll );  }
	inline Mesh::Iterator iterator
	( const tag::OverCellsOfMaxDim &,
	  const tag::Orientation &, const tag::OpposedTo &, const tag::MEsh &,
	  const tag::Around &, const Cell & cll                               ) const
	{	return iterator ( tag::over_cells_of_max_dim,
		                  tag::orientation_opposed_to_mesh, tag::around, cll );  }
	inline Mesh::Iterator iterator
	( const tag::OverCells &, const tag::OfMaxDim &, const tag::Orientation &,
	  const tag::OpposedTo &, const tag::MEsh &, const tag::Around &, const Cell & cll ) const
	{	return iterator ( tag::over_cells_of_max_dim,
		                  tag::orientation_opposed_to_mesh, tag::around, cll );  }
	
	// we are still in class Mesh

	inline Mesh::Iterator iterator
	( const tag::OverCellsOfMaxDim &, const tag::AsFound &, const tag::Around &, const Cell & cll ) const;
	inline Mesh::Iterator iterator
	( const tag::OverCells &, const tag::OfMaxDim &,
	  const tag::AsFound &, const tag::Around &, const Cell & cll ) const
	{	return iterator ( tag::over_cells_of_max_dim, tag::as_found, tag::around, cll );  }
	
	inline Mesh::Iterator iterator
	( const tag::OverCellsOfMaxDim &, const tag::Around &, const Cell & cll ) const
	{	return iterator ( tag::over_cells_of_max_dim, tag::as_found, tag::around, cll );  }
	inline Mesh::Iterator iterator
	( const tag::OverCells &, const tag::OfMaxDim &, const tag::Around &, const Cell & cll ) const
	{	return iterator ( tag::over_cells_of_max_dim, tag::as_found, tag::around, cll );  }

	// we are still in class Mesh

	inline Mesh::Iterator iterator
	( const tag::OverCellsOfMaxDim &, const tag::OrientCompWithCenter &,
	  const tag::Around &, const Cell & cll, const tag::RequireOrder &  ) const;
	inline Mesh::Iterator iterator
	( const tag::OverCells &, const tag::OfMaxDim &, const tag::OrientCompWithCenter &,
	  const tag::Around &, const Cell & cll, const tag::RequireOrder &                 ) const
	{	return iterator ( tag::over_cells_of_max_dim, tag::orientation_compatible_with_center,
		                  tag::around, cll, tag::require_order                                );  }
	inline Mesh::Iterator iterator
	( const tag::OverCellsOfMaxDim &, const tag::Orientation &, const tag::CompatWithCenter &,
	  const tag::Around &, const Cell & cll, const tag::RequireOrder &                        ) const
	{	return iterator ( tag::over_cells_of_max_dim, tag::orientation_compatible_with_center,
		                  tag::around, cll, tag::require_order                                );  }
	inline Mesh::Iterator iterator
	( const tag::OverCells &, const tag::OfMaxDim &,
	  const tag::Orientation &, const tag::CompatWithCenter &,
	  const tag::Around &, const Cell & cll, const tag::RequireOrder & ) const
	{	return iterator ( tag::over_cells_of_max_dim, tag::orientation_compatible_with_center,
		                  tag::around, cll, tag::require_order                                );  }
	inline Mesh::Iterator iterator
	( const tag::OverCellsOfMaxDim &,
	  const tag::Orientation &, const tag::CompatWith &, const tag::Center &,
	  const tag::Around &, const Cell & cll, const tag::RequireOrder &       ) const
	{	return iterator ( tag::over_cells_of_max_dim, tag::orientation_compatible_with_center,
		                  tag::around, cll, tag::require_order                                );  }
	inline Mesh::Iterator iterator
	( const tag::OverCells &, const tag::OfMaxDim &,
	  const tag::Orientation &, const tag::CompatWith &, const tag::Center &,
	  const tag::Around &, const Cell & cll, const tag::RequireOrder &       ) const
	{	return iterator ( tag::over_cells_of_max_dim, tag::orientation_compatible_with_center,
		                  tag::around, cll, tag::require_order                                );  }
	
	// we are still in class Mesh

	inline Mesh::Iterator iterator
	( const tag::OverCellsOfMaxDim &, const tag::OrientOpposToCenter &,
	  const tag::Around &, const Cell & cll, const tag::RequireOrder & ) const;
	inline Mesh::Iterator iterator
	( const tag::OverCells &, const tag::OfMaxDim &, const tag::OrientOpposToCenter &,
	  const tag::Around &, const Cell & cll, const tag::RequireOrder &             ) const
	{	return iterator ( tag::over_cells_of_max_dim, tag::orientation_opposed_to_center,
		                  tag::around, cll, tag::require_order                           );  }
	inline Mesh::Iterator iterator
	( const tag::OverCellsOfMaxDim &, const tag::Orientation &, const tag::OpposToCenter &,
	  const tag::Around &, const Cell & cll, const tag::RequireOrder &                  ) const
	{	return iterator ( tag::over_cells_of_max_dim, tag::orientation_opposed_to_center,
		                  tag::around, cll, tag::require_order                           );  }
	inline Mesh::Iterator iterator
	( const tag::OverCells &, const tag::OfMaxDim &,
	  const tag::Orientation &, const tag::OpposToCenter &,
	  const tag::Around &, const Cell & cll, const tag::RequireOrder & ) const
	{	return iterator ( tag::over_cells_of_max_dim, tag::orientation_opposed_to_center,
		                  tag::around, cll, tag::require_order                           );  }
	inline Mesh::Iterator iterator
	( const tag::OverCellsOfMaxDim &,
	  const tag::Orientation &, const tag::OpposedTo &, const tag::Center &,
	  const tag::Around &, const Cell & cll, const tag::RequireOrder &      ) const
	{	return iterator ( tag::over_cells_of_max_dim, tag::orientation_opposed_to_center,
		                  tag::around, cll, tag::require_order                           );  }
	inline Mesh::Iterator iterator
	( const tag::OverCells &, const tag::OfMaxDim &,
	  const tag::Orientation &, const tag::OpposedTo &, const tag::Center &,
	  const tag::Around &, const Cell & cll, const tag::RequireOrder &      ) const
	{	return iterator ( tag::over_cells_of_max_dim, tag::orientation_opposed_to_center,
		                  tag::around, cll, tag::require_order                           );  }
	
	// we are still in class Mesh
	
	inline Mesh::Iterator iterator
	( const tag::OverCellsOfMaxDim &, const tag::OrientCompWithMesh &,
	  const tag::Around &, const Cell & cll, const tag::RequireOrder & ) const;
	inline Mesh::Iterator iterator
	( const tag::OverCells &, const tag::OfMaxDim &, const tag::OrientCompWithMesh &,
		const tag::Around &, const Cell & cll, const tag::RequireOrder &               ) const
	{	return iterator ( tag::over_cells_of_max_dim, tag::orientation_compatible_with_mesh,
		                  tag::around, cll, tag::require_order                              );  }
	inline Mesh::Iterator iterator
	( const tag::OverCellsOfMaxDim &,
	  const tag::Orientation &, const tag::CompatWithMesh &,
	  const tag::Around &, const Cell & cll, const tag::RequireOrder & ) const
	{	return iterator ( tag::over_cells_of_max_dim, tag::orientation_compatible_with_mesh,
		                  tag::around, cll, tag::require_order                              );  }
	inline Mesh::Iterator iterator
	( const tag::OverCells &, const tag::OfMaxDim &,
	  const tag::Orientation &, const tag::CompatWithMesh &,
	  const tag::Around &, const Cell & cll, const tag::RequireOrder & ) const
	{	return iterator ( tag::over_cells_of_max_dim, tag::orientation_compatible_with_mesh,
		                  tag::around, cll, tag::require_order                              );  }
	inline Mesh::Iterator iterator
	( const tag::OverCellsOfMaxDim &,
	  const tag::Orientation &, const tag::CompatWith &, const tag::MEsh &,
	  const tag::Around &, const Cell & cll, const tag::RequireOrder &     ) const
	{	return iterator ( tag::over_cells_of_max_dim, tag::orientation_compatible_with_mesh,
		                  tag::around, cll, tag::require_order                              );  }
	inline Mesh::Iterator iterator
	( const tag::OverCells &, const tag::OfMaxDim &,
	  const tag::Orientation &, const tag::CompatWith &, const tag::MEsh &,
	  const tag::Around &, const Cell & cll, const tag::RequireOrder &     ) const
	{	return iterator ( tag::over_cells_of_max_dim, tag::orientation_compatible_with_mesh,
		                  tag::around, cll, tag::require_order                              );  }
	
	inline Mesh::Iterator iterator
	( const tag::OverCellsOfMaxDim &, const tag::OrientOpposToMesh &,
	  const tag::Around &, const Cell & cll, const tag::RequireOrder & ) const;
	inline Mesh::Iterator iterator
	( const tag::OverCells &, const tag::OfMaxDim &, const tag::OrientOpposToMesh &,
		const tag::Around &, const Cell & cll, const tag::RequireOrder &              ) const
	{	return iterator ( tag::over_cells_of_max_dim, tag::orientation_opposed_to_mesh,
		                  tag::around, cll, tag::require_order                         );  }
	inline Mesh::Iterator iterator
	( const tag::OverCellsOfMaxDim &,
	  const tag::Orientation &, const tag::OpposToMesh &,
	  const tag::Around &, const Cell & cll, const tag::RequireOrder & ) const
	{	return iterator ( tag::over_cells_of_max_dim, tag::orientation_opposed_to_mesh,
		                  tag::around, cll, tag::require_order                         );  }
	inline Mesh::Iterator iterator
	( const tag::OverCells &, const tag::OfMaxDim &,
	  const tag::Orientation &, const tag::OpposToMesh &,
	  const tag::Around &, const Cell & cll, const tag::RequireOrder & ) const
	{	return iterator ( tag::over_cells_of_max_dim, tag::orientation_opposed_to_mesh,
		                  tag::around, cll, tag::require_order                         );  }
	inline Mesh::Iterator iterator
	( const tag::OverCellsOfMaxDim &,
	  const tag::Orientation &, const tag::OpposedTo &, const tag::MEsh &,
	  const tag::Around &, const Cell & cll, const tag::RequireOrder &     ) const
	{	return iterator ( tag::over_cells_of_max_dim, tag::orientation_opposed_to_mesh,
		                  tag::around, cll, tag::require_order                         );  }
	inline Mesh::Iterator iterator
	( const tag::OverCells &, const tag::OfMaxDim &,
	  const tag::Orientation &, const tag::OpposedTo &, const tag::MEsh &,
	  const tag::Around &, const Cell & cll, const tag::RequireOrder &     ) const
	{	return iterator ( tag::over_cells_of_max_dim, tag::orientation_opposed_to_mesh,
		                  tag::around, cll, tag::require_order                         );  }
	
	// we are still in class Mesh

	inline Mesh::Iterator iterator
	( const tag::OverCellsOfMaxDim &,
	  const tag::Around &, const Cell & cll, const tag::RequireOrder & ) const
	{	return iterator ( tag::over_cells_of_max_dim, tag::orientation_compatible_with_mesh,
		                  tag::around, cll, tag::require_order                              );  }
	inline Mesh::Iterator iterator
	( const tag::OverCells &, const tag::OfMaxDim &,
	  const tag::Around &, const Cell & cll, const tag::RequireOrder & ) const
	{	return iterator ( tag::over_cells_of_max_dim, tag::orientation_compatible_with_mesh,
		                  tag::around, cll, tag::require_order                              );  }

	inline Mesh::Iterator iterator
	( const tag::OverCellsOfMaxDim &, const tag::ForcePositive &,
	  const tag::Around &, const Cell & cll, const tag::RequireOrder & ) const;
	inline Mesh::Iterator iterator
	( const tag::OverCells &, const tag::OfMaxDim &, const tag::ForcePositive &,
	  const tag::Around &, const Cell & cll, const tag::RequireOrder &          ) const
	{	return iterator ( tag::over_cells_of_max_dim, tag::force_positive,
		                  tag::around, cll, tag::require_order            );  }

	// we are still in class Mesh

	inline Mesh::Iterator iterator
	( const tag::OverCellsOfMaxDim &, const tag::OrientCompWithCenter &,
	  const tag::Around &, const Cell & cll, const tag::Backwards & ) const;
	inline Mesh::Iterator iterator
	( const tag::OverCells &, const tag::OfMaxDim &, const tag::OrientCompWithCenter &,
	  const tag::Around &, const Cell & cll, const tag::Backwards &                 ) const
	{	return iterator ( tag::over_cells_of_max_dim, tag::orientation_compatible_with_center,
		                  tag::around, cll, tag::backwards                                    );  }
	inline Mesh::Iterator iterator
	( const tag::OverCellsOfMaxDim &,
	  const tag::Orientation &, const tag::CompatWithCenter &,
	  const tag::Around &, const Cell & cll, const tag::Backwards & ) const
	{	return iterator ( tag::over_cells_of_max_dim, tag::orientation_compatible_with_center,
		                  tag::around, cll, tag::backwards                                    );  }
	inline Mesh::Iterator iterator
	( const tag::OverCells &, const tag::OfMaxDim &,
	  const tag::Orientation &, const tag::CompatWithCenter &,
	  const tag::Around &, const Cell & cll, const tag::Backwards & ) const
	{	return iterator ( tag::over_cells_of_max_dim, tag::orientation_compatible_with_center,
		                  tag::around, cll, tag::backwards                                    );  }
	inline Mesh::Iterator iterator
	( const tag::OverCellsOfMaxDim &,
	  const tag::Orientation &, const tag::CompatWith &, const tag::Center &,
	  const tag::Around &, const Cell & cll, const tag::Backwards &          ) const
	{	return iterator ( tag::over_cells_of_max_dim, tag::orientation_compatible_with_center,
		                  tag::around, cll, tag::backwards                                    );  }
	inline Mesh::Iterator iterator
	( const tag::OverCells &, const tag::OfMaxDim &,
	  const tag::Orientation &, const tag::CompatWith &, const tag::Center &,
	  const tag::Around &, const Cell & cll, const tag::Backwards &          ) const
	{	return iterator ( tag::over_cells_of_max_dim, tag::orientation_compatible_with_center,
		                  tag::around, cll, tag::backwards                                    );  }
	
	// we are still in class Mesh

	inline Mesh::Iterator iterator
	( const tag::OverCellsOfMaxDim &, const tag::OrientOpposToCenter &,
	  const tag::Around &, const Cell & cll, const tag::Backwards & ) const;
	inline Mesh::Iterator iterator
	( const tag::OverCells &, const tag::OfMaxDim &, const tag::OrientOpposToCenter &,
	  const tag::Around &, const Cell & cll, const tag::Backwards &                ) const
	{	return iterator ( tag::over_cells_of_max_dim, tag::orientation_opposed_to_center,
		                  tag::around, cll, tag::backwards                               );  }
	inline Mesh::Iterator iterator
	( const tag::OverCellsOfMaxDim &, const tag::Orientation &, const tag::OpposToCenter &,
	  const tag::Around &, const Cell & cll, const tag::Backwards &                     ) const
	{	return iterator ( tag::over_cells_of_max_dim, tag::orientation_opposed_to_center,
		                  tag::around, cll, tag::backwards                               );  }
	inline Mesh::Iterator iterator
	( const tag::OverCells &, const tag::OfMaxDim &,
	  const tag::Orientation &, const tag::OpposToCenter &,
	  const tag::Around &, const Cell & cll, const tag::Backwards & ) const
	{	return iterator ( tag::over_cells_of_max_dim, tag::orientation_opposed_to_center,
		                  tag::around, cll, tag::backwards                               );  }
	inline Mesh::Iterator iterator
	( const tag::OverCellsOfMaxDim &,
	  const tag::Orientation &, const tag::OpposedTo &, const tag::Center &,
	  const tag::Around &, const Cell & cll, const tag::Backwards &         ) const
	{	return iterator ( tag::over_cells_of_max_dim, tag::orientation_opposed_to_center,
		                  tag::around, cll, tag::backwards                               );  }
	inline Mesh::Iterator iterator
	( const tag::OverCells &, const tag::OfMaxDim &,
	  const tag::Orientation &, const tag::OpposedTo &, const tag::Center &,
	  const tag::Around &, const Cell & cll, const tag::Backwards &         ) const
	{	return iterator ( tag::over_cells_of_max_dim, tag::orientation_opposed_to_center,
		                  tag::around, cll, tag::backwards                               );  }
	
	// we are still in class Mesh

	inline Mesh::Iterator iterator
	( const tag::OverCellsOfMaxDim &, const tag::OrientCompWithMesh &,
	  const tag::Around &, const Cell & cll, const tag::Backwards & ) const;
	inline Mesh::Iterator iterator
	( const tag::OverCells &, const tag::OfMaxDim &, const tag::OrientCompWithMesh &,
		const tag::Around &, const Cell & cll, const tag::Backwards &                ) const
	{	return iterator ( tag::over_cells_of_max_dim, tag::orientation_compatible_with_mesh,
		                  tag::around, cll, tag::backwards                                  );  }
	inline Mesh::Iterator iterator
	( const tag::OverCellsOfMaxDim &,
	  const tag::Orientation &, const tag::CompatWithMesh &,
	  const tag::Around &, const Cell & cll, const tag::Backwards & ) const
	{	return iterator ( tag::over_cells_of_max_dim, tag::orientation_compatible_with_mesh,
		                  tag::around, cll, tag::backwards                                  );  }
	inline Mesh::Iterator iterator
	( const tag::OverCells &, const tag::OfMaxDim &,
	  const tag::Orientation &, const tag::CompatWithMesh &,
	  const tag::Around &, const Cell & cll, const tag::Backwards & ) const
	{	return iterator ( tag::over_cells_of_max_dim, tag::orientation_compatible_with_mesh,
		                  tag::around, cll, tag::backwards                                  );  }
	inline Mesh::Iterator iterator
	( const tag::OverCellsOfMaxDim &,
	  const tag::Orientation &, const tag::CompatWith &, const tag::MEsh &,
	  const tag::Around &, const Cell & cll, const tag::Backwards &        ) const
	{	return iterator ( tag::over_cells_of_max_dim, tag::orientation_compatible_with_mesh,
		                  tag::around, cll, tag::backwards                                  );  }
	inline Mesh::Iterator iterator
	( const tag::OverCells &, const tag::OfMaxDim &,
	  const tag::Orientation &, const tag::CompatWith &, const tag::MEsh &,
	  const tag::Around &, const Cell & cll, const tag::Backwards &        ) const
	{	return iterator ( tag::over_cells_of_max_dim, tag::orientation_compatible_with_mesh,
		                  tag::around, cll, tag::backwards                                  );  }
	inline Mesh::Iterator iterator
	( const tag::OverCellsOfMaxDim &, const tag::Around &, const Cell & cll, const tag::Backwards & ) const
	{	return iterator ( tag::over_cells_of_max_dim, tag::orientation_compatible_with_mesh,
		                  tag::around, cll, tag::backwards                                  );  }
	inline Mesh::Iterator iterator
	( const tag::OverCells &, const tag::OfMaxDim &,
	  const tag::Around &, const Cell & cll, const tag::Backwards & ) const
	{	return iterator ( tag::over_cells_of_max_dim, tag::orientation_compatible_with_mesh,
		                  tag::around, cll, tag::backwards                                  );  }

	// we are still in class Mesh

	inline Mesh::Iterator iterator
	( const tag::OverCellsOfMaxDim &, const tag::OrientOpposToMesh &,
	  const tag::Around &, const Cell & cll, const tag::Backwards & ) const;
	inline Mesh::Iterator iterator
	( const tag::OverCells &, const tag::OfMaxDim &, const tag::OrientOpposToMesh &,
		const tag::Around &, const Cell & cll, const tag::Backwards &                ) const
	{	return iterator ( tag::over_cells_of_max_dim, tag::orientation_opposed_to_mesh,
		                  tag::around, cll, tag::backwards                             );  }
	inline Mesh::Iterator iterator
	( const tag::OverCellsOfMaxDim &,
	  const tag::Orientation &, const tag::OpposToMesh &,
	  const tag::Around &, const Cell & cll, const tag::Backwards & ) const
	{	return iterator ( tag::over_cells_of_max_dim, tag::orientation_opposed_to_mesh,
		                  tag::around, cll, tag::backwards                             );  }
	inline Mesh::Iterator iterator
	( const tag::OverCells &, const tag::OfMaxDim &,
	  const tag::Orientation &, const tag::OpposToMesh &,
	  const tag::Around &, const Cell & cll, const tag::Backwards & ) const
	{	return iterator ( tag::over_cells_of_max_dim, tag::orientation_opposed_to_mesh,
		                  tag::around, cll, tag::backwards                             );  }
	inline Mesh::Iterator iterator
	( const tag::OverCellsOfMaxDim &,
	  const tag::Orientation &, const tag::OpposedTo &, const tag::MEsh &,
	  const tag::Around &, const Cell & cll, const tag::Backwards &        ) const
	{	return iterator ( tag::over_cells_of_max_dim, tag::orientation_opposed_to_mesh,
		                  tag::around, cll, tag::backwards                             );  }
	inline Mesh::Iterator iterator
	( const tag::OverCells &, const tag::OfMaxDim &,
	  const tag::Orientation &, const tag::OpposedTo &, const tag::MEsh &,
	  const tag::Around &, const Cell & cll, const tag::Backwards &        ) const
	{	return iterator ( tag::over_cells_of_max_dim, tag::orientation_opposed_to_mesh,
		                  tag::around, cll, tag::backwards                             );  }

	// we are still in class Mesh

	inline Mesh::Iterator iterator
	( const tag::OverCellsOfMaxDim &, const tag::ForcePositive &,
	  const tag::Around &, const Cell & cll, const tag::Backwards & ) const;
	inline Mesh::Iterator iterator
	( const tag::OverCells &, const tag::OfMaxDim &, const tag::ForcePositive &,
	  const tag::Around &, const Cell & cll, const tag::Backwards &             ) const
	{	return iterator ( tag::over_cells_of_max_dim, tag::force_positive,
		                  tag::around, cll, tag::backwards                );  }

	inline Mesh::Iterator iterator
	( const tag::OverVertices &, const tag::ConnectedTo &, const Cell & V ) const;
	inline Mesh::Iterator iterator
	( const tag::Over &, const tag::Vertices &, const tag::ConnectedTo &, const Cell & V ) const
	{	return  this->iterator ( tag::over_vertices, tag::connected_to, V );  }
	inline Mesh::Iterator iterator ( const tag::OverVertices &,
	    const tag::ConnectedTo &, const Cell & V, const tag::RequireOrder & ) const;
	inline Mesh::Iterator iterator ( const tag::Over &, const tag::Vertices &,
	    const tag::ConnectedTo &, const Cell & V, const tag::RequireOrder &   ) const
	{	return  this->iterator ( tag::over_vertices, tag::connected_to, V, tag::require_order );  }

	// we are still in class Mesh

	inline Mesh::Iterator iterator
	( const tag::OverSegments &, const tag::PointingTowards &, const Cell & V ) const;
	inline Mesh::Iterator iterator
	( const tag::Over &, const tag::Segments &, const tag::PointingTowards &, const Cell & V ) const
	{	return iterator ( tag::over_segments, tag::pointing_towards, V );  }

	inline Mesh::Iterator iterator
	( const tag::OverSegments &, const tag::PointingTowards &, const Cell & V,
	  const tag::RequireOrder &                                               ) const;
	inline Mesh::Iterator iterator
	( const tag::Over &, const tag::Segments &, const tag::PointingTowards &, const Cell & V,
	  const tag::RequireOrder &                                                              ) const
	{	return iterator ( tag::over_segments, tag::pointing_towards, V, tag::require_order );  }

	inline Mesh::Iterator iterator
	( const tag::OverSegments &, const tag::PointingTowards &, const Cell & V,
	  const tag::Backwards &                                                  ) const;
	inline Mesh::Iterator iterator
	( const tag::Over &, const tag::Segments &, const tag::PointingTowards &, const Cell & V,
	  const tag::Backwards &                                                                 ) const
	{	return iterator ( tag::over_segments, tag::pointing_towards, V, tag::backwards );  }

	// we are still in class Mesh

	inline Mesh::Iterator iterator
	( const tag::OverSegments &, const tag::PointingAwayFrom &, const Cell & V ) const;
	inline Mesh::Iterator iterator
	( const tag::Over &, const tag::Segments &, const tag::PointingAwayFrom &, const Cell & V ) const
	{	return iterator ( tag::over_segments, tag::pointing_away_from, V );  }

	inline Mesh::Iterator iterator
	( const tag::OverSegments &,
	  const tag::PointingAwayFrom &, const Cell & V, const tag::RequireOrder & ) const;
	inline Mesh::Iterator iterator
	( const tag::Over &, const tag::Segments &,
	  const tag::PointingAwayFrom &, const Cell & V, const tag::RequireOrder & ) const
	{	return iterator ( tag::over_segments, tag::pointing_away_from, V, tag::require_order );  }

	inline Mesh::Iterator iterator
	( const tag::OverSegments &,
	  const tag::PointingAwayFrom &, const Cell & V, const tag::Backwards & ) const;
	inline Mesh::Iterator iterator
	( const tag::Over &, const tag::Segments &,
	  const tag::PointingAwayFrom &, const Cell & V, const tag::Backwards & ) const
	{	return iterator ( tag::over_segments, tag::pointing_away_from, V, tag::backwards );  }

	// we are still in class Mesh

	inline Mesh::Iterator iterator
	( const tag::OverSegments &, const tag::AsFound &, const tag::Around &, const Cell & V ) const;
	inline Mesh::Iterator iterator ( const tag::OverSegments &, const tag::Around &, const Cell & V ) const
	{	return iterator ( tag::over_segments, tag::as_found, tag::around, V );  }
	inline Mesh::Iterator iterator
	( const tag::Over &, const tag::Segments &, const tag::Around &, const Cell & V ) const
	{	return iterator ( tag::over_segments, tag::as_found, tag::around, V );  }

	inline Mesh::Iterator iterator
	( const tag::OverSegments &, const tag::AsFound &, const tag::Around &, const Cell & V,
	  const tag::RequireOrder &                                                            ) const;
	inline Mesh::Iterator iterator
	( const tag::Over &, const tag::Segments &, const tag::AsFound &,
	  const tag::Around &, const Cell & V, const tag::RequireOrder & ) const
	{	return iterator ( tag::over_segments, tag::as_found, tag::around, V, tag::require_order );  }
	inline Mesh::Iterator iterator
	( const tag::OverSegments &, const tag::Around &, const Cell & V,
	  const tag::RequireOrder &                                      ) const;
	inline Mesh::Iterator iterator
	( const tag::Over &, const tag::Segments &,
	  const tag::Around &, const Cell & V, const tag::RequireOrder & ) const
	{	return iterator ( tag::over_segments, tag::as_found, tag::around, V, tag::require_order );  }

	inline Mesh::Iterator iterator
	( const tag::OverSegments &, const tag::AsFound &,
	  const tag::Around &, const Cell & V, const tag::Backwards &    ) const;
	inline Mesh::Iterator iterator
	( const tag::Over &, const tag::Segments &, const tag::AsFound &,
	  const tag::Around &, const Cell & V, const tag::Backwards &    ) const
	{	return iterator ( tag::over_segments, tag::as_found, tag::around, V, tag::backwards );  }

	inline Mesh::Iterator iterator
	( const tag::OverSegments &, const tag::Around &, const Cell & V,
	  const tag::Backwards &                                         ) const
	{	return iterator ( tag::over_segments, tag::as_found, tag::around, V, tag::backwards );  }
	inline Mesh::Iterator iterator
	( const tag::Over &, const tag::Segments &,
	  const tag::Around &, const Cell & V, const tag::Backwards &    ) const
	{	return iterator ( tag::over_segments, tag::as_found, tag::around, V, tag::backwards );  }

	// we are still in class Mesh

