
# C++ compiler
CC = g++

# compiler flags

# optimization level : -O2 or -O3
CFLAGS = -O2

# you may comment or uncomment lines below
CFLAGS := $(CFLAGS) -DMANIFEM_NO_FEM
# CFLAGS := $(CFLAGS) -DMANIFEM_NO_FRONTAL
# CFLAGS := $(CFLAGS) -DMANIFEM_NO_QUOTIENT
# CFLAGS := $(CFLAGS) -DMANIFEM_COLLECT_CM
# CFLAGS := $(CFLAGS) -DNDEBUG

# directory where Eigen is installed
# if Eigen is not installed, leave the line below as it is
EIGEN_DIR = $(HOME)/include/

# under normal circumstances, there is no need to change text below

CFLAGS := $(CFLAGS) -std=c++23 -c -Wshadow -Wall -I . -I $(EIGEN_DIR) -DOMIT_OBSOLETE_CODE

manifem_objects = extrude.o field.o finite-elem.o frontal.o function.o global.o iterator.o \
                  manifold.o mesh.o var-form.o

%.o: src/%.cpp
	$(CC) $(CFLAGS) $^

parag-%.o: examples-manual/parag-%.cpp
	$(CC) $(CFLAGS) -o $@ $^

manifem-exe-%: parag-%.o $(manifem_objects)
	$(CC) -o $@ $^

# parag-13.14.o: parag-13.14.cpp
# 	$(CC) $(CFLAGS) -o $@ $^

manifem-exe-13.14: parag-13.14.o
	$(CC) -o $@ $^

manifem-exe-user-%: user-%.o $(manifem_objects)
	$(CC) -o $@ $^

user-%.o: user-%.cpp
	$(CC) $(CFLAGS) -o $@ $^

run-parag-%: manifem-exe-%
	./$<
#	./systemSounds.exe hand

run-user-%: manifem-exe-user-%
	./$<
#	./systemSounds.exe hand

static-lib: libmaniFEM.a

libmaniFEM.a: $(manifem_objects)
	ar cr $@ $^

clean:
	rm -f *.o manifem-exe-* libmaniFEM.a

clean-all: clean
	rm -f *.eps *.msh out*.txt libmaniFEM*.a

.SECONDARY:

.PHONY: clean clean-all static-lib run-%

