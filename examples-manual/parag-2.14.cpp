
// example presented in paragraph 2.14 of the manual
// https://maniFEM.rd.ciencias.ulisboa.pt/manual-maniFEM.pdf
// using zero-dimensional manifolds to compute the intersection of two parabolas

#include "maniFEM.h"

using namespace maniFEM;

int main ()

{	Manifold RR2 ( tag::Euclid, tag::of_dim, 2 );
	Function xy = RR2 .build_coordinate_system ( tag::Lagrange, tag::of_degree, 1 );
	Function x = xy[0], y = xy[1];

	Manifold parab_vert  = RR2 .implicit ( y == x * x - 2. );
	Manifold parab_horiz = RR2 .implicit ( x == y * y - 2. );

	Manifold four_points ( tag::intersect, parab_vert, parab_horiz );
	
	Cell A ( tag::vertex, tag::of_coords, {-1.,-1.}, tag::project );
	Cell B ( tag::vertex, tag::of_coords, { 1.,-1.}, tag::project );
	Cell C ( tag::vertex, tag::of_coords, { 1., 1.}, tag::project );
	Cell D ( tag::vertex, tag::of_coords, {-1., 1.}, tag::project );

	// four arcs of parabola :
	parab_vert .set_as_working_manifold();
	Mesh BC = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( B ) .stop_at ( C ) .divided_in ( 12 );
	Mesh DA = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( D ) .stop_at ( A ) .divided_in ( 12 );
	parab_horiz .set_as_working_manifold();
	Mesh AB = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( A ) .stop_at ( B ) .divided_in ( 10 );
	Mesh CD = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( C ) .stop_at ( D ) .divided_in ( 10 );

	RR2 .set_as_working_manifold();

	Mesh ABCD = Mesh::Build ( tag::grid ) .shape ( tag::rectangle ) .faces ( AB, BC, CD, DA );

	ABCD .export_to_file ( tag::gmsh, "rectangle.msh" );
	std::cout << "produced file rectangle.msh" << std::endl;
	
}  // end of main
