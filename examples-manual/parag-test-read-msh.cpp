

// to eliminate


#include "maniFEM.h"

using namespace maniFEM;


int main_1 ( )

{	Manifold RR3 ( tag::Euclid, tag::of_dim, 3 );
	Function xyz = RR3 .build_coordinate_system ( tag::Lagrange, tag::of_degree, 1 );
	// Function x = xyz [0], y = xyz [1], z = xyz [2];

	Mesh::Composite sphere_cyl ( tag::import_from_file, tag::gmsh, "sphere-cyl-composite.msh" );

	std::cout << "points ";
	for ( std::map < std::string, std::vector < Cell > > ::const_iterator
	      it = sphere_cyl .points .begin(); it != sphere_cyl .points .end(); it ++ )
		std::cout << it->first << " ";
	std::cout << std::endl;

	std::cout << "meshes ";
	for ( std::map < std::string, std::vector < Mesh > > ::const_iterator
	      it = sphere_cyl .meshes .begin(); it != sphere_cyl .meshes .end(); it ++ )
	{	std::cout << it->first << " ";
		for ( std::vector < Mesh > ::const_iterator
		      itt = it->second .begin(); itt != it->second .end(); itt ++ )
			std::cout << itt->dim() << " ";                                    }
	std::cout << std::endl;

	return 0;
}


int main ( )

{	Manifold RR3 ( tag::Euclid, tag::of_dim, 3 );
	Function xyz = RR3 .build_coordinate_system ( tag::Lagrange, tag::of_degree, 1 );
	// Function x = xyz [0], y = xyz [1], z = xyz [2];

	Mesh disk = Mesh::Build ( tag::import_from_file )
		.format ( tag::gmsh ) .file_name ( "sphere-cyl.msh" );
	disk .export_to_file ( tag::gmsh, "ttor.msh" );
	return 0;
}

