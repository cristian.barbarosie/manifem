
// example presented in paragraph 2.16 of the manual
// https://maniFEM.rd.ciencias.ulisboa.pt/manual-maniFEM.pdf
// a physalis-like surface

#include "maniFEM.h"

using namespace maniFEM;


int main ( )

{	Manifold RR3 ( tag::Euclid, tag::of_dim, 3 );
	Function xyz = RR3 .build_coordinate_system ( tag::Lagrange, tag::of_degree, 1 );
	Function x = xyz [0], y = xyz [1], z = xyz [2];

	Function r2 = x*x + y*y + z*z;
	const double pi = 3.1415926536;
	Manifold apple = RR3 .implicit ( power(r2,0.5) * sin(r2-pi/6.) == z );

	Cell A ( tag::vertex );  x(A) = 0.;  y(A) = 0.;  z(A) = std::sqrt ( 2.*pi/3. );
	Cell B1 ( tag::vertex );  x(B1) = 1.;  y(B1) = 0.;  z(B1) = 1.;
	Cell C1 ( tag::vertex );  x(C1) = 1.;  y(C1) = 0.;  z(C1) = 0.;
	apple .project (B1);  apple .project (C1);
	Cell D ( tag::vertex );  x(D) = 0.;  y(D) = 0.;  z(D) = 0.;
	Mesh AB1 = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( A ) .stop_at ( B1 ) .divided_in ( 10 );
	Mesh B1C1 = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( B1 ) .stop_at ( C1 ) .divided_in ( 10 );
	Mesh C1D = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( C1 ) .stop_at ( D ) .divided_in ( 10 );
	Cell B2 ( tag::vertex );  x(B2) = 0.707;  y(B2) = 0.707;  z(B2) = 1.;
	Cell C2 ( tag::vertex );  x(C2) = 0.707;  y(C2) = 0.707;  z(C2) = 0.;
	apple .project (B2);  apple .project (C2);
	Mesh AB2 = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( A ) .stop_at ( B2 ) .divided_in ( 10 );
	Mesh B2C2 = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( B2 ) .stop_at ( C2 ) .divided_in ( 10 );
	Mesh C2D = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( C2 ) .stop_at ( D ) .divided_in ( 10 );
	Cell B3 ( tag::vertex );  x(B3) = 0.;  y(B3) = 1.;  z(B3) = 1.;
	Cell C3 ( tag::vertex );  x(C3) = 0.;  y(C3) = 1.;  z(C3) = 0.;
	apple .project (B3);  apple .project (C3);
	Mesh AB3 = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( A ) .stop_at ( B3 ) .divided_in ( 10 );
	Mesh B3C3 = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( B3 ) .stop_at ( C3 ) .divided_in ( 10 );
	Mesh C3D = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( C3 ) .stop_at ( D ) .divided_in ( 10 );
	Cell B4 ( tag::vertex );  x(B4) = -0.707;  y(B4) = 0.707;  z(B4) = 1.;
	Cell C4 ( tag::vertex );  x(C4) = -0.707;  y(C4) = 0.707;  z(C4) = 0.;
	apple .project (B4);  apple .project (C4);
	Mesh AB4 = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( A ) .stop_at ( B4 ) .divided_in ( 10 );
	Mesh B4C4 = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( B4 ) .stop_at ( C4 ) .divided_in ( 10 );
	Mesh C4D = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( C4 ) .stop_at ( D ) .divided_in ( 10 );
	Cell B5 ( tag::vertex );  x(B5) = -1.;  y(B5) = 0.;  z(B5) = 1.;
	Cell C5 ( tag::vertex );  x(C5) = -1.;  y(C5) = 0.;  z(C5) = 0.;
	apple .project (B5);  apple .project (C5);
	Mesh AB5 = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( A ) .stop_at ( B5 ) .divided_in ( 10 );
	Mesh B5C5 = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( B5 ) .stop_at ( C5 ) .divided_in ( 10 );
	Mesh C5D = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( C5 ) .stop_at ( D ) .divided_in ( 10 );
	Cell B6 ( tag::vertex );  x(B6) = -0.707;  y(B6) = -0.707;  z(B6) = 1.;
	Cell C6 ( tag::vertex );  x(C6) = -0.707;  y(C6) = -0.707;  z(C6) = 0.;
	apple .project (B6);  apple .project (C6);
	Mesh AB6 = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( A ) .stop_at ( B6 ) .divided_in ( 10 );
	Mesh B6C6 = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( B6 ) .stop_at ( C6 ) .divided_in ( 10 );
	Mesh C6D = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( C6 ) .stop_at ( D ) .divided_in ( 10 );
	Cell B7 ( tag::vertex );  x(B7) = 0.;  y(B7) = -1.;  z(B7) = 1.;
	Cell C7 ( tag::vertex );  x(C7) = 0.;  y(C7) = -1.;  z(C7) = 0.;
	apple .project (B7);  apple .project (C7);
	Mesh AB7 = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( A ) .stop_at ( B7 ) .divided_in ( 10 );
	Mesh B7C7 = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( B7 ) .stop_at ( C7 ) .divided_in ( 10 );
	Mesh C7D = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( C7 ) .stop_at ( D ) .divided_in ( 10 );
	Cell B8 ( tag::vertex );  x(B8) = 0.707;  y(B8) = -0.707;  z(B8) = 1.;
	Cell C8 ( tag::vertex );  x(C8) = 0.707;  y(C8) = -0.707;  z(C8) = 0.;
	apple .project (B8);  apple .project (C8);
	Mesh AB8 = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( A ) .stop_at ( B8 ) .divided_in ( 10 );
	Mesh B8C8 = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( B8 ) .stop_at ( C8 ) .divided_in ( 10 );
	Mesh C8D = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( C8 ) .stop_at ( D ) .divided_in ( 10 );

	RR3 .set_as_working_manifold();
	Mesh B1B2 = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( B1 ) .stop_at ( B2 ) .divided_in ( 10 );
	Mesh B2B3 = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( B2 ) .stop_at ( B3 ) .divided_in ( 10 );
	Mesh B3B4 = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( B3 ) .stop_at ( B4 ) .divided_in ( 10 );
	Mesh B4B5 = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( B4 ) .stop_at ( B5 ) .divided_in ( 10 );
	Mesh B5B6 = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( B5 ) .stop_at ( B6 ) .divided_in ( 10 );
	Mesh B6B7 = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( B6 ) .stop_at ( B7 ) .divided_in ( 10 );
	Mesh B7B8 = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( B7 ) .stop_at ( B8 ) .divided_in ( 10 );
	Mesh B8B1 = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( B8 ) .stop_at ( B1 ) .divided_in ( 10 );
	Mesh C1C2 = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( C1 ) .stop_at ( C2 ) .divided_in ( 10 );
	Mesh C2C3 = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( C2 ) .stop_at ( C3 ) .divided_in ( 10 );
	Mesh C3C4 = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( C3 ) .stop_at ( C4 ) .divided_in ( 10 );
	Mesh C4C5 = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( C4 ) .stop_at ( C5 ) .divided_in ( 10 );
	Mesh C5C6 = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( C5 ) .stop_at ( C6 ) .divided_in ( 10 );
	Mesh C6C7 = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( C6 ) .stop_at ( C7 ) .divided_in ( 10 );
	Mesh C7C8 = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( C7 ) .stop_at ( C8 ) .divided_in ( 10 );
	Mesh C8C1 = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( C8 ) .stop_at ( C1 ) .divided_in ( 10 );
	
	Mesh AB1B2 = Mesh::Build ( tag::grid ) .shape ( tag::triangle )
		.faces ( AB1, B1B2, AB2 .reverse() );
	Mesh AB2B3 = Mesh::Build ( tag::grid ) .shape ( tag::triangle )
		.faces ( AB2, B2B3, AB3 .reverse() );
	Mesh AB3B4 = Mesh::Build ( tag::grid ) .shape ( tag::triangle )
		.faces ( AB3, B3B4, AB4 .reverse() );
	Mesh AB4B5 = Mesh::Build ( tag::grid ) .shape ( tag::triangle )
		.faces ( AB4, B4B5, AB5 .reverse() );
	Mesh AB5B6 = Mesh::Build ( tag::grid ) .shape ( tag::triangle )
		.faces ( AB5, B5B6, AB6 .reverse() );
	Mesh AB6B7 = Mesh::Build ( tag::grid ) .shape ( tag::triangle )
		.faces ( AB6, B6B7, AB7 .reverse() );
	Mesh AB7B8 = Mesh::Build ( tag::grid ) .shape ( tag::triangle )
		.faces ( AB7, B7B8, AB8 .reverse() );
	Mesh AB8B1 = Mesh::Build ( tag::grid ) .shape ( tag::triangle )
		.faces ( AB8, B8B1, AB1 .reverse() );

	Mesh B1C1C2B2 = Mesh::Build ( tag::grid ) .shape ( tag::rectangle )
		.faces ( B1C1, C1C2, B2C2 .reverse(), B1B2 .reverse() ) .with_triangles();
	Mesh B2C2C3B3 = Mesh::Build ( tag::grid ) .shape ( tag::rectangle )
		.faces ( B2C2, C2C3, B3C3 .reverse(), B2B3 .reverse() ) .with_triangles();
	Mesh B3C3C4B4 = Mesh::Build ( tag::grid ) .shape ( tag::rectangle )
		.faces ( B3C3, C3C4, B4C4 .reverse(), B3B4 .reverse() ) .with_triangles();
	Mesh B4C4C5B5 = Mesh::Build ( tag::grid ) .shape ( tag::rectangle )
		.faces ( B4C4, C4C5, B5C5 .reverse(), B4B5 .reverse() ) .with_triangles();
	Mesh B5C5C6B6 = Mesh::Build ( tag::grid ) .shape ( tag::rectangle )
		.faces ( B5C5, C5C6, B6C6 .reverse(), B5B6 .reverse() ) .with_triangles();
	Mesh B6C6C7B7 = Mesh::Build ( tag::grid ) .shape ( tag::rectangle )
		.faces ( B6C6, C6C7, B7C7 .reverse(), B6B7 .reverse() ) .with_triangles();
	Mesh B7C7C8B8 = Mesh::Build ( tag::grid ) .shape ( tag::rectangle )
		.faces ( B7C7, C7C8, B8C8 .reverse(), B7B8 .reverse() ) .with_triangles();
	Mesh B8C8C1B1 = Mesh::Build ( tag::grid ) .shape ( tag::rectangle )
		.faces ( B8C8, C8C1, B1C1 .reverse(), B8B1 .reverse() ) .with_triangles();

	Mesh C1DC2 = Mesh::Build ( tag::grid ) .shape ( tag::triangle )
		.faces ( C1D, C2D .reverse(), C1C2 .reverse() );
	Mesh C2DC3 = Mesh::Build ( tag::grid ) .shape ( tag::triangle )
		.faces ( C2D, C3D .reverse(), C2C3 .reverse() );
	Mesh C3DC4 = Mesh::Build ( tag::grid ) .shape ( tag::triangle )
		.faces ( C3D, C4D .reverse(), C3C4 .reverse() );
	Mesh C4DC5 = Mesh::Build ( tag::grid ) .shape ( tag::triangle )
		.faces ( C4D, C5D .reverse(), C4C5 .reverse() );
	Mesh C5DC6 = Mesh::Build ( tag::grid ) .shape ( tag::triangle )
		.faces ( C5D, C6D .reverse(), C5C6 .reverse() );
	Mesh C6DC7 = Mesh::Build ( tag::grid ) .shape ( tag::triangle )
		.faces ( C6D, C7D .reverse(), C6C7 .reverse() );
	Mesh C7DC8 = Mesh::Build ( tag::grid ) .shape ( tag::triangle )
		.faces ( C7D, C8D .reverse(), C7C8 .reverse() );
	Mesh C8DC1 = Mesh::Build ( tag::grid ) .shape ( tag::triangle )
		.faces ( C8D, C1D .reverse(), C8C1 .reverse() );

	Mesh sect1 = Mesh::Build ( tag::join ) .meshes ( { AB1B2, B1C1C2B2, C1DC2 } );
	Mesh sect2 = Mesh::Build ( tag::join ) .meshes ( { AB2B3, B2C2C3B3, C2DC3 } );
	Mesh sect3 = Mesh::Build ( tag::join ) .meshes ( { AB3B4, B3C3C4B4, C3DC4 } );
	Mesh sect4 = Mesh::Build ( tag::join ) .meshes ( { AB4B5, B4C4C5B5, C4DC5 } );
	Mesh sect5 = Mesh::Build ( tag::join ) .meshes ( { AB5B6, B5C5C6B6, C5DC6 } );
	Mesh sect6 = Mesh::Build ( tag::join ) .meshes ( { AB6B7, B6C6C7B7, C6DC7 } );
	Mesh sect7 = Mesh::Build ( tag::join ) .meshes ( { AB7B8, B7C7C8B8, C7DC8 } );
	Mesh sect8 = Mesh::Build ( tag::join ) .meshes ( { AB8B1, B8C8C1B1, C8DC1 } );

	std::list < Mesh > lm { sect1, sect2, sect3, sect4, sect5, sect6, sect7, sect8 };
	Mesh fisalis = Mesh::Build ( tag::join ) .meshes ( lm ); 
	
	Mesh::Iterator it = fisalis .iterator ( tag::over_vertices );
	for ( it .reset(); it .in_range(); it++ )
	{	Cell P = *it;
		x(P) *= 0.8;  y(P) *= 0.8;
		if ( z(P) > 1.3 )
		{	x(P) /= 1. + 300. * std::pow ( z(P) - 1.3, 3. );
			y(P) /= 1. + 300. * std::pow ( z(P) - 1.3, 3. );
			z(P) *= 1. + 10. * ( z(P) - 1.3 ) * ( z(P) - 1.3 );  }
		if ( z(P) > 0. ) z(P) *= 0.8;                              }

	std::list < Mesh > ::iterator it1;
	for ( it1 = lm .begin(); it1 != lm .end(); it1++ )
	{	Mesh sect = *it1;
		Mesh::Iterator it2 = sect .iterator ( tag::over_vertices );
		for ( int i = 1; i < 20; i++ )
		for ( it2 .reset(); it2 .in_range(); it2++ )
		{	Cell ver = *it2;
			if ( ver .is_inner_to ( sect ) ) sect .barycenter ( ver );  }  }

	fisalis.export_to_file ( tag::gmsh, "physalis.msh" );
	std::cout << "produced file physalis.msh" << std::endl;

}  // end of main
