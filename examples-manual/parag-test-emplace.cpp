

#include "maniFEM.h"
#include <set>

using namespace maniFEM;

int main ()

{	std::map < int, Cell > m;
	Cell A ( tag::vertex );
	// Cell B ( tag::vertex );
	// m .emplace ( tag::vertex );
	m .insert ( { 0, Cell ( tag::vertex ) } );
	// m .insert ( Cell ( tag::vertex ) );
	m .emplace ( std::piecewise_construct, std::forward_as_tuple (1), std::forward_as_tuple ( tag::vertex, tag::is_positive ) );
	tag::Util::emplace_new_elem ( m, std::piecewise_construct, std::forward_as_tuple (2), std::forward_as_tuple ( tag::vertex, tag::is_positive ) );
}
	
