
// example presented in paragraph 1.9 of the manual
// https://maniFEM.rd.ciencias.ulisboa.pt/manual-maniFEM.pdf
// fill with triangles a star-shaped domain, showing frontal mesh generation

#include "maniFEM.h"

using namespace maniFEM;


int main ( )

{	Manifold RR2 ( tag::Euclid, tag::of_dim, 2 );
	Function xy = RR2 .build_coordinate_system ( tag::Lagrange, tag::of_degree, 1 );
	Function x = xy[0], y = xy[1];

	const double coef = 0.7, seg_len = 0.1;

	Cell A ( tag::vertex );  x(A) =  0.75 * coef;  y(A) =  0.43 * coef;
	Cell B ( tag::vertex );  x(B) =  0.         ;  y(B) =  0.86 * coef;
	Cell C ( tag::vertex );  x(C) = -0.75 * coef;  y(C) =  0.43 * coef;
	Cell D ( tag::vertex );  x(D) = -0.75 * coef;  y(D) = -0.43 * coef;
	Cell E ( tag::vertex );  x(E) =  0.         ;  y(E) = -0.86 * coef;
	Cell F ( tag::vertex );  x(F) =  0.75 * coef;  y(F) = -0.43 * coef;

	const double r2 = (1.-0.75*coef)*(1.-0.75*coef) + 0.43*0.43*coef*coef;
	Manifold circle_1 = RR2 .implicit ( (x-0.5)*(x-0.5) + (y-0.86)*(y-0.86) == r2 );
	Mesh AB = Mesh::Build ( tag::frontal ) .desired_length ( seg_len )
		.start_at ( A ) .towards ( { -1., 0. } ) .stop_at ( B );
	Manifold circle_2 = RR2 .implicit ( (x+0.5)*(x+0.5) + (y-0.86)*(y-0.86) == r2 );
	Mesh BC = Mesh::Build ( tag::frontal ) .desired_length ( seg_len )
		.start_at ( B ) .towards ( { -1., -1. } ) .stop_at ( C );
	Manifold circle_3 = RR2 .implicit ( (x+1.)*(x+1.) + y*y == r2 );
	Mesh CD = Mesh::Build ( tag::frontal ) .desired_length ( seg_len )
		.start_at ( C ) .towards ( { 0.5, -1. } ) .stop_at ( D );
	Manifold circle_4 = RR2 .implicit ( (x+0.5)*(x+0.5) + (y+0.86)*(y+0.86) == r2 );
	Mesh DE = Mesh::Build ( tag::frontal ) .desired_length ( seg_len )
		.start_at ( D ) .towards ( { 1., 0. } ) .stop_at ( E );
	Manifold circle_5 = RR2 .implicit ( (x-0.5)*(x-0.5) + (y+0.86)*(y+0.86) == r2 );
	Mesh EF = Mesh::Build ( tag::frontal ) .desired_length ( seg_len )
		.start_at ( E ) .towards ( { 1., 1. } ) .stop_at ( F );
	Manifold circle_6 = RR2 .implicit ( (x-1.)*(x-1.) + y*y == r2 );
	Mesh FA = Mesh::Build ( tag::frontal ) .desired_length ( seg_len )
		.start_at ( F ) .towards ( { -0.5, 1. } ) .stop_at ( A );

	Mesh bdry = Mesh::Build ( tag::join ) .meshes ( { AB, BC, CD, DE, EF, FA } );

	RR2 .set_as_working_manifold();
	Mesh star = Mesh::Build ( tag::frontal ) .desired_length ( seg_len ) .boundary ( bdry );

	star .export_to_file ( tag::gmsh, "star.msh" );
	std::cout << "produced file star.msh" << std::endl;

}  // end of  main
