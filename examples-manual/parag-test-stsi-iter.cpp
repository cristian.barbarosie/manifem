
#include "maniFEM.h"

using namespace maniFEM;

int main ()

{	Manifold RR2 ( tag::Euclid, tag::of_dim, 2 );
	Function xy = RR2 .build_coordinate_system ( tag::Lagrange, tag::of_degree, 1 );
	Function x = xy [0], y = xy [1];

	Cell O ( tag::vertex, tag::of_coords, { 0., 0. } );
	Cell A ( tag::vertex, tag::of_coords, { 1., 0. } );
	Cell B ( tag::vertex, tag::of_coords, { 1., 1. } );
	Cell C ( tag::vertex, tag::of_coords, { 0., 1. } );
	Cell D ( tag::vertex, tag::of_coords, { 0., -1. } );
	Cell E ( tag::vertex, tag::of_coords, { -1., -1. } );
	Cell F ( tag::vertex, tag::of_coords, { -1., 0. } );

	Cell OA ( tag::segment, O .reverse(), A );
	Cell AB ( tag::segment, A .reverse(), B );
	Cell BO ( tag::segment, B .reverse(), O );
	Cell BC ( tag::segment, B .reverse(), C );
	Cell CO ( tag::segment, C .reverse(), O );
	Cell CA ( tag::segment, C .reverse(), A );
	Cell OD ( tag::segment, O .reverse(), D );
	Cell DE ( tag::segment, D .reverse(), E );
	Cell OE ( tag::segment, O .reverse(), E );
	Cell EF ( tag::segment, E .reverse(), F );
	Cell FD ( tag::segment, F .reverse(), D );
	Cell OF ( tag::segment, O .reverse(), F );

	Cell OAB ( tag::triangle, OA, AB, BO );
	Cell OBC ( tag::triangle, BO .reverse(), BC, CO );
	Cell OCA ( tag::triangle, CO .reverse(), CA, OA .reverse() );
	Cell ODE ( tag::triangle, OD, DE, OE .reverse() );
	Cell OEF ( tag::triangle, OE, EF, OF .reverse() );
	Cell OFD ( tag::triangle, OF, FD, OD .reverse() );

	Mesh msh ( tag::fuzzy, tag::of_dim, 2 );

	OAB .add_to_mesh ( msh );
	OBC .add_to_mesh ( msh );
	OCA .add_to_mesh ( msh );
	ODE .add_to_mesh ( msh );
	OEF .add_to_mesh ( msh );
	OFD .add_to_mesh ( msh );

	std::cout << "main line 49 " << msh .core << std::endl;

	{  // just a block of code for hiding 'it'
	Mesh::Iterator it = msh .iterator
		( tag::over_segments, tag::around, O, tag::pointing_away_from_center );
	for ( it .reset(); it .in_range(); it++ )
	{	Cell seg = * it;
		assert ( seg .base() .reverse() == O );
		Cell U = seg .base() .reverse();
		Cell V = seg .tip();
		std::cout << "[" << x(U) << "," << y(U) << "] [" << x(V) << "," << y(V) << "]" << std::endl;  }
	}  // just a block of code
	
	{  // just a block of code for hiding 'it'
	Mesh::Iterator it = msh .iterator
		( tag::over_vertices, tag::connected_to, O );
	for ( it .reset(); it .in_range(); it++ )
	{	Cell U = * it;
		std::cout << "[" << x(U) << "," << y(U) << "]" << std::endl;  }
	}  // just a block of code
	
	
}
