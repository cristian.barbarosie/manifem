
// example presented in paragraph 3.4 of the manual
// https://maniFEM.rd.ciencias.ulisboa.pt/manual-maniFEM.pdf
// build a circle in 3D (implicit manifold with two equations) by frontal method

#include "maniFEM.h"
#include "math.h"

using namespace maniFEM;


int main ()

{	Manifold RR3 ( tag::Euclid, tag::of_dim, 3 );
	Function xyz = RR3 .build_coordinate_system ( tag::Lagrange, tag::of_degree, 1 );
	Function x = xyz [0], y = xyz [1], z = xyz [2];

	RR3 .implicit ( x*x + y*y == 1., x*y == 4.*z );
	Mesh circle = Mesh::Build ( tag::frontal ) .entire_manifold()
		.orientation ( tag::random ) .desired_length ( 0.1 );

	// should produce error message with no  .orientation
	// should produce error message with     .orientation ( tag::intrinsic )
	// should produce error message with     .orientation ( tag::inherent )

	circle .export_to_file ( tag::gmsh, "circle-3d.msh" );
	std::cout << "produced file circle-3d.msh" << std::endl;

}  // end of main
