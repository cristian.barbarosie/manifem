
// example presented in paragraph 5.7 of the manual
// https://maniFEM.rd.ciencias.ulisboa.pt/manual-maniFEM.pdf
// extruding a composite swatch along a composite track

#include "maniFEM.h"
#include "math.h"

using namespace maniFEM;


int main ( )
  
{	Manifold RR3 ( tag::Euclid, tag::of_dim, 3 );
	Function xyz = RR3 .build_coordinate_system ( tag::Lagrange, tag::of_degree, 1 );
	Function x = xyz [0], y = xyz [1], z = xyz [2];

	RR3 .implicit ( x*x + y*y == 1., z == 0. );

	Cell N ( tag::vertex );  x(N) =  0.;  y(N) =  1.;  z(N) = 0.;
	Cell W ( tag::vertex );  x(W) = -1.;  y(W) =  0.;  z(W) = 0.;
	Cell S ( tag::vertex );  x(S) =  0.;  y(S) = -1.;  z(S) = 0.;
	Cell E ( tag::vertex );  x(E) =  1.;  y(E) =  0.;  z(E) = 0.;

	Mesh NW = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at (N) .stop_at (W) .divided_in ( 10 );
	Mesh WS = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at (W) .stop_at (S) .divided_in ( 10 );
	Mesh SE = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at (S) .stop_at (E) .divided_in ( 10 );
	Mesh EN = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at (E) .stop_at (N) .divided_in ( 10 );

	// Mesh big_circle = Mesh::Build ( tag::join ) .meshes ( { NW, WS, SE, EN } );
	Mesh::Composite big_circle = Mesh::Build ( tag::gather ) .cell ( "N", N )
		.mesh ( "NW", NW ) .mesh ( "WS", WS ) .mesh ( "SE", SE ) .mesh ( "EN", EN );

	RR3 .implicit ( (x-0.75)*(x-0.75) + z*z == 0.0625, y == 0. );
	  
	Cell A = E;         //   x(A) = 1.;     y(A) = 0.;   z(A) =  0.;
	Cell B ( tag::vertex );  x(B) = 0.75;   y(B) = 0.;   z(B) = -0.25;
	Cell C ( tag::vertex );  x(C) = 0.5;    y(C) = 0.;   z(C) =  0.;
	Cell D ( tag::vertex );  x(D) = 0.75;   y(D) = 0.;   z(D) =  0.25;

	Mesh AB = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at (A) .stop_at (B) .divided_in (5);
	Mesh BC = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at (B) .stop_at (C) .divided_in (5);
	Mesh CD = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at (C) .stop_at (D) .divided_in (5);
	Mesh DA = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at (D) .stop_at (A) .divided_in (5);

	// Mesh small_circle = Mesh::Build ( tag::join ) .meshes ( { AB, BC, CD, DA } );
	Mesh::Composite small_circle = Mesh::Build ( tag::gather ) .cell ( "A", A )
		.mesh ( "AB", AB ) .mesh ( "BC", BC ) .mesh ( "CD", CD ) .mesh ( "DA", DA );

	// note that A == E, thus meshes 'big_circle' and 'small_circle' have one vertex in common
	
	RR3 .set_as_working_manifold();
	Mesh::Composite torus = Mesh::Build ( tag::extrude )
		.swatch ( small_circle ) .slide_along ( big_circle ) .follow_curvature();

	torus .export_to_file ( tag::gmsh, "torus.msh" );
	std::cout << "produced file torus.msh" << std::endl;	
	
}  // end of main


