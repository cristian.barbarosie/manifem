


#include "maniFEM.h"
#include <fstream>
#include <set>
#include <iomanip>
#include <Eigen/Sparse>
#include <Eigen/OrderingMethods>
using namespace maniFEM;
using namespace std;


inline double sign ( const double c )
{	if ( c < 0. )  return -1.;
	return 1.;                  }


int main ( )

{	Manifold RR2 ( tag::Euclid, tag::of_dim, 2 );
	Function xy = RR2 .build_coordinate_system ( tag::Lagrange, tag::of_degree, 1 );
	Function coord_x = xy [0], coord_y = xy [1];

	// after declaring 'numbering', space in the memory will be reserved,
	// for each vertex, for a 'size_t' number
	// spurious vertices show up, unfortunately
	// to solve this problem, we compute again the numbering of significant vertices,
	// after the mesh generation is finished
	Cell::Numbering numbering ( tag::field );

	const double seg_size = 0.05;
	std::cout << std::setprecision (9);

	// parameters defining the shape of the hole (super-ellipse)
	const double super_ellipse_param_m_s = 0.5,
	             super_ellipse_param_m_t = 0.8,
	             super_ellipse_param_q   = 2.5;
	const double super_ellipse_param_q_minus_1 = super_ellipse_param_q - 1.;
	const double super_ellipse_param_m_s_over_2 = super_ellipse_param_m_s / 2.,
	             super_ellipse_param_m_t_over_2 = super_ellipse_param_m_t / 2.;

	// linear deformation of the grill ( D phi )
	double D_phi_s_d_x = 1.8, D_phi_s_d_y = 0.2, D_phi_t_d_x = 0.1, D_phi_t_d_y = 1.;
	const double det = D_phi_s_d_x * D_phi_t_d_y - D_phi_s_d_y * D_phi_t_d_x;
	// we can rescale the unit cell (vectors defining the torus)
	// the cellular problem remains the same
	const double sqrt_det = std::sqrt ( det );
	D_phi_s_d_x /= sqrt_det;
	D_phi_s_d_y /= sqrt_det;
	D_phi_t_d_x /= sqrt_det;
	D_phi_t_d_y /= sqrt_det;
	// psi is the inverse of phi
	const double D_psi_x_d_s =   D_phi_t_d_y,   // no need to divide by 'det'
	             D_psi_x_d_t = - D_phi_s_d_y,   // due to the rescaling above
	             D_psi_y_d_s = - D_phi_t_d_x,
	             D_psi_y_d_t =   D_phi_s_d_x;
	// std::cout << D_phi_s_d_x * D_psi_x_d_s + D_phi_s_d_y * D_psi_y_d_s << " "
	// 			 << D_phi_s_d_x * D_psi_x_d_t + D_phi_s_d_y * D_psi_y_d_t << " "
	// 			 << D_phi_t_d_x * D_psi_x_d_s + D_phi_t_d_y * D_psi_y_d_s << " "
	// 			 << D_phi_t_d_x * D_psi_x_d_t + D_phi_t_d_y * D_psi_y_d_t << std::endl;
	Function coord_s = D_phi_s_d_x * coord_x + D_phi_s_d_y * coord_y,
	         coord_t = D_phi_t_d_x * coord_x + D_phi_t_d_y * coord_y;

	Manifold super_ellipse_manif = RR2 .implicit
		(  power ( abs(coord_s) / super_ellipse_param_m_s_over_2, super_ellipse_param_q ) // +
		 + power ( abs(coord_t) / super_ellipse_param_m_t_over_2, super_ellipse_param_q ) == 1. );
	
	Mesh inner = Mesh::Build ( tag::frontal ) .entire_manifold() .desired_length ( seg_size );
	inner .export_to_file ( tag::gmsh, "inner.msh" );

	// Function transl_s = ( D_psi_x_d_s * ( coord_s + 1. ) + D_psi_x_d_t * coord_t ) &&
	//                     ( D_psi_y_d_s * ( coord_s + 1. ) + D_psi_y_d_t * coord_t )    ;
	// Function transl_t = ( D_psi_x_d_s * coord_s + D_psi_x_d_t * ( coord_t + 1. ) ) &&
	//                     ( D_psi_y_d_s * coord_s + D_psi_y_d_t * ( coord_t + 1. ) )    ;
	// expressions above are mathematically equivalent to the ones below
	// however, maniFEM does not recognize expressions above as translations
	// the (homogeneous) linear part is (1,0,0,1)
	// but this is not enough for maniFEM to look at them as translations
	Function transl_s = ( coord_x + D_psi_x_d_s ) && ( coord_y + D_psi_y_d_s );
	Function transl_t = ( coord_x + D_psi_x_d_t ) && ( coord_y + D_psi_y_d_t );
	Manifold::Action gs ( tag::transforms, xy, tag::into, transl_s );
	Manifold::Action gt ( tag::transforms, xy, tag::into, transl_t ); 
	Manifold torus = RR2 .quotient ( gs, gt );
	Function xy_torus = torus .coordinates();
	Function x_torus = xy_torus [0], y_torus = xy_torus [1];

	Mesh inner_fold = inner .fold ( tag::use_existing_vertices );
	
	Mesh perforated_torus = Mesh::Build ( tag::frontal )
		.boundary ( inner_fold .reverse() ) .desired_length ( seg_size );

	// Hooke's Law,  E = 1.,  nu = 0.3
	double lambda = 0.576923, mu = 0.38461538;

	// tag::Util::Tensor is a class defined in maniFEM for arbitrary order tensors
	tag::Util::Tensor <double> Hooke (2,2,2,2);
	Hooke (0,0,0,0) = 2*mu + lambda;
	Hooke (0,0,0,1) = 0.;
	Hooke (0,0,1,0) = 0.;
	Hooke (0,0,1,1) = lambda;
	Hooke (0,1,0,0) = 0.;
	Hooke (0,1,0,1) = mu;
	Hooke (0,1,1,0) = mu;
	Hooke (0,1,1,1) = 0.;
	Hooke (1,0,0,0) = 0.;
	Hooke (1,0,0,1) = mu;
	Hooke (1,0,1,0) = mu;
	Hooke (1,0,1,1) = 0.;
	Hooke (1,1,0,0) = lambda;
	Hooke (1,1,0,1) = 0.;
	Hooke (1,1,1,0) = 0.;
	Hooke (1,1,1,1) = 2*mu + lambda;

	std::cout << "elastic tensor, base material" << std::endl;
	for ( size_t i = 0; i < 2; i++ )
	for ( size_t j = 0; j < 2; j++ )
	{	for ( size_t k = 0; k < 2; k++ )
		for ( size_t l = 0; l < 2; l++ )
			std::cout << Hooke (i,j,k,l) << " ";
		std::cout << std::endl;                  }
	
	// space in the memory has been reserved, for each vertex, for a 'size_t' number
	// however, more vertices have been built, invisibly to the user,
	// than those in 'perforated_torus'
	// so we re-define the numbers of vertices in 'perforated_torus'
	// the others will never been used
	{ // just a block of code for hiding 'it' and 'counter'
	Mesh::Iterator it = perforated_torus .iterator ( tag::over_vertices );
	size_t counter = 0;
	for ( it .reset() ; it .in_range(); it ++ )
	{	Cell V = *it;  numbering (V) = counter;  ++ counter;  }
	std::cout << perforated_torus .number_of ( tag::vertices ) << " vertices" << std::endl;
	std::cout << counter << " counted" << std::endl;
	std::cout << "numbering has size " << numbering .size() << std::endl;
	} // just a block of code

	const size_t number_dofs = 2 * perforated_torus .number_of ( tag::vertices );
	std::cout << "global matrix " << number_dofs << "x" << number_dofs << std::endl;
	Eigen::SparseMatrix < double > matrix_A ( number_dofs, number_dofs );
	
	matrix_A .reserve ( Eigen::VectorXi::Constant ( number_dofs, 14 ) );
	// since we will be working with a mesh of triangles,
	// there will be, in average, 14 = 2*(6+1) non-zero elements per column
	// the diagonal entry plus six neighbour vertices

	Eigen::VectorXd vector_b_case_xx ( number_dofs ),
			vector_b_case_xy ( number_dofs ), vector_b_case_yy ( number_dofs );
	vector_b_case_xx .setZero();
	vector_b_case_xy .setZero();
	vector_b_case_yy .setZero();

	// macroscopic strain matrix, three load cases
	// denoted by A in the paper referred at the beginning of this file
	// tag::Util::Tensor is a class defined in maniFEM for arbitrary order tensors
	tag::Util::Tensor < double > macro_strain_case_xx (2,2),
			macro_strain_case_xy (2,2), macro_strain_case_yy (2,2);
	// 'macro_strain_case_yx' is the transpose of 'macro_strain_case_xy',
	// we do not waste resources for computing 'vector_b_case_yx'
	macro_strain_case_xx (0,0) = 1.;
	macro_strain_case_xx (0,1) = 0.;
	macro_strain_case_xx (1,0) = 0.;
	macro_strain_case_xx (1,1) = 0.;
	macro_strain_case_xy (0,0) = 0.;
	macro_strain_case_xy (0,1) = 1.;
	macro_strain_case_xy (1,0) = 0.;
	macro_strain_case_xy (1,1) = 0.;
	macro_strain_case_yy (0,0) = 0.;
	macro_strain_case_yy (0,1) = 0.;
	macro_strain_case_yy (1,0) = 0.;
	macro_strain_case_yy (1,1) = 1.;

	// 'u' will be a (vector-valued) multi-function defined on the torus
	// the jump of this multi-function (difference between sheets)
	// is given by the 'macro_strain' and also by the geometry of the torus
	Function::Jump jump_of_u_x_case_xx =
		macro_strain_case_xx (0,0) * x_torus .jump() + macro_strain_case_xx (0,1) * y_torus .jump();
	Function::Jump jump_of_u_y_case_xx =
		macro_strain_case_xx (1,0) * x_torus .jump() + macro_strain_case_xx (1,1) * y_torus .jump();
	Function::Jump jump_of_u_x_case_xy =
		macro_strain_case_xy (0,0) * x_torus .jump() + macro_strain_case_xy (0,1) * y_torus .jump();
	Function::Jump jump_of_u_y_case_xy =
		macro_strain_case_xy (1,0) * x_torus .jump() + macro_strain_case_xy (1,1) * y_torus .jump();
	Function::Jump jump_of_u_x_case_yy =
		macro_strain_case_yy (0,0) * x_torus .jump() + macro_strain_case_yy (0,1) * y_torus .jump();
	Function::Jump jump_of_u_y_case_yy =
		macro_strain_case_yy (1,0) * x_torus .jump() + macro_strain_case_yy (1,1) * y_torus .jump();
	
	// declare the type of finite element
	FiniteElement fe ( tag::triangle, tag::Lagrange, tag::of_degree, 1 );
	Integrator integ = fe .set_integrator ( tag::hand_coded );
	Function bf1 ( tag::basis_function, tag::within, fe ),
	         bf2 ( tag::basis_function, tag::within, fe );

	// hand-coded integrators require an early declaration of
	// the integrals we intend to compute later (after docking 'fe' on a cell)
	fe .pre_compute ( tag::for_given, tag::basis_functions, bf1, bf2,
	                  tag::integral_of, { bf1 .deriv ( x_torus ) * bf2 .deriv ( x_torus ),
	                                      bf1 .deriv ( x_torus ) * bf2 .deriv ( y_torus ),
	                                      bf1 .deriv ( y_torus ) * bf2 .deriv ( x_torus ),
	                                      bf1 .deriv ( y_torus ) * bf2 .deriv ( y_torus ) } );

	// impose equilibrium equation
	// run over all triangular cells composing 'perforated_torus'
	{ // just a block of code for hiding 'it'
	Mesh::Iterator it = perforated_torus .iterator ( tag::over_cells_of_max_dim );
	for ( it .reset(); it .in_range(); it++ )
	{	Cell small_tri = *it;
		fe .dock_on ( small_tri, tag::winding );
		// run twice over the three vertices of 'small_tri'
		Mesh::Iterator it_V = small_tri .boundary() .iterator ( tag::over_vertices );
		for ( it_V .reset(); it_V .in_range(); it_V ++ )
		{	Cell V = * it_V;
			Cell seg = small_tri .boundary() .cell_in_front_of (V);
			Cell W = V;
			Function psi_V = fe .basis_function (V);
			Manifold::Action winding_V_W = 0; // winding from V to W
			while ( true )
			{	assert ( W == seg .base() .reverse() );
				// V may be the same as W, no problem about that
				Function psi_W = fe .basis_function (W);
				// 'fe' is already docked on 'small_tri' so this will be the domain of integration
				std::vector < double > integrals = fe .integrate
					( tag::pre_computed, tag::replace, bf1, tag::by, psi_W,
				                        tag::replace, bf2, tag::by, psi_V );
				assert ( integrals .size() == 4 );
				// double int_d_psiW_dx_d_psiV_dx = fe .integrate ( d_psiW_dx * d_psiV_dx );
				// double int_d_psiW_dx_d_psiV_dy = fe .integrate ( d_psiW_dx * d_psiV_dy );
				// double int_d_psiW_dy_d_psiV_dx = fe .integrate ( d_psiW_dy * d_psiV_dx );
				// double int_d_psiW_dy_d_psiV_dy = fe .integrate ( d_psiW_dy * d_psiV_dy );
				// tag::Util::Tensor is a class defined in maniFEM for arbitrary order tensors
				tag::Util::Tensor < double > energy (2,2);
				for ( size_t i = 0; i < 2; i ++ )
				for ( size_t k = 0; k < 2; k ++ )
					energy (i,k) = Hooke (i,0,k,0) * integrals [0] +
					               Hooke (i,0,k,1) * integrals [2] +
					               Hooke (i,1,k,0) * integrals [1] +
					               Hooke (i,1,k,1) * integrals [3];
				// 	energy (i,k) = Hooke (i,0,k,0) * int_d_psiW_dx_d_psiV_dx +
				// 	               Hooke (i,0,k,1) * int_d_psiW_dy_d_psiV_dx +
				// 	               Hooke (i,1,k,0) * int_d_psiW_dx_d_psiV_dy +
				// 	               Hooke (i,1,k,1) * int_d_psiW_dy_d_psiV_dy;
				matrix_A .coeffRef ( 2*numbering(V),   2*numbering(W)   ) += energy (0,0);
				matrix_A .coeffRef ( 2*numbering(V),   2*numbering(W)+1 ) += energy (0,1);
				matrix_A .coeffRef ( 2*numbering(V)+1, 2*numbering(W)   ) += energy (1,0);
				matrix_A .coeffRef ( 2*numbering(V)+1, 2*numbering(W)+1 ) += energy (1,1);
				vector_b_case_xx ( 2*numbering(V) ) -=
					   jump_of_u_x_case_xx ( winding_V_W ) * energy(0,0) // +
					 + jump_of_u_y_case_xx ( winding_V_W ) * energy(0,1);
				vector_b_case_xx ( 2*numbering(V)+1 ) -=
					   jump_of_u_x_case_xx ( winding_V_W ) * energy(1,0) // +
					 + jump_of_u_y_case_xx ( winding_V_W ) * energy(1,1);
				vector_b_case_xy ( 2*numbering(V) ) -=
					   jump_of_u_x_case_xy ( winding_V_W ) * energy(0,0) // +
					 + jump_of_u_y_case_xy ( winding_V_W ) * energy(0,1);
				vector_b_case_xy ( 2*numbering(V)+1 ) -=
					   jump_of_u_x_case_xy ( winding_V_W ) * energy(1,0) // +
					 + jump_of_u_y_case_xy ( winding_V_W ) * energy(1,1);
				vector_b_case_yy ( 2*numbering(V) ) -=
					   jump_of_u_x_case_yy ( winding_V_W ) * energy(0,0) // +
					 + jump_of_u_y_case_yy ( winding_V_W ) * energy(0,1);
				vector_b_case_yy ( 2*numbering(V)+1 ) -=
					   jump_of_u_x_case_yy ( winding_V_W ) * energy(1,0) // +
					 + jump_of_u_y_case_yy ( winding_V_W ) * energy(1,1);
				winding_V_W += seg .winding() ;
				W = seg .tip();
				if ( V == W ) break;
				seg = small_tri .boundary() .cell_in_front_of (W);
			}  // end of loop in W
			// here winding_V_W should be zero again
			assert ( winding_V_W == 0 );
		}  }  // end of loop in V, end of loop in small_tri
	} // just a block of code for hiding 'it'

	std::cout << "now solving the systems of linear equations" << std::endl;
	
	matrix_A .makeCompressed();

	Eigen::SparseQR < Eigen::SparseMatrix<double>, Eigen::COLAMDOrdering<int> > solver;

	solver .compute ( matrix_A );
	if ( solver .info() != Eigen::Success )
	{	std::cout << "Eigen solver.compute failed" << std::endl;
		exit ( 0 );                                               }

	// compute microscopic elastic displacement, three load cases
	Eigen::VectorXd vector_sol_case_xx = solver .solve ( vector_b_case_xx );
	if ( solver .info() != Eigen::Success )
	{	std::cout << "Eigen solver.solve failed, case xx" << std::endl;
		exit (1);                                                        }
	Eigen::VectorXd vector_sol_case_xy = solver .solve ( vector_b_case_xy );
	if ( solver .info() != Eigen::Success )
	{	std::cout << "Eigen solver.solve failed, case xy" << std::endl;
		exit (1);                                                        }
	Eigen::VectorXd vector_sol_case_yy = solver .solve ( vector_b_case_yy );
	if ( solver .info() != Eigen::Success )
	{	std::cout << "Eigen solver.solve failed, case yy" << std::endl;
		exit (1);                                                        }

	// we now gather 'vector_sol_case_**' into one large tensor
	// this is a waste of execution time and of computer memory
	// but without this the code becomes extremely verbose
	size_t n_vertices = perforated_torus .number_of ( tag::vertices );
	assert ( number_dofs == 2 * n_vertices );
	tag::Util::Tensor < double > tensor_sol ( 2, 2, n_vertices, 2 );  // (i,j): load case  k: component
	// microscopic elastic displacement
	for ( size_t nv = 0; nv < n_vertices; nv ++ )
	for ( size_t l = 0; l < 2; l++ )
	{	const size_t dof = 2*nv+l;
		tensor_sol ( 0, 0, nv, l ) = vector_sol_case_xx ( dof );
		tensor_sol ( 0, 1, nv, l ) = // vector_sol_case_xy ( dof )
		tensor_sol ( 1, 0, nv, l ) = vector_sol_case_xy ( dof );
		tensor_sol ( 1, 1, nv, l ) = vector_sol_case_yy ( dof );  }

	tag::Util::Tensor < Function::Jump > jump_of_sol (2,2,2);  // (i,j): load case  k: component
	jump_of_sol (0,0,0) = jump_of_u_x_case_xx;
	jump_of_sol (0,0,1) = jump_of_u_y_case_xx;
	jump_of_sol (0,1,0) = // jump_of_u_x_case_xy
	jump_of_sol (1,0,0) = jump_of_u_x_case_xy;
	jump_of_sol (0,1,1) = // jump_of_u_y_case_xy
	jump_of_sol (1,0,1) = jump_of_u_y_case_xy;
	jump_of_sol (1,1,0) = jump_of_u_x_case_yy;
	jump_of_sol (1,1,1) = jump_of_u_y_case_yy;
	
	// there are two ways of computing the homogenized elastic tensor CH
	// first,  as a linear application,  by CH A   = int C euA
	// second, as a quadratic form,      by CH A B = int C euA euB
	// no need to divide by the volume of the torus, it has been normalized to 1
	// see also 'parag-test-cell-scalar.cpp'
	
	// compute CH as linear application
	tag::Util::Tensor < double > macro_stress (2,2,2,2); // (i,j): load case  (k,l): components of stress
	for ( size_t i = 0; i < 2; i++ )
	for ( size_t j = 0; j < 2; j++ )
	for ( size_t k = 0; k < 2; k++ )
	for ( size_t l = 0; l < 2; l++ )
		macro_stress (i,j,k,l) = 0.;
	
	// hand-coded integrators require an early declaration of
	// the integrals we intend to compute later (after docking 'fe' on a cell)
	fe .pre_compute ( tag::for_a_given, tag::basis_function, bf1,
	                  tag::integral_of, { bf1 .deriv ( x_torus ), bf1 .deriv ( y_torus ) } );

	// run over all triangular cells composing 'perforated_torus'
	{ // just a block of code for hiding 'it'
	Mesh::Iterator it = perforated_torus .iterator ( tag::over_cells_of_max_dim );
	for ( it .reset(); it .in_range(); it++ )
	{	Cell small_tri = *it;
		fe .dock_on ( small_tri, tag::winding );
		// run twice over the four vertices of 'small_tri'
		Mesh::Iterator it_V = small_tri .boundary() .iterator ( tag::over_vertices );
		Manifold::Action winding_V = 0; // winding from first V to current V
		for ( it_V .reset(); it_V .in_range(); it_V++ )
		{	Cell V = *it_V;
			Function psi_V = fe .basis_function (V);
			std::vector < double > integrals = fe .integrate
				( tag::pre_computed, tag::replace, bf1, tag::by, psi_V );
			assert ( integrals .size() == 2 );
			for ( size_t i = 0; i < 2; i++ )
			for ( size_t j = 0; j < 2; j++ )
			for ( size_t k = 0; k < 2; k++ )
			for ( size_t l = 0; l < 2; l++ )
			for ( size_t a = 0; a < 2; a++ )
				macro_stress (i,j,k,l) +=
					( Hooke (k,l,a,0) * integrals [0] + Hooke (k,l,a,1) * integrals [1] ) *
				// ( Hooke (k,l,a,b) * fe .integrate ( d_psi_V_dxb )
					( tensor_sol ( i,j, numbering(V), a ) + jump_of_sol ( i,j, a ) ( winding_V ) );
			Cell seg = small_tri .boundary() .cell_in_front_of (V);
			winding_V += seg .winding() ;
		}  // // end of loop in V
		// here winding_V should be zero again
		assert ( winding_V == 0 );
	}  // end of loop in small_tri
	}  // just a block of code for hiding 'it'

	std::cout << "homogenized elastic tensor computed as linear form" << std::endl;
	for ( size_t i = 0; i < 2; i++ )
	for ( size_t j = 0; j < 2; j++ )
	{	for ( size_t k = 0; k < 2; k++ )
		for ( size_t l = 0; l < 2; l++ )
			std::cout << macro_stress (i,j,k,l) << " ";
		std::cout << std::endl;                     }
	
	// compute CH as a quadratic form
	tag::Util::Tensor <double> macro_energy (2,2,2,2);
	for ( size_t i = 0; i < 2; i++ )
	for ( size_t j = 0; j < 2; j++ )
	for ( size_t k = 0; k < 2; k++ )
	for ( size_t l = 0; l < 2; l++ )
		macro_energy (i,j,k,l) = 0.;
	
	// run over all vertices in 'perforated_torus'
	{ // just a block of code for hiding 'it_ver'
	Mesh::Iterator it_ver = perforated_torus .iterator ( tag::over_vertices );
	for ( it_ver .reset(); it_ver .in_range(); it_ver ++ )
	{	Cell V = * it_ver;
		const size_t nv = numbering(V);
		for ( size_t a = 0; a < 2; a++ )
		for ( size_t b = 0; b < 2; b++ )
		{	const size_t dof_a = 2*nv+a;
			const size_t dof_b = 2*nv+b;
			for ( size_t i = 0; i < 2; i++ )
			for ( size_t j = 0; j < 2; j++ )
			for ( size_t k = 0; k < 2; k++ )
			for ( size_t l = 0; l < 2; l++ )
				macro_energy (i,j,k,l) += matrix_A .coeff ( dof_a, dof_b ) // *
					* tensor_sol ( k, l, nv, b ) * tensor_sol ( i, j, nv, a );  }  }
	}  // just a block of code for hiding 'it_ver'

	// run over all segments of 'perforated_torus'
	{ // just a block of code for hiding 'it_seg'
	Mesh::Iterator it_seg = perforated_torus .iterator ( tag::over_cells_of_dim, 1 );
	for ( it_seg .reset(); it_seg .in_range(); it_seg ++ )
	{	Cell seg = * it_seg;
		Cell V = seg .base() .reverse ( tag::surely_exists );
		Cell W = seg .tip();
		const size_t nv = numbering (V);
		const size_t nw = numbering (W);
		for ( size_t a = 0; a < 2; a++ )
		for ( size_t b = 0; b < 2; b++ )
		{	const size_t dof_v_a = 2*nv+a;
			const size_t dof_w_b = 2*nw+b;
			for ( size_t i = 0; i < 2; i++ )
			for ( size_t j = 0; j < 2; j++ )
			for ( size_t k = 0; k < 2; k++ )
			for ( size_t l = 0; l < 2; l++ )
				macro_energy (i,j,k,l) += matrix_A .coeff ( dof_v_a, dof_w_b ) // *
					* tensor_sol ( k, l, nw, b ) * tensor_sol ( i, j, nv, a );  }  }
	}  // just a block of code for hiding 'it_seg'

	// hand-coded integrators require an early declaration of
	// the integrals we intend to compute later (after docking 'fe' on a cell)
	fe .pre_compute ( tag::for_given, tag::basis_functions, bf1, bf2,
	                  tag::integral_of, { bf1 .deriv ( x_torus ) * bf2 .deriv ( x_torus ),
	                                      bf1 .deriv ( x_torus ) * bf2 .deriv ( y_torus ),
	                                      bf1 .deriv ( y_torus ) * bf2 .deriv ( x_torus ),
	                                      bf1 .deriv ( y_torus ) * bf2 .deriv ( y_torus ) } );

	// run over all triangular cells composing 'entire_torus', skipping those with zero winding
	{ // just a block of code for hiding 'it_tri'
	Mesh::Iterator it_tri = entire_torus .iterator ( tag::over_cells_of_max_dim );
	for ( it_tri .reset(); it_tri .in_range(); it_tri ++ )
	{	Cell small_tri = * it_tri;
		fe .dock_on ( small_tri, tag::winding );
		// run twice over the three vertices of 'small_tri'
		Mesh::Iterator it_V = small_tri .boundary() .iterator ( tag::over_vertices );
		it_V .reset();  assert ( it_V .in_range() );
		Cell A = * it_V;
		Cell AB = small_tri .boundary() .cell_in_front_of ( A, tag::surely_exists );
		Cell B = AB .tip();
		Cell BC = small_tri .boundary() .cell_in_front_of ( B, tag::surely_exists );
		Cell C = BC .tip();
		Manifold::Action winding_AB = AB .winding(),
		                 winding_BC = BC .winding();
		if ( ( winding_AB == 0 ) and ( winding_BC == 0 ) )  continue;
		Manifold::Action winding_AC = winding_AB + winding_BC;
		assert (   small_tri .boundary() .cell_in_front_of ( C, tag::surely_exists ) .winding() // +
		         + winding_AC                                                                   == 0 );
		Function psi_A = fe .basis_function (A),
		         psi_B = fe .basis_function (B),
		         psi_C = fe .basis_function (C);
		// 'fe' is already docked on 'small_tri' so this will be the domain of integration
		std::vector < double > integrals = fe .integrate
			( tag::pre_computed, tag::replace, bf1, tag::by, psi_B,
			                     tag::replace, bf2, tag::by, psi_B );
		assert ( integrals .size() == 4 );
		// double int_d_psiB_dx_d_psiB_dx = fe .integrate ( d_psiB_dx * d_psiB_dx );
		// double int_d_psiB_dx_d_psiB_dy = fe .integrate ( d_psiB_dx * d_psiB_dy );
		// double int_d_psiB_dy_d_psiB_dx = fe .integrate ( d_psiB_dy * d_psiB_dx );
		// double int_d_psiB_dy_d_psiB_dy = fe .integrate ( d_psiB_dy * d_psiB_dy );
		// tag::Util::Tensor is a class defined in maniFEM for arbitrary order tensors
		tag::Util::Tensor < double > energy (2,2);
		for ( size_t i = 0; i < 2; i ++ )
		for ( size_t k = 0; k < 2; k ++ )
			energy (i,k) = Hooke (i,0,k,0) * integrals [0] +
			               Hooke (i,0,k,1) * integrals [2] +
			               Hooke (i,1,k,0) * integrals [1] +
			               Hooke (i,1,k,1) * integrals [3];
		// 	energy (i,k) = Hooke (i,0,k,0) * int_d_psi_B_dx_d_psi_B_dx +
		// 	               Hooke (i,0,k,1) * int_d_psi_B_dy_d_psi_B_dx +
		// 	               Hooke (i,1,k,0) * int_d_psi_B_dx_d_psi_B_dy +
		// 	               Hooke (i,1,k,1) * int_d_psi_B_dy_d_psi_B_dy;


		
		
	std::cout << "homogenized elastic tensor computed as quadratic form" << std::endl;
	for ( size_t i = 0; i < 2; i++ )
	for ( size_t j = 0; j < 2; j++ )
	{	for ( size_t k = 0; k < 2; k++ )
		for ( size_t l = 0; l < 2; l++ )
			std::cout << macro_energy (i,j,k,l) << " ";
		std::cout << std::endl;                         }
	
	// compute volume of the hole, as integral on the boundary of the hole of x dy
	double vol_hole = 0.;
	// compute derivative of vol_frac with respect to m_1, m_2 and q
	double d_vol_frac_d_ms = 0., d_vol_frac_d_mt = 0., d_vol_frac_d_q = 0.;
	// run over all segments on the boundary of the hole
	{ // just a block of code for hiding 'it' and 'counter'
	Mesh::Iterator it = inner .iterator ( tag::over_segments, tag::orientation_compatible_with_mesh );
	for ( it .reset() ; it .in_range(); it ++ )
	{	Cell seg = * it;
		Cell A = seg .base() .reverse ( tag::surely_exists );
		Cell B = seg .tip();
		const double x_A = coord_x (A), y_A = coord_y (A), x_B = coord_x (B), y_B = coord_y (B);
		// const double average_x = ( x_A + x_B ) / 2., average_y = ( y_A + y_B ) / 2.;
		const double dx = x_B - x_A, dy = y_B - y_A;
		vol_hole += ( x_A + x_B ) * dy;  //  division by 2 delayed
		const double len = std::sqrt ( dx*dx + dy*dy );
		const double s_A = coord_s (A), t_A = coord_t (A), s_B = coord_s (B), t_B = coord_t (B);
		// s and t are between -0.5 and 0.5
		// below, we delay the division by two
		const double average_s = s_A + s_B, average_t = t_A + t_B;  //double of the average
		const double sign_s = sign ( average_s ), sign_t = sign ( average_t );
		const double abs_s = sign_s * average_s, abs_t = sign_t * average_t;  // double
		// below, we rotate the tangent vector counter-clockwise
		// to get a normal vector pointing inside the hole
		const double n_x = - dy / len, n_y = dx / len;
		// below, we should divide by 'super_ellipse_param_m_st_over_2'
		// but, since the division by 2 in 'average' was delayed,
		// we use instead 'super_ellipse_param_m_st' and the factor of 2 simplifies
		const double frac_s = abs_s / super_ellipse_param_m_s,
		             frac_t = abs_t / super_ellipse_param_m_t;
		const double frac_s_to_power_q_m1 = std::pow ( frac_s, super_ellipse_param_q_minus_1 );
		const double frac_s_to_power_q = frac_s_to_power_q_m1 * frac_s;
		const double frac_t_to_power_q_m1 = std::pow ( frac_t, super_ellipse_param_q_minus_1 );
		const double frac_t_to_power_q = frac_t_to_power_q_m1 * frac_t;
		const double factor_in_front_of_displ =  // later multiplied by q
			  (  sign_s * frac_s_to_power_q_m1 / super_ellipse_param_m_s_over_2 * D_phi_s_d_x // +
			   + sign_t * frac_t_to_power_q_m1 / super_ellipse_param_m_t_over_2 * D_phi_t_d_x ) * n_x // +
			+ (  sign_s * frac_s_to_power_q_m1 / super_ellipse_param_m_s_over_2 * D_phi_s_d_y // +
			   + sign_t * frac_t_to_power_q_m1 / super_ellipse_param_m_t_over_2 * D_phi_t_d_y ) * n_y;
		// terms below have passed on the other side of equality, whence the switched sign
		const double factor_in_front_of_dms =  // later multiplied by q
			frac_s_to_power_q / super_ellipse_param_m_s;
		const double factor_in_front_of_dmt =  // later multiplied by q
			frac_t_to_power_q / super_ellipse_param_m_t;
		const double factor_in_front_of_dq = - std::log ( frac_s ) * frac_s_to_power_q // -
		                                     - std::log ( frac_t ) * frac_t_to_power_q;
		// below, q simplifies
		const double d_displ_associated_to_dms = factor_in_front_of_dms / factor_in_front_of_displ;
		// const double dx_associated_to_dms = d_displ_associated_to_dms * nx,
		//              dy_associated_to_dms = d_displ_associated_to_dms * ny;
		d_vol_frac_d_ms += d_displ_associated_to_dms * len;
		// below, q simplifies
		const double d_displ_associated_to_dmt = factor_in_front_of_dmt / factor_in_front_of_displ;
		// const double dx_associated_to_dms = d_displ_associated_to_dmt * nx,
		//              dy_associated_to_dmt = d_displ_associated_to_dmt * ny;
		d_vol_frac_d_mt += d_displ_associated_to_dmt * len;
		// below we need to divide by q, unlike above
		const double d_displ_associated_to_dq =
			factor_in_front_of_dq / factor_in_front_of_displ / super_ellipse_param_q;
		// const double dx_associated_to_dq = d_displ_associated_to_dq * nx,
		//              dy_associated_to_dq = d_displ_associated_to_dq * ny;
		d_vol_frac_d_q += d_displ_associated_to_dq * len;
	}  // end of 'for' loop
	} // just a block of code
	vol_hole /= 2.;
	// no need to divide by volume of torus to get volume fractions, it has been rescaled to 1.
	// relevant quantity is volume (fraction) of material
	// vol_frac = vol_torus - vol_hole = 1. - vol_hole
	std::cout << "dv/dms " << d_vol_frac_d_ms << ", dv/dmt " << d_vol_frac_d_mt
	          << ", dv/dq " << d_vol_frac_d_q << std::endl;
	
	perforated_torus .unfold ( tag::over_region, coord_x*coord_x*coord_x*coord_x  // +
	                                        + 2.*coord_y*coord_y*coord_y*coord_y < 15. )
			.export_to_file ( tag::gmsh, "torus.msh" );
	
}  // end of main


//-----------------------------------------------------------------------------------------

