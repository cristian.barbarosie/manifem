
// example presented in paragraph 5.1 of the manual
// https://maniFEM.rd.ciencias.ulisboa.pt/manual-maniFEM.pdf
// writing composite meshes

#include "maniFEM.h"
#include "math.h"

using namespace maniFEM;


int main ()

{	Manifold RR3 ( tag::Euclid, tag::of_dim, 3 );
	Function xyz = RR3 .build_coordinate_system ( tag::Lagrange, tag::of_degree, 1 );
	Function x = xyz [0], y = xyz [1], z = xyz [2];

	const double rs = 1.;    // radius of the sphere
	const double rc = 0.45;  // radius of the cylinder
	const double seg_size = 0.1;
	
	Manifold cylinder = RR3 .implicit ( y*y + (z-0.5)*(z-0.5) == rc*rc );
	Manifold sphere = RR3 .implicit ( x*x + y*y + z*z == rs*rs );

	// base of the cylinder :
	cylinder .implicit ( x == 1.5 );
	Cell start_1 ( tag::vertex, tag::of_coords, { 1.5, 0., 0.5 + rc } );
	Mesh circle_1 = Mesh::Build ( tag::frontal ) .entire_manifold()
		.start_at ( start_1 ) .towards ( { 0., 1., 0. } ) .desired_length ( seg_size );

	// intersection between the cylinder and the sphere
	Manifold interface ( tag::intersect, cylinder, sphere );
	Cell start_2 ( tag::vertex, tag::of_coords, { 1., 0., 0.5 - rc }, tag::project );
	Mesh circle_2 = Mesh::Build ( tag::frontal ) .entire_manifold()
		.start_at ( start_2 ) .towards ( { 0., -1., 0. } ) .desired_length ( seg_size );

	Mesh circles = Mesh::Build ( tag::join ) .meshes ( { circle_1, circle_2 .reverse() } );

	cylinder .set_as_working_manifold();
	Mesh cyl = Mesh::Build ( tag::frontal ) .boundary ( circles )
		.start_at ( start_1 ) .towards ( { -1., 0., 0. } ) .desired_length ( seg_size );

	sphere .set_as_working_manifold();
	Mesh sph = Mesh::Build ( tag::frontal ) .boundary ( circle_2 )
		.start_at ( start_2 ) .towards ( { 0., 0., -1. } ) .desired_length ( seg_size );

	Mesh::Composite sphere_cyl = Mesh::Build ( tag::gather )
		.cell ( "start 1", start_1 )
		// we keep 'start_1' because it may be needed for building another mesh from 'circle_1'
		// it is very unlikely for 'start_2' to be useful so we choose not to keep it
		.mesh ( "circle 1", circle_1 )
		.mesh ( "circle 2", circle_2 )
		.mesh ( "cylinder", cyl )
		.mesh ( "sphere", sph );

	sphere_cyl .export_to_file ( tag::gmsh, "sphere-cyl-composite.msh" );
	std::cout << "produced file sphere-cyl-composite.msh" << std::endl;

}  // end of main
