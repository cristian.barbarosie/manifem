
// example presented in paragraph 11.3 of the manual
// https://maniFEM.rd.ciencias.ulisboa.pt/manual-maniFEM.pdf
// triangular Lagrange P1 finite elements, 1D integrals

#include "maniFEM.h"
#include "math.h"

#include <fstream>
#include <Eigen/SparseCore>
#include <Eigen/IterativeLinearSolvers>

using namespace maniFEM;


void impose_value_of_unknown  // fast  // see below for an equivalent, more readable, version
( Eigen::SparseMatrix < double > & matrix_A, Eigen::VectorXd & vector_b,
  const size_t i, const double val                                      )

// in a system of linear equations, destroy equation 'i' and impose u(i) = val
// change also column 'i' of the matrix, just to preserve symmetry

// used for imposing Dirichlet boundary conditions

{	assert ( ! matrix_A .IsRowMajor );
	const size_t size_matrix = matrix_A .innerSize();

	// destroy row i, even the diagonal entry, change vector_b
	for ( size_t j = 0; j < size_matrix; j++ )
	{	if ( matrix_A .coeff ( i, j ) != 0. )
			matrix_A .coeffRef ( i, j ) = 0.;
		vector_b (j) -= matrix_A .coeff ( j, i ) * val;  }

	// destroy column i, set diagonal entry to 1.
	const size_t oi = matrix_A .outerIndexPtr() [i];     // access to matrix_A .m_outerIndex
	// std::cout << i << " " << oi << " " << matrix_A .innerNonZeroPtr() [i] << " "
	//           << matrix_A .innerIndexPtr() [ oi ] << " " << matrix_A .valuePtr() [ oi ] << std::endl;
	matrix_A .innerNonZeroPtr() [i] = 1;                 // access to matrix_A .m_innerNonZeros
	matrix_A .innerIndexPtr() [ oi ] = i;                // access to matrix_A .m_data .m_indices
	matrix_A .valuePtr() [ oi ] = 1.;                    // access to matrix_A .m_data .m_values

	// change vector
	vector_b (i) = val;                                     }


void impose_value_of_unknown_readable
( Eigen::SparseMatrix < double > & matrix_A, Eigen::VectorXd & vector_b,
  const size_t i, const double val                                      )

// in a system of linear equations, destroy equation 'i' and impose u(i) = val
// change also column 'i' of the matrix, just to preserve symmetry

// used for imposing Dirichlet boundary conditions

{	assert ( ! matrix_A .IsRowMajor );
	size_t size_matrix = matrix_A .innerSize();

	// destroy raw i, even the diagonal entry
	for ( size_t j = 0; j < size_matrix; j++ )
		if ( matrix_A .coeff ( i, j ) != 0. )
			matrix_A .coeffRef ( i, j ) = 0.;

	// destroy column i, change vector
	for ( size_t j = 0; j < size_matrix; j++ )
	{	if ( matrix_A .coeff ( j, i ) == 0. )  continue;
		double & Aji = matrix_A .coeffRef ( j, i );
		vector_b (j) -= Aji * val;
		Aji = 0.;                                         }

	// set diagonal entry to 1., change vector
	matrix_A .coeffRef ( i, i ) = 1.;
	vector_b (i) = val;                                     }

//------------------------------------------------------------------------------------------------------//


int main ()

{	Manifold RR2 ( tag::Euclid, tag::of_dim, 2 );
	Function xy = RR2 .build_coordinate_system ( tag::Lagrange, tag::of_degree, 1 );
	Function x = xy[0], y = xy[1];

	// declare the type of finite element
	FiniteElement fe ( tag::with_master, tag::triangle, tag::Lagrange, tag::of_degree, 1 );
	Integrator integ = fe .set_integrator ( tag::Gauss, tag::tri_6 );
	FiniteElement fe_bdry ( tag::with_master, tag::segment, tag::Lagrange, tag::of_degree, 1 );
	Integrator integ_bdry = fe_bdry .set_integrator ( tag::Gauss, tag::seg_3 );

	// build a 10x10 square mesh, with triangles
	Cell A ( tag::vertex );  x(A) = 0.;   y(A) = 0.;
	Cell B ( tag::vertex );  x(B) = 1.;   y(B) = 0.;
	Cell C ( tag::vertex );  x(C) = 1.;   y(C) = 1.;
	Cell D ( tag::vertex );  x(D) = 0.;   y(D) = 1.;
	Mesh AB = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( A ) .stop_at ( B ) .divided_in ( 10 );
	Mesh BC = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( B ) .stop_at ( C ) .divided_in ( 12 );
	Mesh CD = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( C ) .stop_at ( D ) .divided_in ( 10 );
	Mesh DA = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( D ) .stop_at ( A ) .divided_in ( 12 );
	Mesh ABCD = Mesh::Build ( tag::grid ) .shape ( tag::rectangle )
		.faces ( AB, BC, CD, DA ) .with_triangles();

	// below we build a Cell::Numbering with tag::map, which creates internally a Cell::Numbering::Map
	// this is just an  std::map < Cell, size_t >  disguised
	// a more efficient numbering is shown in paragraph 11.4 of the manual
	Cell::Numbering numbering ( tag::map );
	{ // just a block of code for hiding 'it' and 'counter'
	Mesh::Iterator it = ABCD .iterator ( tag::over_vertices );
	size_t counter = 0;
	for ( it .reset(); it .in_range(); it++ )
	{	Cell V = *it;  numbering (V) = counter;  ++counter;  }
	assert ( counter == numbering .size() );
	} // just a block of code

	const size_t size_matrix = ABCD .number_of ( tag::vertices );
	assert ( size_matrix == numbering .size() );
	std::cout << "global matrix " << size_matrix << "x" << size_matrix << std::endl;
	Eigen::SparseMatrix < double > matrix_A ( size_matrix, size_matrix );
	Eigen::VectorXd vector_b ( size_matrix );
	vector_b .setZero();

	matrix_A .reserve ( Eigen::VectorXi::Constant ( size_matrix, 7 ) );
	// since we will be working with a mesh of triangles,
	// there will be about 7 non-zero elements per column
	// the diagonal entry plus six neighbour vertices

	// run over all triangular cells composing ABCD
	{ // just a block of code for hiding 'it'
	Mesh::Iterator it = ABCD .iterator ( tag::over_cells_of_max_dim );
	for ( it .reset(); it .in_range(); it++ )
	{	Cell small_tri = *it;
		fe .dock_on ( small_tri );
		// run twice over the three vertices of 'small_tri'
		Mesh::Iterator it1 = small_tri .boundary() .iterator ( tag::over_vertices );
		Mesh::Iterator it2 = small_tri .boundary() .iterator ( tag::over_vertices );
		for ( it1 .reset(); it1 .in_range(); it1++ )
		for ( it2 .reset(); it2 .in_range(); it2++ )
		{	Cell V = *it1, W = *it2;  // V may be the same as W, no problem about that
			Function psi_V = fe .basis_function (V),
			         psi_W = fe .basis_function (W),
			         d_psi_V_dx = psi_V .deriv (x),
			         d_psi_V_dy = psi_V .deriv (y),
			         d_psi_W_dx = psi_W .deriv (x),
			         d_psi_W_dy = psi_W .deriv (y);
			// 'fe' is already docked on 'small_tri' so this will be the domain of integration
			matrix_A .coeffRef ( numbering (V), numbering (W) ) +=
				fe .integrate ( d_psi_V_dx * d_psi_W_dx + d_psi_V_dy * d_psi_W_dy );  }  }
	} // just a block of code 

	Function heat_source = y*y;
	// impose Neumann boundary conditions du/dn = heat_source
	{ // just a block of code for hiding 'it'
	Mesh::Iterator it = DA .iterator ( tag::over_segments );
	for ( it .reset(); it .in_range(); it++ )
	{	Cell seg = *it;
		fe_bdry .dock_on ( seg );
		Cell V = seg .base() .reverse();
		assert ( V .is_positive() );
		const size_t i = numbering (V);
		Function psi_V = fe_bdry .basis_function (V);
		vector_b [i] += fe_bdry .integrate ( heat_source * psi_V );
		Cell W = seg .tip();
		assert ( W .is_positive() );
		const size_t j = numbering (W);
		Function psi_W = fe_bdry .basis_function (W);
		vector_b [j] += fe_bdry .integrate ( heat_source * psi_W );   }
	} // just a block of code 
	
	// impose Dirichlet boundary conditions  u = 0
	{ // just a block of code for hiding 'it'
	Mesh::Iterator it = AB .iterator ( tag::over_vertices );
	for ( it .reset(); it .in_range(); it++ )
	{	Cell P = *it;
		const size_t i = numbering (P);
		impose_value_of_unknown ( matrix_A, vector_b, i, 0. );  }
	} { // just a block of code for hiding 'it' 
	Mesh::Iterator it = BC .iterator ( tag::over_vertices );
	for ( it .reset(); it .in_range(); it++ )
	{	Cell P = *it;
		const size_t i = numbering (P);
		impose_value_of_unknown ( matrix_A, vector_b, i, 0. );  }
	} { // just a block of code for hiding 'it'
	Mesh::Iterator it = CD .iterator ( tag::over_vertices );
	for ( it .reset(); it .in_range(); it++ )
	{	Cell P = *it;
		const size_t i = numbering (P);
		impose_value_of_unknown ( matrix_A, vector_b, i, 0. );  }
	} // just a block of code 

	// solve the system of linear equations
	Eigen::ConjugateGradient < Eigen::SparseMatrix < double >,
	                           Eigen::Lower | Eigen::Upper    > cg;
	cg .compute ( matrix_A );

	Eigen::VectorXd vector_sol = cg .solve ( vector_b );
	if ( cg .info() != Eigen::Success )
	{	std::cout << "Eigen solver.solve failed" << std::endl;
		exit (1);                                               }
		
	ABCD .export_to_file ( tag::gmsh, "square-Neumann.msh", numbering );
	{ // just a block of code for hiding variables
	std::ofstream solution_file ( "square-Neumann.msh", std::fstream::app );
	solution_file << "$NodeData" << std::endl;
	solution_file << "1" << std::endl;   // one string follows
	solution_file << "\"temperature\"" << std::endl;
	solution_file << "1" << std::endl;   //  one real follows
	solution_file << "0.0" << std::endl;  // time [??]
	solution_file << "3" << std::endl;   // three integers follow
	solution_file << "0" << std::endl;   // time step [??]
	solution_file << "1" << std::endl;  // scalar values of u
	solution_file << ABCD .number_of ( tag::vertices ) << std::endl;  // number of values listed below
	Mesh::Iterator it = ABCD .iterator ( tag::over_vertices );
	for ( it .reset(); it .in_range(); it++ )
	{	Cell P = *it;
		const size_t i = numbering (P);
		solution_file << i+1 << " " << vector_sol[i] << std::endl;   }
	} // just a block of code

	std::cout << "produced file square-Neumann.msh" << std::endl;

	return 0;

}  // end of main

