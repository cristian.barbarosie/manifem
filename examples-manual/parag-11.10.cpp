
// example presented in paragraph 11.10 of the manual
// https://maniFEM.rd.ciencias.ulisboa.pt/manual-maniFEM.pdf
// solve Laplace equation in a cube with hexahedral finite elements

#include "maniFEM.h"
#include <fstream>
#include <Eigen/SparseCore>
#include <Eigen/IterativeLinearSolvers>

using namespace maniFEM;


void impose_value_of_unknown  // fast  // see below for an equivalent, more readable, version
( Eigen::SparseMatrix < double > & matrix_A, Eigen::VectorXd & vector_b,
  const size_t i, const double val                                      )

// in a system of linear equations, destroy equation 'i' and impose u(i) = val
// change also column 'i' of the matrix, just to preserve symmetry

// used for imposing Dirichlet boundary conditions

{	assert ( ! matrix_A .IsRowMajor );
	const size_t size_matrix = matrix_A .innerSize();

	// destroy row i, even the diagonal entry, change vector_b
	for ( size_t j = 0; j < size_matrix; j++ )
	{	if ( matrix_A .coeff ( i, j ) != 0. )
			matrix_A .coeffRef ( i, j ) = 0.;
		vector_b (j) -= matrix_A .coeff ( j, i ) * val;  }

	// destroy column i, set diagonal entry to 1.
	const size_t oi = matrix_A .outerIndexPtr() [i];     // access to matrix_A .m_outerIndex
	// std::cout << i << " " << oi << " " << matrix_A .innerNonZeroPtr() [i] << " "
	//           << matrix_A .innerIndexPtr() [ oi ] << " " << matrix_A .valuePtr() [ oi ] << std::endl;
	matrix_A .innerNonZeroPtr() [i] = 1;                 // access to matrix_A .m_innerNonZeros
	matrix_A .innerIndexPtr() [ oi ] = i;                // access to matrix_A .m_data .m_indices
	matrix_A .valuePtr() [ oi ] = 1.;                    // access to matrix_A .m_data .m_values

	// change vector
	vector_b (i) = val;                                     }


void impose_value_of_unknown_readable
( Eigen::SparseMatrix < double > & matrix_A, Eigen::VectorXd & vector_b,
  const size_t i, const double val                                      )

// in a system of linear equations, destroy equation 'i' and impose u(i) = val
// change also column 'i' of the matrix, just to preserve symmetry

// used for imposing Dirichlet boundary conditions

{	assert ( ! matrix_A .IsRowMajor );
	const size_t size_matrix = matrix_A .innerSize();

	// destroy raw i, even the diagonal entry
	for ( size_t j = 0; j < size_matrix; j++ )
		if ( matrix_A .coeff ( i, j ) != 0. )
			matrix_A .coeffRef ( i, j ) = 0.;

	// destroy column i, change vector
	for ( size_t j = 0; j < size_matrix; j++ )
	{	if ( matrix_A .coeff ( j, i ) == 0. )  continue;
		double & Aji = matrix_A .coeffRef ( j, i );
		vector_b (j) -= Aji * val;
		Aji = 0.;                                         }

	// set diagonal entry to 1., change vector
	matrix_A .coeffRef ( i, i ) = 1.;
	vector_b (i) = val;                                     }

//------------------------------------------------------------------------------------------------------//


int main ()
	
{	Manifold RR3 ( tag::Euclid, tag::of_dim, 3 );
	Function xyz = RR3 .build_coordinate_system ( tag::Lagrange, tag::of_degree, 1 );
	Function x = xyz [0], y = xyz [1], z = xyz [2];

	Cell A ( tag::vertex );  x(A) = 0.;  y(A) = 0.;  z(A) = 0.;
	Cell B ( tag::vertex );  x(B) = 1.;  y(B) = 0.;  z(B) = 0.;
	Cell C ( tag::vertex );  x(C) = 1.;  y(C) = 0.;  z(C) = 1.;
	Cell D ( tag::vertex );  x(D) = 0.;  y(D) = 0.;  z(D) = 1.;
	Cell E ( tag::vertex );  x(E) = 0.;  y(E) = 1.;  z(E) = 0.;
	Cell F ( tag::vertex );  x(F) = 1.;  y(F) = 1.;  z(F) = 0.;
	Cell G ( tag::vertex );  x(G) = 1.;  y(G) = 1.;  z(G) = 1.;
	Cell H ( tag::vertex );  x(H) = 0.;  y(H) = 1.;  z(H) = 1.;

	Mesh cube = Mesh::Build ( tag::grid ) .shape ( tag::cube )
	  .vertices ( A, B, C, D, E, F, G, H ) .divided_in ( 10, 10, 10 );
	Mesh bdry_cube = cube .boundary();

	FiniteElement fe ( tag::with_master, tag::hexahedron, tag::Lagrange, tag::of_degree, 1 );
	fe .set_integrator ( tag::Gauss, tag::cube_8 );
	#ifndef NDEBUG
	std::cout << fe .info();
	#endif
	
	std::map < Cell, size_t > numbering;
	{ // just a block of code for hiding 'it' and 'counter'
	Mesh::Iterator it = cube .iterator ( tag::over_vertices );
	size_t counter = 0;
	for ( it .reset(); it .in_range(); it++ )
	{	Cell V = *it;  numbering [V] = counter;  ++counter;  }
	assert ( counter == numbering .size() );
	} // just a block of code

	const size_t size_matrix = numbering .size();
	std::cout << "global matrix " << size_matrix << "x" << size_matrix << std::endl;
	Eigen::SparseMatrix <double> matrix_A ( size_matrix, size_matrix );
	
	matrix_A .reserve ( Eigen::VectorXi::Constant ( size_matrix, 21 ) );
	// since we will be working with a mesh of tetrahedra,
	// there will be about 22 non-zero elements per column
	// the diagonal entry plus 21 neighbour vertices

	Eigen::VectorXd vector_b ( size_matrix ), vector_sol ( size_matrix );
	vector_b .setZero();
	
	// run over all tetrahedron cells composing the ball
	{ // just a block of code for hiding 'it'
	Mesh::Iterator it = cube .iterator ( tag::over_cells_of_max_dim );
	for ( it .reset(); it .in_range(); it++ )
	{	Cell small_cube = *it;
		fe .dock_on ( small_cube );
		// run twice over the four vertices of 'small_cube'
		Mesh::Iterator it1 = small_cube .boundary() .iterator ( tag::over_vertices );
		Mesh::Iterator it2 = small_cube .boundary() .iterator ( tag::over_vertices );
		for ( it1 .reset(); it1 .in_range(); it1++ )
		for ( it2 .reset(); it2 .in_range(); it2++ )
		{	Cell V = *it1, W = *it2;  // V may be the same as W, no problem about that
			Function psiV = fe .basis_function (V),
			         psiW = fe .basis_function (W),
			         d_psiV_dx = psiV .deriv (x),
			         d_psiV_dy = psiV .deriv (y),
			         d_psiV_dz = psiV .deriv (z),
			         d_psiW_dx = psiW .deriv (x),
			         d_psiW_dy = psiW .deriv (y),
			         d_psiW_dz = psiW .deriv (z);
			// 'fe' is already docked on 'small_cube' so this will be the domain of integration
			matrix_A .coeffRef ( numbering [V], numbering [W] ) +=
				fe .integrate ( d_psiV_dx * d_psiW_dx + d_psiV_dy * d_psiW_dy + d_psiV_dz * d_psiW_dz );  }  }
	} // just a block of code 
	
	// impose Dirichlet boundary conditions  u = xyz
	{ // just a block of code for hiding 'it'
	Mesh::Iterator it = bdry_cube .iterator ( tag::over_vertices );
	for ( it .reset(); it .in_range(); it++ )
	{	Cell P = *it;
		const size_t i = numbering [P];
		impose_value_of_unknown ( matrix_A, vector_b, i, x(P) * y(P) * z(P) );  }
	}  // just a block of code
	
	// solve the system of linear equations
	Eigen::ConjugateGradient < Eigen::SparseMatrix < double >,
	                           Eigen::Lower | Eigen::Upper    > cg;
	cg .compute ( matrix_A );

	vector_sol = cg .solve ( vector_b );
	if ( cg .info() != Eigen::Success )
	{	std::cout << "Eigen solver.solve failed" << std::endl;
		exit (1);                                               }
		
	cube .export_to_file ( tag::gmsh, "cube-Dirichlet.msh", numbering );

	{ // just a block of code for hiding variables
	std::ofstream solution_file ( "cube-Dirichlet.msh", std::fstream::app );
	solution_file << "$NodeData" << std::endl;
	solution_file << "1" << std::endl;   // one string follows
	solution_file << "\"temperature\"" << std::endl;
	solution_file << "1" << std::endl;   //  one real follows
	solution_file << "0.0" << std::endl;  // time [??]
	solution_file << "3" << std::endl;   // three integers follow
	solution_file << "0" << std::endl;   // time step [??]
	solution_file << "1" << std::endl;  // scalar values of u
	solution_file << cube .number_of ( tag::vertices ) << std::endl;  // number of values listed below
	Mesh::Iterator it = cube .iterator ( tag::over_vertices );
	for ( it .reset(); it .in_range(); it++ )
	{	Cell P = *it;
		const size_t i = numbering [P];
		solution_file << i+1 << " " << vector_sol [i] << std::endl;   }
	} // just a block of code

	std::cout << "produced file cube-Dirichlet.msh" << std::endl;

}  // end of main

