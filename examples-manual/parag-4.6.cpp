
// example presented in paragraph 4.6 of the manual
// https://maniFEM.rd.ciencias.ulisboa.pt/manual-maniFEM.pdf
// extruded mesh, track is closed loop

#include "maniFEM.h"

using namespace maniFEM;
using namespace std;


int main ()

{	Manifold RR2 ( tag::Euclid, tag::of_dim, 2 );
	Function xy = RR2 .build_coordinate_system ( tag::Lagrange, tag::of_degree, 1 );
	Function x = xy[0], y = xy[1];

	RR2 .implicit ( x*x + y*y == 1. );

	Cell N ( tag::vertex );  x(N) =  0.;  y(N) =  1.;
	Cell W ( tag::vertex );  x(W) = -1.;  y(W) =  0.;
	Cell S ( tag::vertex );  x(S) =  0.;  y(S) = -1.;
	Cell E ( tag::vertex );  x(E) =  1.;  y(E) =  0.;

	Mesh NW = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at (N) .stop_at (W) .divided_in ( 10 );
	Mesh WS = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at (W) .stop_at (S) .divided_in ( 10 );
	Mesh SE = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at (S) .stop_at (E) .divided_in ( 10 );
	Mesh EN = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at (E) .stop_at (N) .divided_in ( 10 );

	Mesh circle = Mesh::Build ( tag::join ) .meshes ( { NW, WS, SE, EN } );

	RR2 .implicit ( y ==  (x-1.)*(x-1.) );
	  
	Cell X ( tag::vertex );  x(X) = 2.;   y(X) = 1.;
	Cell Y ( tag::vertex );  x(Y) = 0.5;  y(Y) = 0.25;
	Mesh XE = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at (X) .stop_at (E) .divided_in ( 10 );
	Mesh EY = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at (E) .stop_at (Y) .divided_in (5);
	Mesh XY = Mesh::Build ( tag::join ) .meshes ( { XE, EY } );

	RR2 .set_as_working_manifold();

	Mesh gelly = Mesh::Build ( tag::extrude ) .swatch ( XY ) .slide_along ( circle ) .follow_curvature();
	
	Mesh::Iterator it = XY .iterator ( tag::over_cells_of_max_dim );
	for ( it .reset(); it .in_range(); it ++ )
		assert ( (*it) .belongs_to ( gelly ) );
	it = circle .iterator ( tag::over_cells_of_max_dim );
	for ( it .reset(); it .in_range(); it ++ )
		assert ( (*it) .belongs_to ( gelly ) );
	
	gelly .export_to_file ( tag::gmsh, "annulus.msh" );
	std::cout << "produced file annulus.msh" << std::endl;

}  // end of main

