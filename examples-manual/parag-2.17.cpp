
// example presented in paragraph 2.17 of the manual
// https://maniFEM.rd.ciencias.ulisboa.pt/manual-maniFEM.pdf
// build a circle in 3d (implicit manifold with two equations)

#include "maniFEM.h"
#include "math.h"

using namespace maniFEM;

int main ()

{	Manifold RR3 ( tag::Euclid, tag::of_dim, 3 );
	Function xyz = RR3 .build_coordinate_system ( tag::Lagrange, tag::of_degree, 1 );
	Function x = xyz [0], y = xyz [1], z = xyz [2];

	RR3 .implicit ( x*x + y*y == 1., x*y == 4.*z );
	Cell S ( tag::vertex );  x(S) =  0.;  y(S) = -1.;  z(S) = 0.;
	Cell E ( tag::vertex );  x(E) =  1.;  y(E) =  0.;  z(E) = 0.;
	Cell N ( tag::vertex );  x(N) =  0.;  y(N) =  1.;  z(N) = 0.;
	Cell W ( tag::vertex );  x(W) = -1.;  y(W) =  0.;  z(W) = 0.;
	Mesh SE = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( S ) .stop_at ( E ) .divided_in ( 5 );
	Mesh EN = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( E ) .stop_at ( N ) .divided_in ( 5 );
	Mesh NW = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( N ) .stop_at ( W ) .divided_in ( 5 );
	Mesh WS = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( W ) .stop_at ( S ) .divided_in ( 5 );
	Mesh circle = Mesh::Build ( tag::join ) .meshes ( { SE, EN, NW, WS } );

	circle .export_to_file ( tag::eps3d, "circle-3d.eps" );
	circle .export_to_file ( tag::gmsh, "circle-3d.msh" );
	std::cout << "produced files circle-3d.eps and circle-3d.msh" << std::endl;

}  // end of main
