
// example presented in paragraph 3.19 of the manual
// https://maniFEM.rd.ciencias.ulisboa.pt/manual-maniFEM.pdf
// a sphere with a cylinder-shaped tunnel

#include "maniFEM.h"
#include "math.h"

using namespace maniFEM;


int main ()

{	Manifold RR3 ( tag::Euclid, tag::of_dim, 3 );
	Function xyz = RR3 .build_coordinate_system ( tag::Lagrange, tag::of_degree, 1 );
	Function x = xyz [0], y = xyz [1], z = xyz [2];

	const double rs = 1.;    // radius of the sphere
	const double rc = 0.45;  // radius of the cylinder
	const double seg_size = 0.1;
	
	Manifold cylinder = RR3 .implicit ( y*y + (z-0.5)*(z-0.5) == rc*rc );
	Manifold sphere = RR3 .implicit ( x*x + y*y + z*z == rs*rs );
	Manifold interface ( tag::intersect, cylinder, sphere );

	// we choose names start_2 and circle_2 for consistency with paragraph 3.18
	Cell start_2 ( tag::vertex );
	x ( start_2 ) = 1.;  y ( start_2 ) = 0.;  z ( start_2 ) = 0.5 - rc;
	interface .project ( start_2 );
	Mesh circle_2 = Mesh::Build ( tag::frontal ) .entire_manifold()
	  .start_at ( start_2 ) .towards ( { 0., 1., 0. } ) .desired_length ( seg_size );

	Cell start_3 ( tag::vertex );
	x ( start_3 ) = -1.;  y ( start_3 ) = 0.;  z ( start_3 ) = 0.5 - rc;
	interface .project ( start_3 );
	Mesh circle_3 = Mesh::Build ( tag::frontal ) .entire_manifold()
	  .start_at ( start_3 ) .towards ( { 0., 1., 0. } ) .desired_length ( seg_size );

	Mesh two_circles = Mesh::Build ( tag::join ) .meshes ( { circle_2 .reverse(), circle_3 } );

	cylinder .set_as_working_manifold();
	Mesh cyl = Mesh::Build ( tag::frontal ) .boundary ( two_circles )
	  .start_at ( start_2 ) .towards ( { -1., 0., 0. } ) .desired_length ( seg_size );

	sphere .set_as_working_manifold();
	Mesh sph = Mesh::Build ( tag::frontal ) .boundary ( two_circles .reverse() )
	  .start_at ( start_2 ) .towards ( { 0., 0., -1. } ) .desired_length ( seg_size );

	Mesh all = Mesh::Build ( tag::join ) .meshes ( { cyl, sph } );
	
	all .export_to_file ( tag::gmsh, "sphere-tunnel.msh" );
	std::cout << "produced file sphere-tunnel.msh" << std::endl;

}  // end of main
