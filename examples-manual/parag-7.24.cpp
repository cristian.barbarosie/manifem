
// example presented in paragraph 7.24 of the manual
// https://maniFEM.rd.ciencias.ulisboa.pt/manual-maniFEM.pdf
// a disk on the torus

#include "maniFEM.h"

using namespace maniFEM;


int main ( )

{	// begin with the usual two-dimensional space
	Manifold RR2 ( tag::Euclid, tag::of_dim, 2 );
	Function xy = RR2 .build_coordinate_system ( tag::Lagrange, tag::of_degree, 1 );
	Function x = xy[0], y = xy[1];

	Manifold circle_manif = RR2 .implicit ( x*x + y*y == 1. );
	Mesh circle = Mesh::Build ( tag::frontal ) .entire_manifold() .desired_length ( 0.2 );

	Manifold::Action gx ( tag::transforms, xy, tag::into, ( x + 3. ) && y );
	Manifold::Action gy ( tag::transforms, xy, tag::into, ( x + 1.5 ) && ( y + 2.6 ) ); 
	RR2 .quotient ( gx, gy );

	Mesh circle_fold = circle .fold ( tag::use_existing_vertices );

	Mesh perf_torus = Mesh::Build ( tag::frontal )
		.boundary ( circle_fold ) .desired_length ( 0.2 );

	Mesh torus_unf = perf_torus .unfold ( tag::over_region, x*x + 2.*y*y < 20. );

	torus_unf .export_to_file ( tag::gmsh, "perf-torus.msh" );
	torus_unf .export_to_file ( tag::eps,  "perf-torus.eps" );
	std::cout << "produced files perf-torus.msh and perf_torus.eps" << std::endl;

}  // end of  main



