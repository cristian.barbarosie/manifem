
// example presented in paragraph 5.10 of the manual
// https://maniFEM.rd.ciencias.ulisboa.pt/manual-maniFEM.pdf

// read a mesh from file created in parag-5.9.cpp
// read also the finite element matrix created in parag-5.9.cpp

#include "maniFEM.h"
#include <fstream>
#include <Eigen/SparseCore>

using namespace maniFEM;


// Léo Moreau wrote 'read_sparse_matrix' inspired in Avi Ginsburg's post
// https://scicomp.stackexchange.com/a/21438

namespace Eigen {

template < class SparseMatrixType >
void read_sparse_matrix ( const char* filename, SparseMatrixType & matrix )

{	std::ifstream in ( filename, std::ios::in | std::ios::binary );

	typename SparseMatrixType::Index rows, cols, nonZeros;

	// lire les dimensions de la matrice et le nombre de non-zeros
	in .read ( (char*) (&rows),     sizeof(typename SparseMatrixType::Index) );
	in .read ( (char*) (&cols),     sizeof(typename SparseMatrixType::Index) );
	in .read ( (char*) (&nonZeros), sizeof(typename SparseMatrixType::Index) );

	// redimensionner la matrice et réserver l'espace nécessaire
	matrix .resize ( rows, cols );
	matrix .reserve ( nonZeros );

	// lire les triplets (ligne, colonne, valeur) et les insérer dans la matrice
	for ( typename SparseMatrixType::Index i = 0; i < nonZeros; ++i )
	{	typename SparseMatrixType::Index row, col;
		typename SparseMatrixType::Scalar value;
		in .read ( (char*) (&row),   sizeof(typename SparseMatrixType::Index)  );
		in .read ( (char*) (&col),   sizeof(typename SparseMatrixType::Index)  );
		in .read ( (char*) (&value), sizeof(typename SparseMatrixType::Scalar) );
		matrix .insert ( row, col ) = value;
	}  // end of  for

	// matrix .makeCompressed();  // compresser la matrice après insertion

	in .close();

}  // end of  read_sparse_matrix

}  // end of  namespace Eigen


void impose_value_of_unknown  // see below for an equivalent, more readable, version
( Eigen::SparseMatrix < double > & matrix_A, Eigen::VectorXd & vector_b,
  const size_t i, const double val                                      )

// in a system of linear equations, destroy equation 'i' and impose u(i) = val
// change also column 'i' of the matrix, just to preserve symmetry

// used for imposing Dirichlet boundary conditions

{	assert ( ! matrix_A .IsRowMajor );
	const size_t size_matrix = matrix_A .innerSize();

	// destroy row i, even the diagonal entry, change vector_b
	for ( size_t j = 0; j < size_matrix; j++ )
	{	if ( matrix_A .coeff ( i, j ) != 0. )
			matrix_A .coeffRef ( i, j ) = 0.;
		vector_b (j) -= matrix_A .coeff ( j, i ) * val;  }

	// destroy column i, set diagonal entry to 1.
	const size_t oi = matrix_A .outerIndexPtr() [i];     // access to matrix_A .m_outerIndex
	// std::cout << i << " " << oi << " " << matrix_A .innerNonZeroPtr() [i] << " "
	//           << matrix_A .innerIndexPtr() [ oi ] << " " << matrix_A .valuePtr() [ oi ] << std::endl;
	matrix_A .innerNonZeroPtr() [i] = 1;                 // access to matrix_A .m_innerNonZeros
	matrix_A .innerIndexPtr() [ oi ] = i;                // access to matrix_A .m_data .m_indices
	matrix_A .valuePtr() [ oi ] = 1.;                    // access to matrix_A .m_data .m_values

	// change vector
	vector_b (i) = val;                                     }


void impose_value_of_unknown_readable
( Eigen::SparseMatrix < double > & matrix_A, Eigen::VectorXd & vector_b,
  const size_t i, const double val                                      )

// in a system of linear equations, destroy equation 'i' and impose u(i) = val
// change also column 'i' of the matrix, just to preserve symmetry

// used for imposing Dirichlet boundary conditions

{	assert ( ! matrix_A .IsRowMajor );
	const size_t size_matrix = matrix_A .innerSize();

	// destroy raw i, even the diagonal entry
	for ( size_t j = 0; j < size_matrix; j++ )
		if ( matrix_A .coeff ( i, j ) != 0. )
			matrix_A .coeffRef ( i, j ) = 0.;

	// destroy column i, change vector
	for ( size_t j = 0; j < size_matrix; j++ )
	{	if ( matrix_A .coeff ( j, i ) == 0. )  continue;
		double & Aji = matrix_A .coeffRef ( j, i );
		vector_b (j) -= Aji * val;
		Aji = 0.;                                         }

	// set diagonal entry to 1., change vector
	matrix_A .coeffRef ( i, i ) = 1.;
	vector_b (i) = val;                                     }

//------------------------------------------------------------------------------------------------------//


int main ( )

{	Manifold RR2 ( tag::Euclid, tag::of_dim, 2 );
	Function xy = RR2 .build_coordinate_system ( tag::Lagrange, tag::of_degree, 1 );
	Function x = xy[0], y = xy[1];

	Mesh::Composite arc = Mesh::Build ( tag::import_from_file )
		.format ( tag::gmsh ) .file_name ( "arc.msh" ) .import_numbering();
	Cell A = arc .retrieve_cell ( "left corner" );
	Mesh omega = arc .retrieve_mesh ( "arc" );
	Mesh AB = arc .retrieve_mesh ( "left foot" );
	Mesh CD = arc .retrieve_mesh ( "right foot" );
	Mesh EF = arc .retrieve_mesh ( "top" );
	Cell::Numbering numbering = arc .retrieve_numbering();

	const size_t N = omega .number_of ( tag::vertices );
	assert ( N == numbering .size() );

	Eigen::SparseMatrix <double> matrix_A;
	read_sparse_matrix ( "fe_matrix.dat", matrix_A );
	const size_t size_matrix = 2*N;
	assert ( matrix_A .rows() == int ( size_matrix ) );
	assert ( matrix_A .cols() == int ( size_matrix ) );
	Eigen::VectorXd vector_b ( size_matrix );
	vector_b.setZero();

	// impose Dirichlet boundary conditions on the two feet
	// forbid vertical displacement, allow for horizontal displacement
	Mesh two_feet = Mesh::Build ( tag::join ) .meshes ( { AB, CD } );
	{ // just a block of code for hiding 'it'
	Mesh::Iterator it = two_feet .iterator ( tag::over_vertices );
	for ( it .reset(); it .in_range(); it++ )
	{	Cell P = *it;
		size_t i = numbering (P);
		// impose_value_of_unknown ( matrix_A, vector_b, i, 0. ); 
		impose_value_of_unknown ( matrix_A, vector_b, i+N, 0. );  }
	} { // just a block of code
	// fix A horizontally, too
	size_t i = numbering (A);
	impose_value_of_unknown ( matrix_A, vector_b, i, 0. );
	} // just a block of code

	// 1D finite element used for imposing load on EF
	const double pressure = 1.;  // force = pressure * (0,-1)
	FiniteElement fe_bdry ( tag::with_master, tag::segment, tag::Lagrange, tag::of_degree, 1 );
	Integrator integ = fe_bdry .set_integrator ( tag::Gauss, tag::seg_3 );
	{ // just a block of code for hiding 'it'
	Mesh::Iterator it = EF .iterator ( tag::over_cells_of_max_dim );
	for ( it .reset(); it .in_range(); it++ )
	{	Cell seg = *it;
		assert ( seg .dim() == 1 );
		fe_bdry .dock_on ( seg );
		Cell V = seg .base() .reverse();
		assert ( V .is_positive() );
		const size_t i = numbering (V);
		Function psiV = fe_bdry .basis_function (V);
		vector_b [i+N] -= pressure * fe_bdry .integrate ( psiV );
		Cell W = seg .tip();
		assert ( W .is_positive() );
		const size_t j = numbering (W);
		Function psiW = fe_bdry .basis_function (W);
		vector_b [j+N] -= pressure * fe_bdry .integrate ( psiW );   }
	} // just a block of code
	
	// solve the system of linear equations
	Eigen::ConjugateGradient < Eigen::SparseMatrix < double >,
	                           Eigen::Lower | Eigen::Upper    > cg;
	cg .compute ( matrix_A );

	Eigen::VectorXd vector_sol = cg .solve ( vector_b );
	if ( cg .info() != Eigen::Success )
	{	std::cout << "Eigen solver.solve failed" << std::endl;
		exit (1);                                               }
		
	Mesh::Iterator it = omega .iterator ( tag::over_vertices );
	for ( it .reset(); it .in_range(); it++ )
	{	Cell P = *it;
		size_t i = numbering (P);
		x(P) += vector_sol (i);	
		y(P) += vector_sol (i+N);  }

	omega .export_to_file ( tag::gmsh, "arc-deform.msh" );
	std::cout << "produced file arc-deform.msh" << std::endl;

}  // end of  main
