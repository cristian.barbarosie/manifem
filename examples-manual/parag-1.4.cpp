
// example presented in paragraph 1.4 of the manual
// https://maniFEM.rd.ciencias.ulisboa.pt/manual-maniFEM.pdf
// build an L-shaped mesh by joining rectangles

#include "maniFEM.h"

using namespace maniFEM;
using namespace std;

int main ()

{	// we choose our (geometric) space dimension :
	Manifold RR2 ( tag::Euclid, tag::of_dim, 2 );
	
	// xy is a map defined on our future mesh with values in RR2 :
	Function xy = RR2 .build_coordinate_system ( tag::Lagrange, tag::of_degree, 1 );
	// in this example, we do not need to give names to the two components of xy

	Cell A ( tag::vertex, tag::of_coords, { -1., 0.  } );
	Cell B ( tag::vertex, tag::of_coords, {  0., 0.  } );
	Cell C ( tag::vertex, tag::of_coords, {  0., 0.5 } );
	Cell D ( tag::vertex, tag::of_coords, { -1., 0.5 } );
	Cell E ( tag::vertex, tag::of_coords, {  0., 1.  } );
	Cell F ( tag::vertex, tag::of_coords, { -1., 1.  } );
	Cell G ( tag::vertex, tag::of_coords, {  1., 0.  } );
	Cell H ( tag::vertex, tag::of_coords, {  1., 0.5 } );

	Mesh AB = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( A ) .stop_at ( B ) .divided_in ( 10 );
	Mesh BC = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( B ) .stop_at ( C ) .divided_in ( 8 );
	Mesh CD = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( C ) .stop_at ( D ) .divided_in ( 10 );
	Mesh DA = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( D ) .stop_at ( A ) .divided_in ( 8 );
	Mesh CE = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( C ) .stop_at ( E ) .divided_in ( 7 );
	Mesh EF = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( E ) .stop_at ( F ) .divided_in ( 10 );
	Mesh FD = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( F ) .stop_at ( D ) .divided_in ( 7 );
	Mesh BG = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( B ) .stop_at ( G ) .divided_in ( 12 );
	Mesh GH = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( G ) .stop_at ( H ) .divided_in ( 8 );
	Mesh HC = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( H ) .stop_at ( C ) .divided_in ( 12 );

	Mesh ABCD = Mesh::Build ( tag::grid ) .shape ( tag::quadrangle )
		.faces ( AB, BC, CD, DA );
	Mesh CEFD = Mesh::Build ( tag::grid ) .shape ( tag::quadrangle )
		.faces ( CE, EF, FD, CD .reverse() );
	Mesh BGHC = Mesh::Build ( tag::grid ) .shape ( tag::quadrangle )
		.faces ( GH, HC, BC .reverse(), BG );
	Mesh L_shaped = Mesh::Build ( tag::join ) .meshes ( { ABCD, CEFD, BGHC } );
	// alternative syntax
	// Mesh L_shaped = Mesh::Build ( tag::join ) .mesh ( ABCD ) .mesh ( CEFD ) .mesh ( BGHC );

	L_shaped .export_to_file ( tag::gmsh, "L-shaped.msh" );
	L_shaped .export_to_file ( tag::PostScript, "L-shaped.eps" );
	// tag::PostScript and tag::eps are interchangeable:
	// L_shaped .export_to_file ( tag::eps, "L-shaped.eps" );
	cout << "produced files L-shaped.msh and L-shaped.eps" << endl;

}  // end of main
