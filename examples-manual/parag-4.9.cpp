
// example presented in paragraph 4.9 of the manual
// https://maniFEM.rd.ciencias.ulisboa.pt/manual-maniFEM.pdf
// extruded mesh, 2D swatch, closed track

#include "maniFEM.h"

using namespace maniFEM;
using namespace std;

int main ()

{	Manifold RR3 ( tag::Euclid, tag::of_dim, 3 );
	Function xyz = RR3 .build_coordinate_system ( tag::Lagrange, tag::of_degree, 1 );
	Function x = xyz [0], y = xyz [1], z = xyz [2];

	const double coef = 0.7, seg_len = 0.1;

	Manifold RR2 = RR3 .implicit ( z == 0. );
	
	Cell A ( tag::vertex );  x(A) =  0.75 * coef;  y(A) =  0.43 * coef;  z(A) = 0.;
	Cell B ( tag::vertex );  x(B) =  0.;           y(B) =  0.86 * coef;  z(B) = 0.;
	Cell C ( tag::vertex );  x(C) = -0.75 * coef;  y(C) =  0.43 * coef;  z(C) = 0.;
	Cell D ( tag::vertex );  x(D) = -0.75 * coef;  y(D) = -0.43 * coef;  z(D) = 0.;
	Cell E ( tag::vertex );  x(E) =  0.;           y(E) = -0.86 * coef;  z(E) = 0.;
	Cell F ( tag::vertex );  x(F) =  0.75 * coef;  y(F) = -0.43 * coef;  z(F) = 0.;

	const double r2 = (1.-0.75*coef)*(1.-0.75*coef) + 0.43*0.43*coef*coef;
	Manifold circle_1 = RR2 .implicit ( (x-0.5)*(x-0.5) + (y-0.86)*(y-0.86) == r2 );
	Mesh AB = Mesh::Build ( tag::frontal ) .desired_length ( seg_len )
		.start_at ( A ) .towards ( { -1., 0., 0. } ) .stop_at ( B );
	Manifold circle_2 = RR2 .implicit ( (x+0.5)*(x+0.5) + (y-0.86)*(y-0.86) == r2 );
	Mesh BC = Mesh::Build ( tag::frontal ) .desired_length ( seg_len )
		.start_at ( B ) .towards ( { -1., -1., 0. } ) .stop_at ( C );
	Manifold circle_3 = RR2 .implicit ( (x+1.)*(x+1.) + y*y == r2 );
	Mesh CD = Mesh::Build ( tag::frontal ) .desired_length ( seg_len )
		.start_at ( C ) .towards ( { 0.5, -1., 0. } ) .stop_at ( D );
	Manifold circle_4 = RR2 .implicit ( (x+0.5)*(x+0.5) + (y+0.86)*(y+0.86) == r2 );
	Mesh DE = Mesh::Build ( tag::frontal ) .desired_length ( seg_len )
		.start_at ( D ) .towards ( { 1., 0., 0. } ) .stop_at ( E );
	Manifold circle_5 = RR2 .implicit ( (x-0.5)*(x-0.5) + (y+0.86)*(y+0.86) == r2 );
	Mesh EF = Mesh::Build ( tag::frontal ) .desired_length ( seg_len )
		.start_at ( E ) .towards ( { 1., 1., 0. } ) .stop_at ( F );
	Manifold circle_6 = RR2 .implicit ( (x-1.)*(x-1.) + y*y == r2 );
	Mesh FA = Mesh::Build ( tag::frontal ) .desired_length ( seg_len )
	  .start_at ( F ) .towards ( { -0.5, 1., 0. } ) .stop_at ( A );

	Mesh bdry = Mesh::Build ( tag::join ) .meshes ( { AB, BC, CD, DE, EF, FA } );

	RR2 .set_as_working_manifold();
	Mesh star = Mesh::Build ( tag::frontal ) .desired_length ( seg_len ) .boundary ( bdry )
	  .start_at (E) .towards ( { 0., 1., 0. } );

	RR3 .set_as_working_manifold();
	Cell G ( tag::vertex );  x(G) = x(E);  y(G) = y(E);  z(G) = 3.14159;
	Mesh EG = Mesh::Build ( tag::grid ) .shape ( tag::segment )
	  .start_at (E) .stop_at (G) .divided_in ( 20 );
	
	// twist EG around the vertical axis  x = 0  y = -1
	Mesh::Iterator it = EG .iterator ( tag::over_vertices );
	for ( it .reset(); it .in_range(); it ++ )
	{	Cell P = * it;
		double xx = x(P), yy = y(P) + 1.;
		const double theta = 2. * z(P), c = std::cos ( theta ), s = std::sin ( theta );
		const double tmp = c*xx - s*yy;
		yy = s*xx + c*yy;
		xx = tmp;
		x(P) = xx;  y(P) = yy - 1.;                                                      }
	
	Mesh tower = Mesh::Build ( tag::extrude ) .swatch ( star ) .slide_along ( EG );
	
	tower .export_to_file ( tag::gmsh, "tower.msh" );
	cout << "produced file tower.msh" << endl;

}  // end of main
