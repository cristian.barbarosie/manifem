
// example presented in paragraph 4.2 of the manual
// https://maniFEM.rd.ciencias.ulisboa.pt/manual-maniFEM.pdf
// rectangle built by extruding, non-uniform distribution of vertices

#include "maniFEM.h"

using namespace maniFEM;
using namespace std;


std::vector < double > uniform_sequence
( double t_min, double t_max, size_t div );

std::vector < double > geometric_sequence_symmetric
( double t_min, double t_max, size_t div, double ratio );

std::vector < double > geometric_sequence_increasing
( double t_min, double t_max, size_t div, double ratio );

std::vector < double > geometric_sequence_decreasing
( double t_min, double t_max, size_t div, double ratio );


int main ()

{	Manifold RR2 ( tag::Euclid, tag::of_dim, 2 );
	Function xy = RR2 .build_coordinate_system ( tag::Lagrange, tag::of_degree, 1 );
	Function x = xy[0], y = xy[1];

	Cell A ( tag::vertex );  x(A) = 0.;  y(A) = 0.;
	Cell B ( tag::vertex );  x(B) = 3.;  y(B) = 0.;
	Cell C ( tag::vertex );  x(C) = 3.;  y(C) = 1.;
	Mesh AB = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( A ) .stop_at ( B ) .divided_in ( 25 );
	Mesh BC = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( B ) .stop_at ( C ) .divided_in ( 15 );
	// segments AB and BC are defined above with a uniform distribution of vertices
	// below we change the position of the inner vertices

	double ratio = 1.1;
	std::vector < double > coords = geometric_sequence_increasing
		( x(A), x(B), AB .number_of ( tag::segments), ratio );
	Mesh::Iterator it = AB .iterator ( tag::over_vertices, tag::require_order );
	it .reset();
	for ( size_t i = 0; i < coords .size(); i ++ )
	{	assert ( it .in_range() );
		x ( *it ) = coords [i];
		it ++;                      }
	assert ( not it .in_range() );
	coords = geometric_sequence_increasing
		( y(B), y(C), BC .number_of ( tag::segments), ratio );
	it = BC .iterator ( tag::over_vertices, tag::require_order );
	it .reset();
	for ( size_t i = 0; i < coords .size(); i ++ )
	{	assert ( it .in_range() );
		y ( *it ) = coords [i];
		it ++;                      }
	assert ( not it .in_range() );

	Mesh square = Mesh::Build ( tag::extrude ) .swatch ( BC ) .slide_along ( AB );
	
	square .export_to_file ( tag::gmsh, "rectangle.msh" );
	std::cout << "produced file rectangle.msh" << std::endl;

}  // end of main


std::vector < double > uniform_sequence
( double t_min, double t_max, size_t div )

{	std::vector < double > result ( div+1 );
	double current_t = t_min;
	result [0] = current_t;
	double len_seg = (t_max-t_min) / div;
	for ( size_t i = 1; i <= div; i++ )
	{	current_t += len_seg;
		result [i] = current_t;  }
	return result;                                 }


std::vector < double > geometric_sequence_symmetric
( double t_min, double t_max, size_t div, double ratio )

{	std::vector < double > result ( div+1 );
	double current_t = 0.;
	result [0] = current_t;
	double len_seg = 1.;
	for ( size_t i = 1; i <= div; i++ )
	{	current_t += len_seg;
		if ( 2*i < div )  len_seg *= ratio;
		if ( 2*i > div )  len_seg /= ratio;
		result [i] = current_t;  }
	double rescale = (t_max-t_min) / current_t;
	for ( size_t i = 0; i <= div; i++ )
		result [i] = t_min + result [i] * rescale;
	return result;                                 }


std::vector < double > geometric_sequence_increasing
( double t_min, double t_max, size_t div, double ratio )

{	std::vector < double > result ( div+1 );
	double current_t = 0.;
	result [0] = current_t;
	double len_seg = 1.;
	for ( size_t i = 1; i <= div; i++ )
	{	current_t += len_seg;
		len_seg *= ratio;
		result [i] = current_t;  }
	double rescale = (t_max-t_min) / current_t;
	for ( size_t i = 0; i <= div; i++ )
		result [i] = t_min + result [i] * rescale;
	return result;                                 }


std::vector < double > geometric_sequence_decreasing
( double t_min, double t_max, size_t div, double ratio )

{	std::vector < double > result ( div+1 );
	double current_t = 0.;
	result [0] = current_t;
	double len_seg = 1.;
	for ( size_t i = 1; i <= div; i++ )
	{	current_t += len_seg;
		len_seg /= ratio;
		result [i] = current_t;  }
	double rescale = (t_max-t_min) / current_t;
	for ( size_t i = 0; i <= div; i++ )
		result [i] = t_min + result [i] * rescale;
	return result;                                 }
