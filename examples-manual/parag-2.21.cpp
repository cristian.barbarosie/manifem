
// example presented in paragraph 2.21 of the manual
// https://maniFEM.rd.ciencias.ulisboa.pt/manual-maniFEM.pdf
// close a circle in a cumbersome manner

#include "maniFEM.h"
#include "math.h"

using namespace maniFEM;


int main ()

{	Manifold circle_manif ( tag::Euclid, tag::of_dim, 1 );
	Function t = circle_manif .build_coordinate_system ( tag::Lagrange, tag::of_degree, 1 );
	const double pi = 4. * std::atan(1.);
	
	Cell A ( tag::vertex );  t(A) = 0.;
	Cell B ( tag::vertex );  t(B) = 1.9*pi;
	Mesh incomplete_circle = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( A ) .stop_at ( B ) .divided_in ( 19 );

	Mesh small_piece = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( B ) .stop_at ( A ) .divided_in ( 1 );
	Mesh circle = Mesh::Build ( tag::join ) .meshes ( { incomplete_circle, small_piece } );

	// forget about t, in future statements x and y will be used
	Function x = cos(t), y = sin(t);
	Manifold RR2 ( tag::Euclid, tag::of_dimension, 2 );
	RR2 .set_coordinates ( x && y );

	circle .export_to_file ( tag::eps, "circle.eps" );
	circle .export_to_file ( tag::gmsh, "circle.msh" );
	std::cout << "produced files circle.eps and circle.msh" << std::endl;

}  // end of main
