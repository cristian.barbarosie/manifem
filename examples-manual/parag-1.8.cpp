
// example presented in paragraph 1.8 of the manual
// https://maniFEM.rd.ciencias.ulisboa.pt/manual-maniFEM.pdf
// build a star-shaped closed curve, showing use of implicit manifolds

#include "maniFEM.h"

using namespace maniFEM;


int main ( )

{	Manifold RR2 ( tag::Euclid, tag::of_dim, 2 );
	Function xy = RR2 .build_coordinate_system ( tag::Lagrange, tag::of_degree, 1 );
	Function x = xy[0], y = xy[1];

	const double coef = 0.7;

	Cell A ( tag::vertex );  x(A) =  0.75 * coef;  y(A) =  0.43 * coef;
	Cell B ( tag::vertex );  x(B) =  0.         ;  y(B) =  0.86 * coef;
	Cell C ( tag::vertex );  x(C) = -0.75 * coef;  y(C) =  0.43 * coef;
	Cell D ( tag::vertex );  x(D) = -0.75 * coef;  y(D) = -0.43 * coef;
	Cell E ( tag::vertex );  x(E) =  0.         ;  y(E) = -0.86 * coef;
	Cell F ( tag::vertex );  x(F) =  0.75 * coef;  y(F) = -0.43 * coef;

	const double r2 = (1.-0.75*coef)*(1.-0.75*coef) + 0.43*0.43*coef*coef;
	Manifold circle_1 = RR2 .implicit ( (x-0.5)*(x-0.5) + (y-0.86)*(y-0.86) == r2 );
	Mesh AB = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( A ) .stop_at ( B ) .divided_in ( 7 );
	Manifold circle_2 = RR2 .implicit ( (x+0.5)*(x+0.5) + (y-0.86)*(y-0.86) == r2 );
	Mesh BC = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( B ) .stop_at ( C ) .divided_in ( 7 );
	Manifold circle_3 = RR2 .implicit ( (x+1.)*(x+1.) + y*y == r2 );
	Mesh CD = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( C ) .stop_at ( D ) .divided_in ( 7 );
	Manifold circle_4 = RR2 .implicit ( (x+0.5)*(x+0.5) + (y+0.86)*(y+0.86) == r2 );
	Mesh DE = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( D ) .stop_at ( E ) .divided_in ( 7 );
	Manifold circle_5 = RR2 .implicit ( (x-0.5)*(x-0.5) + (y+0.86)*(y+0.86) == r2 );
	Mesh EF = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( E ) .stop_at ( F ) .divided_in ( 7 );
	Manifold circle_6 = RR2 .implicit ( (x-1.)*(x-1.) + y*y == r2 );
	Mesh FA = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( F ) .stop_at ( A ) .divided_in ( 7 );

	Mesh star = Mesh::Build ( tag::join ) .meshes ( { AB, BC, CD, DE, EF, FA } );

	star .export_to_file ( tag::gmsh, "star.msh" );
	std::cout << "produced file star.msh" << std::endl;

}  // end of  main
