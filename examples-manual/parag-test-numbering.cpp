
// example presented in paragraph ?? of the manual
// https://maniFEM.rd.ciencias.ulisboa.pt/manual-maniFEM.pdf
// read composite meshes, import numbering

// reads file "boxes-comp.msh" produced in paragraph 2.3 of the manual


#include "maniFEM.h"

using namespace maniFEM;


int main ()

{	Manifold RR3 ( tag::Euclid, tag::of_dim, 3 );
	Function xyz = RR3 .build_coordinate_system ( tag::Lagrange, tag::of_degree, 1 );
	Function x = xyz [0], y = xyz [1], z = xyz [2];

	Mesh::Composite boxes
		( tag::import_from_file, tag::gmsh, "boxes-comp.msh", tag::import_numbering );
	Mesh box_A12 = boxes .retrieve_mesh ( "box_A12" );
	Mesh box_B12 = boxes .retrieve_mesh ( "box_B12" );
	Mesh box_C12 = boxes .retrieve_mesh ( "box_C12" );
	Mesh box_D12 = boxes .retrieve_mesh ( "box_D12" );
	Mesh box_A23 = boxes .retrieve_mesh ( "box_A23" );
	Mesh box_B23 = boxes .retrieve_mesh ( "box_B23" );
	Mesh box_C23 = boxes .retrieve_mesh ( "box_C23" );
	Mesh box_D23 = boxes .retrieve_mesh ( "box_D23" );
	Mesh box_34  = boxes .retrieve_mesh ( "box_34"  );
	
	Mesh parallelipiped ( tag::join, { box_A12, box_B12, box_C12, box_D12,
	                        box_A23, box_B23, box_C23, box_D23, box_34 } );

	Cell::Numbering & numbering = boxes .retrieve_numbering();

	size_t min_n = 10000, max_n = 0;
	Mesh::Iterator it = parallelipiped .iterator ( tag::over_vertices );
	for ( it .reset(); it .in_range(); it ++ )
	{	Cell P = * it;
		if ( numbering (P) < min_n )  min_n = numbering (P);
		if ( numbering (P) > max_n )  max_n = numbering (P);  }

	std::cout << "mesh has " << parallelipiped .number_of ( tag::vertices ) << " vertices" << std::endl;
	std::cout << "numbers between " << min_n << " and " << max_n << std::endl;
	
} // end of main

