
// example presented in paragraph 4.1 of the manual
// https://maniFEM.rd.ciencias.ulisboa.pt/manual-maniFEM.pdf
// rectangle built by extruding

#include "maniFEM.h"

using namespace maniFEM;
using namespace std;


int main ()

{	Manifold RR2 ( tag::Euclid, tag::of_dim, 2 );
	Function xy = RR2 .build_coordinate_system ( tag::Lagrange, tag::of_degree, 1 );
	Function x = xy[0], y = xy[1];

	Cell O ( tag::vertex );  x(O) = 0.;   y(O) = 0.;
	Cell A ( tag::vertex );  x(A) = 1.;   y(A) = 0.;
	Cell B ( tag::vertex );  x(B) = 0.5;  y(B) = 0.8;

	Mesh OA = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( O ) .stop_at ( A ) .divided_in ( 10 );
	Mesh OB = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( O ) .stop_at ( B ) .divided_in ( 10 );

	Mesh rhombus = Mesh::Build ( tag::extrude ) .swatch ( OB ) .slide_along ( OA );

	Mesh::Iterator it = OB .iterator ( tag::over_cells_of_max_dim );
	for ( it .reset(); it .in_range(); it ++ )
		assert ( (*it) .belongs_to ( rhombus ) );
	it = OA .iterator ( tag::over_cells_of_max_dim );
	for ( it .reset(); it .in_range(); it ++ )
		assert ( (*it) .belongs_to ( rhombus ) );
	
	rhombus .export_to_file ( tag::gmsh, "rhombus.msh" );
	std::cout << "produced file rhombus.msh" << std::endl;

}  // end of main

