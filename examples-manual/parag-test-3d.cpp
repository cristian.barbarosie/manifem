
#include "maniFEM.h"

using namespace maniFEM;

void main1 ()  // main1

{	Manifold RR3 ( tag::Euclid, tag::of_dim, 3 );
	Function xyz = RR3 .build_coordinate_system ( tag::Lagrange, tag::of_degree, 1 );
	Function x = xyz [0], y = xyz [1], z = xyz [2];

	Mesh two_tori ( tag::import_from_file, tag::gmsh, "sphere.msh" );
	
	Mesh ball ( tag::frontal, tag::boundary, two_tori, tag::desired_length, 0.2 );
	ball .export_to_file ( tag::gmsh, "ball.msh" );

	std::cout << "produced files interf.msh and ball.msh" << std::endl;

}  // end of main1


void main2 ()  // main2  // paragraph 3.25

{	Manifold RR3 ( tag::Euclid, tag::of_dim, 3 );
	Function xyz = RR3 .build_coordinate_system ( tag::Lagrange, tag::of_degree, 1 );
	Function x = xyz [0], y = xyz [1], z = xyz [2];

	// unlike in paragraph 3.7, here we don't need to cut f1 and f3 with 0.1,
	// the shape of each torus avoids the singularities
	// we change slightly f3 (replace 0.4 by 0.5) to highlight the intersection
	Function f1 = x*x + y*y;
	Function f2 = 1. - power ( f1, -0.5 );
	Function d1 = f1 * f2 * f2 + z*z;  // squared distance to a circle in the xy plane
	Function f3 = (x-0.5)*(x-0.5) + z*z;
	Function f4 = 1. - power ( f3, -0.5 );
	Function d2 = y*y + f3 * f4 * f4;  // squared distance to a circle in the xz plane

	// const double seg_size = 0.17;
	// Function seg_size = 0.03 + 0.05 * abs ( d2 - d1 );
	// Function seg_size = 0.1 * power ( 0.15 + abs ( d2 - d1 ), 0.4 );
	Function seg_size = 0.2 + 0.01 * x;

	Manifold intersection = RR3 .implicit ( d1 == 0.15, d2 == 0.15 );

	Cell start_1 ( tag::vertex, tag::of_coords, { 1.3, -0.4, 0. }, tag::project );
	Mesh circle_1 ( tag::frontal, tag::start_at, start_1, tag::towards, { 0., 0., 1. },
	                tag::desired_length, seg_size                                      );

	Cell start_2 ( tag::vertex, tag::of_coords, { -0.9, 0., 0.4 }, tag::project );
	Mesh circle_2 ( tag::frontal, tag::start_at, start_2, tag::towards, { 0., 1., 0. },
	                tag::desired_length, seg_size                                      );

	Mesh two_circles ( tag::join, circle_1, circle_2 );

	RR3 .implicit ( d1 == 0.15 );
	Mesh torus_1 ( tag::frontal, tag::boundary, two_circles,
	               tag::start_at, start_1, tag::towards, { -0.2, -1., 0. },
	               tag::desired_length, seg_size                           );

	RR3 .implicit ( d2 == 0.15 );
	Mesh torus_2 ( tag::frontal, tag::boundary, two_circles .reverse(),
	               tag::start_at, start_1, tag::towards, { 1., -0.2, 0. },
	               tag::desired_length, seg_size                          );

	Mesh two_tori ( tag::join, torus_1, torus_2 );
	two_tori .export_to_file ( tag::hmsh, "sphere.msh" );
	
	RR3 .set_as_working_manifold();
	Mesh ball ( tag::frontal, tag::boundary, two_tori, tag::desired_length, seg_size );
	ball .export_to_file ( tag::gmsh, "ball.msh" );

	std::cout << "produced files sphere.msh, interf.msh and ball.msh" << std::endl;

}  // end of main2


// #include <random>
// std::default_random_engine random_generator;
// std::uniform_int_distribution < int > distr ( -30, 30 );

int main ()  // main3

{	Manifold RR3 ( tag::Euclid, tag::of_dim, 3 );
	Function xyz = RR3 .build_coordinate_system ( tag::Lagrange, tag::of_degree, 1 );
	Function x = xyz [0], y = xyz [1], z = xyz [2];

	std::cout << "this example takes time" << std::endl;
	
	const double step = 0.125 * 0.125 ;  //  2 ^ -6 = 0.015625  // 2^6 = 64
	const double stepp = step * 0.0625;   //  2 ^ -9 = 0.00195
	int nx = 2, ny = -3, nl = -5;   //  |n| <= 30
	std::cout << "insert three integers between -30 and 30 " << std::endl;
	std::cin >> nx >> ny >> nl;
	const double coef_x = 1. + nx * step, coef_y = 1. + ny * step;   //  | coef - 1 | < 0.47
	Function seg_len = 0.1 + nl * stepp * x;                        //  | nl * stepp | < 0.03
	RR3 .implicit ( coef_x*x*x + coef_y*y*y + z*z == 1. );
	Mesh sphere ( tag::frontal, tag::desired_length, seg_len );
	// sphere .export_to_file ( tag::gmsh, "sphere.msh" );

	RR3 .set_as_working_manifold();
	Mesh ball ( tag::frontal, tag::boundary, sphere, tag::desired_length, seg_len );
	ball .export_to_file ( tag::gmsh, "ball.msh" );

	std::cout << "produced file interf.msh" << std::endl;

}  // end of main3
