
// example presented in paragraph 4.8 of the manual
// https://maniFEM.rd.ciencias.ulisboa.pt/manual-maniFEM.pdf
// extruded mesh in RR3, both swatch and track are closed loops
// result is a torus surface

#include "maniFEM.h"

using namespace maniFEM;
using namespace std;


int main ()

{	Manifold RR3 ( tag::Euclid, tag::of_dim, 3 );
	Function xyz = RR3 .build_coordinate_system ( tag::Lagrange, tag::of_degree, 1 );
	Function x = xyz [0], y = xyz [1], z = xyz [2];

	Manifold RR2 = RR3 .implicit ( z == 0. );

	const double coef = 0.7;

	Cell A ( tag::vertex );  x(A) =  0.75 * coef;  y(A) =  0.43 * coef;  z(A) = 0.;
	Cell B ( tag::vertex );  x(B) =  0.         ;  y(B) =  0.86 * coef;  z(B) = 0.;
	Cell C ( tag::vertex );  x(C) = -0.75 * coef;  y(C) =  0.43 * coef;  z(C) = 0.;
	Cell D ( tag::vertex );  x(D) = -0.75 * coef;  y(D) = -0.43 * coef;  z(D) = 0.;
	Cell F ( tag::vertex );  x(F) =  0.         ;  y(F) = -0.86 * coef;  z(F) = 0.;
	Cell G ( tag::vertex );  x(G) =  0.75 * coef;  y(G) = -0.43 * coef;  z(G) = 0.;

	const double r2 = (1.-0.75*coef)*(1.-0.75*coef) + 0.43*0.43*coef*coef;
	Manifold circle_1 = RR2 .implicit ( (x-0.5)*(x-0.5) + (y-0.86)*(y-0.86) == r2 );
	Mesh AB = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( A ) .stop_at ( B ) .divided_in ( 7 );
	Manifold circle_2 = RR2 .implicit ( (x+0.5)*(x+0.5) + (y-0.86)*(y-0.86) == r2 );
	Mesh BC = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( B ) .stop_at ( C ) .divided_in ( 7 );
	Manifold circle_3 = RR2 .implicit ( (x+1.)*(x+1.) + y*y == r2 );
	Mesh CD = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( C ) .stop_at ( D ) .divided_in ( 7 );
	Manifold circle_4 = RR2 .implicit ( (x+0.5)*(x+0.5) + (y+0.86)*(y+0.86) == r2 );
	Mesh DF = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( D ) .stop_at ( F ) .divided_in ( 7 );
	Manifold circle_5 = RR2 .implicit ( (x-0.5)*(x-0.5) + (y+0.86)*(y+0.86) == r2 );
	Mesh FG = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( F ) .stop_at ( G ) .divided_in ( 7 );
	Manifold circle_6 = RR2 .implicit ( (x-1.)*(x-1.) + y*y == r2 );
	Mesh GA = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( G ) .stop_at ( A ) .divided_in ( 7 );

	Mesh star = Mesh::Build ( tag::join ) .meshes ( { AB, BC, CD, DF, FG, GA } );

	const double R = 1.;
	RR3 .implicit ( (y-R-0.86*coef)*(y-R-0.86*coef) + z*z == R*R, x == 0. );

	// Cell N ( tag::vertex );  x(N) = 0.;  y(N) =  0.86 * coef;  z(N) = 0.;
	Cell N = B;
	Cell W ( tag::vertex );  x(W) = 0.;  y(W) =    R + 0.86 * coef;  z(W) = -R;
	Cell S ( tag::vertex );  x(S) = 0.;  y(S) = 2.*R + 0.86 * coef;  z(S) =  0.;
	Cell E ( tag::vertex );  x(E) = 0.;  y(E) =    R + 0.86 * coef;  z(E) =  R;

	Mesh NW = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at (N) .stop_at (W) .divided_in ( 20 );
	Mesh WS = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at (W) .stop_at (S) .divided_in ( 20 );
	Mesh SE = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at (S) .stop_at (E) .divided_in ( 20 );
	Mesh EN = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at (E) .stop_at (N) .divided_in ( 20 );

	Mesh big_circle = Mesh::Build ( tag::join ) .meshes ( { NW, WS, SE, EN } );

	RR3 .set_as_working_manifold();
	Mesh circus = Mesh::Build ( tag::extrude )
		.swatch ( star ) .slide_along ( big_circle ) .follow_curvature();
	
	Mesh::Iterator it = star .iterator ( tag::over_cells_of_max_dim );
	for ( it .reset(); it .in_range(); it ++ )
		assert ( (*it) .belongs_to ( circus ) );
	it = big_circle .iterator ( tag::over_cells_of_max_dim );
	for ( it .reset(); it .in_range(); it ++ )
		assert ( (*it) .belongs_to ( circus ) );
	
	circus .export_to_file ( tag::gmsh, "circus.msh" );
	std::cout << "produced file circus.msh" << std::endl;

}  // end of main
