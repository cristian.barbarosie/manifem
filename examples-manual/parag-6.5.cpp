
// example presented in paragraph 6.5 of the manual
// https://maniFEM.rd.ciencias.ulisboa.pt/manual-maniFEM.pdf
// variational formulation, concentrated load

#include "maniFEM.h"
#include <fstream>

using namespace maniFEM;


int main ()

{	Manifold RR2 ( tag::Euclid, tag::of_dim, 2 );
	Function xy = RR2 .build_coordinate_system ( tag::Lagrange, tag::of_degree, 1 );
	Function x = xy[0], y = xy[1];

	//  A B C
	//  D E F
	
	Cell A ( tag::vertex );  x(A) = -1.;  y(A) = 1.;
	Cell B ( tag::vertex );  x(B) =  0.;  y(B) = 1.;
	Cell C ( tag::vertex );  x(C) =  1.;  y(C) = 1.;
	Cell D ( tag::vertex );  x(D) = -1.;  y(D) = 0.;
	Cell E ( tag::vertex );  x(E) =  0.;  y(E) = 0.;
	Cell F ( tag::vertex );  x(F) =  1.;  y(F) = 0.;
	Cell M ( tag::vertex );  x(M) =  0.;  y(M) = 0.5;

	Mesh DE = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( D ) .stop_at ( E ) .divided_in ( 10 );
	Mesh EF = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( E ) .stop_at ( F ) .divided_in ( 10 );
	Mesh BA = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( B ) .stop_at ( A ) .divided_in ( 10 );
	Mesh CB = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( C ) .stop_at ( B ) .divided_in ( 10 );
	Mesh AD = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( A ) .stop_at ( D ) .divided_in ( 10 );
	Mesh BM = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( B ) .stop_at ( M ) .divided_in ( 5 );
	Mesh ME = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( M ) .stop_at ( E ) .divided_in ( 5 );
	Mesh BE = Mesh::Build ( tag::join ) .meshes ( { BM, ME } );
	Mesh FC = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( F ) .stop_at ( C ) .divided_in ( 10 );

	Mesh ADEB = Mesh::Build ( tag::grid ) .shape ( tag::rectangle )
		.faces ( AD, DE, BE .reverse(), BA );
	Mesh BEFC = Mesh::Build ( tag::grid ) .shape ( tag::rectangle )
		.faces ( BE, EF, FC, CB );

	Mesh domain = Mesh::Build ( tag::join ) .meshes ( { ADEB, BEFC } );
	Mesh Dirichlet_bdry = Mesh::Build ( tag::join ) .meshes ( { DE, EF, CB, BA } );

	Function temperature ( tag::unknown, tag::Lagrange, tag::of_degree, 1, tag::mere_symbol );
	Function phi ( tag::test, tag::Lagrange, tag::of_degree, 1, tag::mere_symbol );

	VariationalFormulation Laplace =
	    (   temperature .deriv(x) * phi .deriv(x) // +
			+ temperature .deriv(y) * phi .deriv(y) ) .integral_on ( domain )
		 ==   phi .integral_on ( ADEB )  // -
		    - phi .integral_on ( BEFC )  // -
		    - ( y * phi ) .integral_on ( AD )  // +
		    + ( y * phi ) .integral_on ( FC )
		    - 0.5 * phi .value_at (M);
	// it would be more elegant to write '-0.5*phi(M)'
	// unfortunately, syntactic constraints in C++ prevent us from using such a construct ...

	Laplace .add_boundary_condition ( tag::Dirichlet, temperature == 0., tag::on, Dirichlet_bdry );

	temperature = Laplace .solve();  // here we use the same wrapper Function 'temperature'
	// or, equivalently, we can create a new Function wrapper :
	// Function computed_temp = Laplace .solve();

	Mesh::Composite mesh_with_temp = Mesh::Build ( tag::gather )
		.mesh ( "rectangle", domain )
		.function ( "temperature", temperature );
	mesh_with_temp .export_to_file ( tag::gmsh, "rectangle-Poisson.msh" );
	
	std::cout << "produced file rectangle-Poisson.msh" << std::endl;

}  // end of  main
