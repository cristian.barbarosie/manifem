
// this program is written in C++ and uses the maniFEM library
// https://maniFEM.rd.ciencias.ulisboa.pt/

// to compile and run this program, you should issue, in the main directory of maniFEM, the command
//    make run-parag-test-cell-scalar

// builds a cell (torus) with a super-ellipsoidal hole
// subsequently deformed by a linear map

// solves the cellular problem for a scalar elliptic equation (heat diffusion),
// two load cases, then computes the homogenized conductivity tensor
// in three different ways


#include "maniFEM.h"
#include <fstream>
#include <set>
#include <iomanip>
#include <Eigen/Sparse>
#include <Eigen/OrderingMethods>
using namespace maniFEM;
using namespace std;


inline double sign ( const double c )
{	if ( c < 0. )  return -1.;
	return 1.;                  }


int main ( )

{	Manifold RR2 ( tag::Euclid, tag::of_dim, 2 );
	Function xy = RR2 .build_coordinate_system ( tag::Lagrange, tag::of_degree, 1 );
	Function coord_x = xy [0], coord_y = xy [1];

	// after declaring 'numbering', space in the memory will be reserved,
	// for each vertex, for a 'size_t' number
	// spurious vertices show up, unfortunately
	// to solve this problem, we compute again the numbering of significant vertices,
	// after the mesh generation is finished
	Cell::Numbering numbering ( tag::field );

	const double seg_size = 0.05;
	std::cout << std::setprecision (9);

	// parameters defining the shape of the hole (super-ellipse)
	const double super_ellipse_param_m_s = 0.5,
	             super_ellipse_param_m_t = 0.8,
	             super_ellipse_param_q   = 2.5;
	const double super_ellipse_param_m_s_over_2 = super_ellipse_param_m_s / 2.,
	             super_ellipse_param_m_t_over_2 = super_ellipse_param_m_t / 2.;

	// linear deformation of the grill ( D phi )
	double D_phi_s_d_x = 1.8, D_phi_s_d_y = 0.2, D_phi_t_d_x = 0.1, D_phi_t_d_y = 1.;
	const double det = D_phi_s_d_x * D_phi_t_d_y - D_phi_s_d_y * D_phi_t_d_x;
	// we can rescale the unit cell (vectors defining the torus)
	// the cellular problem remains the same
	const double sqrt_det = std::sqrt ( det );
	D_phi_s_d_x /= sqrt_det;
	D_phi_s_d_y /= sqrt_det;
	D_phi_t_d_x /= sqrt_det;
	D_phi_t_d_y /= sqrt_det;
	// psi is the inverse of phi
	const double D_psi_x_d_s =   D_phi_t_d_y,   // no need to divide by 'det'
	             D_psi_x_d_t = - D_phi_s_d_y,   // due to the rescaling above
	             D_psi_y_d_s = - D_phi_t_d_x,
	             D_psi_y_d_t =   D_phi_s_d_x;
	// std::cout << D_phi_s_d_x * D_psi_x_d_s + D_phi_s_d_y * D_psi_y_d_s << " "
	// 			 << D_phi_s_d_x * D_psi_x_d_t + D_phi_s_d_y * D_psi_y_d_t << " "
	// 			 << D_phi_t_d_x * D_psi_x_d_s + D_phi_t_d_y * D_psi_y_d_s << " "
	// 			 << D_phi_t_d_x * D_psi_x_d_t + D_phi_t_d_y * D_psi_y_d_t << std::endl;
	Function coord_s = D_phi_s_d_x * coord_x + D_phi_s_d_y * coord_y,
	         coord_t = D_phi_t_d_x * coord_x + D_phi_t_d_y * coord_y;

	Manifold super_ellipse_manif = RR2 .implicit
		(  power ( abs(coord_s) / super_ellipse_param_m_s_over_2, super_ellipse_param_q ) // +
		 + power ( abs(coord_t) / super_ellipse_param_m_t_over_2, super_ellipse_param_q ) == 1. );
	
	Mesh inner = Mesh::Build ( tag::frontal ) .entire_manifold() .desired_length ( seg_size );

	// Function transl_s = ( D_psi_x_d_s * ( coord_s + 1. ) + D_psi_x_d_t * coord_t ) &&
	//                     ( D_psi_y_d_s * ( coord_s + 1. ) + D_psi_y_d_t * coord_t )    ;
	// Function transl_t = ( D_psi_x_d_s * coord_s + D_psi_x_d_t * ( coord_t + 1. ) ) &&
	//                     ( D_psi_y_d_s * coord_s + D_psi_y_d_t * ( coord_t + 1. ) )    ;
	// expressions above are mathematically equivalent to the ones below
	// however, maniFEM does not recognize expressions above as translations
	// the (homogeneous) linear part is (1,0,0,1)
	// but this is not enough for maniFEM to look at them as translations
	Function transl_s = ( coord_x + D_psi_x_d_s ) && ( coord_y + D_psi_y_d_s );
	Function transl_t = ( coord_x + D_psi_x_d_t ) && ( coord_y + D_psi_y_d_t );
	Manifold::Action gs ( tag::transforms, xy, tag::into, transl_s );
	Manifold::Action gt ( tag::transforms, xy, tag::into, transl_t ); 
	Manifold torus = RR2 .quotient ( gs, gt );
	Function xy_torus = torus .coordinates();
	Function x_torus = xy_torus [0], y_torus = xy_torus [1];

	Mesh inner_fold = inner .fold ( tag::use_existing_vertices );
	
	Mesh inside_hole = Mesh::Build ( tag::frontal )
		.boundary ( inner_fold ) .desired_length ( seg_size );
	Mesh perforated_torus = Mesh::Build ( tag::frontal )
		.boundary ( inner_fold .reverse() ) .desired_length ( seg_size );
	Mesh entire_torus = Mesh::Build ( tag::join ) .meshes ( { inside_hole, perforated_torus } );

	// tag::Util::Tensor is a class defined in maniFEM for arbitrary order tensors
	tag::Util::Tensor <double> conduct (2,2);
	conduct (0,0) = 1.4;
	conduct (0,1) = 0.12;
	conduct (1,0) = 0.12;
	conduct (1,1) = 1.;

	// space in the memory has been reserved, for each vertex, for a 'size_t' number
	// however, more vertices have been built, invisibly to the user,
	// than those in 'entire_torus'
	// so we re-define the numbers of vertices in 'entire_torus'
	// the others will never been used
	{ // just a block of code for hiding 'it' and 'counter'
	Mesh::Iterator it = entire_torus .iterator ( tag::over_vertices );
	size_t counter = 0;
	for ( it .reset() ; it .in_range(); it ++ )
	{	Cell V = *it;  numbering (V) = counter;  ++ counter;  }
	std::cout << entire_torus .number_of ( tag::vertices ) << " vertices" << std::endl;
	std::cout << counter << " counted" << std::endl;
	std::cout << "numbering has size " << numbering .size() << std::endl;
	} // just a block of code

	const size_t number_dofs = entire_torus .number_of ( tag::vertices );
	std::cout << "global matrix " << number_dofs << "x" << number_dofs << std::endl;
	Eigen::SparseMatrix < double > matrix_A ( number_dofs, number_dofs );
	
	matrix_A .reserve ( Eigen::VectorXi::Constant ( number_dofs, 7 ) );
	// since we will be working with a mesh of triangles,
	// there will be, in average, 7 = 6+1 non-zero elements per column
	// the diagonal entry plus six neighbour vertices

	Eigen::VectorXd vector_b_case_x ( number_dofs ), vector_b_case_y ( number_dofs );
	vector_b_case_x .setZero();
	vector_b_case_y .setZero();

	// macroscopic strain matrix, defined by the user (this is the input of the program)
	// tag::Util::Tensor is a class defined in maniFEM for arbitrary order tensors
	tag::Util::Tensor < double > macro_temp_grad_case_x (2), macro_temp_grad_case_y (2);
	macro_temp_grad_case_x (0) = 1.;
	macro_temp_grad_case_x (1) = 0.;
	macro_temp_grad_case_y (0) = 0.;
	macro_temp_grad_case_y (1) = 1.;

	// 'u' will be a (vector-valued) multi-function defined on the torus
	// the jump of this multi-function (difference between sheets)
	// is given by the 'macro_strain' and also by the geometry of the torus
	Function::Jump jump_of_sol_case_x =
		macro_temp_grad_case_x (0) * x_torus .jump() + macro_temp_grad_case_x (1) * y_torus .jump();
	Function::Jump jump_of_sol_case_y =
		macro_temp_grad_case_y (0) * x_torus .jump() + macro_temp_grad_case_y (1) * y_torus .jump();
	
	// declare the type of finite element
	FiniteElement fe ( tag::triangle, tag::Lagrange, tag::of_degree, 1 );
	Integrator integ = fe .set_integrator ( tag::hand_coded );
	Function bf1 ( tag::basis_function, tag::within, fe ),
	         bf2 ( tag::basis_function, tag::within, fe );

	// hand-coded integrators require an early declaration of
	// the integrals we intend to compute later (after docking 'fe' on a cell)
	fe .pre_compute ( tag::for_given, tag::basis_functions, bf1, bf2,
	                  tag::integral_of, { bf1 .deriv ( x_torus ) * bf2 .deriv ( x_torus ),
	                                      bf1 .deriv ( x_torus ) * bf2 .deriv ( y_torus ),
	                                      bf1 .deriv ( y_torus ) * bf2 .deriv ( x_torus ),
	                                      bf1 .deriv ( y_torus ) * bf2 .deriv ( y_torus ) } );

	// impose equilibrium equation
	// run over all triangular cells composing 'entire_torus'
	{ // just a block of code for hiding 'it'
	Mesh::Iterator it = entire_torus .iterator ( tag::over_cells_of_max_dim );
	for ( it .reset(); it .in_range(); it++ )
	{	Cell small_tri = *it;
		fe .dock_on ( small_tri, tag::winding );
		// run twice over the three vertices of 'small_tri'
		Mesh::Iterator it_V = small_tri .boundary() .iterator ( tag::over_vertices );
		for ( it_V .reset(); it_V .in_range(); it_V ++ )
		{	Cell V = * it_V;
			Cell seg = small_tri .boundary() .cell_in_front_of (V);
			Cell W = V;
			Function psi_V = fe .basis_function (V);
			Manifold::Action winding_V_W = 0; // winding from V to W
			while ( true )
			{	assert ( W == seg .base() .reverse() );
				// V may be the same as W, no problem about that
				Function psi_W = fe .basis_function (W);
				// 'fe' is already docked on 'small_tri' so this will be the domain of integration
				std::vector < double > integrals = fe .integrate
					( tag::pre_computed, tag::replace, bf1, tag::by, psi_W,
					                     tag::replace, bf2, tag::by, psi_V );
				assert ( integrals .size() == 4 );
				// double int_d_psiW_dx_d_psiV_dx = fe .integrate ( d_psiW_dx * d_psiV_dx );
				// double int_d_psiW_dx_d_psiV_dy = fe .integrate ( d_psiW_dx * d_psiV_dy );
				// double int_d_psiW_dy_d_psiV_dx = fe .integrate ( d_psiW_dy * d_psiV_dx );
				// double int_d_psiW_dy_d_psiV_dy = fe .integrate ( d_psiW_dy * d_psiV_dy );
				double energy = conduct (0,0) * integrals [0] +
				                conduct (0,1) * integrals [2] +
				                conduct (1,0) * integrals [1] +
				                conduct (1,1) * integrals [3];
				// 	energy = conduct (0,0) * int_d_psiW_dx_d_psiV_dx +
				// 	         conduct (0,1) * int_d_psiW_dy_d_psiV_dx +
				// 	         conduct (1,0) * int_d_psiW_dx_d_psiV_dy +
				// 	         conduct (1,1) * int_d_psiW_dy_d_psiV_dy;
				matrix_A .coeffRef ( numbering(V), numbering(W) ) += energy;
				vector_b_case_x ( numbering(V) ) -= jump_of_sol_case_x ( winding_V_W ) * energy;
				vector_b_case_y ( numbering(V) ) -= jump_of_sol_case_y ( winding_V_W ) * energy;
				winding_V_W += seg .winding() ;
				W = seg .tip();
				if ( V == W ) break;
				seg = small_tri .boundary() .cell_in_front_of (W);
			}  // end of loop in W
			// here winding_V_W should be zero again
			assert ( winding_V_W == 0 );
		}  }  // end of loop in V, end of loop in small_tri
	} // just a block of code for hiding 'it'

	std::cout << "now solving the systems of linear equations" << std::endl;
	
	matrix_A .makeCompressed();

	Eigen::SparseQR < Eigen::SparseMatrix<double>, Eigen::COLAMDOrdering<int> > solver;

	solver .compute ( matrix_A );
	if ( solver .info() != Eigen::Success )
	{	std::cout << "Eigen solver.compute failed" << std::endl;
		exit ( 0 );                                               }

	Eigen::VectorXd vector_sol_case_x = solver .solve ( vector_b_case_x );
	if ( solver .info() != Eigen::Success )
	{	std::cout << "Eigen solver.solve failed, case x" << std::endl;
		exit (1);                                                        }
	Eigen::VectorXd vector_sol_case_y = solver .solve ( vector_b_case_y );
	if ( solver .info() != Eigen::Success )
	{	std::cout << "Eigen solver.solve failed, case y" << std::endl;
		exit (1);                                                        }

	// there are two ways of computing the homogenized conductivity tensor CH
	// first,  as a linear application,  by CH A   = int conduct grad uA
	// second, as a quadratic form,      by CH A B = int conduct grad uA grad uB
	// no need to divide by the volume of the torus, it has been normalized to 1
	
	// compute CH as linear application
	tag::Util::Tensor < double > macro_heat_flux_case_x (2), macro_heat_flux_case_y (2);
	for ( size_t i = 0; i < 2; i++ )
	{	macro_heat_flux_case_x (i) = 0.;
		macro_heat_flux_case_y (i) = 0.;  }
	
	// hand-coded integrators require an early declaration of
	// the integrals we intend to compute later (after docking 'fe' on a cell)
	fe .pre_compute ( tag::for_a_given, tag::basis_function, bf1,
	                  tag::integral_of, { bf1 .deriv ( x_torus ), bf1 .deriv ( y_torus ) } );

	// run over all triangular cells composing 'entire_torus'
	{ // just a block of code for hiding 'it'
	Mesh::Iterator it = entire_torus .iterator ( tag::over_cells_of_max_dim );
	for ( it .reset(); it .in_range(); it++ )
	{	Cell small_tri = *it;
		fe .dock_on ( small_tri, tag::winding );
		// run twice over the four vertices of 'small_tri'
		Mesh::Iterator it_V = small_tri .boundary() .iterator ( tag::over_vertices );
		Manifold::Action winding_V = 0; // winding from first V to current V
		for ( it_V .reset(); it_V .in_range(); it_V++ )
		{	Cell V = *it_V;
			Function psi_V = fe .basis_function (V);
			std::vector < double > integrals = fe .integrate
				( tag::pre_computed, tag::replace, bf1, tag::by, psi_V );
			assert ( integrals .size() == 2 );
			for ( size_t i = 0; i < 2; i++ )
			{	macro_heat_flux_case_x (i) +=
				   ( vector_sol_case_x ( numbering(V) ) + jump_of_sol_case_x ( winding_V ) ) *
				   ( conduct (i,0) * integrals [0] + conduct (i,1) * integrals [1] );
				//   ( conduct (i,0) * fe .integrate ( d_psi_V_dx ) +
				//     conduct (i,1) * fe .integrate ( d_psi_V_dy )  ) +
				macro_heat_flux_case_y (i) +=
				   ( vector_sol_case_y ( numbering(V) ) + jump_of_sol_case_y ( winding_V ) ) *
				   ( conduct (i,0) * integrals [0] + conduct (i,1) * integrals [1] );        }
			Cell seg = small_tri .boundary() .cell_in_front_of (V);
			winding_V += seg .winding() ;
		}  // // end of loop in V
		// here winding_V should be zero again
		assert ( winding_V == 0 );
	}  // end of loop in small_tri
	}  // just a block of code for hiding 'it'

	tag::Util::Tensor <double> CH (2,2);
	for ( size_t i = 0; i < 2; i++ )
	{	CH (i,0) = macro_heat_flux_case_x (i);
		CH (i,1) = macro_heat_flux_case_y (i);  }

	std::cout << "homogenized conductivity tensor computed as linear form" << std::endl;
	for ( size_t i = 0; i < 2; i++ )
	{	for ( size_t j = 0; j < 2; j++ )
			std::cout << CH (i,j) << " ";
		std::cout << std::endl;                     }
	
	// compute CH as a quadratic form, first try
	// computes again all integrals of products of derivatives of base functions
	for ( size_t i = 0; i < 2; i++ )
	for ( size_t j = 0; j < 2; j++ )
		CH (i,j) = 0.;
	
	// hand-coded integrators require an early declaration of
	// the integrals we intend to compute later (after docking 'fe' on a cell)
	fe .pre_compute ( tag::for_given, tag::basis_functions, bf1, bf2,
	                  tag::integral_of, { bf1 .deriv ( x_torus ) * bf2 .deriv ( x_torus ),
	                                      bf1 .deriv ( x_torus ) * bf2 .deriv ( y_torus ),
	                                      bf1 .deriv ( y_torus ) * bf2 .deriv ( x_torus ),
	                                      bf1 .deriv ( y_torus ) * bf2 .deriv ( y_torus ) } );

	// run over all triangular cells composing 'entire_torus'
	// note that the windings are more carefully taken into account here
	// than in the above block where the finite element matrix is assembled
	{ // just a block of code for hiding 'it'
	Mesh::Iterator it = entire_torus .iterator ( tag::over_cells_of_max_dim );
	for ( it .reset(); it .in_range(); it++ )
	{	Cell small_tri = *it;
		fe .dock_on ( small_tri, tag::winding );
		Manifold::Action winding_V = 0;
		// run twice over the three vertices of 'small_tri'
		Mesh::Iterator it_V = small_tri .boundary() .iterator ( tag::over_vertices );
		it_V .reset();  assert ( it_V .in_range() );
		Cell V = * it_V;  // always the same
		Cell seg_V = small_tri .boundary() .cell_in_front_of (V);
		for ( size_t i_V = 0; i_V < 3; i_V ++ )
		{	Cell W = * it_V;  // always the same
			Cell seg_W = small_tri .boundary() .cell_in_front_of (W);
			Manifold::Action winding_W = 0;
			Function psi_V = fe .basis_function (V);
			for ( size_t i_W = 0; i_W < 3; i_W ++ )  // V may be the same as W, no problem about that
			{	Function psi_W = fe .basis_function (W);
				// 'fe' is already docked on 'small_tri' so this will be the domain of integration
				std::vector < double > integrals = fe .integrate
					( tag::pre_computed, tag::replace, bf1, tag::by, psi_W,
					                     tag::replace, bf2, tag::by, psi_V );
				assert ( integrals .size() == 4 );
				// double int_d_psiW_dx_d_psiV_dx = fe .integrate ( d_psiW_dx * d_psiV_dx );
				// double int_d_psiW_dx_d_psiV_dy = fe .integrate ( d_psiW_dx * d_psiV_dy );
				// double int_d_psiW_dy_d_psiV_dx = fe .integrate ( d_psiW_dy * d_psiV_dx );
				// double int_d_psiW_dy_d_psiV_dy = fe .integrate ( d_psiW_dy * d_psiV_dy );
				double energy = conduct (0,0) * integrals [0] +
				                conduct (0,1) * integrals [2] +
				                conduct (1,0) * integrals [1] +
				                conduct (1,1) * integrals [3];
				// 	energy = conduct (0,0) * int_d_psiW_dx_d_psiV_dx +
				// 	         conduct (0,1) * int_d_psiW_dy_d_psiV_dx +
				// 	         conduct (1,0) * int_d_psiW_dx_d_psiV_dy +
				// 	         conduct (1,1) * int_d_psiW_dy_d_psiV_dy;
				CH (0,0) += energy *
					( vector_sol_case_x ( numbering(W) ) + jump_of_sol_case_x ( winding_W ) ) *
					( vector_sol_case_x ( numbering(V) ) + jump_of_sol_case_x ( winding_V ) );
				CH (0,1) += energy *
					( vector_sol_case_y ( numbering(W) ) + jump_of_sol_case_y ( winding_W ) ) *
					( vector_sol_case_x ( numbering(V) ) + jump_of_sol_case_x ( winding_V ) );
				CH (1,0) += energy *
					( vector_sol_case_x ( numbering(W) ) + jump_of_sol_case_x ( winding_W ) ) *
					( vector_sol_case_y ( numbering(V) ) + jump_of_sol_case_y ( winding_V ) );
				CH (1,1) += energy *
					( vector_sol_case_y ( numbering(W) ) + jump_of_sol_case_y ( winding_W ) ) *
					( vector_sol_case_y ( numbering(V) ) + jump_of_sol_case_y ( winding_V ) );
				assert ( W == seg_W .base() .reverse() );
				winding_W += seg_W .winding() ;
				W = seg_W .tip();
				seg_W = small_tri .boundary() .cell_in_front_of (W);
			}  // end of loop in W
			// here winding_V_W should be zero again
			assert ( winding_W == 0 );
			assert ( V == seg_V .base() .reverse() );
			winding_V += seg_V .winding() ;
			V = seg_V .tip();
			seg_V = small_tri .boundary() .cell_in_front_of (V);
		}  // end of loop in V
		assert ( winding_V == 0 );
	}  // end of loop in small_tri
	} // just a block of code for hiding 'it'

	std::cout << "homogenized conductivity tensor computed as quadratic form" << std::endl;
	for ( size_t i = 0; i < 2; i++ )
	{	for ( size_t j = 0; j < 2; j++ )
			std::cout << CH (i,j) << " ";
		std::cout << std::endl;           }

	// compute CH as a quadratic form, second try
	// take advantage of the already assembled finite element matrix
	// only compute new integrals on triangles with non-zero winding
	for ( size_t i = 0; i < 2; i++ )
	for ( size_t j = 0; j < 2; j++ )
		CH (i,j) = 0.;
	
	// run over all vertices in 'entire_torus'
	{ // just a block of code for hiding 'it_ver'
	Mesh::Iterator it_ver = entire_torus .iterator ( tag::over_vertices );
	for ( it_ver .reset(); it_ver .in_range(); it_ver ++ )
	{	Cell V = * it_ver;
		CH (0,0) += matrix_A .coeff ( numbering(V), numbering(V) ) *
		   vector_sol_case_x ( numbering(V) ) * vector_sol_case_x ( numbering(V) );
		CH (0,1) += matrix_A .coeff ( numbering(V), numbering(V) ) *
		   vector_sol_case_y ( numbering(V) ) * vector_sol_case_x ( numbering(V) );
		CH (1,0) += matrix_A .coeff ( numbering(V), numbering(V) ) *
		   vector_sol_case_x ( numbering(V) ) * vector_sol_case_y ( numbering(V) );
		CH (1,1) += matrix_A .coeff ( numbering(V), numbering(V) ) *
		   vector_sol_case_y ( numbering(V) ) * vector_sol_case_y ( numbering(V) );  }
	} // just a block of code for hiding 'it_ver'
	
	// run over all segments of 'entire_torus'
	{ // just a block of code for hiding 'it_seg'
	Mesh::Iterator it_seg = entire_torus .iterator ( tag::over_cells_of_dim, 1 );
	for ( it_seg .reset(); it_seg .in_range(); it_seg ++ )
	{	Cell seg = * it_seg;
		Cell V = seg .base() .reverse ( tag::surely_exists );
		Cell W = seg .tip();
		CH (0,0) += 2. * matrix_A .coeff ( numbering(V), numbering(W) ) *
			vector_sol_case_x ( numbering(W) ) * vector_sol_case_x ( numbering(V) );
		CH (0,1) += matrix_A .coeff ( numbering(V), numbering(W) ) *
			(   vector_sol_case_y ( numbering(W) ) * vector_sol_case_x ( numbering(V) )  // +
			  + vector_sol_case_x ( numbering(W) ) * vector_sol_case_y ( numbering(V) ) );
		CH (1,0) += matrix_A .coeff ( numbering(V), numbering(W) ) *
			(   vector_sol_case_x ( numbering(W) ) * vector_sol_case_y ( numbering(V) )  // +
			  + vector_sol_case_y ( numbering(W) ) * vector_sol_case_x ( numbering(V) ) );
		CH (1,1) += 2. * matrix_A .coeff ( numbering(V), numbering(W) ) *
			vector_sol_case_y ( numbering(W) ) * vector_sol_case_y ( numbering(V) );
	} // end of loop over segments of 'entire_torus'
	} // just a block of code for hiding 'it_seg'

	// hand-coded integrators require an early declaration of
	// the integrals we intend to compute later (after docking 'fe' on a cell)
	fe .pre_compute ( tag::for_given, tag::basis_functions, bf1, bf2,
	                  tag::integral_of, { bf1 .deriv ( x_torus ) * bf2 .deriv ( x_torus ),
	                                      bf1 .deriv ( x_torus ) * bf2 .deriv ( y_torus ),
	                                      bf1 .deriv ( y_torus ) * bf2 .deriv ( x_torus ),
	                                      bf1 .deriv ( y_torus ) * bf2 .deriv ( y_torus ) } );

	// run over all triangular cells composing 'entire_torus', skipping those with zero winding
	{ // just a block of code for hiding 'it_tri'
	Mesh::Iterator it_tri = entire_torus .iterator ( tag::over_cells_of_max_dim );
	for ( it_tri .reset(); it_tri .in_range(); it_tri ++ )
	{	Cell small_tri = * it_tri;
		fe .dock_on ( small_tri, tag::winding );
		// run twice over the three vertices of 'small_tri'
		Mesh::Iterator it_V = small_tri .boundary() .iterator ( tag::over_vertices );
		it_V .reset();  assert ( it_V .in_range() );
		Cell A = * it_V;
		Cell AB = small_tri .boundary() .cell_in_front_of ( A, tag::surely_exists );
		Cell B = AB .tip();
		Cell BC = small_tri .boundary() .cell_in_front_of ( B, tag::surely_exists );
		Cell C = BC .tip();
		Manifold::Action winding_AB = AB .winding(),
		                 winding_BC = BC .winding();
		if ( ( winding_AB == 0 ) and ( winding_BC == 0 ) )  continue;
		Manifold::Action winding_AC = winding_AB + winding_BC;
		assert (   small_tri .boundary() .cell_in_front_of ( C, tag::surely_exists ) .winding() // +
		         + winding_AC                                                                   == 0 );
		Function psi_A = fe .basis_function (A),
		         psi_B = fe .basis_function (B),
		         psi_C = fe .basis_function (C);
		// 'fe' is already docked on 'small_tri' so this will be the domain of integration
		std::vector < double > integrals = fe .integrate
			( tag::pre_computed, tag::replace, bf1, tag::by, psi_B,
			                     tag::replace, bf2, tag::by, psi_B );
		assert ( integrals .size() == 4 );
		// double int_d_psiB_dx_d_psiB_dx = fe .integrate ( d_psiB_dx * d_psiB_dx );
		// double int_d_psiB_dx_d_psiB_dy = fe .integrate ( d_psiB_dx * d_psiB_dy );
		// double int_d_psiB_dy_d_psiB_dx = fe .integrate ( d_psiB_dy * d_psiB_dx );
		// double int_d_psiB_dy_d_psiB_dy = fe .integrate ( d_psiB_dy * d_psiB_dy );
		double energy = conduct (0,0) * integrals [0] +
		                conduct (0,1) * integrals [2] +
		                conduct (1,0) * integrals [1] +
		                conduct (1,1) * integrals [3];
		// 	energy = conduct (0,0) * int_d_psiB_dx_d_psiB_dx +
		// 	         conduct (0,1) * int_d_psiB_dy_d_psiB_dx +
		// 	         conduct (1,0) * int_d_psiB_dx_d_psiB_dy +
		// 	         conduct (1,1) * int_d_psiB_dy_d_psiB_dy;
		CH (0,0) += energy *
			(   2. * vector_sol_case_x ( numbering(B) ) * jump_of_sol_case_x ( winding_AB ) // +
			  + jump_of_sol_case_x ( winding_AB ) * jump_of_sol_case_x ( winding_AB )       );
		CH (0,1) += energy *
			(   vector_sol_case_y ( numbering(B) ) * jump_of_sol_case_x ( winding_AB ) // +
			  + vector_sol_case_x ( numbering(B) ) * jump_of_sol_case_y ( winding_AB ) // +
			  + jump_of_sol_case_y ( winding_AB ) * jump_of_sol_case_x ( winding_AB )  );
		CH (1,0) += energy *
			(   vector_sol_case_x ( numbering(B) ) * jump_of_sol_case_y ( winding_AB ) // +
			  + vector_sol_case_y ( numbering(B) ) * jump_of_sol_case_x ( winding_AB ) // +
			  + jump_of_sol_case_x ( winding_AB ) * jump_of_sol_case_y ( winding_AB )  );
		CH (1,1) += energy *
			(   2. * vector_sol_case_y ( numbering(B) ) * jump_of_sol_case_y ( winding_AB ) // +
			  + jump_of_sol_case_y ( winding_AB ) * jump_of_sol_case_y ( winding_AB )       );
		// 'fe' is already docked on 'small_tri' so this will be the domain of integration
		integrals = fe .integrate
			( tag::pre_computed, tag::replace, bf1, tag::by, psi_C,
			                     tag::replace, bf2, tag::by, psi_C );
		assert ( integrals .size() == 4 );
		// double int_d_psiC_dx_d_psiC_dx = fe .integrate ( d_psiC_dx * d_psiC_dx );
		// double int_d_psiC_dx_d_psiC_dy = fe .integrate ( d_psiC_dx * d_psiC_dy );
		// double int_d_psiC_dy_d_psiC_dx = fe .integrate ( d_psiC_dy * d_psiC_dx );
		// double int_d_psiC_dy_d_psiC_dy = fe .integrate ( d_psiC_dy * d_psiC_dy );
		energy = conduct (0,0) * integrals [0] +
		         conduct (0,1) * integrals [2] +
		         conduct (1,0) * integrals [1] +
		         conduct (1,1) * integrals [3];
		// 	energy = conduct (0,0) * int_d_psiC_dx_d_psiC_dx +
		// 	         conduct (0,1) * int_d_psiC_dy_d_psiC_dx +
		// 	         conduct (1,0) * int_d_psiC_dx_d_psiC_dy +
		// 	         conduct (1,1) * int_d_psiC_dy_d_psiC_dy;
		CH (0,0) += energy *
			(   2. * vector_sol_case_x ( numbering(C) ) * jump_of_sol_case_x ( winding_AC ) // +
			  + jump_of_sol_case_x ( winding_AC ) * jump_of_sol_case_x ( winding_AC )       );
		CH (0,1) += energy *
			(   vector_sol_case_y ( numbering(C) ) * jump_of_sol_case_x ( winding_AC ) // +
			  + vector_sol_case_x ( numbering(C) ) * jump_of_sol_case_y ( winding_AC ) // +
			  + jump_of_sol_case_y ( winding_AC ) * jump_of_sol_case_x ( winding_AC )  );
		CH (1,0) += energy *
			(   vector_sol_case_x ( numbering(C) ) * jump_of_sol_case_y ( winding_AC ) // +
			  + vector_sol_case_y ( numbering(C) ) * jump_of_sol_case_x ( winding_AC ) // +
			  + jump_of_sol_case_x ( winding_AC ) * jump_of_sol_case_y ( winding_AC )  );
		CH (1,1) += energy *
			(   2. * vector_sol_case_y ( numbering(C) ) * jump_of_sol_case_y ( winding_AC ) // +
			  + jump_of_sol_case_y ( winding_AC ) * jump_of_sol_case_y ( winding_AC )       );
		// 'fe' is already docked on 'small_tri' so this will be the domain of integration
		integrals = fe .integrate
			( tag::pre_computed, tag::replace, bf1, tag::by, psi_A,
			                     tag::replace, bf2, tag::by, psi_B );
		assert ( integrals .size() == 4 );
		// double int_d_psiA_dx_d_psiB_dx = fe .integrate ( d_psiA_dx * d_psiB_dx );
		// double int_d_psiA_dx_d_psiB_dy = fe .integrate ( d_psiA_dx * d_psiB_dy );
		// double int_d_psiA_dy_d_psiB_dx = fe .integrate ( d_psiA_dy * d_psiB_dx );
		// double int_d_psiA_dy_d_psiB_dy = fe .integrate ( d_psiA_dy * d_psiB_dy );
		energy = conduct (0,0) * integrals [0] +
		         conduct (0,1) * integrals [2] +
		         conduct (1,0) * integrals [1] +
		         conduct (1,1) * integrals [3];
		// 	energy = conduct (0,0) * int_d_psiA_dx_d_psiB_dx +
		// 	         conduct (0,1) * int_d_psiA_dy_d_psiB_dx +
		// 	         conduct (1,0) * int_d_psiA_dx_d_psiB_dy +
		// 	         conduct (1,1) * int_d_psiA_dy_d_psiB_dy;
		CH (0,0) += 2. * energy *
			vector_sol_case_x ( numbering(A) ) * jump_of_sol_case_x ( winding_AB );
		CH (0,1) += energy *
			(   vector_sol_case_y ( numbering(A) ) * jump_of_sol_case_x ( winding_AB ) // +
			  + vector_sol_case_x ( numbering(A) ) * jump_of_sol_case_y ( winding_AB ) );
		CH (1,0) += energy *
			(   vector_sol_case_x ( numbering(A) ) * jump_of_sol_case_y ( winding_AB ) // +
			  + vector_sol_case_y ( numbering(A) ) * jump_of_sol_case_x ( winding_AB ) );
		CH (1,1) += 2. * energy *
			vector_sol_case_y ( numbering(A) ) * jump_of_sol_case_y ( winding_AB );
		// 'fe' is already docked on 'small_tri' so this will be the domain of integration
		integrals = fe .integrate
			( tag::pre_computed, tag::replace, bf1, tag::by, psi_A,
			                     tag::replace, bf2, tag::by, psi_C );
		assert ( integrals .size() == 4 );
		// double int_d_psiA_dx_d_psiC_dx = fe .integrate ( d_psiA_dx * d_psiC_dx );
		// double int_d_psiA_dx_d_psiC_dy = fe .integrate ( d_psiA_dx * d_psiC_dy );
		// double int_d_psiA_dy_d_psiC_dx = fe .integrate ( d_psiA_dy * d_psiC_dx );
		// double int_d_psiA_dy_d_psiC_dy = fe .integrate ( d_psiA_dy * d_psiC_dy );
		energy = conduct (0,0) * integrals [0] +
		         conduct (0,1) * integrals [2] +
		         conduct (1,0) * integrals [1] +
		         conduct (1,1) * integrals [3];
		// 	energy = conduct (0,0) * int_d_psiA_dx_d_psiC_dx +
		// 	         conduct (0,1) * int_d_psiA_dy_d_psiC_dx +
		// 	         conduct (1,0) * int_d_psiA_dx_d_psiC_dy +
		// 	         conduct (1,1) * int_d_psiA_dy_d_psiC_dy;
		CH (0,0) += 2. * energy *
			vector_sol_case_x ( numbering(A) ) * jump_of_sol_case_x ( winding_AC );
		CH (0,1) += energy *
			(   vector_sol_case_y ( numbering(A) ) * jump_of_sol_case_x ( winding_AC ) // +
			  + vector_sol_case_x ( numbering(A) ) * jump_of_sol_case_y ( winding_AC ) );
		CH (1,0) += energy *
			(   vector_sol_case_x ( numbering(A) ) * jump_of_sol_case_y ( winding_AC ) // +
			  + vector_sol_case_y ( numbering(A) ) * jump_of_sol_case_x ( winding_AC ) );
		CH (1,1) += 2. * energy *
			vector_sol_case_y ( numbering(A) ) * jump_of_sol_case_y ( winding_AC );
		// 'fe' is already docked on 'small_tri' so this will be the domain of integration
		integrals = fe .integrate
			( tag::pre_computed, tag::replace, bf1, tag::by, psi_B,
			                     tag::replace, bf2, tag::by, psi_C );
		assert ( integrals .size() == 4 );
		// double int_d_psiB_dx_d_psiC_dx = fe .integrate ( d_psiB_dx * d_psiC_dx );
		// double int_d_psiB_dx_d_psiC_dy = fe .integrate ( d_psiB_dx * d_psiC_dy );
		// double int_d_psiB_dy_d_psiC_dx = fe .integrate ( d_psiB_dy * d_psiC_dx );
		// double int_d_psiB_dy_d_psiC_dy = fe .integrate ( d_psiB_dy * d_psiC_dy );
		energy = conduct (0,0) * integrals [0] +
		         conduct (0,1) * integrals [2] +
		         conduct (1,0) * integrals [1] +
		         conduct (1,1) * integrals [3];
		// 	energy = conduct (0,0) * int_d_psiB_dx_d_psiC_dx +
		// 	         conduct (0,1) * int_d_psiB_dy_d_psiC_dx +
		// 	         conduct (1,0) * int_d_psiB_dx_d_psiC_dy +
		// 	         conduct (1,1) * int_d_psiB_dy_d_psiC_dy;
		CH (0,0) += 2. * energy *
			(   ( vector_sol_case_x ( numbering(B) ) + jump_of_sol_case_x ( winding_AB ) ) *
			    ( vector_sol_case_x ( numbering(C) ) + jump_of_sol_case_x ( winding_AC ) ) // -
			  - vector_sol_case_x ( numbering(B) ) * vector_sol_case_x ( numbering(C) )    );
		CH (0,1) += energy *
			(   ( vector_sol_case_y ( numbering(B) ) + jump_of_sol_case_y ( winding_AB ) ) *
			    ( vector_sol_case_x ( numbering(C) ) + jump_of_sol_case_x ( winding_AC ) ) // -
			  - vector_sol_case_y ( numbering(B) ) * vector_sol_case_x ( numbering(C) )  // +
			  + ( vector_sol_case_x ( numbering(B) ) + jump_of_sol_case_x ( winding_AB ) ) *
			    ( vector_sol_case_y ( numbering(C) ) + jump_of_sol_case_y ( winding_AC ) ) // -
			  - vector_sol_case_x ( numbering(B) ) * vector_sol_case_y ( numbering(C) )    );
		CH (1,0) += energy *
			(   ( vector_sol_case_y ( numbering(B) ) + jump_of_sol_case_y ( winding_AB ) ) *
			    ( vector_sol_case_x ( numbering(C) ) + jump_of_sol_case_x ( winding_AC ) ) // -
			  - vector_sol_case_y ( numbering(B) ) * vector_sol_case_x ( numbering(C) )  // +
			  + ( vector_sol_case_x ( numbering(B) ) + jump_of_sol_case_x ( winding_AB ) ) *
			    ( vector_sol_case_y ( numbering(C) ) + jump_of_sol_case_y ( winding_AC ) ) // -
			  - vector_sol_case_x ( numbering(B) ) * vector_sol_case_y ( numbering(C) )    );
		CH (1,1) += 2. * energy *
			(   ( vector_sol_case_y ( numbering(B) ) + jump_of_sol_case_y ( winding_AB ) ) *
			    ( vector_sol_case_y ( numbering(C) ) + jump_of_sol_case_y ( winding_AC ) ) // -
			  - vector_sol_case_y ( numbering(B) ) * vector_sol_case_y ( numbering(C) )    );
	}  // end of loop in 'small_tri'
	} // just a block of code for hiding 'it'
	
	std::cout << "homogenized conductivity tensor computed as quadratic form" << std::endl;
	for ( size_t i = 0; i < 2; i++ )
	{	for ( size_t j = 0; j < 2; j++ )
			std::cout << CH (i,j) << " ";
		std::cout << std::endl;           }
	
	entire_torus .unfold ( tag::over_region,      coord_x*coord_x*coord_x*coord_x  // +
	                                         + 2.*coord_y*coord_y*coord_y*coord_y < 15. )
		.export_to_file ( tag::gmsh, "torus.msh" );
	
}  // end of main


//-----------------------------------------------------------------------------------------

