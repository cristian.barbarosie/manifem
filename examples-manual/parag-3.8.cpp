
// example presented in paragraph 3.8 of the manual
// https://maniFEM.rd.ciencias.ulisboa.pt/manual-maniFEM.pdf
// fill with tetrahedra the sphere built in paragraph 3.6

#include "maniFEM.h"

using namespace maniFEM;


int main ()
	
{	Manifold RR3 ( tag::Euclid, tag::of_dim, 3 );
	Function xyz = RR3 .build_coordinate_system ( tag::Lagrange, tag::of_degree, 1 );
	Function x = xyz [0], y = xyz [1], z = xyz [2];

	const double l = 0.15;
	// in paragraph 3.6 l was 0.11, here we increase it a little bit
	// otherwise the code takes a very long time to run

	std::cout << "this example takes time" << std::endl;
	RR3 .implicit ( x*x + y*y + z*z == 1. );
	Mesh sphere = Mesh::Build ( tag::frontal ) .entire_manifold() .desired_length ( l );

	RR3 .set_as_working_manifold();
	Mesh ball = Mesh::Build ( tag::frontal ) .boundary ( sphere ) .desired_length ( l );

	ball .export_to_file ( tag::gmsh, "ball.msh" );
	std::cout << "produced file ball.msh" << std::endl;

}  // end of main

