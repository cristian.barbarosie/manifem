
// example presented in paragraph 11.9 of the manual
// https://maniFEM.rd.ciencias.ulisboa.pt/manual-maniFEM.pdf
// solve Laplace equation in a ball with tetrahedral finite elements

#include "maniFEM.h"
#include <fstream>
#include <Eigen/SparseCore>
#include <Eigen/IterativeLinearSolvers>

using namespace maniFEM;


void impose_value_of_unknown  // fast  // see below for an equivalent, more readable, version
( Eigen::SparseMatrix < double > & matrix_A, Eigen::VectorXd & vector_b,
  const size_t i, const double val                                      )

// in a system of linear equations, destroy equation 'i' and impose u(i) = val
// change also column 'i' of the matrix, just to preserve symmetry

// used for imposing Dirichlet boundary conditions

{	assert ( ! matrix_A .IsRowMajor );
	const size_t size_matrix = matrix_A .innerSize();

	// destroy row i, even the diagonal entry, change vector_b
	for ( size_t j = 0; j < size_matrix; j++ )
	{	if ( matrix_A .coeff ( i, j ) != 0. )
			matrix_A .coeffRef ( i, j ) = 0.;
		vector_b (j) -= matrix_A .coeff ( j, i ) * val;  }

	// destroy column i, set diagonal entry to 1.
	const size_t oi = matrix_A .outerIndexPtr() [i];     // access to matrix_A .m_outerIndex
	// std::cout << i << " " << oi << " " << matrix_A .innerNonZeroPtr() [i] << " "
	//           << matrix_A .innerIndexPtr() [ oi ] << " " << matrix_A .valuePtr() [ oi ] << std::endl;
	matrix_A .innerNonZeroPtr() [i] = 1;                 // access to matrix_A .m_innerNonZeros
	matrix_A .innerIndexPtr() [ oi ] = i;                // access to matrix_A .m_data .m_indices
	matrix_A .valuePtr() [ oi ] = 1.;                    // access to matrix_A .m_data .m_values

	// change vector
	vector_b (i) = val;                                     }


void impose_value_of_unknown_readable
( Eigen::SparseMatrix < double > & matrix_A, Eigen::VectorXd & vector_b,
  const size_t i, const double val                                      )

// in a system of linear equations, destroy equation 'i' and impose u(i) = val
// change also column 'i' of the matrix, just to preserve symmetry

// used for imposing Dirichlet boundary conditions

{	assert ( ! matrix_A .IsRowMajor );
	const size_t size_matrix = matrix_A .innerSize();

	// destroy raw i, even the diagonal entry
	for ( size_t j = 0; j < size_matrix; j++ )
		if ( matrix_A .coeff ( i, j ) != 0. )
			matrix_A .coeffRef ( i, j ) = 0.;

	// destroy column i, change vector
	for ( size_t j = 0; j < size_matrix; j++ )
	{	if ( matrix_A .coeff ( j, i ) == 0. )  continue;
		double & Aji = matrix_A .coeffRef ( j, i );
		vector_b (j) -= Aji * val;
		Aji = 0.;                                         }

	// set diagonal entry to 1., change vector
	matrix_A .coeffRef ( i, i ) = 1.;
	vector_b (i) = val;                                     }

//------------------------------------------------------------------------------------------------------//


int main ()
	
{	Manifold RR3 ( tag::Euclid, tag::of_dim, 3 );
	Function xyz = RR3 .build_coordinate_system ( tag::Lagrange, tag::of_degree, 1 );
	Function x = xyz [0], y = xyz [1], z = xyz [2];

	const double l = 0.15;

	std::cout << "this example takes time" << std::endl;
	RR3 .implicit ( x*x + y*y + z*z == 1. );
	Mesh sphere = Mesh::Build ( tag::frontal ) .entire_manifold() .desired_length ( l );

	RR3 .set_as_working_manifold();
	Mesh ball = Mesh::Build ( tag::frontal ) .boundary ( sphere ) .desired_length ( l );
	
	FiniteElement fe ( tag::with_master, tag::tetrahedron, tag::Lagrange, tag::of_degree, 1 );
	fe .set_integrator ( tag::Gauss, tag::tetra_4 );
	#ifndef NDEBUG
	std::cout << fe .info();
	#endif
	
	std::map < Cell, size_t > numbering;
	{ // just a block of code for hiding 'it' and 'counter'
	Mesh::Iterator it = ball .iterator ( tag::over_vertices );
	size_t counter = 0;
	for ( it .reset(); it .in_range(); it++ )
	{	Cell V = *it;  numbering [V] = counter;  ++counter;  }
	assert ( counter == numbering .size() );
	} // just a block of code

	const size_t size_matrix = numbering .size();
	std::cout << "global matrix " << size_matrix << "x" << size_matrix << std::endl;
	Eigen::SparseMatrix <double> matrix_A ( size_matrix, size_matrix );
	
	matrix_A .reserve ( Eigen::VectorXi::Constant ( size_matrix, 21 ) );
	// since we will be working with a mesh of tetrahedra,
	// there will be about 22 non-zero elements per column
	// the diagonal entry plus 21 neighbour vertices

	Eigen::VectorXd vector_b ( size_matrix ), vector_sol ( size_matrix );
	vector_b .setZero();
	
	// run over all tetrahedron cells composing the ball
	{ // just a block of code for hiding 'it'
	Mesh::Iterator it = ball .iterator ( tag::over_cells_of_max_dim );
	for ( it .reset(); it .in_range(); it++ )
	{	Cell small_tetra = *it;
		fe .dock_on ( small_tetra );
		// run twice over the four vertices of 'small_tetra'
		Mesh::Iterator it1 = small_tetra .boundary() .iterator ( tag::over_vertices );
		Mesh::Iterator it2 = small_tetra .boundary() .iterator ( tag::over_vertices );
		for ( it1 .reset(); it1 .in_range(); it1++ )
		for ( it2 .reset(); it2 .in_range(); it2++ )
		{	Cell V = *it1, W = *it2;  // V may be the same as W, no problem about that
			Function psiV = fe .basis_function (V),
			         psiW = fe .basis_function (W),
			         d_psiV_dx = psiV .deriv (x),
			         d_psiV_dy = psiV .deriv (y),
			         d_psiV_dz = psiV .deriv (z),
			         d_psiW_dx = psiW .deriv (x),
			         d_psiW_dy = psiW .deriv (y),
			         d_psiW_dz = psiW .deriv (z);
			// 'fe' is already docked on 'small_tetra' so this will be the domain of integration
			matrix_A .coeffRef ( numbering [V], numbering [W] ) +=
				fe .integrate ( d_psiV_dx * d_psiW_dx + d_psiV_dy * d_psiW_dy + d_psiV_dz * d_psiW_dz );  }  }
	} // just a block of code 
	
	// impose Dirichlet boundary conditions  u = xyz
	{ // just a block of code for hiding 'it'
	Mesh::Iterator it = sphere .iterator ( tag::over_vertices );
	for ( it .reset(); it .in_range(); it++ )
	{	Cell P = *it;
		const size_t i = numbering [P];
		impose_value_of_unknown ( matrix_A, vector_b, i, x(P) * y(P) * z(P) );  }
	}  // just a block of code
	
	// solve the system of linear equations
	Eigen::ConjugateGradient < Eigen::SparseMatrix < double >,
	                           Eigen::Lower | Eigen::Upper    > cg;
	cg .compute ( matrix_A );

	vector_sol = cg .solve ( vector_b );
	if ( cg .info() != Eigen::Success )
	{	std::cout << "Eigen solver.solve failed" << std::endl;
		exit (1);                                               }
		
	ball .export_to_file ( tag::gmsh, "ball-Dirichlet.msh", numbering );

	{ // just a block of code for hiding variables
	std::ofstream solution_file ( "ball-Dirichlet.msh", std::fstream::app );
	solution_file << "$NodeData" << std::endl;
	solution_file << "1" << std::endl;   // one string follows
	solution_file << "\"temperature\"" << std::endl;
	solution_file << "1" << std::endl;   //  one real follows
	solution_file << "0.0" << std::endl;  // time [??]
	solution_file << "3" << std::endl;   // three integers follow
	solution_file << "0" << std::endl;   // time step [??]
	solution_file << "1" << std::endl;  // scalar values of u
	solution_file << ball .number_of ( tag::vertices ) << std::endl;  // number of values listed below
	Mesh::Iterator it = ball .iterator ( tag::over_vertices );
	for ( it .reset(); it .in_range(); it++ )
	{	Cell P = *it;
		const size_t i = numbering [P];
		solution_file << i+1 << " " << vector_sol [i] << std::endl;   }
	} // just a block of code

	std::cout << "produced file ball-Dirichlet.msh" << std::endl;

}  // end of main

