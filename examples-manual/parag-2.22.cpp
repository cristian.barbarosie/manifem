
// example presented in paragraph 2.22 of the manual
// https://maniFEM.rd.ciencias.ulisboa.pt/manual-maniFEM.pdf
// build an imperfect donut (parametric)

#include "maniFEM.h"
#include "math.h"

using namespace maniFEM;


int main ()

{	Manifold torus ( tag::Euclid, tag::of_dim, 2 );
	Function alpha_beta = torus .build_coordinate_system ( tag::Lagrange, tag::of_degree, 1 );
	Function alpha = alpha_beta [0], beta = alpha_beta [1];

	const double pi = 4. * std::atan(1.);
	Cell A ( tag::vertex );  alpha (A) = 0.;       beta (A) = 0.;
	Cell B ( tag::vertex );  alpha (B) = 0.;       beta (B) = 1.9*pi;
	Cell C ( tag::vertex );  alpha (C) = 1.95*pi;  beta (C) = 1.9*pi;
	Cell D ( tag::vertex );  alpha (D) = 1.95*pi;  beta (D) = 0.;

	Mesh AB = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( A ) .stop_at ( B ) .divided_in ( 19 );
	Mesh BC = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( B ) .stop_at ( C ) .divided_in ( 39 );
	Mesh CD = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( C ) .stop_at ( D ) .divided_in ( 19 );
	Mesh DA = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( D ) .stop_at ( A ) .divided_in ( 39 );

	Mesh ABCD = Mesh::Build ( tag::grid ) .shape ( tag::rectangle ) .faces ( AB, BC, CD, DA );

	const double big_radius = 3, small_radius = 1;
	Function x = ( big_radius + small_radius * cos(beta) ) * cos(alpha),
	         y = ( big_radius + small_radius * cos(beta) ) * sin(alpha),
	         z = small_radius * sin(beta);

	// forget about alpha and beta, in future statements x, y and z will be used
	Manifold RR3 ( tag::Euclid, tag::of_dimension, 3 );
	RR3 .set_coordinates ( x && y && z );

	ABCD .export_to_file ( tag::gmsh, "torus.msh" );
	std::cout << "produced file torus.msh" << std::endl;

}  // end of main
