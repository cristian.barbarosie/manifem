
// example presented in paragraph 2.2 of the manual
// https://maniFEM.rd.ciencias.ulisboa.pt/manual-maniFEM.pdf
// build an arrow-shaped mesh

#include "maniFEM.h"

using namespace maniFEM;


int main ()

{	// we choose our (geometric) space dimension :
	Manifold RR2 ( tag::Euclid, tag::of_dim, 2 );
	
	// xy is a map defined on our future mesh with values in RR2 :
	Function xy = RR2 .build_coordinate_system ( tag::Lagrange, tag::of_degree, 1 );

	// we can extract components of xy using the [] operator :
	Function x = xy[0], y = xy[1];

	Cell A ( tag::vertex );  x(A) = 0.;  y(A) =  0.  ;
	Cell B ( tag::vertex );  x(B) = 1.;  y(B) = -0.5 ;
	Cell C ( tag::vertex );  x(C) = 1.;  y(C) = -0.25;
	Cell D ( tag::vertex );  x(D) = 3.;  y(D) = -0.25;
	Cell E ( tag::vertex );  x(E) = 3.;  y(E) =  0.25;
	Cell F ( tag::vertex );  x(F) = 1.;  y(F) =  0.25;
	Cell G ( tag::vertex );  x(G) = 1.;  y(G) =  0.5 ;
	Mesh AB = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( A ) .stop_at ( B ) .divided_in ( 15 );
	Mesh BC = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( B ) .stop_at ( C ) .divided_in ( 4 );
	Mesh CF = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( C ) .stop_at ( F ) .divided_in ( 7 );
	Mesh CD = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( C ) .stop_at ( D ) .divided_in ( 25 );
	Mesh DE = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( D ) .stop_at ( E ) .divided_in ( 7 );
	Mesh EF = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( E ) .stop_at ( F ) .divided_in ( 25 );
	Mesh FG = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( F ) .stop_at ( G ) .divided_in ( 4 );
	Mesh GA = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( G ) .stop_at ( A ) .divided_in ( 15 );

	Mesh BG = Mesh::Build ( tag::join ) .meshes ( { BC, CF, FG } );
	
	Mesh ABG = Mesh::Build ( tag::grid ) .shape ( tag::triangle )
		.faces ( AB, BG, GA );
	Mesh CDEF = Mesh::Build ( tag::grid ) .shape ( tag::rectangle )
		.faces ( CD, DE, EF, CF .reverse() );
	Mesh arrow = Mesh::Build ( tag::join ) .meshes ( { ABG, CDEF } );

	arrow .export_to_file ( tag::eps, "arrow.eps" );
	arrow .export_to_file ( tag::gmsh, "arrow.msh" );
	std::cout << "produced files arrow.eps and arrow.msh" << std::endl;

}  // end of main
