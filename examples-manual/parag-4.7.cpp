
// example presented in paragraph 4.7 of the manual
// https://maniFEM.rd.ciencias.ulisboa.pt/manual-maniFEM.pdf
// extruded mesh in RR3, track is closed loop
// result is a wheel rim

#include "maniFEM.h"

using namespace maniFEM;
using namespace std;



int main ()

{	Manifold RR3 ( tag::Euclid, tag::of_dim, 3 );
	Function xyz = RR3 .build_coordinate_system ( tag::Lagrange, tag::of_degree, 1 );
	Function x = xyz [0], y = xyz [1], z = xyz [2];

	RR3 .implicit ( x*x + y*y == 1., z == 0. );

	Cell N ( tag::vertex );  x(N) =  0.;  y(N) =  1.;  z(N) = 0.;
	Cell W ( tag::vertex );  x(W) = -1.;  y(W) =  0.;  z(W) = 0.;
	Cell S ( tag::vertex );  x(S) =  0.;  y(S) = -1.;  z(S) = 0.;
	Cell E ( tag::vertex );  x(E) =  1.;  y(E) =  0.;  z(E) = 0.;

	Mesh NW = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at (N) .stop_at (W) .divided_in ( 10 );
	Mesh WS = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at (W) .stop_at (S) .divided_in ( 10 );
	Mesh SE = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at (S) .stop_at (E) .divided_in ( 10 );
	Mesh EN = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at (E) .stop_at (N) .divided_in ( 10 );

	Mesh circle = Mesh::Build ( tag::join ) .meshes ( { NW, WS, SE, EN } );

	RR3 .implicit ( x ==  z*z + 1., y == 0. );
	  
	Cell X ( tag::vertex );  x(X) = 1.49;   y(X) = 0.;  z(X) = -0.7;
	Cell Y ( tag::vertex );  x(Y) = 1.49;   y(Y) = 0.;  z(Y) =  0.7;
	Mesh XE = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at (X) .stop_at (E) .divided_in ( 10 );
	Mesh EY = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at (E) .stop_at (Y) .divided_in ( 10 );
	Mesh XY = Mesh::Build ( tag::join ) .meshes ( { XE, EY } );

	RR3 .set_as_working_manifold();
	Mesh gelly = Mesh::Build ( tag::extrude ) .swatch ( XY ) .slide_along ( circle ) .follow_curvature();
	
	Mesh::Iterator it = XY .iterator ( tag::over_cells_of_max_dim );
	for ( it .reset(); it .in_range(); it ++ )
		assert ( (*it) .belongs_to ( gelly ) );
	it = circle .iterator ( tag::over_cells_of_max_dim );
	for ( it .reset(); it .in_range(); it ++ )
		assert ( (*it) .belongs_to ( gelly ) );
	
	gelly .reverse() .export_to_file ( tag::gmsh, "rim.msh" );
	std::cout << "produced file rim.msh" << std::endl;

}  // end of main



