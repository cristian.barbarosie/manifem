
// example presented in paragraph 2.8 of the manual
// https://maniFEM.rd.ciencias.ulisboa.pt/manual-maniFEM.pdf
// build a hemisphere by joining four curved triangles

#include "maniFEM.h"

using namespace maniFEM;


int main ()

{	Manifold RR3 ( tag::Euclid, tag::of_dim, 3 );
	Function xyz = RR3 .build_coordinate_system ( tag::Lagrange, tag::of_degree, 1 );
	Function x = xyz [0], y = xyz [1], z = xyz [2];

	RR3.implicit ( x*x + y*y + z*z == 1. );

	// let's mesh halph of a sphere
	Cell E ( tag::vertex );  x(E)  =  1.;   y(E)  =  0.;   z(E)  = 0.;
	Cell N ( tag::vertex );  x(N)  =  0.;   y(N)  =  1.;   z(N)  = 0.;
	Cell W ( tag::vertex );  x(W)  = -1.;   y(W)  =  0.;   z(W)  = 0.;
	Cell S ( tag::vertex );  x(S)  =  0.;   y(S)  = -1.;   z(S)  = 0.;
	Cell up( tag::vertex );  x(up) =  0.;   y(up) =  0.;   z(up) = 1.;
	int n = 15;
	Mesh EN = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( E ) .stop_at ( N ) .divided_in ( n );
	Mesh NW = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( N ) .stop_at ( W ) .divided_in ( n );
	Mesh WS = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( W ) .stop_at ( S ) .divided_in ( n );
	Mesh SE = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( S ) .stop_at ( E ) .divided_in ( n );
	Mesh upE = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( up ) .stop_at ( E ) .divided_in ( n );
	Mesh upN = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( up ) .stop_at ( N ) .divided_in ( n );
	Mesh upW = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( up ) .stop_at ( W ) .divided_in ( n );
	Mesh upS = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( up ) .stop_at ( S ) .divided_in ( n );
	
	// now four triangles
	Mesh ENup = Mesh::Build ( tag::grid ) .shape ( tag::triangle )
		.faces ( EN, upN .reverse(), upE );
	Mesh NWup = Mesh::Build ( tag::grid ) .shape ( tag::triangle )
		.faces ( NW, upW .reverse(), upN );
	Mesh WSup = Mesh::Build ( tag::grid ) .shape ( tag::triangle )
		.faces ( WS, upS .reverse(), upW );
	Mesh SEup = Mesh::Build ( tag::grid ) .shape ( tag::triangle )
		.faces ( SE, upE .reverse(), upS );

	// and finally join the triangles :
	Mesh hemisphere = Mesh::Build ( tag::join ) .meshes ( { ENup, NWup, WSup, SEup } );

	hemisphere .export_to_file ( tag::gmsh, "hemisphere.msh");
	
	std::cout << "produced file hemisphere.msh" << std::endl;

}  // end of main
