
// example presented in paragraph 10.4 of the manual
// https://maniFEM.rd.ciencias.ulisboa.pt/manual-maniFEM.pdf
// in a mesh of triangles, flip segments with the aim of controlling the number of neighbours

// the manual shows a mesh on an ellipse
// however, in the meanwhile the frontal mesh generation algorithm has changed slightly
// and the "defect" does not show up anymore

// below we present a different mesh, inspired in paragraph 2.8


#include "maniFEM.h"
#include <set>

using namespace maniFEM;


void limit_number_of_neighbours ( Mesh msh );


int main ( )

{	Manifold RR3 ( tag::Euclid, tag::of_dim, 3 );
	Function xyz = RR3 .build_coordinate_system ( tag::Lagrange, tag::of_degree, 1 );
	Function x = xyz [0], y = xyz [1], z = xyz [2];

	RR3.implicit ( x*x + y*y + z*z == 1. );

	// let's mesh half of a sphere
	Cell E ( tag::vertex );  x(E)  =  1.;   y(E)  =  0.;   z(E)  = 0.;
	Cell N ( tag::vertex );  x(N)  =  0.;   y(N)  =  1.;   z(N)  = 0.;
	Cell W ( tag::vertex );  x(W)  = -1.;   y(W)  =  0.;   z(W)  = 0.;
	Cell S ( tag::vertex );  x(S)  =  0.;   y(S)  = -1.;   z(S)  = 0.;
	Cell up( tag::vertex );  x(up) =  0.;   y(up) =  0.;   z(up) = 1.;
	int n = 10;
	Mesh EN = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( E ) .stop_at ( N ) .divided_in ( n );
	Mesh NW = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( N ) .stop_at ( W ) .divided_in ( n );
	Mesh WS = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( W ) .stop_at ( S ) .divided_in ( n );
	Mesh SE = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( S ) .stop_at ( E ) .divided_in ( n );
	Mesh upE = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( up ) .stop_at ( E ) .divided_in ( n );
	Mesh upN = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( up ) .stop_at ( N ) .divided_in ( n );
	Mesh upW = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( up ) .stop_at ( W ) .divided_in ( n );
	Mesh upS = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( up ) .stop_at ( S ) .divided_in ( n );
	
	// now four triangles
	Mesh ENup = Mesh::Build ( tag::grid ) .shape ( tag::triangle )
		.faces ( EN, upN .reverse(), upE );
	Mesh NWup = Mesh::Build ( tag::grid ) .shape ( tag::triangle )
		.faces ( NW, upW .reverse(), upN );
	Mesh WSup = Mesh::Build ( tag::grid ) .shape ( tag::triangle )
		.faces ( WS, upS .reverse(), upW );
	Mesh SEup = Mesh::Build ( tag::grid ) .shape ( tag::triangle )
		.faces ( SE, upE .reverse(), upS );

	// and finally join the triangles :
	Mesh hemisphere = Mesh::Build ( tag::join ) .meshes ( { ENup, NWup, WSup, SEup } );
	
	hemisphere .export_to_file ( tag::gmsh, "hemisphere-before.msh" );

	limit_number_of_neighbours ( hemisphere );

	hemisphere .export_to_file ( tag::gmsh, "hemisphere-after.msh" );
	std::cout << "produced files hemisphere-before.msh and hemisphere-after.msh" << std::endl;

}  // end of main

//------------------------------------------------------------------------------------------------------//


inline bool flip_segment ( Mesh & msh, Cell & seg )

// flip 'seg' if it is inner to 'msh'
// equilibrates (barycenter) the four neighbour vertices

// return true if the segment has been flipped, false if not

// assumes there are only triangular cells
	
// assumes there is no higher-dimensional mesh "above" 'msh'

{	Cell tri2 = msh.cell_in_front_of ( seg, tag::may_not_exist );
	if ( not tri2 .exists() ) return false; 
	Cell tri1 = msh .cell_behind ( seg, tag::may_not_exist );
	if ( not tri1 .exists() ) return false;
	// or, equivalently :  if ( not seg .is_inner_to ( msh ) ) return false

	Cell A = seg .base() .reverse();
	Cell B = seg .tip();

	Cell BC = tri1 .boundary() .cell_in_front_of ( B, tag::surely_exists );
	Cell CA = tri1 .boundary() .cell_behind ( A, tag::surely_exists );
	Cell AD = tri2 .boundary() .cell_in_front_of ( A, tag::surely_exists );
	Cell DB = tri2 .boundary() .cell_behind ( B, tag::surely_exists );
	Cell C = BC .tip();
	assert ( CA .base() .reverse() == C );
	Cell D = AD .tip();
	assert ( DB .base() .reverse() == D );
	
	B .cut_from_bdry_of ( seg, tag::do_not_bother );
	A .reverse() .cut_from_bdry_of ( seg, tag::do_not_bother );
	CA .cut_from_bdry_of ( tri1, tag::do_not_bother );
	DB .cut_from_bdry_of ( tri2, tag::do_not_bother );
	C .reverse() .glue_on_bdry_of ( seg, tag::do_not_bother );
	D .glue_on_bdry_of ( seg, tag::do_not_bother );
	DB .glue_on_bdry_of ( tri1, tag::do_not_bother );
	CA .glue_on_bdry_of ( tri2, tag::do_not_bother );

	tri1 .boundary() .closed_loop ( B );
	tri2 .boundary() .closed_loop ( A );

	if ( A .is_inner_to ( msh ) ) msh .barycenter ( A );
	if ( B .is_inner_to ( msh ) ) msh .barycenter ( B );
	if ( C .is_inner_to ( msh ) ) msh .barycenter ( C );
	if ( D .is_inner_to ( msh ) ) msh .barycenter ( D );

	return true;

}  // end of  flip_segment
	
//------------------------------------------------------------------------------------------------------//


double length_square ( Cell AB )
	
{	Manifold space = Manifold::working;
	assert ( space .exists() );  // we use the current (quotient) manifold
	Function coords = space .coordinates();
	Cell A = AB .base() .reverse();
	Cell B = AB .tip();
	std::vector < double > A_co = coords (A);
	std::vector < double > B_co = coords (B);
	size_t n = A_co .size();
	assert ( n == B_co .size() );
	double len_AB_2 = 0.;
	for ( size_t i = 0; i < n; i++ )
		{	double v = B_co [i] - A_co [i];  len_AB_2 += v*v;  }
	return len_AB_2;                                        }
	

class compare_lenghts_of_segs

{ public :
	inline bool operator() ( Cell AB, Cell CD ) const
	{	return length_square ( AB ) > length_square ( CD );  }
};

//------------------------------------------------------------------------------------------------------//


void limit_number_of_neighbours ( Mesh msh )

// in a mesh of triangles, flip segments in order to prevent
// vertices with only four (or three) neighbours
// and vertices with eight neighbours or more
	
// assumes there is no higher-dimensional mesh "above" 'msh'

{	std::forward_list < Cell > has_few_neighbours, has_many_neighbours;

	{ // just a block of code for hiding 'it'
	Mesh::Iterator it = msh .iterator ( tag::over_vertices );
	for ( it .reset(); it .in_range(); it++ )
	{	Cell P = * it;
		if ( not P .is_inner_to ( msh ) ) continue;
		// how many neighbours does P have ?
		size_t counter = 0;
		Mesh::Iterator it_around_P = msh .iterator ( tag::over_segments, tag::pointing_towards, P );
		for ( it_around_P .reset(); it_around_P .in_range(); it_around_P ++ )
			counter ++;
		if ( counter < 5 ) has_few_neighbours .push_front ( P );
		if ( counter > 7 ) has_many_neighbours .push_front ( P );                         }
	} // just a block of code for hiding 'it'

	// we use a map to order neighbour segments, we flip the longest one
	compare_lenghts_of_segs comp_len;
	for ( std::forward_list < Cell > ::iterator it = has_few_neighbours .begin();
				it != has_few_neighbours .end(); it ++                                 )
	{	Cell P = * it;
		// we count again the neighbours, configuration may have changed in the meanwhile
		size_t counter = 0;
		std::multiset < Cell, compare_lenghts_of_segs > ms ( comp_len );
		Mesh::Iterator it_around_P =
			msh .iterator ( tag::over_cells_of_dim, 2, tag::around, P );
		for ( it_around_P .reset(); it_around_P .in_range(); it_around_P ++ )
		{	Cell tri = * it_around_P;
			counter ++;
			assert ( tri.dim() == 2 );
			Cell SP = tri .boundary() .cell_behind ( P, tag::surely_exists );
			Cell PT = tri .boundary() .cell_in_front_of ( P, tag::surely_exists );
			Cell T = PT .tip();
			Cell TS = tri .boundary() .cell_in_front_of ( T, tag::surely_exists );
			assert ( TS .tip() == SP .base() .reverse() );
			ms .insert ( TS );                                                     }
		if ( counter > 4 ) continue;
		std::cout << "flipping a segment" << std::endl;
		for ( std::multiset < Cell, compare_lenghts_of_segs > ::iterator itt = ms .begin();
					itt != ms .end(); itt ++                                                      )
		{	Cell seg = * itt;
			if ( flip_segment ( msh, seg ) )  break;  }                                         }
			// we choose to to flip only one segment

	for ( std::forward_list < Cell > ::iterator it = has_many_neighbours .begin();
				it != has_many_neighbours .end(); it ++                                 )
	{	Cell P = * it;
		// we count again the neighbours, configuration may have changed in the meanwhile
		size_t counter = 0;
		std::multiset < Cell, compare_lenghts_of_segs > ms ( comp_len );
		Mesh::Iterator it_around_P =
			msh .iterator ( tag::over_segments, tag::pointing_towards, P );
		for ( it_around_P .reset(); it_around_P .in_range(); it_around_P ++ )
		{	Cell seg = * it_around_P;
			assert ( seg.tip() == P );
			counter ++;
			ms .insert ( seg );        }
		if ( counter < 8 ) continue;
		std::cout << "flipping a segment" << std::endl;
		for ( std::multiset < Cell, compare_lenghts_of_segs > ::iterator itt = ms .begin();
					itt != ms .end(); itt ++                                                      )
		{	Cell seg = * itt;
			if ( flip_segment ( msh, seg ) )  break;  }                                         }
			// we choose to to flip only one segment

}  // end of  limit_number_of_neighbours
	
//------------------------------------------------------------------------------------------------------//

