

#include "maniFEM.h"
using namespace maniFEM;


int main ( )

{	Mesh msh ( tag::STSI, tag::of_dim, 1 );

	Cell A ( tag::vertex ), B ( tag::vertex ), C ( tag::vertex ), D ( tag::vertex );

	Cell AB ( tag::segment, A .reverse(), B );
	Cell AC ( tag::segment, A .reverse(), C );
	Cell BD ( tag::segment, B .reverse(), D );
	Cell BC ( tag::segment, B .reverse(), C );
	Cell CD ( tag::segment, C .reverse(), D );

	std::cout << "main line 19" << std::endl;
	AB .add_to_mesh ( msh );
	std::cout << "main line 21" << std::endl;
	BC .add_to_mesh ( msh );
	std::cout << "main line 23" << std::endl;
	BD .add_to_mesh ( msh );
	std::cout << "main line 25" << std::endl;
	AB .remove_from_mesh ( msh );
	std::cout << "main line 27" << std::endl;
	// BD .remove_from_mesh ( msh );
	std::cout << "main line 29" << std::endl;
	
}  // end of main

// statement at line 28 produces error
// assertion "it_unique->first .exists()" failed: file "src/mesh.cpp", line 1408, function: virtual void maniFEM::Cell::Negative::stsi_remove_cell_behind(maniFEM::Cell::Core*, maniFEM::Mesh::STSI*)

// if we remove line 28,
// assertion "seg->base_attr .core->cell_behind_within .find (this) != seg->base_attr .core->cell_behind_within .end()" failed: file "src/mesh.cpp", line 4711, function: virtual void maniFEM::Mesh::NotZeroDim::remove_pos_seg(maniFEM::Cell::Positive::Segment*, const maniFEM::tag::MeshIsNotBdry&)

// if we remove or both lines 26 and 28,
// the destructors called implicitly at the end of the program produce error
// assertion "seg->tip_attr .core->cell_behind_within .find (this) != seg->tip_attr .core->cell_behind_within .end()" failed: file "src/mesh.cpp", line 4715, function: virtual void maniFEM::Mesh::NotZeroDim::remove_pos_seg(maniFEM::Cell::Positive::Segment*, const maniFEM::tag::MeshIsNotBdry&)

// it's not clear why the error is not the same

// if we remove line 26, the program ends normally

// also : the wrapper destructor is called twice, which is wierd ...
