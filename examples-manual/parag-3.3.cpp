
// example presented in paragraph 3.3 of the manual
// https://maniFEM.rd.ciencias.ulisboa.pt/manual-maniFEM.pdf
// mesh a disk with an eccentric hole

#include "maniFEM.h"

using namespace maniFEM;


int main ( )

{	Manifold RR2 ( tag::Euclid, tag::of_dim, 2 );
	Function xy = RR2 .build_coordinate_system ( tag::Lagrange, tag::of_degree, 1 );
	Function x = xy[0], y = xy[1];

	double d = 0.1;

	Manifold circle = RR2 .implicit ( x*x + y*y == 1. );
	Mesh outer = Mesh::Build ( tag::frontal )
		.entire_manifold()
		.desired_length ( d );

	double y0 = 0.36;
	Manifold ellipse = RR2 .implicit ( x*x + (y-y0)*(y-y0) + 0.3*x*y == 0.25 );
	Mesh inner = Mesh::Build ( tag::frontal )
		.entire_manifold()
		.desired_length ( d );

	Mesh circles = Mesh::Build ( tag::join ) .mesh ( outer ) .mesh ( inner .reverse() );

	RR2 .set_as_working_manifold();
	Mesh disk = Mesh::Build ( tag::frontal )
		.boundary ( circles )
		.desired_length ( d );

	disk .export_to_file ( tag::gmsh, "disk.msh" );
	disk .export_to_file ( tag::eps, "disk.eps" );
	std::cout << "produced files disk.msh and disk.eps" << std::endl;

}  // end of main
