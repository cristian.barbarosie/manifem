
// read a 2d mesh in RR3 and the coordinates of a point and a radius
// produce a 2d mesh in the neighbourhood of that point (localized)

#include "maniFEM.h"

using namespace maniFEM;

int main ()

{	Manifold RR3 ( tag::Euclid, tag::of_dim, 3 );
	Function xyz = RR3 .build_coordinate_system ( tag::Lagrange, tag::of_degree, 1 );
	Function x = xyz [0], y = xyz [1], z = xyz [2];

	// const std::string n = "15307";
	double Px = 1., Py = -0.2, Pz = 0.3;
	std::cin >> Px >> Py >> Pz;
	double radius = 0.25;  std::cin >> radius;
	const double r2 = radius * radius;

	Mesh large ( tag::import, tag::gmsh, "interf.msh", tag::STSI );
	Mesh local ( tag::STSI, tag::of_dim, 2 );
	
	Mesh::Iterator it = large .iterator
		( tag::over_cells_of_max_dim, tag::orientation_compatible_with_mesh );
	for ( it .reset(); it .in_range(); it ++ )
	{	Cell tri = * it;
		bool close_to_P = false;
		Mesh::Iterator it_ver = tri .boundary() .iterator ( tag::over_vertices );
		for ( it_ver .reset(); it_ver .in_range(); it_ver ++ )
		{	Cell Q = * it_ver;
			double dist_sq = ( Px - x(Q) ) * ( Px - x(Q) )
				+ ( Py - y(Q) ) * ( Py - y(Q) ) + ( Pz - z(Q) ) * ( Pz - z(Q) );
			if ( dist_sq < r2 )
			{	close_to_P = true;  break;  }                                     }
		if ( close_to_P )  tri .add_to_mesh ( local );                             }

	assert ( local .number_of ( tag::cells_of_max_dim ) > 0 );
	local .export_to_file ( tag::gmsh, "interf-loc.msh" );
		
}  // end of main
