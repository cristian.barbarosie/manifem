# maniFEM
This directory contains several examples of use of the
[maniFEM](https://maniFEM.rd.ciencias.ulisboa.pt) library.

Each file corresponds to a paragraph in the
[manual](https://maniFEM.rd.ciencias.ulisboa.pt/manual-maniFEM.pdf).
Files named `parag-published-*.cpp` or `parag-test-*.cpp` are an exception
(they do not correspond to any paragraph in the manual).
Another exception is `parag-13.14.cpp` which is left in the root directory (parent of this one)
because we want it to work as a stand-alone example for
[MetricTree](https://codeberg.org/cristian.barbarosie/MetricTree).

To run these examples, you should be in the parent directory.
Just `make run-parag-1.1` for the example in paragraph 1.1, 
`make run-parag-2.16` for the example in paragraph 2.16, and so on.
More details [here](https://maniFEM.rd.ciencias.ulisboa.pt) or
in paragraph 12.15 of the [manual](https://maniFEM.rd.ciencias.ulisboa.pt/manual-maniFEM.pdf).

Copyright 2019 - 2025  Cristian Barbarosie  cristian.barbarosie@gmail.com

