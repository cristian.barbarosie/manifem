
// example presented in paragraph 1.6 of the manual
// https://maniFEM.rd.ciencias.ulisboa.pt/manual-maniFEM.pdf
// a mesh which mixes triangles and rectangles

#include "maniFEM.h"

using namespace maniFEM;
using namespace std;

int main ()

{	// we choose our (geometric) space dimension :
	Manifold RR2 ( tag::Euclid, tag::of_dim, 2 );
	
	// xy is a map defined on our future mesh with values in RR2 :
	Function xy = RR2 .build_coordinate_system ( tag::Lagrange, tag::of_degree, 1 );

	// we can extract components of xy using the [] operator :
	Function x = xy[0], y = xy[1];

	Cell A ( tag::vertex );  x(A) = -1. ;  y(A) = 0. ;
	Cell B ( tag::vertex );  x(B) =  0. ;  y(B) = 0. ;
	Cell C ( tag::vertex );  x(C) =  1. ;  y(C) = 0. ;
	Cell D ( tag::vertex );  x(D) = -0.5;  y(D) = 0.8;
	Cell E ( tag::vertex );  x(E) =  0.5;  y(E) = 0.8;
	Cell F ( tag::vertex );  x(F) =  0. ;  y(F) = 1. ;

	Mesh AB = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( A ) .stop_at ( B ) .divided_in ( 8 );
	Mesh BC = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( B ) .stop_at ( C ) .divided_in ( 8 );
	Mesh AD = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( A ) .stop_at ( D ) .divided_in ( 8 );
	Mesh BD = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( B ) .stop_at ( D ) .divided_in ( 8 );
	Mesh BE = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( B ) .stop_at ( E ) .divided_in ( 8 );
	Mesh CE = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( C ) .stop_at ( E ) .divided_in ( 8 );
	Mesh EF = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( E ) .stop_at ( F ) .divided_in ( 8 );
	Mesh FD = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( F ) .stop_at ( D ) .divided_in ( 8 );

	Mesh ABD = Mesh::Build ( tag::grid ) .shape ( tag::triangle )
		.faces ( AB, BD, AD .reverse() );
	Mesh BCE = Mesh::Build ( tag::grid ) .shape ( tag::triangle )
		.faces ( BC, CE, BE .reverse() );
	Mesh BEFD = Mesh::Build ( tag::grid ) .shape ( tag::quadrangle )
		.faces ( BE, EF, FD, BD .reverse() );

	Mesh two_tri_one_rect = Mesh::Build ( tag::join ) .meshes ( { ABD, BEFD, BCE } );

	two_tri_one_rect .export_to_file ( tag::gmsh, "two-tri-one-rect.msh" );
	cout << "produced file two-tri-one-rect.msh" << endl;

}  // end of main
