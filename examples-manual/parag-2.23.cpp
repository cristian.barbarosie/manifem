
// example presented in paragraph 2.23 of the manual
// https://maniFEM.rd.ciencias.ulisboa.pt/manual-maniFEM.pdf
// a cylindrical shell

#include "maniFEM.h"

using namespace maniFEM;


int main ()

{	Manifold RR3_param ( tag::Euclid, tag::of_dim, 3 );
	Function zrt = RR3_param .build_coordinate_system ( tag::Lagrange, tag::of_degree, 1 );
	Function z = zrt [0], r = zrt [1], theta = zrt [2];

	Cell A ( tag::vertex );  r(A) = 1. ;  theta(A) = 0.;  z(A) = -1.;
	Cell B ( tag::vertex );  r(B) = 1.2;  theta(B) = 0.;  z(B) = -1.;
	Cell C ( tag::vertex );  r(C) = 1.2;  theta(C) = 0.;  z(C) =  1.;
	Cell D ( tag::vertex );  r(D) = 1. ;  theta(D) = 0.;  z(D) =  1.;
	Cell E ( tag::vertex );  r(E) = 1. ;  theta(E) = 1.;  z(E) = -1.;
	Cell F ( tag::vertex );  r(F) = 1.2;  theta(F) = 1.;  z(F) = -1.;
	Cell G ( tag::vertex );  r(G) = 1.2;  theta(G) = 1.;  z(G) =  1.;
	Cell H ( tag::vertex );  r(H) = 1. ;  theta(H) = 1.;  z(H) =  1.;

	Mesh shell = Mesh::Build ( tag::grid ) .shape ( tag::cube )
		.vertices ( A, B, C, D, E, F, G, H ) .divided_in ( 5, 15, 15 );

	// cylindrical coordinates
	Function x = r * cos(theta), y = r * sin(theta);
	Manifold RR3 ( tag::Euclid, tag::of_dim, 3 );
	RR3 .set_coordinates ( x && y && z );

	shell .export_to_file ( tag::gmsh, "shell.msh" );
	std::cout << "produced file shell.msh" << std::endl;

}  // end of main


