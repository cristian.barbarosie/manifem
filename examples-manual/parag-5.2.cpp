
// example presented in paragraph 5.2 of the manual
// https://maniFEM.rd.ciencias.ulisboa.pt/manual-maniFEM.pdf
// reading composite meshes

#include "maniFEM.h"
#include "math.h"

using namespace maniFEM;


int main ()

{	Manifold RR3 ( tag::Euclid, tag::of_dim, 3 );
	Function xyz = RR3 .build_coordinate_system ( tag::Lagrange, tag::of_degree, 1 );
	Function x = xyz [0], y = xyz [1], z = xyz [2];

	Mesh::Composite cyl_sph = Mesh::Build ( tag::import_from_file )
		.format ( tag::gmsh ) .file_name ( "sphere-cyl-composite.msh" );
	Cell S        = cyl_sph .retrieve_cell ( "start 1"  );
	Mesh circle_1 = cyl_sph .retrieve_mesh ( "circle 1" );
	Mesh circle_2 = cyl_sph .retrieve_mesh ( "circle 2" );
	Mesh cylinder = cyl_sph .retrieve_mesh ( "cylinder" );
	Mesh sphere   = cyl_sph .retrieve_mesh ( "sphere"   );
	
	Mesh all = Mesh::Build ( tag::join ) .mesh ( cylinder ) .mesh ( sphere );
	all .export_to_file ( tag::gmsh, "sphere-cyl.msh" );
	std::cout << "produced file sphere-cyl.msh" << std::endl;

} // end of main

