
// example presented in paragraph 9.2 of the manual
// https://maniFEM.rd.ciencias.ulisboa.pt/manual-maniFEM.pdf
// build a ring-shaped mesh

#include "maniFEM.h"

using namespace maniFEM;


int main ( )

{	Manifold RR2 ( tag::Euclid, tag::of_dim, 2 );
	Function xy = RR2 .build_coordinate_system ( tag::Lagrange, tag::of_degree, 1 );
	Function x = xy[0], y = xy[1];

	short int n_sectors = 15;
	double step_theta = 8 * atan(1.) / n_sectors;
	short int radial_divisions = 10;
	short int rot_divisions = 5;

	// start the process by building a segment
	Cell ini_A ( tag::vertex );  x ( ini_A ) = 1.;  y ( ini_A ) = 0.;
	Cell ini_B ( tag::vertex );  x ( ini_B ) = 2.;  y ( ini_B ) = 0.;
	Mesh ini_seg = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( ini_A ) .stop_at ( ini_B ) .divided_in ( radial_divisions );
	Mesh prev_seg = ini_seg;
	Cell  A = ini_A,  B = ini_B;
	std::vector < Mesh > sectors;

	for ( short int i = 1; i < n_sectors; i++ )
	{	double theta = i * step_theta;
		// we build two new points
		Cell C ( tag::vertex );  x (C) =    cos(theta);  y (C) =    sin(theta);
		Cell D ( tag::vertex );  x (D) = 2.*cos(theta);  y (D) = 2.*sin(theta);
		// and three new segments
		Mesh BD = Mesh::Build ( tag::grid ) .shape ( tag::segment )
			.start_at ( B ) .stop_at ( D ) .divided_in ( rot_divisions );
		Mesh DC = Mesh::Build ( tag::grid ) .shape ( tag::segment )
			.start_at ( D ) .stop_at ( C ) .divided_in ( radial_divisions );
		Mesh CA = Mesh::Build ( tag::grid ) .shape ( tag::segment )
			.start_at ( C ) .stop_at ( A ) .divided_in ( rot_divisions );
		// and a quadrangle
		Mesh quadr = Mesh::Build ( tag::grid ) .shape ( tag::quadrangle )
			.faces ( prev_seg, BD, DC, CA );
		sectors .push_back ( quadr );
		prev_seg = DC .reverse();
		A = C;  B = D;                                                                }

	// we now build the last sector, thus closing the ring
	// prev_seg, A and B have rotated during the construction process
	// but ini_seg, ini_A and ini_B are the same, initial, ones
	Mesh outer = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( B ) .stop_at ( ini_B ) .divided_in ( rot_divisions );
	Mesh inner = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( ini_A ) .stop_at ( A ) .divided_in ( rot_divisions );
	Mesh quadr = Mesh::Build ( tag::grid ) .shape ( tag::quadrangle )
		.faces ( outer, ini_seg .reverse(), inner, prev_seg );
	sectors .push_back ( quadr );

	Mesh ring = Mesh::Build ( tag::join ) .meshes ( sectors );

	ring .export_to_file ( tag::gmsh, "ring.msh" );
	std::cout << "produced file ring.msh" << std::endl;

}  // end of main
