
// example presented in paragraph 3.24 of the manual
// https://maniFEM.rd.ciencias.ulisboa.pt/manual-maniFEM.pdf
// two intersecting tori

#include "maniFEM.h"
#include "math.h"

using namespace maniFEM;


int main ()
	
{	Manifold RR3 ( tag::Euclid, tag::of_dim, 3 );
	Function xyz = RR3 .build_coordinate_system ( tag::Lagrange, tag::of_degree, 1 );
	Function x = xyz [0], y = xyz [1], z = xyz [2];
	std::cout << "this example takes time" << std::endl;

	// unlike in paragraph 3.7, here we don't need to cut f1 and f3 with 0.1,
	// the shape of each torus avoids the singularities
	// we change slightly f3 (replace 0.4 by 0.5) to highlight the intersection
	Function f1 = x*x + y*y;
	Function f2 = 1. - power ( f1, -0.5 );
	Function d1 = f1 * f2 * f2 + z*z;  // squared distance to a circle in the xy plane
	Function f3 = (x-0.5)*(x-0.5) + z*z;
	Function f4 = 1. - power ( f3, -0.5 );
	Function d2 = y*y + f3 * f4 * f4;  // squared distance to a circle in the xz plane

	// const double seg_size = 0.1;
	// Function seg_size = 0.03 + 0.05 * abs ( d2 - d1 );
	Function seg_size = 0.1 * power ( 0.15 + abs ( d2 - d1 ), 0.4 );

	Manifold intersection = RR3 .implicit ( d1 == 0.15, d2 == 0.15 );

	Cell start_1 ( tag::vertex, tag::of_coords, { 1.3, -0.4, 0. }, tag::project );
	Mesh loop_1 = Mesh::Build ( tag::frontal ) .entire_manifold()
		.start_at ( start_1 ) .towards ( { 0., 0., 1. } ) .desired_length ( seg_size );

	Cell start_2 ( tag::vertex, tag::of_coords, { -0.9, 0., 0.4 }, tag::project );
	Mesh loop_2 = Mesh::Build ( tag::frontal ) .entire_manifold()
		.start_at ( start_2 ) .towards ( { 0., 1., 0. } ) .desired_length ( seg_size );

	Mesh two_loops = Mesh::Build ( tag::join ) .meshes ( { loop_1, loop_2 } );

	RR3 .implicit ( d1 == 0.15 );
	Mesh torus_1 = Mesh::Build ( tag::frontal ) .boundary ( two_loops )
		.start_at ( start_1 ) .towards ( { -0.2, -1., 0. } ) .desired_length ( seg_size );

	RR3 .implicit ( d2 == 0.15 );
	Mesh torus_2 = Mesh::Build ( tag::frontal ) .boundary ( two_loops .reverse() )
		.start_at ( start_1 ) .towards ( { 1., -0.2, 0. } ) .desired_length ( seg_size );

	Mesh two_tori = Mesh::Build ( tag::join ) .meshes ( { torus_1, torus_2 } );

	two_tori .export_to_file ( tag::gmsh, "two-tori.msh" );
	std::cout << "produced file two-tori.msh" << std::endl;

}  // end of main



