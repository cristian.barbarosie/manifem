
// example presented in paragraph 7.25 of the manual
// https://maniFEM.rd.ciencias.ulisboa.pt/manual-maniFEM.pdf
// build an intricate mesh on a torus, having two V-shaped holes
// homogenized elastic tensor exhibits negative Poisson coefficient

#include "maniFEM.h"

using namespace maniFEM;
	

int main ( )

{	// begin with the usual two-dimensional space
	Manifold RR2 ( tag::Euclid, tag::of_dim, 2 );
	Function xy = RR2 .build_coordinate_system ( tag::Lagrange, tag::of_degree, 1 );
	Function x = xy[0], y = xy[1];

	const double e = 1.5;
	Manifold ellip_1 = RR2 .implicit 
		( 300.*power((x+y)*(x+y),e) + power((x-y-1.)*(x-y-1.),e) == 1. );
	Manifold ellip_2 = RR2 .implicit 
		( 300.*power((x-y)*(x-y),e) + power((x+y+1.)*(x+y+1.),e) == 1. );
	Manifold two_points_12 ( tag::intersect, ellip_1, ellip_2 );
	Cell S12 ( tag::vertex, tag::of_coords, { 0., -0.1 }, tag::project );
	Cell T12 ( tag::vertex, tag::of_coords, { 0.,  0.  }, tag::project );

	ellip_1 .set_as_working_manifold();
	Mesh ellipse_1 = Mesh::Build ( tag::frontal )
		.start_at ( S12 ) .stop_at ( T12 )
		.desired_length ( 0.05 ) .orientation ( tag::inherent );

	ellip_2 .set_as_working_manifold();
	Mesh ellipse_2 = Mesh::Build ( tag::frontal )
		.start_at ( T12 ) .stop_at ( S12 )
		.desired_length ( 0.05 ) .orientation ( tag::inherent );

	double a = 0.15, b = 0.82;
	Manifold ellip_3 = RR2 .implicit 
		( 300.*power((x-a+y+b)*(x-a+y+b),e) + power((x-a-y-b-1.)*(x-a-y-b-1.),e) == 1. );
	Manifold ellip_4 = RR2 .implicit 
		( 300.*power((x-2.-a-y-b)*(x-2.-a-y-b),e) + power((x-2.-a+y+b+1.)*(x-2.-a+y+b+1.),e) == 1. );
	Manifold two_points_34 ( tag::intersect, ellip_3, ellip_4 );
	Cell S34 ( tag::vertex, tag::of_coords, { 1.15, -1.7 }, tag::project );
	Cell T34 ( tag::vertex, tag::of_coords, { 1.15, -1.8 }, tag::project );

	ellip_3 .set_as_working_manifold();
	Mesh ellipse_3 = Mesh::Build ( tag::frontal )
		.start_at ( S34 ) .stop_at ( T34 )
		.desired_length ( 0.05 ) .orientation ( tag::inherent );

	ellip_4 .set_as_working_manifold();
	Mesh ellipse_4 = Mesh::Build ( tag::frontal )
		.start_at ( T34 ) .stop_at ( S34 )
		.desired_length ( 0.05 ) .orientation ( tag::inherent );

	// ellipse_1 :   0   < x < 1     -1   < y <  0
	// ellipse_2 :  -1   < x < 0     -1   < y <  0
	// ellipse_3 :   0.1 < x < 1.1   -1.8 < y < -0.8
	// ellipse_4 :   1.1 < x < 2.1   -1.8 < y < -0.8

	Mesh two_loops = Mesh::Build ( tag::join )
		.meshes ( { ellipse_1, ellipse_2, ellipse_3, ellipse_4 } );

	Manifold::Action gx ( tag::transforms, xy, tag::into, ( x + 2.3 ) && y );
	Manifold::Action gy ( tag::transforms, xy, tag::into, x && ( y + 1.4 ) ); 
	RR2 .quotient ( gx, gy );

	Mesh folded_loops = two_loops .fold ( tag::use_existing_vertices );
	// folded_loops .draw_ps ( "VV.eps", tag::unfold, tag::over_region, -1.5 < x < 2.5, -2. < y < 0.5 );

	Mesh perforated = Mesh::Build ( tag::frontal )
		.boundary ( folded_loops .reverse() )
		.desired_length ( 0.05 );
	std::cout << "produced folded mesh, now drawing, please wait" << std::endl << std::flush;
	
	perforated .export_to_file ( tag::eps, "perforated.eps", tag::unfold,
	                             tag::over_region, -1.5 < x < 2.5, -2. < y < 0.5 );
	std::cout << "produced file perforated.eps - please edit before viewing" << std::endl;
	
}  // end of  main

