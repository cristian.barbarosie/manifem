
// example presented in paragraph 3.26 of the manual
// https://maniFEM.rd.ciencias.ulisboa.pt/manual-maniFEM.pdf
// a disk with an eccentric hole, non-uniform meshing

#include "maniFEM.h"

using namespace maniFEM;


int main ( )

{	Manifold RR2 ( tag::Euclid, tag::of_dim, 2 );
	Function xy = RR2 .build_coordinate_system ( tag::Lagrange, tag::of_degree, 1 );
	Function x = xy[0], y = xy[1];

	Function d = 0.3 + 0.4 * ( ( x + 0.3 ) * ( x + 0.3 ) + ( y - 0.9 ) * ( y - 0.9 ) );
	// Function d = 0.3 * smooth_max ( x - 3.*y + 3.5, 1., tag::threshold, 0.01 );

	RR2 .set_metric ( tag::isotropic, 1./d );
	// implicit submanifolds 'circle' and 'ellipse' will inherit this metric

	Manifold circle = RR2 .implicit ( x*x + y*y == 1. );
	Mesh outer = Mesh::Build ( tag::frontal ) .entire_manifold() .desired_length ( 0.1 );

	double y0 = 0.37;
	Manifold ellipse = RR2 .implicit ( x*x + (y-y0)*(y-y0) + 0.3*x*y == 0.25 );
	Mesh inner = Mesh::Build ( tag::frontal ) .entire_manifold() .desired_length ( 0.1 );

	Mesh circles = Mesh::Build ( tag::join ) .meshes ( { outer, inner .reverse() } );

	RR2 .set_as_working_manifold();
	Mesh disk = Mesh::Build ( tag::frontal ) .boundary ( circles ) .desired_length ( 0.1 );

	disk .export_to_file ( tag::gmsh, "disk.msh" );
	disk .export_to_file ( tag::eps, "disk.eps" );
	std::cout << "produced files disk.msh and disk.eps" << std::endl;

}  // end of main
