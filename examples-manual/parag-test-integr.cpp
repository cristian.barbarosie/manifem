

#include "maniFEM.h"

using namespace maniFEM;

	
	
int main ()  // triangle

{	Manifold RR2 ( tag::Euclid, tag::of_dim, 2 );
	Function xy = RR2 .build_coordinate_system ( tag::Lagrange, tag::of_degree, 1 );
	Function x = xy [0], y = xy [1];

	Cell A ( tag::vertex );   x (A) = 0.;   y (A) = 0.;
	Cell B ( tag::vertex );   x (B) = 1.3;  y (B) = 0.07;
	Cell C ( tag::vertex );   x (C) = 1.13;  y (C) = 1.23;

	Cell AB ( tag::segment, A .reverse(), B );
	Cell BC ( tag::segment, B .reverse(), C );
	Cell CA ( tag::segment, C .reverse(), A );
	Cell ABC ( tag::triangle, AB, BC, CA );

	{ // just a block of code for hiding names
		
	// declare a finite element with Gauss quadrature on master
	FiniteElement fe_gauss ( tag::with_master, tag::triangle, tag::Lagrange, tag::of_degree, 1 );
	fe_gauss .set_integrator ( tag::Gauss, tag::tri_13 );
	#ifndef NDEBUG
	std::cout << fe_gauss .info();
	#endif

	fe_gauss .dock_on ( ABC );
	Function psi_A = fe_gauss .basis_function (A),
	         psi_B = fe_gauss .basis_function (B),
	         psi_C = fe_gauss .basis_function (C);
	std::cout << fe_gauss .integrate ( psi_C ) << " "
	          << fe_gauss .integrate ( psi_C * psi_A ) << " || ";
	std::cout << fe_gauss .integrate ( psi_C ) << " "
	          << fe_gauss .integrate ( psi_C * psi_B ) << " || ";
	std::cout << fe_gauss .integrate ( psi_C ) << " "
	          << fe_gauss .integrate ( psi_C * psi_C ) << std::endl;

	} { // just a block of code for hiding names
		
	// a second finite element with no master and hand-computed integrator
	FiniteElement fe_hand_coded ( tag::triangle, tag::Lagrange, tag::of_degree, 1 );
	Integrator integ_hand_coded = fe_hand_coded .set_integrator ( tag::hand_coded );

	// hand-coded integrators require an early declaration of
	// the integrals we intend to compute later (after docking on a cell)
	Function bf1 ( tag::basis_function, tag::within, fe_hand_coded ),
	         bf2 ( tag::basis_function, tag::within, fe_hand_coded );
	// fe_hand_coded .pre_compute
	// 	( tag::for_a_given, tag::basis_function, bf1, tag::integral_of, { bf1, bf1 .deriv(y) } );
	fe_hand_coded .pre_compute ( tag::for_given, tag::basis_functions, bf1, bf2,
										  tag::integral_of, { bf1, bf1 * bf2 } );
	#ifndef NDEBUG
	std::cout << fe_hand_coded .info();
	#endif

	fe_hand_coded .dock_on ( ABC );
	Function psi_A = fe_hand_coded .basis_function (A),
	         psi_B = fe_hand_coded .basis_function (B),
	         psi_C = fe_hand_coded .basis_function (C);
	std::vector < double > result = fe_hand_coded .integrate
		( tag::pre_computed, tag::replace, bf1, tag::by, psi_C, tag::replace, bf2, tag::by, psi_A );
	assert ( result .size() == 2 );
	std::cout << result [0] << " " << result [1] << " || ";
	result = fe_hand_coded .integrate
		( tag::pre_computed, tag::replace, bf1, tag::by, psi_C, tag::replace, bf2, tag::by, psi_B );
	assert ( result .size() == 2 );
	std::cout << result [0] << " " << result [1] << " || ";
	result = fe_hand_coded .integrate
		( tag::pre_computed, tag::replace, bf1, tag::by, psi_C, tag::replace, bf2, tag::by, psi_C );
	assert ( result .size() == 2 );
	std::cout << result [0] << " " << result [1] << std::endl;
	} // just a block of code for hiding names

	return 0;

}  // end of main triangle


int main_quadri ()  // quadrilateral

{	Manifold RR2 ( tag::Euclid, tag::of_dim, 2 );
	Function xy = RR2 .build_coordinate_system ( tag::Lagrange, tag::of_degree, 1 );
	Function x = xy [0], y = xy [1];

	Cell A ( tag::vertex );   x (A) = 0.;   y (A) = 0.;
	Cell B ( tag::vertex );   x (B) = 1.3;  y (B) = 0.;
	Cell C ( tag::vertex );   x (C) = 1.3;  y (C) = 1.23;
	Cell D ( tag::vertex );   x (D) = 0.;   y (D) = 1.23;

	Cell AB ( tag::segment, A .reverse(), B );
	Cell BC ( tag::segment, B .reverse(), C );
	Cell CD ( tag::segment, C .reverse(), D );
	Cell DA ( tag::segment, D .reverse(), A );
	Cell ABCD ( tag::quadrangle, AB, BC, CD, DA );

	{ // just a block of code for hiding names
		
	// declare a finite element with Gauss quadrature on master
	FiniteElement fe_gauss ( tag::with_master, tag::quadrangle, tag::Lagrange, tag::of_degree, 1 );
	fe_gauss .set_integrator ( tag::Gauss, tag::quad_9 );
	#ifndef NDEBUG
	std::cout << fe_gauss .info();
	#endif

	fe_gauss .dock_on ( ABCD );
	Function psi_A = fe_gauss .basis_function (A),
	         psi_B = fe_gauss .basis_function (B),
	         psi_C = fe_gauss .basis_function (C),
	         psi_D = fe_gauss .basis_function (D);
	std::cout << fe_gauss .integrate ( psi_A * psi_A ) << " "
	          << fe_gauss .integrate ( psi_A .deriv (x) * psi_A .deriv (x) +
	                                   psi_A .deriv (y) * psi_A .deriv (y) ) << " || ";
	std::cout << fe_gauss .integrate ( psi_A * psi_B ) << " "
	          << fe_gauss .integrate ( psi_A .deriv (x) * psi_B .deriv (x) +
	                                   psi_A .deriv (y) * psi_B .deriv (y) ) << " || ";
	std::cout << fe_gauss .integrate ( psi_A * psi_C ) << " "
	          << fe_gauss .integrate ( psi_A .deriv (x) * psi_C .deriv (x) +
	                                   psi_A .deriv (y) * psi_C .deriv (y) ) << " || ";
	std::cout << fe_gauss .integrate ( psi_A * psi_D ) << " "
	          << fe_gauss .integrate ( psi_A .deriv (x) * psi_D .deriv (x) +
	                                   psi_A .deriv (y) * psi_D .deriv (y) ) << std::endl;

	} { // just a block of code for hiding names
		
	// a second finite element with no master and hand-computed integrator
	FiniteElement fe_hand_coded ( tag::rectangle, tag::Lagrange, tag::of_degree, 1 );
	Integrator integ_hand_coded = fe_hand_coded .set_integrator ( tag::hand_coded );

	// hand-coded integrators require an early declaration of
	// the integrals we intend to compute later (after docking on a cell)
	Function bf1 ( tag::basis_function, tag::within, fe_hand_coded ),
	         bf2 ( tag::basis_function, tag::within, fe_hand_coded );
	// fe_hand_coded .pre_compute
	// 	( tag::for_a_given, tag::basis_function, bf1, tag::integral_of, { bf1, bf1 .deriv(y) } );
	fe_hand_coded .pre_compute ( tag::for_given, tag::basis_functions, bf1, bf2,
										  tag::integral_of, { bf1 .deriv (x) * bf2 .deriv (x) + bf1 .deriv (y) * bf2 .deriv (y), bf1 .deriv (x) } );
	#ifndef NDEBUG
	std::cout << fe_hand_coded .info();
	#endif

	fe_hand_coded .dock_on ( ABCD );
	Function psi_A = fe_hand_coded .basis_function (A),
	         psi_B = fe_hand_coded .basis_function (B),
	         psi_C = fe_hand_coded .basis_function (C),
	         psi_D = fe_hand_coded .basis_function (D);
	std::vector < double > result = fe_hand_coded .integrate
		( tag::pre_computed, tag::replace, bf1, tag::by, psi_A, tag::replace, bf2, tag::by, psi_A );
	assert ( result .size() == 2 );
	std::cout << result [0] << " " << result [1] << " || ";
	result = fe_hand_coded .integrate
		( tag::pre_computed, tag::replace, bf1, tag::by, psi_A, tag::replace, bf2, tag::by, psi_B );
	assert ( result .size() == 2 );
	std::cout << result [0] << " " << result [1] << " || ";
	result = fe_hand_coded .integrate
		( tag::pre_computed, tag::replace, bf1, tag::by, psi_A, tag::replace, bf2, tag::by, psi_C );
	assert ( result .size() == 2 );
	std::cout << result [0] << " " << result [1] << " || ";
	result = fe_hand_coded .integrate
		( tag::pre_computed, tag::replace, bf1, tag::by, psi_A, tag::replace, bf2, tag::by, psi_D );
	assert ( result .size() == 2 );
	std::cout << result [0] << " " << result [1] << std::endl;
	} // just a block of code for hiding names

	return 0;

}  // end of main quadrilateral


int main_tetra ()  // tetrahedron

{	Manifold RR3 ( tag::Euclid, tag::of_dim, 3 );
	Function xyz = RR3 .build_coordinate_system ( tag::Lagrange, tag::of_degree, 1 );
	Function x = xyz [0], y = xyz [1], z = xyz [2];

	Cell A ( tag::vertex );   x (A) = 0.;    y (A) = 0.;   z (A) = 0.;
	Cell B ( tag::vertex );   x (B) = 1.1;   y (B) = 0.11;   z (B) = 0.16;
	Cell C ( tag::vertex );   x (C) = 0.2;   y (C) = 1.24;   z (C) = -0.17;
	Cell D ( tag::vertex );   x (D) = 0.3;   y (D) = 0.39;   z (D) = 1.033;

	Cell AB ( tag::segment, A .reverse(), B );
	Cell BC ( tag::segment, B .reverse(), C );
	Cell CD ( tag::segment, C .reverse(), D );
	Cell DA ( tag::segment, D .reverse(), A );
	Cell AC ( tag::segment, A .reverse(), C );
	Cell BD ( tag::segment, B .reverse(), D );
	Cell ACB ( tag::triangle, AC, BC .reverse(), AB .reverse() );
	Cell CAD ( tag::triangle, AC .reverse(), DA .reverse(), CD .reverse() );
	Cell BCD ( tag::triangle, BC, CD, BD .reverse() );
	Cell ABD ( tag::triangle, AB, BD, DA );
	Cell ABCD ( tag::tetrahedron, ACB, CAD, BCD, ABD );

	{ // just a block of code for hiding names
	  
	// declare a finite element with Gauss quadrature on master
	FiniteElement fe_gauss ( tag::with_master, tag::tetrahedron, tag::Lagrange, tag::of_degree, 1 );
	fe_gauss .set_integrator ( tag::Gauss, tag::tetra_5 );
	#ifndef NDEBUG
	std::cout << fe_gauss .info();
	#endif

	fe_gauss .dock_on ( ABCD );
	Function psi_A = fe_gauss .basis_function (A),
	         psi_B = fe_gauss .basis_function (B),
	         psi_C = fe_gauss .basis_function (C),
	         psi_D = fe_gauss .basis_function (D);
	std::cout << fe_gauss .integrate ( psi_A * psi_D ) << " "
	          << fe_gauss .integrate ( psi_A .deriv (z) * psi_D .deriv (z) ) << " || ";
	std::cout << fe_gauss .integrate ( psi_B * psi_D ) << " "
	          << fe_gauss .integrate ( psi_B .deriv (z) * psi_D .deriv (z) ) << " || ";
	std::cout << fe_gauss .integrate ( psi_C * psi_D ) << " "
	          << fe_gauss .integrate ( psi_C .deriv (z) * psi_D .deriv (z) ) << " || ";
	std::cout << fe_gauss .integrate ( psi_D * psi_D ) << " "
	          << fe_gauss .integrate ( psi_D .deriv (z) * psi_D .deriv (z) ) << std::endl;

	} { // just a block of code for hiding names
	  
	// declare a second finite element with no master and hand-computed integrator
	FiniteElement fe_hand_coded ( tag::tetrahedron, tag::Lagrange, tag::of_degree, 1 );
	Integrator integ_hand_coded = fe_hand_coded .set_integrator ( tag::hand_coded );

	// hand-coded integrators require an early declaration of
	// the integrals we intend to compute later (after docking on a cell)
	Function bf1 ( tag::basis_function, tag::within, fe_hand_coded ),
	         bf2 ( tag::basis_function, tag::within, fe_hand_coded );
	// fe_hand_coded .pre_compute
	// 		( tag::for_a_given, tag::basis_function, bf1,
	//         tag::integral_of, { bf1, bf1 .deriv (x) } );
	fe_hand_coded .pre_compute ( tag::for_given, tag::basis_functions, bf1, bf2,
	  tag::integral_of, { bf1 * bf2, bf1 .deriv (z), bf1 .deriv (z) * bf2 .deriv (z) } );
	#ifndef NDEBUG
	std::cout << fe_hand_coded .info();
	#endif

	fe_hand_coded .dock_on ( ABCD );
	Function psi_A = fe_hand_coded .basis_function (A),
	         psi_B = fe_hand_coded .basis_function (B),
	         psi_C = fe_hand_coded .basis_function (C),
	         psi_D = fe_hand_coded .basis_function (D);
	std::vector < double > result = fe_hand_coded .integrate
		( tag::pre_computed, tag::replace, bf1, tag::by, psi_A, tag::replace, bf2, tag::by, psi_D );
	assert ( result .size() == 3 );
	std::cout << result [0] << " " << result [2] << " || ";
	result = fe_hand_coded .integrate
		( tag::pre_computed, tag::replace, bf1, tag::by, psi_B, tag::replace, bf2, tag::by, psi_D );
	assert ( result .size() == 3 );
	std::cout << result [0] << " " << result [2] << " || ";
	result = fe_hand_coded .integrate
		( tag::pre_computed, tag::replace, bf1, tag::by, psi_C, tag::replace, bf2, tag::by, psi_D );
	assert ( result .size() == 3 );
	std::cout << result [0] << " " << result [2] << " || ";
	result = fe_hand_coded .integrate
		( tag::pre_computed, tag::replace, bf1, tag::by, psi_D, tag::replace, bf2, tag::by, psi_D );
	assert ( result .size() == 3 );
	std::cout << result [0] << " " << result [2] << std::endl;
	
	} // just a block of code for hiding names

	return 0;

}  // end of main tetrahedron
