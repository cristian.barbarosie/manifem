
// example presented in paragraph 2.20 of the manual
// https://maniFEM.rd.ciencias.ulisboa.pt/manual-maniFEM.pdf
// build a spiral mesh (parametric)

#include "maniFEM.h"
#include "math.h"

using namespace maniFEM;


int main ()

{	Manifold RR ( tag::Euclid, tag::of_dim, 1 );
	Function t = RR .build_coordinate_system ( tag::Lagrange, tag::of_degree, 1 );
	const double pi = 4. * std::atan(1.);
	
	Cell A ( tag::vertex );  t(A) = pi/2.;
	Cell B ( tag::vertex );  t(B) = 5.*pi;
	Mesh arc_of_spiral = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( A ) .stop_at ( B ) .divided_in ( 50 );

	// forget about t, in future statements x and y will be used
	Function x = t * cos(t), y = t * sin(t);
	Manifold RR2 ( tag::Euclid, tag::of_dimension, 2 );
	RR2 .set_coordinates ( x && y );

	arc_of_spiral .export_to_file ( tag::eps, "spiral.eps" );
	arc_of_spiral .export_to_file ( tag::gmsh, "spiral.msh" );
	std::cout << "produced files spiral.eps and spiral.msh" << std::endl;

}  // end of main
