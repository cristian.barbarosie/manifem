
// example presented in paragraph 6.2 of the manual
// https://maniFEM.rd.ciencias.ulisboa.pt/manual-maniFEM.pdf
// integrates functions on a sphere

#include "maniFEM.h"

namespace maniFEM {


class Integrator::Gauss : public Integrator::Core

{	public :

	FiniteElement finite_element;
	
	std::vector < Cell > points;
	std::vector < double > weights;
	
	// constructor

	inline Gauss ( const tag::gauss_quadrature & q )
	: Integrator::Core (), finite_element ( tag::non_existent )
	{	switch ( q )
		{	case tag::tri_3 : std::cout << "tri 3" << std::endl;  break;
			case tag::tri_4 : std::cout << "tri 4" << std::endl;  break;
			case tag::tri_6 : std::cout << "tri 6" << std::endl;  break;
			default: std::cout << "default" << std::endl;                 }  }

	Gauss ( const tag::gauss_quadrature & q,
	        const tag::FromFiniteElementWithMaster &, FiniteElement & fe );

	double action ( Function f, const FiniteElement & fe );
	double action ( Function f );
	double action ( const Function & f, const tag::On &, const Cell & cll );
	double action ( const Function & f, const tag::On &, const Mesh & msh );
  // virtual from Integrator::Core
	
	//  pre_compute  and  retrieve_precomputed  are virtual from Integrator::Core,
	// here execution forbidden
	void pre_compute ( const std::vector < Function > & bf,
	                   const std::vector < Function > & res );
	std::vector < double > retrieve_precomputed ( const Function & bf, const Function & psi );
	std::vector < double > retrieve_precomputed
	( const Function & bf1, const Function & psi1,
	  const Function & bf2, const Function & psi2 );

	void dock_on ( const Cell & cll );  // virtual from Integrator::Core

};  // end of  class Integrator::Gauss

//------------------------------------------------------------------------------------------------------//

};  // end of  namespace maniFEM


using namespace maniFEM;


int main ()

{	Manifold RR3 ( tag::Euclid, tag::of_dim, 3 );
	Function xyz = RR3 .build_coordinate_system ( tag::Lagrange, tag::of_degree, 1 );
	Function x = xyz [0], y = xyz [1], z = xyz [2];

	RR3 .implicit ( x*x + y*y + z*z == 1. );
	Mesh sphere ( tag::frontal, tag::desired_length, 0.1 );

	sphere .export_to_file ( tag::gmsh, "sphere.msh" );
	
	Integrator integr ( tag::Gauss, tag::tri_6 );
	std::cout << integr .info();
	Integrator::Gauss * in = dynamic_cast < Integrator::Gauss * > ( integr .core );
	assert ( in );
	std::cout << in->finite_element .info();	

	std::cout << "integral of 1 = " << integr ( 1., tag::on, sphere ) << std::endl;
	std::cout << "integral of x = " << integr ( x, tag::on, sphere ) << std::endl;
	std::cout << "integral of x*x = " << integr ( x*x, tag::on, sphere ) << std::endl;
	std::cout << "integral of x*y = " << integr ( x*y, tag::on, sphere ) << std::endl;

}  // end of main
