
// example presented in paragraph 2.4 of the manual
// https://maniFEM.rd.ciencias.ulisboa.pt/manual-maniFEM.pdf
// a rectangle with non-uniformly distributed vertices, motivation for extruding meshes


#include "maniFEM.h"

using namespace maniFEM;


std::vector < double > uniform_sequence
( double t_min, double t_max, size_t div );

std::vector < double > geometric_sequence_symmetric
( double t_min, double t_max, size_t div, double ratio );

std::vector < double > geometric_sequence_increasing
( double t_min, double t_max, size_t div, double ratio );

std::vector < double > geometric_sequence_decreasing
( double t_min, double t_max, size_t div, double ratio );


int main ()

{	// we choose our (geometric) space dimension :
	Manifold RR2 ( tag::Euclid, tag::of_dim, 2 );
	Function xy = RR2 .build_coordinate_system ( tag::Lagrange, tag::of_degree, 1 );
	Function x = xy[0], y = xy[1];

	Cell A ( tag::vertex );  x(A) = 0.;  y(A) = 0.;
	Cell B ( tag::vertex );  x(B) = 3.;  y(B) = 0.;
	Cell C ( tag::vertex );  x(C) = 3.;  y(C) = 1.;
	Cell D ( tag::vertex );  x(D) = 0.;  y(D) = 1.;
	Mesh AB = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( A ) .stop_at ( B ) .divided_in ( 25 );
	Mesh BC = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( B ) .stop_at ( C ) .divided_in ( 15 );
	Mesh CD = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( C ) .stop_at ( D ) .divided_in ( 25 );
	Mesh DA = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( D ) .stop_at ( A ) .divided_in ( 15 );
	// the above segment meshes have their vertices uniformly distributed
	// below, we discard this uniform distribution by modifying coordinates of vertices

	double ratio = 1.1;
	std::vector < double > coords = geometric_sequence_increasing
			( x(A), x(B), AB .number_of ( tag::segments), ratio );
	Mesh::Iterator it = AB .iterator ( tag::over_vertices, tag::require_order );
	it .reset();
	for ( size_t i = 0; i < coords .size(); i ++ )
	{	assert ( it .in_range() );
		x ( *it ) = coords [i];
		it ++;                      }
	assert ( not it .in_range() );
	coords = geometric_sequence_increasing
			( y(B), y(C), BC .number_of ( tag::segments), ratio );
	it = BC .iterator ( tag::over_vertices, tag::require_order );
	it .reset();
	for ( size_t i = 0; i < coords .size(); i ++ )
	{	assert ( it .in_range() );
		y ( *it ) = coords [i];
		it ++;                      }
	assert ( not it .in_range() );
	coords = geometric_sequence_increasing
			( x(D), x(C), CD .number_of ( tag::segments), ratio );
	it = CD .iterator ( tag::over_vertices, tag::backwards );
	it .reset();
	for ( size_t i = 0; i < coords .size(); i ++ )
	{	assert ( it .in_range() );
		x ( *it ) = coords [i];
		it ++;                      }
	assert ( not it .in_range() );
	coords = geometric_sequence_increasing
			( y(A), y(D), DA .number_of ( tag::segments), ratio );
	it = DA .iterator ( tag::over_vertices, tag::backwards );
	it .reset();
	for ( size_t i = 0; i < coords .size(); i ++ )
	{	assert ( it .in_range() );
		y ( *it ) = coords [i];
		it ++;                      }
	assert ( not it .in_range() );

	Mesh ABCD = Mesh::Build ( tag::grid ) .shape ( tag::rectangle ) .faces ( AB, BC, CD, DA );

	ABCD .export_to_file ( tag::eps, "rectangle.eps" );
	ABCD .export_to_file ( tag::gmsh, "rectangle.msh" );
	std::cout << "produced files rectangle.eps and rectangle.msh" << std::endl;

}  // end of main



std::vector < double > uniform_sequence
( double t_min, double t_max, size_t div )

{	std::vector < double > result ( div+1 );
	double current_t = t_min;
	result [0] = current_t;
	double len_seg = (t_max-t_min) / div;
	for ( size_t i = 1; i <= div; i++ )
	{	current_t += len_seg;
		result [i] = current_t;  }
	return result;                                 }


std::vector < double > geometric_sequence_symmetric
( double t_min, double t_max, size_t div, double ratio )

{	std::vector < double > result ( div+1 );
	double current_t = 0.;
	result [0] = current_t;
	double len_seg = 1.;
	for ( size_t i = 1; i <= div; i++ )
	{	current_t += len_seg;
		if ( 2*i < div )  len_seg *= ratio;
		if ( 2*i > div )  len_seg /= ratio;
		result [i] = current_t;  }
	double rescale = (t_max-t_min) / current_t;
	for ( size_t i = 0; i <= div; i++ )
		result [i] = t_min + result [i] * rescale;
	return result;                                 }


std::vector < double > geometric_sequence_increasing
( double t_min, double t_max, size_t div, double ratio )

{	std::vector < double > result ( div+1 );
	double current_t = 0.;
	result [0] = current_t;
	double len_seg = 1.;
	for ( size_t i = 1; i <= div; i++ )
	{	current_t += len_seg;
		len_seg *= ratio;
		result [i] = current_t;  }
	double rescale = (t_max-t_min) / current_t;
	for ( size_t i = 0; i <= div; i++ )
		result [i] = t_min + result [i] * rescale;
	return result;                                 }


std::vector < double > geometric_sequence_decreasing
( double t_min, double t_max, size_t div, double ratio )

{	std::vector < double > result ( div+1 );
	double current_t = 0.;
	result [0] = current_t;
	double len_seg = 1.;
	for ( size_t i = 1; i <= div; i++ )
	{	current_t += len_seg;
		len_seg /= ratio;
		result [i] = current_t;  }
	double rescale = (t_max-t_min) / current_t;
	for ( size_t i = 0; i <= div; i++ )
		result [i] = t_min + result [i] * rescale;
	return result;                                 }
