
// example presented in paragraph 2.3 of the manual
// https://maniFEM.rd.ciencias.ulisboa.pt/manual-maniFEM.pdf
// a parallelipipedic box made of nine parts


#include "maniFEM.h"

using namespace maniFEM;


int main ()

{	Manifold RR3 ( tag::Euclid, tag::of_dim, 3 );
	Function xyz = RR3 .build_coordinate_system ( tag::Lagrange, tag::of_degree, 1 );
	Function x = xyz [0], y = xyz [1], z = xyz [2];

	// define points

	Cell A1 ( tag::vertex, tag::of_coords, { -1., -1., -0.1 } );
	Cell B1 ( tag::vertex, tag::of_coords, {  1., -1., -0.1 } );
	Cell C1 ( tag::vertex, tag::of_coords, {  1.,  1., -0.1 } );
	Cell D1 ( tag::vertex, tag::of_coords, { -1.,  1., -0.1 } );
	Cell E1 ( tag::vertex, tag::of_coords, { -1.,  0., -0.1 } );
	Cell F1 ( tag::vertex, tag::of_coords, {  0., -1., -0.1 } );
	Cell G1 ( tag::vertex, tag::of_coords, {  1.,  0., -0.1 } );
	Cell H1 ( tag::vertex, tag::of_coords, {  0.,  1., -0.1 } );
	Cell O1 ( tag::vertex, tag::of_coords, {  0.,  0., -0.1 } );
	Cell A2 ( tag::vertex, tag::of_coords, { -1., -1.,  0.  } );
	Cell B2 ( tag::vertex, tag::of_coords, {  1., -1.,  0.  } );
	Cell C2 ( tag::vertex, tag::of_coords, {  1.,  1.,  0.  } );
	Cell D2 ( tag::vertex, tag::of_coords, { -1.,  1.,  0.  } );
	Cell E2 ( tag::vertex, tag::of_coords, { -1.,  0.,  0.  } );
	Cell F2 ( tag::vertex, tag::of_coords, {  0., -1.,  0.  } );
	Cell G2 ( tag::vertex, tag::of_coords, {  1.,  0.,  0.  } );
	Cell H2 ( tag::vertex, tag::of_coords, {  0.,  1.,  0.  } );
	Cell O2 ( tag::vertex, tag::of_coords, {  0.,  0.,  0   } );
	Cell A3 ( tag::vertex, tag::of_coords, { -1., -1.,  0.5 } );
	Cell B3 ( tag::vertex, tag::of_coords, {  1., -1.,  0.5 } );
	Cell C3 ( tag::vertex, tag::of_coords, {  1.,  1.,  0.5 } );
	Cell D3 ( tag::vertex, tag::of_coords, { -1.,  1.,  0.5 } );
	Cell E3 ( tag::vertex, tag::of_coords, { -1.,  0.,  0.5 } );
	Cell F3 ( tag::vertex, tag::of_coords, {  0., -1.,  0.5 } );
	Cell G3 ( tag::vertex, tag::of_coords, {  1.,  0.,  0.5 } );
	Cell H3 ( tag::vertex, tag::of_coords, {  0.,  1.,  0.5 } );
	Cell O3 ( tag::vertex, tag::of_coords, {  0.,  0.,  0.5 } );
	Cell A4 ( tag::vertex, tag::of_coords, { -1., -1.,  0.8 } );
	Cell B4 ( tag::vertex, tag::of_coords, {  1., -1.,  0.8 } );
	Cell C4 ( tag::vertex, tag::of_coords, {  1.,  1.,  0.8 } );
	Cell D4 ( tag::vertex, tag::of_coords, { -1.,  1.,  0.8 } );

	// define segments (edges)

	Mesh A1F1 = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( A1 ) .stop_at ( F1 ) .divided_in ( 10 );
	Mesh F1B1 = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( F1 ) .stop_at ( B1 ) .divided_in ( 10 );
	Mesh B1G1 = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( B1 ) .stop_at ( G1 ) .divided_in ( 10 );
	Mesh G1C1 = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( G1 ) .stop_at ( C1 ) .divided_in ( 10 );
	Mesh C1H1 = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( C1 ) .stop_at ( H1 ) .divided_in ( 10 );
	Mesh H1D1 = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( H1 ) .stop_at ( D1 ) .divided_in ( 10 );
	Mesh D1E1 = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( D1 ) .stop_at ( E1 ) .divided_in ( 10 );
	Mesh E1A1 = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( E1 ) .stop_at ( A1 ) .divided_in ( 10 );
	Mesh E1O1 = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( E1 ) .stop_at ( O1 ) .divided_in ( 10 );
	Mesh F1O1 = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( F1 ) .stop_at ( O1 ) .divided_in ( 10 );
	Mesh G1O1 = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( G1 ) .stop_at ( O1 ) .divided_in ( 10 );
	Mesh H1O1 = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( H1 ) .stop_at ( O1 ) .divided_in ( 10 );
	Mesh A1A2 = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( A1 ) .stop_at ( A2 ) .divided_in ( 1 );
	Mesh F1F2 = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( F1 ) .stop_at ( F2 ) .divided_in ( 1 );
	Mesh B1B2 = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( B1 ) .stop_at ( B2 ) .divided_in ( 1 );
	Mesh G1G2 = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( G1 ) .stop_at ( G2 ) .divided_in ( 1 );
	Mesh C1C2 = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( C1 ) .stop_at ( C2 ) .divided_in ( 1 );
	Mesh H1H2 = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( H1 ) .stop_at ( H2 ) .divided_in ( 1 );
	Mesh D1D2 = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( D1 ) .stop_at ( D2 ) .divided_in ( 1 );
	Mesh E1E2 = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( E1 ) .stop_at ( E2 ) .divided_in ( 1 );
	Mesh O1O2 = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( O1 ) .stop_at ( O2 ) .divided_in ( 1 );

	Mesh A2F2 = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( A2 ) .stop_at ( F2 ) .divided_in ( 10 );
	Mesh F2B2 = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( F2 ) .stop_at ( B2 ) .divided_in ( 10 );
	Mesh B2G2 = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( B2 ) .stop_at ( G2 ) .divided_in ( 10 );
	Mesh G2C2 = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( G2 ) .stop_at ( C2 ) .divided_in ( 10 );
	Mesh C2H2 = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( C2 ) .stop_at ( H2 ) .divided_in ( 10 );
	Mesh H2D2 = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( H2 ) .stop_at ( D2 ) .divided_in ( 10 );
	Mesh D2E2 = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( D2 ) .stop_at ( E2 ) .divided_in ( 10 );
	Mesh E2A2 = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( E2 ) .stop_at ( A2 ) .divided_in ( 10 );
	Mesh E2O2 = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( E2 ) .stop_at ( O2 ) .divided_in ( 10 );
	Mesh F2O2 = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( F2 ) .stop_at ( O2 ) .divided_in ( 10 );
	Mesh G2O2 = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( G2 ) .stop_at ( O2 ) .divided_in ( 10 );
	Mesh H2O2 = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( H2 ) .stop_at ( O2 ) .divided_in ( 10 );

	Mesh A2A3 = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( A2 ) .stop_at ( A3 ) .divided_in ( 5 );
	Mesh F2F3 = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( F2 ) .stop_at ( F3 ) .divided_in ( 5 );
	Mesh B2B3 = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( B2 ) .stop_at ( B3 ) .divided_in ( 5 );
	Mesh G2G3 = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( G2 ) .stop_at ( G3 ) .divided_in ( 5 );
	Mesh C2C3 = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( C2 ) .stop_at ( C3 ) .divided_in ( 5 );
	Mesh H2H3 = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( H2 ) .stop_at ( H3 ) .divided_in ( 5 );
	Mesh D2D3 = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( D2 ) .stop_at ( D3 ) .divided_in ( 5 );
	Mesh E2E3 = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( E2 ) .stop_at ( E3 ) .divided_in ( 5 );
	Mesh O2O3 = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( O2 ) .stop_at ( O3 ) .divided_in ( 5 );

	Mesh A3F3 = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( A3 ) .stop_at ( F3 ) .divided_in ( 10 );
	Mesh F3B3 = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( F3 ) .stop_at ( B3 ) .divided_in ( 10 );
	Mesh B3G3 = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( B3 ) .stop_at ( G3 ) .divided_in ( 10 );
	Mesh G3C3 = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( G3 ) .stop_at ( C3 ) .divided_in ( 10 );
	Mesh C3H3 = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( C3 ) .stop_at ( H3 ) .divided_in ( 10 );
	Mesh H3D3 = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( H3 ) .stop_at ( D3 ) .divided_in ( 10 );
	Mesh D3E3 = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( D3 ) .stop_at ( E3 ) .divided_in ( 10 );
	Mesh E3A3 = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( E3 ) .stop_at ( A3 ) .divided_in ( 10 );
	Mesh E3O3 = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( E3 ) .stop_at ( O3 ) .divided_in ( 10 );
	Mesh F3O3 = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( F3 ) .stop_at ( O3 ) .divided_in ( 10 );
	Mesh G3O3 = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( G3 ) .stop_at ( O3 ) .divided_in ( 10 );
	Mesh H3O3 = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( H3 ) .stop_at ( O3 ) .divided_in ( 10 );

	Mesh A3B3 = Mesh::Build ( tag::join ) .meshes ( { A3F3, F3B3 } );
	Mesh B3C3 = Mesh::Build ( tag::join ) .meshes ( { B3G3, G3C3 } );
	Mesh C3D3 = Mesh::Build ( tag::join ) .meshes ( { C3H3, H3D3 } );
	Mesh D3A3 = Mesh::Build ( tag::join ) .meshes ( { D3E3, E3A3 } );

	Mesh A3A4 = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( A3 ) .stop_at ( A4 ) .divided_in ( 3 );
	Mesh B3B4 = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( B3 ) .stop_at ( B4 ) .divided_in ( 3 );
	Mesh C3C4 = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( C3 ) .stop_at ( C4 ) .divided_in ( 3 );
	Mesh D3D4 = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( D3 ) .stop_at ( D4 ) .divided_in ( 3 );
	Mesh A4B4 = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( A4 ) .stop_at ( B4 ) .divided_in ( 20 );
	Mesh B4C4 = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( B4 ) .stop_at ( C4 ) .divided_in ( 20 );
	Mesh C4D4 = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( C4 ) .stop_at ( D4 ) .divided_in ( 20 );
	Mesh D4A4 = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( D4 ) .stop_at ( A4 ) .divided_in ( 20 );

	// define rectangles (faces)

	Mesh E1A1F1O1 = Mesh::Build ( tag::grid ) .shape ( tag::rectangle )
		.faces ( E1A1, A1F1, F1O1, E1O1 .reverse() );
	Mesh F1B1G1O1 = Mesh::Build ( tag::grid ) .shape ( tag::rectangle )
		.faces ( F1B1, B1G1, G1O1, F1O1 .reverse() );
	Mesh G1C1H1O1 = Mesh::Build ( tag::grid ) .shape ( tag::rectangle )
		.faces ( G1C1, C1H1, H1O1, G1O1 .reverse() );
	Mesh H1D1E1O1 = Mesh::Build ( tag::grid ) .shape ( tag::rectangle )
		.faces ( H1D1, D1E1, E1O1, H1O1 .reverse() );

	Mesh A1F1F2A2 = Mesh::Build ( tag::grid ) .shape ( tag::rectangle )
		.faces ( A1F1, F1F2, A2F2 .reverse(), A1A2 .reverse() );
	Mesh F1B1B2F2 = Mesh::Build ( tag::grid ) .shape ( tag::rectangle )
		.faces ( F1B1, B1B2, F2B2 .reverse(), F1F2 .reverse() );
	Mesh B1G1G2B2 = Mesh::Build ( tag::grid ) .shape ( tag::rectangle )
		.faces ( B1G1, G1G2, B2G2 .reverse(), B1B2 .reverse() );
	Mesh G1C1C2G2 = Mesh::Build ( tag::grid ) .shape ( tag::rectangle )
		.faces ( G1C1, C1C2, G2C2 .reverse(), G1G2 .reverse() );
	Mesh C1H1H2C2 = Mesh::Build ( tag::grid ) .shape ( tag::rectangle )
		.faces ( C1H1, H1H2, C2H2 .reverse(), C1C2 .reverse() );
	Mesh H1D1D2H2 = Mesh::Build ( tag::grid ) .shape ( tag::rectangle )
		.faces ( H1D1, D1D2, H2D2 .reverse(), H1H2 .reverse() );
	Mesh D1E1E2D2 = Mesh::Build ( tag::grid ) .shape ( tag::rectangle )
		.faces ( D1E1, E1E2, D2E2 .reverse(), D1D2 .reverse() );
	Mesh E1A1A2E2 = Mesh::Build ( tag::grid ) .shape ( tag::rectangle )
		.faces ( E1A1, A1A2, E2A2 .reverse(), E1E2 .reverse() );
	Mesh E1O1O2E2 = Mesh::Build ( tag::grid ) .shape ( tag::rectangle )
		.faces ( E1O1, O1O2, E2O2 .reverse(), E1E2 .reverse() );
	Mesh F1O1O2F2 = Mesh::Build ( tag::grid ) .shape ( tag::rectangle )
		.faces ( F1O1, O1O2, F2O2 .reverse(), F1F2 .reverse() );
	Mesh G1O1O2G2 = Mesh::Build ( tag::grid ) .shape ( tag::rectangle )
		.faces ( G1O1, O1O2, G2O2 .reverse(), G1G2 .reverse() );
	Mesh H1O1O2H2 = Mesh::Build ( tag::grid ) .shape ( tag::rectangle )
		.faces ( H1O1, O1O2, H2O2 .reverse(), H1H2 .reverse() );

	Mesh E2A2F2O2 = Mesh::Build ( tag::grid ) .shape ( tag::rectangle )
		.faces ( E2A2, A2F2, F2O2, E2O2 .reverse() );
	Mesh F2B2G2O2 = Mesh::Build ( tag::grid ) .shape ( tag::rectangle )
		.faces ( F2B2, B2G2, G2O2, F2O2 .reverse() );
	Mesh G2C2H2O2 = Mesh::Build ( tag::grid ) .shape ( tag::rectangle )
		.faces ( G2C2, C2H2, H2O2, G2O2 .reverse() );
	Mesh H2D2E2O2 = Mesh::Build ( tag::grid ) .shape ( tag::rectangle )
		.faces ( H2D2, D2E2, E2O2, H2O2 .reverse() );

	Mesh A2F2F3A3 = Mesh::Build ( tag::grid ) .shape ( tag::rectangle )
		.faces ( A2F2, F2F3, A3F3 .reverse(), A2A3 .reverse() );
	Mesh F2B2B3F3 = Mesh::Build ( tag::grid ) .shape ( tag::rectangle )
		.faces ( F2B2, B2B3, F3B3 .reverse(), F2F3 .reverse() );
	Mesh B2G2G3B3 = Mesh::Build ( tag::grid ) .shape ( tag::rectangle )
		.faces ( B2G2, G2G3, B3G3 .reverse(), B2B3 .reverse() );
	Mesh G2C2C3G3 = Mesh::Build ( tag::grid ) .shape ( tag::rectangle )
		.faces ( G2C2, C2C3, G3C3 .reverse(), G2G3 .reverse() );
	Mesh C2H2H3C3 = Mesh::Build ( tag::grid ) .shape ( tag::rectangle )
		.faces ( C2H2, H2H3, C3H3 .reverse(), C2C3 .reverse() );
	Mesh H2D2D3H3 = Mesh::Build ( tag::grid ) .shape ( tag::rectangle )
		.faces ( H2D2, D2D3, H3D3 .reverse(), H2H3 .reverse() );
	Mesh D2E2E3D3 = Mesh::Build ( tag::grid ) .shape ( tag::rectangle )
		.faces ( D2E2, E2E3, D3E3 .reverse(), D2D3 .reverse() );
	Mesh E2A2A3E3 = Mesh::Build ( tag::grid ) .shape ( tag::rectangle )
		.faces ( E2A2, A2A3, E3A3 .reverse(), E2E3 .reverse() );
	Mesh E2O2O3E3 = Mesh::Build ( tag::grid ) .shape ( tag::rectangle )
		.faces ( E2O2, O2O3, E3O3 .reverse(), E2E3 .reverse() );
	Mesh F2O2O3F3 = Mesh::Build ( tag::grid ) .shape ( tag::rectangle )
		.faces ( F2O2, O2O3, F3O3 .reverse(), F2F3 .reverse() );
	Mesh G2O2O3G3 = Mesh::Build ( tag::grid ) .shape ( tag::rectangle )
		.faces ( G2O2, O2O3, G3O3 .reverse(), G2G3 .reverse() );
	Mesh H2O2O3H3 = Mesh::Build ( tag::grid ) .shape ( tag::rectangle )
		.faces ( H2O2, O2O3, H3O3 .reverse(), H2H3 .reverse() );

	Mesh E3A3F3O3 = Mesh::Build ( tag::grid ) .shape ( tag::rectangle )
		.faces ( E3A3, A3F3, F3O3, E3O3 .reverse() );
	Mesh F3B3G3O3 = Mesh::Build ( tag::grid ) .shape ( tag::rectangle )
		.faces ( F3B3, B3G3, G3O3, F3O3 .reverse() );
	Mesh G3C3H3O3 = Mesh::Build ( tag::grid ) .shape ( tag::rectangle )
		.faces ( G3C3, C3H3, H3O3, G3O3 .reverse() );
	Mesh H3D3E3O3 = Mesh::Build ( tag::grid ) .shape ( tag::rectangle )
		.faces ( H3D3, D3E3, E3O3, H3O3 .reverse() );

	Mesh A3B3C3D3 = Mesh::Build ( tag::join )
		.meshes ( { E3A3F3O3, F3B3G3O3, G3C3H3O3, H3D3E3O3 } );

	Mesh A3B3B4A4 = Mesh::Build ( tag::grid ) .shape ( tag::rectangle )
		.faces ( A3B3, B3B4, A4B4 .reverse(), A3A4 .reverse() );
	Mesh B3C3C4B4 = Mesh::Build ( tag::grid ) .shape ( tag::rectangle )
		.faces ( B3C3, C3C4, B4C4 .reverse(), B3B4 .reverse() );
	Mesh C3D3D4C4 = Mesh::Build ( tag::grid ) .shape ( tag::rectangle )
		.faces ( C3D3, D3D4, C4D4 .reverse(), C3C4 .reverse() );
	Mesh D3A3A4D4 = Mesh::Build ( tag::grid ) .shape ( tag::rectangle )
		.faces ( D3A3, A3A4, D4A4 .reverse(), D3D4 .reverse() );
	Mesh A4B4C4D4 = Mesh::Build ( tag::grid ) .shape ( tag::rectangle )
		.faces ( A4B4, B4C4, C4D4, D4A4 );

	// define nine parallelipipeds
	Mesh box_A12 = Mesh::Build ( tag::grid ) .shape ( tag::parallelipiped )
		.faces ( E1A1F1O1 .reverse(), E2A2F2O2, A1F1F2A2, E1O1O2E2 .reverse(), F1O1O2F2, E1A1A2E2 );
	Mesh box_B12 = Mesh::Build ( tag::grid ) .shape ( tag::parallelipiped )
		.faces ( F1B1G1O1 .reverse(), F2B2G2O2, B1G1G2B2, F1O1O2F2 .reverse(), G1O1O2G2, F1B1B2F2 );
	Mesh box_C12 = Mesh::Build ( tag::grid ) .shape ( tag::parallelipiped )
		.faces ( G1C1H1O1 .reverse(), G2C2H2O2, C1H1H2C2, G1O1O2G2 .reverse(), H1O1O2H2, G1C1C2G2 );
	Mesh box_D12 = Mesh::Build ( tag::grid ) .shape ( tag::parallelipiped )
		.faces ( H1D1E1O1 .reverse(), H2D2E2O2, D1E1E2D2, H1O1O2H2 .reverse(), E1O1O2E2, H1D1D2H2 );
	Mesh box_A23 = Mesh::Build ( tag::grid ) .shape ( tag::parallelipiped )
		.faces ( E2A2F2O2 .reverse(), E3A3F3O3, A2F2F3A3, E2O2O3E3 .reverse(), F2O2O3F3, E2A2A3E3 );
	Mesh box_B23 = Mesh::Build ( tag::grid ) .shape ( tag::parallelipiped )
		.faces ( F2B2G2O2 .reverse(), F3B3G3O3, B2G2G3B3, F2O2O3F3 .reverse(), G2O2O3G3, F2B2B3F3 );
	Mesh box_C23 = Mesh::Build ( tag::grid ) .shape ( tag::parallelipiped )
		.faces ( G2C2H2O2 .reverse(), G3C3H3O3, C2H2H3C3, G2O2O3G3 .reverse(), H2O2O3H3, G2C2C3G3 );
	Mesh box_D23 = Mesh::Build ( tag::grid ) .shape ( tag::parallelipiped )
		.faces ( H2D2E2O2 .reverse(), H3D3E3O3, D2E2E3D3, H2O2O3H3 .reverse(), E2O2O3E3, H2D2D3H3 );
	Mesh box_34 = Mesh::Build ( tag::grid ) .shape ( tag::parallelipiped )
		.faces ( A3B3C3D3 .reverse(), A4B4C4D4, D3A3A4D4, B3C3C4B4, A3B3B4A4, C3D3D4C4 );

	Mesh boxes = Mesh::Build ( tag::join )
		.meshes ( { box_A12, box_B12, box_C12, box_D12, box_A23,
		            box_B23, box_C23, box_D23, box_34           } );
	boxes .export_to_file ( tag::gmsh, "boxes.msh" );

	Mesh::Composite boxes_comp = Mesh::Build ( tag::gather )
		.mesh ( "box_A12", box_A12 )
		.mesh ( "box_B12", box_B12 )
		.mesh ( "box_C12", box_C12 )
		.mesh ( "box_D12", box_D12 )
		.mesh ( "box_A23", box_A23 )
		.mesh ( "box_B23", box_B23 )
		.mesh ( "box_C23", box_C23 )
		.mesh ( "box_D23", box_D23 )
		.mesh ( "box_34",  box_34  );
	boxes_comp .export_to_file ( tag::gmsh, "boxes-comp.msh" );

	std::cout << "produced files boxes.msh and boxes-comp.msh" << std::endl;

} // end of main
