
// example presented in paragraph 5.8 of the manual
// https://maniFEM.rd.ciencias.ulisboa.pt/manual-maniFEM.pdf

// build a 3d mesh for a ribbed channel as requested in
// https://www.reddit.com/r/CFD/comments/1fm25x8/structured_mesh_generation_help_ribbed_channel/

#include "maniFEM.h"

using namespace maniFEM;


std::vector < double > uniform_sequence
( double t_min, double t_max, size_t div );

std::vector < double > geometric_sequence_symmetric
( double t_min, double t_max, size_t div, double ratio );

std::vector < double > geometric_sequence_increasing
( double t_min, double t_max, size_t div, double ratio );

std::vector < double > geometric_sequence_decreasing
( double t_min, double t_max, size_t div, double ratio );

void redistribute_vertices
( Mesh & msh, const Function & coord_func, const std::vector < double > & coord_vec );
									 

int main ()

{	Manifold RR3 ( tag::Euclid, tag::of_dim, 3 );
	Function xyz = RR3 .build_coordinate_system ( tag::Lagrange, tag::of_degree, 1 );
	Function x = xyz [0], y = xyz [1], z = xyz [2];

	// geometric growth described by
	const double ratio = 1.12;

	// in the x coordinate, we want 4 full-height zones, with 3 spaces between them (ribs)
	// the 3 spaces are divided in uniform intervals
	const double channel_height = 0.7, width_space = 0.09;
	const size_t div_spaces = 4;
	// the first and last full zones are divided in intervals with a non-symmetric geometric growth
	// the two "inner" full zones are divided in intervals with a symmetric geometric growth
	const std::vector < double > width_full_zones { 0.4, 0.2, 0.2, 0.8 };
	const std::vector < size_t > div_full_zones { 10, 10, 10, 10 };
	assert ( width_full_zones .size() == div_full_zones .size() );
	// x_min = 0.
	// x_max = x_min + 3*width_space + sum_of width_full_zones
	
	// in the y coordinate (vertical), we only want two intervals
	// both are divided in intervals with a symmetric geometric growth
	std::vector < double > coords_y_lower =
		geometric_sequence_symmetric ( 0.,  width_space, 9,  ratio );
	std::vector < double > coords_y_upper =
		geometric_sequence_symmetric ( width_space, channel_height, 21, ratio );

	// start at (0,0,0), build one full-height box (first vertical strip)
	double x_min = 0., x_max = width_full_zones [0];

	// first, a small box at the bottom
	Cell A1 ( tag::vertex, tag::of_coords, { x_min, 0., 0. } );
	Cell B1 ( tag::vertex, tag::of_coords, { x_max, 0., 0. } );
	Cell B2 ( tag::vertex, tag::of_coords, { x_max, width_space, 0. } );
	Mesh B1B2 = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( B1 ) .stop_at ( B2 ) .divided_in ( coords_y_lower .size() - 1 );
	redistribute_vertices ( B1B2, y, coords_y_lower );
	Mesh::Composite B1B2_comp = Mesh::Build ( tag::gather )
		.cell ( "B1", B1 ) .mesh ( "B1B2", B1B2 ) .cell ( "B2", B2 );
	Mesh A1B1 = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( A1 ) .stop_at ( B1 ) .divided_in ( div_full_zones [0] );
	std::vector < double > coords_x = geometric_sequence_decreasing
		( x_min, x_max, div_full_zones [0], ratio );
	redistribute_vertices ( A1B1, x, coords_x );
	Mesh::Composite A1B1_comp = Mesh::Build ( tag::gather )
		.cell ( "A1", A1 ) .mesh ( "A1B1", A1B1 ) .cell ( "B1", B1 );
	Mesh::Composite A1B1B2A2_comp = Mesh::Build ( tag::extrude )
		.swatch ( B1B2_comp ) .slide_along ( A1B1_comp );
	Mesh A2B2 = A1B1B2A2_comp .retrieve_mesh ( "B2 times A1B1" );

	// now the upper box
	Cell B3 ( tag::vertex, tag::of_coords, { x_max, channel_height, 0. } );
	Mesh B2B3 = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( B2 ) .stop_at ( B3 ) .divided_in ( coords_y_upper .size() - 1 );
	redistribute_vertices ( B2B3, y, coords_y_upper );
	// here we do not need a composite mesh
	Mesh A2B2B3A3 = Mesh::Build ( tag::extrude ) .swatch ( B2B3 ) .slide_along ( A2B2 );

	// now the second vertical strip, only the upper part
	x_min = x_max;  x_max += width_space;
	Cell C1 ( tag::vertex, tag::of_coords, { x_max, 0., 0. } );
	Cell C2 ( tag::vertex, tag::of_coords, { x_max, width_space, 0. } );
	Mesh::Composite B2B3_comp = Mesh::Build ( tag::gather )
		.cell ( "B2", B2 ) .mesh ( "B2B3", B2B3 ) .cell ( "B3", B3 );
	Mesh B2C2 = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( B2 ) .stop_at ( C2 ) .divided_in ( div_spaces );
	// no need to redistribute vertices, distribution is uniform
	Mesh::Composite B2C2_comp = Mesh::Build ( tag::gather )
		.cell ( "B2", B2 ) .mesh ( "B2C2", B2C2 ) .cell ( "C2", C2 );
	Mesh::Composite B2C2C3B3_comp = Mesh::Build ( tag::extrude )
		.swatch ( B2B3_comp ) .slide_along ( B2C2_comp );
	Cell C3 = B2C2C3B3_comp .retrieve_cell ( "B3 times C2" );
	Mesh C2C3 = B2C2C3B3_comp .retrieve_mesh ( "B2B3 times C2" );

	// now the third vertical strip, full height
	x_min = x_max;  x_max += width_full_zones [1];

	// lower box
	Cell D1 ( tag::vertex, tag::of_coords, { x_max, 0., 0. } );
	Mesh C1C2 = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( C1 ) .stop_at ( C2 ) .divided_in ( coords_y_lower .size() - 1 );	
	redistribute_vertices ( C1C2, y, coords_y_lower );
	Mesh::Composite C1C2_comp = Mesh::Build ( tag::gather )
		.cell ( "C1", C1 ) .mesh ( "C1C2", C1C2 ) .cell ( "C2", C2 );
	Mesh C1D1 = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( C1 ) .stop_at ( D1 ) .divided_in ( div_full_zones [1] );
	coords_x = geometric_sequence_symmetric ( x_min, x_max, div_full_zones [1], ratio );
	redistribute_vertices ( C1D1, x, coords_x );
	Mesh::Composite C1D1_comp = Mesh::Build ( tag::gather )
		.cell ( "C1", C1 ) .mesh ( "C1D1", C1D1 ) .cell ( "D1", D1 );
	Mesh::Composite C1D1D2C2_comp = Mesh::Build ( tag::extrude )
		.swatch ( C1C2_comp ) .slide_along ( C1D1_comp );
	Cell D2 = C1D1D2C2_comp .retrieve_cell ( "C2 times D1" );
	Mesh C2D2 = C1D1D2C2_comp .retrieve_mesh ( "C2 times C1D1" );

	// upper box
	Mesh::Composite C2C3_comp = Mesh::Build ( tag::gather )
		.cell ( "C2", C2 ) .mesh ( "C2C3", C2C3 ) .cell ( "C3", C3 );
	Mesh::Composite C2D2_comp = Mesh::Build ( tag::gather )
		.cell ( "C2", C2 ) .mesh ( "C2D2", C2D2 ) .cell ( "D2", D2 );
	Mesh::Composite C2D2D3C3_comp = Mesh::Build ( tag::extrude )
		.swatch ( C2C3_comp ) .slide_along ( C2D2_comp );
	Mesh D2D3 = C2D2D3C3_comp .retrieve_mesh ( "C2C3 times D2" );
	Cell D3 = C2D2D3C3_comp .retrieve_cell ( "C3 times D2" );    
	
	// now the fourth vertical strip, only the upper part
	x_min = x_max;  x_max += width_space;
	Cell E1 ( tag::vertex, tag::of_coords, { x_max, 0., 0. } );
	Cell E2 ( tag::vertex, tag::of_coords, { x_max, width_space, 0. } );
	Mesh::Composite D2D3_comp = Mesh::Build ( tag::gather )
		.cell ( "D2", D2 ) .mesh ( "D2D3", D2D3 ) .cell ( "D3", D3 );
	Mesh D2E2 = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( D2 ) .stop_at ( E2 ) .divided_in ( div_spaces );
	// no need to redistribute vertices, distribution is uniform
	Mesh::Composite D2E2_comp = Mesh::Build ( tag::gather )
		.cell ( "D2", D2 ) .mesh ( "D2E2", D2E2 ) .cell ( "E2", E2 );
	Mesh::Composite D2E2E3D3_comp = Mesh::Build ( tag::extrude )
		.swatch ( D2D3_comp ) .slide_along ( D2E2_comp );
	Cell E3 = D2E2E3D3_comp .retrieve_cell ( "D3 times E2" );
	Mesh E2E3 = D2E2E3D3_comp .retrieve_mesh ( "D2D3 times E2" );

	// now the fifth vertical strip, full height
	x_min = x_max;  x_max += width_full_zones [2];

	// lower box
	Cell F1 ( tag::vertex, tag::of_coords, { x_max, 0., 0. } );
	Mesh E1E2 = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( E1 ) .stop_at ( E2 ) .divided_in ( coords_y_lower .size() - 1 );	
	redistribute_vertices ( E1E2, y, coords_y_lower );
	Mesh::Composite E1E2_comp = Mesh::Build ( tag::gather )
		.cell ( "E1", E1 ) .mesh ( "E1E2", E1E2 ) .cell ( "E2", E2 );
	Mesh E1F1 = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( E1 ) .stop_at ( F1 ) .divided_in ( div_full_zones [2] );
	coords_x = geometric_sequence_symmetric ( x_min, x_max, div_full_zones [2], ratio );
	redistribute_vertices ( E1F1, x, coords_x );
	Mesh::Composite E1F1_comp = Mesh::Build ( tag::gather )
		.cell ( "E1", E1 ) .mesh ( "E1F1", E1F1 ) .cell ( "F1", F1 );
	Mesh::Composite E1F1F2E2_comp = Mesh::Build ( tag::extrude )
		.swatch ( E1E2_comp ) .slide_along ( E1F1_comp );
	Cell F2 = E1F1F2E2_comp .retrieve_cell ( "E2 times F1" );
	Mesh E2F2 = E1F1F2E2_comp .retrieve_mesh ( "E2 times E1F1" );

	// upper box
	Mesh::Composite E2E3_comp = Mesh::Build ( tag::gather )
		.cell ( "E2", E2 ) .mesh ( "E2E3", E2E3 ) .cell ( "E3", E3 );
	Mesh::Composite E2F2_comp = Mesh::Build ( tag::gather )
		.cell ( "E2", E2 ) .mesh ( "E2F2", E2F2 ) .cell ( "F2", F2 );
	Mesh::Composite E2F2F3E3_comp = Mesh::Build ( tag::extrude )
		.swatch ( E2E3_comp ) .slide_along ( E2F2_comp );
	Mesh F2F3 = E2F2F3E3_comp .retrieve_mesh ( "E2E3 times F2" );
	Cell F3 = E2F2F3E3_comp .retrieve_cell ( "E3 times F2" );    
		
	// now the sixth vertical strip, only the upper part
	x_min = x_max;  x_max += width_space;
	Cell G1 ( tag::vertex, tag::of_coords, { x_max, 0., 0. } );
	Cell G2 ( tag::vertex, tag::of_coords, { x_max, width_space, 0. } );
	Mesh::Composite F2F3_comp = Mesh::Build ( tag::gather )
		.cell ( "F2", F2 ) .mesh ( "F2F3", F2F3 ) .cell ( "F3", F3 );
	Mesh F2G2 = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( F2 ) .stop_at ( G2 ) .divided_in ( div_spaces );
	// no need to redistribute vertices, distribution is uniform
	Mesh::Composite F2G2_comp = Mesh::Build ( tag::gather )
		.cell ( "F2", F2 ) .mesh ( "F2G2", F2G2 ) .cell ( "G2", G2 );
	Mesh::Composite F2G2G3F3_comp = Mesh::Build ( tag::extrude )
		.swatch ( F2F3_comp ) .slide_along ( F2G2_comp );
	Cell G3 = F2G2G3F3_comp .retrieve_cell ( "F3 times G2" );
	Mesh G2G3 = F2G2G3F3_comp .retrieve_mesh ( "F2F3 times G2" );

	// now the seventh vertical strip, full height
	x_min = x_max;  x_max += width_full_zones [3];

	// lower box
	Cell H1 ( tag::vertex, tag::of_coords, { x_max, 0., 0. } );
	Mesh G1G2 = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( G1 ) .stop_at ( G2 ) .divided_in ( coords_y_lower .size() - 1 );	
	redistribute_vertices ( G1G2, y, coords_y_lower );
	Mesh::Composite G1G2_comp = Mesh::Build ( tag::gather )
		.cell ( "G1", G1 ) .mesh ( "G1G2", G1G2 ) .cell ( "G2", G2 );
	Mesh G1H1 = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( G1 ) .stop_at ( H1 ) .divided_in ( div_full_zones [3] );
	coords_x = geometric_sequence_increasing ( x_min, x_max, div_full_zones [3], ratio );
	redistribute_vertices ( G1H1, x, coords_x );
	Mesh::Composite G1H1_comp = Mesh::Build ( tag::gather )
		.cell ( "G1", G1 ) .mesh ( "G1H1", G1H1 ) .cell ( "H1", H1 );
	Mesh::Composite G1H1H2G2_comp = Mesh::Build ( tag::extrude )
		.swatch ( G1G2_comp ) .slide_along ( G1H1_comp );
	Cell H2 = G1H1H2G2_comp .retrieve_cell ( "G2 times H1" );
	Mesh G2H2 = G1H1H2G2_comp .retrieve_mesh ( "G2 times G1H1" );

	// upper box
	Mesh::Composite G2G3_comp = Mesh::Build ( tag::gather )
		.cell ( "G2", G2 ) .mesh ( "G2G3", G2G3 ) .cell ( "G3", G3 );
	Mesh::Composite G2H2_comp = Mesh::Build ( tag::gather )
		.cell ( "G2", G2 ) .mesh ( "G2H2", G2H2 ) .cell ( "H2", H2 );
	Mesh::Composite G2H2H3G3_comp = Mesh::Build ( tag::extrude )
		.swatch ( G2G3_comp ) .slide_along ( G2H2_comp );
	Mesh H2H3 = G2H2H3G3_comp .retrieve_mesh ( "G2G3 times H2" );
	Cell H3 = G2H2H3G3_comp .retrieve_cell ( "G3 times H2" );    
	
	
	Mesh rectangles = Mesh::Build ( tag::join )
		.meshes ( { A2B2B3A3,
		            A1B1B2A2_comp .retrieve_mesh ( "B1B2 times A1B1" ),
		            B2C2C3B3_comp .retrieve_mesh ( "B2B3 times B2C2" ),
		            C1D1D2C2_comp .retrieve_mesh ( "C1C2 times C1D1" ),
		            C2D2D3C3_comp .retrieve_mesh ( "C2C3 times C2D2" ),
		            D2E2E3D3_comp .retrieve_mesh ( "D2D3 times D2E2" ),
		            E1F1F2E2_comp .retrieve_mesh ( "E1E2 times E1F1" ),
		            E2F2F3E3_comp .retrieve_mesh ( "E2E3 times E2F2" ),
		            F2G2G3F3_comp .retrieve_mesh ( "F2F3 times F2G2" ),
		            G1H1H2G2_comp .retrieve_mesh ( "G1G2 times G1H1" ),
		            G2H2H3G3_comp .retrieve_mesh ( "G2G3 times G2H2" ) } );
	rectangles .export_to_file ( tag::gmsh, "rectangle.msh" );

	// finally, extrude in the z direction to get a 3D mesh
	
	Cell A4 ( tag::vertex, tag::of_coords, { 0., 0., 0.5 } );
	Mesh A1A4 = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( A1 ) .stop_at ( A4 ) .divided_in ( 10 );
	// no need to redistribute vertices, distribution is uniform
	
	Mesh cube = Mesh::Build ( tag::extrude ) .swatch ( rectangles ) .slide_along ( A1A4 );
	cube .export_to_file ( tag::gmsh, "cube.msh" );

	std::cout << "produced files rectangle.msh and cube.msh" << std::endl;	
	
} // end of main


std::vector < double > uniform_sequence
( double t_min, double t_max, size_t div )

{	std::vector < double > result ( div+1 );
	double current_t = t_min;
	result [0] = current_t;
	double len_seg = (t_max-t_min) / div;
	for ( size_t i = 1; i <= div; i++ )
	{	current_t += len_seg;
		result [i] = current_t;  }
	return result;                                 }


std::vector < double > geometric_sequence_symmetric
( double t_min, double t_max, size_t div, double ratio )

{	std::vector < double > result ( div+1 );
	double current_t = 0.;
	result [0] = current_t;
	double len_seg = 1.;
	for ( size_t i = 1; i <= div; i++ )
	{	current_t += len_seg;
		if ( 2*i < div )  len_seg *= ratio;
		if ( 2*i > div )  len_seg /= ratio;
		result [i] = current_t;  }
	const double rescale = (t_max-t_min) / current_t;
	for ( size_t i = 0; i <= div; i++ )
		result [i] = t_min + result [i] * rescale;
	return result;                                     }


std::vector < double > geometric_sequence_increasing
( double t_min, double t_max, size_t div, double ratio )

{	std::vector < double > result ( div+1 );
	double current_t = 0.;
	result [0] = current_t;
	double len_seg = 1.;
	for ( size_t i = 1; i <= div; i++ )
	{	current_t += len_seg;
		len_seg *= ratio;
		result [i] = current_t;  }
	const double rescale = (t_max-t_min) / current_t;
	for ( size_t i = 0; i <= div; i++ )
		result [i] = t_min + result [i] * rescale;
	return result;                                     }


std::vector < double > geometric_sequence_decreasing
( double t_min, double t_max, size_t div, double ratio )

{	std::vector < double > result ( div+1 );
	double current_t = 0.;
	result [0] = current_t;
	double len_seg = 1.;
	for ( size_t i = 1; i <= div; i++ )
	{	current_t += len_seg;
		len_seg /= ratio;
		result [i] = current_t;  }
	const double rescale = (t_max-t_min) / current_t;
	for ( size_t i = 0; i <= div; i++ )
		result [i] = t_min + result [i] * rescale;
	return result;                                     }

void redistribute_vertices
( Mesh & msh, const Function & coord_func, const std::vector < double > & coord_vec )

{	Mesh::Iterator it = msh .iterator ( tag::over_vertices, tag::require_order );
	it .reset();
	for ( size_t i = 0; i < coord_vec .size(); i++ )
	{	assert ( it .in_range() );
		Cell V = *it;
		coord_func (V) = coord_vec [i];
		it ++;                           }
	assert ( not it .in_range() );                                                 }

