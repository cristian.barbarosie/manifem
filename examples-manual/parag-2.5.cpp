
// example presented in paragraph 2.5 of the manual
// https://maniFEM.rd.ciencias.ulisboa.pt/manual-maniFEM.pdf
// an L-shaped mesh with mixed triangles and rectangles

#include "maniFEM.h"

using namespace maniFEM;


int main ()

{	Manifold RR2 ( tag::Euclid, tag::of_dim, 2 );
	Function xy = RR2 .build_coordinate_system ( tag::Lagrange, tag::of_degree, 1 );
	Function x = xy[0], y = xy[1];

	Cell A ( tag::vertex );  x(A) = -1.;  y(A) = 0.;
	Cell B ( tag::vertex );  x(B) =  0.;  y(B) = 0.;
	Cell C ( tag::vertex );  x(C) =  0.;  y(C) = 0.5;
	Cell D ( tag::vertex );  x(D) = -1.;  y(D) = 0.5;
	Cell E ( tag::vertex );  x(E) =  0.;  y(E) = 1.;
	Cell F ( tag::vertex );  x(F) = -1.;  y(F) = 1.;
	Cell G ( tag::vertex );  x(G) =  1.;  y(G) = 0.;
	Cell H ( tag::vertex );  x(H) =  1.;  y(H) = 0.5;

	Mesh AB = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( A ) .stop_at ( B ) .divided_in ( 10 );
	Mesh BC = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( B ) .stop_at ( C ) .divided_in ( 8 );
	Mesh CD = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( C ) .stop_at ( D ) .divided_in ( 10 );
	Mesh DA = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( D ) .stop_at ( A ) .divided_in ( 8 );
	Mesh CE = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( C ) .stop_at ( E ) .divided_in ( 7 );
	Mesh EF = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( E ) .stop_at ( F ) .divided_in ( 10 );
	Mesh FD = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( F ) .stop_at ( D ) .divided_in ( 7 );
	Mesh BG = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( B ) .stop_at ( G ) .divided_in ( 12 );
	Mesh GH = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( G ) .stop_at ( H ) .divided_in ( 8 );
	Mesh HC = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( H ) .stop_at ( C ) .divided_in ( 12 );

	Mesh ABCD = Mesh::Build ( tag::grid ) .shape ( tag::rectangle )
		.faces ( AB, BC, CD, DA );
	Mesh CEFD = Mesh::Build ( tag::grid ) .shape ( tag::rectangle )
		.faces ( CE, EF, FD, CD .reverse() );
	Mesh BGHC = Mesh::Build ( tag::grid ) .shape ( tag::rectangle )
		.faces ( BG, GH, HC, BC .reverse() ) .with_triangles();
	
	Mesh L_shaped = Mesh::Build ( tag::join ) .meshes ( { ABCD, CEFD, BGHC } );

	L_shaped .export_to_file ( tag::eps, "L-shaped.eps" );
	L_shaped .export_to_file ( tag::gmsh, "L-shaped.msh" );
	std::cout << "produced files L-shaped.eps and L-shaped.msh" << std::endl;

}  // end of main
