
// example presented in paragraph 2.12 of the manual
// https://maniFEM.rd.ciencias.ulisboa.pt/manual-maniFEM.pdf
// mesh half of a disk

#include "maniFEM.h"

using namespace maniFEM;

int main ()

{	Manifold RR2 ( tag::Euclid, tag::of_dim, 2 );
	Function xy = RR2 .build_coordinate_system ( tag::Lagrange, tag::of_degree, 1 );
	Function x = xy[0], y = xy[1];

	Cell O ( tag::vertex );  x(O) =  0.;   y(O) = 0.;
	Cell A ( tag::vertex );  x(A) =  1.;   y(A) = 0.;
	Cell B ( tag::vertex );  x(B) =  0.5;  y(B) = 0.8;
	Cell C ( tag::vertex );  x(C) = -0.5;  y(C) = 0.8;
	Cell D ( tag::vertex );  x(D) = -1.;   y(D) = 0.;

	Manifold circle = RR2 .implicit ( x*x + y*y == 1. );
	circle .project (B);  circle .project (C);

	// three arcs of circle :
	Mesh AB = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( A ) .stop_at ( B ) .divided_in ( 10 );
	Mesh BC = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( B ) .stop_at ( C ) .divided_in ( 10 );
	Mesh CD = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( C ) .stop_at ( D ) .divided_in ( 10 );

	RR2 .set_as_working_manifold();

	Mesh OA = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( O ) .stop_at ( A ) .divided_in ( 10 );
	Mesh OB = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( O ) .stop_at ( B ) .divided_in ( 10 );
	Mesh OC = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( O ) .stop_at ( C ) .divided_in ( 10 );
	Mesh OD = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( O ) .stop_at ( D ) .divided_in ( 10 );

	Mesh OAB = Mesh::Build ( tag::grid ) .shape ( tag::triangle )
		.faces ( OA, AB, OB .reverse() );
	Mesh OBC = Mesh::Build ( tag::grid ) .shape ( tag::triangle )
		.faces ( OB, BC, OC .reverse() );
	Mesh OCD = Mesh::Build ( tag::grid ) .shape ( tag::triangle )
		.faces ( OC, CD, OD .reverse() );
	Mesh half_disk = Mesh::Build ( tag::join ) .meshes ( { OAB, OBC, OCD } );

	half_disk .export_to_file ( tag::gmsh, "half-disk.msh" );
	std::cout << "produced file half-disk.msh" << std::endl;
	
}  // end of main
