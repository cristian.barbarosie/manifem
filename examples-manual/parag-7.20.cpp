
// example presented in paragraph 7.20 of the manual
// https://maniFEM.rd.ciencias.ulisboa.pt/manual-maniFEM.pdf
// fold a square around a torus

#include "maniFEM.h"

using namespace maniFEM;


int main ( )

{	Manifold RR2 ( tag::Euclid, tag::of_dim, 2 );
	Function xy = RR2 .build_coordinate_system ( tag::Lagrange, tag::of_degree, 1 );
	Function x = xy[0], y = xy[1];

	size_t n = 20;
	double d = 2.6 / double(n);

	Cell A ( tag::vertex );  x(A) = -1.3;  y(A) = -1.3;
	Cell B ( tag::vertex );  x(B) =  1.3;  y(B) = -1.3;
	Cell C ( tag::vertex );  x(C) =  1.3;  y(C) =  1.3;
	Cell D ( tag::vertex );  x(D) = -1.3;  y(D) =  1.3;

	Mesh AB = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( A ) .stop_at ( B ) .divided_in ( n );
	Mesh BC = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( B ) .stop_at ( C ) .divided_in ( n );
	Mesh CD = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( C ) .stop_at ( D ) .divided_in ( n );
	Mesh DA = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( D ) .stop_at ( A ) .divided_in ( n );

	Manifold circle = RR2 .implicit ( x*x + y*y == 1. );
	Mesh inner = Mesh::Build ( tag::frontal ) .entire_manifold() .desired_length ( d );

	Mesh bdry = Mesh::Build ( tag::join ) .meshes ( { AB, BC, CD, DA, inner .reverse() } );

	RR2 .set_as_working_manifold();
	Mesh square = Mesh::Build ( tag::frontal ) .boundary ( bdry ) .desired_length ( d );

	Mesh torus = square .fold ( tag::identify, AB, tag::with, CD.reverse(),
	                            tag::identify, BC, tag::with, DA.reverse(),
	                            tag::use_existing_vertices                 );
	std::cout << "produced folded mesh, now drawing, please wait" << std::endl << std::flush;
	
	torus .export_to_file ( tag::eps, "torus.eps", tag::unfold,
                           tag::over_region, -2.1 < x < 4.3, -3.6 < y < 2.1 );
	std::cout << "produced file torus.eps - please edit before viewing" << std::endl;	

}  // end of main


