
// example presented in paragraph 5.6 of the manual
// https://maniFEM.rd.ciencias.ulisboa.pt/manual-maniFEM.pdf
// writing a composite mesh, sphere with three handles

#include "maniFEM.h"
#include "math.h"

using namespace maniFEM;


int main ()
	
{	Manifold RR3 ( tag::Euclid, tag::of_dim, 3 );
	Function xyz = RR3 .build_coordinate_system ( tag::Lagrange, tag::of_degree, 1 );
	Function x = xyz [0], y = xyz [1], z = xyz [2];

	double l = 0.12;
	
	std::cout << "this example takes time" << std::endl;
	Function dist_orig_sq = x*x + y*y + z*z;  // we cut with 0.1 to avoid singularities

	Function f1 = max ( (x-1.6)*(x-1.6) + z*z, 0.1 );
	Function g1 = 1. - power ( f1, -0.5 );
	Function d1 = y*y + f1 * g1 * g1;

	Function x2 = -0.5 * x + tag::Util::sqrt_three_quarters * z;
	Function z2 = -tag::Util::sqrt_three_quarters * x - 0.5 * z;
	Function f2 = max ( (x2-1.6)*(x2-1.6) + z2*z2, 0.1 );
	Function g2 = 1. - power ( f2, -0.5 );
	Function d2 = y*y + f2 * g2 * g2;

	Function x3 = -0.5 * x - tag::Util::sqrt_three_quarters * z;
	Function z3 =  tag::Util::sqrt_three_quarters * x - 0.5 * z;
	Function f3 = max ( (x3-1.6)*(x3-1.6) + z3*z3, 0.1 );
	Function g3 = 1. - power ( f3, -0.5 );
	Function d3 = y*y + f3 * g3 * g3;

	Manifold surface = RR3 .implicit
		( smooth_min ( 0.06 * dist_orig_sq, d1, d2, d3, tag::threshold, 0.25 ) == 0.1 );

	// we intersect with another sphere, obtain two loops
	surface .implicit ( 1.1*(x-1.7)*(x-1.7) + 2.*y*y + z*z == 1. );
	Cell A ( tag::vertex, tag::of_coords, { 2.6, 0.3, 0. }, tag::project );
	Mesh loop_A = Mesh::Build ( tag::frontal ) .entire_manifold()
		.start_at ( A ) .towards ( { 0., 0., 1. } ) .desired_length ( l );
	Cell B ( tag::vertex, tag::of_coords, { 2.6, -0.3, 0. }, tag::project );
	Mesh loop_B = Mesh::Build ( tag::frontal ) .entire_manifold()
		.start_at ( B ) .towards ( { 0., 0., 1. } ) .desired_length ( l );

	Mesh two_loops_AB = Mesh::Build ( tag::join ) .meshes ( { loop_A, loop_B .reverse() } );

	surface .set_as_working_manifold();
	Mesh piece_AB = Mesh::Build ( tag::frontal ) .boundary ( two_loops_AB )
		.start_at ( A ) .towards ( { -1., 0., 0. } ) .desired_length ( l );

	// we intersect with another sphere, obtain two loops
	surface .implicit ( 1.1*(x2-1.7)*(x2-1.7) + 2.*y*y + z2*z2 == 1. );
	Cell C ( tag::vertex, tag::of_coords, { -1.3, 0.3, 2.25 }, tag::project );
	Mesh loop_C = Mesh::Build ( tag::frontal ) .entire_manifold()
		.start_at ( C )  .towards ( { -1., 0., -1. } ) .desired_length ( l );
	Cell D ( tag::vertex, tag::of_coords, { -1.3, -0.3, 2.25 }, tag::project );
	Mesh loop_D = Mesh::Build ( tag::frontal ) .entire_manifold()
		.start_at ( D ) .towards ( { -1., 0., -1. } ) .desired_length ( l );

	Mesh two_loops_CD = Mesh::Build ( tag::join ) .meshes ( { loop_C, loop_D .reverse() } );

	surface .set_as_working_manifold();
	Mesh piece_CD = Mesh::Build ( tag::frontal ) .boundary ( two_loops_CD )
		.start_at ( C ) .towards ( { 1., 0., -1. } ) .desired_length ( l );

	// we intersect with another sphere, obtain two loops
	surface .implicit ( 1.1*(x3-1.7)*(x3-1.7) + 2.*y*y + z3*z3 == 1. );
	Cell E ( tag::vertex, tag::of_coords, { -1.3, 0.3, -2.25 }, tag::project );
	Mesh loop_E = Mesh::Build ( tag::frontal ) .entire_manifold()
		.start_at ( E ) .towards ( { 1., 0., -1. } ) .desired_length ( l );
	Cell F ( tag::vertex, tag::of_coords, { -1.3, -0.3, -2.25 }, tag::project );
	Mesh loop_F = Mesh::Build ( tag::frontal ) .entire_manifold()
		.start_at ( F ) .towards ( { 1., 0., -1. } ) .desired_length ( l );

	Mesh two_loops_EF = Mesh::Build ( tag::join ) .meshes ( { loop_E, loop_F .reverse() } );

	surface .set_as_working_manifold();
	Mesh piece_EF = Mesh::Build ( tag::frontal ) .boundary ( two_loops_EF )
		.start_at ( E ) .towards ( { 1., 0., 1. } ) .desired_length ( l );

	Mesh six_loops = Mesh::Build ( tag::join )
		.meshes ( { loop_A .reverse(), loop_B, loop_C .reverse(),
		            loop_D, loop_E .reverse(), loop_F            });

	Mesh piece = Mesh::Build ( tag::frontal ) .boundary ( six_loops )
		.start_at ( A ) .towards ( { 1., 0., 0. } ) .desired_length ( l );

	Mesh all = Mesh::Build ( tag::join ) .meshes ( { piece_AB, piece_CD, piece_EF, piece } );
	
	Mesh::Composite comp_msh = Mesh::Build ( tag::gather )
		.mesh ( "loop A", loop_A )
		.mesh ( "loop C", loop_C )
		.mesh ( "loop E", loop_E )
		.mesh ( "all", all );

	comp_msh .export_to_file ( tag::gmsh, "sphere-three-tori-comp.msh" );
	std::cout << "produced file sphere-three-tori-comp.msh" << std::endl;

}  // end of main

