
// example presented in paragraph 2.18 of the manual
// https://maniFEM.rd.ciencias.ulisboa.pt/manual-maniFEM.pdf
// a 3D star-like shape

#include "maniFEM.h"

using namespace maniFEM;


int main ()

{	Manifold RR3 ( tag::Euclid, tag::of_dim, 3 );
	Function xyz = RR3 .build_coordinate_system ( tag::Lagrange, tag::of_degree, 1 );
	Function x = xyz [0], y = xyz [1], z = xyz [2];

	Cell A ( tag::vertex );  x(A) = -1.;  y(A) = -1.;  z(A) = -1.;
	Cell B ( tag::vertex );  x(B) =  1.;  y(B) = -1.;  z(B) = -1.;
	Cell C ( tag::vertex );  x(C) =  1.;  y(C) = -1.;  z(C) =  1.;
	Cell D ( tag::vertex );  x(D) = -1.;  y(D) = -1.;  z(D) =  1.;
	Cell E ( tag::vertex );  x(E) = -1.;  y(E) =  1.;  z(E) = -1.;
	Cell F ( tag::vertex );  x(F) =  1.;  y(F) =  1.;  z(F) = -1.;
	Cell G ( tag::vertex );  x(G) =  1.;  y(G) =  1.;  z(G) =  1.;
	Cell H ( tag::vertex );  x(H) = -1.;  y(H) =  1.;  z(H) =  1.;

	const double p = 3.3;  // recommended values p > 3.
	const double q = (1.-p) / 4.;

	// one can define the same curve by different sets of equations

	// for instance, below we use the equation already introduced in paragraph 2.15
	// and add the equation of a plane
	
	RR3 .implicit ( x*x + q*(y+z)*(y+z) == 2.-p, y == z );

	// but this is equivalent to (prove it mathematically, check it out in the computer) :
	//      RR3 .implicit ( x*x + (1.-p)*y*y == 2.-p, y == z );
	// or : RR3 .implicit ( x*x + (1.-p)*z*z == 2.-p, y == z );
	// or : RR3 .implicit ( x*x + y*y - p*z*z == 2.-p, y == z );
	// or : RR3 .implicit ( x*x - p*y*y + z*z == 2.-p, y == z );
	
	Mesh AB = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( A ) .stop_at ( B ) .divided_in ( 15 );
	Mesh GH = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( G ) .stop_at ( H ) .divided_in ( 15 );
	
	RR3 .implicit ( x*x + q*(y-z)*(y-z) == 2.-p, y == -z );
	// or : RR3 .implicit ( x*x + (1.-p)*y*y == 2.-p, y == -z );
	// or : RR3 .implicit ( x*x + (1.-p)*z*z == 2.-p, y == -z );
	// or : RR3 .implicit ( x*x + y*y - p*z*z == 2.-p, y == -z );
	// or : RR3 .implicit ( x*x - p*y*y + z*z == 2.-p, y == -z );
	Mesh CD = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( C ) .stop_at ( D ) .divided_in ( 15 );
	Mesh EF = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( E ) .stop_at ( F ) .divided_in ( 15 );

	// below is still another set of equations defining the same kind of one-dimensional manifold
	
	RR3 .implicit ( x*x + y*y - p*z*z == 2.-p, - p*x*x + y*y + z*z == 2.-p );
	Mesh AE = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( A ) .stop_at ( E ) .divided_in ( 15 );
	Mesh BF = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( B ) .stop_at ( F ) .divided_in ( 15 );
	Mesh CG = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( C ) .stop_at ( G ) .divided_in ( 15 );
	Mesh DH = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( D ) .stop_at ( H ) .divided_in ( 15 );

	RR3 .implicit ( - p*x*x + y*y + z*z == 2.-p, x*x - p*y*y + z*z == 2.-p );
	Mesh BC = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( B ) .stop_at ( C ) .divided_in ( 15 );
	Mesh DA = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( D ) .stop_at ( A ) .divided_in ( 15 );
	Mesh FG = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( F ) .stop_at ( G ) .divided_in ( 15 );
	Mesh HE = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( H ) .stop_at ( E ) .divided_in ( 15 );

	// for surfaces, we do not have so much freedom :
	
	Manifold faces_x = RR3 .implicit ( - p*x*x + y*y + z*z == 2.-p );
	Mesh DHEA = Mesh::Build ( tag::grid ) .shape ( tag::quadrangle )
		.faces ( DH, HE, AE .reverse(), DA .reverse() );
	Mesh BFGC = Mesh::Build ( tag::grid ) .shape ( tag::quadrangle )
		.faces ( BF, FG, CG .reverse(), BC .reverse() );

	Manifold faces_y = RR3 .implicit ( x*x - p*y*y + z*z == 2.-p );
	Mesh ABCD = Mesh::Build ( tag::grid ) .shape ( tag::quadrangle )
		.faces ( AB, BC, CD, DA );
	Mesh EFGH = Mesh::Build ( tag::grid ) .shape ( tag::quadrangle )
		.faces ( EF, FG, GH, HE );

	Manifold faces_z = RR3 .implicit ( x*x + y*y - p*z*z == 2.-p );
	Mesh AEFB = Mesh::Build ( tag::grid ) .shape ( tag::quadrangle )
		.faces ( AE, EF, BF .reverse(), AB .reverse() );
	Mesh CGHD = Mesh::Build ( tag::grid ) .shape ( tag::quadrangle )
		.faces ( CG, GH, DH .reverse(), CD .reverse() );

	// by the way, another way of defining the manifolds used to build the sides is :
	// Manifold sides_x ( tag::intersect, faces_y, faces_z ) -- for building AB, GH, CD, EF
	// Manifold sides_y ( tag::intersect, faces_x, faces_z ) -- for building AE, BF, CG, DH
	// Manifold sides_z ( tag::intersect, faces_x, faces_y ) -- for building BC, DA, FG, HE
	
	// back to the initial 3D Euclidian space :

	RR3 .set_as_working_manifold();
	Mesh cube = Mesh::Build ( tag::grid ) .shape ( tag::cube )
		.faces ( ABCD, EFGH .reverse(), BFGC, DHEA, CGHD, AEFB );

	cube .export_to_file ( tag::gmsh, "cube.msh" );
	std::cout << "produced file cube.msh" << std::endl;

}  // end of main


