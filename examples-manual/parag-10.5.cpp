
// example presented in paragraph 10.5 of the manual
// https://maniFEM.rd.ciencias.ulisboa.pt/manual-maniFEM.pdf
// an evolving interface (shape optimization)

#include "maniFEM.h"
#include <fstream>
#include <Eigen/SparseCore>
#include <Eigen/IterativeLinearSolvers>

using namespace maniFEM;


void impose_value_of_unknown  // fast  // see below for an equivalent, more readable, version
( Eigen::SparseMatrix < double > & matrix_A, Eigen::VectorXd & vector_b,
  const size_t i, const double val                                      )

// in a system of linear equations, destroy equation 'i' and impose u(i) = val
// change also column 'i' of the matrix, just to preserve symmetry

// used for imposing Dirichlet boundary conditions

{	assert ( ! matrix_A .IsRowMajor );
	const size_t size_matrix = matrix_A .innerSize();

	// destroy row i, even the diagonal entry, change vector_b
	for ( size_t j = 0; j < size_matrix; j++ )
	{	if ( matrix_A .coeff ( i, j ) != 0. )
			matrix_A .coeffRef ( i, j ) = 0.;
		vector_b (j) -= matrix_A .coeff ( j, i ) * val;  }

	// destroy column i, set diagonal entry to 1.
	const size_t oi = matrix_A .outerIndexPtr() [i];     // access to matrix_A .m_outerIndex
	// std::cout << i << " " << oi << " " << matrix_A .innerNonZeroPtr() [i] << " "
	//           << matrix_A .innerIndexPtr() [ oi ] << " " << matrix_A .valuePtr() [ oi ] << std::endl;
	matrix_A .innerNonZeroPtr() [i] = 1;                 // access to matrix_A .m_innerNonZeros
	matrix_A .innerIndexPtr() [ oi ] = i;                // access to matrix_A .m_data .m_indices
	matrix_A .valuePtr() [ oi ] = 1.;                    // access to matrix_A .m_data .m_values

	// change vector
	vector_b (i) = val;                                     }


void impose_value_of_unknown_readable
( Eigen::SparseMatrix < double > & matrix_A, Eigen::VectorXd & vector_b,
  const size_t i, const double val                                      )

// in a system of linear equations, destroy equation 'i' and impose u(i) = val
// change also column 'i' of the matrix, just to preserve symmetry

// used for imposing Dirichlet boundary conditions

{	assert ( ! matrix_A .IsRowMajor );
	const size_t size_matrix = matrix_A .innerSize();

	// destroy raw i, even the diagonal entry
	for ( size_t j = 0; j < size_matrix; j++ )
		if ( matrix_A .coeff ( i, j ) != 0. )
			matrix_A .coeffRef ( i, j ) = 0.;

	// destroy column i, change vector
	for ( size_t j = 0; j < size_matrix; j++ )
	{	if ( matrix_A .coeff ( j, i ) == 0. )  continue;
		double & Aji = matrix_A .coeffRef ( j, i );
		vector_b (j) -= Aji * val;
		Aji = 0.;                                         }

	// set diagonal entry to 1., change vector
	matrix_A .coeffRef ( i, i ) = 1.;
	vector_b (i) = val;                                     }

//------------------------------------------------------------------------------------------------------//


inline void compute_integral_of_grad_on_cell_in_2D
( const Eigen::VectorXd & temperature, const Cell & cll, FiniteElement & fe,
  Cell::Numbering & numbering, double & dtemp_dx_on_cll, double & dtemp_dy_on_cll )
// 'fe' is already docked on 'cll'
// base functions associated to vertices only
// return values: dtemp_dx_on_cll, dtemp_dy_on_cll

{	Manifold RR2 = Manifold::working;
	Function xy = RR2 .coordinates();
	assert ( xy .number_of ( tag::components ) == 2 );
	Function x = xy[0], y = xy[1];
	dtemp_dx_on_cll = 0.;  dtemp_dy_on_cll = 0.;
	// run over the three vertices of 'cll'
	Mesh::Iterator it = cll .boundary() .iterator ( tag::over_vertices );
	for ( it .reset(); it .in_range(); it++ )
	{	Cell V = *it;
		Function psiV = fe .basis_function (V),
		         d_psiV_dx = psiV .deriv (x),
		         d_psiV_dy = psiV .deriv (y);
		// 'fe' is already docked on 'cll' so this will be the domain of integration
		size_t i = numbering (V);
		dtemp_dx_on_cll += fe .integrate ( temperature [i] * d_psiV_dx );
		dtemp_dy_on_cll += fe .integrate ( temperature [i] * d_psiV_dy );  }  }

//------------------------------------------------------------------------------------------------------//


int main ()

{	Manifold RR2 ( tag::Euclid, tag::of_dim, 2 );
   Function xy = RR2 .build_coordinate_system ( tag::Lagrange, tag::of_degree, 1 );
	Function x = xy[0], y = xy[1];
	
	Cell A ( tag::vertex );  x(A) = -2.;  y(A) = -2.;
	Cell B ( tag::vertex );  x(B) =  2.;  y(B) = -2.;
	Cell C ( tag::vertex );  x(C) =  2.;  y(C) =  2.;
	Cell D ( tag::vertex );  x(D) = -2.;  y(D) =  2.;
	Mesh AB = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( A ) .stop_at ( B ) .divided_in ( 20 );
	Mesh BC = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( B ) .stop_at ( C ) .divided_in ( 20 );
	Mesh CD = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( C ) .stop_at ( D ) .divided_in ( 20 );
	Mesh DA = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( D ) .stop_at ( A ) .divided_in ( 20 );
	
	// define a circle of radius 1 :
	Manifold circle_manif = RR2 .implicit ( x*x + y*y == 1. );
	Mesh circle = Mesh::Build ( tag::frontal ) .entire_manifold() .desired_length ( 0.2 );
	
	// we now leave the ’circle_manif’, go back to RR2 :
	RR2 .set_as_working_manifold();
	// mesh the region bounded by ’circle’ :
	Mesh disk = Mesh::Build ( tag::frontal ) .boundary ( circle ) .desired_length ( 0.2 );
	// mesh the region bounded (outside) by ’ABCD’ and (inside) by ’circle’ :
	Mesh exterior_boundary = Mesh::Build ( tag::join ) .meshes ( { AB, BC, CD, DA } );
	Mesh boundaries = Mesh::Build ( tag::join ) .meshes ( { exterior_boundary, circle .reverse() } );
	Mesh square_with_round_hole = Mesh::Build ( tag::frontal )
		.boundary ( boundaries ) .desired_length ( 0.2 );
	// simply join the two regions :
	Mesh entire_square = Mesh::Build ( tag::join ) .meshes ( { square_with_round_hole, disk } );

	FiniteElement fe ( tag::with_master, tag::triangle, tag::Lagrange, tag::of_degree, 1 );
	Integrator integ = fe .set_integrator ( tag::Gauss, tag::tri_3 );

	for ( size_t iter = 0; iter < 5; iter ++ )
	{

 	Cell::Numbering numbering ( tag::map );
	{ // just a block of code for hiding 'it' and 'counter'
	Mesh::Iterator it = entire_square .iterator ( tag::over_vertices );
	size_t counter = 0;
	for ( it .reset(); it .in_range(); it++ )
	{	Cell V = *it;  numbering (V) = counter;  ++counter;  }
	assert ( counter == numbering .size() );
	} // just a block of code

	const size_t size_matrix = entire_square .number_of ( tag::vertices );
	assert ( size_matrix == numbering .size() );
	std::cout << "global matrix " << size_matrix << "x" << size_matrix << std::endl;
	Eigen::SparseMatrix < double > matrix_A ( size_matrix, size_matrix );
	Eigen::VectorXd vector_b ( size_matrix );
	vector_b .setZero();

	const double cond_bulk = 1., cond_incl = 0.12;
	
	// run over all triangular cells composing square_with_round_hole
	{ // just a block of code for hiding 'it'
	Mesh::Iterator it = square_with_round_hole .iterator ( tag::over_cells_of_max_dim );
	for ( it .reset(); it .in_range(); it++ )
	{	Cell small_triangle = *it;
		fe .dock_on ( small_triangle );
		// run twice over the three vertices of 'small_triangle'
		Mesh::Iterator it1 = small_triangle .boundary() .iterator ( tag::over_vertices );
		Mesh::Iterator it2 = small_triangle .boundary() .iterator ( tag::over_vertices );
		for ( it1 .reset(); it1 .in_range(); it1++ )
		for ( it2 .reset(); it2 .in_range(); it2++ )
		{	Cell V = *it1, W = *it2;  // V may be the same as W, no problem about that
			Function psiV = fe .basis_function (V),
			         psiW = fe .basis_function (W),
			         d_psiV_dx = psiV .deriv (x),
			         d_psiV_dy = psiV .deriv (y),
			         d_psiW_dx = psiW .deriv (x),
			         d_psiW_dy = psiW .deriv (y);
			// 'fe' is already docked on 'small_triangle' so this will be the domain of integration
			matrix_A .coeffRef ( numbering (V), numbering (W) ) += cond_bulk *
				fe .integrate ( d_psiV_dx * d_psiW_dx + d_psiV_dy * d_psiW_dy );  }  }
	} // just a block of code 

	// run over all triangular cells composing disk
	{ // just a block of code for hiding 'it'
	Mesh::Iterator it = disk .iterator ( tag::over_cells_of_max_dim );
	for ( it .reset(); it .in_range(); it++ )
	{	Cell small_triangle = *it;
		fe .dock_on ( small_triangle );
		// run twice over the three vertices of 'small_triangle'
		Mesh::Iterator it1 = small_triangle .boundary() .iterator ( tag::over_vertices );
		Mesh::Iterator it2 = small_triangle .boundary() .iterator ( tag::over_vertices );
		for ( it1 .reset(); it1 .in_range(); it1++ )
		for ( it2 .reset(); it2 .in_range(); it2++ )
		{	Cell V = *it1, W = *it2;  // V may be the same as W, no problem about that
			Function psiV = fe .basis_function (V),
			         psiW = fe .basis_function (W),
			         d_psiV_dx = psiV .deriv (x),
			         d_psiV_dy = psiV .deriv (y),
			         d_psiW_dx = psiW .deriv (x),
			         d_psiW_dy = psiW .deriv (y);
			// 'fe' is already docked on 'small_triangle' so this will be the domain of integration
			matrix_A .coeffRef ( numbering (V), numbering (W) ) += cond_incl *
				fe .integrate ( d_psiV_dx * d_psiW_dx + d_psiV_dy * d_psiW_dy );  }  }
	} // just a block of code 

	{ // just a block of code for hiding 'it'
	Mesh::Iterator it = exterior_boundary .iterator ( tag::over_vertices );
	for ( it .reset(); it .in_range(); it++ )
	{	Cell P = *it;
		size_t i = numbering (P);
		impose_value_of_unknown ( matrix_A, vector_b, i, x(P) + 2. * y(P) );  }
	} // just a block of code 
	
	// solve the system of linear equations
	Eigen::ConjugateGradient < Eigen::SparseMatrix < double >,
	                           Eigen::Lower | Eigen::Upper    > cg;
	cg .compute ( matrix_A );

	Eigen::VectorXd temperature = cg .solve ( vector_b );
	if ( cg .info() != Eigen::Success )
	{	std::cout << "Eigen solver.solve failed" << std::endl;
		exit (1);                                               }
		
	// we now deform the 'circle' according to the shape derivative
	const double eta = 0.02, Lagr = 5.;
	
	{ // just a block of code for hiding 'it'
	Mesh::Iterator it = circle .iterator ( tag::over_vertices );
	for ( it .reset(); it .in_range(); it++ )
	{	Cell Q = *it;
		// compute the average value of grad temp on one side of 'circle'
		// (in 'disk') (in the inclusion)
		double avrg_dtemp_dx_incl = 0., avrg_dtemp_dy_incl = 0.;
		double area = 0.;
		Mesh::Iterator it_incl = disk .iterator ( tag::over_cells, tag::of_max_dim, tag::around, Q );
		for ( it_incl .reset(); it_incl .in_range(); it_incl ++ )
		{	Cell tri = * it_incl;
			fe .dock_on ( tri );
			double dtemp_dx_on_tri, dtemp_dy_on_tri;
			compute_integral_of_grad_on_cell_in_2D
				( temperature, tri, fe, numbering, dtemp_dx_on_tri, dtemp_dy_on_tri );
			// 'fe' is already docked on 'tri' so this will be the domain of integration
			double area_tri = fe .integrate ( 1. );
			area += area_tri;
			avrg_dtemp_dx_incl += dtemp_dx_on_tri;
			avrg_dtemp_dy_incl += dtemp_dy_on_tri;                                     }
		avrg_dtemp_dx_incl /= area;
		avrg_dtemp_dy_incl /= area;
		// compute the average value of grad temp on the other side of 'circle'
		// (in 'square_with_round_hole') (in the bulk)
		double avrg_dtemp_dx_bulk = 0., avrg_dtemp_dy_bulk = 0.;
		area = 0.;
		Mesh::Iterator it_bulk =
		  square_with_round_hole .iterator ( tag::over_cells, tag::of_max_dim, tag::around, Q );
		for ( it_bulk .reset(); it_bulk .in_range(); it_bulk ++ )
		{	Cell tri = * it_bulk;
			fe .dock_on ( tri );
			double dtemp_dx_on_tri, dtemp_dy_on_tri;
			compute_integral_of_grad_on_cell_in_2D
				( temperature, tri, fe, numbering, dtemp_dx_on_tri, dtemp_dy_on_tri );
			// 'fe' is already docked on 'tri' so this will be the domain of integration
			double area_tri = fe .integrate ( 1. );
			area += area_tri;
			avrg_dtemp_dx_bulk += dtemp_dx_on_tri;
			avrg_dtemp_dy_bulk += dtemp_dy_on_tri;                                       }
		avrg_dtemp_dx_bulk /= area;
		avrg_dtemp_dy_bulk /= area;
		// compute normal vector, pointing inside 'disk'
		Cell PQ = circle .cell_behind (Q);
		Cell QR = circle .cell_in_front_of (Q);
		Cell P = PQ .base() .reverse();
		Cell R = QR .tip();
		double n_x = y(P) - y(R), n_y = x(R) - x(P);
		double norm = std::sqrt ( n_x * n_x + n_y * n_y );
		n_x /= norm;  n_y /= norm;
		const double jump_of_energy =
			  cond_bulk * ( avrg_dtemp_dx_bulk * avrg_dtemp_dx_bulk +
			                avrg_dtemp_dy_bulk * avrg_dtemp_dy_bulk   ) // -
			- cond_incl * ( avrg_dtemp_dx_incl * avrg_dtemp_dx_incl +
			                avrg_dtemp_dy_incl * avrg_dtemp_dy_incl   );
		const double twice_normal_heat_flux =
			  ( cond_bulk * avrg_dtemp_dx_bulk + cond_incl * avrg_dtemp_dx_incl ) * n_x // +
			+ ( cond_bulk * avrg_dtemp_dy_bulk + cond_incl * avrg_dtemp_dy_incl ) * n_y;
		// quantities above are, in theory, the same on either side of the interface
		// however, to attend to numerical errors, we use the average
		double jump_of_dtemp_dx = avrg_dtemp_dx_bulk - avrg_dtemp_dx_incl,
		       jump_of_dtemp_dy = avrg_dtemp_dy_bulk - avrg_dtemp_dy_incl;
		// the pair above is, in theory, normal to the interface
		// however, to attend to numerical errors, we multiply by the normal :
		const double jump_of_grad_temp_times_n = jump_of_dtemp_dx * n_x + jump_of_dtemp_dy * n_y;
		jump_of_dtemp_dx = jump_of_grad_temp_times_n * n_x;
		jump_of_dtemp_dy = jump_of_grad_temp_times_n * n_y;
		const double F_x = jump_of_energy * n_x - twice_normal_heat_flux * jump_of_dtemp_dx - Lagr * n_x,
		             F_y = jump_of_energy * n_y - twice_normal_heat_flux * jump_of_dtemp_dy - Lagr * n_y;
		// size_t i = numbering (Q);
		// impose_value_of_unknown ( matrix_A, vector_bx, i, F_x );
		// impose_value_of_unknown ( matrix_A, vector_by, i, F_y );
		x(Q) += eta * F_x;
		y(Q) += eta * F_y;
	}  // end of  for  over vertices of 'circle'
	} // just a block of code 

	// remesh:
	disk = Mesh::Build ( tag::frontal ) .boundary ( circle ) .desired_length ( 0.2 );
	square_with_round_hole =
	  Mesh::Build ( tag::frontal ) .boundary ( boundaries ) .desired_length ( 0.2 );
	entire_square = Mesh::Build ( tag::join ) .meshes ( { square_with_round_hole, disk } );

	} // end of  for  iter

	Mesh::Composite square;
	square .add_mesh ( "circle", circle );
	square .add_mesh ( "square with hole", square_with_round_hole );
	square .add_mesh ( "disk", disk );
	square .export_to_file ( tag::gmsh, "square-split.msh" );

	std::cout << "produced file square-split.msh" << std::endl;

	return 0;
}
