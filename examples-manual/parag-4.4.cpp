
// example presented in paragraph 4.4 of the manual
// https://maniFEM.rd.ciencias.ulisboa.pt/manual-maniFEM.pdf
// extruded mesh, follow curvature

#include "maniFEM.h"

using namespace maniFEM;
using namespace std;


int main ()

{	Manifold RR2 ( tag::Euclid, tag::of_dim, 2 );
	Function xy = RR2 .build_coordinate_system ( tag::Lagrange, tag::of_degree, 1 );
	Function x = xy[0], y = xy[1];
	const double pi = 3.1415926536;

	Cell O ( tag::vertex );  x(O) = 0.;    y(O) =  0.;
	Cell A ( tag::vertex );  x(A) = 0.2;   y(A) = -1.;
	Cell B ( tag::vertex );  x(B) = 0.2;   y(B) =  1.;

	RR2 .implicit ( x == 0.2*y*y );
	Mesh OA = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( O ) .stop_at ( A ) .divided_in ( 10 );
	Mesh OB = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( O ) .stop_at ( B ) .divided_in ( 10 );

	RR2 .implicit ( y == 0.5*sin(x) );
	Cell C ( tag::vertex, tag::of_coords, { -pi, 0. }, tag::project );
	Cell D ( tag::vertex, tag::of_coords, { pi/2., 0. }, tag::project );
	Mesh OC = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( O ) .stop_at ( C ) .divided_in ( 20 );
	Mesh OD = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( O ) .stop_at ( D ) .divided_in ( 10 );

	RR2 .set_as_working_manifold();
	Mesh AB = Mesh::Build ( tag::join ) .meshes ( { OA .reverse(), OB } );
	Mesh CD = Mesh::Build ( tag::join ) .meshes ( { OC .reverse(), OD } );

	Mesh gelly = Mesh::Build ( tag::extrude ) .swatch ( AB ) .slide_along ( CD ) .follow_curvature();
	
	gelly .export_to_file ( tag::gmsh, "gelly.msh" );
	std::cout << "produced file gelly.msh" << std::endl;

}  // end of main

