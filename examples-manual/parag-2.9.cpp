
// example presented in paragraph 2.9 of the manual
// https://maniFEM.rd.ciencias.ulisboa.pt/manual-maniFEM.pdf
// build a "bumpy" hemisphere by joining twelve rectangles

#include "maniFEM.h"
#include "math.h"

using namespace maniFEM;


int main ()

{	Manifold RR3 ( tag::Euclid, tag::of_dim, 3 );
	Function xyz = RR3 .build_coordinate_system ( tag::Lagrange, tag::of_degree, 1 );
	Function x = xyz [0], y = xyz [1], z = xyz [2];

	Manifold nut = RR3 .implicit ( x*x + y*y + z*z + 1.5*x*y*z == 1. );

	// let's mesh a hemisphere (much deformed)
	// we take the four triangles in example 2.7 in the manual
	// and turn them into twelve rectangles
	// first, corners, middles of segments and centers of the four triangles :
	Cell S     ( tag::vertex );  x (S)     =  0.;  y (S)     = -1.;  z (S)     =  0.;
	Cell E     ( tag::vertex );  x (E)     =  1.;  y (E)     =  0.;  z (E)     =  0.;
	Cell N     ( tag::vertex );  x (N)     =  0.;  y (N)     =  1.;  z (N)     =  0.;
	Cell W     ( tag::vertex );  x (W)     = -1.;  y (W)     =  0.;  z (W)     =  0.;
	Cell up    ( tag::vertex );  x (up)    =  0.;  y (up)    =  0.;  z (up)    =  1.;
	// no need to project these
	Cell mSW   ( tag::vertex );  x (mSW)   = -1.;  y (mSW)   = -1.;  z (mSW)   =  0.;
	Cell mSE   ( tag::vertex );  x (mSE)   =  1.;  y (mSE)   = -1.;  z (mSE)   =  0.;
	Cell mNE   ( tag::vertex );  x (mNE)   =  1.;  y (mNE)   =  1.;  z (mNE)   =  0.;
	Cell mNW   ( tag::vertex );  x (mNW)   = -1.;  y (mNW)   =  1.;  z (mNW)   =  0.;
	nut .project ( mSW );  nut .project ( mSE );  nut .project ( mNE );  nut .project ( mNW );
	Cell mSup  ( tag::vertex );  x (mSup)  =  0.;  y (mSup)  = -1.;  z (mSup)  =  1.;
	Cell mEup  ( tag::vertex );  x (mEup)  =  1.;  y (mEup)  =  0.;  z (mEup)  =  1.;
	Cell mNup  ( tag::vertex );  x (mNup)  =  0.;  y (mNup)  =  1.;  z (mNup)  =  1.;
	Cell mWup  ( tag::vertex );  x (mWup)  = -1.;  y (mWup)  =  0.;  z (mWup)  =  1.;
	nut .project ( mSup );  nut .project ( mEup );  nut .project ( mNup );  nut .project ( mWup );
	Cell mSWup ( tag::vertex );  x (mSWup) = -1.;  y (mSWup) = -1.;  z (mSWup) =  1.;
	Cell mSEup ( tag::vertex );  x (mSEup) =  1.;  y (mSEup) = -1.;  z (mSEup) =  1.;
	Cell mNEup ( tag::vertex );  x (mNEup) =  1.;  y (mNEup) =  1.;  z (mNEup) =  1.;
	Cell mNWup ( tag::vertex );  x (mNWup) = -1.;  y (mNWup) =  1.;  z (mNWup) =  1.;
	nut .project ( mSWup );  nut .project ( mSEup );
	nut .project ( mNEup );  nut .project ( mNWup );

	// now build segments :
	int n = 10;
	Mesh S_mSW = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( S ) .stop_at ( mSW ) .divided_in ( n );
	Mesh S_mSE = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( S ) .stop_at ( mSE ) .divided_in ( n );
	Mesh E_mSE = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( E ) .stop_at ( mSE ) .divided_in ( n );
	Mesh E_mNE = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( E ) .stop_at ( mNE ) .divided_in ( n );
	Mesh N_mNE = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( N ) .stop_at ( mNE ) .divided_in ( n );
	Mesh N_mNW = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( N ) .stop_at ( mNW ) .divided_in ( n );
	Mesh W_mSW = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( W ) .stop_at ( mSW ) .divided_in ( n );
	Mesh W_mNW = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( W ) .stop_at ( mNW ) .divided_in ( n );
	Mesh S_mSup = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( S ) .stop_at ( mSup ) .divided_in ( n );
	Mesh N_mNup = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( N ) .stop_at ( mNup ) .divided_in ( n );
	Mesh E_mEup = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( E ) .stop_at ( mEup ) .divided_in ( n );
	Mesh W_mWup = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( W ) .stop_at ( mWup ) .divided_in ( n );
	Mesh mSW_mSWup = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( mSW ) .stop_at ( mSWup ) .divided_in ( n );
	Mesh mSE_mSEup = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( mSE ) .stop_at ( mSEup ) .divided_in ( n );
	Mesh mNE_mNEup = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( mNE ) .stop_at ( mNEup ) .divided_in ( n );
	Mesh mNW_mNWup = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( mNW ) .stop_at ( mNWup ) .divided_in ( n );
	Mesh up_mSup = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( up ) .stop_at ( mSup ) .divided_in ( n );
	Mesh up_mEup = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( up ) .stop_at ( mEup ) .divided_in ( n );
	Mesh up_mNup = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( up ) .stop_at ( mNup ) .divided_in ( n );
	Mesh up_mWup = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( up ) .stop_at ( mWup ) .divided_in ( n );
	Mesh mSup_mSEup = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( mSup ) .stop_at ( mSEup ) .divided_in ( n );
	Mesh mSup_mSWup = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( mSup ) .stop_at ( mSWup ) .divided_in ( n );
	Mesh mEup_mSEup = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( mEup ) .stop_at ( mSEup ) .divided_in ( n );
	Mesh mEup_mNEup = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( mEup ) .stop_at ( mNEup ) .divided_in ( n );
	Mesh mNup_mNEup = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( mNup ) .stop_at ( mNEup ) .divided_in ( n );
	Mesh mNup_mNWup = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( mNup ) .stop_at ( mNWup ) .divided_in ( n );
	Mesh mWup_mSWup = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( mWup ) .stop_at ( mSWup ) .divided_in ( n );
	Mesh mWup_mNWup = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( mWup ) .stop_at ( mNWup ) .divided_in ( n );
	
	// now the twelve rectangles :
	Mesh rect_S_SE = Mesh::Build ( tag::grid ) .shape ( tag::rectangle )
		.faces ( S_mSE, mSE_mSEup, mSup_mSEup .reverse(), S_mSup .reverse() );
	Mesh rect_S_SW = Mesh::Build ( tag::grid ) .shape ( tag::rectangle )
		.faces ( S_mSup, mSup_mSWup, mSW_mSWup .reverse(), S_mSW .reverse() );
	Mesh rect_E_SE = Mesh::Build ( tag::grid ) .shape ( tag::rectangle )
		.faces ( E_mEup, mEup_mSEup, mSE_mSEup .reverse(), E_mSE .reverse() );
	Mesh rect_E_NE = Mesh::Build ( tag::grid ) .shape ( tag::rectangle )
		.faces ( E_mNE, mNE_mNEup, mEup_mNEup .reverse(), E_mEup .reverse() );
	Mesh rect_N_NE = Mesh::Build ( tag::grid ) .shape ( tag::rectangle )
		.faces ( N_mNup, mNup_mNEup, mNE_mNEup .reverse(), N_mNE .reverse() );
	Mesh rect_N_NW = Mesh::Build ( tag::grid ) .shape ( tag::rectangle )
		.faces ( N_mNW, mNW_mNWup, mNup_mNWup .reverse(), N_mNup .reverse() );
	// yes, this sounds a lot like Carry Grant ...
	Mesh rect_W_SW = Mesh::Build ( tag::grid ) .shape ( tag::rectangle )
		.faces ( W_mSW, mSW_mSWup, mWup_mSWup .reverse(), W_mWup .reverse() );
	Mesh rect_W_NW = Mesh::Build ( tag::grid ) .shape ( tag::rectangle )
		.faces ( W_mWup, mWup_mNWup, mNW_mNWup .reverse(), W_mNW .reverse() );
	Mesh rect_up_SW = Mesh::Build ( tag::grid ) .shape ( tag::rectangle )
		.faces ( up_mWup, mWup_mSWup, mSup_mSWup .reverse(), up_mSup .reverse() );
	Mesh rect_up_SE = Mesh::Build ( tag::grid ) .shape ( tag::rectangle )
		.faces ( up_mSup, mSup_mSEup, mEup_mSEup .reverse(), up_mEup .reverse() );
	Mesh rect_up_NE = Mesh::Build ( tag::grid ) .shape ( tag::rectangle )
		.faces ( up_mEup, mEup_mNEup, mNup_mNEup .reverse(), up_mNup .reverse() );
	Mesh rect_up_NW = Mesh::Build ( tag::grid ) .shape ( tag::rectangle )
		.faces ( up_mNup, mNup_mNWup, mWup_mNWup .reverse(), up_mWup .reverse() );
											 
	// and finally join the rectangles :
	Mesh bumpy = Mesh::Build ( tag::join )
		.meshes ( { rect_S_SE, rect_S_SW, rect_E_SE, rect_E_NE, rect_N_NE, rect_N_NW,
		            rect_W_SW, rect_W_NW, rect_up_SW, rect_up_SE, rect_up_NE, rect_up_NW } );
	
	bumpy .export_to_file ( tag::gmsh, "helmet.msh" );
	std::cout << "produced file helmet.msh" << std::endl;

}  // end of main
