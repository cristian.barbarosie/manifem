
// example presented in paragraph 3.1 of the manual
// https://maniFEM.rd.ciencias.ulisboa.pt/manual-maniFEM.pdf
// build a circle from four curved segments then fill the disk with triangles

#include "maniFEM.h"

using namespace maniFEM;


int main ( )

{	Manifold RR2 ( tag::Euclid, tag::of_dim, 2 );
	Function xy = RR2 .build_coordinate_system ( tag::Lagrange, tag::of_degree, 1 );
	Function x = xy[0], y = xy[1];

	RR2 .implicit ( x*x + y*y == 1. );

	Cell N ( tag::vertex );  x(N) =  0.;  y(N) =  1.;
	Cell W ( tag::vertex );  x(W) = -1.;  y(W) =  0.;
	Cell S ( tag::vertex );  x(S) =  0.;  y(S) = -1.;
	Cell E ( tag::vertex );  x(E) =  1.;  y(E) =  0.;

	Mesh NW = Mesh::Build ( tag::grid ) .shape ( tag::segment )
	  .start_at ( N ) .stop_at ( W ) .divided_in ( 10 );
	Mesh WS = Mesh::Build ( tag::grid ) .shape ( tag::segment )
	  .start_at ( W ) .stop_at ( S ) .divided_in ( 10 );
	Mesh SE = Mesh::Build ( tag::grid ) .shape ( tag::segment )
	  .start_at ( S ) .stop_at ( E ) .divided_in ( 10 );
	Mesh EN = Mesh::Build ( tag::grid ) .shape ( tag::segment )
	  .start_at ( E ) .stop_at ( N ) .divided_in ( 10 );

	Mesh circle = Mesh::Build ( tag::join ) .meshes ( { NW, WS, SE, EN } );
	
	RR2 .set_as_working_manifold();
	Mesh disk = Mesh::Build ( tag::frontal ) .boundary ( circle )
	                                         .desired_length ( 0.157 );

	disk .export_to_file ( tag::eps, "disk.eps" );
	disk .export_to_file ( tag::gmsh, "disk.msh" );
	std::cout << "produced files disk.eps and disk.msh" << std::endl;

}  // end of main
