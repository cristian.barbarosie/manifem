
// example presented in paragraph 1.7 of the manual
// https://maniFEM.rd.ciencias.ulisboa.pt/manual-maniFEM.pdf
// mesh a cube

#include "maniFEM.h"
#include <fstream>

using namespace maniFEM;

int main ()

{	// we choose our (geometric) space dimension :
	Manifold RR3 ( tag::Euclid, tag::of_dim, 3 );
	
	// xyz is a map defined on our future mesh with values in RR2 :
	Function xyz = RR3 .build_coordinate_system ( tag::Lagrange, tag::of_degree, 1 );

	// we can extract components of xyz using the [] operator :
	Function x = xyz [0], y = xyz [1], z = xyz [2];

	Cell A ( tag::vertex );  x(A) = 0.;  y(A) = 0.;  z(A) = 0.;
	Cell B ( tag::vertex );  x(B) = 1.;  y(B) = 0.;  z(B) = 0.;
	Cell C ( tag::vertex );  x(C) = 1.;  y(C) = 0.;  z(C) = 1.;
	Cell D ( tag::vertex );  x(D) = 0.;  y(D) = 0.;  z(D) = 1.;
	Cell E ( tag::vertex );  x(E) = 0.;  y(E) = 1.;  z(E) = 0.;
	Cell F ( tag::vertex );  x(F) = 1.;  y(F) = 1.;  z(F) = 0.;
	Cell G ( tag::vertex );  x(G) = 1.;  y(G) = 1.;  z(G) = 1.;
	Cell H ( tag::vertex );  x(H) = 0.;  y(H) = 1.;  z(H) = 1.;

	Mesh AB = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( A ) .stop_at ( B ) .divided_in ( 10 );
	Mesh BC = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( B ) .stop_at ( C ) .divided_in ( 10 );
	Mesh CD = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( C ) .stop_at ( D ) .divided_in ( 10 );
	Mesh DA = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( D ) .stop_at ( A ) .divided_in ( 10 );
	Mesh EF = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( E ) .stop_at ( F ) .divided_in ( 10 );
	Mesh FG = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( F ) .stop_at ( G ) .divided_in ( 10 );
	Mesh GH = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( G ) .stop_at ( H ) .divided_in ( 10 );
	Mesh HE = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( H ) .stop_at ( E ) .divided_in ( 10 );
	Mesh AE = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( A ) .stop_at ( E ) .divided_in ( 10 );
	Mesh BF = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( B ) .stop_at ( F ) .divided_in ( 10 );
	Mesh CG = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( C ) .stop_at ( G ) .divided_in ( 10 );
	Mesh DH = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( D ) .stop_at ( H ) .divided_in ( 10 );

	Mesh ABCD = Mesh::Build ( tag::grid ) .shape ( tag::quadrangle )
		.faces ( AB, BC, CD, DA );
	Mesh EFGH = Mesh::Build ( tag::grid ) .shape ( tag::quadrangle )
		.faces ( EF, FG, GH, HE );
	Mesh AEFB = Mesh::Build ( tag::grid ) .shape ( tag::quadrangle )
		.faces ( AE, EF, BF .reverse(), AB .reverse() );
	Mesh DHEA = Mesh::Build ( tag::grid ) .shape ( tag::quadrangle )
		.faces ( DH, HE, AE .reverse(), DA .reverse() );
	Mesh CGHD = Mesh::Build ( tag::grid ) .shape ( tag::quadrangle )
		.faces ( CG, GH, DH .reverse(), CD .reverse() );
	Mesh BFGC = Mesh::Build ( tag::grid ) .shape ( tag::quadrangle )
		.faces ( BF, FG, CG .reverse(), BC .reverse() );

	Mesh cube = Mesh::Build ( tag::grid ) .shape ( tag::cube )
		.faces ( ABCD, EFGH .reverse(), BFGC, DHEA, CGHD, AEFB );

	// or, somewhat equivalently :
	// Mesh cube = Mesh::Build ( tag::grid ) .shape ( tag::cube )
	//   .vertices ( A, B, C, D, E, F, G, H ) .divided_in ( 10, 10, 10 );

	cube .export_to_file ( tag::gmsh, "cube.msh" );
	std::cout << "produced file cube.msh" << std::endl;

}  // end of main


