
// example presented in paragraph 5.5 of the manual
// https://maniFEM.rd.ciencias.ulisboa.pt/manual-maniFEM.pdf
// writing a composite mesh, torus with triangles

#include "maniFEM.h"
#include "math.h"

using namespace maniFEM;


int main ()
	
{	Manifold RR3 ( tag::Euclid, tag::of_dim, 3 );
	Function xyz = RR3 .build_coordinate_system ( tag::Lagrange, tag::of_degree, 1 );
	Function x = xyz [0], y = xyz [1], z = xyz [2];

	Function f1 = x*x + y*y;
	Function f2 = 1. - power ( f1, -0.5 );
	Function d = f1 * f2 * f2 + z*z;  // squared distance to a circle in the xy plane

	double seg_size = 0.15;
	
	RR3 .implicit ( d == 0.15, y == 0. );
	// RR3 .implicit ( d == 0.15, y == 0., z == 0. );
	Cell A ( tag::vertex, tag::of_coords, { 1.4, 0., 0. }, tag::project );
	Cell B ( tag::vertex, tag::of_coords, { 0.6, 0., 0. }, tag::project );
	Cell C ( tag::vertex, tag::of_coords, { -1.4, 0., 0. }, tag::project );
	Cell D ( tag::vertex, tag::of_coords, { -0.6, 0., 0. }, tag::project );

	RR3 .implicit ( d == 0.15, z == 0. );
	Mesh AC = Mesh::Build ( tag::frontal )
		.start_at ( A ) .towards ( { 0., 1., 0. }) .stop_at ( C ) .desired_length ( seg_size );
	Mesh CA = Mesh::Build ( tag::frontal )
		.start_at ( C ) .towards ( { 0., -1., 0. } ) .stop_at ( A ) .desired_length ( seg_size );
	Mesh BD = Mesh::Build ( tag::frontal )
		.start_at ( B ) .towards ( { 0., 1., 0. } ) .stop_at ( D ) .desired_length ( seg_size );
	Mesh DB = Mesh::Build ( tag::frontal )
		.start_at ( D ) .towards ( { 0., -1., 0. } ) .stop_at ( B ) .desired_length ( seg_size );

	RR3 .implicit ( d == 0.15, y == 0. );
	Mesh AB = Mesh::Build ( tag::frontal )
		.start_at ( A ) .towards ( { 0., 0., 1. } ) .stop_at ( B ) .desired_length ( seg_size );
	Mesh BA = Mesh::Build ( tag::frontal )
		.start_at ( B ) .towards ( { 0., 0., -1. } ) .stop_at ( A ) .desired_length ( seg_size );
	Mesh CD = Mesh::Build ( tag::frontal )
		.start_at ( C ) .towards ( { 0., 0., 1. } ) .stop_at ( D ) .desired_length ( seg_size );
	Mesh DC = Mesh::Build ( tag::frontal )
		.start_at ( D ) .towards ( { 0., 0., -1.} ) .stop_at ( C ) .desired_length ( seg_size );

	RR3 .implicit ( d == 0.15 );
	// Mesh torus ( tag::frontal, tag::desired_length, seg_size );

	Mesh torus_bdry = Mesh::Build ( tag::join ) .meshes ( { AB, BD, CD .reverse(), AC .reverse() } );
	Mesh torus_1 = Mesh::Build ( tag::frontal ) .desired_length ( seg_size )
	  .boundary ( torus_bdry ) .start_at ( A ) .towards ( { 0., 1., 1. } );

	torus_bdry = Mesh::Build ( tag::join ) .meshes ( { BA, BD .reverse(), DC .reverse(), AC } );
	Mesh torus_2 = Mesh::Build ( tag::frontal ) .desired_length ( seg_size )
	  .boundary ( torus_bdry ) .start_at ( A ) .towards ( { 0., 1., -1. } );

	torus_bdry = Mesh::Build ( tag::join ) .meshes ( { AB .reverse(), DB, CD, CA .reverse() } );
	Mesh torus_3 = Mesh::Build ( tag::frontal ) .desired_length ( seg_size )
	  .boundary ( torus_bdry ) .start_at ( A ) .towards ( { 0., -1., 1. } );

	torus_bdry = Mesh::Build ( tag::join ) .meshes ( { BA .reverse(), DB .reverse(), DC, CA } );
	Mesh torus_4 = Mesh::Build ( tag::frontal ) .desired_length ( seg_size )
	  .boundary ( torus_bdry ) .start_at ( A ) .towards ( { 0., -1., -1. } );

	Mesh torus = Mesh::Build ( tag::join ) .meshes ( { torus_1, torus_2, torus_3, torus_4 } );

	Mesh big_loop = Mesh::Build ( tag::join ) .meshes ( { AC, CA });
	Mesh small_loop = Mesh::Build ( tag::join ) .meshes ( { AB, BA } );

	Mesh::Composite comp_msh = Mesh::Build ( tag::gather )
	// .cell ( "intersection between loops", A )
		.mesh ( "big loop", big_loop )
		.mesh ( "small loop", small_loop )
		.mesh ( "torus", torus );

	comp_msh .export_to_file ( tag::gmsh, "composite.msh" );
	std::cout << "produced file composite.msh" << std::endl;	

}  // end of main
	
