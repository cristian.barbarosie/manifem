
// example presented in paragraph 1.10 of the manual
// https://maniFEM.rd.ciencias.ulisboa.pt/manual-maniFEM.pdf
// solve the Laplace operator on a triangle with Dirichlet boundary conditions
// same as in paragraph 6.3


#include "maniFEM.h"
#include <fstream>

using namespace maniFEM;


#ifdef MANIFEM_NO_FEM
int main ()
{	std::cout << "to run this program, "
	          << "you need to de-activate the MANIFEM_NO_FEM compilation flag" << std::endl
	          << "and to install the Eigen library" << std::endl
	          << "see paragraph 12.15 in the manual" << std::endl;                           }
#else  // ifndef MANIFEM_NO_FEM

	 
int main ()

{	Manifold RR2 ( tag::Euclid, tag::of_dim, 2 );
	Function xy = RR2 .build_coordinate_system ( tag::Lagrange, tag::of_degree, 1 );
	Function x = xy[0], y = xy[1];

	Cell A ( tag::vertex );  x(A) = -1.;  y(A) = 0.;
	Cell B ( tag::vertex );  x(B) =  1.;  y(B) = 0.;
	Cell C ( tag::vertex );  x(C) =  0.;  y(C) = 1.7;
 
	Mesh ABC = Mesh::Build ( tag::grid ) .shape ( tag::triangle )
		.vertices ( A, B, C ) .divided_in ( 20 );

	Mesh bdry = ABC .boundary();

	// we solve the elliptic equation
	//   - laplace u = -4
	// whose exact solution is  x^2+y^2

	Function temperature ( tag::unknown, tag::Lagrange, tag::of_degree, 1, tag::mere_symbol );
	Function phi ( tag::test, tag::Lagrange, tag::of_degree, 1, tag::mere_symbol );

	VariationalFormulation Laplace =
	    ( temperature .deriv(x) * phi .deriv(x) + temperature .deriv(y) * phi .deriv(y) )
	                 .integral_on ( ABC )
		 == -4. * phi .integral_on ( ABC );

	Laplace .add_boundary_condition ( tag::Dirichlet, temperature == x*x + y*y, tag::on, bdry );

	temperature = Laplace .solve();  // here we use the same wrapper Function 'temperature'
	// or, equivalently, we can create a new Function wrapper :
	// Function computed_temp = Laplace .solve();

	Mesh::Composite mesh_with_temp = Mesh::Build ( tag::gather )
		.mesh ( "triangle", ABC )
		.function ( "temperature", temperature );
	mesh_with_temp .export_to_file ( tag::gmsh, "triangle-Dirichlet.msh" );
	
	std::cout << "produced file triangle-Dirichlet.msh" << std::endl;

}  // end of  main

#endif  // ifdef MANIFEM_NO_FEM
