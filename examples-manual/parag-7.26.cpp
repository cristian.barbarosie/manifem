
// example presented in paragraph 7.26 of the manual
// https://maniFEM.rd.ciencias.ulisboa.pt/manual-maniFEM.pdf
// frontal mesh generation directly on the cylinder

#include "maniFEM.h"

using namespace maniFEM;


int main ( )

{	// begin with the usual two-dimensional space
	Manifold RR2 ( tag::Euclid, tag::of_dim, 2 );
	Function xy = RR2 .build_coordinate_system ( tag::Lagrange, tag::of_degree, 1 );
	Function x = xy[0], y = xy[1];

	size_t n = 20;
	double d = 2.6 / double(n);

	Manifold circle_manif = RR2 .implicit ( x*x + y*y == 1. );
	Mesh circle = Mesh::Build ( tag::frontal ) .entire_manifold() .desired_length ( d );

	Manifold::Action gx ( tag::transforms, xy, tag::into, ( x + 2.6 ) && y );
	RR2 .quotient ( gx );

	Mesh circle_fold = circle .fold ( tag::use_existing_vertices );

	Cell A ( tag::vertex );  x(A) = -1.3;  y(A) = -1.3;
	Cell C ( tag::vertex );  x(C) =  1.3;  y(C) =  1.3;
	Mesh AB = Mesh::Build ( tag::grid ) .shape ( tag::segment )
	  .start_at ( A ) .stop_at ( A ) .divided_in ( n ) .winding ( gx );
	Mesh CD = Mesh::Build ( tag::grid ) .shape ( tag::segment )
	  .start_at ( C ) .stop_at ( C ) .divided_in ( n ) .winding ( -gx );

	Mesh bdry = Mesh::Build ( tag::join ) .meshes ( { circle_fold .reverse(), AB, CD } );
	
	Mesh perf_cyl = Mesh::Build ( tag::frontal ) .boundary ( bdry ) .desired_length ( d );

	Mesh cyl_unf = perf_cyl .unfold ( tag::over_region, x*x + 2.*y*y < 20. );

	cyl_unf .export_to_file ( tag::gmsh, "perf-cyl.msh" );
	cyl_unf .export_to_file ( tag::eps, "perf-cyl.eps" );
	std::cout << "produced files perf-cyl.msh and perf_cyl.eps" << std::endl;
	
}  // end of  main



