
// example presented in paragraph 3.14 of the manual
// https://maniFEM.rd.ciencias.ulisboa.pt/manual-maniFEM.pdf
// a "bumpy" hemisphere built with frontal method

#include "maniFEM.h"
#include "math.h"

using namespace maniFEM;


int main ()

{	Manifold RR3 ( tag::Euclid, tag::of_dim, 3 );
	Function xyz = RR3 .build_coordinate_system ( tag::Lagrange, tag::of_degree, 1 );
	Function x = xyz [0], y = xyz [1], z = xyz [2];

	Manifold nut = RR3 .implicit ( x*x + y*y + z*z + 1.5*x*y*z == 1. );

	// build the base (a closed curve)
	nut .implicit ( x*x + 3.*z == 0. );
	// now the above nameless Manifold becomes the working manifold

	Cell S ( tag::vertex );  x(S) = 0.;  y(S) = -1.;  z(S) = 0.;
	std::vector < double > tau { 1., 0., 0. };
	Mesh circle = Mesh::Build ( tag::frontal ) .start_at ( S ) .towards ( tau )
		.desired_length ( 0.1 ) .entire_manifold();
	// or, equivalently :
	// Mesh circle = Mesh::Bumpy ( tag::frontal ) .start_at ( S )
	// 	towards ( { 1., 0., 0. } ) .desired_length ( 0.1 ) .entire_manifold();

	nut .set_as_working_manifold();
	std::vector < double > N { 0., 0., 1. };
	Mesh bumpy = Mesh::Build ( tag::frontal ) .boundary ( circle )
		.start_at ( S ) .towards ( N ) .desired_length ( 0.1 );
	// or, equivalently :
	// Mesh bumpy = Mesh::Build ( tag::frontal ) .boundary ( circle )
	// 	.start_at ( S ) .towards ( { 0., 0., 1. } ) .desired_length ( 0.1 );
	
	bumpy .export_to_file ( tag::gmsh, "helmet.msh" );
	std::cout << "produced file helmet.msh" << std::endl;

}  // end of main
