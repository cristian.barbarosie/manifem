
// example presented in paragraph 5.9 of the manual
// https://maniFEM.rd.ciencias.ulisboa.pt/manual-maniFEM.pdf

// mesh an arc-shaped domain, build the finite element (global) matrix
// save the mesh (composed) together with the numbering
// save also the finite element matrix
// read them back in parag-5.10.cpp

#include "maniFEM.h"
#include <fstream>
#include <Eigen/SparseCore>

using namespace maniFEM;


// Léo Moreau wrote 'write_sparse_matrix' inspired in Avi Ginsburg's post
// https://scicomp.stackexchange.com/a/21438

namespace Eigen {

template < class SparseMatrixType >
void write_sparse_matrix ( const char* filename, const SparseMatrixType & matrix )

{	std::ofstream out ( filename, std::ios::out | std::ios::binary | std::ios::trunc );

	typename SparseMatrixType::Index rows = matrix .rows(), cols = matrix .cols();
	typename SparseMatrixType::Index nonZeros = matrix .nonZeros();

	// écrire les dimensions de la matrice et le nombre de non-zeros
	out .write ( (char*) (&rows),     sizeof(typename SparseMatrixType::Index) );
	out .write ( (char*) (&cols),     sizeof(typename SparseMatrixType::Index) );
	out .write ( (char*) (&nonZeros), sizeof(typename SparseMatrixType::Index) );

	// écrire les données : indices de ligne, indices de colonne et valeurs
	for (int k = 0; k < matrix .outerSize(); ++k)
	for ( typename SparseMatrixType::InnerIterator it (matrix, k); it; ++it)
	{	typename SparseMatrixType::Index row = it .row();
		typename SparseMatrixType::Index col = it .col();
		auto value = it .value();
		out .write ( (char*) (&row),   sizeof(typename SparseMatrixType::Index)  );
		out .write ( (char*) (&col),   sizeof(typename SparseMatrixType::Index)  );
		out .write ( (char*) (&value), sizeof(typename SparseMatrixType::Scalar) );
	}  // end of two  for  loops

	out.close();
	
}  // end of  write_sparse_matrix

}  // end of  namespace Eigen


int main ( )

{	Manifold RR2 ( tag::Euclid, tag::of_dim, 2 );
	Function xy = RR2 .build_coordinate_system ( tag::Lagrange, tag::of_degree, 1 );
	Function x = xy[0], y = xy[1];

	Function seg_len = 0.005 + 0.05*power(x*x+y*y,0.5);
	
	Cell A ( tag::vertex );  x(A) = -1.;   y(A) = 0.;
	Cell B ( tag::vertex );  x(B) = -0.7;  y(B) = 0.;
	Cell C ( tag::vertex );  x(C) =  0.7;  y(C) = 0.;
	Cell D ( tag::vertex );  x(D) =  1.;   y(D) = 0.;

	Manifold horizontal_axis = RR2 .implicit ( y == 0. );
	Mesh AB = Mesh::Build ( tag::frontal )
		.start_at ( A ) .towards ( { 1., 0. } ) .stop_at ( B ) .desired_length ( seg_len );
	Mesh CD = Mesh::Build ( tag::frontal )
		.start_at ( C ) .towards ( { 1., 0. } ) .stop_at ( D ) .desired_length ( seg_len );

	Manifold circle_manif = RR2 .implicit ( x*x + y*y == 1. );
	Cell E ( tag::vertex, tag::of_coords, { -0.1, 1. }, tag::project );
	Cell F ( tag::vertex, tag::of_coords, {  0.1, 1. }, tag::project );
	Mesh AE = Mesh::Build ( tag::frontal )
		. start_at ( A ) .towards ( { 0., 1. } ) .stop_at ( E ) .desired_length ( seg_len );
	Mesh FD = Mesh::Build ( tag::frontal )
		.start_at ( F ) .towards ( { 1., 0. } ) .stop_at ( D ) .desired_length ( seg_len );

	const double r_min = 0.07, gamma = 1000.;
	const double alpha = (0.49-r_min*r_min)/(1.-1./(1.+0.2401*gamma));
	Manifold M_manif = RR2 .implicit
		( x*x + y*y == alpha * ( 1. - 1. / (1.+gamma*x*x*x*x) ) + r_min*r_min );
	// Manifold M_manif = RR2 .implicit ( x*x + y*y == 0.49 );
	Mesh BC = Mesh::Build ( tag::frontal )
		.start_at ( B ) .towards ( { 0., 1. } ) .stop_at ( C ) .desired_length ( seg_len );

	RR2 .set_as_working_manifold();
	Mesh EF = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( E ) .stop_at ( F ) .divided_in ( 0.2 / seg_len (E) );
	Mesh AD = Mesh::Build ( tag::join ) .meshes ( { AE, EF, FD } );
	Mesh contour = Mesh::Build ( tag::join ) .meshes ( { AD. reverse(), AB, BC, CD } );

	Mesh omega = Mesh::Build ( tag::frontal ) .boundary ( contour ) .desired_length ( seg_len );

	// Hooke's Law 
	const double lambda = 1., mu = 3.;

	tag::Util::Tensor < double > Hooke (2,2,2,2);
	Hooke (0,0,0,0) = 2*mu+lambda;
	Hooke (0,0,0,1) = 0.;
	Hooke (0,0,1,0) = 0.;
	Hooke (0,0,1,1) = lambda;
	Hooke (0,1,0,0) = 0.;
	Hooke (0,1,0,1) = mu;
	Hooke (0,1,1,0) = mu;
	Hooke (0,1,1,1) = 0.;
	Hooke (1,0,0,0) = 0.;
	Hooke (1,0,0,1) = mu;
	Hooke (1,0,1,0) = mu;
	Hooke (1,0,1,1) = 0.;
	Hooke (1,1,0,0) = lambda;
	Hooke (1,1,0,1) = 0.;
	Hooke (1,1,1,0) = 0.;
	Hooke (1,1,1,1) = 2*mu+lambda;
	
	Cell::Numbering numbering ( tag::map );
	{ // just a block of code for hiding 'it' and 'counter'
	Mesh::Iterator it = omega.iterator ( tag::over_vertices );
	size_t counter = 0;
	for ( it .reset() ; it .in_range(); it ++ )
	{	Cell p = *it;  numbering (p) = counter;  ++counter;  }
	} // just a block of code 
	// in each node P with number i>=0 ,  u_x(P)=vector_sol(i) , u_y(P)=vector_sol(i+N)
    // where N is the number of nodes  i=numbering(P)	
	size_t N = omega .number_of ( tag::vertices );
	assert ( N == numbering.size() );
	size_t size_matrix = 2*N;
	std::cout << "global matrix " << size_matrix << "x" << size_matrix << std::endl;
	Eigen::SparseMatrix <double> matrix_A ( size_matrix, size_matrix );

	// declare the type of finite element
	FiniteElement fe ( tag::with_master, tag::triangle, tag::Lagrange, tag::of_degree, 1 );
	Integrator integ = fe .set_integrator ( tag::Gauss, tag::tri_1 );
	
	// run over all triangular cells composing omega
	{ // just a block of code for hiding 'it'
	Mesh::Iterator it = omega .iterator ( tag::over_cells_of_max_dim );
	for ( it.reset(); it.in_range(); it++ )
	{	Cell small_tri = *it;
		assert ( small_tri .dim() == 2 );
		fe.dock_on ( small_tri );
		// run twice over the four vertices of 'small_tri'
		Mesh::Iterator it1 = small_tri .boundary() .iterator ( tag::over_vertices );
		Mesh::Iterator it2 = small_tri .boundary() .iterator ( tag::over_vertices );
		for ( it1 .reset(); it1 .in_range(); it1 ++ )
		{	Cell V = *it1 ;
			Function psiV = fe .basis_function (V),
			         d_psiV_dx = psiV .deriv (x),
			         d_psiV_dy = psiV .deriv (y);
			for ( it2 .reset(); it2 .in_range(); it2 ++ )
			{	Cell W = *it2;  // V may be the same as W, no problem about that
				Function psiW = fe .basis_function (W),
				         d_psiW_dx = psiW .deriv (x),
				         d_psiW_dy = psiW .deriv (y);
				double int_d_psiW_dx_d_psiV_dx = fe .integrate ( d_psiW_dx * d_psiV_dx );
				double int_d_psiW_dx_d_psiV_dy = fe .integrate ( d_psiW_dx * d_psiV_dy );
				double int_d_psiW_dy_d_psiV_dx = fe .integrate ( d_psiW_dy * d_psiV_dx );
				double int_d_psiW_dy_d_psiV_dy = fe .integrate ( d_psiW_dy * d_psiV_dy );
				// 'fe' is already docked on 'small_tri' so this will be the domain of integration
				matrix_A.coeffRef ( numbering (V), numbering (W) ) += 
				  Hooke (0,0,0,0) *	int_d_psiW_dx_d_psiV_dx +
				  Hooke (0,0,0,1) *	int_d_psiW_dy_d_psiV_dx +
				  Hooke (0,1,0,0) *	int_d_psiW_dx_d_psiV_dy +
				  Hooke (0,1,0,1) *	int_d_psiW_dy_d_psiV_dy  ;
				matrix_A.coeffRef ( numbering (V), numbering (W) + N ) += 
				  Hooke (0,0,1,0) *	int_d_psiW_dx_d_psiV_dx +
				  Hooke (0,0,1,1) *	int_d_psiW_dy_d_psiV_dx +
				  Hooke (0,1,1,0) *	int_d_psiW_dx_d_psiV_dy +
				  Hooke (0,1,1,1) *	int_d_psiW_dy_d_psiV_dy  ;
				matrix_A.coeffRef ( numbering (V) + N, numbering (W) ) += 
				  Hooke (1,0,0,0) *	int_d_psiW_dx_d_psiV_dx +
				  Hooke (1,0,0,1) *	int_d_psiW_dy_d_psiV_dx +
				  Hooke (1,1,0,0) *	int_d_psiW_dx_d_psiV_dy +
				  Hooke (1,1,0,1) *	int_d_psiW_dy_d_psiV_dy  ;
				matrix_A.coeffRef ( numbering (V) + N, numbering (W) + N ) += 
				  Hooke (1,0,1,0) *	int_d_psiW_dx_d_psiV_dx +
				  Hooke (1,0,1,1) *	int_d_psiW_dy_d_psiV_dx +
				  Hooke (1,1,1,0) *	int_d_psiW_dx_d_psiV_dy +
				  Hooke (1,1,1,1) *	int_d_psiW_dy_d_psiV_dy  ;
			}  // end of  for it1 (over vertices of small_tri)
		}  // end of  for it2 (over vertices of small_tri)
	}  // end of  for it1 (over triangles of omega)
	} // just a block of code 

	Mesh::Composite arc = Mesh::Build ( tag::gather )
		.mesh ( "arc", omega )
		.mesh ( "top", EF )         // we may want to impose Neumann   bc here
		.cell ( "left corner", A )  // we may want to impose Dirichlet bc here
		.mesh ( "left foot", AB )   // we may want to impose Dirichlet bc here
		.mesh ( "right foot", CD )  // we may want to impose Dirichlet bc here
		.numbering ( numbering );
	arc .export_to_file ( tag::gmsh, "arc.msh" );
	
	write_sparse_matrix ( "fe_matrix.dat", matrix_A );

}  // end of  main
