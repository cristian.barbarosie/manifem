
// example presented in paragraph 3.9 of the manual
// https://maniFEM.rd.ciencias.ulisboa.pt/manual-maniFEM.pdf
// a "bumpy" hemisphere meshed by frontal method

#include "maniFEM.h"
#include "math.h"

using namespace maniFEM;


int main ()

{	Manifold RR3 ( tag::Euclid, tag::of_dim, 3 );
	Function xyz = RR3 .build_coordinate_system ( tag::Lagrange, tag::of_degree, 1 );
	Function x = xyz [0], y = xyz [1], z = xyz [2];

	Manifold nut = RR3 .implicit ( x*x + y*y + z*z + 1.5*x*y*z == 1. );

	// build the base (a closed curve)
	nut .implicit ( x*x + 3.*z == 0. );

	Mesh circle = Mesh::Build ( tag::frontal ) .entire_manifold()
		.desired_length ( 0.1 ) .orientation ( tag::random );
	  
	// should produce error message  if no orientation is provided
	// should produce error message  with  .orientation ( tag::intrinsic )
	// should produce error message  with  .orientation ( tag::inherent )

	nut .set_as_working_manifold();
	Mesh bumpy = Mesh::Build ( tag::frontal ) .boundary ( circle )
		.desired_length ( 0.1 ) .orientation ( tag::inherent );
	  
	// should produce error message  if no orientation is provided
	// should produce error message  with  .orientation ( tag::intrinsic )
	// with .orientation ( tag::inherent )  we will get unpredictably the upper or lower half
	//                                      depending on the orientation of 'circle'

	Mesh::Iterator it = bumpy .iterator ( tag::over_vertices );
	for ( it .reset();  it .in_range(); it++ )
	{	Cell ver = *it;
		if ( ver .is_inner_to ( bumpy ) )  bumpy .barycenter ( ver );  }
	
	bumpy .export_to_file ( tag::gmsh, "helmet.msh" );
	std::cout << "produced file helmet.msh" << std::endl;

}  // end of main
