
// example presented in paragraph 3.2 of the manual
// https://maniFEM.rd.ciencias.ulisboa.pt/manual-maniFEM.pdf
// build a circle then fill the disk, all frontal

#include "maniFEM.h"

using namespace maniFEM;


int main ( )

{	Manifold RR2 ( tag::Euclid, tag::of_dim, 2 );
	Function xy = RR2 .build_coordinate_system ( tag::Lagrange, tag::of_degree, 1 );
	Function x = xy[0], y = xy[1];

	Manifold circle_manif = RR2 .implicit ( x*x + y*y == 1. );

	// Mesh circle ( tag::frontal, tag::entire_manifold, circle_manif, tag::desired_length, 0.2 );
	// Mesh circle ( tag::frontal, tag::desired_length, 0.2 );

	Cell A ( tag::vertex );  x(A) = 0.;  y(A) = 1.;
	Mesh circle = Mesh::Build ( tag::frontal ) .entire_manifold()
	                                           .start_at ( A )
	                                           .desired_length ( 0.2 );

	// should work without                .start_at ( A )
	// should work just the same with     .orientation ( tag::inherent )
	// should produce error message with  .orientation ( tag::intrinsic )
	// should work with                   .orientation ( tag::random )
	//        but constructor  Mesh disk  below may enter in an endless process
	//        of meshing the exterior of the disk

	RR2 .set_as_working_manifold();
	Mesh disk = Mesh::Build ( tag::frontal ) .boundary ( circle )
	                                         .desired_length ( 0.2 );

	// should work just the same with     .orientation ( tag::intrinsic )
	// should produce error message with  .orientation ( tag::random )
	// should produce error message with  .orientation ( tag::inherent )

	disk .export_to_file ( tag::eps, "disk.eps" );
	disk .export_to_file ( tag::gmsh, "disk.msh" );
	std::cout << "produced files disk.eps and disk.msh" << std::endl;

}  // end of main
