
// example presented in paragraph 7.22 of the manual
// https://maniFEM.rd.ciencias.ulisboa.pt/manual-maniFEM.pdf
// fold a six-sided figure

#include "maniFEM.h"

using namespace maniFEM;


int main ( )

{	Manifold RR2 ( tag::Euclid, tag::of_dim, 2 );
	Function xy = RR2 .build_coordinate_system ( tag::Lagrange, tag::of_degree, 1 );
	Function x = xy[0], y = xy[1];

	const double d = 0.13;
	const size_t n = 1.5 / d, m = 2.6 / d;

	Cell A ( tag::vertex );  x(A) = -1.5;  y(A) = -1.3;
	Cell B ( tag::vertex );  x(B) =  0. ;  y(B) = -1.3;
	Cell C ( tag::vertex );  x(C) =  1.5;  y(C) = -1.3;
	Cell D ( tag::vertex );  x(D) =  1.5;  y(D) =  1.3;
	Cell E ( tag::vertex );  x(E) =  0. ;  y(E) =  1.3;
	Cell F ( tag::vertex );  x(F) = -1.5;  y(F) =  1.3;

	Mesh AB = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( A ) .stop_at ( B ) .divided_in ( n );
	Mesh BC = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( B ) .stop_at ( C ) .divided_in ( n );
	Mesh CD = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( C ) .stop_at ( D ) .divided_in ( m );
	Mesh DE = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( D ) .stop_at ( E ) .divided_in ( n );
	Mesh EF = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( E ) .stop_at ( F ) .divided_in ( n );
	Mesh FA = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( F ) .stop_at ( A ) .divided_in ( m );

	Manifold circle = RR2 .implicit ( x*x + y*y == 1. );
	Mesh inner = Mesh::Build ( tag::frontal ) .entire_manifold() .desired_length ( d );	

	Mesh bdry = Mesh::Build ( tag::join )
		.meshes ( { AB, BC, CD, DE, EF, FA, inner .reverse() } );

	RR2 .set_as_working_manifold();
	Mesh square = Mesh::Build ( tag::frontal ) .boundary ( bdry ) .desired_length ( d );

	Mesh torus = square .fold ( tag::identify, CD, tag::with, FA .reverse(),
	                            tag::identify, BC, tag::with, EF .reverse(),
	                            tag::identify, AB, tag::with, DE .reverse(),
	                            tag::use_existing_vertices                  );
	std::cout << "produced folded mesh, now drawing, please wait" << std::endl << std::flush;
	
	torus .export_to_file ( tag::eps, "torus.eps", tag::unfold,
	                        tag::over_region, -2.1 < x < 4.3, -3.6 < y < 2.1 );
	std::cout << "produced file torus.eps - please edit before viewing" << std::endl;	

}  // end of main




