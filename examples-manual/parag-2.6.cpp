
// example presented in paragraph 2.6 of the manual
// https://maniFEM.rd.ciencias.ulisboa.pt/manual-maniFEM.pdf
// an arc of hiperbola on an implicit manifold

#include "maniFEM.h"

using namespace maniFEM;


int main ()

{	Manifold RR2 ( tag::Euclid, tag::of_dim, 2 );
	Function xy = RR2 .build_coordinate_system ( tag::Lagrange, tag::of_degree, 1 );
	Function x = xy[0], y = xy[1];

	RR2.implicit ( x*y == 1. );

	Cell A ( tag::vertex );  x(A) = 0.5;  y(A) = 2.;
	Cell B ( tag::vertex );  x(B) = 3.;   y(B) = 0.333333333333;

	Mesh arc_of_hiperbola = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( A ) .stop_at ( B ) .divided_in ( 7 );

	arc_of_hiperbola .export_to_file ( tag::eps, "hiperbola.eps" );
	arc_of_hiperbola .export_to_file ( tag::gmsh, "hiperbola.msh" );
	std::cout << "produced files hiperbola.eps and hiperbola.msh" << std::endl;

}  // end of main
