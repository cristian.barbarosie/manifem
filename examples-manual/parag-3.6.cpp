
// example presented in paragraph 3.6 of the manual
// https://maniFEM.rd.ciencias.ulisboa.pt/manual-maniFEM.pdf
// mesh a sphere by frontal method

#include "maniFEM.h"

using namespace maniFEM;


int main ()

{	Manifold RR3 ( tag::Euclid, tag::of_dim, 3 );
	Function xyz = RR3 .build_coordinate_system ( tag::Lagrange, tag::of_degree, 1 );
	Function x = xyz [0], y = xyz [1], z = xyz [2];

	RR3 .implicit ( x*x + y*y + z*z == 1. );
	Mesh sphere = Mesh::Build ( tag::frontal ) .desired_length ( 0.11 ) .entire_manifold();

	// should produce error message without .entire_manifold()
	// should work just the same with       .orientation ( tag::inherent )
	// should work with                     .orientation ( tag::random )
	// should produce error message with    .orientation ( tag::intrinsic )

	// uncomment the three lines below to slightly improve the quality of the mesh
	
	// Mesh::Iterator it = sphere .iterator ( tag::over_vertices );
	// for ( it .reset(); it .in_range(); it++ )
	// {	Cell ver = *it;  sphere .barycenter ( ver );  }
	
	sphere .export_to_file ( tag::gmsh, "sphere.msh" );
	std::cout << "produced file sphere.msh" << std::endl;

}  // end of main
