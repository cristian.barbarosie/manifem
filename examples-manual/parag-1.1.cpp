
// example presented in paragraph 1.1 of the manual
// https://maniFEM.rd.ciencias.ulisboa.pt/manual-maniFEM.pdf
// build a twisted rectangle in 3D

#include "maniFEM.h"

using namespace maniFEM;
using namespace std;

int main ()

{	// we choose our (geometric) space dimension :
	Manifold RR3 ( tag::Euclid, tag::of_dim, 3 );
	
	// xyz is a map defined on our future mesh with values in RR3 :
	Function xyz = RR3 .build_coordinate_system ( tag::Lagrange, tag::of_degree, 1 );

	// we can extract components of xyz using the [] operator :
	Function x = xyz [0], y = xyz [1], z = xyz [2];

	// Let's build a rectangular mesh. First, the four corners :
	Cell SW ( tag::vertex );  x(SW) = -1.;  y(SW) =  0.;  z(SW) =  0.;
	Cell SE ( tag::vertex );  x(SE) =  1.;  y(SE) =  0.;  z(SE) =  0.;
	Cell NE ( tag::vertex );  x(NE) =  1.;  y(NE) =  1.;  z(NE) =  0.;
	Cell NW ( tag::vertex );  x(NW) = -1.;  y(NW) =  1.;  z(NW) =  1.;

	// alternative syntax for declaring a vertex :
	// Cell SW ( tag::vertex, tag::of_coords, { -1., 0., 0. } )
	
	// we access the coordinates of a point using the () operator :
	cout << "coordinates of NW : " << x(NW) << " " << y(NW) << " " << z(NW) << endl;
	
	// now build the four sides of the rectangle :
	Mesh south = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( SW ) .stop_at ( SE ) .divided_in ( 10 );
	Mesh east = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( SE ) .stop_at ( NE ) .divided_in ( 12 );
	Mesh north = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( NE ) .stop_at ( NW ) .divided_in ( 10 );
	Mesh west  = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( NW ) .stop_at ( SW ) .divided_in ( 12 );

	// and now the rectangle :
	Mesh rect_mesh = Mesh::Build ( tag::grid ) .shape ( tag::rectangle )
		.faces ( south, east, north, west );
	 
	// we may want to visualize the resulting mesh
	// here is one way to export the mesh in the "msh" format :
	rect_mesh .export_to_file ( tag::gmsh, "rectangle.msh" );

	// let's define two symbolic functions
	Function f = x*x + 1/(5+y), g = x*y;
	// and compute their integral on the rectangle, using Gauss quadrature with 9 points :
		
   // ------------  code below depends on the MANIFEM_NO_FEM compilation flag  ----------------
   // ------------ and on the Eigen library - see paragraph 12.15 in the manual ---------------

	#ifndef MANIFEM_NO_FEM
	Integrator integr ( tag::Gauss, tag::quad_9 );
	cout << "integral of f " << integr ( f, tag::on, rect_mesh ) << endl;
	cout << "integral of g " << integr ( g, tag::on, rect_mesh ) << endl;
	#else  // ifndef MANIFEM_NO_FEM
	cout << "to compute integrals, undefine flag MANIFEM_NO_FEM" << endl;
	cout << "(remember to make clean before running again)" << endl;
	#endif  // ifndef MANIFEM_NO_FEM
		
	cout << "produced file rectangle.msh" << endl;

}  // end of main
