
// example presented in paragraph 9.3 of the manual
// https://maniFEM.rd.ciencias.ulisboa.pt/manual-maniFEM.pdf
// build a disk-like mosaic mesh, solve a variational problem

#include "maniFEM.h"

using namespace maniFEM;

//                 E
//
//                       D
//
//               C
//               
//                           B
//                            
//          O         A        

int main ( )

{	Manifold RR2 ( tag::Euclid, tag::of_dim, 2 );
	Function xy = RR2 .build_coordinate_system ( tag::Lagrange, tag::of_degree, 1 );
	Function x = xy[0], y = xy[1];

	const size_t inner_divisions = 5, outer_divisions = 8;
	const double inner_radius = 1., outer_width = 1.;
	const double pi = 4. * std::atan(1.);
	const double step_theta = pi / 3.;

	Cell O ( tag::vertex, tag::of_coords, { 0., 0. } );
	Cell ini_A ( tag::vertex, tag::of_coords, { inner_radius, 0. } );
	Mesh ini_OA = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( O ) .stop_at ( ini_A ) .divided_in ( inner_divisions );
	Cell ini_B ( tag::vertex, tag::of_coords,
	             { inner_radius + 0.86 * outer_width, 0.5 * outer_width } );
	Mesh ini_AB = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( ini_A ) .stop_at ( ini_B ) .divided_in ( outer_divisions );

	Cell A = ini_A, B = ini_B;
	Mesh OA = ini_OA, AB = ini_AB;
	std::list < Mesh > tiles, Dirichlet_sides, Neumann_sides;

	// build five shapes
	// each shape includes two triangles and a rectangle
	for ( size_t i = 1; i < 6; i++ )
	{	double theta = i * step_theta;
		double sin_theta = std::sin ( theta ), cos_theta = std::cos ( theta );
		// one new vertex
		Cell C ( tag::vertex, tag::of_coords,
		         { inner_radius * std::cos ( theta ), inner_radius * std::sin ( theta ) } );
		// two new segments
		Mesh OC = Mesh::Build ( tag::grid ) .shape ( tag::segment )
			.start_at ( O ) .stop_at ( C ) .divided_in ( inner_divisions );
		Mesh AC = Mesh::Build ( tag::grid ) .shape ( tag::segment )
			.start_at ( A ) .stop_at ( C ) .divided_in ( inner_divisions );
		// a triangle
		Mesh OAC = Mesh::Build ( tag::grid ) .shape ( tag::triangle )
			.faces ( OA, AC, OC .reverse() );
		tiles .push_back ( OAC );
		// one new vertex
		Cell D ( tag::vertex, tag::of_coords,
		         {   ( inner_radius + 0.86 * outer_width ) * cos_theta // +
		           + ( 0.5 * outer_width )                 * sin_theta,
		             ( inner_radius + 0.86 * outer_width ) * sin_theta // -
		           - ( 0.5 * outer_width )                 * cos_theta } );
		// two new segments
		Mesh BD = Mesh::Build ( tag::grid ) .shape ( tag::segment )
			.start_at ( B ) .stop_at ( D ) .divided_in ( inner_divisions );
		Mesh CD = Mesh::Build ( tag::grid ) .shape ( tag::segment )
			.start_at ( C ) .stop_at ( D ) .divided_in ( outer_divisions );
		Dirichlet_sides .push_back ( BD );
		// a rectangle
		Mesh ABDC = Mesh::Build ( tag::grid ) .shape ( tag::rectangle )
		  .faces ( AB, BD, CD .reverse(), AC .reverse() );
		tiles .push_back ( ABDC );
		// one new vertex
		Cell E ( tag::vertex, tag::of_coords,
		         {   ( inner_radius + 0.86 * outer_width ) * cos_theta // -
		           - ( 0.5 * outer_width )                 * sin_theta,
		             ( inner_radius + 0.86 * outer_width ) * sin_theta // +
		           + ( 0.5 * outer_width )                 * cos_theta } );
		// two new segments
		Mesh DE = Mesh::Build ( tag::grid ) .shape ( tag::segment )
			.start_at ( D ) .stop_at ( E ) .divided_in ( outer_divisions );
		Mesh CE = Mesh::Build ( tag::grid ) .shape ( tag::segment )
			.start_at ( C ) .stop_at ( E ) .divided_in ( outer_divisions );
		Neumann_sides .push_back ( DE );
		// a triangle
		Mesh CDE = Mesh::Build ( tag::grid ) .shape ( tag::triangle )
			.faces ( CD, DE, CE .reverse() );
		tiles .push_back ( CDE );
		A = C;  B = E;  OA = OC;  AB = CE;                                                     }

	// one last shape to close the loop
	// one new segment
	Mesh AC = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( A ) .stop_at ( ini_A ) .divided_in ( inner_divisions );
	// a triangle
	Mesh OAC = Mesh::Build ( tag::grid ) .shape ( tag::triangle )
		.faces ( OA, AC, ini_OA .reverse() );
	tiles .push_back ( OAC );
	// one new vertex
	Cell D ( tag::vertex, tag::of_coords,
	         { inner_radius + 0.86 * outer_width, -0.5 * outer_width } );
	// two new segments
	Mesh BD = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( B ) .stop_at ( D ) .divided_in ( inner_divisions );
	Mesh CD = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( ini_A ) .stop_at ( D ) .divided_in ( outer_divisions );
	Dirichlet_sides .push_back ( BD );
	// a rectangle
	Mesh ABDC = Mesh::Build ( tag::grid ) .shape ( tag::quadrangle )
		.faces ( AB, BD, CD .reverse(), AC .reverse() );
	tiles .push_back ( ABDC );
	// one new segment
	Mesh DE = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( D ) .stop_at ( ini_B ) .divided_in ( outer_divisions );
	Neumann_sides .push_back ( DE );
	// a triangle
	Mesh CDE = Mesh::Build ( tag::grid ) .shape ( tag::triangle )
		.faces ( CD, DE, ini_AB .reverse() );
	tiles .push_back ( CDE );

	// join all tiles
	Mesh mosaic = Mesh::Build ( tag::join ) .meshes ( tiles );
	Mesh Dirichlet_bdry = Mesh::Build ( tag::join ) .meshes ( Dirichlet_sides );
	Mesh Neumann_bdry = Mesh::Build ( tag::join ) .meshes ( Neumann_sides );

	Function temperature ( tag::unknown, tag::Lagrange, tag::of_degree, 1, tag::mere_symbol );
	Function phi ( tag::test, tag::Lagrange, tag::of_degree, 1, tag::mere_symbol );

	VariationalFormulation Laplace =
	    (   temperature .deriv(x) * phi .deriv(x) // +
			+ temperature .deriv(y) * phi .deriv(y) ) .integral_on ( mosaic )
		 == 6. * phi .integral_on ( Neumann_bdry );	

	Laplace .add_boundary_condition ( tag::Dirichlet, temperature == x+y, tag::on, Dirichlet_bdry );

	temperature = Laplace .solve();  // here we use the same wrapper Function 'temperature'
	// or, equivalently, we can create a new Function wrapper :
	// Function computed_temp = Laplace .solve();

	Mesh::Composite mesh_with_temp = Mesh::Build ( tag::gather )
		.mesh ( "mosaic", mosaic )
		.function ( "temperature", temperature );
	mesh_with_temp .export_to_file ( tag::gmsh, "mosaic.msh" );
	
	std::cout << "produced file mosaic.msh" << std::endl;

}  // end of  main

