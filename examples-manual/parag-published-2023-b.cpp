
// this example is presented in the paper
// C. Barbarosie, A.M. Toader,
// Stress formulation and duality approach in periodic homogenization,
// Discrete and Continuous Dynamical Systems, Series B, vol. 29, 2023
// https://arxiv.org/abs/2209.06921

// it is written in C++ and uses the maniFEM library
// https://maniFEM.rd.ciencias.ulisboa.pt/

// you must activate finite elements in maniFEM and install the Eigen library
// as explained in paragraph 12.15 of maniFEM's manual

// to compile and run this program, you should issue, in the main directory of maniFEM, the command
//    make run-parag-published-2023-b.cpp

// we build a mesh with square periodicity having a circular hole
// the mesh is made of triangular elements
// we implement the formulation in stress of the cellular problem,
// equation (23) in the above referred paper


#include "maniFEM.h"
#include <fstream>
#include <set>
#include <Eigen/Sparse>
#include <Eigen/OrderingMethods>
using namespace maniFEM;
using namespace std;


int main ( )

{	Manifold RR2 ( tag::Euclid, tag::of_dim, 2 );
	Function xy = RR2 .build_coordinate_system ( tag::Lagrange, tag::of_degree, 1 );
	Function x = xy [0], y = xy [1];

	size_t n = 10;
	double l = 2.6;
	double d = l / double(n);
	double epsilon = 0.1;
	double areaY = l*l ;

	Cell A ( tag::vertex, tag::of_coords, { -l/2. - epsilon, -l/2. } );
	Cell B ( tag::vertex, tag::of_coords, {  l/2. - epsilon, -l/2. } );
	Cell C ( tag::vertex, tag::of_coords, {  l/2. + epsilon,  l/2. } );
	Cell D ( tag::vertex, tag::of_coords, { -l/2. + epsilon,  l/2. } );

	Mesh AB = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( A ) .stop_at ( B ) .divided_in ( n );
	Mesh BC = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( B ) .stop_at ( C ) .divided_in ( n );
	Mesh CD = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( C ) .stop_at ( D ) .divided_in ( n );
	Mesh DA = Mesh::Build ( tag::grid ) .shape ( tag::segment )
		.start_at ( D ) .stop_at ( A ) .divided_in ( n );

	Manifold circle = RR2 .implicit ( x*x + y*y == 0.7 );
	Mesh inner = Mesh::Build ( tag::frontal ) .entire_manifold() .desired_length ( d );

	Mesh bdry = Mesh::Build ( tag::join ) .meshes ( { AB, BC, CD, DA, inner .reverse() } );

	RR2.set_as_working_manifold();
	Mesh square = Mesh::Build ( tag::frontal ) .boundary ( bdry ) .desired_length ( d );

	Mesh torus = square .fold ( tag::identify, BC, tag::with, DA.reverse(),
	                            tag::identify, AB, tag::with, CD.reverse(),
	                            tag::use_existing_vertices                 );

	// Hooke's Law,  E = 1.,  nu = 0.3
	double lambda = 0.576923, mu = 0.38461538;

	// tag::Util::Tensor is a class defined in maniFEM for arbitrary order tensors
	tag::Util::Tensor < double > Hooke (2,2,2,2);
	Hooke (0,0,0,0) = 2*mu + lambda;
	Hooke (0,0,0,1) = 0.;
	Hooke (0,0,1,0) = 0.;
	Hooke (0,0,1,1) = lambda;
	Hooke (0,1,0,0) = 0.;
	Hooke (0,1,0,1) = mu;
	Hooke (0,1,1,0) = mu;
	Hooke (0,1,1,1) = 0.;
	Hooke (1,0,0,0) = 0.;
	Hooke (1,0,0,1) = mu;
	Hooke (1,0,1,0) = mu;
	Hooke (1,0,1,1) = 0.;
	Hooke (1,1,0,0) = lambda;
	Hooke (1,1,0,1) = 0.;
	Hooke (1,1,1,0) = 0.;
	Hooke (1,1,1,1) = 2*mu + lambda;

	// declare the type of finite element
	FiniteElement fe ( tag::with_master, tag::triangle, tag::Lagrange, tag::of_degree, 1 );
	fe .set_integrator ( tag::Gauss, tag::tri_6 );

	std::map < Cell, size_t > numbering;
	{ // just a block of code for hiding 'it' and 'counter'
	Mesh::Iterator it = square .iterator ( tag::over_vertices );
	size_t counter = 0;
	for ( it .reset() ; it .in_range(); it ++ )
	{	Cell V = *it;  numbering [V] = counter;  ++ counter;  }
	assert ( counter == numbering .size() );
	} // just a block of code

	const size_t number_dofs = numbering.size();
	std::cout << "global matrix " << 2*number_dofs + 4 << "x" << 2*number_dofs + 4 << std::endl;
	Eigen::SparseMatrix < double > matrix_A ( 2*number_dofs + 4, 2*number_dofs + 4 );
	// our unknowns are : values of the displacement at nodes (2*number_dofs values)
	// and the unknown 'macro_strain' (4 values)
	// 'macro_stress' is given as input, see below
	
	matrix_A .reserve ( Eigen::VectorXi::Constant ( 2*number_dofs + 4, 18 ) );
	// since we will be working with a mesh of triangles,
	// there will be, in average, 18 = 2*(6+1) + 4 non-zero elements per column
	// the diagonal entry plus six neighbour vertices plus four equations

	// we fill the main diagonal with ones
	// then we put zero for vertices belonging to 'torus'
	{ // just a block of code for hiding 'it'
	for ( size_t i = 0; i < 2*number_dofs; i++ ) matrix_A .coeffRef ( i, i ) = 1.;
	Mesh::Iterator it = torus .iterator ( tag::over_vertices );
	for ( it .reset(); it .in_range(); it++ )
	{	Cell V = *it;
		matrix_A .coeffRef ( 2 * numbering [V],     2 * numbering [V]     ) = 0.;
		matrix_A .coeffRef ( 2 * numbering [V] + 1, 2 * numbering [V] + 1 ) = 0.;  }
	} // just a block of code for hiding 'it'
	
	Eigen::VectorXd vector_b ( 2*number_dofs + 4 );
	vector_b .setZero();

	Function xy_torus = Manifold::working .coordinates();
	Function x_torus = xy_torus [0], y_torus = xy_torus [1];
	// 'x_torus' and 'y_torus' are multi-functions defined on the torus
	// if the periodicity cell is given by two vectors v , w in RR^2,
	// then 'x_torus .jump()' can be identified with the pair (vx, wx)
	//  and 'y_torus .jump()' can be identified with the pair (vy, wy)

	// macroscopic stress matrix, defined by the user (this is the input of the program)
	// denoted by S in the paper referred at the beginning of this file
	// tag::Util::Tensor is a class defined in maniFEM for arbitrary order tensors
	tag::Util::Tensor < double > macro_stress (2,2);
	macro_stress (0,0) = 0.744035;
	macro_stress (0,1) = 0.00289613;
	macro_stress (1,0) = 0.00289613;
	macro_stress (1,1) = 0.742596;

	// 'u' will be a (vector-valued) multi-function defined on the torus
	// the jump of this multi-function (difference between sheets)
	// is unknown beforehand
	Function::Jump jump_of_x = x_torus .jump(), jump_of_y = y_torus .jump();

	// impose equilibrium equation
	// run over all triangular cells composing 'torus'
	{  // just a block of code for hiding 'it'
	Mesh::Iterator it = torus .iterator ( tag::over_cells_of_max_dim );
	for ( it .reset(); it .in_range(); it++ )
	{	Cell small_tri = *it;
		fe .dock_on ( small_tri, tag::winding );
		// run twice over the three vertices of 'small_tri'
		Mesh::Iterator it_V = small_tri .boundary() .iterator
			( tag::over_vertices, tag::require_order );
		for ( it_V .reset(); it_V .in_range(); it_V ++ )
		{	Cell V = * it_V;   // the test function is psi_V
			Cell seg = small_tri .boundary() .cell_in_front_of (V);
			Cell W = V;
			Function psi_V = fe .basis_function (V),
			         d_psiV_dx = psi_V .deriv ( x_torus ),
			         d_psiV_dy = psi_V .deriv ( y_torus );
			Manifold::Action winding_V_W = 0; // winding from V to W
			while ( true )
			{	assert ( W == seg .base() .reverse() );
				// V may be the same as W, no problem about that
				Function psi_W = fe .basis_function (W),
				         d_psiW_dx = psi_W .deriv ( x_torus ),
				         d_psiW_dy = psi_W .deriv ( y_torus );
				// 'fe' is already docked on 'small_tri' so this will be the domain of integration
				double int_d_psiW_dx_d_psiV_dx = fe .integrate ( d_psiW_dx * d_psiV_dx );
				double int_d_psiW_dx_d_psiV_dy = fe .integrate ( d_psiW_dx * d_psiV_dy );
				double int_d_psiW_dy_d_psiV_dx = fe .integrate ( d_psiW_dy * d_psiV_dx );
				double int_d_psiW_dy_d_psiV_dy = fe .integrate ( d_psiW_dy * d_psiV_dy );
				// tag::Util::Tensor is a class defined in maniFEM for arbitrary order tensors
				tag::Util::Tensor < double > energy (2,2);
				for ( size_t i = 0; i < 2; i ++ )
				for ( size_t k = 0; k < 2; k ++ )
					energy (i,k) = Hooke (i,0,k,0) * int_d_psiW_dx_d_psiV_dx +
					               Hooke (i,0,k,1) * int_d_psiW_dy_d_psiV_dx +
					               Hooke (i,1,k,0) * int_d_psiW_dx_d_psiV_dy +
					               Hooke (i,1,k,1) * int_d_psiW_dy_d_psiV_dy;
				matrix_A .coeffRef ( 2 * numbering [V],     2 * numbering [W]     ) += energy (0,0);
				matrix_A .coeffRef ( 2 * numbering [V],     2 * numbering [W] + 1 ) += energy (0,1);
				matrix_A .coeffRef ( 2 * numbering [V] + 1, 2 * numbering [W]     ) += energy (1,0);
				matrix_A .coeffRef ( 2 * numbering [V] + 1, 2 * numbering [W] + 1 ) += energy (1,1);
				matrix_A .coeffRef ( 2 * numbering [V], 2 * number_dofs ) +=   //  strain 00
					jump_of_x ( winding_V_W ) * energy (0,0);
				matrix_A .coeffRef ( 2*numbering[V], 2 * number_dofs + 1 ) +=   //  strain 01
					jump_of_y ( winding_V_W ) * energy (0,0);
				matrix_A .coeffRef ( 2*numbering[V], 2 * number_dofs + 2 ) +=   //  strain 10
					jump_of_x ( winding_V_W ) * energy (0,1);
				matrix_A .coeffRef ( 2*numbering[V], 2 * number_dofs + 3 ) +=   //  strain 11
					jump_of_y ( winding_V_W ) * energy (0,1);
				matrix_A .coeffRef ( 2*numbering[V]+1, 2 * number_dofs     ) +=   //  strain 00
					jump_of_x ( winding_V_W ) * energy (1,0);
				matrix_A .coeffRef ( 2*numbering[V]+1, 2 * number_dofs + 1 ) +=   //  strain 01
					jump_of_y ( winding_V_W ) * energy (1,0);
				matrix_A .coeffRef ( 2*numbering[V]+1, 2 * number_dofs + 2 ) +=   //  strain 10
					jump_of_x ( winding_V_W ) * energy (1,1);
				matrix_A .coeffRef ( 2*numbering[V]+1, 2 * number_dofs + 3 ) +=   //  strain 11
					jump_of_y ( winding_V_W ) * energy (1,1);
				winding_V_W += seg .winding() ;
				W = seg .tip();
				if ( V == W ) break;
				seg = small_tri .boundary() .cell_in_front_of (W);
			}  // end of loop in W
			// here winding_V_W should be zero again
			assert ( winding_V_W == 0 );
		}  }  // end of loop in V, end of loop in small_tri
	}  // just a block of code for hiding 'it'

	// impose the last four equations, relating the unknown 'macro_strain' to the given 'macro_stress'
	// second line in equation (23) in the paper referred at the beginning of this file
	// run over all triangular cells composing 'torus'
	{  // just a block of code for hiding 'it'
	Mesh::Iterator it = torus .iterator ( tag::over_cells_of_max_dim );
	for ( it .reset(); it .in_range(); it++ )
	{	Cell small_tri = *it;
		fe .dock_on ( small_tri, tag::winding );
		// run over the three vertices of 'small_tri'
		Mesh::Iterator it_V = small_tri .boundary() .iterator
			( tag::over_vertices, tag::require_order );
		Manifold::Action winding_V = 0; // winding from first V to current V
		for ( it_V .reset(); it_V .in_range(); it_V ++ )
		{	Cell V = * it_V;
			Cell seg = small_tri .boundary() .cell_in_front_of (V);
			Function psi_V = fe .basis_function ( V ),
			         d_psiV_dx = psi_V .deriv ( x_torus ),
			         d_psiV_dy = psi_V .deriv ( y_torus );
			double int_psi_x = fe .integrate ( d_psiV_dx ),
			       int_psi_y = fe .integrate ( d_psiV_dy );
			for ( size_t i = 0; i < 2; i++ )
			for ( size_t j = 0; j < 2; j++ )
			for ( size_t k = 0; k < 2; k++ )
			{	double integral = Hooke (i,j,k,0) * int_psi_x + Hooke (i,j,k,1) * int_psi_y;
				matrix_A .coeffRef ( 2 * number_dofs + 2*i + j, 2 * numbering [V] + k ) += integral;
				matrix_A .coeffRef ( 2 * number_dofs + 2*i + j, 2 * number_dofs + 2*k ) +=
					integral * jump_of_x ( winding_V );
				matrix_A .coeffRef ( 2 * number_dofs + 2*i + j, 2 * number_dofs + 2*k + 1 ) +=
					integral * jump_of_y ( winding_V );                                                }
			winding_V += seg .winding() ;
		}  // // end of loop in V
		// here winding_V should be zero again
		assert ( winding_V == 0 );
	}  // end of loop in small_tri
	}  // just a block of code for hiding 'it'

	for ( size_t i = 0; i < 2; i++ )
	for ( size_t j = 0; j < 2; j++ )
		vector_b ( 2*number_dofs + 2*i+j ) = areaY * macro_stress (i,j);
		// 'macro_stress' is called 'S' in second line in equation (23)
		// in the paper referred at the beginning of this file
	
	std::cout << "now solving the system of linear equations" << std::endl;
	
	matrix_A .makeCompressed();

	Eigen::SparseQR < Eigen::SparseMatrix<double>, Eigen::COLAMDOrdering<int> > solver;

	solver .compute ( matrix_A );
	if ( solver .info() != Eigen::Success )
	{	std::cout << "Eigen solver.compute failed" << std::endl;
		exit ( 0 );                                              }

	Eigen::VectorXd vector_sol = solver .solve ( vector_b );
	if ( solver .info() != Eigen::Success )
	{	std::cout << "Eigen solver.solve failed" << std::endl;
		exit (1);                                               }

	// recover the macroscopic strain matrix from the last 4 entries of 'vector_sol'
	// 'macro_strain' is called 'A' in the paper referred at the beginning of this file
	// tag::Util::Tensor is a class defined in maniFEM for arbitrary order tensors
	tag::Util::Tensor < double > macro_strain (2,2);
	for ( size_t i = 0; i < 2; i++ )
	for ( size_t j = 0; j < 2; j++ )
		macro_strain (i,j) = vector_sol ( 2*number_dofs + 2*i+j );
	
	cout << "macro stress " << macro_stress (0,0) << " " << macro_stress (0,1) << " "
	                        << macro_stress (1,0) << " " << macro_stress (1,1) << " " << endl;
	cout << "macro strain " << macro_strain (0,0) << " " << macro_strain (0,1) << " "
	                        << macro_strain (1,0) << " " << macro_strain (1,1) << " " << endl;

	// we average the off-diagonal entries of 'macro_strain'
	double deviation = 0.5 * (   vector_sol ( 2*number_dofs + 1 ) // -
                             - vector_sol ( 2*number_dofs + 2 ) );
	vector_sol ( 2*number_dofs + 1 ) -= deviation;
	vector_sol ( 2*number_dofs + 2 ) += deviation;
	
	RR2 .set_as_working_manifold();  // recall 'xy' are the coordinates on RR2

	// run over all vertices of 'torus'
	{  // just a block of code for hiding 'it'
	Mesh::Iterator it = torus .iterator ( tag::over_vertices );
	for ( it .reset(); it .in_range(); it++ )
	{	Cell V = *it;
		vector_sol [ 2 * numbering [V]     ] -= deviation * y(V);
		vector_sol [ 2 * numbering [V] + 1 ] += deviation * x(V);  }	
	}  // just a block of code for hiding 'it'
	
	for ( size_t i = 0; i < 2; i++ )
	for ( size_t j = 0; j < 2; j++ )
		macro_strain (i,j) = vector_sol ( 2 * number_dofs + 2*i + j );
	
	cout << "macro strain " << macro_strain (0,0) << " " << macro_strain (0,1) << " "
	                        << macro_strain (1,0) << " " << macro_strain (1,1) << " " << endl;

	// we extend the solution to all vertices of 'square'	(those not belonging to 'torus')
	// it is easier to impose the zero average condition on 'square'
	// it is also easier to export_to_file
	
	{ // just a block of code for hiding variables
	size_t j = numbering [B];
	assert ( not A .belongs_to ( torus ) );
	vector_sol [ 2 * numbering [A] ] = vector_sol [ 2*j ]
		+ macro_strain (0,0) * ( x(A) - x(B) )
		+ macro_strain (0,1) * ( y(A) - y(B) );
	vector_sol [ 2 * numbering [A] + 1 ] =  vector_sol [ 2*j+1 ]
		+ macro_strain (1,0) * ( x(A) - x(B) )
		+ macro_strain (1,1) * ( y(A) - y(B) );
	assert ( not C .belongs_to ( torus ) );
	vector_sol [ 2 * numbering [C] ] = vector_sol [ 2*j ]
		+ macro_strain (0,0) * ( x(C) - x(B) )
		+ macro_strain (0,1) * ( y(C) - y(B) );
	vector_sol [ 2 * numbering [C] + 1 ] =  vector_sol [ 2*j+1 ]
		+ macro_strain (1,0) * ( x(C) - x(B) )
		+ macro_strain (1,1) * ( y(C) - y(B) );
	assert ( not D .belongs_to ( torus ) );
	vector_sol [ 2 * numbering [D] ] = vector_sol [ 2*j ]
		+ macro_strain (0,0) * ( x(D) - x(B) )
		+ macro_strain (0,1) * ( y(D) - y(B) );
	vector_sol [ 2 * numbering [D] + 1 ] =  vector_sol [ 2*j+1 ]
		+ macro_strain (1,0) * ( x(D) - x(B) )
		+ macro_strain (1,1) * ( y(D) - y(B) );
	Mesh::Iterator it_AB = AB .iterator ( tag::over_vertices, tag::require_order );
	Mesh::Iterator it_CD = CD .iterator ( tag::over_vertices, tag::backwards );
	it_AB .reset();  assert ( it_AB .in_range() );
	assert ( *it_AB == A );  it_AB ++;
	it_CD .reset();  assert ( it_CD .in_range() );
	assert ( *it_CD == D );  it_CD ++;
	for ( ; ; it_AB ++, it_CD ++ )
	{	assert ( it_AB .in_range() );  assert ( it_CD .in_range() );
		Cell V = *it_AB, W = *it_CD;
		if ( V == B )  {  assert ( W == C );  break;  }
		assert ( not W .belongs_to ( torus ) );
		j = numbering [ V ];
		vector_sol [ 2 * numbering [W] ] = vector_sol [ 2*j ]
			+ macro_strain (0,0) * ( x(W) - x(V) )
			+ macro_strain (0,1) * ( y(W) - y(V) );
		vector_sol [ 2 * numbering [W] + 1 ] =  vector_sol [ 2*j+1 ]
			+ macro_strain (1,0) * ( x(W) - x(V) )
			+ macro_strain (1,1) * ( y(W) - y(V) );                    }
	Mesh::Iterator it_BC = BC .iterator ( tag::over_vertices, tag::require_order );
	Mesh::Iterator it_DA = DA .iterator ( tag::over_vertices, tag::backwards );
	it_BC .reset();  assert ( it_BC .in_range() );
	assert ( *it_BC == B );  it_BC ++;
	it_DA .reset();  assert ( it_DA .in_range() );
	assert ( *it_DA == A );  it_DA ++;
	for ( ; ; it_BC ++, it_DA ++ )
	{	assert ( it_BC .in_range() );  assert ( it_DA .in_range() );
		Cell V = *it_BC, W = *it_DA;
		if ( V == C )  {  assert ( W == D );  break;  }
		assert ( not W .belongs_to ( torus ) );
		j = numbering [ V ];
		vector_sol [ 2 * numbering [W] ] = vector_sol [ 2*j ]
			+ macro_strain(0,0) * ( x(W) - x(V) )
			+ macro_strain(0,1) * ( y(W) - y(V) );
		vector_sol [ 2 * numbering [W] + 1 ] =  vector_sol [ 2*j+1 ]
			+ macro_strain(1,0) * ( x(W) - x(V) )
			+ macro_strain(1,1) * ( y(W) - y(V) );                     }
	} // just a block of code

	// impose sum u_x = 0, sum u_y = 0
	{ // just a block of code for hiding variables
	double sum_x = 0., sum_y = 0.;
	for ( size_t i = 0; i < number_dofs; i ++ )
	{	sum_x += vector_sol [ 2*i ];
		sum_y += vector_sol [ 2*i+1 ];  }
	sum_x /= number_dofs;
	sum_y /= number_dofs;
	for ( size_t i = 0; i < number_dofs; i ++ )
	{	vector_sol [ 2*i ] -= sum_x;
		vector_sol [ 2*i+1 ] -= sum_y;  }	
	} // just a block of code
	
	square .export_to_file ( tag::gmsh, "cell-stress.msh", numbering );

	{ // just a block of code for hiding variables
	std::ofstream solution_file ("cell-stress.msh", std::fstream::app );
	solution_file << "$NodeData" << std::endl;
	solution_file << "1" << std::endl;   // one string follows
	solution_file << "\"elastic displacement\"" << std::endl;
	solution_file << "1" << std::endl;   //  one real follows
	solution_file << "0.0" << std::endl;  // time [??]
	solution_file << "3" << std::endl;   // three integers follow
	solution_file << "0" << std::endl;   // time step [??]
	solution_file << "3" << std::endl;  // scalar values of u
	solution_file << square .number_of ( tag::vertices ) << std::endl;
	// number of values listed below
	Mesh::Iterator it = square .iterator ( tag::over_vertices );
	for ( it .reset(); it .in_range(); it++ )
	{	Cell P = *it;
		const size_t j = numbering [P];
		solution_file << j + 1 << " " << vector_sol [ 2*j ] << " "
		                              << vector_sol [ 2*j+1 ] << " 0. "<< std::endl;  }
	} // just a block of code

	std::cout << "produced file cell-stress.msh" << std::endl;	
							
}  //  end of 'main'


//-----------------------------------------------------------------------------------------


