
// example presented in paragraph 3.29 of the manual
// https://maniFEM.rd.ciencias.ulisboa.pt/manual-maniFEM.pdf
// anisotropic mesh, inner spectral radius

#include "maniFEM.h"

using namespace maniFEM;


int main ( )

{	Manifold RR2 ( tag::Euclid, tag::of_dim, 2 );
	Function xy = RR2 .build_coordinate_system ( tag::Lagrange, tag::of_degree, 1 );
	Function x = xy[0], y = xy[1];

	Function alpha = 5. + 10. * ( (x+0.3)*(x+0.3) + (y-0.9)*(y-0.9) );
	Function beta = ( 1. + 5. / alpha ) / alpha;
	RR2 .set_metric ( tag::inner_spectral_radius, 1.,
	                  tag::m11_m12_m22, 2. * beta, -6. * beta, 18. * beta );
	// implicit manifolds 'circle' and 'ellipse' will inherit this metric

	Manifold circle = RR2 .implicit ( x*x + y*y == 1. );
	Mesh outer = Mesh::Build ( tag::frontal ) .entire_manifold() .desired_length ( 0.1 );

	double y0 = 0.36;
	Manifold ellipse = RR2 .implicit ( x*x + (y-y0)*(y-y0) + 0.3*x*y == 0.25 );
	Mesh inner = Mesh::Build ( tag::frontal ) .entire_manifold() .desired_length ( 0.1 );

	Mesh circles = Mesh::Build ( tag::join ) .meshes ( { outer, inner .reverse() } );

	RR2 .set_as_working_manifold();
	Mesh disk = Mesh::Build ( tag::frontal ) .boundary ( circles ) .desired_length ( 0.1 );

	disk .export_to_file ( tag::gmsh, "disk.msh" );
	disk .export_to_file ( tag::eps, "disk.eps" );
	std::cout << "produced files disk.msh and disk.eps" << std::endl;

}  // end of main
