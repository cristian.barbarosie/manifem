
// build a shoe-like shape

#include "maniFEM.h"

using namespace maniFEM;

int main ()

{	Manifold RR3 ( tag::Euclid, tag::of_dim, 3 );
	Function xyz = RR3 .build_coordinate_system ( tag::Lagrange, tag::of_degree, 1 );
	Function x = xyz [0], y = xyz [1], z = xyz [2];

	const double seg_len = 0.03;

	// we start in the plane Oxy, with an ellipse, then we add a perturbation
	Manifold RR2 = RR3 .implicit ( z == 0. );
	double x0 = -0.04;
	Manifold contour_manif = RR2 .implicit ( x*x + 3.*y*y + 1.7/((x-x0)*(x-x0)+1.) == 2. );
	Cell heel ( tag::vertex, tag::of_coords, { -1., 0., 0. }, tag::project );
	Mesh contour ( tag::frontal, tag::start_at, heel, tag::towards, { 0., -1., 0. },
	               tag::desired_length, seg_len                                     );

	// we mesh the lower face of the sole which has no hole
	RR2 .set_as_working_manifold();
	Mesh down_sole ( tag::frontal, tag::boundary, contour .reverse(), tag::start_at, heel,
	                 tag::towards, { 1., 0., 0. }, tag::desired_length, seg_len           );

	// build 'circle' which is actually two segments plus a semi-circle
	double cx = 0.0;
	Cell A ( tag::vertex, tag::of_coords, { cx,  0.2, 0. } );
	Cell B ( tag::vertex, tag::of_coords, { cx,  0.,  0. } );
	Cell C ( tag::vertex, tag::of_coords, { cx, -0.2, 0. } );
	Mesh AB ( tag::segment, A .reverse(), B, tag::divided_in, int(0.2/seg_len) );
	Mesh BC ( tag::segment, B .reverse(), C, tag::divided_in, int(0.2/seg_len) );
	Manifold circle_manif = RR2 .implicit ( 0.3*(x-cx)*(x-cx) + y*y == 0.04 );
	Mesh CA ( tag::frontal, tag::start_at, C, tag::towards, { 1., 0., 0. },
	          tag::stop_at, A, tag::desired_length, seg_len                );
	Mesh circle ( tag::join, AB, BC, CA );

	// 'up_sole_perf' (upper face of the sole) has a hole
	Mesh two_curves ( tag::join, contour, circle .reverse() );
	RR2 .set_as_working_manifold();
	Mesh half_disk ( tag::frontal, tag::boundary, circle, tag::start_at, B,
	                 tag::towards, { 1., 0., 0. }, tag::desired_length, seg_len );
	Mesh up_sole_perf ( tag::frontal, tag::boundary, two_curves, tag::start_at, heel,
	                    tag::towards, { 1., 0., 0. }, tag::desired_length, seg_len   );

	// upper face of the sole without hole
	Mesh up_sole ( tag::join, up_sole_perf, half_disk );

	Mesh sole ( tag::join, up_sole, down_sole );

	// build two beams of height h
	RR3 .set_as_working_manifold();
	const double h = 0.07;
	Cell D ( tag::vertex, tag::of_coords, { -1.2,  0.2, 0. } );
	Cell E ( tag::vertex, tag::of_coords, { -1.2,  0.,  0. } );
	Cell F ( tag::vertex, tag::of_coords, { -1.2,  0.,  0. } );
	Cell G ( tag::vertex, tag::of_coords, { -1.2, -0.2, 0. } );
	Mesh AD ( tag::segment, A .reverse(), D, tag::divided_in, int((1.2-cx)/seg_len) );
	Mesh BE ( tag::segment, B .reverse(), E, tag::divided_in, int((1.2-cx)/seg_len) );
	Mesh BF ( tag::segment, B .reverse(), F, tag::divided_in, int((1.2-cx)/seg_len) );
	Mesh EB ( tag::segment, E .reverse(), B, tag::divided_in, int((1.2-cx)/seg_len) );
	Mesh CG ( tag::segment, C .reverse(), G, tag::divided_in, int((1.2-cx)/seg_len) );
	Mesh DE ( tag::segment, D .reverse(), E, tag::divided_in, int(0.2/seg_len) );
	Mesh FG ( tag::segment, F .reverse(), G, tag::divided_in, int(0.2/seg_len) );
	Mesh ADEB ( tag::rectangle, AD, DE, EB, AB .reverse(), tag::with_triangles );
	Mesh BFGC ( tag::rectangle, BF, FG, CG .reverse(), BC .reverse(), tag::with_triangles );

	Cell As ( tag::vertex, tag::of_coords, { cx,  0.2, h } );
	Cell Bs ( tag::vertex, tag::of_coords, { cx,  0.,  h } );
	Cell Cs ( tag::vertex, tag::of_coords, { cx, -0.2, h } );
	Mesh ABs ( tag::segment, As .reverse(), Bs, tag::divided_in, int(0.2/seg_len) );
	Mesh BCs ( tag::segment, Bs .reverse(), Cs, tag::divided_in, int(0.2/seg_len) );
	Mesh AAs ( tag::segment, A .reverse(), As, tag::divided_in, int(h/seg_len) );
	Mesh BBs ( tag::segment, B .reverse(), Bs, tag::divided_in, int(h/seg_len) );
	Mesh CCs ( tag::segment, C .reverse(), Cs, tag::divided_in, int(h/seg_len) );
	Mesh ABBsAs ( tag::rectangle, AB, BBs, ABs .reverse(), AAs .reverse(), tag::with_triangles );
	Mesh BCCsBs ( tag::rectangle, BC, CCs, BCs .reverse(), BBs .reverse(), tag::with_triangles );
	
	Cell Ds ( tag::vertex, tag::of_coords, { -1.2,  0.2, h } );
	Cell Es ( tag::vertex, tag::of_coords, { -1.2,  0.,  h } );
	Cell Fs ( tag::vertex, tag::of_coords, { -1.2,  0.,  h } );
	Cell Gs ( tag::vertex, tag::of_coords, { -1.2, -0.2, h } );
	Mesh ADs ( tag::segment, As .reverse(), Ds, tag::divided_in, int((1.2-cx)/seg_len) );
	Mesh BEs ( tag::segment, Bs .reverse(), Es, tag::divided_in, int((1.2-cx)/seg_len) );
	Mesh BFs ( tag::segment, Bs .reverse(), Fs, tag::divided_in, int((1.2-cx)/seg_len) );
	Mesh EBs ( tag::segment, Es .reverse(), Bs, tag::divided_in, int((1.2-cx)/seg_len) );
	Mesh CGs ( tag::segment, Cs .reverse(), Gs, tag::divided_in, int((1.2-cx)/seg_len) );
	Mesh DEs ( tag::segment, Ds .reverse(), Es, tag::divided_in, int(0.2/seg_len) );
	Mesh FGs ( tag::segment, Fs .reverse(), Gs, tag::divided_in, int(0.2/seg_len) );
	Mesh ADEBs ( tag::rectangle, ADs, DEs, EBs, ABs .reverse(), tag::with_triangles );
	Mesh BFGCs ( tag::rectangle, BFs, FGs, CGs .reverse(), BCs .reverse(), tag::with_triangles );

	Mesh DDs ( tag::segment, D .reverse(), Ds, tag::divided_in, int(h/seg_len) );
	Mesh EEs ( tag::segment, E .reverse(), Es, tag::divided_in, int(h/seg_len) );
	Mesh FFs ( tag::segment, F .reverse(), Fs, tag::divided_in, int(h/seg_len) );
	Mesh GGs ( tag::segment, G .reverse(), Gs, tag::divided_in, int(h/seg_len) );
	Mesh ADDA ( tag::rectangle, AD, DDs, ADs .reverse(), AAs .reverse(), tag::with_triangles );
	Mesh DEED ( tag::rectangle, DE, EEs, DEs .reverse(), DDs .reverse(), tag::with_triangles );
	Mesh EBBE ( tag::rectangle, EB, BBs, EBs .reverse(), EEs .reverse(), tag::with_triangles );
	Mesh ABBA ( tag::rectangle, AB .reverse(), AAs, ABs, BBs .reverse(), tag::with_triangles );
	Mesh BFFB ( tag::rectangle, BF, FFs, BFs .reverse(), BBs .reverse(), tag::with_triangles );
	Mesh FGGF ( tag::rectangle, FG, GGs, FGs .reverse(), FFs .reverse(), tag::with_triangles );
	Mesh GCCG ( tag::rectangle, CG .reverse(), CCs, CGs, GGs .reverse(), tag::with_triangles );
	Mesh BCCB ( tag::rectangle, BC .reverse(), BBs, BCs, CCs .reverse(), tag::with_triangles );

	Mesh beam_A ( tag::join, { ADEBs, ADDA, DEED, EBBE, ADEB .reverse(), ABBsAs .reverse() } );
	Mesh beam_C ( tag::join, { BFGCs, BFFB, FGGF, GCCG, BFGC .reverse(), BCCsBs .reverse() } );

	// we place ourselves in another horizontal plane, above Oxy
	RR2 = RR3 .implicit ( z == h );
	// build another (smaller) half_disk
	circle_manif = RR2 .implicit ( (0.3+3.5*z)*(x-cx)*(x-cx) + y*y == 0.04 );
	Mesh CAs ( tag::frontal, tag::start_at, Cs, tag::towards, { 1., 0., 0. },
	           tag::stop_at, As, tag::desired_length, seg_len                );
	Mesh circle_s ( tag::join, ABs, BCs, CAs );
	RR2 .set_as_working_manifold();
	Mesh half_disk_s ( tag::frontal, tag::boundary, circle_s, tag::start_at, Bs,
	                   tag::towards, { 1., 0., 0. }, tag::desired_length, seg_len );

	// 'ACCA' will define the shape of 'button'
	RR3 .implicit ( (0.3+3.5*z)*(x-cx)*(x-cx) + y*y == 0.04 );
	Mesh bdry ( tag::join, CA, AAs, CAs .reverse(), CCs .reverse() );
	Mesh ACCA ( tag::frontal, tag::boundary, bdry, tag::start_at, A,
	            tag::towards, { 1., 0., 1. }, tag::desired_length, seg_len );

	// 'button' is a small piece, having the shape of a half-disk, having thickness 'h'
	// it joins the sole to the two beams
	RR3 .set_as_working_manifold();
	Mesh button ( tag::join, { ABBsAs, BCCsBs, half_disk .reverse(), half_disk_s, ACCA } );
		
	std::cout << "meshing beam_A with tetrahedra" << std::endl;
	Mesh beam_A_full ( tag::frontal, tag::boundary, beam_A, tag::desired_length, seg_len );
	std::cout << "meshing beam_C with tetrahedra" << std::endl;
	Mesh beam_C_full ( tag::frontal, tag::boundary, beam_C, tag::desired_length, seg_len );

	double zc = h/2.;
	double yc = 0.1;
	const double pi = 3.1415926536;
	Mesh::Iterator it_beam_A = beam_A_full .iterator ( tag::over_vertices );
	for ( it_beam_A .reset(); it_beam_A .in_range(); it_beam_A ++ )
	{	Cell V = * it_beam_A;
		const double xV = x(V), yV = y(V), zV = z(V);
		double theta = -xV / 1. * pi/2.;
		if ( xV < -1. )  theta = pi/2.;
		y(V) = yc + (yV-yc) * std::cos(theta) + (zV-zc) * std::sin(theta);
		z(V) = zc - (yV-yc) * std::sin(theta) + (zV-zc) * std::cos(theta);  }
	for ( it_beam_A .reset(); it_beam_A .in_range(); it_beam_A ++ )
	{	Cell V = * it_beam_A;
		const double xV = x(V), zV = z(V);
		double xc = xV / 2.3;
		double theta = -xV / 1. * 0.25*pi;
		x(V) = xc + (xV-xc) * std::cos(theta) + (zV-zc) * std::sin(theta);
		z(V) = zc - (xV-xc) * std::sin(theta) + (zV-zc) * std::cos(theta);  }

	yc = -0.1;
	Mesh::Iterator it_beam_C = beam_C_full .iterator ( tag::over_vertices );
	for ( it_beam_C .reset(); it_beam_C .in_range(); it_beam_C ++ )
	{	Cell V = * it_beam_C;
		const double xV = x(V), yV = y(V), zV = z(V);
		double theta = -xV / 1. * pi/2.;
		if ( xV < -1. )  theta = pi/2.;
		y(V) = yc + (yV-yc) * std::cos(theta) - (zV-zc) * std::sin(theta);
		z(V) = zc + (yV-yc) * std::sin(theta) + (zV-zc) * std::cos(theta);  }
	for ( it_beam_C .reset(); it_beam_C .in_range(); it_beam_C ++ )
	{	Cell V = * it_beam_C;
		const double xV = x(V), zV = z(V);
		double xc = xV / 2.3;
		double theta = -xV / 1. * 0.25*pi;
		x(V) = xc + (xV-xc) * std::cos(theta) + (zV-zc) * std::sin(theta);
		z(V) = zc - (xV-xc) * std::sin(theta) + (zV-zc) * std::cos(theta);  }

	Mesh::Iterator it_up_sole = up_sole .iterator ( tag::over_vertices );
	for ( it_up_sole .reset(); it_up_sole .in_range(); it_up_sole ++ )
	{	Cell V = *it_up_sole;
		if ( V .belongs_to ( contour, tag::cell_has_low_dim ) )  continue;
		z(V) = 0.1;                                                         }

	Mesh::Iterator it_up_sole_perf = up_sole_perf .iterator ( tag::over_vertices );
	for ( size_t i = 0; i < 15; i++ )
	for ( it_up_sole_perf .reset(); it_up_sole_perf .in_range(); it_up_sole_perf ++ )
	{	Cell V = *it_up_sole_perf;
		if ( V .belongs_to ( two_curves, tag::cell_has_low_dim ) )  continue;
		up_sole_perf .barycenter (V);                                               }

	Mesh::Iterator it_down_sole = down_sole .iterator ( tag::over_vertices );
	for ( it_down_sole .reset(); it_down_sole .in_range(); it_down_sole ++ )
	{	Cell V = *it_down_sole;
		if ( V .belongs_to ( contour, tag::cell_has_low_dim ) )  continue;
		z(V) = -0.03;                                                        }

	for ( size_t i = 0; i < 15; i++ )
	for ( it_down_sole .reset(); it_down_sole .in_range(); it_down_sole ++ )
	{	Cell V = *it_down_sole;
		if ( V .belongs_to ( contour, tag::cell_has_low_dim ) )  continue;
		down_sole .barycenter (V);                                          }

	double n = circle .number_of ( tag::vertices );
	double sum_xi = 0., sum_xi2 = 0., sum_zi = 0., sum_xizi = 0.;
	Mesh::Iterator it_sole = sole .iterator ( tag::over_vertices );
	for ( it_sole .reset(); it_sole .in_range(); it_sole ++ )
	{	Cell V = *it_sole;
		// if ( V .belongs_to ( disk, tag::cell_has_low_dim ) )  continue;
		double xV = x(V);
		const double delta_z = 0.4*xV*xV;
		if ( xV > 0. )  z(V) += delta_z;
		if ( xV < 0. )  z(V) += 6./(2.2+xV) - 6./2.2 + 1.2*xV - 1.3*xV*xV;
		if ( V .belongs_to ( circle, tag::cell_has_low_dim ) )
		{	assert ( xV >= 0. );
			sum_xi += xV;
			sum_xi2 += xV*xV;
			sum_zi += delta_z;
			sum_xizi += xV*delta_z;    }                                     }
	const double det = n * sum_xi2 - sum_xi * sum_xi;
	double alpha = ( n * sum_xizi - sum_xi * sum_zi ) / det;
	double beta = ( sum_zi * sum_xi2 - sum_xizi * sum_xi ) / det;
	alpha *= 0.9;
	beta += 0.11;

	for ( it_sole .reset(); it_sole .in_range(); it_sole ++ )
	{	Cell V = * it_sole;
		z(V) -= alpha * x(V) + beta;                                       }
	
	beam_A .export_to_file ( tag::gmsh, "beam-A.msh" );
	beam_C .export_to_file ( tag::gmsh, "beam-C.msh" );
	button .export_to_file ( tag::gmsh, "button.msh" );
	sole .export_to_file ( tag::gmsh, "sole.msh" );

	std::cout << "meshing button with tetrahedra" << std::endl;
	Mesh button_full ( tag::frontal, tag::boundary, button, tag::desired_length, seg_len );
	std::cout << "meshing sole with tetrahedra" << std::endl;
	Mesh sole_full ( tag::frontal, tag::boundary, sole, tag::desired_length, seg_len );

	std::cout << "saving composed mesh" << std::endl;
	Mesh::Composite prosthesis_comp;
	prosthesis_comp .add_mesh ( "beam-A", beam_A_full );
	prosthesis_comp .add_mesh ( "beam-C", beam_C_full );
	prosthesis_comp .add_mesh ( "sole", sole_full );
	prosthesis_comp .add_mesh ( "button", button_full );
	prosthesis_comp .add_mesh ( "lower-face", down_sole );
	prosthesis_comp .add_mesh ( "DEED", DEED );	
	prosthesis_comp .add_mesh ( "FGGF", FGGF );
	prosthesis_comp .export_to_file ( tag::gmsh, "prosthesis-comp.msh" );
								  
}  // end of main
