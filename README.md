# maniFEM
[ManiFEM](https://maniFEM.rd.ciencias.ulisboa.pt) is a C++ library for generating meshes 
and solving partial differential equations through the finite element method.
The name comes from "finite elements on manifolds". 
ManiFEM was designed with the goal of coping with very general meshes,
in particular meshes on Riemannian manifolds, including quotient manifolds
like the [flat torus](https://en.wikipedia.org/wiki/Torus#Flat_torus).
Although most constructors build two- or three-dimensional meshes,
the internal implementation of cells and meshes allows for any topological dimension.
Also, maniFEM was written with the goal of being conceptually clear and easy to read.
We hope it will be particularly useful for people who want fine control over the mesh, 
e.g. for implementing their own meshing or remeshing algorithms.

ManiFEM is just a collection of C++ classes.
It has no user-friendly interface nor graphic capabilities. 
The user should have some understanding of programming and of C++. 
However, maniFEM can be used at a basic level by people with no deep knowledge of C++.
Have a look at the [gallery](https://maniFEM.rd.ciencias.ulisboa.pt/gallery/).	
Similar products (competitors), as well as strong and weak points of maniFEM,
are listed in the [manual](https://maniFEM.rd.ciencias.ulisboa.pt/manual-maniFEM.pdf),
at the end of section 1.

In its current version, [release 25.02](https://codeberg.org/cristian.barbarosie/maniFEM/releases),
maniFEM works well for mesh generation, including meshes on quotient manifolds.
The user can control variations in the element size; anisotropic meshes can also be built.
Lagrange finite elements of degree one and two are implemented for triangular and quadrangular cells;
Lagrange finite elements of degree one are implemented for tetrahedral and hexahedral cells;
many other types of finite elements are still to be implemented.
Variational formulations are implemented as C++ objects; they allow for compact and elegant code.
A changelog is available at the end of the
[manual](https://maniFEM.rd.ciencias.ulisboa.pt/manual-maniFEM.pdf), just before the index.
To check which version of maniFEM is installed in your computer, see at the beginning
of the file `maniFEM.h`.

ManiFEM is being developed by [Cristian Barbarosie](mailto:cristian.barbarosie@gmail.com)
and [Anca-Maria Toader](mailto:anca.maria.toader@gmail.com);
see also the [list of contributors](https://codeberg.org/cristian.barbarosie/maniFEM/src/branch/main/misc/contributors.md)
and the [colophon](https://codeberg.org/cristian.barbarosie/maniFEM/src/branch/main/misc/colophon.md).

To learn maniFEM, you should read the
[manual](https://maniFEM.rd.ciencias.ulisboa.pt/manual-maniFEM.pdf) (version 25.02).

To use maniFEM, choose a [release](https://codeberg.org/cristian.barbarosie/maniFEM/releases)
and download all files to some directory in your computer.
Current code might be unstable; releases are stable.
You can then run the examples in the manual: just `make run-parag-1.1` for the example in paragraph 1.1, 
`make run-parag-2.16` for the example in paragraph 2.16, and so on.
You will need a recent C++ compiler (we use `g++` from the
[GNU Compiler Collection](https://gcc.gnu.org/)) and the `make` utility. 
Under linux it should be easy to install them. 
It is not that easy to install and use them under Windows, but it is certainly possible,
for instance by using [cygwin](https://cygwin.org).
You may want to use [gmsh](http://gmsh.info/) for visualization purposes. 

Finite element computations are disabled by default in maniFEM.
To enable finite element computations you must comment out (or delete) the line containing
`-DMANIFEM_NO_FEM` in the `Makefile`.
You must also install the [Eigen](http://eigen.tuxfamily.org/) library; 
just copy its source tree somewhere in your computer and assign that path to a variable named
`EIGEN_DIR` in the `Makefile`.
Paragraphs 12.14 and 12.15 in the [manual](https://maniFEM.rd.ciencias.ulisboa.pt/manual-maniFEM.pdf)
give more details.

A component of maniFEM, [MetricTree](https://codeberg.org/cristian.barbarosie/MetricTree),
can be used independently.

------------

This work is supported by national funding from FCT - _Fundação para a Ciência e a Tecnologia_
(Portugal), through _Faculdade de Ciências da Universidade de Lisboa_ and 
_Centro de Matemática, Aplicações Fundamentais e Investigação Operacional_,
project UIDB/04561/2020.

------------

Copyright 2019 - 2025 Cristian Barbarosie cristian.barbarosie@gmail.com

ManiFEM is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

ManiFEM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

Full text of the GNU Lesser General Public License is available 
in files [src/COPYING](https://codeberg.org/cristian.barbarosie/maniFEM/src/branch/main/src/COPYING) and
[src/COPYING.LESSER](https://codeberg.org/cristian.barbarosie/maniFEM/src/branch/main/src/COPYING.LESSER).
It can also be found at <https://www.gnu.org/licenses/>.
